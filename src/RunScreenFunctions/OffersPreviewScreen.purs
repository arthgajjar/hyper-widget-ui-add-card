module RunScreenFunctions.OffersPreviewScreen where


import HyperPrelude.External
import HyperPrelude.Internal
import PaymentPageConfig (PaymentOptions(..), getCustomConfigResponse,configFunction)
import Flow.Types (FlowResponse(..))
import Remote.Types(ConfigPayload(..))
import Validation (ValidationState(..),InvalidState(..))
import Service.EC.Types.Instruments (EMIData(..),EMIPlan(..))
import Payments.Wallets.Types (MandateType(..))
import Service.EC.Types.Instruments (StoredCard(..),Wallet(..),StoredWallet(..),StoredVPA(..))
import Payments.Wallets.Types as WUtils
import Payments.NetBanking.Utils (Bank(..))
import Foreign.Object as H
import View.OffersPreview.Controllers.Controller(dummyOffer)
import View.OffersPreview.Screens.Screen as OffersPreview

run :: Flow FlowResponse -- make changes in payload
run = do
  confPayload <-  doAff configFunction
  let configPayload = getCustomConfigResponse confPayload
  let startCollect = false
  let screenInput =  { configPayload
                      , offers : [dummyOffer]
                     }
  screenOutput <- runScreen (OffersPreview.screen screenInput)
  case screenOutput of
    _ -> pure $ Return Nothing
