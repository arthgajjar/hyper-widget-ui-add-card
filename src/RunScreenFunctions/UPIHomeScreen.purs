module RunScreenFunctions.UPIHomeScreen where

import HyperPrelude.External
import HyperPrelude.Internal
import PaymentPageConfig (PaymentOptions(..), getCustomConfigResponse,configFunction)
import Flow.Types (FlowResponse(..))
import Remote.Types(ConfigPayload(..),defaultMerchantOffer)
import Validation (ValidationState(..),InvalidState(..))
import Service.EC.Types.Instruments as Instruments
import Payments.Wallets.Types (MandateType(..))
import Service.EC.Types.Instruments (EMIData(..),EMIPlan(..),StoredCard(..),Wallet(..),StoredWallet(..),StoredVPA(..))
import Payments.Wallets.Types as WUtils
import Payments.NetBanking.Utils (Bank(..))
import Foreign.Object as H
import Service.EC.Types.Response as ECRTypes
import View.OffersPreview.Controllers.Controller(dummyOffer)
import Payments.Core.Commons (getCheckoutDetails, getOffers, getValueFromPayload')
import Service.UPIIntent.Types(UPIApp(..))
import View.QuickPay.Controllers.Controller (QuickPayInstrumentType(..))
import Engineering.Helpers.Commons(PaymentOffer(..))
import View.UPI.Controllers.HomeController(StoredUpiVpa(..))
import View.UPI.Screens.HomeScreen as UpiScreenAndroid

run :: Flow FlowResponse
run = do
  confPayload <-  doAff configFunction
  let configPayload = getCustomConfigResponse confPayload
  let startCollect = false
  let suv = Instruments.StoredVPA 	{	vpa : "1234@upi"
                  		,	id : "vpaid"
                  		,	count : Nothing
                  		,	lastUsed : Nothing
                  		,	rating : Nothing
                  		}
  let suv2 = Instruments.StoredVPA 	{	vpa : "9131@upi"
                  		,	id : "paid"
                  		,	count : Nothing
                  		,	lastUsed : Nothing
                  		,	rating : Nothing
                  		}
  let offerDescription = 	Instruments.OfferDescription{ offerDescription : Just "offerdescription", offerDisplay1 : Just "offer description", offerDisplay2 : Just "offer description", offerDisplay3 : Just "offer description"}
  let paymentOffer =   PaymentOffer { paymentMethodType : "UPI", paymentMethod : "UPI", offerText : "Some offer text", paymentMethodFilter : Nothing, voucherCode : "axdcv", offerDescription : Just offerDescription}
  let upiapp = UPIApp { packageName : "com.google.android.apps.nbu.paisa.user", appName : "Google Pay"}
  let upiapp2 = UPIApp  {packageName: "com.phonepe.app", appName: "PhonePe"}
  let sv = StoredUpiVpa {bankImage : "",   bankName : "HDFC bank",  account : "",  vpa : "aman@upi" }
  let screenInput = { configPayload
                     , upiApps : [upiapp, upiapp2]
                     , savedVpas : [suv,suv2]
                     , savedUpiVpas : [sv]
                     , offers : [paymentOffer]
                     , defUPI : ""
                     , amount : 299.0
                     , mid : "vodafone_web"
                     , phoneNumber : "98888822"
                     , orderDesc : "Some description of order"
                     , showOnlyUPIApps : false
                     , customerName : "john doe"
                     , allBanks : []
                     , allCards : []
                     , allWallets : []
                     }
  screenOutput <- runScreen (UpiScreenAndroid.screen screenInput)
  case screenOutput of
    _ -> pure $ Return Nothing
