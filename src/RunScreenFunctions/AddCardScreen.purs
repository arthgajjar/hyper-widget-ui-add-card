module RunScreenFunctions.AddCardScreen where

import HyperPrelude.External
import HyperPrelude.Internal
import PaymentPageConfig (PaymentOptions(..), getCustomConfigResponse,configFunction)
import Flow.Types (FlowResponse(..))
import Remote.Types(ConfigPayload(..),MerchantOffer(..))
import Validation (ValidationState(..),InvalidState(..))
import Payments.Wallets.Types (MandateType(..))
import View.AddCard.Screens.Screen as AddCardScreen


run :: Flow FlowResponse
run = do
  confPayload <-  doAff configFunction
  let configPayload = getCustomConfigResponse confPayload
  let startCollect = false
  let merchantOffer = MerchantOffer{ applied :"", payment_method_type : Nothing, payment_method : Nothing, payment_card_type : Nothing, payment_card_issuer : Nothing, offer_code :Nothing, amount : Nothing}
  let screenInput =   { configPayload
                      , merchantOffer
                      , amount : 288.88
                      , savedCards : []
                      , cardNumber : { status : INVALID BLANK, value : ""}
                      , cVV : { status : INVALID BLANK, value : ""}
                      , expiry : { status : INVALID BLANK, value : ""}
                      , mandateFeature : Nothing
                      , cardTypes : []
                      , mid : "vodafone_web"
                      , allBanks : []
                      , allCards : []
                      , allWallets : []
                      , creditOutages : []
                      , debitOutages : []
                      , mandateType : None
                      , cardBinOffers : []
                      , phoneNumber : "9888888888"
                      , orderDesc : "Some Order Description"
                      , outages : []
                      , customerName : "Customer_Name"
                      }

  screenOutput <- runScreen (AddCardScreen.screen screenInput)
  case screenOutput of
    _ -> pure $ Return Nothing
