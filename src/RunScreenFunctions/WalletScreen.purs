module RunScreenFunctions.WalletScreen where

import HyperPrelude.External
import HyperPrelude.Internal
import PaymentPageConfig (PaymentOptions(..), getCustomConfigResponse,configFunction)
import Flow.Types (FlowResponse(..))
import Remote.Types(ConfigPayload(..),defaultMerchantOffer)
import Validation (ValidationState(..),InvalidState(..))
import Service.EC.Types.Instruments as Instruments
import Payments.Wallets.Types (MandateType(..))
import Service.EC.Types.Instruments (EMIData(..),EMIPlan(..),StoredCard(..),Wallet(..),StoredWallet(..),StoredVPA(..))
import Payments.Wallets.Types as WUtils
import Payments.NetBanking.Utils (Bank(..))
import Foreign.Object as H
import Service.EC.Types.Response as ECRTypes
import View.OffersPreview.Controllers.Controller(dummyOffer)
import Payments.Core.Commons (getCheckoutDetails, getOffers, getValueFromPayload')
import Service.UPIIntent.Types(UPIApp(..))
import View.QuickPay.Controllers.Controller (QuickPayInstrumentType(..))
import Engineering.Helpers.Commons(PaymentOffer(..))
import View.UPI.Controllers.HomeController(StoredUpiVpa(..))
import View.Wallet.Screens.Screen as WalletScreen


run :: Flow FlowResponse
run = do
  confPayload <-  doAff configFunction
  let configPayload = getCustomConfigResponse confPayload
  let startCollect = false
  let offerDescription = 	Instruments.OfferDescription{ offerDescription : Just "offerdescription", offerDisplay1 : Just "offer description", offerDisplay2 : Just "offer description", offerDisplay3 : Just "offer description"}
  let paymentOffer =   PaymentOffer { paymentMethodType : "WALLET", paymentMethod : "LAZYPAY", offerText : "Some offer text", paymentMethodFilter : Nothing, voucherCode : "axdcv", offerDescription : Just offerDescription}
  let wallet = Wallet { wallet :Just "LAZYPAY" , token : Just "12", linked :Just true, id : "123js", current_balance : Just 100.88, last_refreshed : Nothing, currentBalance :Just 100.88, metadata :Nothing , gateway_reference_id  : Just "" , lastRefreshed : Nothing, object :Nothing, status : Nothing, error_code : Nothing}
  let wallet2 = Wallet { wallet :Just "PAYTM" , token : Just "23", linked :Just true, id : "2dada", current_balance : Just 100.88, last_refreshed : Nothing, currentBalance :Just 100.88, metadata :Nothing , gateway_reference_id :Nothing, lastRefreshed : Nothing, object :Nothing, status : Nothing, error_code : Nothing}
  let screenInput =   { unlinkedWallets : []
                      , linkedWallets : [(StoredWallet wallet), (StoredWallet wallet2)]
                      , configPayload
                      , defWallet : ""
                      , amount : 299.99
                      , isPayLater : true
                      , payLaterEligibility :[]
                      , offers : [paymentOffer]
                      , justLinked : Just ""
                      , phoneNumber : "9888888888"
                      , orderDesc : "Some Order Description"
                      , mandateType : None
                      , outages : []
                      , mandateFeature : Nothing
                      , mid : "vodafone_web"
                      , customerName : "Customer_name"
                      , allBanks :[]
                      , allCards : []
                      , allWallets : []
                      }
  screenOutput <- runScreen (WalletScreen.screen screenInput)
  case screenOutput of
    _ -> pure $ Return Nothing
