module RunScreenFunctions.PaymentManagementScreen where


import HyperPrelude.External
import HyperPrelude.Internal
import PaymentPageConfig (PaymentOptions(..), getCustomConfigResponse,configFunction)
import Flow.Types (FlowResponse(..))
import Remote.Types(ConfigPayload(..))
import Validation (ValidationState(..),InvalidState(..))
import Service.EC.Types.Instruments as Instruments
import Payments.Wallets.Types (MandateType(..))
import Service.EC.Types.Instruments (EMIData(..),EMIPlan(..),StoredCard(..),Wallet(..),StoredWallet(..),StoredVPA(..))
import Payments.Wallets.Types as WUtils
import Payments.NetBanking.Utils (Bank(..))
import Foreign.Object as H
import Service.EC.Types.Response as ECRTypes
import View.OffersPreview.Controllers.Controller(dummyOffer)
import View.PaymentManagement.Screens.PaymentManagementScreen as PaymentManagementScreen


run :: Flow FlowResponse -- make changes in payload
run = do
  confPayload <-  doAff configFunction
  let configPayload = getCustomConfigResponse confPayload
  let startCollect = false
  let w = Wallet { wallet : Just "PAYTM"
              		, token : Nothing
              		, linked : Just true
              		, id : "walletid"
              		, current_balance : Just 645.99
              		, last_refreshed : Nothing
              		, currentBalance : Just 645.99
                  , metadata : Nothing
                  , gateway_reference_id : Nothing
              		, lastRefreshed : Nothing
              		, object : Nothing
              		, status : Nothing
              		, error_code : Nothing
              		}

  let w2 = Wallet { wallet : Just "LAZYPAY"
              		, token : Nothing
              		, linked : Just true
              		, id : "LazyPayWalletid"
              		, current_balance : Just 645.99
              		, last_refreshed : Nothing
              		, currentBalance : Just 645.99
                  , metadata : Nothing
                  , gateway_reference_id : Nothing
              		, lastRefreshed : Nothing
              		, object : Nothing
              		, status : Nothing
              		, error_code : Nothing
              		}


  let uw = WUtils.Wallet { code : ""
                  , name : "PAYTM"
                  , directDebitSupport : false
                  , mandateSupport : false
                  , status : Nothing
                  }


  let sw = StoredWallet w
  let sw2 = StoredWallet w2
  let sv = StoredVPA 	{	vpa : "1234@upi"
                  		,	id : "vpaid"
                  		,	count : Nothing
                  		,	lastUsed : Nothing
                  		,	rating : Nothing
                  		}
  let sv2 = StoredVPA 	{	vpa : "9131@upi"
                  		,	id : "vpaid2"
                  		,	count : Nothing
                  		,	lastUsed : Nothing
                  		,	rating : Nothing
                  		}
  let sc =   StoredCard { nickname : "John Doe"
                        , nameOnCard : "John Doe"
                        , expired : false
                        , cardType : "visa"
                        , cardToken : ""
                        , cardReference : ""
                        , cardNumber : "4242 4242 4242 4242"
                        , cardIssuer : ""
                        , cardIsin : ""
                        , cardFingerprint : ""
                        , cardExpYear : "22"
                        , cardExpMonth : "03"
                        , cardBrand : ""
                        , count : Nothing
                        , lastUsed : Nothing
                        , rating : Nothing
                        , mandateSupport : Nothing
                        }

  let sc2 =   StoredCard { nickname : "John Doe"
                        , nameOnCard : "John Doe"
                        , expired : false
                        , cardType : "visa"
                        , cardToken : ""
                        , cardReference : ""
                        , cardNumber : "4223 4234 4324 4323"
                        , cardIssuer : ""
                        , cardIsin : ""
                        , cardFingerprint : ""
                        , cardExpYear : "22"
                        , cardExpMonth : "03"
                        , cardBrand : ""
                        , count : Nothing
                        , lastUsed : Nothing
                        , rating : Nothing
                        , mandateSupport : Nothing
                        }
  let paymentSourceResp = ECRTypes.PaymentSourceResp  { wallets : [sw, sw2]
                                                      , vpas : [sv,sv2]
                                                      , nbMethods : []
                                                      , cards : [sc,sc2]
                                                      , lastUsedPaymentMethod :  Nothing
                                                      , merchantPaymentMethods :  [Instruments.MerchantPaymentMethod { supportedReferenceIds :Just ["vodafone"], paymentMethodType : "NB", paymentMethod : "NB_AXIS", description : "Axis Bank", walletDirectDebitSupport : Nothing}]
                                                      , appsUsed : []
                                                      , emandatePaymentMethods : Nothing
                                                      , offers : [dummyOffer]
                                                      , outages :Nothing
                                                      }

  let screenInput =   { savedWallets : [sw , sw2]
                      , savedCards : [sc, sc2]
                      , savedVpas : [sv, sv2]
                      , configPayload
                      , unlinkedWallets : [uw]
                      , defOptionType : ""
                      , defOption : ""
                      , gatewayReferenceId : ""
                      , paymentSourceResp
                      }
  screenOutput <- runScreen (PaymentManagementScreen.screen screenInput)
  case screenOutput of
    _ -> pure $ Return Nothing
