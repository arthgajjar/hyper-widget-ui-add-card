module RunScreenFunctions.UPIAddScreen where

import HyperPrelude.External
import HyperPrelude.Internal
import PaymentPageConfig (PaymentOptions(..), getCustomConfigResponse,configFunction)
import Flow.Types (FlowResponse(..))
import Remote.Types(ConfigPayload(..),defaultMerchantOffer)
import Validation (ValidationState(..),InvalidState(..))
import Service.EC.Types.Instruments as Instruments
import Payments.Wallets.Types (MandateType(..))
import Service.EC.Types.Instruments (EMIData(..),EMIPlan(..),StoredCard(..),Wallet(..),StoredWallet(..),StoredVPA(..))
import Payments.Wallets.Types as WUtils
import Payments.NetBanking.Utils (Bank(..))
import Foreign.Object as H
import Service.EC.Types.Response as ECRTypes
import View.OffersPreview.Controllers.Controller(dummyOffer)
import Payments.Core.Commons (getCheckoutDetails, getOffers, getValueFromPayload')
import Service.UPIIntent.Types(UPIApp(..))
import View.QuickPay.Controllers.Controller (QuickPayInstrumentType(..))
import View.UPI.Screens.AddScreen as VpaScreen



run :: Flow FlowResponse
run = do
  confPayload <-  doAff configFunction
  let configPayload = getCustomConfigResponse confPayload
  let startCollect = false
  let screenInput = {configPayload, startCollect : false, collectStatus : "", collectTTL : 300, isValidVpa : false, customerName : "", message : "", vpa: "", amount : 299.0, phoneNumber : "", orderDesc : "", useAsWidget : false, paymentCustomerName : " ", mid : "vodafone_web", allBanks : [], allCards : [], allWallets : [], savedVPAs : [], outages : [], offers : []}
  screenOutput <- runScreen (VpaScreen.screen screenInput)
  case screenOutput of
    _ -> pure $ Return Nothing
