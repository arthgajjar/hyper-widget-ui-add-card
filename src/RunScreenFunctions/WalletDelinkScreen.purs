module RunScreenFunctions.WalletDelinkScreen where

import HyperPrelude.External
import HyperPrelude.Internal
import PaymentPageConfig (PaymentOptions(..), getCustomConfigResponse,configFunction)
import Flow.Types (FlowResponse(..))
import Remote.Types(ConfigPayload(..),defaultMerchantOffer)
import Validation (ValidationState(..),InvalidState(..))
import Service.EC.Types.Instruments as Instruments
import Payments.Wallets.Types (MandateType(..))
import Service.EC.Types.Instruments (EMIData(..),EMIPlan(..),StoredCard(..),Wallet(..),StoredWallet(..),StoredVPA(..))
import Payments.Wallets.Types as WUtils
import Payments.NetBanking.Utils (Bank(..))
import Foreign.Object as H
import Service.EC.Types.Response as ECRTypes
import View.OffersPreview.Controllers.Controller(dummyOffer)
import Payments.Core.Commons (getCheckoutDetails, getOffers, getValueFromPayload')
import Service.UPIIntent.Types(UPIApp(..))
import View.QuickPay.Controllers.Controller (QuickPayInstrumentType(..))
import Engineering.Helpers.Commons(PaymentOffer(..))
import View.UPI.Controllers.HomeController(StoredUpiVpa(..))
import View.Wallet.Screens.DelinkScreen as DelinkWalletScreenAndroid


run :: Flow FlowResponse
run = do
  confPayload <-  doAff configFunction
  let configPayload = getCustomConfigResponse confPayload
  let startCollect = false
  let wallet = Wallet { wallet :Just "PAYTM", token : Just "", linked :Just true, id : "wdad", current_balance : Just 100.88, last_refreshed : Nothing, currentBalance :Just 100.88, metadata :Nothing , gateway_reference_id :Nothing, lastRefreshed : Nothing, object :Nothing, status : Nothing, error_code : Nothing}
  let wallet2 = Wallet { wallet :Just "AMAZONPAY", token : Just "", linked :Just true, id : "awdwww", current_balance : Just 100.88, last_refreshed : Nothing, currentBalance :Just 100.88, metadata :Nothing , gateway_reference_id :Nothing, lastRefreshed : Nothing, object :Nothing, status : Nothing, error_code : Nothing}
  let screenInput =   { storedWallets : [(StoredWallet wallet), (StoredWallet wallet2)]
                      , configPayload
                      , phoneNumber : "9888888888"
                      , amount : 299999.0
                      }
  screenOutput <- runScreen (DelinkWalletScreenAndroid.screen screenInput)
  case screenOutput of
    _ -> pure $ Return Nothing
