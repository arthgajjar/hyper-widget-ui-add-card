module RunScreenFunctions.InstrumentsScreen where

import HyperPrelude.External
import HyperPrelude.Internal
import PaymentPageConfig (PaymentOptions(..), getCustomConfigResponse,configFunction)
import Flow.Types (FlowResponse(..))
import Remote.Types(ConfigPayload(..))
import Validation (ValidationState(..),InvalidState(..))
import Service.EC.Types.Instruments (EMIData(..),EMIPlan(..))
import Payments.Wallets.Types (MandateType(..))
import Service.EC.Types.Instruments (StoredCard(..))
import Foreign.Object as H
import View.EMI.Screens.InstrumentsScreen as InstrumentsScreen



run :: Flow FlowResponse
run = do
  confPayload <-  doAff configFunction
  let configPayload = getCustomConfigResponse confPayload
  let startCollect = false
  let emiPlan = EMIPlan {transactionAmount : 280.00 , totalAmount : Just 290.00, min_amount : Just 250.00 , tenure : 3, interest : 10.00, gateway : "", gatewayId : 0, emiAmount : Just 100.00, bank : "Some bank"}
  let sc =   StoredCard { nickname : "John Doe"
                        , nameOnCard : "John Doe"
                        , expired : false
                        , cardType : "visa"
                        , cardToken : ""
                        , cardReference : ""
                        , cardNumber : "4242 4242 4242 4242"
                        , cardIssuer : ""
                        , cardIsin : ""
                        , cardFingerprint : ""
                        , cardExpYear : "22"
                        , cardExpMonth : "03"
                        , cardBrand : ""
                        , count : Nothing
                        , lastUsed : Nothing
                        , rating : Nothing
                        , mandateSupport : Nothing
                        }

  let sc2 =   StoredCard { nickname : "John Doe"
                        , nameOnCard : "John Doe"
                        , expired : false
                        , cardType : "visa"
                        , cardToken : ""
                        , cardReference : ""
                        , cardNumber : "4223 4234 4324 4323"
                        , cardIssuer : ""
                        , cardIsin : ""
                        , cardFingerprint : ""
                        , cardExpYear : "22"
                        , cardExpMonth : "03"
                        , cardBrand : ""
                        , count : Nothing
                        , lastUsed : Nothing
                        , rating : Nothing
                        , mandateSupport : Nothing
                        }

  let screenInput =   { configPayload
                      , emiPlans: H.empty
                      , storedCards : [sc,sc2]
                      , amount : 288.90
                      , phoneNumber : "988888888"
                      }

  screenOutput <- runScreen (InstrumentsScreen.screen screenInput)
  case screenOutput of
    _ -> pure $ Return Nothing
