module RunScreenFunctions.LoaderScreen where

import HyperPrelude.External
import HyperPrelude.Internal
import PaymentPageConfig (PaymentOptions(..), getCustomConfigResponse,configFunction)
import Flow.Types (FlowResponse(..))
import Remote.Types(ConfigPayload(..))
import Validation (ValidationState(..),InvalidState(..))
import Service.EC.Types.Instruments (EMIData(..),EMIPlan(..))
import Payments.Wallets.Types (MandateType(..))
import Service.EC.Types.Instruments (StoredCard(..))
import Foreign.Object as H
import View.Loader.Screens.Screen as LoaderScreen

run :: Flow FlowResponse -- make changes in payload
run = do
  confPayload <-  doAff configFunction
  let configPayload = getCustomConfigResponse confPayload
  let startCollect = false
  let emiPlan = EMIPlan {transactionAmount : 280.00 , totalAmount : Just 290.00, min_amount : Just 250.00 , tenure : 3, interest : 10.00, gateway : "", gatewayId : 0, emiAmount : Just 100.00, bank : "Some bank"}
  let screenInput = { customLoader : true
                    , parentId : ""
                    }

  screenOutput <- runScreen (LoaderScreen.screen screenInput)
  case screenOutput of
    _ -> pure $ Return Nothing
