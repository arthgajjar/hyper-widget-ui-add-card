module RunScreenFunctions.MoreOptionsScreen where

import HyperPrelude.External
import HyperPrelude.Internal
import PaymentPageConfig (PaymentOptions(..), getCustomConfigResponse,configFunction)
import Flow.Types (FlowResponse(..))
import Remote.Types(ConfigPayload(..))
import Validation (ValidationState(..),InvalidState(..))
import Service.EC.Types.Instruments (EMIData(..),EMIPlan(..))
import Payments.Wallets.Types (MandateType(..))
import Service.EC.Types.Instruments (StoredCard(..),Wallet(..),StoredWallet(..),StoredVPA(..))
import Payments.Wallets.Types as WUtils
import Payments.NetBanking.Utils (Bank(..))
import Foreign.Object as H
import View.MoreOptions.Screens.Screen as MoreOptionScreenAndroid

run :: Flow FlowResponse -- make changes in payload
run = do
  confPayload <-  doAff configFunction
  let configPayload = getCustomConfigResponse confPayload
  let startCollect = false
  let w = Wallet { wallet : Just "PAYTM"
              		, token : Nothing
              		, linked : Just true
              		, id : ""
              		, current_balance : Just 645.99
              		, last_refreshed : Nothing
              		, currentBalance : Just 645.99
                  , metadata : Nothing
                  , gateway_reference_id : Nothing
              		, lastRefreshed : Nothing
              		, object : Nothing
              		, status : Nothing
              		, error_code : Nothing
              		}


  let uw = WUtils.Wallet { code : ""
                  , name : "PAYTM"
                  , directDebitSupport : false
                  , mandateSupport : false
                  , status : Nothing
                  }

  let sw = StoredWallet w
  let sv = StoredVPA 	{	vpa : "1234@upi"
                  		,	id : ""
                  		,	count : Nothing
                  		,	lastUsed : Nothing
                  		,	rating : Nothing
                  		}
  let sc =   StoredCard { nickname : "John Doe"
                        , nameOnCard : "John Doe"
                        , expired : false
                        , cardType : ""
                        , cardToken : ""
                        , cardReference : ""
                        , cardNumber : "4242 4242 4242 4242"
                        , cardIssuer : ""
                        , cardIsin : ""
                        , cardFingerprint : ""
                        , cardExpYear : "22"
                        , cardExpMonth : "03"
                        , cardBrand : ""
                        , count : Nothing
                        , lastUsed : Nothing
                        , rating : Nothing
                        , mandateSupport : Nothing
                        }

  let screenInput = { configPayload,
                     amount : 299.88,
                     mid : "vodafone_web",
                     allBanks : [Bank {name : "NB_SBBJ",  code : "NB_SBBJ"}]
                    , allCards : [sc]
                    , allWallets : [sw]
                    }
  screenOutput <- runScreen (MoreOptionScreenAndroid.screen screenInput)
  case screenOutput of
    _ -> pure $ Return Nothing
