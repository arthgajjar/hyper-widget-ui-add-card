module RunScreenFunctions.CheckoutScreen where

import HyperPrelude.External
import HyperPrelude.Internal
import PaymentPageConfig (PaymentOptions(..), getCustomConfigResponse,configFunction)
import Flow.Types (FlowResponse(..))
import Remote.Types(ConfigPayload(..))
import Validation (ValidationState(..),InvalidState(..))
import Service.EC.Types.Instruments (EMIData(..),EMIPlan(..))
import Payments.Wallets.Types (MandateType(..))
import View.EMI.Screens.CheckoutScreen as EMICheckoutScreen


run :: Flow FlowResponse
run = do
  confPayload <-  doAff configFunction
  let configPayload = getCustomConfigResponse confPayload
  let startCollect = false
  let emiPlan = EMIPlan {transactionAmount : 280.00 , totalAmount : Just 290.00, min_amount : Just 250.00 , tenure : 3, interest : 10.00, gateway : "", gatewayId : 0, emiAmount : Just 100.00, bank : "Some bank"}
  let screenInput = 	{ configPayload
                    	, emiPlan : EMIData {amount : 288.99, tenure : 0, noCost : true, interest : 0.0, bank : "Some Bank", emiPlan}
                      , amount : 288.98
                      , subventionAmount : 280.00
                      , mandateType : None
                    	}

  screenOutput <- runScreen (EMICheckoutScreen.screen screenInput)
  case screenOutput of
    _ -> pure $ Return Nothing
