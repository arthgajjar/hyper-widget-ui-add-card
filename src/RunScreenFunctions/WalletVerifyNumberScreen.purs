module RunScreenFunctions.WalletVerifyNumberScreen where

import HyperPrelude.External
import HyperPrelude.Internal
import PaymentPageConfig (PaymentOptions(..), getCustomConfigResponse,configFunction)
import Flow.Types (FlowResponse(..))
import Remote.Types(ConfigPayload(..),defaultMerchantOffer)
import Validation (ValidationState(..),InvalidState(..))
import Service.EC.Types.Instruments as Instruments
import Payments.Wallets.Types (MandateType(..))
import Service.EC.Types.Instruments (EMIData(..),EMIPlan(..),StoredCard(..),Wallet(..),StoredWallet(..),StoredVPA(..))
import Payments.Wallets.Types as WUtils
import Payments.NetBanking.Utils (Bank(..))
import Foreign.Object as H
import Service.EC.Types.Response as ECRTypes
import View.OffersPreview.Controllers.Controller(dummyOffer)
import Payments.Core.Commons (getCheckoutDetails, getOffers, getValueFromPayload')
import Service.UPIIntent.Types(UPIApp(..))
import View.QuickPay.Controllers.Controller (QuickPayInstrumentType(..))
import Engineering.Helpers.Commons(PaymentOffer(..))
import View.UPI.Controllers.HomeController(StoredUpiVpa(..))
import View.Wallet.Screens.VerifyNumberScreen as VerifyNumberScreen

run :: Flow FlowResponse
run = do
  confPayload <-  doAff configFunction
  let configPayload = getCustomConfigResponse confPayload
  let startCollect = false
  let screenInput =   { configPayload
                      , walletName : "PAYTM"
                      , number : "9888888888"
                      , amount : 299.88
                      , orderDesc : "Some Order Description"
                      , customerName : "Customer_name"
                      , mid : "vodafone_web"
                      , allBanks : []
                      , allCards : []
                      , allWallets : []
                      , isOffer : true
                      }
  screenOutput <- runScreen (VerifyNumberScreen.screen screenInput)
  case screenOutput of
    _ -> pure $ Return Nothing
