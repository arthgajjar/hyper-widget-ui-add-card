module RunScreenFunctions.QuickPayScreen where


import HyperPrelude.External
import HyperPrelude.Internal
import PaymentPageConfig (PaymentOptions(..), getCustomConfigResponse,configFunction)
import Flow.Types (FlowResponse(..))
import Remote.Types(ConfigPayload(..),defaultMerchantOffer)
import Validation (ValidationState(..),InvalidState(..))
import Service.EC.Types.Instruments as Instruments
import Payments.Wallets.Types (MandateType(..))
import Service.EC.Types.Instruments (EMIData(..),EMIPlan(..),StoredCard(..),Wallet(..),StoredWallet(..),StoredVPA(..))
import Payments.Wallets.Types as WUtils
import Payments.NetBanking.Utils (Bank(..))
import Foreign.Object as H
import Service.EC.Types.Response as ECRTypes
import View.OffersPreview.Controllers.Controller(dummyOffer)
import Payments.Core.Commons (getCheckoutDetails, getOffers, getValueFromPayload')
import Service.UPIIntent.Types(UPIApp(..))
import View.QuickPay.Controllers.Controller (QuickPayInstrumentType(..))
import View.PaymentPage.QuickPay.Screens.Screen as QuickPayScreen


run :: Flow FlowResponse
run = do
  confPayload <-  doAff configFunction
  let configPayload = getCustomConfigResponse confPayload
  let startCollect = false
  let emiPlan = EMIPlan {transactionAmount : 280.00 , totalAmount : Just 290.00, min_amount : Just 250.00 , tenure : 3, interest : 10.00, gateway : "", gatewayId : 0, emiAmount : Just 100.00, bank : "Some bank"}
  let screenInput = { configPayload
                    , quickPayInstrument : UpiIntent
                    , amount : 288.99
                    , mandateType : None
                    , offers : []
                    }

  screenOutput <- runScreen (QuickPayScreen.screen screenInput)
  case screenOutput of
    _ -> pure $ Return Nothing
