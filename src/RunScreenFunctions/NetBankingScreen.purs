module RunScreenFunctions.NetBankingScreen where

import HyperPrelude.External
import HyperPrelude.Internal
import PaymentPageConfig (PaymentOptions(..), getCustomConfigResponse,configFunction)
import Flow.Types (FlowResponse(..))
import Remote.Types(ConfigPayload(..))
import Validation (ValidationState(..),InvalidState(..))
import Service.EC.Types.Instruments (EMIData(..),EMIPlan(..))
import Payments.Wallets.Types (MandateType(..))
import Service.EC.Types.Instruments (StoredCard(..),Wallet(..),StoredWallet(..),StoredVPA(..))
import Payments.Wallets.Types as WUtils
import Payments.NetBanking.Utils (Bank(..))
import Foreign.Object as H
import View.NB.Screens.Screen as NBScreen


run :: Flow FlowResponse -- make changes in payload
run = do
  confPayload <-  doAff configFunction
  let configPayload = getCustomConfigResponse confPayload
  let startCollect = false
  let screenInput =  { otherBanks : []
                     , popularBanks : []
                     , downBanks : []
                     , configPayload
                     , defNB : ""
                     , amount : 299.88
                     , offers : []
                     , mid : "vodafone_web"
                     , allCards : []
                     , allWallets : []
                     , phoneNumber : "9888888888"
                     , orderDesc : "some order description"
                     , customerName : "Customer_Name"
                     }
  screenOutput <- runScreen (NBScreen.screen screenInput)
  case screenOutput of
    _ -> pure $ Return Nothing
