window.currentPage = "";
window.localHistory = [];

function getOS() {
    if (window.parent.innerWidth < 700) {
        return "MOBILE_WEB";
    } else {
        return "WEB";
    }
    // var userAgent = navigator.userAgent;
    // if (userAgent) {
        // if (userAgent.indexOf("Android") != -1) {
        //     if (userAgent.indexOf("Version/") != -1) {
        //         return "ANDROID";
        //     }
        //     return "MOBILE_WEB";
        // }
        // if (userAgent.indexOf("iPhone") != -1) {
        //     if (userAgent.indexOf("Version/") != -1) {
        //         return "MOBILE_WEB";
        //     }
        //     return "IOS";

        // } else {
        //     return "WEB";
        // }
    // } else {
    //     console.log("Null user agent defaulting to android ");
    // }
}

exports['pushHistory'] = function (x) {
    // console.log("window.localHistory ", window.localHistory)
    if (getOS() === "MOBILE_WEB") {
        if (window.currentPage.length === 0) {
            window.currentPage = x;
        } else {
            window.localHistory.push(window.currentPage);
            window.currentPage = x;
        }
        if (!window.parent.location.href.includes('isEdit=true') && window.localHistory.length > 0) {
            
            window.parent.history.pushState(null, null, window.parent.location.href + '&isEdit=true');
        }
    }
    var payload ={
        event:"setPageTitle",
        data:x,
        history:window.localHistory
    }
    parent.postMessage(payload,"*");
    // updateHeight();
    // console.log("push ", payload);
    return "";
};

exports['popHistory'] = function (x) {
    // console.log("window.localHistory ", window.localHistory)
    console.log("pop history")
    if (getOS() === "MOBILE_WEB") {
        if (window.localHistory.length > 0) {
            window.currentPage = window.localHistory.pop()
        }
        if (window.parent.location.href.includes('isEdit=true') && window.localHistory.length <= 0) {   
            console.log("lets do history back");
            // history.back();
        }
    }
    if (!window.currentPage || window.currentPage.length == 0){
        return "";
    }
    var payload ={
        event:"setPageTitle",
        data:window.currentPage,
        history:window.localHistory
    }
    parent.postMessage(payload,"*")
    // updateHeight();
    // console.log("pop ", payload);
    return "";
};
