module Validation where

import Prelude

import Data.Array (elemLastIndex, filter, foldl, index, length, (!!),partition)
import Data.Foldable (elem)
import Data.Int (fromString)
import Data.Lens ((^.))
import Data.Maybe (Maybe(..), fromMaybe, maybe)
import Data.Number as Number
import Data.String (toLower)
import Data.String as String
import Global (readInt)
import Data.String.Common (toUpper)
import Debug.Trace (spy)
import Service.EC.Types.Instruments (MerchantPaymentMethod(..))
import Engineering.Helpers.Types.Accessor (_card_type, _luhn_valid, _supported_lengths)
import JBridge (CardDetails(..))
import PaymentPageConfig as PPConfig
import Remote.Types (CardInfo(..), MerchantOffer(..))
import Engineering.Helpers.Commons (PaymentOffer(..))
import Payments.Wallets.Types (MandateType(..))
import UI.Constant.Str.Default as STR
foreign import getCurrentMonth :: String -> Int
foreign import getCurrentYear :: String -> Int

correctBoxStroke :: String
correctBoxStroke = "1,#0FCE84"

errorBoxStroke :: String
errorBoxStroke = "1,#f0615e"

defaultBoxStroke :: String
defaultBoxStroke = "1,#97a9b3"

correctStateColor :: String
correctStateColor = "#666970"

errorStateColor :: String
errorStateColor = "#DD5C64"

defaultStateColor :: String
defaultStateColor = "#878992"

unsupportedError :: CardDetails -> String -> String
unsupportedError (CardDetails cardDetails) language = cardDetails.card_type <> (STR.cardNotSupError language)

-- Will be used for saveCard as well.
data InvalidState
    = BLANK
    | INPROGRESS
    | ERROR String

derive instance eqInvalidState :: Eq InvalidState

data ValidationState
    = VALID
    | INVALID InvalidState

derive instance eqEditFieldState :: Eq ValidationState


-- ADD_NEW_CARD VALIDATIONS
extractOutMethod
  :: MerchantPaymentMethod
  -> { paymentMethodType :: String , paymentMethod :: String , description :: String }
extractOutMethod (MerchantPaymentMethod mpM) =
    { paymentMethodType :  mpM.paymentMethodType
    , paymentMethod : mpM.paymentMethod
    , description : mpM.description
    }

compare'
  :: ∀ a
   . { card_type :: String | a }
  -> MerchantPaymentMethod
  -> Boolean
compare' d c = ( ( (String.toLower d.card_type)  )==   ( String.toLower  (extractOutMethod c).paymentMethod ) )

isTypeNotSupported :: String -> Maybe (Array String) -> Boolean
isTypeNotSupported cardType disabledTypes =
    case disabledTypes of
        Just disTypes -> do
            let filterArray = filter (\(cType) -> cType == cardType) disTypes
            filterArray /= []
        Nothing -> false


isNetworkNotSupported :: CardDetails -> Array MerchantPaymentMethod -> Maybe (Array String) -> Boolean
isNetworkNotSupported (CardDetails cardDetails) supportedMethods disabledNetworks =
    case disabledNetworks of
        Just disNetworks -> do
            let filterArray = filter (\(cType) -> cType == (toUpper cardDetails.card_type)) disNetworks
            filterArray /= []
        Nothing -> false


isCardBlocked :: String -> Maybe (Array String) -> Boolean
isCardBlocked cardNumber blockedCards =
    case blockedCards of
        Just blocked -> do
            let filterArray = filter (\(card) -> card == cardNumber) blocked
            filterArray /= []
        Nothing -> false
-- getMinLength :: Array Int -> Int
-- getMinLength =
--     foldl min 1000

-- getMaxLength :: Array Int -> Int
-- getMaxLength =
--     foldl max 0

getMinLength :: Array Int -> Int
getMinLength a = foldl min (fromMaybe 1000 (a !! 0)) a

getMaxLength :: Array Int -> Int
getMaxLength a = foldl max (fromMaybe 0 (a !! 0)) a

isCardIncomplete :: String -> CardDetails -> Boolean
isCardIncomplete cardNumber cardDetails =
    let l = getMaxLength (cardDetails ^. _supported_lengths)
     in String.length cardNumber < l


isMaxCardLength :: String -> CardDetails -> Boolean
isMaxCardLength cardNumber cardDetails =
    let l = getMaxLength (cardDetails ^. _supported_lengths)
     in l /= 0 && String.length cardNumber == l

isCardLengthInvalid :: String -> CardDetails -> Boolean
isCardLengthInvalid cardNumber cardDetails =
    let l = getMaxLength (cardDetails ^. _supported_lengths)
     in l /= 0 && String.length cardNumber > l

isCardStartInvalid :: String -> Boolean
isCardStartInvalid cardNum =
    let index = elemLastIndex (String.take 1 cardNum) ["1","7","9","0"]
     in case index of
           Just _ -> true
           Nothing -> false

isValidCandidate :: String -> CardDetails -> Boolean
isValidCandidate cardNumber cardDetails =
    let l = getMinLength (cardDetails ^. _supported_lengths)
     in l /= 1000 && String.length cardNumber >= l


bankMapperForCard :: String -> String -- not in android
bankMapperForCard bankOrg = do
    let bank = toLower bankOrg
    case bank of
        "axis bank"->"AXIS"
        "bank of india"->"BOI"
        "bank of maharashtra"->"BOM"
        "central bank of india"->"CBI"
        "corporation bank"->"CORP"
        "development credit bank"->"DCB"
        "federal bank"->"FED"
        "hdfc bank"->"HDFC"
        "icici bank"->"ICICI"
        "industrial development bank of india"->"IDBI"
        "indian bank"->"INDB"
        "indusind bank"->"INDUS"
        "indian overseas bank"->"IOB"
        "jammu and kashmir bank"->"JNK"
        "karnataka bank"->"KARN"
        "karur vysya"->"KVB"
        "state bank of bikaner and jaipur"->"SBBJ"
        "state bank of hyderabad"->"SBH"
        "state bank of india"->"SBI"
        "state bank of mysore"->"SBM"
        "state bank of travancore"->"SBT"
        "south indian bank"->"SOIB"
        "union bank of india"->"UBI"
        "united bank of india"->"UNIB"
        "vijaya bank"->"VJYB"
        "yes bank"->"YESB"
        "cityunion"->"CUB"
        "canara bank"->"CANR"
        "state bank of patiala"->"SBP"
        "citi bank "->"CITI"
        "deutsche bank"->"DEUT"
        "kotak bank"->"KOTAK"
        "dhanalaxmi bank"->"DLS"
        "ing vysya bank"->"ING"
        "andhra bank"->"ANDHRA"
        "punjab national bank corporate"->"PNBCORP"
        "punjab national bank"->"PNB"
        "bank of baroda"->"BOB"
        "catholic syrian bank"->"CSB"
        "oriental bank of commerce"->"OBC"
        "standard chartered bank"->"SCB"
        "tamilnad mercantile bank"->"TMB"
        "saraswat bank"->"SARASB"
        "syndicate bank"->"SYNB"
        "uco bank"->"UCOB"
        "bank of baroda corporate"->"BOBCORP"
        "allahabad bank"->"ALLB"
        "bank of bahrain and kuwait"->"BBKM"
        "janata sahakari bank"->"JSB"
        "lakshmi vilas bank corporate"->"LVBCORP"
        "lakshmi vilas bank retail"->"LVB"
        "north kanara gsb"->"NKGSB"
        "punjab and maharashtra coop bank"->"PMCB"
        "punjab and sind bank"->"PNJSB"
        "ratnakar bank"->"RATN"
        "royal bank of scotland"->"RBS"
        "shamrao vithal coop bank"->"SVCB"
        "tamil nadu state apex coop bank"->"TNSC"
        "dena bank"->"DENA"
        "cosmos bank"->"COSMOS"
        "dbs bank ltd"->"DBS"
        "dcb bank business"->"DCBB"
        "svc cooperative bank"->"SVC"
        "bharat bank"->"BHARAT"
        "karur vysya corporate banking"->"KVBCORP"
        "union bank corporate banking"->"UBICORP"
        "idfc bank"->"IDFC"
        "the nainital bank"->"NAIB"
        _ -> bank

offerApplied :: String -> CardDetails -> String -> String -> MerchantOffer -> Array PaymentOffer -> Boolean
offerApplied cardNum cardDetails cType cBank (MerchantOffer merchantOffer) cardBinOffers = do
    let offerBins = filter (\(PaymentOffer bins) -> Just bins.voucherCode == merchantOffer.offer_code) cardBinOffers !! 0
    if merchantOffer.applied == "true"
        then case merchantOffer.payment_method_type of
                Just "CARD" -> do
                    let cardNetwork = maybe false (\pm -> pm /= (toUpper $ cardDetails ^. _card_type)) merchantOffer.payment_method
                    let cardType = maybe false (\pct -> pct /= cType) merchantOffer.payment_card_type
                    let cardBank = maybe false (\pci -> cBank /= pci) merchantOffer.payment_card_issuer
                    let cardBin = isCardNumberOutsideBins cardNum offerBins
                    (cardNetwork || cardType || cardBank || cardBin)
                _ -> false
        else false


isCardNumberOutsideBins :: String -> Maybe PaymentOffer -> Boolean
isCardNumberOutsideBins cardNum (Just (PaymentOffer { paymentMethodFilter : Just offerBins})) = do
    let firstSix = readInt 10 $ String.take 6 cardNum
    not $ foldl (\acc bin -> acc || (isCardNumberInsideBin firstSix bin)) false offerBins
isCardNumberOutsideBins _ _ = false

isCardNumberInsideBin :: Number -> String -> Boolean
isCardNumberInsideBin firstSix bin = do
    let range = String.split (String.Pattern "::") bin
        lVal = readInt 10 $ fromMaybe "" $ range !! 0
        rVal = readInt 10 $ fromMaybe "" $ range !! 1
    firstSix >= lVal && firstSix <= rVal

offerAppliedTwo :: String -> CardDetails -> String -> String -> MerchantOffer -> Boolean  -- differnet in web and android
offerAppliedTwo cardNum cardDetails cType cBank (MerchantOffer merchantOffer) = do
    if merchantOffer.applied == "true"
        then case merchantOffer.payment_method_type of
                Just car -> do
                    case toUpper car of
                        "CARD"-> do
                            case merchantOffer.payment_method of
                                Just a -> do
                                            case a of
                                                ""-> do
                                                        let cBankNew = bankMapperForCard cBank
                                                        let cardType = maybe false (\pct -> pct /= (toUpper cType)) merchantOffer.payment_card_type
                                                        let cardBank = maybe false (\pci ->  (cBankNew /= pci) && ((toUpper $ cBank)/=pci)) merchantOffer.payment_card_issuer
                                                        case merchantOffer.payment_card_type of
                                                            Just b -> do
                                                                if b/=""
                                                                    then do
                                                                        case merchantOffer.payment_card_issuer of
                                                                            Just c -> if c==""
                                                                                        then cardType
                                                                                        else (cardType || cardBank)
                                                                            Nothing -> cardType

                                                                    else do
                                                                    case merchantOffer.payment_card_issuer of
                                                                        Just c -> if c==""
                                                                                    then false
                                                                                    else cardBank
                                                                        Nothing -> false
                                                            Nothing -> do
                                                                        case merchantOffer.payment_card_issuer of
                                                                            Just c -> if c==""
                                                                                        then false
                                                                                        else cardBank
                                                                            Nothing -> false
                                                _ -> do
                                                        let cBankNew = bankMapperForCard cBank
                                                        let crdType = if (toUpper $ cardDetails ^. _card_type) == "MASTER"
                                                                            then "MASTERCARD"
                                                                            else (toUpper $ cardDetails ^. _card_type)
                                                        let cardNetwork = maybe false (\pm -> pm /= crdType) merchantOffer.payment_method
                                                        let cardType = maybe false (\pct -> pct /= (toUpper cType)) merchantOffer.payment_card_type
                                                        let cardBank = maybe false (\pci -> (cBankNew /= pci) && ((toUpper $ cBank)/=pci)) merchantOffer.payment_card_issuer
                                                        case merchantOffer.payment_card_type of
                                                            Just b -> do
                                                                if b/=""
                                                                    then do
                                                                    case merchantOffer.payment_card_issuer of
                                                                            Just c -> if c==""
                                                                                        then (cardType || cardNetwork)
                                                                                        else (cardType || cardBank || cardNetwork)
                                                                            Nothing -> (cardType || cardNetwork)

                                                                    else do
                                                                        case merchantOffer.payment_card_issuer of
                                                                                Just c -> if c==""
                                                                                            then ( cardNetwork)
                                                                                            else (cardBank || cardNetwork)
                                                                                Nothing -> ( cardNetwork)

                                                            Nothing -> do
                                                                        case merchantOffer.payment_card_issuer of
                                                                                Just c -> if c==""
                                                                                            then ( cardNetwork)
                                                                                            else (cardBank || cardNetwork)
                                                                                Nothing -> ( cardNetwork)

                                Nothing -> do
                                        let cBankNew = bankMapperForCard cBank
                                        let cardType = maybe false (\pct -> pct /= (toUpper cType)) merchantOffer.payment_card_type
                                        let cardBank = maybe false (\pci -> (cBankNew /= pci) && ((toUpper $ cBank)/=pci)) merchantOffer.payment_card_issuer
                                        let _ = spy cBankNew merchantOffer.payment_card_issuer
                                        case merchantOffer.payment_card_type of
                                                            Just b -> do
                                                                if b/=""
                                                                    then do
                                                                        case merchantOffer.payment_card_issuer of
                                                                            Just c -> if c==""
                                                                                        then cardType
                                                                                        else (cardType || cardBank)
                                                                            Nothing -> cardType

                                                                    else do
                                                                    case merchantOffer.payment_card_issuer of
                                                                        Just c -> if c==""
                                                                                    then false
                                                                                    else cardBank
                                                                        Nothing -> false
                                                            Nothing -> do
                                                                        case merchantOffer.payment_card_issuer of
                                                                            Just c -> if c==""
                                                                                        then false
                                                                                        else cardBank
                                                                            Nothing -> false
                        _ -> false

                _ -> false
        else false



isGatewaySupported :: Array String -> String -> Boolean -- not in android
isGatewaySupported cardTypes cardBrand = do
        if (length cardTypes == 0)
            then false
            else
                if (\i -> cardBrand `elem` i) cardTypes
                    then false
                    else true
getCardIcon :: Boolean -> String -> String --differnet in android and web
getCardIcon  useLocal cardType= do
    if useLocal
        then case (String.toLower $ cardType) of
            "amex" -> PPConfig.getLocalIcon "amex" "dark" --"card_type_amex"
            "diners" -> PPConfig.getLocalIcon "diners" "dark"
            {-- "diners" -> "card_type_diners_club_international" --}
            "jcb" -> PPConfig.getLocalIcon "jcb" "dark"
            "laser" -> PPConfig.getLocalIcon "laser" "dark"
            "visa_electron" -> PPConfig.getLocalIcon "visa" "dark"
            "visa" -> PPConfig.getLocalIcon "visa" "dark"
            "master" -> PPConfig.getLocalIcon "mastercard" "dark"
            "mastercard" -> PPConfig.getLocalIcon "mastercard" "dark"
            "maestro" -> PPConfig.getLocalIcon "maestro" "dark"
            "rupay" -> PPConfig.getLocalIcon "rupay" "dark"
            "discover" -> PPConfig.getLocalIcon "discover" "dark"
            "default" -> "white_img"
            _ ->  PPConfig.getLocalIcon "default_zee" "dark"
        else
        case (String.toLower $ cardType) of
            "amex" -> "card_type_amex"
            "diners" -> "card_type_diners_club_carte_blanche"
            {-- "diners" -> "card_type_diners_club_international" --}
            "jcb" -> "card_type_jcb"
            "laser" -> "card_type_laser"
            "visa_electron" -> "card_type_visa_electron"
            "visa" -> PPConfig.getVisaIcon "#ffffff"
            "master" -> PPConfig.getMasterCardIcon "#ffffff"
            "mastercard" -> PPConfig.getMasterCardIcon "#ffffff"
            "maestro" -> "card_type_maestro"
            "rupay" -> "card_type_rupay"
            "discover" -> "card_type_discover"
            "default" -> "white_img"
            _ -> PPConfig.getAddCardIcon "#ffffff"

getCardPattern :: String -> String  -- not in android
getCardPattern cardType = case (String.toLower $ cardType) of
    -- "amex" -> "^([0-9]| )+$,24"
    -- "diners" -> "^([0-9]| )+$,18"
    -- {-- "diners" -> "card_type_diners_club_international" --}
    -- "jcb" -> "^([0-9]| )+$,18"
    -- "laser" -> "^([0-9]| )+$,18"
    -- "visa_electron" -> "^([0-9]| )+$,19"
    -- "visa" -> "^([0-9]| )+$,19"
    -- "master" -> "^([0-9]| )+$,19"
    -- "mastercard" -> "^([0-9]| )+$,19"
    -- "maestro" -> "^([0-9]| )+$,18"
    -- "rupay" -> "^([0-9]| )+$,18"
    -- "discover" -> "^([0-9]| )+$,18"
    -- "default" -> "^([0-9]| )+$,18"
    _ -> "^([0-9]| )+$,24"

getCardNumberStatus
    :: String
    -> Array String
    -> String
    -> CardDetails
    -> Maybe CardInfo
    -> Array MerchantPaymentMethod
    -> Maybe (Array String)
    -> Maybe (Array String)
    -> Maybe (Array String)
    -> MerchantOffer
    -> Array PaymentOffer
    -> MandateType
    -> Boolean
    -> ValidationState
getCardNumberStatus language onlyEnabledCards cardNumber cardDetails cardInfo supportedMethods disabledNetworks disabledTypes blockedCards merchantOffer cardBinOffers mandate enableSI= getStatus
    where
        -- cardInfo is from API call, cardDetails is local function
        enabledCardsSplitted = partition (\card -> String.toLower card == "credit" || String.toLower card == "debit") onlyEnabledCards
        enabledCardTypes = enabledCardsSplitted.yes
        enabledBanks = enabledCardsSplitted.no
        cnLength = String.length cardNumber
        cType = getCardType cardInfo
        cBank = getCardBank cardInfo
        mandateSupport = getMandateSupport cardInfo
        getStatus | cnLength == 0 = INVALID BLANK
                --   | isCardStartInvalid cardNumber = INVALID $ ERROR cardNumberError -- "start invalid"
                  | isNetworkNotSupported cardDetails supportedMethods disabledNetworks = INVALID $ ERROR $ (unsupportedError cardDetails language)
                  | cnLength > 5 && (isTypeNotSupported cType disabledTypes) = INVALID $ ERROR $ cType <> " card is not supported"
                  | cnLength <= 5 = INVALID INPROGRESS
                  | offerApplied cardNumber cardDetails cType cBank merchantOffer cardBinOffers = INVALID $ ERROR "Card not supported for selected offer"
                  | isCardIncomplete cardNumber cardDetails = INVALID INPROGRESS
                  | isMaxCardLength cardNumber cardDetails && not luhnValid = INVALID $ ERROR (STR.cardNumberError "")-- "luhn failed"
                  | isCardLengthInvalid cardNumber cardDetails = INVALID $ ERROR (STR.cardNumberError "") --  "Length Invalid"
                  | not mandateSupportCheck = INVALID $ ERROR STR.getMandateSupportError
                  | isCardBankAllowed cBank  = INVALID $ ERROR $ if (cBank == "") then "Card Not Supported" else (cBank <>" not allowed")
                  | isCardTypeAllowed cType = INVALID $ ERROR $ if (cType == "") then "Card Not Supported" else (cType <> " not allowed")
                  {-- | isMaxCardLength cardNumber cardDetails && isNotSupported cardDetails supportedMethods = INVALID $ ERROR "Card is not supported." --}
                  | isCardBlocked cardNumber blockedCards = INVALID $ ERROR "Card Blocked"
                  | isValidCandidate cardNumber cardDetails && luhnValid = VALID
                  | otherwise = INVALID $ ERROR (STR.cardNumberError "")

        luhnValid = cardDetails ^. _luhn_valid

        supportedLengths = cardDetails ^. _supported_lengths


        isCardBankAllowed bank = do
            if length enabledBanks == 0 || bank == ""
                then false
                else not $
                    (foldl (\acc item -> acc || String.contains (String.Pattern (String.toLower item)) (String.toLower bank)) false enabledBanks)

        isCardTypeAllowed cardType = do
            if length enabledCardTypes == 0 || cardType == ""
                then false
                else not $
                    (foldl (\acc item -> acc ||String.toLower item == String.toLower cardType ) false enabledCardTypes)

        mandateSupportCheck =
            case mandate of
                Required -> mandateSupport
                Optional -> if enableSI then mandateSupport else true
                _ -> true

getCardSIEnableStatus --  not in android
    :: String
    -> Maybe CardInfo
    -> Maybe Boolean
    -> ValidationState
getCardSIEnableStatus cardNumber cardInfo mandateFeature =
    getStatus
    where
        -- cardInfo is from API call, cardDetails is local function
        cnLength = String.length cardNumber
        cMandate = getMandateSupport cardInfo
        getStatus | ((cnLength > 6) && (mandateFeature /= Just true) && (cMandate == false)) = INVALID $ ERROR $ STR.getSiNotSupported
                  | otherwise = VALID


getCardType :: Maybe CardInfo -> String
getCardType =
  case _ of
       Nothing -> ""
       Just (CardInfo cInfo) -> cInfo."type"

getCardBank :: Maybe CardInfo -> String
getCardBank =
  case _ of
       Nothing -> ""
       Just (CardInfo cInfo) -> cInfo.bank



isCardAllowed :: Maybe CardInfo -> String -> Boolean
isCardAllowed cardInfo internationalCardsAllowed =
  case cardInfo of
       Nothing -> true
       Just (CardInfo cInfo) -> if (cInfo.country == "INDIA")
                                    then true
                                    else if internationalCardsAllowed == "true"
                                                then true
                                                else false

getMandateSupport :: Maybe CardInfo -> Boolean
getMandateSupport =
    case _ of
        Nothing -> true
        Just (CardInfo cInfo) -> fromMaybe false cInfo.mandate_support


-- | TODO : Change year < ((getCurrentYear "") `mod` 100) logic to patch cards meant to expire in 2100+
getExpiryDateStatus :: String -> ValidationState
getExpiryDateStatus expiryDate = getStatus
    where
        getStatus | ( String.length expiryDate ) == 0 = INVALID BLANK
                  | String.length expiryDate < 5  = INVALID INPROGRESS
                  | year < currentYear = INVALID $ ERROR $ STR.cardExpired ""
                  | year == currentYear && month < (getCurrentMonth "") = INVALID $ ERROR $ STR.cardExpired ""
                  | year > 50 = INVALID $ ERROR $ STR.invalidExpiry "" -- | This logic is here because EC don't suport card with exp date above 50
                  | month > 12 = INVALID $ ERROR $ STR.invalidMonth ""
                  | month < 1 = INVALID $ ERROR $ STR.invalidMonth ""
                  | otherwise = VALID

        month = getMonth expiryDate
        year = getYear expiryDate
        currentYear = (getCurrentYear "") `mod` 100


getMonth :: String -> Int
getMonth expiryDate = fromMaybe (negate 1) (fromString ( fromMaybe "-1" (index (String.split (String.Pattern "/") expiryDate) 0) ) )

getYear :: String -> Int
getYear expiryDate = fromMaybe (negate 1) (fromString ( fromMaybe "-1" (index (String.split (String.Pattern "/") expiryDate) 1) ) )


reqCVVLength :: String -> Int
reqCVVLength cardType = if String.toLower (cardType) == "amex"
                    then 4
                    else 3

getCvvStatus :: String -> String -> ValidationState
getCvvStatus cvv cardType
    | ( String.length cvv ) == 0 = INVALID BLANK
    | ( String.length cvv ) < (reqCVVLength cardType) = INVALID INPROGRESS
    | (String.toLower (cardType) == "maestro") = VALID
    | ( if String.toLower (cardType) == "amex" then 4 else 3 ) == ( String.length cvv ) = VALID
    | otherwise = INVALID $ ERROR "invalid cvv"


         {-- cvv = state.formState.cvv.value --}

         {-- cardType = state.formState.cardNumber.cardDetails.card_type --}


getAllValidation :: ValidationState -> ValidationState -> ValidationState -> Boolean -> ValidationState
getAllValidation =
    case _,_,_,_ of
        VALID,_,_,true              -> VALID
        VALID,VALID,VALID,_         -> VALID
        INVALID err@(ERROR _),_,_,_ -> INVALID err
        _,INVALID err@(ERROR _),_,_ -> INVALID err
        _,_,INVALID err@(ERROR _),_ -> INVALID err
        _,_,_,_                     -> INVALID BLANK


validateData :: ValidationState -> Boolean
validateData =
    case _ of
         VALID -> true
         _ -> false

getValidationColor :: String -> ValidationState -> String
getValidationColor errorColor status =
    case status of
         INVALID (ERROR _) -> errorColor
         VALID -> correctStateColor
         _ -> defaultStateColor

getErrorText :: ValidationState -> String
getErrorText =
    case _ of
         INVALID (ERROR txt) -> txt
         _ -> ""

getCardLabel :: ValidationState -> String
getCardLabel =
    case _ of
         VALID -> STR.paybill ""
         INVALID (ERROR _) -> STR.verifyCard ""
         _ -> STR.addNewCard ""

getAmountValidation :: String -> String -> String -> ValidationState
getAmountValidation currAmount minAmount maxAmount = do
    if currAmount == ""
        then INVALID BLANK
        else do
            let amountint = Number.fromString currAmount
            let min = 1.0--fromMaybe 0.0 $ Number.fromString minAmount
            let max = 1000000.0--fromMaybe 1000000.0 $ Number.fromString maxAmount
            case amountint of
                Just a -> if (a >= min && a <= max)
                            then VALID
                            else INVALID $ ERROR $ STR.errorText4 ""
                Nothing -> INVALID $ ERROR $ STR.errorText4 ""
