module Engineering.Helpers.Events where

import Control.Monad.Except (runExcept)
import Data.Either (Either(..))
import Data.Generic.Rep (class Generic)
import Data.Maybe (Maybe(..))
import Data.Newtype (class Newtype)
import Data.String as Str
import Effect (Effect)
import Engineering.Helpers.Commons (callAPI', NativeRequest(..))
import Engineering.Helpers.Store as Store
import Halogen.VDom.DOM.Prop (Prop(..))
import JBridge (getCardValidation)
import Foreign (Foreign)
import Foreign.Class (class Encode, encode)
import Foreign.Generic (encodeJSON)
import Payments.Core.Commons (Checkout, checkoutDetails, getCheckoutDetails)
import Prelude (class Applicative, class Bind, Unit, bind, const, discard, pure, unit, ($), (*>), (<), (<<<), (<>), (||))
import Presto.Core.Types.API (URL)
import Presto.Core.Utils.Encoding (defaultDecodeJSON,defaultEncode)
import PrestoDOM (onBackPressed)
import PrestoDOM.Types.Core (Prop)
import Remote.Types (MerchantOffer, defaultMerchantOffer)
import UI.Utils (FieldType(..), _cvvHandler, _expiryHandler, getFieldTypeID)
import Remote.Types (ConfigPayload)
import PaymentPageConfig (getCustomConfigResponse, defaultValue)

import Unsafe.Coerce as U
import Validation as V
import Web.Event.Event (EventType(..), Event) as DOM

foreign import checkFirstSixNotChanged :: String -> Effect Boolean
foreign import executeEvent :: String -> (DOM.Event -> Effect Unit) -> (DOM.Event -> Effect Unit)
foreign import onMerchantEvent' :: (String -> Effect Unit) -> Effect Unit


foreign import _emitEvent ::  String -> Effect Unit -- not in web
newtype JP_CONSUMING_BACKPRESS = JP_CONSUMING_BACKPRESS { -- not in web
  jp_consuming_backpress :: Boolean
}
derive instance genericJP_CONSUMING_BACKPRESS :: Generic JP_CONSUMING_BACKPRESS _  -- not in web

instance encodeJP_CONSUMING_BACKPRESS :: Encode JP_CONSUMING_BACKPRESS where  -- not in web
  encode = defaultEncode
newtype EventResult = EventResult {event :: String, payload :: Foreign}  -- not in web
derive instance eventResultGeneric :: Generic EventResult  _  -- not in web
derive instance eventResultNewtype :: Newtype EventResult _  -- not in web
instance encodeEventResult :: Encode EventResult where encode = defaultEncode  -- not in web

initiateCardInfo :: String -> Effect Unit
initiateCardInfo dCN = do
    let cN = Str.replace (Str.Pattern " ") (Str.Replacement "") dCN
        cardDetails = getCardValidation cN
        bool1 = V.isNetworkNotSupported cardDetails [] Nothing
        bool2 = V.isCardStartInvalid cN
        fstSix = Str.take 6 cN
        bool3 = Str.length fstSix < 6

    boolC <- checkFirstSixNotChanged fstSix
    if (bool1 || bool2 || bool3 || boolC)
        then pure unit
        else do
            checkoutDetails <- getCheckoutDetails
            Store.initiate
              (Store.Key "cardInfo")
              (\cb ->
                  callAPI'
                      (cb <<< Left)
                      (cb <<< Right)
                      (NativeRequest { method : "GET"
                                      , url : getUrl fstSix checkoutDetails
                                      , payload : ""
                                      , headers : []
                                      }
                      )
              )

    where
          getUrl :: String -> Checkout -> URL
          getUrl cN checkoutDetails = (baseUrl checkoutDetails.environment)
            <> cN
            <> "?options.check_direct_otp_support=true&merchant_id="
            <> checkoutDetails.merchant_id
            <> "&options.check_mandate_support=true"
            where
              baseUrl "sandbox" = "https://sandbox.juspay.in/cardbins/"
              baseUrl         _ = "https://api.juspay.in/cardbins/"

makeCardNumberEvent :: ∀ a. (a -> Effect Unit) -> (DOM.Event -> Effect Unit)
makeCardNumberEvent push = \ev -> do
    let (cN :: String) = U.unsafeCoerce ev
    _ <- push (U.unsafeCoerce ev)
    initiateCardInfo cN
    pure unit

makeEvent :: ∀ a b c d. Bind a => Applicative a => (c -> a b) -> d -> a Unit -- not in android
makeEvent push = \ev -> do
    _ <- push (U.unsafeCoerce ev)
    pure unit

-- onMenuItemClick :: ∀ a eff. (a -> Effect Unit ) -> (Int -> a) -> Prop (Effect Unit)
-- onMenuItemClick push f = Handler (DOM.EventType "onMenuItemClick") (Just <<< (makeEvent (push <<< f)))

-- onFocus :: ∀ a eff. (a -> Effect Unit ) -> (Boolean -> a) -> Prop (Effect Unit)
-- onFocus push f = Handler (DOM.EventType "onFocus") (Just <<< (makeEvent (push <<< f <<< toBool)))

-- onLongPress :: ∀ a eff. (a -> Effect Unit ) -> (Boolean -> a) -> Prop (Effect Unit)
-- onLongPress push f = Handler (DOM.EventType "onLongPress") (Just <<< (makeEvent (push <<< f)))

toBool :: String -> Boolean
toBool "false" = false
toBool _ = true

toMerchantOfferType :: String -> MerchantOffer
toMerchantOfferType offerBundle = do
    let response = (runExcept $ defaultDecodeJSON offerBundle :: _ MerchantOffer)
    case response of
        Right offers -> offers
        Left err -> defaultMerchantOffer

toConfigType :: String -> ConfigPayload  --not in android
toConfigType config = do
    let response = (runExcept $ defaultDecodeJSON config :: _ ConfigPayload)
    case response of
        Right resp -> do
            resp
        Left err -> do
            defaultValue--getCustomConfigResponse

foreign import timerHandlerImpl :: ∀  a. Int -> Int -> (a -> Effect Unit) -> Effect Unit
foreign import timerHandlerImplWithIntervalImpl :: ∀  a. Number -> (a ->  Effect Unit) -> Effect Unit

foreign import cancelTimerHandler :: Effect Unit

-- timerHandler :: ∀ eff a. Int -> (a ->  Effect Unit) -> (DOM.Event → Effect Unit)
-- timerHandler time push = \ev -> do
--     _ <- timerHandlerImpl time push
--     pure unit

timerHandlerImplWithInterval :: ∀ a. Number -> (a ->  Effect Unit) -> (DOM.Event → Effect Unit)
timerHandlerImplWithInterval time push = \ev -> do
    _ <- timerHandlerImplWithIntervalImpl time push
    pure unit

attachTimer :: ∀ a . Int -> Int -> (a ->  Effect Unit) -> (Int -> a) -> Prop (Effect Unit)
attachTimer time interval push f =
  let _ = timerHandlerImpl time interval (push <<< f)
  in Handler (DOM.EventType "executeTimer") (Just <<< const (pure unit))

triggerEvent :: ∀ a . Number -> (a ->  Effect Unit) -> (Unit -> a) -> Prop (Effect Unit)
triggerEvent time push f = Handler (DOM.EventType "executeCustomEvent") (Just <<< timerHandlerImplWithInterval time (push <<< f))

-- registerNewEvent :: ∀ a eff. String -> (a ->  Effect Unit) -> (Unit -> a) -> Prop (Effect Unit)
-- registerNewEvent eventType push f = x
--   where
--     x = Handler (DOM.EventType eventType) (Just <<< (makeEvent (push <<< f)))
--     p = registerEvent eventType $ makeEvent (push <<< f)

-- registerNewListener :: ∀ t31 t34 t41 t42 t43 t44. Semigroupoid t41 => (t31 -> t41 t44 t42 -> Event -> t34) -> t31 -> t41 t43 t42 -> t41 t44 t43 -> Prop t34
-- registerNewListener handler id push f = Handler (DOM.EventType "onChange") (Just <<< (handler id (push <<< f)))


-- registerNewEventWithString :: ∀ a eff. String -> (a ->  Effect Unit) -> (String -> a) -> Prop (Effect Unit)
-- registerNewEventWithString eventType push f = x
--   where
--     x = Handler (DOM.EventType eventType) (Just <<< (makeEvent (push <<< f)))
--     p = registerEvent eventType $ makeEvent (push <<< f)



-- ****** --
-- exports.timerHandlerImplWithIntervalImpl = function(time) {
-- return function(push) {
-- if (window.__OS.toLowerCase() != "ios") {
-- var clearId = setInterval(function() {
-- push()();
-- }, time * 1000);
-- setTimeout(function() {
-- clearInterval(clearId);
-- }, time * 1000);
-- } else {
-- var timerCallback = callbackMapper.map(function(value) {
-- push()();
-- });
-- JBridge.startTimerWithTime(0, time, timerCallback);
-- }
-- };
-- };
-- foreign import timerHandlerImplWithIntervalImpl :: ∀ eff a. Number -> (a -> Effect Unit) -> Effect Unit

-- triggerEvent :: ∀ a eff. Number -> (a -> Effect Unit) -> (Unit -> a) -> Prop (Effect Unit)
-- triggerEvent time push f =
-- let _ = timerHandlerImplWithIntervalImpl time (push <<< f)
-- in Handler (DOM.EventType "executeCustomEvent") (Just <<< const (pure unit))

onFocus :: ∀ a. (a -> Effect Unit) -> (Boolean -> a) -> Prop (Effect Unit)
onFocus push f = Handler (DOM.EventType "onFocus") (Just <<< (makeEvent (push <<< f <<< toBool)))

onWidthUpdate :: ∀ a. (a -> Effect Unit) -> (Boolean -> a) -> Prop (Effect Unit)  -- not in android
onWidthUpdate push f = Handler (DOM.EventType "onWidthUpdate") (Just <<< (makeEvent (push <<< f <<< toBool)))

onOrderUpdate :: ∀ a. (a -> Effect Unit) -> (MerchantOffer -> a) -> Prop (Effect Unit)
onOrderUpdate push f = Handler (DOM.EventType "updateOrder") (Just <<< (makeEvent (push <<< f <<< toMerchantOfferType)))


onToggleUpdate :: ∀ a. (a -> Effect Unit) -> (Boolean -> a) -> Prop (Effect Unit)  -- not in android
onToggleUpdate  push f = Handler (DOM.EventType "click") (Just <<< (makeEvent (push <<< f <<< toBool)))

onConfigUpdate :: ∀ a. (a -> Effect Unit) -> (ConfigPayload -> a) -> Prop (Effect Unit)  -- not in android
onConfigUpdate push f =
        Handler (DOM.EventType "updateConfigPayload") (Just <<< (makeEvent (push <<< f <<< toConfigType)))

cardNumberOnChange :: ∀ a. (a -> Effect Unit) -> (String -> a) -> Prop (Effect Unit)
cardNumberOnChange push f  = Handler (DOM.EventType "onChange") (Just <<< (makeCardNumberEvent (push <<< f)))


expiryOnChange :: ∀ a. (a -> Effect Unit) -> (String -> a) -> Prop (Effect Unit)
expiryOnChange = registerNewListener expiryHandler (getFieldTypeID ExpiryDate)

registerNewListener
  :: ∀ a b
     . (String -> (a -> Effect Unit) -> DOM.Event -> Effect Unit)
  -> String
  -> (b -> Effect Unit)
  -> (a -> b)
  -> Prop (Effect Unit)
registerNewListener handler id push f = Handler (DOM.EventType "onChange") (Just <<< (handler id (push <<< f)))

registerNewEvent :: ∀ a b. String -> (a ->  Effect Unit) -> (b -> a) -> Prop (Effect Unit)
registerNewEvent eventType push f = Handler (DOM.EventType eventType) (Just <<< (makeEvent (push <<< f)))



expiryHandler :: ∀ a. String -> (a -> Effect Unit) -> (DOM.Event -> Effect Unit)
expiryHandler id push = \str -> do
    _ <- _expiryHandler id str push
    pure unit


cvvOnChange :: ∀ a. (a -> Effect Unit) -> (String -> a) -> Prop (Effect Unit) -- not in android
cvvOnChange = registerNewListener cvvHandler ""

cvvHandler :: ∀ a. String -> (a -> Effect Unit) -> (DOM.Event -> Effect Unit) -- not in android
cvvHandler id push = \str -> do
  _ <- _cvvHandler id str push
  pure unit

getFromStoreEventImpl :: ∀ a. Store.Key -> (a -> Effect Unit) -> (DOM.Event -> Effect Unit)
getFromStoreEventImpl key push = \ev -> do
    _ <- Store.lazyGet key push
    pure unit

getFromStoreEvent :: ∀ a b. Store.Key -> (a -> Effect Unit) -> (b -> a) -> Prop (Effect Unit)
getFromStoreEvent key@(Store.Key k) push f = Handler (DOM.EventType $ "getFromStore" <> k) (Just <<< fn)
    where
      fn = executeEvent ("getFromStore" <> k) (getFromStoreEventImpl key (push <<< f))


-- (DOM.Event -> Effect Unit)
foreign import setEvent :: (DOM.Event -> Effect Unit) -> (DOM.Event -> Effect Unit)
foreign import customBackPressHandelerImpl :: ∀  a. (a -> Effect Unit) -> Effect Unit

customBackPressHandeler :: ∀ a. (a ->  Effect Unit) -> (DOM.Event → Effect Unit)
customBackPressHandeler push = \ev -> do
    _ <- (customBackPressHandelerImpl push *> pure unit )
    -- _ <- push (U.unsafeCoerce ev)
    pure unit

addCustomBackPress :: ∀ a .(a ->  Effect Unit) -> (Unit -> a) -> Prop (Effect Unit)
addCustomBackPress = onBackPressed

afterRenderEvent :: (Effect Unit) -> Prop (Effect Unit)
afterRenderEvent effFn = Handler (DOM.EventType "afterRender") (Just <<< (\ev -> effFn))

-- onMerchantEvent :: Flow Unit
-- onMerchantEvent = doAff do
--   args <- makeAff (\sc -> onMerchantEvent' (Right >>> sc) *> pure nonCanceler)
--   pure unit --(NoBack (MerchantEvent (args :: String)))
