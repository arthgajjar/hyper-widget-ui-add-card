module Engineering.Helpers.Commons where

import Prelude

import Control.Monad.Except.Trans (ExceptT(..))
import Control.Transformers.Back.Trans (BackT(..), FailBack(..))
import Data.Array (filter, (!!), foldl, length)
import Data.Either (Either(..))
import Data.Foldable (elem)
import Data.Function.Uncurried (Fn2, Fn3, runFn3)
import Data.Maybe (Maybe(..), fromMaybe)
import Data.String (toLower)
import Service.EC.Types.Instruments (MerchantPaymentMethod(..), Offer(..), OfferDescription(..))
import Effect (Effect)
import Effect.Aff (makeAff, nonCanceler)
import Effect.Class (liftEffect)
import Effect.Exception (Error)
import Engineering.Types.App (FlowBT, PaymentPageError, liftFlowBT)
import Engineering.Types.App as App
import Foreign (Foreign, isNull, unsafeFromForeign)
import Presto.Core.Flow (Flow, doAff, oneOf, runUI)
import Presto.Core.Types.API (Header(..), Headers(..), Request(..), URL)
import Presto.Core.Types.Language.Flow (runScreen)
import Presto.Core.Types.Language.Interaction (class Interact)
import PrestoDOM (Screen)

foreign import filterGatewayRef :: Array MerchantPaymentMethod->Array String -> Array MerchantPaymentMethod
foreign import removeDuplicate :: Unit -> Boolean
foreign import canBecomeValidVPA ::String -> Boolean
foreign import convertMdToHtml :: String -> String
foreign import getVoucherDetail :: Unit -> Boolean
foreign import getMoreOptions :: Unit -> Array String
foreign import removeFocusVPA ::Boolean -> String -> String
foreign import getBankHighlightedName :: Array String -> String
foreign import showUI' :: Fn2 (String -> Effect  Unit) String (Effect Unit)
foreign import callAPI' :: AffError -> AffSuccess String -> NativeRequest -> (Effect Unit)
foreign import bankList :: Unit -> String
foreign import log :: ∀ a. String -> a -> a
foreign import startAnim :: String -> Effect Unit
foreign import startAnim_ :: String -> Unit
foreign import onBackPress' :: (Unit -> Effect Unit) -> Effect Unit
foreign import unsafeGet' :: String -> Effect Foreign
foreign import setOnWindow' :: ∀ a. String -> a -> Effect Unit
foreign import dpToPx :: Int -> Int
foreign import bringToFocus :: String -> String -> Boolean
foreign import checkPermissions :: (Array String) -> Effect String
foreign import requestPermissions :: (AffSuccess String ) -> (Array String) -> Effect Unit
foreign import getIin :: String -> String
foreign import getNbIin :: String -> String
foreign import getIinNb :: String -> String
foreign import getIinFromName :: String -> String
foreign import ourMaybe :: ∀ a. Maybe a -> a
foreign import getLabelWidth :: String -> String -> Int -> Int -> Int -> Int
foreign import getLabelHeight :: String -> String -> Int -> Int -> Int -> Int
foreign import getNameFromIin :: String -> String
foreign import arrToStr :: Array String -> String
foreign import replaceAndCapitalize :: String -> String
foreign import getValueFromPayload' :: String -> String
foreign import getOS :: Unit -> String
foreign import getSessionInfo :: ∀ json. Effect {|json}
foreign import getSMSFromInbox :: String -> Array { from::String,time::String }
foreign import getPaymentApps :: Effect (Array String)
foreign import subtractTime :: String -> Int -> String
foreign import substr :: Int -> Int -> String -> String
foreign import getUrl :: String -> String
foreign import textOrderBar :: String -> String
foreign import getFocusId :: Unit -> String
foreign import getFocusClass :: Unit -> String
foreign import unsafeJsonStringify :: ∀ a. a -> String
foreign import unsafeJsonDecodeImpl :: ∀ a. Fn3 String (a -> Maybe a) (Maybe a) (Maybe a)
foreign import dropLastChar :: String -> String
foreign import getFormattedBankNameAndIIN :: ∀ json. String -> {|json}
foreign import getValueFromIPField :: String -> String
foreign import stripPrefixCustom :: String -> String ->Array String
foreign import isPopUp :: Unit -> Boolean
foreign import isFilterExists :: String -> Array String -> Boolean
foreign import getOfferText :: String -> Array PaymentOffer -> String
foreign import hasFocus :: String -> Boolean
foreign import getFormattedIssuerName :: String -> String
foreign import emiBankIINMap :: String -> String

--checked from here
type NativeHeader = { field :: String , value :: String}
type NativeHeaders = Array NativeHeader
type AffError = (Error -> Effect Unit)
type AffSuccess s = (s -> Effect Unit)

newtype PaymentOffer = -- not in android
  PaymentOffer { paymentMethodType :: String
  , paymentMethod :: String
  , offerText :: String
  , paymentMethodFilter :: Maybe (Array String)
  , voucherCode :: String
  , offerDescription :: Maybe OfferDescription
  }



isHavingOffer :: String -> Array PaymentOffer -> String -> String -> Boolean -- not in android
isHavingOffer paymentMethod offers os alpha = do
  let filteredOffers =  if (toLower paymentMethod) == "upi"
                          then (filter(
                                    \ (PaymentOffer offer) ->  ( "upi" ==  (toLower offer.paymentMethodType))
                                  ) offers)
                          else if alpha == ""
                            then filter(\ (PaymentOffer offer) -> (
                                                (\i -> paymentMethod `elem` i) ([offer.paymentMethod])
                              )) offers
                            else filter(\ (PaymentOffer offer) -> (
                                        (\i -> paymentMethod `elem` i) ([offer.paymentMethod]) &&
                                        (isFilterExists alpha (fromMaybe [] offer.paymentMethodFilter))
                                        -- (\i -> (toLower alpha) `elem` i)) (fromMaybe [] offer.paymentMethodFilter)
                              )) offers

  if (length filteredOffers > 0)
    then true
    else false

liftFlow :: ∀ val . (Effect val)  -> Flow val
liftFlow effVal = doAff do liftEffect (effVal)

newtype NativeRequest = NativeRequest
  { method :: String
  , url :: URL
  , payload :: String
  , headers :: NativeHeaders
  }

getOfferDescription :: String -> Array PaymentOffer -> Maybe OfferDescription -- not in android
getOfferDescription paymentMethod offers = do
  let filteredOffers =
                      if (toLower paymentMethod) == "upi"
                          then (filter(
                                    \ (PaymentOffer offer) -> ( "upi" ==  (toLower offer.paymentMethodType))
                                  ) offers)
                          else (filter(
                                    \ (PaymentOffer offer) -> (
                                                          (\i -> paymentMethod `elem` i)
                                                          ([offer.paymentMethod])
                                  )
                                ) offers)
  if(length filteredOffers > 0 )
      then do
        let offer = (filteredOffers !! 0)
        case offer of
          Just ofr -> do
            let PaymentOffer enOfr = ofr
            enOfr.offerDescription
          _ -> Nothing
      else Nothing

mkNativeRequest :: Request -> NativeRequest
mkNativeRequest (Request request@{headers: Headers hs}) = NativeRequest
                                          { method : show request.method
                                            , url: request.url
                                            , payload: request.payload
                                            , headers: mkNativeHeader <$> hs
                                            }

mkNativeHeader :: Header -> NativeHeader
mkNativeHeader (Header field val) = { field: field, value: val}

runUI' :: ∀ a b e. Interact Error a b => a -> App.Flow e b  -- not in android
runUI' a = ExceptT (Right <$> runUI a)

onBackPress :: ∀ a. Flow (FailBack a)  -- not in android
onBackPress = doAff do
  makeAff (\sc -> onBackPress' (Right >>> sc) *> pure nonCanceler ) *> pure GoBack

liftRunScreen :: ∀ a s o. (o -> FailBack o) -> (Screen a s o) -> FlowBT PaymentPageError o  -- not in android
liftRunScreen f a = BackT <<< ExceptT $ Right <$> (oneOf [ f <$> runScreen a, onBackPress ])

getFromWindow :: ∀ a. String -> Flow (Maybe a)  -- not in android
getFromWindow key = do
  val <- doAff (liftEffect $ unsafeGet' key)
  case (isNull val) of
    true -> pure Nothing
    false -> pure $ Just (unsafeFromForeign val)

setOnWindow :: ∀ a t. String -> a -> FlowBT t Unit  -- not in android
setOnWindow key val = liftFlowBT $ doAff (liftEffect $ setOnWindow' key val)

continue :: ∀ a m. Applicative m => a -> m a -- not in android
continue = pure




unsafeJsonDecode :: ∀ a. String -> Maybe a  -- not in android
unsafeJsonDecode val = runFn3 unsafeJsonDecodeImpl val Just Nothing
