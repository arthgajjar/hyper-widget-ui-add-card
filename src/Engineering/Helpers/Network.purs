module Engineering.Helpers.Network where

import Data.Generic.Rep (class Generic)
import Foreign.Class (class Encode)
import Foreign.Generic.Class (class GenericEncode)
import Presto.Core.Types.API (Headers(..), Method, Request(..), URL)
import Presto.Core.Utils.Encoding (defaultEncodeJSON) 
 
urlEncodedMakeRequest :: 
  ∀ req r2
   . Encode req
  => Generic req r2
  => GenericEncode r2
  => Method
  -> URL
  -> Headers
  -> req
  -> Request
urlEncodedMakeRequest method u (Headers headers) req = Request
  {   method
    , url : u
    , headers : Headers headers
    , payload: defaultEncodeJSON req
  }