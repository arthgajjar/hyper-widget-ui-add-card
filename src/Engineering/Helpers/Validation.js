exports.getCurrentYear = function (a) {
  var date = new Date();
  return date.getFullYear();
}

exports.getCurrentMonth = function (b) {
  var date = new Date();
  return (date.getMonth() + 1);
}
