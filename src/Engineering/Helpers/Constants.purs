module Engineering.Helpers.Constants
where

import UI.Constant.Type
import UI.Constant.Str.Default as STR

buttonText6 :: Str
buttonText6 = STR.getProceedToPay ""

netBanking92 :: Str
netBanking92 = "Debit card"

netBanking93 :: Str
netBanking93 = "Net banking"

netBanking94 :: Str
netBanking94 = "UPI"

netBanking95 :: Str
netBanking95 = "UPI Apps"

cardNumberLabel8 :: Str
cardNumberLabel8 = "CARD NUMBER"

cardNumberText9 :: Str
cardNumberText9 = "0000 0000 0000 0000"

expiryLabel10 :: Str
expiryLabel10 = "EXPIRY DATE"

expiryText11 :: Str
expiryText11 = "MM/YY"

cvvLabel12 :: Str
cvvLabel12 = "CVV"

cvvText13 :: Str
cvvText13 = "ooo"

errorText4 :: Str
errorText4 = "Amount not in limit"

last6DigitsOfDeb64 :: Str
last6DigitsOfDeb64 = "last 6 digits of your debit card"

eNTEREXPIRY65 :: Str
eNTEREXPIRY65 = "expiry date"

infoRegularText91 :: Str
infoRegularText91 = "Choose the SIM that has the number linked to your bank accounts"

paymentErrorText :: Str
paymentErrorText = "we will refund in 7 working days if money has been debited"
