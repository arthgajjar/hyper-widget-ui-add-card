module Engineering.Helpers.Store
    ( Status(..)
    , Store(..)
    , Request(..)
    , Key(..)
    , initiate
    , setValue
    , getValue
    , status
    , lazyGet  
    ) where

import Prelude


import Effect.Aff (makeAff, nonCanceler, runAff_)
import Effect (Effect)
import Effect.Exception (Error)
import Effect.Uncurried as EffectFn

import Data.Either (Either(Right, Left))

import Data.Maybe (Maybe(..))

foreign import setToStore :: ∀ a. EffectFn.EffectFn2 String a Unit

foreign import setErrorToStore :: EffectFn.EffectFn2 String Error Unit

foreign import getFromStore :: ∀ a. EffectFn.EffectFn3 String (Maybe a) (a -> Maybe a) (Maybe a)

foreign import registerCallback :: ∀ a. EffectFn.EffectFn2 String  (a -> Effect Unit) Unit

foreign import checkStatus :: EffectFn.EffectFn1 String Int

newtype Key = Key String


data Request
    = Registered
    | Responsed
    | NoReq

data Store
    = Initiated
    | Populated

data Status
    = Status Store Request
    | NoKey



storeCallback :: ∀ a. Key -> Either Error a -> Effect Unit
storeCallback (Key key) =
    case _ of
         Left err -> EffectFn.runEffectFn2 setErrorToStore key err
         Right a  -> EffectFn.runEffectFn2 setToStore key a


initiate :: ∀ a. Key -> ((Either Error a -> Effect Unit) -> Effect Unit) -> Effect Unit
initiate key fn = do
    _ <- runAff_ (storeCallback key) $ makeAff (\cb -> fn cb *> pure nonCanceler)
    pure unit



setValue :: ∀ a. Key -> a -> Effect Unit
setValue (Key key) value = EffectFn.runEffectFn2 setToStore key value

getValue :: ∀ a. Key -> Effect (Maybe a)
getValue (Key key) = EffectFn.runEffectFn3 getFromStore key Nothing Just



status :: Key -> Effect Status
status (Key key) = EffectFn.runEffectFn1 checkStatus key >>= pure <<<
    case _ of
         0 -> NoKey
         1 -> Status Initiated NoReq
         2 -> Status Initiated Registered
         3 -> Status Populated NoReq
         4 -> Status Populated Responsed
         _ -> NoKey

lazyGet :: ∀ a. Key -> (a -> Effect Unit) -> Effect Unit
lazyGet (Key key) cb = EffectFn.runEffectFn2 registerCallback key cb


