module ECFlowRuntime where

import Prelude

import Control.Monad.State as S
import Data.Either (Either(..))
import Data.Function.Uncurried (runFn2)
import Effect.Aff (Error, attempt, makeAff, nonCanceler)
import Effect.Aff.AVar (new) as AVar
import Engineering.Helpers.Commons (callAPI', mkNativeRequest, showUI')
import Engineering.OS.Permission (checkIfPermissionsGranted, requestPermissions)
import Foreign.Object (empty)
import Presto.Core.Flow (Flow, doAff)
import Presto.Core.Language.Runtime.API (APIRunner)
import Presto.Core.Language.Runtime.Interpreter (PermissionCheckRunner, PermissionRunner(..), PermissionTakeRunner, Runtime(..), UIRunner, run)

-- Run EC Flows in a separate Presto Context to avoid it's errors from killing this App Flow
runECFlow :: ∀ a. Flow a -> Flow (Either Error a)
runECFlow appFlow = do
  let runtime  = Runtime uiRunner permissionRunner apiRunner
  let isActivityRecreated = false
  let freeFlow = S.evalStateT (run runtime (appFlow))
  doAff $ attempt (AVar.new empty >>= freeFlow)
  -- pure unit 
  where
 
  uiRunner :: UIRunner
  uiRunner a = makeAff (\cb -> do
      _ <- runFn2 showUI' (cb <<< Right) ""
      pure $ nonCanceler
                          )
  permissionCheckRunner :: PermissionCheckRunner
  permissionCheckRunner = checkIfPermissionsGranted

  permissionTakeRunner :: PermissionTakeRunner
  permissionTakeRunner = requestPermissions

  permissionRunner :: PermissionRunner
  permissionRunner = PermissionRunner permissionCheckRunner permissionTakeRunner

  apiRunner :: APIRunner
  apiRunner request = makeAff (\cb -> do
      _ <- callAPI' (cb <<< Left) (cb <<< Right) (mkNativeRequest request)
      pure $ nonCanceler
    )