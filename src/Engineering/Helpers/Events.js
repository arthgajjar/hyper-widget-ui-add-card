"use strict";

exports.checkFirstSixNotChanged = function(cn) {
  return function() {
    if (window.__cardNumFirstSixDigit) {
      var bool = (window.__cardNumFirstSixDigit == cn);
      window.__cardNumFirstSixDigit = cn;
      return bool;
    } else {
      window.__cardNumFirstSixDigit = cn;
      return false;
    }
  }
}

exports.executeEvent = function(eventType) {
  return function(f) {
    window["afterRender"] = window["afterRender"] || {};
    if(window.__dui_screen) {
      window["afterRender"][window.__dui_screen] = window["afterRender"][window.__dui_screen] || {};
      if (window["afterRender"][window.__dui_screen]["executed"] && window.__dui_last_patch_screen != window.__dui_screen) {
        f()();
      } else {
        window["afterRender"][window.__dui_screen][eventType] = f;
      }
    }
    return f;
  }
}

const callbackMapper = {
  map : function(fn) {
    if(typeof window.__FN_INDEX !== 'undefined' && window.__FN_INDEX !== null) {
      var proxyFnName = 'F' + window.__FN_INDEX;
      window.__PROXY_FN[proxyFnName] = fn;
        window.__FN_INDEX++;
      return proxyFnName;
    } else {
      throw new Error("Please initialise window.__FN_INDEX = 0 in index.js of your project.");
    }
  }
}

exports.timerHandlerImpl = function (time) {
  return function(interval) {
    return function(push) {
      if(typeof window.__cancelTimer === "function"){
        window.__cancelTimer()
      }
      if (window.__OS.toLowerCase() != "ios") {
        var i = time;
        var clearId = setInterval(function () {
          push(i--)();
        }, interval);
        var f1 = function () {
          clearInterval(clearId); // Comment this line
        }
        window.__cancelTimer = f1;
        setTimeout(f1, time * 1000);
      } else {
        var timerCallback = callbackMapper.map(function(value) {
          push(value)();
        });
        var f1 = function () {
          JBridge.stopTimer();
        }
        window.__cancelTimer = f1;
        JBridge.startTimerWithTime(time, 1, timerCallback);
      }
    }
  }
}

exports.cancelTimerHandler = function() {
  if (window.__cancelTimer) {
    window.__cancelTimer();
  }
}


exports.timerHandlerImplWithIntervalImpl = function (time) {
  return function(push) {
    return function() {

      if (window.__OS.toLowerCase() != "ios") {
        var clearId = setInterval(function () {
          push()();
        }, time * 1000);
        setTimeout(function () {
          clearInterval(clearId);
        }, time * 1000);
      } else {
        var timerCallback = callbackMapper.map(function(value) {
          push()();
        });
        JBridge.startTimerWithTime(0, time, timerCallback);
      }
    }
  }
}

exports.setEvent = function(f) {
    return f()();
}

exports.customBackPressHandelerImpl = function (push) {
  return function(){
    if(window.__dui_screen == null){
      console.error("Call Set Screen before handelling back press")
      return;
    }

    if(window.onCustomBackPress == null){
      window.onCustomBackPress = []
    }
    window.onCustomBackPress[window.__dui_screen] = function(){
      push()();
    };
  }
}

exports["onMerchantEvent'"] = function(cb) {
  return function() {
    window.onMerchantEventCallback = function(args) {
      console.log("getting onMerchantEvent" , args);
      window.onMerchantEventCallback = null;
      cb(args)();
    }
  }
};


exports["_emitEvent"] = function (payload) {
  return function () {
    console.log("Triggering Event: "+ payload);
    JBridge.runInJuspayBrowser("onEvent", payload, "");
  }
}
