module Tracker.Tracker where

import Prelude

import Data.Array (length, slice, take)
import Effect (Effect)
import Effect.Timer (IntervalId)
import Engineering.Helpers.Commons (liftFlow)
import Presto.Core.Flow(Flow)

foreign import preProcessEvent :: String -> String -> String -> String -> Effect String
foreign import preProcessTrackExceptionEvent :: String -> String -> String -> Effect String
foreign import preProcessTrackInfo :: String -> Effect  String
foreign import preProcessSessionInfo :: ∀ a  . {|a} -> Effect String
foreign import preProcessPaymentDetails :: Effect  String
foreign import addToLogList' :: Boolean -> String -> Effect Unit
foreign import canPostLogs :: Effect  Boolean
foreign import updateLogList :: Array String -> Effect Unit
foreign import getLogList :: Effect (Array String)
foreign import postLogsToAnalytics :: Array String -> Effect Unit
foreign import preProcessTrackPage :: String -> String -> Effect String
foreign import preProcessHyperPayEvent :: ∀ a  . String -> a -> Effect String
foreign import submitAllLogs :: Effect Unit
foreign import trackAPICalls' :: String -> String -> String -> Int -> Effect Unit
foreign import setInterval :: 
  Int
  -> Effect Unit
  -> Effect IntervalId
foreign import toString :: ∀ a. a-> String

addToLogList :: String -> Flow Unit
addToLogList =  liftFlow <<< addToLogList' false

trackEvent' :: String -> String -> String -> String -> Boolean -> Effect Unit
trackEvent' category action label value shouldShareWithMerchant = addToLogList' shouldShareWithMerchant =<< preProcessEvent category action label value

theOthertrackEventT :: Boolean -> String -> String -> String -> String -> Effect Unit
theOthertrackEventT shouldShareWithMerchant category action label value = (trackEvent' category action label value shouldShareWithMerchant)

trackEventMerchantV2 :: String -> String -> Effect Unit
trackEventMerchantV2 = theOthertrackEventT true "UserFlow" "Event"

trackEventV2 :: String -> String -> Effect Unit
trackEventV2 = theOthertrackEventT false "UserFlow" "Event"

trackEventT ::Boolean -> String -> String -> String -> String -> Flow Unit
trackEventT shouldShareWithMerchant category action label value = liftFlow (trackEvent' category action label value shouldShareWithMerchant)

trackEventMerchant :: String -> String -> String -> String -> Flow Unit
trackEventMerchant = trackEventT true

trackEvent :: String -> String -> String -> String -> Flow Unit
trackEvent = trackEventT false

trackInfo :: String -> Flow Unit
trackInfo infoVal = addToLogList =<< (liftFlow (preProcessTrackInfo infoVal))

trackHyperPayEvent' :: ∀ a  .String -> a -> Effect Unit
trackHyperPayEvent' key value = addToLogList' false =<< preProcessHyperPayEvent key value

trackHyperPayEvent :: ∀ a .String -> a -> Flow Unit
trackHyperPayEvent key value = liftFlow (trackHyperPayEvent' key value)

trackException ::  String -> String -> String -> Flow Unit
trackException desc msg trace = addToLogList =<< (liftFlow (preProcessTrackExceptionEvent desc msg trace))

trackPage' :: String -> String -> Effect Unit
trackPage' startTime fragmentName = addToLogList' false =<< preProcessTrackPage "" fragmentName

trackPage ::  String -> String -> Flow Unit
trackPage startTime fragmentName = liftFlow (trackPage' startTime fragmentName)

trackSession :: ∀ a . {|a} -> Flow Unit
trackSession session = addToLogList =<< (liftFlow (preProcessSessionInfo session))

trackPaymentDetails :: Flow Unit
trackPaymentDetails = addToLogList =<< (liftFlow preProcessPaymentDetails)

trackAPICalls :: String -> String -> String -> Int -> Flow Unit
trackAPICalls url startTime endTime statusCode = liftFlow (trackAPICalls' url startTime endTime statusCode)

initTracking :: Int -> Effect Unit
initTracking batch = do
  t <- setInterval batch do
    ifM (canPostLogs) postLogs (pure unit)
  pure unit

postLogs :: Effect Unit
postLogs = ((getLogList >>= chopChop) >>= postLogsToAnalytics)

chopChop :: Array String -> Effect (Array String)
chopChop list
  | length list > 75 = let extraLogs = slice 75 (length list) list in sliceNdice list extraLogs
  | otherwise        = sliceNdice list []

sliceNdice :: Array String -> Array String -> Effect (Array String)
sliceNdice list extra = do
  updateLogList extra
  if length list >= 75 
    then 
      pure $ take 75 list
    else
      pure $ list