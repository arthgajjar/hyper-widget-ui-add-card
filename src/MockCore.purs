module MockCore where

import Prelude
import Control.Monad.Except (runExcept)
import Control.Monad.Free (wrap)
import Data.Array (elem, filter, length, (!!))
import Data.Either (Either(..))
import Data.Maybe (Maybe(..), fromMaybe, isJust, maybe)
import Data.String (Pattern(..), Replacement(..), toLower)
import Data.String as S
import Effect.Aff (Milliseconds(..), delay)
import Effect.Class (liftEffect)
import Engineering.Helpers.Commons (filterGatewayRef, getOS, getPaymentApps, getSMSFromInbox, getSessionInfo, isPopUp, liftFlow, substr, subtractTime)
import JBridge as JBridge
import Payments.Core.Commons as Commons
import Payments.Wallets.Utils as WalletUtils
import Payments.WebRedirect.Flow (redir)
import Tracker.Tracker (initTracking, trackSession, trackPaymentDetails)
import Presto.Core.Flow (Flow, initUI, oneOf, runScreen, showScreen, fork)
import Presto.Core.Types.API (Header(..), Headers(..))
import Presto.Core.Types.Language.Flow (APIResult, callAPI, doAff, loadS)
import Presto.Core.Utils.Encoding (defaultDecodeJSON)
import Remote.Types (defaultMerchantOffer, getExitResponse)
import Tracker.Tracker as Tracker
import Flow.Types (FlowResponse(..), ReturnResponse(..))
import UrlUtils as UrlUtils
import Utils (defaultMerchantOff, default_payment_option, default_payment_option_type, exitSDKPrevPage, exitSDKWithStatus, exit_code_failure, exit_code_success, exit_status_back, exit_status_unapproved, loadFromLocal, msg_unapproved, pig_card, pig_card_od, pig_cash_od, pig_nb, pig_pay_later, pig_upi, pig_wallet)
import Utils (getCurrentTimeStamp)
import UI.Utils (resetListKeyBoard,getLoaderConfig)
import View.Loader.Screens.Screen as LoaderScreen
import RunScreenFunctions.WalletVerifyNumberScreen as WalletVerifyNumberScreen
import RunScreenFunctions.WalletScreen as WalletScreen
import RunScreenFunctions.WalletOtpScreen as WalletOtpScreen
import RunScreenFunctions.WalletDelinkScreen as WalletDelinkScreen
import RunScreenFunctions.UPIHomeScreen as UPIHomeScreen
import RunScreenFunctions.UPIAddScreen as UPIAddScreen
import RunScreenFunctions.QuickPayScreen as QuickPayScreen
import RunScreenFunctions.ProcessingScreen as ProcessingScreen
import RunScreenFunctions.PlansScreen as PlansScreen
import RunScreenFunctions.PaymentPageScreen as PaymentPageScreen
import RunScreenFunctions.PaymentManagementScreen as PaymentManagementScreen
import RunScreenFunctions.OffersPreviewScreen as OffersPreviewScreen
import RunScreenFunctions.NetBankingScreen as NetBankingScreen
import RunScreenFunctions.MoreOptionsScreen as MoreOptionsScreen
import RunScreenFunctions.LoaderScreen as LoaderScreen
import RunScreenFunctions.InstrumentsScreen as InstrumentsScreen
import RunScreenFunctions.CheckoutScreen as CheckoutScreen
import RunScreenFunctions.AddCardScreen as AddCardScreen

startTrackerEngine :: Flow Unit
startTrackerEngine = do
  liftFlow (initTracking 10000)
  sessionInfo <- liftFlow getSessionInfo
  _ <- trackPaymentDetails
  _ <- trackSession sessionInfo
  pure unit


runScreenName :: Flow FlowResponse
runScreenName = UPIHomeScreen.run

-- To run any function type run_ + ParentFolder + _ +ScreenName. Eg. Addcard > Screens > Screen would be run_AddCard_Screen

appFlow :: Boolean -> Flow Unit
appFlow recreated = do
  _ <- liftFlow resetListKeyBoard
  _ <- startTrackerEngine
  _ <- initUI
  _ <- oneOf [showScreen (LoaderScreen.screen getLoaderConfig), doAff do delay (Milliseconds 200.0)]
  checkout <- doAff do liftEffect Commons.getCheckoutDetails
  Tracker.trackEvent "hyperpay" "info" "process_started" $ (Tracker.toString checkout)
  --flowResponse <- runAppCustom checkout.widget_name 0
  --flowResponse <- runAppDummy checkout
  flowResponse <- runScreenName --runfunction
  case flowResponse of
    Return (Just (ReturnResponse retData)) -> do
      let exit_status = case retData.status of
                          "FORCE_EXIT"    -> exit_status_unapproved
                          "BACKPRESS"     -> exit_status_back
                          "GUESTLOGIN"    -> Just "guest_login"
                          a               -> Just $ toLower a
      let msg = if retData.status == "FORCE_EXIT"
                  then msg_unapproved
                  else Nothing
      let payment_inst = if retData.code /= ""
                          then (Just retData.code)
                          else Nothing
      let errorCode = if retData.hasApiFailed then "JP001" else ""
      let exitPayload = getExitResponse checkout.order_id retData.hasApiFailed checkout.action exit_status msg payment_inst retData.pig errorCode
      let exit_code = if retData.status == "CHARGED" || retData.status == "COD_INITIATED"
                        then exit_code_success
                        else if retData.status == "GUESTLOGIN"
                          then do
                            let x = redir checkout.guest_login_url
                            exit_code_success
                          else exit_code_failure
      Tracker.trackEvent "hyperpay" "info" "process_finished" $ (Tracker.toString exitPayload)
      exitSDKWithStatus exitPayload exit_code
    _ -> do
      let exitPayload = getExitResponse checkout.order_id true checkout.action Nothing Nothing Nothing Nothing "JP001"
      Tracker.trackEvent "hyperpay" "error" "process_finished" $ (Tracker.toString exitPayload)
      exitSDKWithStatus exitPayload exit_code_failure
