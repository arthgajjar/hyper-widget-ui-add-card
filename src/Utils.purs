module Utils where

import Prelude
import HyperPrelude.External(filter, (!!), length,Maybe(..), fromMaybe,Effect)

import Data.Generic.Rep (class Generic)
import Data.Number (fromString)
import Foreign.Generic.Class (class GenericEncode)
import Presto.Core.Flow (Flow)
import Presto.Core.Utils.Encoding (defaultEncodeJSON)
import Remote.Types (ConfigPayload(..), MerchantOffer(..))
foreign import consoleLog :: ∀ a. a -> a
foreign import getLabelHeight :: String -> String -> Int -> Int -> Int -> Int
foreign import getLabelWidth :: String -> String -> Int -> Int -> Int -> Int
foreign import screenWidth :: Unit -> Int
foreign import getOS :: Unit -> String
foreign import exitSDK' :: ∀ a. a -> Int -> Unit
foreign import log :: ∀ a. String -> a -> a
foreign import dropLastChar :: String -> String
foreign import attachMerchantView :: String -> Effect Unit
foreign import integrationType :: Unit -> String
foreign import exitSDKPrevPage' :: ∀ a. a -> Int -> Unit
foreign import buildCSS :: ConfigPayload -> String
foreign import getVoucher :: Unit -> Array String
foreign import saveToLocal :: String -> String -> String -> Unit
foreign import loadFromLocal :: String -> String
foreign import defaultOpExist :: String -> Boolean
foreign import getCurrentTimeStamp  :: forall e. (Effect String)
foreign import logW :: ∀ a. String -> a -> a
foreign import arrayJoin :: Array String -> String -> String
foreign import _hide_loader :: Effect Unit
foreign import logE :: ∀ a. String -> a -> a

exitSDK :: ∀ a. a -> Int -> Flow Unit
exitSDK response code = do
  pure $ exitSDK' response code

exitSDKPrevPage :: ∀ a. a -> Int -> Flow Unit
exitSDKPrevPage response code = do
  pure $ exitSDKPrevPage' response code

exitSDKWithStatus :: ∀ a b. Generic a b => GenericEncode b => a -> Int -> Flow Unit
exitSDKWithStatus response code = do
  let status = defaultEncodeJSON response
  pure $ exitSDK' status code

-------------------- New Returns --------------------

defaultMerchantOff ::String -> MerchantOffer
defaultMerchantOff tab =
    MerchantOffer
    { applied : apply
    , payment_method_type : Just pay_method_type
    , payment_method : Just pay_method
    , payment_card_type : Just card_type
    , payment_card_issuer : Just card_issue
    , offer_code : Just code
    , amount : Just amt
    }
    where
      arr = getVoucher unit
      apply = fromMaybe "" (arr !! 0)
      pay_method= fromMaybe "" (arr !! 1)
      pay_method_type = fromMaybe "" (arr !! 2)
      code = fromMaybe "" (arr !! 3)
      amt =fromMaybe "0.0" (arr !! 4)
      card_type = fromMaybe "" (arr !! 5)
      card_issue = fromMaybe "" (arr !! 6)



exit_code_success :: Int
exit_code_success = (-1)

exit_code_failure :: Int
exit_code_failure = 0

exit_status_success :: Maybe String
exit_status_success = Just "success"

exit_status_failure :: Maybe String
exit_status_failure = Just "failed"

exit_status_cancel :: Maybe String
exit_status_cancel = Just "cancelled"

exit_status_back :: Maybe String
exit_status_back = Just "backpressed"

exit_status_unapproved :: Maybe String
exit_status_unapproved = Just "not approved"

pig_cash_od :: Maybe String
pig_cash_od = Just "CASH_ON_DELIVERY"

pig_card_od :: Maybe String
pig_card_od = Just "CARD_ON_DELIVERY"

pig_upi :: Maybe String
pig_upi = Just "UPI"

pig_card :: Maybe String
pig_card = Just "CARD"

pig_nb :: Maybe String
pig_nb = Just "NB"

pig_wallet :: Maybe String
pig_wallet = Just "WALLET"

pig_pay_later :: Maybe String
pig_pay_later = Just "PAY_LATER"

msg_unapproved :: Maybe String
msg_unapproved = Just "payment not approved by bb"

-------------------- Caching --------------------

default_payment_option :: String
default_payment_option = "defOption"

default_payment_option_type :: String
default_payment_option_type = "defOptionType"

-------------------- Old Returns --------------------
key_pass :: String
key_pass = "ALL_PRESENT"

key_fail :: String -> String
key_fail input = "NOT_PASSED_" <> input

msg_user_abort :: String
msg_user_abort = "user aborted"

msg_cod_selected :: String
msg_cod_selected = "cash on delivery "

msg_cardod_selected :: String
msg_cardod_selected = "card on delivery selected"

msg_no_internet :: String
msg_no_internet = "There is no Internet connection"

msg_txn_failure :: String
msg_txn_failure = "Any amount deducted will be refunded within 3 days.\nHow would you like to proceed?"

msg_wallet_link_failure :: String
msg_wallet_link_failure = "Link Wallet Failed"

msg_token_expired :: String
msg_token_expired = "Token Expired"

exit_code_processed :: Int
exit_code_processed = (-1)
