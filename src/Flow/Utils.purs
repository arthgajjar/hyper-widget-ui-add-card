module Flow.Utils where

import Prelude

import Data.Array (elem, filter)
import Data.Either (Either(..))
import Data.Maybe (Maybe(..), fromMaybe)
import Data.String (toUpper)
import Service.EC.Types.Response (ECResponse(..), ErrorResult(..), SDKResponse(..), OrderStatusResp(..), PaymentSourceResp(..), OutagesStatus(..))
import Effect.Aff (makeAff, nonCanceler)
import Effect.Class (liftEffect)
import Flow.Types (FlowResponse(..), RefreshState(..), ReturnResponse(..))
import JBridge as JBridge
import Engineering.Helpers.Commons (PaymentOffer(..))
import Payments.Core.Commons (Checkout)
import Payments.Core.Commons as Commons
import Payments.Wallets (getEligibilityDetails)
import Payments.Wallets.Utils (PaymentMethodsEligibility, dummyPaymentMethodsEligibility)
import Presto.Core.Types.Language.Flow (Flow, doAff, saveS, delete)
import Remote.Types (MerchantOffer)
import Tracker.Tracker as Tracker
import UI.Config (isGuestUser)
import Utils (default_payment_option, default_payment_option_type, saveToLocal)
-- handles back cases for FlowResponse



handleBack :: Flow FlowResponse -> FlowResponse -> Flow FlowResponse
handleBack flow Back = flow
handleBack _ a = pure a

handleBackAndLoop :: (PaymentSourceResp -> Flow FlowResponse) -> PaymentSourceResp -> FlowResponse -> Flow FlowResponse
handleBackAndLoop flow psr Back = flow psr
handleBackAndLoop flow psr (RefreshState RefreshPSR) = do
    updatedPsr <- Commons.getPaymentSource (Just "true") (Just "true")
    case updatedPsr of
        Right updatedPsr' -> flow updatedPsr'
        _ -> flow psr

handleBackAndLoop _ _ a = pure a

--- Handling different cases of response of a transaction
handleTxnRespTemp :: Unit -> Flow FlowResponse
handleTxnRespTemp unit =
    pure $ Return $ Just $ ReturnResponse { status : "CHARGED", code: "test", pig : Nothing, hasApiFailed : false }

handleTxnResp' :: Either ErrorResult OrderStatusResp -> String -> Flow FlowResponse
handleTxnResp' ecResp code = do
    Tracker.trackEvent "hyper_sdk" "api_call" "txns" (Tracker.toString ecResp)
    pure $ case ecResp of
        Right (OrderStatusResp os) -> Return $ Just $ ReturnResponse { status : fromMaybe "" os.status, code, pig : Nothing, hasApiFailed : false }
        Left (ErrorResult err) -> Return $ Just $ ReturnResponse { status : err.status, code, pig : Nothing, hasApiFailed : true }

handleTxnResp :: Maybe SDKResponse -> String -> Flow FlowResponse
handleTxnResp sdkResp code = do
  pure $ case sdkResp of
    Just (SDKResponse sdkResponse) -> do
      case sdkResponse.payload of
        OrderStatus (OrderStatusResp os) -> do
          Return $ Just $
            ReturnResponse {
              status : fromMaybe "" os.status
            , code
            , pig : Nothing
            , hasApiFailed : sdkResponse.error
            }
        _ -> Back
    _ -> Back


---- approving payment from merchant
approvePayment :: String -> String -> String -> String -> Flow Boolean
approvePayment pmt pm pct pci = do
    res <- doAff $ makeAff (\cb -> do
        _ <- JBridge.callMerchantAndWait (Right >>> cb) "paymentAttempt"
            { payment_method_type : pmt
            , payment_method : pm
            , payment_card_type : pct
            , payment_card_issuer : pci
            }
        pure nonCanceler)
    Tracker.trackEvent "hyper_sdk" "api_call" "event_to_merchant_req" (Tracker.toString ({ payment_method_type : pmt
            , payment_method : pm
            , payment_card_type : pct
            , payment_card_issuer : pci
            }))
    Tracker.trackEvent "hyper_sdk" "api_call" "event_to_merchant_response" (Tracker.toString res)
    pure $ JBridge.getApprovalResult res

setAsDefaultOption :: Boolean -> String -> String ->String-> Flow Unit
setAsDefaultOption false _ _ _= pure unit
setAsDefaultOption true payment_option_type payment_option merchant= do
    pure $ saveToLocal  payment_option_type payment_option merchant

setAndGetDefaultOption :: Boolean -> String -> String -> String-> Flow String
setAndGetDefaultOption false _ payment_option _ = pure payment_option
setAndGetDefaultOption true payment_option_type payment_option merchant = do
    let _ = saveToLocal payment_option_type payment_option merchant
    pure payment_option

filterOffer :: String -> Array PaymentOffer -> Array PaymentOffer
filterOffer typeOffer offers = filter (\(PaymentOffer offer) -> toUpper (offer.paymentMethodType) == typeOffer) offers


deleteDefaultOption :: Boolean -> String -> String -> Flow Unit
deleteDefaultOption false _ _ = pure unit
deleteDefaultOption true payment_option_type payment_option = do
    delete payment_option_type
    delete payment_option

patchOrderStatusResp :: Either String String -> Either ErrorResult OrderStatusResp
patchOrderStatusResp status =
    case status of
        Right str -> Right $ OrderStatusResp { order_id : Just "", status : Just str, other_info : Nothing, browserResponse : Nothing, upiResponse : Nothing}
        Left str  -> Left $ ErrorResult {status : str, errorMessage : "", errorCode : ""}

shouldPlaceOrder :: String -> Boolean
shouldPlaceOrder gatewayRefId = elem gatewayRefId orderReferenceArray

orderReferenceArray :: Array String
orderReferenceArray = ["bigbasket_b2c_checkout", "bigbasket_b2b_checkout"]

getOrderAmount :: Flow Number
getOrderAmount = do
    checkout <- doAff do liftEffect Commons.getCheckoutDetails
    pure checkout.amount

findInstrumentGroup :: String -> Array OutagesStatus -> Array String
findInstrumentGroup pig outages =
  map (\(OutagesStatus out) -> out.bank) $ filter (\(OutagesStatus out) -> out.paymentInstrumentGroup == pig) outages


makeFlowResponseJson :: MerchantOffer -> Maybe String -> Flow FlowResponse -> Flow {flowResponse :: FlowResponse, selected_pig :: Maybe String, merchant_offer :: MerchantOffer}
makeFlowResponseJson merchant_offer selected_pig flow = flow <#> \flowResponse -> {flowResponse, selected_pig, merchant_offer}

getPayLaterEligibility :: Boolean -> Flow (Array PaymentMethodsEligibility)
getPayLaterEligibility doesSimpleExist= do
    checkout <- doAff do liftEffect Commons.getCheckoutDetails
    if (doesSimpleExist && not isGuestUser)
        then
            getEligibilityDetails checkout
        else pure [dummyPaymentMethodsEligibility "SIMPL"]

getCustomerName :: Checkout -> String
getCustomerName checkout =
  checkout.customer_first_name <> " " <> checkout.customer_last_name
