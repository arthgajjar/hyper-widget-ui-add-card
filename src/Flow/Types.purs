module Flow.Types where

import Data.Maybe (Maybe)
import Prelude (Unit, bind, const, pure, ($), (/=), (&&), not)
import Service.EC.Types.Response (OutagesStatus(..), PaymentSourceResp(..))
import Payments.Core.Commons as Commons
import Engineering.Helpers.Commons (PaymentOffer)
import Payments.Wallets.Utils (PaymentMethodsEligibility(..))
import Remote.Types (ConfigPayload, MerchantOffer)
import Payments.NetBanking.Utils (Bank(..))
import Presto.Core.Types.Language.Flow (Control)
import Service.EC.Types.Response (PaymentSourceResp(..))
import Service.EC.Types.Instruments (StoredCard(..), StoredWallet(..))

data FlowResponse
    = RefreshState RefreshState
    | Back
    | Return (Maybe ReturnResponse)
    | SwitchTab String
    | SwitchNavBar String


newtype ReturnResponse
    = ReturnResponse
        { status :: String
        , code :: String
        , pig :: Maybe String
        , hasApiFailed :: Boolean
        }

data RefreshState
    = NONE
    | RefreshPSR
    | UpdatePSR PaymentSourceResp

type PaymentPageFlowInput =
    { paymentSourceResp :: PaymentSourceResp
    , upiApps :: Array Commons.UPIEnabledApps
    , configPayload:: ConfigPayload
    , outages:: Array OutagesStatus
    , merchantOffer:: MerchantOffer
    , selectedPig:: Maybe String
    , defType :: String
    , walletFiber :: Control (Array PaymentMethodsEligibility)
    , defOpt :: String
    , showQuickPay :: Boolean
    , checkout :: Commons.Checkout
    , isPayLater :: Boolean
    , isOfferSection :: Boolean
    , payLaterEligibility :: Array PaymentMethodsEligibility
    , sessionFiber :: Control Unit
    , isFirstScreen :: Boolean
    }




type VerifyWalletOTPFlowInput =
    { walletName :: String
    , mobileNumber :: Maybe String
    , amount :: Number
    , configPayload :: ConfigPayload
    , orderDesc :: String
    , customerName :: String
    , allBanks :: Array Bank
    , allCards :: Array StoredCard
    , allWallets :: Array StoredWallet
    , isOffer :: Boolean
    }

type AddAndLinkWalletsFlowInput =
    { configPayload :: ConfigPayload
    , outages :: Array String
    , offers :: Array PaymentOffer
    , defWallet ::String
    , usePayLater :: Boolean
    , payLaterEligibility :: Array PaymentMethodsEligibility
    , justLinked :: Maybe String
    , sessionFiber :: Control Unit
    , allBanks :: Array Bank
    , allCards :: Array StoredCard
    , allWallets :: Array StoredWallet
    }
type HandleDelinkWalletInput =
    { walletName :: String
    , walletId :: String
    , configPayload :: ConfigPayload
    , defWallet :: String
    }

type DelinkWalletFlowInput =
    { configPayload :: ConfigPayload
    , outages :: Array String
    , defWallet :: String
    , gatewayRefId :: Array String
    }
