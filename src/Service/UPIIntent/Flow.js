

exports["startUPIIntent'"] = function (payload) {
	return function (sc) {
		return function () {
			var cb = function (code) {
				return function (response) {
					return function () {
						console.log("UPI Intent Response ", response);
						response = castToSKDResponse(response);
						sc(response)();
					}
				}
			}
			if (JOS) {
				payload = JSON.parse(payload);
                payload.pay_with_app = null;

				var outerPayload = {
					payload : payload,
					requestId : window.uuidFN(),
					service : "in.juspay.upiintent"
				};

				console.log("UPI Intent Request ", payload);

				if (JOS.isMAppPresent("in.juspay.upiintent")()){
					JOS.emitEvent("in.juspay.upiintent")("onMerchantEvent")(["process",JSON.stringify(outerPayload)])(cb)();
				} else {
					JOS.startApp("in.juspay.ec.upi")(payload)(cb)();
				}
			}
		}
	}
}

// this function is for supporting backward compatibility with upi-intent service
function castToSKDResponse(resp){
    try {
        var response = JSON.parse(resp);
        if(response["requestId"] == undefined){
            var updatedResponse =
                { service : "in.juspay.upiintent"
                , requestId : "1234567"
                , payload : response
                , errorMessage : ""
                , errorCode : ""
                , error : false
                }
            return JSON.stringify(updatedResponse);
        }
    } catch(e){
        console.log("upi-intent-resp error: ", e);
    }
	return resp;
}