module Service.UPIIntent.Types where

import Prelude
import Data.Generic.Rep (class Generic)
import Data.Maybe (Maybe)
import Data.Newtype (class Newtype)
import Foreign.Class (class Decode, class Encode)
import Presto.Core.Utils.Encoding (defaultDecode, defaultEncode)

newtype UPIIntentPayload =
	UPIIntentPayload
		{ merchant_id :: String
		, client_id :: String
		, display_note :: String
		, order_id :: String
		, currency :: String
		, environment :: String
		, "WHITE_LIST" :: String
		, "UPI_PAYMENT_METHOD" :: String
		, get_available_apps :: String
		, pay_with_app :: Maybe String
		}

newtype UPIIntentResponse =
	UPIIntentResponse
		{ requestId :: String
		, service :: String
		, payload :: { status :: String , response :: AvailableApps }
		, errorMessage :: String
		, errorCode :: String
		, error :: Boolean
		}

newtype AvailableApps =
	AvailableApps
		{ available_apps :: Array UPIApp }

newtype UPIApp =
	UPIApp
		{ packageName :: String
		, appName :: String
		}

-- Instances

-- UPI Intent Payload
derive instance genericUPIIntentPayload :: Generic UPIIntentPayload _
derive instance newtypeUPIIntentPayload :: Newtype UPIIntentPayload _
instance encodeUPIIntentPayload :: Encode UPIIntentPayload where encode = defaultEncode

derive instance genericUPIIntentResponse :: Generic UPIIntentResponse _
derive instance newtypeUPIIntentResponse :: Newtype UPIIntentResponse _
instance decodeUPIIntentResponse :: Decode UPIIntentResponse where decode = defaultDecode

derive instance genericAvailableApps :: Generic AvailableApps _
derive instance newtypeAvailableApps :: Newtype AvailableApps _
instance decodeAvailableApps :: Decode AvailableApps where decode = defaultDecode
instance encodeAvailableApps :: Encode AvailableApps where encode = defaultEncode

derive instance genericUPIApp :: Generic UPIApp _
derive instance newtypeUPIApp :: Newtype UPIApp _
instance decodeUPIApp :: Decode UPIApp where decode = defaultDecode
instance encodeUPIApp :: Encode UPIApp where encode = defaultEncode

instance eqUPIApp :: Eq UPIApp where
	eq app1 app2 = compareUPIApp app1 app2

compareUPIApp :: UPIApp -> UPIApp -> Boolean
compareUPIApp (UPIApp app1) (UPIApp app2) =
	app1.packageName  == app2.packageName &&
	app1.appName == app2.appName