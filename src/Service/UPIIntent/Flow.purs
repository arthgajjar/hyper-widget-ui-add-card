module Service.UPIINtent.Flow where

import Prelude
import Service.UPIIntent.Types

import Data.Maybe (Maybe)
import Effect (Effect)
import Engineering.Helpers.Commons (AffSuccess)
import Presto.Core.Flow (Flow, doAff)
import Control.Monad.Except (runExcept)
import Data.Either (Either(..), hush)
import Effect.Aff (makeAff, nonCanceler)
import Foreign.Generic (decodeJSON, encodeJSON)

type MicroAPPInvokeSignature = String -> (AffSuccess String) -> Effect Unit

foreign import startUPIIntent' :: MicroAPPInvokeSignature

startUPIIntentFlow :: UPIIntentPayload -> Flow (Maybe UPIIntentResponse)
startUPIIntentFlow payload = do
	response <- doAff $ makeAff (\cb -> (startUPIIntent' (encodeJSON payload) (Right >>> cb) ) *> pure nonCanceler)
	pure $ hush $ runExcept $ decodeJSON response

