module Service.EC.Types.Request where

import Control.Alt ((<|>))
import Data.Generic.Rep (class Generic)
import Data.Maybe (Maybe)
import Data.Newtype (class Newtype)
import Foreign.Class (class Decode, class Encode)
import Prelude (class Eq, class Show, bind, pure, show, ($), (<$>), (<>), (==), (>>=), (||))
import Foreign (F, Foreign, ForeignError(..), fail, isNull, isUndefined, readString, unsafeToForeign)
import Presto.Core.Utils.Encoding (defaultDecode, defaultEncode)
import Foreign.Class (class Decode, class Encode, decode, encode)
import Foreign (F, Foreign, ForeignError(..), fail, isNull, isUndefined, readString, unsafeToForeign)
import Foreign.Index (readProp)

newtype SDKPayload
  = SDKPayload
  { requestId :: String
  , payload :: ECPayload
  , service :: String
  , merchant_id :: String
  , client_id :: String
  , customer_id :: String
  , environment :: String
  }

data ECPayload
  = NewCard NewCardPayload
  | SavedCard SavedCardPayload
  | NB NBPayload
  | UPI UPIPayload
  | WalletRedirect WalletTxnPayload
  | WalletSDKDebit WalletTxnPayload
  | WalletDirectDebit WalletTxnPayload
  | ListWallets ListWalletsPayload
  | LinkWallet LinkWalletPayload
  | CreateWallet CreateWalletPayload
  | AuthWallet AuthWalletPayload
  | GetWallet WalletPayload
  | DeLinkWallet WalletPayload
  | RefreshWallet WalletPayload
  | ListCards ListCardsPayload
  | TokenizeCard NewCardPayload
  | ProcessOTP ProcessOTPPayload
  | CardInfo CardInfoPayload
  | RefreshWalletBalances RefreshWalletBalancesPayload
  | DeleteCard DeleteCardPayload
  | PaymentMethod PaymentMethodsPayload
  | PaymentSource PaymentSourcePayload
  | DeviceReady DeviceReadyPayload
  | CashOnD CashOnDPayload
  | CardOnD CardOnDPayload
  | OutageAPI OutageAPIPayload
  | SumbitOTP SubmitOTPPayload
  | ConsumerFinance ConsumerFinancePayload
  | Initiate InitiatePayload
  | Eligibility EligibilityPayload
  | LegacyWalletEligibility LegacyWalletEligibilityPayload
  | EmiPlans EmiPayload
  | StartJuspaySafe JuspaySafePayload
  | DirectDotp DotpPayload
  | ListNB ListNBPayload
  | ValidateNB NBValidationPayload
  | ValidationStatusNB NBValidationStatusPayload

-- Net Banking Payload
newtype NBPayload
  = NBPayload
  { action :: String
  , orderId :: String
  , endUrls :: Array String
  , paymentMethod :: String
  , offerToken :: Maybe String
  , amount :: Number
  , orderDetails :: Maybe String
  , merchantKeyId :: Maybe String
  , signature :: Maybe String
  , shouldCreateMandate :: Maybe Boolean
  , bankIfsc :: Maybe String
  , bankAccountNumber :: Maybe String
  , bankBeneficiaryName :: Maybe String
  , mandateType :: Maybe String
  , bankId :: Maybe String
  }

-- Wallet Payload
newtype WalletTxnPayload =
  WalletTxnPayload
    { action :: String
    , orderId :: String
    , clientAuthToken :: String
    , endUrls :: Maybe (Array String)
    , paymentMethod :: String
    , shouldLink :: Maybe Boolean
    , directWalletToken :: Maybe String
    , sdkPresent :: Maybe String
    , walletMobileNumber :: Maybe String
    , walletEmail :: Maybe String
    , offerToken :: Maybe String
    , riskId :: Maybe String
    , orderDetails :: Maybe String
    , signature :: Maybe String
    , merchantKeyId :: Maybe String
    , mandateType :: Maybe String
    , shouldCreateMandate :: Maybe Boolean
    , useFallback :: Maybe Boolean
    , editFi :: Maybe Boolean
    }

-- Create Wallet Payload
newtype CreateWalletPayload =
  CreateWalletPayload
    { action :: String
    , clientAuthToken :: String
    , walletName :: String
    , sdkWalletIdentifier :: Maybe String
    , mobileNumber :: Maybe String
    , gatewayReferenceId :: Maybe String
    }

newtype NBValidationStatusPayload = NBValidationStatusPayload {
    "action" :: String
  , "clientAuthToken" :: String
  , "bankId":: String
  , "showLoader" :: Maybe Boolean
}

derive instance newtypeNBValidationStatusPayload :: Newtype NBValidationStatusPayload _
derive instance genericNBValidationStatusPayload :: Generic NBValidationStatusPayload _
instance encodeNBValidationStatusPayload :: Encode NBValidationStatusPayload where
  encode = defaultEncode
instance decodeNBValidationStatusPayload :: Decode NBValidationStatusPayload where
  decode = defaultDecode


newtype NBValidationPayload = NBValidationPayload
  { "action" :: String
  , "clientAuthToken" :: String
  , "paymentMethod" :: String
  , "gatewayId" :: Int
  , "bankIfsc" :: String
  , "bankAccountNumber" :: String
  , "bankBeneficiaryName" :: String
  , "gatewayReferenceId" :: String
  , "usePolling" :: Maybe Boolean
  }

derive instance newtypeNBValidationPayload :: Newtype NBValidationPayload _
derive instance genericNBValidationPayload :: Generic NBValidationPayload _
instance encodeNBValidationPayload :: Encode NBValidationPayload where
  encode = defaultEncode
instance decodeNBValidationPayload :: Decode NBValidationPayload where
  decode = defaultDecode

newtype ListNBPayload
  = ListNBPayload { "action" :: String, "clientAuthToken" :: String }

derive instance newtypeListNBPayload :: Newtype ListNBPayload _

derive instance genericListNBPayload :: Generic ListNBPayload _

instance encodeListNBPayload :: Encode ListNBPayload where
  encode = defaultEncode

instance decodeListNBPayload :: Decode ListNBPayload where
  decode = defaultDecode

newtype DotpPayload = DotpPayload {
    action :: String,
    amount :: String,
    merchantId :: String,
    clientId :: String,
    txnResponse :: TxnResponse,
    cardNumber :: String,
    clientAuthToken :: String,
    endUrls :: Array String,
    txnInitTime :: Maybe String
}

derive instance newDotpPayload :: Newtype DotpPayload _
derive instance genericDotpPayload :: Generic DotpPayload _
instance encodeDotpPayload  :: Encode DotpPayload where encode = defaultEncode
instance decodeDotpPayload  :: Decode DotpPayload where decode = defaultDecode

newtype TxnResponse = TxnResponse {
  order_id :: String,
  txn_id :: String,
  txn_uuid :: Maybe String,
  status :: String,
  payment :: Payment
}


derive instance txnResponseGeneric :: Generic TxnResponse _
derive instance txnResponseNewtype :: Newtype TxnResponse _

instance encodeTxnResponse :: Encode TxnResponse where encode = defaultEncode
instance decodeTxnResponse :: Decode TxnResponse where decode = defaultDecode

newtype Payment = Payment {
  authentication :: Authentication,
  sdk_params :: Maybe SDKTxnParams
}

derive instance paymentGeneric :: Generic Payment _
instance encodePayment :: Encode Payment where encode = defaultEncode
instance decodePayment :: Decode Payment where decode = defaultDecode
derive instance newtypePayment :: Newtype Payment _



data SDKTxnParams = Amazon AmazonSDKParams | PhonePe PhonePeSDKParams

derive instance sdkTxnParamsGeneric :: Generic SDKTxnParams _
instance encodeSDKTxnParams :: Encode SDKTxnParams where encode = defaultEncode
instance decodeSDKTxnParams :: Decode SDKTxnParams where
  decode fgn = (Amazon <$> decode fgn) <|> (PhonePe <$> decode fgn)

newtype Authentication = Authentication {
  method :: String,
  url :: String,
  params :: Maybe Params
}
derive instance authenticationGeneric :: Generic Authentication _
derive instance authenticationNewType :: Newtype Authentication _
instance encodeAuthentication :: Encode Authentication where encode = defaultEncode
instance decodeAuthentication :: Decode Authentication where decode = defaultDecode

newtype AmazonSDKParams = AmazonSDKParams {
  encryptedParameters :: String,
  returnUrl :: String,
  sellerId :: String
}

derive instance amazonSDKParamsGeneric :: Generic AmazonSDKParams _
derive instance amazonSDKParamsNewType :: Newtype AmazonSDKParams _
instance encodeAmazonSDKParams :: Encode AmazonSDKParams where encode = defaultEncode
instance decodeAmazonSDKParams :: Decode AmazonSDKParams where decode = defaultDecode



data Params = ParamsCard CardParams | Nb Foreign | CardParamsVies CardParamsVies'


derive instance genericParams :: Generic Params _
derive instance paramsGeneric :: Generic CardParams _
derive instance paramsNewType :: Newtype CardParams _
instance encodeParams :: Encode Params
  where encode (ParamsCard cp ) = defaultEncode cp
        encode (CardParamsVies cv) = defaultEncode cv
        encode f = defaultEncode f
instance decodeParams :: Decode Params where
  decode fgn = (ParamsCard <$> decode fgn)
                <|> (CardParamsVies <$> decode fgn)
                <|> (Nb <$> decode fgn) -- TODO Check this

newtype CardParams = CardParams {
  id :: String,
  challenge_id :: String,
  auth_type :: String,
  card_isin :: String,
  card_issuer_bank_name :: String,
  resend_otp_allowed :: Boolean,
  submit_otp_allowed :: Boolean,
  fallback_url :: Maybe String
}


derive instance cardParamsGeneric :: Generic CardParams _
derive instance cardParamsNewType :: Newtype CardParams _
instance encodeCardParams :: Encode CardParams where encode = defaultEncode
instance decodeCardParams :: Decode CardParams where decode = defaultDecode

newtype CardParamsVies' = CardParamsVies' {
  card_fingerprint :: Maybe String
  , masked_card_number :: Maybe String
  , card_isin :: Maybe String
  , auth_type :: String
}

derive instance cardParamsVies'Generic :: Generic CardParamsVies' _
derive instance cardParamsVies'NewType :: Newtype CardParamsVies' _
instance encodeCardParamsVies' :: Encode CardParamsVies' where encode = defaultEncode
instance decodeCardParamsVies' :: Decode CardParamsVies' where decode = defaultDecode





newtype PhonePeSDKParams = PhonePeSDKParams {
  body :: Maybe String,
  checksum :: Maybe String,
  apiEndPoint :: Maybe String,
  headers:: Maybe PhonePeHeaders,
  redirectURL :: Maybe String,
  redirectType  :: Maybe String
}


derive instance phonePeSDKParamsGeneric :: Generic PhonePeSDKParams _
derive instance phonePeSDKParamsNewType :: Newtype PhonePeSDKParams _
instance encodePhonePeSDKParams :: Encode PhonePeSDKParams where encode = defaultEncode
instance decodePhonePeSDKParams :: Decode PhonePeSDKParams where decode = defaultDecode

newtype PhonePeHeaders = PhonePeHeaders {
  "X-CALLBACK-MODE":: String,
  "X-CALLBACK-URL":: String
}

derive instance phonePeHeadersGeneric :: Generic PhonePeHeaders _
derive instance phonePeHeadersNewType :: Newtype PhonePeHeaders _
instance encodePhonePeHeaders :: Encode PhonePeHeaders where encode = defaultEncode
instance decodePhonePeHeaders :: Decode PhonePeHeaders where decode = defaultDecode



newtype JuspaySafePayload = JuspaySafePayload
  { action :: String
  , orderId :: String
  , transactionId :: Maybe String
  , url :: String
  , postData :: Maybe String
  , html :: Maybe String
  , endUrls :: Array String
  , customBrandingEnabled :: Maybe Boolean
  , customBrandingLayout :: Maybe Int
  }

derive instance newtypeJuspaySafePayload :: Newtype JuspaySafePayload _

derive instance genericJuspaySafePayload :: Generic JuspaySafePayload _

instance encodeJuspaySafePayload :: Encode JuspaySafePayload where
  encode = defaultEncode

instance decodeJuspaySafePayload :: Decode JuspaySafePayload where
  decode = defaultDecode

newtype LegacyWalletEligibilityPayload = LegacyWalletEligibilityPayload
  { "action" :: String
  , "customerId" :: String
  , "amount" :: String
  , "orderId" :: Maybe String
  , "clientAuthToken" :: Maybe String
  , "mobile" :: String
  , "email" :: Maybe String
  , "walletName" :: String
  }
derive instance newtypeLegacyWalletEligibilityPayload :: Newtype LegacyWalletEligibilityPayload _

derive instance genericLegacyWalletEligibilityPayload :: Generic LegacyWalletEligibilityPayload _

instance encodeLegacyWalletEligibilityPayload :: Encode LegacyWalletEligibilityPayload where
  encode = defaultEncode

instance decodeLegacyWalletEligibilityPayload :: Decode LegacyWalletEligibilityPayload where
  decode = defaultDecode

newtype InitiatePayload
  = InitiatePayload
  { clientId :: String
  , merchantId :: String
  , environment :: String
  , customerId :: String
  , merchantLoader :: Maybe Boolean
  , action :: String
  }

derive instance newtypeInitiatePayload :: Newtype InitiatePayload _

derive instance genericInitiatePayload :: Generic InitiatePayload _

instance encodeInitiatePayload :: Encode InitiatePayload where
  encode = defaultEncode

instance decodeInitiatePayload :: Decode InitiatePayload where
  decode = defaultDecode

newtype ConsumerFinancePayload
  = ConsumerFinancePayload
  { "action" :: String
  , "endUrls" :: Array String
  , "orderId" :: String
  , "paymentMethod" :: String
  , "offerToken" :: Maybe String
  }

derive instance newtypeConsumerFinancePayload :: Newtype ConsumerFinancePayload _

derive instance genericConsumerFinancePayload :: Generic ConsumerFinancePayload _

instance encodeConsumerFinancePayload :: Encode ConsumerFinancePayload where
  encode = defaultEncode

instance decodeConsumerFinancePayload :: Decode ConsumerFinancePayload where
  decode = defaultDecode


newtype SubmitOTPPayload
  = SubmitOTPPayload { "action" :: String, "otp" :: String, "id" :: String, "challengeId" :: String }

derive instance newtypeSubmitOTPPayload :: Newtype SubmitOTPPayload _

derive instance genericSubmitOTPPayload :: Generic SubmitOTPPayload _

instance encodeSubmitOTPPayload :: Encode SubmitOTPPayload where
  encode = defaultEncode

instance decodeSubmitOTPPayload :: Decode SubmitOTPPayload where
  decode = defaultDecode

newtype CardOnDPayload
  = CardOnDPayload { "action" :: String }

derive instance newtypeCardOnDPayload :: Newtype CardOnDPayload _

derive instance genericCardOnDPayload :: Generic CardOnDPayload _

instance encodeCardOnDPayload :: Encode CardOnDPayload where
  encode = defaultEncode

instance decodeCardOnDPayload :: Decode CardOnDPayload where
  decode = defaultDecode

newtype CashOnDPayload
  = CashOnDPayload { "action" :: String, "orderId" :: String }

derive instance newtypeCashOnDPayload :: Newtype CashOnDPayload _

derive instance genericCashOnDPayload :: Generic CashOnDPayload _

instance encodeCashOnDPayload :: Encode CashOnDPayload where
  encode = defaultEncode

instance decodeCashOnDPayload :: Decode CashOnDPayload where
  decode = defaultDecode

newtype PaymentMethodsPayload
  = PaymentMethodsPayload { "action" :: String }

derive instance newtypePaymentMethodsPayload :: Newtype PaymentMethodsPayload _

derive instance genericPaymentMethodsPayload :: Generic PaymentMethodsPayload _

instance encodePaymentMethodsPayload :: Encode PaymentMethodsPayload where
  encode = defaultEncode

instance decodePaymentMethodsPayload :: Decode PaymentMethodsPayload where
  decode = defaultDecode

newtype RefreshWalletBalancesPayload
  = RefreshWalletBalancesPayload
  { "action" :: String
  , "clientAuthToken" :: String
  , "sdkWalletIdentifier" :: Maybe String
  , "showLoader" :: Maybe Boolean
  }

derive instance newtypeRefreshWalletBalancesPayload :: Newtype RefreshWalletBalancesPayload _

derive instance genericRefreshWalletBalancesPayload :: Generic RefreshWalletBalancesPayload _

instance encodeRefreshWalletBalancesPayload :: Encode RefreshWalletBalancesPayload where
  encode = defaultEncode

instance decodeRefreshWalletBalancesPayload :: Decode RefreshWalletBalancesPayload where
  decode = defaultDecode

newtype CardInfoPayload
  = CardInfoPayload
  { "action" :: String
  , "cardBin" :: String
  , "checkAtmPinAuthSupport" :: Maybe Boolean
  , "checkDirectOtpSupport" :: Maybe Boolean
  }

derive instance newtypeCardInfoPayload :: Newtype CardInfoPayload _

derive instance genericCardInfoPayload :: Generic CardInfoPayload _

instance encodeCardInfoPayload :: Encode CardInfoPayload where
  encode = defaultEncode

instance decodeCardInfoPayload :: Decode CardInfoPayload where
  decode = defaultDecode

newtype ProcessOTPPayload
  = ProcessOTPPayload
  { "action" :: String
  , "challengeId" :: String
  , "otp" :: String
  , "autoCapture" :: Maybe String
  , "id" :: String
  }

derive instance newtypeProcessOTPPayload :: Newtype ProcessOTPPayload _

derive instance genericProcessOTPPayload :: Generic ProcessOTPPayload _

instance encodeProcessOTPPayload :: Encode ProcessOTPPayload where
  encode = defaultEncode

instance decodeProcessOTPPayload :: Decode ProcessOTPPayload where
  decode = defaultDecode



derive instance newtypeWalletPayload :: Newtype WalletPayload _

derive instance genericWalletPayload :: Generic WalletPayload _

instance encodeWalletPayload :: Encode WalletPayload where
  encode = defaultEncode

instance decodeWalletPayload :: Decode WalletPayload where
  decode = defaultDecode

newtype AuthWalletPayload
  = AuthWalletPayload
  { "action" :: String
  , "clientAuthToken" :: String
  , "walletId" :: String
  , "gatewayReferenceId" :: Maybe String
  }

derive instance newtypeAuthWalletPayload :: Newtype AuthWalletPayload _

derive instance genericAuthWalletPayload :: Generic AuthWalletPayload _

instance encodeAuthWalletPayload :: Encode AuthWalletPayload where
  encode = defaultEncode

instance decodeAuthWalletPayload :: Decode AuthWalletPayload where
  decode = defaultDecode

newtype ListWalletsPayload
  = ListWalletsPayload { "action" :: String, "clientAuthToken" :: String }

derive instance newtypeListWalletsPayload :: Newtype ListWalletsPayload _

derive instance genericListWalletsPayload :: Generic ListWalletsPayload _

instance encodeListWalletsPayload :: Encode ListWalletsPayload where
  encode = defaultEncode

instance decodeListWalletsPayload :: Decode ListWalletsPayload where
  decode = defaultDecode



-- Link Wallet Payload
newtype LinkWalletPayload =
  LinkWalletPayload
    { action :: String
    , clientAuthToken :: String
    , walletId :: String
    , otp :: String
    , walletName :: String
    , sdkWalletIdentifier :: Maybe String
    , gatewayReferenceId :: Maybe String
    }

-- Delink Wallet Payload
newtype WalletPayload =
  WalletPayload
    { action :: String
    , clientAuthToken :: String
    , walletId :: String
    , walletName :: String
    , sdkWalletIdentifier :: Maybe String
    }


-- Device Ready Payload
newtype DeviceReadyPayload =
  DeviceReadyPayload
    { action :: String
    , sdkPresent :: String
    }


-- UPI Payload
newtype UPIPayload =
  UPIPayload
    { action :: String
    , endUrls :: Maybe (Array String)
    , orderId :: String
    , upiSdkPresent :: Maybe Boolean
    , paymentMethod :: Maybe String
    , displayNote :: Maybe String
    , custVpa :: Maybe String
    , payWithApp :: Maybe String
    , currency :: Maybe String
    , getAvailableApps :: Maybe Boolean
    , signature :: Maybe String
    , merchantKeyId :: Maybe String
    , orderDetails :: Maybe String
    , saveToLocker :: Maybe Boolean
    , handlePolling :: Maybe Boolean
    , showLoader :: Maybe Boolean
    }

-- EMI Payloads
newtype EmiPayload = 
  EmiPayload 
    { clientAuthToken :: String
    , orderId :: Maybe String 
    , amount :: String 
    , orderDetails :: Maybe String
    , action :: String
    , customerId :: String
    }

-- Card Payloads
newtype NewCardPayload =
  NewCardPayload
    { action :: String
    , orderId :: Maybe String
    , amount :: Maybe String
    , endUrls :: Maybe (Array String)
    , cardNumber :: String
    , cardExpMonth :: String
    , cardExpYear :: String
    , cardSecurityCode :: String
    , nameOnCard :: Maybe String
    , saveToLocker :: Maybe Boolean
    , isEmi :: Maybe Boolean
    , emiBank :: Maybe String
    , emiTenure :: Maybe Int
    , clientAuthToken :: Maybe String
    , authType :: Maybe String
    , smsPermissionDenyLimit :: Maybe Int
    , offerToken :: Maybe String
    , paymentMethod :: Maybe String
    , redirectAfterPayment :: Maybe Boolean
    , shouldCreateMandate :: Maybe Boolean
    , orderDetails :: Maybe String
    , signature :: Maybe String
    , merchantKeyId :: Maybe String
    , cardAlias :: Maybe String
    , maskedCardNumber :: Maybe String
    , emiType :: Maybe String
    }


newtype SavedCardPayload =
  SavedCardPayload
    { action :: String
    , endUrls :: Array String
    , amount :: Maybe String
    , orderId :: String
    , cardBin :: Maybe String
    , cardToken :: String
    , cardSecurityCode :: String
    , authType :: Maybe String
    , clientAuthToken :: Maybe String
    , isEmi :: Maybe Boolean
    , emiBank :: Maybe String
    , emiTenure :: Maybe String
    , smsPermissionDenyLimit :: Maybe Int
    , offerToken :: Maybe String
    , paymentMethod :: Maybe String
    , shouldCreateMandate :: Maybe Boolean
    , orderDetails :: Maybe String
    , signature :: Maybe String
    , merchantKeyId :: Maybe String
    , cardAlias :: Maybe String
    , maskedCardNumber :: Maybe String
    , saveToLocker :: Maybe Boolean
    , emiType :: Maybe String
    }


newtype ListCardsPayload =
  ListCardsPayload
    { action :: String
    , clientAuthToken :: String
    }


newtype DeleteCardPayload =
  DeleteCardPayload
    { action :: String
    , cardToken :: String
    , clientAuthToken :: String
    , cardAlias :: Maybe String
    }

newtype PaymentSourcePayload =
  PaymentSourcePayload
    { clientAuthToken :: Maybe String
    , offers :: Maybe String
    , refresh :: Maybe String
    , signature :: Maybe String
    , order_details :: Maybe String
    , merchant_key_id :: Maybe String
    , mandate_feature :: Maybe String
    , add_emandate_payment_methods :: Maybe String
    , supported_reference_ids_feature :: Maybe String
    , action :: String
    }


-- Eligibility Payload

newtype EligibilityPayload = 
	EligibilityPayload 
		{ action :: String
    , amount :: String
    , orderId :: String
    , clientAuthToken :: String
    , environment :: String
		, "data" ::  EligibilityData
    , showLoader :: Maybe Boolean
		}



newtype EligibilityData = EligibilityData
  { cards :: Maybe (Array CardEligibilityData)
  , wallets :: Maybe (Array WalletEligibilityData)
  }

newtype CardEligibilityData = CardEligibilityData
  { cardBin :: String
  , cardAlias :: Maybe String
  , checkType :: Array String
  }


newtype WalletEligibilityData = WalletEligibilityData
  { customerId :: String
  , mobile :: String
  , email :: Maybe String
  , checkType :: Array String
  }

newtype OutageAPIPayload =
  OutageAPIPayload
    { action :: String
    , clientAuthToken :: String
    , customerId :: String
    }

-- Instances --

-- SDK Payload
derive instance genericSDKPayload :: Generic SDKPayload _
derive instance newtypeSDKPayload :: Newtype SDKPayload _
instance encodeSDKPayload :: Encode SDKPayload where encode = defaultEncode

-- EC Payload
derive instance genericECPayload :: Generic ECPayload _
instance encodeECPayload :: Encode ECPayload where encode = defaultEncode



-- NB Payload
derive instance genericNBPayload :: Generic NBPayload _
instance encodeNBPayload :: Encode NBPayload where encode = defaultEncode

-- Wallet TXN
derive instance genericWalletTxnPayload :: Generic WalletTxnPayload _
instance encodeWalletTxnPayload :: Encode WalletTxnPayload where encode = defaultEncode

-- Create Wallet
derive instance genericCreateWalletPayload :: Generic CreateWalletPayload  _
instance encodeCreateWalletPayload :: Encode CreateWalletPayload where encode = defaultEncode

-- Link Wallet
derive instance genericLinkWalletPayload :: Generic LinkWalletPayload  _
instance encodeLinkWalletPayload :: Encode LinkWalletPayload where encode = defaultEncode

-- Delink Wallet

-- Device Ready
derive instance genericDeviceReadyPayload :: Generic DeviceReadyPayload _
instance encodeDeviceReadyPayload :: Encode DeviceReadyPayload where encode = defaultEncode

-- UPI Payload
derive instance newtypeUPIPayload :: Newtype UPIPayload _
derive instance genericUPIPayload :: Generic UPIPayload _
instance encodeUPIPayload :: Encode UPIPayload where encode = defaultEncode
instance decodeUPIPayload :: Decode UPIPayload where decode = defaultDecode

-- EMI Payload
derive instance newtypeEmiPayload :: Newtype EmiPayload _
derive instance genericEmiPayload :: Generic EmiPayload _
instance encodeEmiPayload :: Encode EmiPayload where encode = defaultEncode
instance decodeEmiPayload :: Decode EmiPayload where decode = defaultDecode

-- New Card Payload
derive instance newtypeNewCardPayload :: Newtype NewCardPayload _
derive instance genericNewCardPayload :: Generic NewCardPayload _
instance encodeNewCardPayload :: Encode NewCardPayload where encode = defaultEncode
instance decodeNewCardPayload :: Decode NewCardPayload where decode = defaultDecode

-- Saved Card Payload
derive instance newtypeSavedCardPayload :: Newtype SavedCardPayload _
derive instance genericSavedCardPayload :: Generic SavedCardPayload _
instance encodeSavedCardPayload :: Encode SavedCardPayload where encode = defaultEncode
instance decodeSavedCardPayload :: Decode SavedCardPayload where decode = defaultDecode

-- List Card Payload
derive instance newtypeListCardsPayload :: Newtype ListCardsPayload _
derive instance genericListCardsPayload :: Generic ListCardsPayload _
instance encodeListCardsPayload :: Encode ListCardsPayload where encode = defaultEncode
instance decodeListCardsPayload :: Decode ListCardsPayload where decode = defaultDecode

-- Delete Card Payload
derive instance newtypeDeleteCardPayload :: Newtype DeleteCardPayload _
derive instance genericDeleteCardPayload :: Generic DeleteCardPayload _
instance encodeDeleteCardPayload :: Encode DeleteCardPayload where encode = defaultEncode
instance decodeDeleteCardPayload :: Decode DeleteCardPayload where decode = defaultDecode

-- Payment Source Payload
derive instance newtypePaymentSourcePayload :: Newtype PaymentSourcePayload _
derive instance genericPaymentSourcePayload :: Generic PaymentSourcePayload _
instance encodePaymentSourcePayload :: Encode PaymentSourcePayload where encode = defaultEncode
instance decodePaymentSourcePayload :: Decode PaymentSourcePayload where decode = defaultDecode

--Eligibility Payload

derive instance genericEligibilityRequestPayload :: Generic EligibilityPayload _
derive instance newtypeEligibilityRequestPayload :: Newtype EligibilityPayload _
instance decodeEligibilityRequestPayload :: Decode EligibilityPayload where decode = defaultDecode
instance encodeEligibilityRequestPayload :: Encode EligibilityPayload where encode = defaultEncode

derive instance newtypeWalletEligibilityData :: Newtype WalletEligibilityData _
derive instance genericWalletEligibilityData :: Generic WalletEligibilityData _
instance encodeWalletEligibilityData :: Encode WalletEligibilityData where encode = defaultEncode
instance decodeWalletEligibilityData :: Decode WalletEligibilityData where decode = defaultDecode

derive instance newtypeEligibilityData :: Newtype EligibilityData _
derive instance genericEligibilityData :: Generic EligibilityData _
instance encodeEligibilityData :: Encode EligibilityData where encode = defaultEncode
instance decodeEligibilityData :: Decode EligibilityData where decode = defaultDecode

derive instance newtypeCardEligibilityData :: Newtype CardEligibilityData _
derive instance genericCardEligibilityData :: Generic CardEligibilityData _
instance encodeCardEligibilityData :: Encode CardEligibilityData where encode = defaultEncode
instance decodeCardEligibilityData :: Decode CardEligibilityData where decode = defaultDecode

-- OutageAPI Payload

derive instance newtypeOutageAPIPayload :: Newtype OutageAPIPayload _
derive instance genericOutageAPIPayload :: Generic OutageAPIPayload _
instance encodeOutageAPIPayload :: Encode OutageAPIPayload where encode = defaultEncode
instance decodeOutageAPIPayload :: Decode OutageAPIPayload where decode = defaultDecode















