module Service.EC.Types.Instruments where

import Prelude

import Data.Generic.Rep (class Generic)
import Data.Maybe (Maybe)
import Data.Newtype (class Newtype)
import Foreign.Class (class Decode, class Encode)
import Presto.Core.Utils.Encoding (defaultDecode, defaultEncode)

newtype StoredCard =
  StoredCard
    { nickname :: String
    , nameOnCard :: String
    , expired :: Boolean
    , cardType :: String
    , cardToken :: String
    , cardReference :: String
    , cardNumber :: String
    , cardIssuer :: String
    , cardIsin :: String
    , cardFingerprint :: String
    , cardExpYear :: String
    , cardExpMonth :: String
    , cardBrand :: String
    , count :: Maybe Number
    , lastUsed :: Maybe String
    , rating :: Maybe Number
    , mandateSupport :: Maybe Boolean
    }

newtype EMIPlan =
  EMIPlan
    { transactionAmount :: Number
    , totalAmount :: Maybe Number
    , min_amount :: Maybe Number
    , tenure :: Int
    , interest :: Number
    , gateway :: String
    , gatewayId :: Int
    , emiAmount :: Maybe Number
    , bank :: String
    }

newtype EMIData =
  EMIData
		{ amount :: Number
		, tenure :: Int
		, noCost :: Boolean
		, interest :: Number
		, bank :: String
		, emiPlan :: EMIPlan
		}

newtype Wallet =
  Wallet
		{ wallet :: Maybe String
		, token :: Maybe String
		, linked :: Maybe Boolean
		, id :: String
		, current_balance :: Maybe Number
		, last_refreshed :: Maybe String
		, currentBalance :: Maybe Number
    , metadata :: Maybe WalletMetadata
    , gateway_reference_id :: Maybe String
		, lastRefreshed :: Maybe String
		, object :: Maybe String
		, status :: Maybe String
		, error_code :: Maybe String
		}


newtype WalletMetadata = WalletMetadata {
  email :: Maybe String,
  payer_id :: Maybe String
}

newtype StoredWallet = StoredWallet Wallet

newtype StoredVPA =
	StoredVPA
		{	vpa :: String
		,	id :: String
		,	count :: Maybe Number
		,	lastUsed :: Maybe String
		,	rating :: Maybe Number
		}

newtype StoredNB =
  StoredNB
		{ method :: String
		, id :: String
		, count :: Maybe Number
		, lastUsed :: Maybe String
		, rating :: Maybe Number
		}

newtype LastUsedPaymentMethod =
  LastUsedPaymentMethod
		{
		-- Common Keys --
			methodType ::  Maybe String
		,	id :: Maybe String
		,	lastUsed :: Maybe String
		-- Card Data --
		, 	cardBrand :: Maybe String
		,	cardExpMonth ::  Maybe String
		,	cardExpYear ::  Maybe String
		,	cardFingerprint ::  Maybe String
		,	cardIsin ::  Maybe String
		,	cardIssuer ::  Maybe String
		,	cardNumber ::  Maybe String
		,	cardReference ::  Maybe String
		,	cardToken ::  Maybe String
		,	cardType ::  Maybe String
		,	expired ::  Maybe Boolean
		,	nameOnCard ::  Maybe String
		,	nickname ::  Maybe String
		,	value :: Maybe String
		, mandateSupport :: Maybe Boolean
		-- Wallet Data --
		, 	currentBalance :: Maybe Number
		,	lastRefreshed :: Maybe String
		,	linked :: Maybe Boolean
		,	token :: Maybe String
		,	wallet :: Maybe String
		-- NB Data --
		, 	method  :: Maybe String
		-- VPA --
		, 	vpa :: Maybe String
		}

newtype MerchantPaymentMethod =
  MerchantPaymentMethod
		{ paymentMethodType:: String
		, paymentMethod:: String
		, description:: String
		, walletDirectDebitSupport :: Maybe Boolean
		, supportedReferenceIds :: Maybe (Array String)
		}

newtype AppUsed =
	AppUsed
		{ packageName :: Maybe String
		, lastUsed :: Maybe String
		, id :: Maybe String
		, count :: Maybe Number
		}

newtype Offer =
	Offer
		{ voucherCode :: String
		, visibleToCustomer :: Maybe Boolean
		, paymentMethodType :: Maybe String
		, paymentMethod :: Maybe (Array String)
		, offerDescription :: Maybe OfferDescription
		, paymentChannel :: Maybe (Array String)
  	, paymentMethodFilter :: Maybe (Array String)
    , applicationMode :: Maybe String
    , calculationMode :: Maybe String
    , discountValue :: Maybe Int
    , id :: Maybe String
    , offerToken :: Maybe String
		}

newtype OfferDescription =
	OfferDescription
		{ offerDescription :: Maybe String
		, offerDisplay1 :: Maybe String
		, offerDisplay2 :: Maybe String
		, offerDisplay3 :: Maybe String
		}

newtype WalletEligibilityRespItem =
	WalletEligibilityRespItem
		{ status :: String,
      payment_method_type :: String,
      payment_method :: String,
      is_eligible :: Boolean,
      eligibility_strategy :: Maybe String,
      description :: String,
      balance :: Maybe Number,
      gateway_error_code :: Maybe String,
      gateway_error_message :: Maybe String
		}

newtype PaymentMethodsEligibility =
	PaymentMethodsEligibility
		{ status :: String
		, paymentMethodType :: String
		, paymentMethod :: String
		, isEligible :: Boolean
		, description :: String
		, balance :: Maybe Number
    , gateway_error_message :: Maybe String
		}

--- Instances ---

-- Stored Card
derive instance genericStoredCard :: Generic StoredCard _
instance decodeStoredCard :: Decode StoredCard where decode = defaultDecode
instance encodeStoredCard :: Encode StoredCard where encode = defaultEncode

instance eqStoredCard :: Eq StoredCard where
  eq s1 s2 = compareStoredCard s1 s2

compareStoredCard :: StoredCard -> StoredCard -> Boolean
compareStoredCard (StoredCard s1) (StoredCard s2) =
    s1.nickname ==  s2.nickname &&
    s1.nameOnCard == s2.nameOnCard &&
  s1.expired == s2.expired &&
  s1.cardType == s2.cardType &&
  s1.cardToken == s2.cardToken &&
  s1.cardReference == s2.cardReference &&
  s1.cardNumber == s2.cardNumber &&
  s1.cardIssuer == s2.cardIssuer &&
  s1.cardIsin == s2.cardIsin &&
  s1.cardFingerprint == s2.cardFingerprint &&
  s1.cardExpYear == s2.cardExpYear &&
  s1.cardExpMonth == s2.cardExpMonth &&
  s1.cardBrand == s2.cardBrand &&
  s1.count == s2.count &&
  s1.lastUsed == s2.lastUsed &&
  s1.rating == s2.rating &&
  s1.mandateSupport == s2.mandateSupport

-- EMI Plans
derive instance genericEMIPlan :: Generic EMIPlan _
instance decodeEMIPlan :: Decode EMIPlan where decode = defaultDecode
instance encodeEMIPlan :: Encode EMIPlan where encode = defaultEncode

-- EMI Data
derive instance newtypeEMIData :: Newtype EMIData _

instance eqEMIData :: Eq EMIData where
  eq e1 e2 = compareEMIData e1 e2

compareEMIData :: EMIData -> EMIData -> Boolean
compareEMIData (EMIData e1) (EMIData e2) =
  e1.amount == e2.amount &&
  e1.tenure == e2.tenure &&
  e1.noCost == e2.noCost &&
  e1.interest == e2.interest &&
  e1.bank == e2.bank

-- Wallet
derive instance genericWallet :: Generic Wallet _
instance decodeWallet :: Decode Wallet where decode = defaultDecode
instance encodeWallet :: Encode Wallet where encode = defaultEncode

derive instance walletMetadataGeneric :: Generic WalletMetadata _
instance encodeWalletMetadata :: Encode WalletMetadata where encode = defaultEncode
instance decodeWalletMetadata :: Decode WalletMetadata where decode = defaultDecode

instance eqWalletData :: Eq Wallet where
  eq w1 w2 = compareWalletData w1 w2

compareWalletData :: Wallet -> Wallet -> Boolean
compareWalletData (Wallet w1) (Wallet w2) =
	w1.id == w2.id &&
	w1.token == w2.token &&
	w1.wallet == w2.wallet

-- Stored Wallet
derive instance genericStoredWallet :: Generic StoredWallet _
instance decodeStoredWallet :: Decode StoredWallet where decode = defaultDecode
instance encodeStoredWallet :: Encode StoredWallet where encode = defaultEncode

instance eqStoredWalletData :: Eq StoredWallet where
  eq w1 w2 = compareStoredWalletData w1 w2

compareStoredWalletData :: StoredWallet -> StoredWallet -> Boolean
compareStoredWalletData (StoredWallet w1) (StoredWallet w2) = compareWalletData w1 w2

-- Stored VPA
derive instance genericStoredVPA :: Generic StoredVPA _
instance decodeStoredVPA :: Decode StoredVPA where decode = defaultDecode
instance encodeStoredVPA :: Encode StoredVPA where encode = defaultEncode

instance eqStoredVPAData :: Eq StoredVPA where
  eq v1 v2 = compareStoredVPAData v1 v2

compareStoredVPAData :: StoredVPA -> StoredVPA -> Boolean
compareStoredVPAData (StoredVPA v1) (StoredVPA v2) =
	v1.vpa == v2.vpa  &&
	v1.id == v2.id

-- Stored NB
derive instance genericStoredNB :: Generic StoredNB _
instance decodeStoredNB :: Decode StoredNB where decode = defaultDecode
instance encodeStoredNB :: Encode StoredNB where encode = defaultEncode

-- Last Used Payment Instrument
derive instance genericLastUsedPaymentMethod :: Generic LastUsedPaymentMethod _
instance decodeLastUsedPaymentMethod :: Decode LastUsedPaymentMethod where decode = defaultDecode
instance encodeLastUsedPaymentMethod :: Encode LastUsedPaymentMethod where encode = defaultEncode

-- Merchant Payment Method
derive instance genericMerchantPaymentMethod :: Generic MerchantPaymentMethod _
instance decodeMerchantPaymentMethod :: Decode MerchantPaymentMethod where decode = defaultDecode
instance encodeMerchantPaymentMethod :: Encode MerchantPaymentMethod where encode = defaultEncode

instance eqMerchantPaymentMethod :: Eq MerchantPaymentMethod where
  eq m1 m2 = compareMerchantPaymentMethod m1 m2

compareMerchantPaymentMethod :: MerchantPaymentMethod -> MerchantPaymentMethod -> Boolean
compareMerchantPaymentMethod (MerchantPaymentMethod m1) (MerchantPaymentMethod m2) =
	m1.paymentMethodType == m2.paymentMethodType &&
	m1.paymentMethod == m2.paymentMethod

-- App Used
derive instance genericAppUsed :: Generic AppUsed _
instance decodeAppUsed :: Decode AppUsed where decode = defaultDecode
instance encodeAppUsed :: Encode AppUsed where encode = defaultEncode

-- Offer
derive instance genericOffer :: Generic Offer _
instance decodeOffer :: Decode Offer where decode = defaultDecode
instance encodeOffer :: Encode Offer where encode = defaultEncode

instance eqOfferData :: Eq Offer where
  eq v1 v2 = compareOfferData v1 v2

compareOfferData :: Offer -> Offer -> Boolean
compareOfferData (Offer f1) (Offer f2) =
	f1.voucherCode == f2.voucherCode &&
	f1.paymentMethodType == f2.paymentMethodType
	-- , paymentMethod :: Maybe (Array String) TODO :: add compare for this as well

-- OfferDescription
derive instance genericOfferDescription :: Generic OfferDescription _
instance decodeOfferDescription :: Decode OfferDescription where decode = defaultDecode
instance encodeOfferDescription :: Encode OfferDescription where encode = defaultEncode

derive instance genericWalletEligibilityRespItem :: Generic WalletEligibilityRespItem _
derive instance newtypeWalletEligibilityRespItem :: Newtype WalletEligibilityRespItem _
instance decodeWalletEligibilityRespItem :: Decode WalletEligibilityRespItem where decode = defaultDecode
instance encodeWalletEligibilityRespItem :: Encode WalletEligibilityRespItem where encode = defaultEncode
instance eqWalletEligibilityRespItem :: Eq WalletEligibilityRespItem where
  eq (WalletEligibilityRespItem i1) (WalletEligibilityRespItem i2) = i1.payment_method == i2.payment_method

-- Eligibility
derive instance genericPaymentMethodsEligibility :: Generic PaymentMethodsEligibility _
derive instance newtypePaymentMethodsEligibility :: Newtype PaymentMethodsEligibility _
instance decodePaymentMethodsEligibility :: Decode PaymentMethodsEligibility where decode = defaultDecode
instance encodePaymentMethodsEligibility :: Encode PaymentMethodsEligibility where encode = defaultEncode
