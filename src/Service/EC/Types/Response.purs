module Service.EC.Types.Response where

import Prelude

import Data.Generic.Rep (class Generic)
import Data.Maybe (Maybe)
import Prelude (class Eq, class Show, bind, pure, show, ($), (<$>), (<>), (==), (>>=), (||))
import Data.Newtype (class Newtype)
import Foreign (Foreign, readString, fail, ForeignError(..))
import Foreign.Class (class Decode, class Encode, decode, encode)
import Foreign.Generic (decodeJSON, encodeJSON)
import Foreign.Index (readProp)
import Payments.Core.Types (DeleteCardResp)
import Payments.UPI.Types (PaymentResp(..))
import Foreign (F, Foreign, ForeignError(..), fail, isNull, isUndefined, readString, unsafeToForeign)
import Service.EC.Types.Request (TxnResponse,Payment)
import Presto.Core.Utils.Encoding (defaultDecode, defaultEncode)
import Service.EC.Types.Instruments (Wallet, AppUsed, EMIPlan, LastUsedPaymentMethod, MerchantPaymentMethod, Offer, PaymentMethodsEligibility, StoredCard, StoredNB, StoredVPA, StoredWallet)
import Control.Alt ((<|>))

newtype SDKResponse =
  SDKResponse
    { requestId :: String
    , service :: String
    , payload :: ECResponse
    , errorMessage :: String
    , errorCode :: String
    , error :: Boolean
    }
data ECResponse
  = ListCard ListCardsResp
  | CardDeleted DeleteCardResp
  | OrderStatus OrderStatusResp
  | UPITxn TxnResponse
  | PaymentSources PaymentSourceResp
  | PaymentMethods PaymentMethodsResp
  | OutageAPIECResp OutageAPIResp
  | IsDeviceReady DeviceReadyResponse
  | WalletEligibility WalletEligibilitySdkResp
  | EMI EmiPlansResp
  | JuspaySafeResponse Foreign
  | CreateWalletResponse CreateWalletResp
  | LinkWalletResponse LinkWalletResp
  | DelinkWalletResponse DelinkWalletResp
  | DeleteCardResponse DeleteCardResp
  | UPIResponse UPIResp
  | EligibilityResponse EligibilityResp
  | OutageAPIResponse OutageAPIResp
  | Txn TxnResponse -- CardToken CardTokenizeResp
  | OTPProcess ProcessOTPResp
  | InfoCard CardInfoResp
  | WalletOperation WalletOpResponse
  | AvailableUPIApps UPIAppsResponse
  | Initiation InitiateResponse
  | DotpResp DotpResponse
  | NBListResp NbListResp
  | NBValidationResp NbList
  | NBValidationStatusResp NbList
  | OnError ErrorPayload



derive instance genericECResponse :: Generic ECResponse _
instance decodeECResponse :: Decode ECResponse where
  decode response = do
    action <- readProp "action" response >>= readString
    case action of
      "emiPlans"      -> EMI <$> decode response
      "cardTxn"       -> Txn <$> decode response
      "walletTxn"     -> Txn <$> decode response
      "nbTxn"         -> Txn <$> decode response
      "codTxn"        -> Txn <$> decode response -- TODO :: check if this is useful or not.
      "paymentSource" -> PaymentSources <$> decode response
      "createWallet"  -> CreateWalletResponse <$> decode response
      "linkWallet"    -> LinkWalletResponse <$> decode response
      "delinkWallet"  -> DelinkWalletResponse <$> decode response
      "deleteCard"    -> CardDeleted <$> decode response
      "eligibility"   -> EligibilityResponse <$> decode response
      "isDeviceReady" -> IsDeviceReady <$> decode response
      "getOutage"     -> OutageAPIECResp <$> decode response
      "upiTxn"        -> UPITxn <$> decode response <|> Txn <$> decode response
      _ ->  fail $ ForeignError $ "Unknown action: " <> action



isPropValid :: String -> Foreign -> F Boolean
isPropValid prop fgn = do
  val <- readProp prop fgn
  if isNull val || isUndefined val
    then pure false
    else do
      _val <- readString val
      if _val == ""
        then pure false
        else pure true

newtype ErrorPayload = ErrorPayload
  { "error_code" :: Maybe String
  , "error_message" :: Maybe String
  , "status" :: Maybe String
  , "userMessage" :: Maybe String
  , "error" :: Maybe Boolean
  , "errorMessage" :: Maybe String
  }


derive instance errorPayloadGeneric :: Generic ErrorPayload _
derive instance errorPayloadNewtype :: Newtype ErrorPayload _

instance encodeErrorPayload :: Encode ErrorPayload where encode = defaultEncode
instance decodeErrorPayload :: Decode ErrorPayload where
  decode fgn  = do
    valid <- isPropValid "userMessage" fgn
    if valid then do
            error_message <- readProp "errorMessage" fgn >>= readString
            defaultDecode $ unsafeToForeign {error_code : errorCode.default_error, error_message : error_message}
      else do defaultDecode fgn
instance showErrorPayload :: Show ErrorPayload where show = encodeJSON

type ErrorCode = {
    invalid_request :: String
  , amazonpay_error :: String
  , googlepay_error :: String
  , phonepe_error :: String
  , sdk_internal_error :: String
  , default_error :: String
  }

errorCode :: ErrorCode
errorCode = {
    invalid_request : "invalid_request_error"
  , amazonpay_error : "amazonpay_error"
  , googlepay_error : "googlepay_error"
  , phonepe_error : "phonepe_error"
  , sdk_internal_error : "sdk_internal_error"
  , default_error : "error"
  }
newtype NbListResp = NbListResp {
    total :: Int
   , list :: Array NbList
}
derive instance nbListRespGeneric :: Generic NbListResp _
instance encodeNbListResp :: Encode NbListResp where encode = defaultEncode
instance decodeNbListResp :: Decode NbListResp where decode = defaultDecode
instance showNbListAPIResp :: Show NbListResp where show = encodeJSON

newtype NbList = NbList { validation_status :: String
  , validation_amount :: Int
  , object :: String
  , last_validated :: String
  , ifsc :: String
  , id :: String
  , date_created :: String
  , customer_id :: String
  , currency :: String
  , beneficiary_name :: String
  , bank_name :: String
  , account_number :: String
}

derive instance nbListGeneric :: Generic NbList _
instance encodeNbList :: Encode NbList where encode = defaultEncode
instance decodeNbList :: Decode NbList where decode = defaultDecode
instance showNbList :: Show NbList where show = encodeJSON




newtype DotpResponse = DotpResponse { status :: String }

instance showDotpResponse :: Show DotpResponse where show (DotpResponse s)= show s
derive instance genericDotpResponse :: Generic DotpResponse _
instance encodeDotpResponse :: Encode DotpResponse where encode = defaultEncode
instance decodeDotpResponse :: Decode DotpResponse where decode = defaultDecode

newtype InitiateResponse = InitiateResponse { status :: Maybe String}
derive instance newtypeInitiateResponse :: Newtype InitiateResponse _
derive instance genericInitiateResponse :: Generic InitiateResponse _
instance encodeInitiateResponse :: Encode InitiateResponse where encode = defaultEncode
instance decodeInitiateResponse :: Decode InitiateResponse where decode = defaultDecode


newtype InstalledApp
  = InstalledApp
  { app_name :: String
  , package_name :: String
  }

derive instance newtypeInstalledApp :: Newtype InstalledApp _

derive instance genericInstalledApp :: Generic InstalledApp _

instance encodeInstalledApp :: Encode InstalledApp where
  encode = defaultEncode

instance decodeInstalledApp :: Decode InstalledApp where
  decode f = do -- camel case -> snake case
    appName <- readProp "appName" f >>= readString
    packageName <- readProp "packageName" f >>= readString
    defaultDecode $ unsafeToForeign $ { package_name: packageName, app_name: appName }


newtype UPIAppsResponse
  = UPIAppsResponse
  { available_apps :: Array InstalledApp
  }

derive instance newtypeUPIAppsResponse :: Newtype UPIAppsResponse _

derive instance genericUPIAppsResponse :: Generic UPIAppsResponse _

instance encodeUPIAppsResponse :: Encode UPIAppsResponse where
  encode = defaultEncode

instance decodeUPIAppsResponse :: Decode UPIAppsResponse where
  decode = defaultDecode


data WalletOpResponse = WalletObject Wallet | ListWallet ListWalletResp
derive instance walletOpResponseGeneric :: Generic WalletOpResponse _
-- defaultEncode
instance encodeWalletOpResponse :: Encode WalletOpResponse where
  encode fgn = do
    case fgn of
      WalletObject a -> encode a
      ListWallet b -> encode b

instance decodeWalletOpResponse :: Decode WalletOpResponse where
  decode fgn = do
    object <- readProp "object" fgn >>= readString
    case object of
      "list" -> ListWallet <$> decode fgn
      _      -> WalletObject <$> decode fgn





newtype ListWalletResp = ListWalletResp {
  object:: String,
  list:: Array Wallet,
  count:: Int,
  offset:: Int,
  total:: Int
}

derive instance listWalletRespGeneric :: Generic ListWalletResp _
instance encodeListWalletResp :: Encode ListWalletResp where encode = defaultEncode
instance decodeListWalletResp :: Decode ListWalletResp where decode = defaultDecode
instance showListWalletResp :: Show ListWalletResp where show = encodeJSON



newtype CardTokenizeResp = CardTokenizeResp {
  token :: String
}

derive instance cardTokenizeRespGeneric :: Generic CardTokenizeResp _
instance decodeCardTokenizeResp :: Decode CardTokenizeResp where decode = defaultDecode
instance encodeCardTokenizeResp :: Encode CardTokenizeResp where encode = defaultEncode
instance showCardTokenizeResp :: Show CardTokenizeResp where show = encodeJSON

newtype CardInfoResp = CardInfoResp {
    id::String,
    object::String,
    brand::String,
    bank::String,
    country::String,
    type::String,
    atm_pin_auth_support :: Maybe Boolean,
    direct_otp_support :: Maybe Boolean
}

derive instance cardInfoRespGeneric :: Generic CardInfoResp _
instance decodeCardInfoResp :: Decode CardInfoResp where decode = defaultDecode
instance encodeCardInfoResp :: Encode CardInfoResp where encode = defaultEncode
instance showCardInfoResp :: Show CardInfoResp where show = encodeJSON

newtype ProcessOTPResp = ProcessOTPResp {
    id::String,
    order_id::String,
    txn_id::String,
    status::String,
    gateway::String,
    created::String,
    resp_code::String,
    resp_message::String,
    payment :: Maybe Payment
}

derive instance processOTPRespGeneric :: Generic ProcessOTPResp _
instance encodeProcessOTPResp :: Encode ProcessOTPResp where encode = defaultEncode
instance decodeProcessOTPResp :: Decode ProcessOTPResp where decode = defaultDecode
instance showProcessOTPResp :: Show ProcessOTPResp where show = encodeJSON

newtype ListCardsResp = ListCardsResp {
  customer_id:: String,
  merchantId:: String,
  cards:: Array CardType
}
derive instance listCardsRespGeneric :: Generic ListCardsResp _
instance encodeListCardsResp :: Encode ListCardsResp where encode = defaultEncode
instance decodeListCardsResp :: Decode ListCardsResp where decode = defaultDecode
instance showListCardsResp :: Show ListCardsResp where show = encodeJSON



newtype CardType = CardType {
  card_token:: String,
  card_reference:: String,
  card_fingerprint:: String,
  card_number:: String,
  card_isin:: String,
  card_exp_year:: String,
  card_exp_month:: String,
  card_type:: String,
  card_issuer:: String,
  card_brand:: String,
  nickname:: String,
  name_on_card:: String,
  expired:: Boolean
}


derive instance cardTypeGeneric :: Generic CardType _
instance encodeCardType :: Encode CardType where encode = defaultEncode
instance decodeCardType :: Decode CardType where decode = defaultDecode


newtype PaymentMethodsResp = PaymentMethodsResp {
  payment_methods :: Array ECMerchantPaymentMethod
}


derive instance paymentMethodsRespGeneric :: Generic PaymentMethodsResp _
instance encodePaymentMethodsResp :: Encode PaymentMethodsResp where encode = defaultEncode
instance decodePaymentMethodsResp :: Decode PaymentMethodsResp where decode = defaultDecode
instance showPaymentMethodResp :: Show PaymentMethodsResp where show = encodeJSON

newtype ECMerchantPaymentMethod = ECMerchantPaymentMethod  {
  payment_method_type:: String,
  payment_method:: String,
  description:: String
}


derive instance ecMerPayMethodReqGeneric :: Generic ECMerchantPaymentMethod _

instance decodeECMerchantPaymentMethod :: Decode ECMerchantPaymentMethod where decode = defaultDecode
instance encodeECMerchantPaymentMethod :: Encode ECMerchantPaymentMethod where encode = defaultEncode

newtype UPIResp =
  UPIResp
    { orderId :: String
    , txnId :: String
    , status :: String
    , payment :: PaymentResp
    , txnUuid :: String
    }

newtype DeviceReadyResponse =
  DeviceReadyResponse
    { status :: Boolean
    , message :: String
    }

newtype OrderStatusResp =
  OrderStatusResp
    { order_id :: Maybe String
    , status :: Maybe String
    , other_info :: Maybe Foreign
    , browserResponse :: Maybe Foreign
    , upiResponse :: Maybe Foreign
    }

newtype EmiPlansResp =
  EmiPlansResp
    { emiPlans :: Array EMIPlan }


newtype PaymentSourceResp =
  PaymentSourceResp
    { wallets :: Array StoredWallet
    , vpas :: Array StoredVPA
    , nbMethods :: Array StoredNB
    , cards :: Array StoredCard
    , lastUsedPaymentMethod ::  Maybe LastUsedPaymentMethod
    , merchantPaymentMethods :: Array MerchantPaymentMethod
    , appsUsed :: Array AppUsed
    , emandatePaymentMethods :: Maybe (Array MerchantPaymentMethod)
    , offers :: Array Offer
    , outages :: Maybe (Array OutagesStatus)
    }


newtype EligibilityResp =
  EligibilityResp
    {
     cards :: Maybe (Array CardEligibilityResp),
     wallets :: Maybe (Array WalletEligibilitySdkResp) }

newtype CardEligibilityResp = CardEligibilityResp {
  cardBin :: String
, cardAlias :: String
, checkType :: EligibilityCheckType
}


newtype EligibilityCheckType = EligibilityCheckType {
    otp :: Maybe OtpEligibility
  , vies :: Maybe Foreign
}

newtype OtpEligibility = OtpEligibility {
  eligible :: Boolean
}


newtype WalletEligibilitySdkResp =
  WalletEligibilitySdkResp
    { customer_id :: String
    , order_id :: Maybe String
    , paymentMethodsEligibility :: Array PaymentMethodsEligibility
    }

newtype ErrorResult =
  ErrorResult
    { status :: String
    , errorMessage :: String
    , errorCode :: String
    }

-- Add only required Keys in CreateWalletResponse & LinkWalletResponse
newtype CreateWalletResp =
  CreateWalletResp
    { wallet :: Maybe String
    , linked :: Maybe Boolean
    , id :: String
    }

newtype LinkWalletResp =
  LinkWalletResp
    { wallet :: Maybe String
    , linked :: Maybe Boolean
    , id :: String
    }

newtype DelinkWalletResp =
  DelinkWalletResp
    { wallet :: Maybe String
    , linked :: Maybe Boolean
    , id :: String
    }

newtype OutageAPIResp =
  OutageAPIResp
    { outages :: Array OutagesStatus }


newtype OutagesStatus =
  OutagesStatus
    { status :: String
    , startTime :: String
    , scope :: String
    , paymentInstrumentGroup :: String
    , gateway :: String
    , endTime :: String
    , bank :: String
    }



-- Instances

-- Payment Source Response
derive instance genericPaymentSourceResp :: Generic PaymentSourceResp _
derive instance newtypePaymentSourceResp :: Newtype PaymentSourceResp _
instance encodePaymentSourceResp :: Encode PaymentSourceResp where encode = defaultEncode
instance decodePaymentSourceResp :: Decode PaymentSourceResp where decode = defaultDecode

-- Order Status Response
derive instance genericOrderStatusResp :: Generic OrderStatusResp _
derive instance newtypeOrderStatusResp :: Newtype OrderStatusResp _
instance decodeOrderStatusResp :: Decode OrderStatusResp where decode = defaultDecode

-- SDK Response
derive instance genericSDKResponse :: Generic SDKResponse _
derive instance newtypeSDKResponse :: Newtype SDKResponse _
instance decodeSDKResponse :: Decode SDKResponse where decode = defaultDecode

-- EMI Plans Response
derive instance genericEmiPlansResp :: Generic EmiPlansResp _
derive instance newtypeEmiPlansResp :: Newtype EmiPlansResp _
instance decodeEmiPlansResp :: Decode EmiPlansResp where decode = defaultDecode

-- Create Wallet Response
derive instance genericCreateWalletResp :: Generic CreateWalletResp _
derive instance newtypeCreateWalletResp :: Newtype CreateWalletResp _
instance decodeCreateWalletResp :: Decode CreateWalletResp where decode = defaultDecode

-- Link Wallet Response
derive instance genericLinkWalletResp :: Generic LinkWalletResp _
derive instance newtypeLinkWalletResp :: Newtype LinkWalletResp _
instance decodeLinkWalletResp :: Decode LinkWalletResp where decode = defaultDecode

-- Delink Wallet Response
derive instance genericDelinkWalletResp :: Generic DelinkWalletResp _
derive instance newtypeDelinkWalletResp :: Newtype DelinkWalletResp _
instance decodeDelinkWalletResp :: Decode DelinkWalletResp where decode = defaultDecode

-- UPI Response
-- Eligibility Response
derive instance genericEligibilityResponse :: Generic EligibilityResp _
derive instance newtypeEligibilityResponse :: Newtype EligibilityResp _
instance decodeEligibilityResponse :: Decode EligibilityResp where decode = defaultDecode
instance encodeEligibilityResponse :: Encode EligibilityResp where encode = defaultEncode

derive instance genericEligibilityCheckType :: Generic EligibilityCheckType _
derive instance newtypeEligibilityCheckType :: Newtype EligibilityCheckType _
instance encodeEligibilityCheckType  :: Encode EligibilityCheckType where encode = defaultEncode
instance decodeEligibilityCheckType :: Decode EligibilityCheckType where decode = defaultDecode
instance showEligibilityCheckType :: Show EligibilityCheckType where show = encodeJSON

derive instance genericOtpEligibility :: Generic OtpEligibility _
derive instance newtypeOtpEligibility :: Newtype OtpEligibility _
instance encodeOtpEligibility  :: Encode OtpEligibility where encode = defaultEncode
instance decodeOtpEligibility :: Decode OtpEligibility where decode = defaultDecode
instance showOtpEligibility :: Show OtpEligibility where show = encodeJSON

derive instance genericCardEligibilityResp :: Generic CardEligibilityResp _
derive instance newtypeCardEligibilityResp :: Newtype CardEligibilityResp _
instance encodeCardEligibilityResp  :: Encode CardEligibilityResp where encode = defaultEncode
instance decodeCardEligibilityResp :: Decode CardEligibilityResp where decode = defaultDecode
instance showCardEligibilityResp :: Show CardEligibilityResp where show = encodeJSON


derive instance newtypeWalletEligibilitySdkResp :: Newtype WalletEligibilitySdkResp _
derive instance genericWalletEligibilitySdkResp :: Generic WalletEligibilitySdkResp _
instance encodeWalletEligibilitySdkResp :: Encode WalletEligibilitySdkResp where encode = defaultEncode
instance decodeWalletEligibilitySdkResp :: Decode WalletEligibilitySdkResp where decode = defaultDecode

-- Device Ready Response
derive instance genericDeviceReadyResponse :: Generic DeviceReadyResponse _
derive instance newtypeDeviceReadyResponse :: Newtype DeviceReadyResponse _
instance decodeDeviceReadyResponse :: Decode DeviceReadyResponse where decode = defaultDecode

-- OutageAPI Response
derive instance newtypeOutageAPIResp :: Newtype OutageAPIResp _
derive instance genericOutageAPIResp :: Generic OutageAPIResp _
instance encodeOutageAPIResp :: Encode OutageAPIResp where encode = defaultEncode
instance decodeOutageAPIResp :: Decode OutageAPIResp where decode = defaultDecode

-- OutageStatus
derive instance newtypeOutagesStatus :: Newtype OutagesStatus _
derive instance genericOutagesStatus :: Generic OutagesStatus _
instance encodeOutagesStatus :: Encode OutagesStatus where encode = defaultEncode
instance decodeOutagesStatus :: Decode OutagesStatus where decode = defaultDecode

-- instance makePaymentMethods :: RestEndpoint PaymentMethodsReq (APIResponse PaymentMethodsResp ErrorPayload) where
--   makeRequest reqBody@(PaymentMethodsReq req) headers = defaultMakeRequest_ GET ((baseUrl reqBody) <> "/merchants/"<> req.merchant_id <> "/paymentmethods") headers
--   decodeResponse = decodeJSON
