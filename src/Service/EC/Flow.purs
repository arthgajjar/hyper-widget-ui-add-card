module Service.EC.Flow where

import Prelude

import Control.Monad.Except (runExcept)
import Data.Either (Either(..), hush)
import Data.Maybe (Maybe)
import Effect (Effect)
import Effect.Aff (makeAff, nonCanceler)
import Engineering.Helpers.Commons (AffSuccess, liftFlow)
import Foreign.Generic (decodeJSON, encodeJSON)
import Service.EC.Types.Request (ECPayload, SDKPayload(..))
import Service.EC.Types.Response (SDKResponse)
import Presto.Core.Flow (Flow, doAff)

foreign import getRequestID :: Effect String

type MicroAPPInvokeSignature = String -> (AffSuccess String) -> Effect Unit

foreign import startECFlow' :: MicroAPPInvokeSignature

startECFlow :: ECPayload -> Flow (Maybe SDKResponse)
startECFlow ecPayload = do
  sdkPayload <- getSDKPayload ecPayload
  response <- doAff $ makeAff (\cb -> (startECFlow' (encodeJSON sdkPayload) (Right >>> cb) )*> pure nonCanceler)
  pure $ hush $ runExcept $ decodeJSON response


getSDKPayload :: ECPayload -> Flow SDKPayload
getSDKPayload payload = do
  requestId <- liftFlow $ getRequestID
  let service = "in.juspay.ec"
  pure $ SDKPayload { service, requestId, payload, client_id : "", customer_id: "", environment: "", merchant_id: "" }
