exports['getOS'] = function (x) {
	var userAgent = navigator.userAgent;
	if (userAgent) {
		if (userAgent.indexOf('Android') != -1) {
			return 'ANDROID';
		}
		if (userAgent.indexOf('iPhone') != -1) {
			return 'IOS';
		} else {
			return 'WEB';
		}
	} else {
		console.log('Null user agent defaulting to android ');
	}
};
exports["saveToLocal"]= function(a){
	return function(b){
		return function(c){
			JBridge.saveToLocal(a,b,c)
		}
	}
}

exports.logE = function (tag) {
	return function (a) {
		console.error(tag + " >>", a);
		return a;
	}
}

exports._hide_loader = function () {
	console.log("Hiding Loader => ", {event:"hide_loader"});
	JOS.emitEvent("java")("onEvent")(JSON.stringify({event:"hide_loader"}))()();
}

exports["loadFromLocal"]= function(a){
			var mid=window.__payload.merchant_id;
			return JBridge.loadFromLocal(mid+":"+a)


}


exports.logW = function (tag) {
	return function (a) {
		console.warn(tag + " >>", a);
		return a;
	}
}

exports["getCurrentTimeStamp"] = function () {
	var date = new Date().getTime();
	return date.toString();
}

exports["defaultOpExist"] = function(mid){
	if ( JBridge.loadFromLocal(mid+":"+"defOption") && JBridge.loadFromLocal(mid+":"+"defOptionType")){
		console.log("HERE")
		return true
	}
	return false

}

exports['getVoucher']= function(){
	var arr=[]
	if (window.offer_details){
		var offer = JSON.parse(window.offer_details)

		arr.push(offer.applied?offer.applied: "false");
		arr.push(offer.payment_method?offer.payment_method.toUpperCase(): "")
		arr.push(offer.payment_method_type? offer.payment_method_type.toUpperCase(): "")
		arr.push(offer.offer_code?offer.offer_code:"")
		arr.push(offer.amount? offer.amount :"")
		arr.push(offer.payment_card_type? offer.payment_card_type.toUpperCase():"")
		arr.push(offer.payment_card_issuer? offer.payment_card_issuer.toUpperCase(): "")


	}
	else{
		arr.push("false");
		arr.push("")
		arr.push("")
		arr.push("")
		arr.push("")
		arr.push("");
		arr.push("")
	}
	return arr;
}

exports['consoleLog'] = function (x) {
	console.log(x);
	return x;
};

exports["buildCSS"] = function(config) {
  const styleElem = document.getElementById('dynamicCSS_sidebar');
  var selectedColor = config.sideBar.selectedColor;
  var notSelectedColor = config.sideBar.notSelectedColor;
  var sideTabColor = config.sideBar.tabColor;
  var css = '';

  css += '.sidebar_wrapper .sidebarItemWrap .sidebarItem_object { background: ' + notSelectedColor + ' !important;}';
  css += '.sidebar_wrapper .sidebarItemWrap:hover .sidebarItem_object, .sidebar_wrapper .sidebarItemWrap.active .sidebarItem_object{ background: ' + selectedColor + ' !important;}';
  css += '.sidebar_wrapper .sidebarItemWrap .sidebarItem_border { background: transparent !important;}';
  css += '.sidebar_wrapper .sidebarItemWrap:hover .sidebarItem_border, .sidebar_wrapper .sidebarItemWrap.active .sidebarItem_border{ background: ' + sideTabColor + ' !important;}';

  if(styleElem.styleSheet) {
    styleElem.styleSheet.cssText = css;
  } else {
    styleElem.innerHTML = '';
    styleElem.appendChild(document.createTextNode(css));
  }

  console.log('hehe', config);
  return "made";
}

exports['getLabelHeight'] = function (text) {
	return function (fontName) {
		return function (fontSize) {
			return function (width) {
				return function (height) {
					if (window.__OS == 'ANDROID') {
						return 0;
					}
					var size = JBridge.sizeForLabelWithText(text, fontName, fontSize, width, height);
					return parseInt(JSON.parse(size)['height']);
				};
			};
		};
	};
};

exports['getLabelWidth'] = function (text) {
	return function (fontName) {
		return function (fontSize) {
			return function (width) {
				return function (height) {
					if (window.__OS == 'ANDROID') {
						return 0;
					}
					var size = JBridge.sizeForLabelWithText(text, fontName, fontSize, width, height);
					console.log('Size :', size);
					return parseInt(JSON.parse(size)['width']);
				};
			};
		};
	};
};

exports['screenWidth'] = function (x) {
	if (window.__OS == 'ANDROID') {
		if (window.__android_screenWidth) {
			return window.__android_screenWidth;
		} else {
			return (window.__android_screenWidth =
				JSON.parse(Android.getScreenDimensions()).width / JBridge.getPixels());
		}
	} else {
		if (window.__ios_screenWidth) {
			return window.__ios_screenWidth;
		} else {
			return (window.__ios_screenWidth = JSON.parse(Android.getScreenDimensions()).width);
		}
	}
};


exports["exitSDKPrevPage'"] = function (payload) {
	return function (code) {
		console.log("document.referrer " , document.referrer);
		if ( window.location === window.parent.location ) {
			location.href = document.referrer;
		} else {
			var msg = {
				event: "goToPreviousPage"
			};
			console.log("in else post message");
			parent.postMessage(msg, "*");
		}
	}
}


exports["exitSDK'"] = function (payload) {
	console.log("HERE", payload)
	var obj = JSON.parse(window.__payload.sdk_payload);
	var return_url = JSON.parse((window.__payload.order_details)).return_url;
	var txnUUid = window.__payload.txnuuid;
	var mid = JSON.parse(window.__payload.order_details).merchant_id;
	var payL=JSON.parse(payload);
	if (payL["errorCode"]=="API_FAILURE"){
		var msg = {
			event: "invokeErrorHandling",
			data: {
				error_message:"Something went wrong"
			}
		};
		window.parent.postMessage(msg, "*");
	}
	payL.return_url=return_url;
	payL.txnUUid=txnUUid;
	payL.mid=mid;
	payL=JSON.stringify(payL);

	return function (code) {
		if (obj.integration == "iframe" && obj.merchant_uri) {
			if (window.parent.location.href.indexOf("upiWaiting")!=1){
					if (window.__payload.environment == "sandbox"){
						var url = "https://sandbox.juspay.in/pay/finish/" + mid.toString() + "/" + txnUUid.toString();
						window.parent.location.href = url;
					}else{
					var url = "https://api.juspay.in/pay/finish/" + mid.toString() + "/" + txnUUid.toString();
					window.parent.location.href = url;
					}

			}
			var msg = {
				event: "ExitSDK",
				data: payL
			};
			window.parent.postMessage(msg, obj.merchant_uri);
			var p = JSON.parse(payload)
			var pay = (p.payload);
			if (pay.paymentInstrumentGroup == "UPI") {
				if (window.__payload.environment == "sandbox"){
					var url = "https://sandbox.juspay.in/pay/finish/" + mid.toString() + "/" + txnUUid.toString();
					window.parent.location.href = url;
				}else{
				var url = "https://api.juspay.in/pay/finish/" + mid.toString() + "/" + txnUUid.toString();
				window.parent.location.href = url;
				}
			}
			return;
		}
		try {
			var p = JSON.parse(payload)
			var pay = (p.payload);
			if (pay.paymentInstrumentGroup == "UPI" || p.error) {
				if (window.__payload.environment == "sandbox"){

					var url = "https://sandbox.juspay.in/pay/finish/" + mid.toString() + "/" + txnUUid.toString();
					window.parent.location.href = url;
				}else{
				var url = "https://api.juspay.in/pay/finish/" + mid.toString() + "/" + txnUUid.toString();
				window.parent.location.href = url;
				}
			}
		} catch (e) {
			if (window.__payload.environment == "sandbox"){
				var url = "https://sandbox.juspay.in/pay/finish/" + mid.toString() + "/" + txnUUid.toString();
				window.parent.location.href = url;
			}else{
			var url = "https://api.juspay.in/pay/finish/" + mid.toString() + "/" + txnUUid.toString();
			window.parent.location.href = url;
			}
		}

		console.log("EXIT SDK => ", payload);
		if (typeof JOS !== 'undefined') {
			JOS.finish(code)(payload)()
		}

	}
}

exports["integrationType"] = function () {
	var sdk = JSON.parse(window.__payload.sdk_payload)
	return sdk.integration;
}


exports["log"] = function (tag) {
	return function (a) {
		console.error(tag + " >>", a);
		return a;
	}
}

exports.dropLastChar = function (str) {
	console.log("Splice => ", str);
	str = str.slice(0, str.length - 1);
	console.log("Splice => ", str);
	return str;
}

exports.arrayJoin = function(arr) {
	return function(sep) {
		return arr.join(sep);
	}
}

exports.attachMerchantView = function (id) {
	return function () {
		//JBridge.attachMerchantView(parseInt(id));
	}
}
