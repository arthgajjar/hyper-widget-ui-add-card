module JBridge where

import Prelude

import Data.Either (Either(..))
import Data.Generic.Rep (class Generic)
import Data.Generic.Rep.Show (genericShow)
import Data.Newtype (class Newtype)
import Effect (Effect)
import Service.EC.Types.Request as ECRemoteTypes
import Effect.Aff (makeAff, nonCanceler)
import Effect.Class (liftEffect)
import Engineering.Helpers.Commons (AffSuccess, liftFlow)
import Payments.UPI.Utils (UpiCollectResp, UpiQRResp(..))
import Presto.Core.Types.Language.Flow (Flow, doAff)
import Type.Data.Boolean (kind Boolean)
foreign import logAny :: ∀ a . a -> Unit
foreign import getElemPostion :: String -> {x :: Int , y :: Int}
foreign import changeCursorTo :: String  -> Unit
foreign import updateLayoutCursor :: String -> String  -> Unit
foreign import updateViewPort :: ScreenSize -> Boolean
foreign import getText :: String -> String
foreign import getPaymentMethods :: String -> Array String
foreign import getFromConfig :: String -> Array String
foreign import getWalletConfigs :: Unit -> Array WalletConfig
foreign import getSessionAttribute' :: String -> String
foreign import detach :: String -> Unit
foreign import getScrollTop :: String -> Int
foreign import getKeyboardHeight :: Unit -> Int
foreign import getKeyboardDuration :: Unit -> Int
foreign import getNewIDWithTag' :: String -> String
foreign import startInHiddenWebView :: ECRemoteTypes.TxnResponse-> Unit
foreign import qrtxn :: UpiQRResp-> Unit
foreign import setScaleType :: String -> String -> Effect Unit
foreign import getUrlFromPayload :: ECRemoteTypes.TxnResponse -> String
foreign import getStatusBarHeight :: Unit -> Int
-- takes imageViewId (firstParam) & scaleType (SecondParam)
-- refer here https://developer.android.com/reference/android/widget/ImageView.ScaleType#summary for scaleType values
-- pass exact scaleType values

foreign import callMerchantAndWait :: ∀ a. (String -> Effect Unit) -> String -> a -> Effect Unit
foreign import getApprovalResult :: ∀ a. a -> Boolean

getNewIDWithTag :: Tag -> String
getNewIDWithTag tag = getNewIDWithTag' $ show tag

data Tag = OTP | VPA | MORE | SEARCHBOX | MOBILE_NUM | MERCHANT_VIEW | SAVED_CARD String | MSCR_ONE | MSCR_TWO | EMI_PLAN_ID String


derive instance genericTag :: Generic Tag _
instance showTag :: Show Tag
    where show = genericShow

type ScreenSize = { height :: Int, width :: Int}

type WalletConfig = {
    wallet :: String
    , wait_for :: Int
    , otp_length :: Int
}

newtype CardDetails = CardDetails {
    card_type :: String,
    valid :: Boolean,
    luhn_valid :: Boolean,
    length_valid :: Boolean,
    cvv_length :: Array Int,
    supported_lengths :: Array Int
}

derive instance cardDetailsNewtype :: Newtype CardDetails _
defaultValidatorOutput :: CardDetails
defaultValidatorOutput = CardDetails $ {
    card_type : "undefined",
    valid : false,
    luhn_valid : false ,
    length_valid : false,
    cvv_length : [3],
    supported_lengths : [16]
}
foreign import getCardValidation :: String -> CardDetails
foreign import hideKeyboard :: Unit -> Unit
foreign import requestKeyboardHide :: Effect Unit
foreign import showKeyboard :: String -> Unit
foreign import requestKeyboardShow :: String -> Effect Unit


-- | pass the ScrolView ID + ChildView Id to bring the childView in focus
foreign import bringToFocus :: String -> String -> Boolean
foreign import requestFocus :: String -> Boolean
foreign import attach' :: String -> String -> String -> Effect Boolean

attach :: String -> String -> String -> Flow Boolean
attach eventListener args callbackId = (liftFlow ( attach' eventListener args callbackId))
isCardValid :: CardDetails -> Boolean
isCardValid (CardDetails card)= card.valid

getcardType :: CardDetails -> String
getcardType (CardDetails card)= card.card_type
-- | UPI Interface
foreign import getToken :: String -> String -> String -> Effect String
foreign import init ::  (AffSuccess String) -> String -> String -> String -> Effect Unit
foreign import sendSMS ::  (AffSuccess {status :: String}) -> String -> Effect Unit
foreign import setMPIN ::  (AffSuccess {status :: String, response :: {status :: String}}) -> String -> Effect String
foreign import checkBalance :: (AffSuccess String) -> String -> Effect String
foreign import transaction :: (AffSuccess {status :: String, statusCode :: String, response :: {status :: String}}) -> String -> Effect Unit
foreign import collectApprove :: (AffSuccess String) -> String -> Effect String
foreign import encrypt :: String -> String -> String
foreign import decrypt :: String -> String -> String
foreign import openWhatsapp :: String -> Effect Unit
foreign import toast :: String -> Effect Unit


-- | SIMPL Wallet SDK
foreign import _getSimplFingerPrint :: String -> String -> String -> (AffSuccess String) -> Effect String
foreign import _doesSimplExist :: Effect Boolean

doesSimplExist :: Flow Boolean
doesSimplExist = doAff do liftEffect $ _doesSimplExist

getSimplFingerPrint :: String -> String -> String -> Flow String
getSimplFingerPrint mobile email mid = doAff do
  makeAff (\cb ->
    (_getSimplFingerPrint mobile email mid (Right >>> cb))
    *> pure nonCanceler
  )
