module PaymentPageConfig (
    getOfferOptions,
    getConfig,
    getConfigResponse,
    getSavedConfig,
    getScreenTransitionConfig,
    getSavedOptionWeb,
    getPopularBanks,
    getHighlight,
    getPaymentOptions,
    getQRData,
    getDisabledCardNetworks,
    getWrapperColor,
    savedVpasVisibility,
    getDisabledCardTypes,
    getBlockedCardNumbers,
    getDisabledWallets,
    getDisableUpiApps,
    isUPIQR,
    isCardLayout,
    getMoreOptions,
    getSaveCardToolTip,
    getDisabledBanks,
    getScreenTransitionCurve,
    getScreenTransitionDuration,
    getPrimaryFont,
    defaultValue,
    getUseLocalIcons,
    getCVVToolTip,
    getDefaultOptionSelect,
    getHighlightViewType,
    getWalletViewType,
    getNBViewType,
    getFontSize,
    getUpiViewType,
    getWrapperPaddingTop,
    getIconSize,
    getRadioButtonPosition,
    getToolbarIconLocation,
    collectUpiWithGodel,
    showLineSeparator,
    getGridFontSize,
    getLocalIcon,
    getGridIconSize,
    getRadioIconSize,
    getSpacing,
    getListItemHeight,
    getShowUPIForGuest,
    getBackgroundColor,
    updatePOVisibility,
    getLogoRadius,
    selectedRadioBBIcon,
    selectedRadioBB,
    getShowTopBar,
    getLogoMargin,
    getLogoMarginValue,
    getLogoBackground,
    findElementInRecord,
    findElementInRecord',
    getaddCardFontColor,
    getRedirectUPI,
    getFontColor,
    getDisabledFontColor,
    getSectionHeaderSize,
    getSectionHeaderFontColor,
    getSectionHeaderFontWeight,
    getToolbarColor,
    ifHighlightVisible,
    ifCombinedWallets,
    getCornerRadius,
    getCVVCornerRadius,
    getWrapperRadius,
    getWrapperPadding,
    getCvvBoxType,
    ifMobileNumberVerify,
    ifOfferVisible,
    getTextGravity,
    getContainerPadding,
    getButtonText,
    getVodaConfigResponse,
    getEnabledWallets,
    getCustomConfigResponse,
    ifCurvedPaymentOption,
    getPaymentOptionShadow,
    ifSeperatedSections,
    getSideBarTabs,
    isInputBoxed,
    getCurvedWrapperColor,
    getGuestUserAllowed,
    getUseBtnGrad,
    getBtnGradient,
    getTextBoxConfig,
    getButtonConfig,
    getSideBarConfig,
    getGradientType,
    getGradientAngle,
    getGradientColors,
    getSideBarWidth,
    getMainBackground,
    getLineSeparatorColor,
    getIconBackground,
    getIconColor,
    getTick,
    getCardIcon, --check
    getIconClose,
    getUpiIcon,
    getJuspayLogoIcon,
    getMobileTopIcon,
    getWalletIcon,--check
    getNetBankingIcon, --check
    getIcBankIcon,  --check
    getGreenPlusIcon, --check
    getMasterCardIcon,
    getVisaIcon,
    getJuspayIcon,
    getAxisBankIcon,
    getPCIIcon,
    getSecuredMasterCardIcon,
    getVerifiedVisaIcon,
    getButtonWidth,
    getIcPaytmIcon,

    getCheckBoxForSavedCard,
    getHideTopToolBar,
    getHideOrderSummary,
    getUpi1, --check
    getUpi2, --check
    getUpi3, --check
    getFreechargeIcon,
    getAddCardIcon,
    getBackIcon1,
    upiWaitingLogo,
    getUseBackgroundImage,
    getBackIcon2,
    getQuestionIcon,
    configFunction,
    getMainHeaderConfig,
    getPPBorderContainerPadding,
    getAddCardConfig,
    getPPBorderContainerBGColor,
    getPPBorderExists,
    getErrorMessage,
    disableForOutage,
    getOutageMessage,
    getOutageMessageForPopularNB,
    nbLayoutVertical,
    getUPIHomeQRIcon,
    module Remote.Types
) where

import Prelude

import Control.Monad.Except (runExcept)
import Data.Array (elem, elemIndex, filter, length, updateAt, (!!))
import Data.Array (filter, length, (!!))
import Data.Either (Either(..))
import Data.Foldable (foldl)
import Data.Int (fromString, toNumber)
import Data.Int as IntUtils
import Data.Maybe (Maybe(..), fromMaybe)
import Data.Number as NumUtils
import Data.String as String
import Data.String.Yarn (unwords)
import Effect (Effect)
import Effect.Aff (nonCanceler, makeAff)
import Data.String (Pattern(..), split)
import Engineering.Helpers.Commons (getOS)
import Utils (logW)
import Presto.Core.Utils.Encoding (defaultDecodeJSON)
import PrestoDOM (Margin(..), Padding(..), cornerRadius, letterSpacing)
import Remote.Types (AddCardConfig(..), Button(..), ButtonGradient(..), CardInfo(..), ConfigPayload(..), FontConfig(..), MoreOption(..), OutageConfig(..), PPBorderContainer(..), PPConfigReq(..), PaymentOption(..), PaymentOptionError(..), PaymentOptions(..), Saved(..), ScreenTransition(..), SideBar(..), TextBox(..), Toolbar(..), TopBar(..), VisiblePayOption, Wrapper(..), WrapperPadding(..))
import Remote.Types

foreign import getQRData :: String -> String
foreign import upiWaitingLogo :: Unit -> String
foreign import someFFI :: (String -> Effect Unit) -> Effect Unit
foreign import unwordWithSeparator :: Array String ->String -> String
foreign import getCustomConfig :: Unit -> String
foreign import getTick :: String -> String
foreign import getUPIHomeQRIcon :: String -> String -> String
foreign import getIconClose :: String -> String
foreign import getJuspayLogoIcon :: String -> String
foreign import getMobileTopIcon :: String -> String -> String -> String

foreign import getCardIcon :: String -> String
foreign import getUpiIcon :: String -> String
foreign import getWalletIcon :: String -> String
foreign import getNetBankingIcon :: String -> String

foreign import getIcBankIcon :: String -> String
foreign import getGreenPlusIcon :: String -> String -> String
foreign import getMasterCardIcon :: String -> String
foreign import getVisaIcon :: String -> String
foreign import getJuspayIcon :: String -> String
foreign import getAxisBankIcon :: String -> String
foreign import getPCIIcon :: Unit -> String
foreign import getSecuredMasterCardIcon :: Unit -> String
foreign import getVerifiedVisaIcon :: Unit -> String
foreign import getIcPaytmIcon :: String -> String
foreign import getCheckBoxForSavedCard :: String -> String
foreign import getUpi1 :: String -> String->String->String
foreign import getUpi2 :: String -> String->String->String
foreign import getUpi3 :: String -> String->String->String
foreign import getFreechargeIcon :: String -> String
foreign import getAddCardIcon :: String ->String
foreign import getBackIcon1  :: String -> String
foreign import getBackIcon2  :: String -> String
foreign import getQuestionIcon :: String -> String
foreign import selectedRadioBBIcon :: Unit -> String

foreign import getLocalIcon :: String -> String -> String
configFunction = makeAff (\cb -> someFFI (Right >>> cb) *> pure nonCanceler)

foreign import getIdeaConfig :: Effect String
foreign import getCureFitConfig :: Effect String
foreign import getJiosaavnConfig :: Effect String
foreign import getHyperBetaSDKConfig :: Effect String
foreign import getZee5Config :: Effect String


getConfig' :: String -> Effect String
getConfig' "curefit" = getCureFitConfig
getConfig' "idea" = getIdeaConfig
getConfig' "jiosaavn" = getJiosaavnConfig
getConfig' "zee5" = getZee5Config
getConfig' _ = getHyperBetaSDKConfig

getCVVToolTip :: ConfigPayload -> Boolean
getCVVToolTip (ConfigPayload confPay)=confPay.cvvToolTip == "true"

getSaveCardToolTip :: ConfigPayload -> Boolean
getSaveCardToolTip (ConfigPayload confPay)=confPay.saveCardToolTip == "true"

getButtonWidth :: ConfigPayload -> Int
getButtonWidth (ConfigPayload configPayload) =
                    if getOS unit == "WEB"
                        then fromMaybe 0 $ IntUtils.fromString button.width
                        else fromMaybe 0 $ IntUtils.fromString ""
                    where
                      (Button button) =  configPayload.button

getDefaultOptionSelect :: ConfigPayload -> Boolean
getDefaultOptionSelect (ConfigPayload confPay)=confPay.defaultOptionSelect == "true"
nbLayoutVertical :: ConfigPayload -> Boolean
nbLayoutVertical (ConfigPayload confPay)=confPay.nbLayout == "vertical"
selectedRadioBB :: ConfigPayload -> String
selectedRadioBB (ConfigPayload confPay)=confPay.iconUrlNonSelected
isCardLayout :: ConfigPayload -> Boolean
isCardLayout (ConfigPayload confPay)=confPay.isCardLayout == "true"

savedVpasVisibility :: ConfigPayload -> Boolean
savedVpasVisibility (ConfigPayload confPay)=confPay.showSavedVPAs == "true"
defaultValue :: ConfigPayload
defaultValue = ConfigPayload { primaryColor : "#008080"
                , language : Just "english"
                , showSavedVPAs : "false"
                , isCardLayout : "false"
                , upiQREnable : "false"
                , useLocalIcons : "false"
                , errorTextColor : "#dd5c2f"
                , showTopBar: "false"
                , radioButtonPosition : "left"
                , primaryFont : "Arial"
                , modalView : "false"
                , defaultOptionSelect : "false"
                , redirectUPI :"false"
                , nbLayout : ""
                , disabledFontColor : ""
                , iconUrlNonSelected : ""
                , cvvToolTip : ""
                , saveCardToolTip : ""
                , showUPIForGuest : ""
                , offerOptions : []
                , uiCard:
                  UICard
                    { translation: ""
                    , cornerRadius: "5.0"
                    , horizontalPadding : "12"
                    , verticalPadding : "16"
                    , attachPOHeaders: Nothing
                    , color : "#ffffff"
                    }
                , containerAttribs:
                  ContainerAttribs
                    { horizontalSpacing: "15" -- is equal to conatinerPadding
                    , verticalSpacing: "0" -- decide for BB
                    , sectionSpacing: "0" -- decide for BB
                    }
                , inputField :
                  InputField
                    { "type" : "underlined"
                    , useMaterialView : "false"
                    , cornerRadius : Just ""
                    , focusColor : ""
                    , textPadding : ""
                    , background : "#ffffff"
                    , fontStyle : "SemiBold"
                    , labelFontFace : "Regular"
                    , labelFontSize : "12"
                    , labelFontColor : "#3e4154"
                    }
                , upiCollectWithGodel : "false"
                , useQuickPayRanking : "false"
                , gridProps:
                   GridProps
                     { useTick : "true"
                     , useStroke : "true"
                     , itemSize : "10"
                     , gridSelectedStroke : "2,#dcdcdc"
                     , gridLogoStroke : "1,#dcdcdc"
                     , useButtonForSelection : "true"
                     , gridViewBackground : "#ffffff"
                     , horizontalFade: "true"
                     , fadingEdgeLength: "0"
                     , strokeCornerRadius : "4.0"
                     , gridFontSize : "14"
                     , gridIconSize : "38"
                     }
                 , mandateViewProps:
                     MandateViewProps
                       { textColor : "#898989"
                       , backgroundColor : "#f8f8f8"
                       , cornerRadius : "4.0"
                       , padding : { left : "" , right : "" , vertical : ""  }
                       , textSize : "12"
                       }
                 , offerViewProps:
                     OfferViewProps
                       { textColor : "#898989"
                       , backgroundColor : "#f8f8f8"
                       , cornerRadius : "4.0"
                       , padding : { left : "" , right : "" , vertical : ""  }
                       , textSize : "12"
                       }
                 , topRowVisibility: "true"
                 , widgets:
                   Just
                     $ Widgets
                         { allowed: [ "paymentPage" ]
                         , primary: "paymentPage"
                         }
                 , tertiaryFontColor : "#008080"
                 , outageViewProps:
                   OutageViewProps
                     { showOutageView : "true"
                     , textColor : "#D84954"
                     , backgroundColor : "#f8f8f8"
                     , cornerRadius : "4.0"
                     , padding : { left : "" , right : "" , vertical : ""  }
                     , textSize : "12"
                     , outageMessage : "Experiencing low success rates for"
                     , restrictPayment : "false"
                     }
                 , secondaryButton :
                   SecondaryButton
                     { useNavigationArrow : "false"
                     , width : "WRAP_CONTENT"
                     , height : "24"
                     , buttonAlignment : "LEFT" -- LEFT || CENTER || RIGHT
                     , buttonBackground : ""
                     , buttonCornerRadius : ""
                     , textColor : "#000000"
                     , textSize : "15"
                     , fontFace : "bold"
                     }
                 , labelButton :
                   LabelButton
                     { color : "#eff0f4"
                     , textColor : "#000000"
                     , textFontFace : "Regular"
                     , cornerRadius : "3.0"
                     , textSize : "12"
                     , height : "30"
                     }
                 , mandateInstruments : []
                 , highlightMessage :
                   HighlightMessage
                     { textColor : "#000000"
                     , backgroundColor : "#f0f0f0"
                     , padding : { vertical : "10", horizontal : "14" }
                     , textFontFace : "Regular"
                     }
                 , headerProps:
                   HeaderProps
                     { size : "12"
                     , color : "#3e4154"
                     , fontFace : "Semibold"
                     , bottomMargin : "10"
                     }
                , attachPOHeaders : "true"

                , toolbarIconLocation : "left"
                , toolbar : Toolbar { back : "VISIBLE"
                                    , pageTitle : "ERROR"
                                    , textColor : "#ffffff"
                                    , color : "#ffffff"
                                    , toolbarIcon : "arrow"
                                    , toolbarIconColor : "black"
                                    , textFont : ""
                                    , textSize : ""
                                    }
                , screenTransition : ScreenTransition { duration : "200"
                                        , curve : ["0.1", "0.4", "0.4", "0.9"]
                                        }
                , popularBanks : ["NB_AXIS"]
                , highlight : [
                        (PaymentOptions
                            { group: "others"
                            , po: "upi"
                            , visibility: "VISIBLE"
                            , onlyEnable : (Just ["GOOGLEPAY", "PAYTM"])
                            , onlyDisable: Nothing
                            , isLast : "false"
                            }
                        )]
                , saved : Saved { saved : "VISIBLE"
                                , preffered : "VISIBLE"
                                , otherSaved : "VISIBLE"
                                }
                , paymentOptions :
                        [ PaymentOptions
                            { group: "others"
                            , po: "wallets"
                            , visibility: "visible"
                            , onlyEnable : Nothing
                            , onlyDisable: (Just ["GOOGLEPAY", "PAYPAL", "OLAPOSTPAID"])
                            , isLast : "false"}
                        , PaymentOptions
                            { group: "others"
                            , po: "nb"
                            , visibility: "visible"
                            , onlyEnable : Nothing
                            , onlyDisable : (Just ["NB_DUMMY", "NB_SBM", "NB_SBT", "NB_CANR"])
                            , isLast : "false"}
                        , PaymentOptions
                            { group: "others"
                            , po: "upi"
                            , visibility: "visible"
                            , onlyEnable : Nothing
                            , onlyDisable : (Just ["SHAREit", "Airtel", "WhatsApp"])
                            , isLast : "false"}
                        , PaymentOptions
                            { group: "others"
                            , po: "cards"
                            , visibility: "visible"
                            , onlyEnable : Nothing
                            , onlyDisable : Nothing
                            , isLast : "false"}--(Just ["VISA", "CREDIT", "5500000000000004"]) }
                        ]
                , moreOption : MoreOption
                                { visibility : "invisible"
                                , icon : "null"
                                , name : "null"
                                , view : {
                                        toolbar : Toolbar { back : "null" , pageTitle : "null", textColor : "#ffffff", color : "#ffffff", toolbarIcon : "arrow", toolbarIconColor : "black", textFont : "", textSize : ""}
                                    ,   content : "null"
                                    ,   footer : "null"
                                    ,   action : "null"
                                    }
                                }
                , highlightViewType : "grid"
                , expandedWalletView : "false"
                , expandPopularNBView : "false"
                , expandUpiView : "false"
                , iconSize : "32"
                , fontSize : "15"
                , lineSeparator : "true"
                , gridFontSize : "12"
                , gridIconSize : "32"
                , checkboxSize : "22"
                , checkboxSizeMobile : "16"
                , addCardFontColor: "#323640"
                , spacing : "24"
                , listItemHeight : "59"
                , fontColor : "#3e4154"
                , backgroundColor : "#f4f4f5"
                , backgroundColorMobile : "#f4f4f5"
                , toolbarColor : "#ffffff"
                , combineWallets : "false"
                , cornerRadius : "12.0"
                , wrapperRadius : "0.0"
                , wrapper : Wrapper {
                    wrapperPadding : WrapperPadding {
                        wrapperPaddingHorizontal : "0"
                        , wrapperPaddingTop : "8"
                        , wrapperPaddingBottom : "0"
                    }
                }
                , cvvInputBoxType : "boxed"
                , cvvInputBoxRadius : ""
                , verifyMobile : "true"
                , offers : "gone"
                , containerPadding : "0"
                , containerPaddingMobile : "15"
                , buttonText : Just "Pay"
                , useCurvedPaymentOption : "true"
                , paymentOptionShadow : "false"
                , useSeperatedSections : "false"
                , paymentOptionFontFace : "Regular"
                , searchBoxCornerRadius : ""
                , strechedConfirmButton : ""
                , showlineSepOnSeprateSections : ""
                , hideLastDivider : "true"
                , textGravity : ""
                , sideBarTabs : []
                , savedOptionsWeb :""
                , topBar : TopBar
                    {
                        backgroundColor : "",
                        height : "70",
                        visibility : "true",
                        primaryFontSize : "",
                        secondaryFontSize : "",
                        summaryTitleOne : "",
                        summaryTitleTwo :"",
                        summaryTextOne : "",
                        summaryTextTwo : "",
                        fontColor : "#323640",
                        font : ""
                    }
                , topBarMobile : TopBar
                    {
                        backgroundColor : "",
                        height : "70",
                        visibility : "true",
                        primaryFontSize : "",
                        secondaryFontSize : "",
                        summaryTitleOne : "",
                        summaryTitleTwo :"",
                        summaryTextOne : "",
                        summaryTextTwo : "",
                        fontColor : "#323640",
                        font : ""
                    }
                , sideBarNav : ""
                , sideBarNavDirection : ""
                , isInputBoxed : ""
                , verifyVPA : "true"
                , guestUserAllowed: "false"
                , wrapperColor : "#323640"
                , curvedWrapperColor : "#ffffff"
                , isButtonGradient : "false"
                , buttonGradient : ButtonGradient
                         {
                            gradtype : "",
                            angle : "",
                            values : []
                        }
                 , mainBackground : ""
                 , lineSeparatorColor : ""
                 , addIconBackground : "#ffffff"
                 , addIconColor : "#363636"
                 , iconUrl : ""
                 , backIcon : ""
                 , backIconMobile : ""
                 , timerColor : ""
                 , logoMargin : ""
                , logoBackground : ""
                , logoRadius : ""
                , useBackgroundImage: "false"
                , hideOrderSummary : "false"
                , hideTopToolBar : "false"
                , internationalCardsAllowed : "true"
                , amountBar:
                  AmountBar
                    { amountBarData : []
                    , placeAmountAtRight : "true"
                    , attachOnTop : "true"
                    , attachOnAllScreens : "true"
                    , visibility : "visible"
                    , verticalPadding : "5"
                    , translation : "4.0"
                    , amountTextSize : "16"
                    , color : "#ffffff"
                    , addDivider : "false"
                    , dividerColor : "#efefef"
                    }
                , button : Button {
                      translation: ""
                        , colorActive : "#008080"
                        , alphaActive : "1"
                        , colorInactive : "#008080"
                        , alphaInactive : "0.3"
                        , height : "40"
                        , atBottom: "false"
                        , width :"120"
                        , cornerRadius : "10"
                        , text: Just "Pay"
                        , color: "#e56167"
                        , background: "#eff0f4"
                        , changeText: "false"
                    }
                , textBox : TextBox {
                        activeColor : "#999999"
                        , inactiveColor : "#d3d3d3"
                        , errorColor : "#e60000"
                        , borderWidth : "2"
                        , maxWidth : "399"
                        , height : "45"
                    }
                , sideBar : SideBar {
                    selectedColor :"#323640"
                    , notSelectedColor: "#ffffff"
                    , iconVisibility : "true"
                    , selectedFontColor : "#323640"
                    , notSelectedFontColor : "#323640"
                    , tabColor : "#323640"
                    , fontSize :"18"
                    , tabHeight :"100"
                    , width : "250"
                    , selectedFontWeight : "bold"
                    , notSelectedFontWeight : "regular"
                    , border : ""
                    , tabBorder : ""
                    , tabPaddingHorizontalOuter : "0"
                    , tabPaddingVerticalOuter : "0"
                    , tabCornerRadius : "0"

                }
                , sectionHeader : FontConfig {
                    fontColor : "#008080"
                    , fontSize : "12"
                    , fontWeight : "bold"
                    , letterSpacing : "0.0"
                }
                , mainHeader : FontConfig {
                    fontColor : "#008080"
                    , fontSize : "19"
                    , fontWeight : "regular"
                    , letterSpacing : "0.0"
                }
                , paymentOption : PaymentOption {
                    primary : FontConfig {
                        fontColor : "#008080"
                        , fontSize : "19"
                        , fontWeight : "regular"
                        , letterSpacing : "0.0"
                    }
                    , secondary : FontConfig {
                        fontColor : "#008080"
                        , fontSize : "19"
                        , fontWeight : "regular"
                        , letterSpacing : "0.0"
                    }
                    , tertiary : FontConfig {
                        fontColor : "#008080"
                        , fontSize : "19"
                        , fontWeight : "regular"
                        , letterSpacing : "0.0"
                    }
                    , border : ""
                    , paddingVertical : "0"
                    , paddingHorizontal : "0"
                    , paddingSecondaryTop : "0"
                    , paddingTertiaryTop : "0"
                    , paddingBottom : "0"
                    , paddingOuterHorizontal : "0"
                    , paddingOuterVertical : "0"
                    , shadow : "0.0"
                }
                , ppBorderContainer : PPBorderContainer {
                    paddingTop : "0"
                    , paddingBottom : "0"
                    , paddingHorizontal : "0"
                    , backgroundColor : "#ffffff"
                }
                , paymentOptionErrors : []
                , outageConfig : OutageConfig{
                    outageType : "none"
                    , message : ""
                }
                , addCardConfig : AddCardConfig{
                    cardInBoxIcon : "false"
                    , cardInputWidth : "399"
                    , checkBoxMessage : ""
                    , showSecurityIcons : "false"
                }
                -- , language: "english"
            }

getAddCardConfig :: ConfigPayload -> AddCardConfig
getAddCardConfig (ConfigPayload config) =
    config.addCardConfig

getPPBorderContainerPadding :: ConfigPayload -> Padding
getPPBorderContainerPadding (ConfigPayload config) =
    (Padding mh mt mh mb)
    where
        (PPBorderContainer ppBorderContainer) = config.ppBorderContainer
        mh= fromMaybe 0 (fromString ppBorderContainer.paddingHorizontal)
        mt= fromMaybe 0 (fromString ppBorderContainer.paddingTop)
        mb= fromMaybe 0 (fromString ppBorderContainer.paddingBottom)

getPPBorderContainerBGColor :: ConfigPayload -> String
getPPBorderContainerBGColor (ConfigPayload config) =
    ppBorderContainer.backgroundColor
    where
        (PPBorderContainer ppBorderContainer) = config.ppBorderContainer

getPPBorderExists :: ConfigPayload -> Boolean
getPPBorderExists (ConfigPayload config) =
    if (mh == 0 && mt == 0 && mb == 0)
        then false
        else true
    where
        (PPBorderContainer ppBorderContainer) = config.ppBorderContainer
        mh= fromMaybe 0 (fromString ppBorderContainer.paddingHorizontal)
        mt= fromMaybe 0 (fromString ppBorderContainer.paddingTop)
        mb= fromMaybe 0 (fromString ppBorderContainer.paddingBottom)

getRadioButtonPosition :: ConfigPayload -> String
getRadioButtonPosition (ConfigPayload conf) = conf.radioButtonPosition
getRedirectUPI :: ConfigPayload -> Boolean
getRedirectUPI (ConfigPayload configPayload)= configPayload.redirectUPI == "true"

updatePOVisibility :: String -> Boolean -> Array PaymentOptions -> Array PaymentOptions
updatePOVisibility poType hidePO paymentOptions =
     if hidePO
         then fromMaybe paymentOptions do
             payOp@(PaymentOptions po) <- findElementInRecord' poType paymentOptions
             index <- elemIndex payOp paymentOptions
             updateAt index (PaymentOptions $ po {visibility = "GONE"}) paymentOptions
         else
            paymentOptions
findElementInRecord' :: String -> Array PaymentOptions -> Maybe PaymentOptions
findElementInRecord' poType paymentOptions =
    filter (\(PaymentOptions payOpt) -> payOpt.po == poType) paymentOptions !! 0
isUPIQR :: ConfigPayload -> Boolean
isUPIQR (ConfigPayload configPayload) = configPayload.upiQREnable == "true"
getShowTopBar :: ConfigPayload -> Boolean
getShowTopBar (ConfigPayload configPayload) = configPayload.showTopBar == "true"
getShowUPIForGuest :: ConfigPayload -> Boolean
getShowUPIForGuest (ConfigPayload configPayload)= if configPayload.showUPIForGuest =="true"
                                                        then true
                                                        else false
getLogoMargin :: ConfigPayload -> Margin
getLogoMargin (ConfigPayload configPayload)=do
                if configPayload.logoMargin ==""
                    then Margin 0 0 0 0
                    else Margin m m m m
                where
                  m = fromMaybe 0 (fromString configPayload.logoMargin)

getUseLocalIcons ::ConfigPayload -> Boolean
getUseLocalIcons (ConfigPayload confPay) = confPay.useLocalIcons == "true"

getLogoMarginValue :: ConfigPayload -> Int
getLogoMarginValue (ConfigPayload configPayload)=do
                if configPayload.logoMargin ==""
                    then 0
                    else m
                where
                  m = fromMaybe 0 (fromString configPayload.logoMargin)


getConfig :: String -> Effect ConfigPayload
getConfig client_id = do
  let clientId = fromMaybe "" $ split (Pattern "_") client_id !! 0
  config <- getConfig' clientId
  let response = (runExcept $ defaultDecodeJSON config :: _ ConfigPayload)
  pure
    $ case response of
        Right res -> res
        Left err -> do
          let _ = logW "ConfigPayload decode failed: " err
          defaultValue

getLogoBackground :: ConfigPayload -> String
getLogoBackground (ConfigPayload configPayload)=configPayload.logoBackground

getLogoRadius :: ConfigPayload -> Number
getLogoRadius (ConfigPayload configPayload)=do
                if configPayload.logoRadius ==""
                    then 0.0
                    else m
                where
                  m= toNumber $ fromMaybe 0 (fromString configPayload.logoRadius)

isInputBoxed :: ConfigPayload -> String
isInputBoxed (ConfigPayload conPayload) = conPayload.isInputBoxed
getConfigResponse :: ConfigPayload
getConfigResponse =
    let response = (runExcept $ defaultDecodeJSON (getCustomConfig unit) :: _ ConfigPayload) in
    case response of
        Right x -> x
        Left err -> defaultValue

getSideBarTabs :: ConfigPayload -> Array String
getSideBarTabs (ConfigPayload configPayload) = configPayload.sideBarTabs --Add some logic here
getMainBackground :: ConfigPayload -> String
getMainBackground (ConfigPayload configPayload) = configPayload.mainBackground

getLineSeparatorColor :: ConfigPayload -> String
getLineSeparatorColor (ConfigPayload configPayload) = configPayload.lineSeparatorColor

getIconBackground :: ConfigPayload -> String
getIconBackground (ConfigPayload configPayload) = configPayload.addIconBackground

getIconColor :: ConfigPayload -> String
getIconColor (ConfigPayload configPayload) = configPayload.addIconColor

getVodaConfigResponse :: ConfigPayload
getVodaConfigResponse =
    let response = (runExcept $ defaultDecodeJSON (getCustomConfig unit) :: _ ConfigPayload) in
    case response of
        Right res -> res
        Left err -> defaultValue

getCustomConfigResponse :: String-> ConfigPayload
getCustomConfigResponse confPayload = let
    response = (runExcept $ defaultDecodeJSON (confPayload) :: _ ConfigPayload) in
    case  response of
        Right res -> res
        Left err -> defaultValue

getSavedConfig :: ConfigPayload -> Saved
getSavedConfig (ConfigPayload configPayload) = configPayload.saved

getPrimaryFont :: ConfigPayload -> String
getPrimaryFont (ConfigPayload configPayload) = configPayload.primaryFont

getPaymentOptions :: ConfigPayload -> Array PaymentOptions
getPaymentOptions (ConfigPayload configPayload) = configPayload.paymentOptions

getPopularBanks :: ConfigPayload -> Array String
getPopularBanks (ConfigPayload configPayload) = configPayload.popularBanks

getHighlight :: ConfigPayload -> Array PaymentOptions
getHighlight (ConfigPayload configPayload) = configPayload.highlight

getScreenTransitionConfig :: ConfigPayload -> ScreenTransition
getScreenTransitionConfig (ConfigPayload configPayload) = configPayload.screenTransition

getSavedOptionWeb :: ConfigPayload -> Boolean
getSavedOptionWeb (ConfigPayload configPayload) = (configPayload.savedOptionsWeb == "true")


getScreenTransitionDuration :: ConfigPayload -> Int
getScreenTransitionDuration (ConfigPayload configPayload) =
    let ScreenTransition screenTransitionConfig = getScreenTransitionConfig (ConfigPayload configPayload) in
        fromMaybe 200 $ IntUtils.fromString screenTransitionConfig.duration

getScreenTransitionCurve :: ConfigPayload -> Array Number
getScreenTransitionCurve (ConfigPayload configPayload) =
    let ScreenTransition screenTransitionConfig = getScreenTransitionConfig (ConfigPayload configPayload)
        beizerArray = screenTransitionConfig.curve in
        map (\(a) -> fromMaybe 0.0 $ NumUtils.fromString a) beizerArray

findElementInRecord :: String -> ConfigPayload -> Maybe PaymentOptions
findElementInRecord poType (ConfigPayload configPayload) = do
    let paymentOptionsArray = configPayload.paymentOptions
    let filterArray= filter (\(PaymentOptions payOpt) -> payOpt.po == poType) paymentOptionsArray
    filterArray !! 0

getDisabledWallets :: ConfigPayload -> Maybe (Array String)
getDisabledWallets (ConfigPayload configPayload) = do
    let findWallets = findElementInRecord "wallets" (ConfigPayload configPayload)
    case findWallets of
        Just wallets -> do
            let (PaymentOptions disWallets) = wallets
            disWallets.onlyDisable
        Nothing -> Nothing

getEnabledWallets :: ConfigPayload -> Maybe (Array String)
getEnabledWallets (ConfigPayload configPayload) = do
    let findWallets = findElementInRecord "wallets" (ConfigPayload configPayload)
    case findWallets of
        Just wallets -> do
            let (PaymentOptions enWallets) = wallets
            enWallets.onlyEnable
        Nothing -> Nothing

getDisabledBanks :: ConfigPayload -> Maybe (Array String)
getDisabledBanks (ConfigPayload configPayload)= do
    let findBanks = findElementInRecord "nb" (ConfigPayload configPayload)
    case findBanks of
        Just banks -> do
            let (PaymentOptions disBanks) = banks
            disBanks.onlyDisable
        Nothing -> Nothing

getDisableUpiApps :: ConfigPayload -> Maybe (Array String)
getDisableUpiApps (ConfigPayload configPayload)= do
    let findApps = findElementInRecord "upi" (ConfigPayload configPayload)
    case findApps of
        Just apps -> do
            let (PaymentOptions disApps) = apps
            disApps.onlyDisable
        Nothing -> Nothing

getUseBtnGrad :: ConfigPayload -> Boolean
getUseBtnGrad (ConfigPayload configPayload) = configPayload.isButtonGradient == "true"

getDisabledCards :: ConfigPayload -> Maybe (Array String)
getDisabledCards (ConfigPayload configPayload) = do
    let findCards = findElementInRecord "cards" (ConfigPayload configPayload)
    case findCards of
        Just cards -> do
            let (PaymentOptions disCards) = cards
            disCards.onlyDisable
        Nothing -> Nothing

getDisabledCardNetworks :: ConfigPayload -> Maybe (Array String)
getDisabledCardNetworks (ConfigPayload configPayload) = do
    let disCards = getDisabledCards (ConfigPayload configPayload)
    case disCards of
        Just dCards -> do
            let disCardNetworks = filter (\(cType) ->
                                            cType == "VISA" ||
                                            cType == "MASTER" ||
                                            cType == "RUPAY" ||
                                            cType == "AMEX" ||
                                            cType == "DINERS" ||
                                            cType == "JCB" ||
                                            cType == "DISCOVER" ||
                                            cType == "MAESTRO")
                                    dCards
            if disCardNetworks == []
                then Nothing
                else Just disCardNetworks
        Nothing -> Nothing

getDisabledCardTypes :: ConfigPayload -> Maybe (Array String)
getDisabledCardTypes (ConfigPayload configPayload) = do
    let disCards = getDisabledCards (ConfigPayload configPayload)
    case disCards of
        Just dCards -> do
            let disCardTypes = filter (\(cType) ->
                                            cType == "CREDIT" ||
                                            cType == "DEBIT")
                                dCards
            if disCardTypes == []
                then Nothing
                else Just disCardTypes
        Nothing -> Nothing

getBlockedCardNumbers :: ConfigPayload -> Maybe (Array String)
getBlockedCardNumbers (ConfigPayload configPayload) = do
    let disCards = getDisabledCards (ConfigPayload configPayload)
    case disCards of
        Just dCards -> do
            let disCardNumbers = filter (\(c) -> isCardNumber c) dCards
            if disCardNumbers == []
                then Nothing
                else Just disCardNumbers
        Nothing -> Nothing


isCardNumber :: String -> Boolean
isCardNumber card =
    let cNo = NumUtils.fromString card
    in case cNo of
        Just _ -> true
        Nothing -> false

getCurvedWrapperColor :: ConfigPayload -> String
getCurvedWrapperColor (ConfigPayload configPayload)= configPayload.curvedWrapperColor

getWrapperColor :: ConfigPayload -> String
getWrapperColor (ConfigPayload configPayload)= configPayload.wrapperColor


getMoreOptions :: ConfigPayload -> MoreOption
getMoreOptions (ConfigPayload configPayload) = (configPayload.moreOption)

getHighlightViewType :: ConfigPayload -> String
getHighlightViewType (ConfigPayload configPayload) = String.toLower configPayload.highlightViewType

getWalletViewType :: ConfigPayload -> Boolean
getWalletViewType (ConfigPayload configPayload) = String.toLower configPayload.expandedWalletView == "true"

getNBViewType :: ConfigPayload -> Boolean
getNBViewType (ConfigPayload configPayload) = String.toLower configPayload.expandPopularNBView == "true"

getGridFontSize :: ConfigPayload -> Int
getGridFontSize (ConfigPayload configPayload) = fromMaybe 16 $ IntUtils.fromString configPayload.gridFontSize

getUpiViewType :: ConfigPayload -> Boolean
getUpiViewType (ConfigPayload configPayload) = String.toLower configPayload.expandUpiView == "true"

getFontSize :: ConfigPayload -> Int
getFontSize (ConfigPayload configPayload) = fromMaybe 16 $ IntUtils.fromString configPayload.fontSize

getIconSize :: ConfigPayload -> Int
getIconSize (ConfigPayload configPayload) = fromMaybe 40 $ IntUtils.fromString configPayload.iconSize

getToolbarIconLocation :: ConfigPayload -> String
getToolbarIconLocation (ConfigPayload configPayload) = if getOS unit == "WEB"
                                                            then "left"
                                                            else configPayload.toolbarIconLocation

getHideOrderSummary :: ConfigPayload -> Boolean
getHideOrderSummary (ConfigPayload configPayload) = String.toLower configPayload.hideOrderSummary == "true"

getHideTopToolBar :: ConfigPayload -> Boolean
getHideTopToolBar (ConfigPayload configPayload) = String.toLower configPayload.hideTopToolBar == "true"

getGridIconSize :: ConfigPayload -> Int
getGridIconSize (ConfigPayload configPayload) = fromMaybe 20 $ IntUtils.fromString configPayload.gridIconSize

getRadioIconSize :: ConfigPayload -> Int
getRadioIconSize (ConfigPayload configPayload) = if getOS unit == "WEB"
                                                        then fromMaybe 22 $ IntUtils.fromString configPayload.checkboxSize
                                                        else fromMaybe 22 $ IntUtils.fromString configPayload.checkboxSizeMobile

getaddCardFontColor :: ConfigPayload -> String
getaddCardFontColor (ConfigPayload configPayload) = configPayload.addCardFontColor

collectUpiWithGodel :: ConfigPayload -> Boolean
collectUpiWithGodel (ConfigPayload configPayload) = String.toLower configPayload.upiCollectWithGodel == "true"

showLineSeparator :: ConfigPayload -> Boolean
showLineSeparator (ConfigPayload configPayload) = String.toLower configPayload.lineSeparator == "true"

getSpacing :: ConfigPayload -> Int
getSpacing (ConfigPayload configPayload) = if getOS unit == "WEB"
                                                then fromMaybe 12 $ IntUtils.fromString configPayload.spacing
                                                else fromMaybe 12 $ IntUtils.fromString "0"

getListItemHeight :: ConfigPayload -> Int
getListItemHeight (ConfigPayload configPayload) = fromMaybe 12 $ IntUtils.fromString configPayload.listItemHeight

getFontColor :: ConfigPayload -> String
getFontColor (ConfigPayload configPayload) = configPayload.fontColor

getDisabledFontColor :: ConfigPayload -> String
getDisabledFontColor (ConfigPayload configPayload) = configPayload.disabledFontColor
getBackgroundColor :: ConfigPayload -> String
getBackgroundColor (ConfigPayload configPayload) = configPayload.backgroundColor

getSectionHeaderSize :: ConfigPayload -> Int
getSectionHeaderSize (ConfigPayload configPayload) = fromMaybe 12 $ IntUtils.fromString header.fontSize
    where
        FontConfig header = configPayload.sectionHeader

getSectionHeaderFontWeight :: ConfigPayload -> String
getSectionHeaderFontWeight (ConfigPayload configPayload) = header.fontWeight
    where
        FontConfig header = configPayload.sectionHeader

getSectionHeaderFontColor :: ConfigPayload -> String
getSectionHeaderFontColor (ConfigPayload configPayload) = header.fontColor
    where
        FontConfig header = configPayload.sectionHeader

getToolbarColor :: ConfigPayload -> String
getToolbarColor (ConfigPayload configPayload) = configPayload.toolbarColor

getTextGravity  :: ConfigPayload -> String
getTextGravity (ConfigPayload configPayload) = configPayload.textGravity

ifHighlightVisible :: ConfigPayload -> Boolean
ifHighlightVisible configPayload= do
    let highlightItems = getHighlight configPayload
    if length highlightItems == 0
        then false
        else do
          let x = foldl (\acc (PaymentOptions po) -> acc || (ifVisible po.visibility)) false highlightItems
          x
    where
        ifVisible x = ((String.toLower x) == "visible")

ifCombinedWallets :: ConfigPayload -> Boolean
ifCombinedWallets (ConfigPayload configPayload) = ((String.toLower configPayload.combineWallets) == "true")

ifCurvedPaymentOption :: ConfigPayload -> Boolean
ifCurvedPaymentOption (ConfigPayload configPayload) = ((String.toLower configPayload.useCurvedPaymentOption) == "true")

ifSeperatedSections :: ConfigPayload -> Boolean
ifSeperatedSections (ConfigPayload configPayload) = ((String.toLower configPayload.useSeperatedSections) == "true")

getPaymentOptionShadow :: ConfigPayload -> Number
getPaymentOptionShadow (ConfigPayload configPayload) = fromMaybe 0.0 $ NumUtils.fromString configPayload.paymentOptionShadow

getCornerRadius :: ConfigPayload -> Number
getCornerRadius (ConfigPayload configPayload) = fromMaybe 0.0 $ NumUtils.fromString configPayload.cornerRadius

getCVVCornerRadius :: ConfigPayload -> Number
getCVVCornerRadius (ConfigPayload configPayload) = fromMaybe 0.0 $ NumUtils.fromString configPayload.cvvInputBoxRadius


getWrapperRadius :: ConfigPayload -> Number
getWrapperRadius (ConfigPayload configPayload) = if getOS unit == "WEB" then fromMaybe 0.0 (NumUtils.fromString "0.0") else  fromMaybe 0.0 $ NumUtils.fromString configPayload.wrapperRadius

getWrapperPadding :: ConfigPayload -> Padding
getWrapperPadding (ConfigPayload configPayload)=do
                if getOS unit == "WEB"
                    then (Padding (mh+sideBarWidth) (mt+space) mh mb)
                    else (Padding mh mt mh mb)
                where
                  (Wrapper wrapper) = configPayload.wrapper
                  (WrapperPadding wrapperPadding) = wrapper.wrapperPadding
                  mh= if getOS unit == "WEB" then fromMaybe 0 (fromString "0") else fromMaybe 0 (fromString wrapperPadding.wrapperPaddingHorizontal)
                  mt= if getOS unit == "WEB" then fromMaybe 0 (fromString "0") else fromMaybe 0 (fromString wrapperPadding.wrapperPaddingTop)
                  mb= if getOS unit == "WEB" then fromMaybe 0 (fromString "0") else fromMaybe 0 (fromString wrapperPadding.wrapperPaddingBottom)
                  sideBarWidth = fromMaybe 0 (fromString (getSideBarWidth (ConfigPayload configPayload)))
                  space = getSpacing (ConfigPayload configPayload)

getSideBarWidth :: ConfigPayload -> String
getSideBarWidth (ConfigPayload configPayload) = sideBar.width
    where
        SideBar sideBar = configPayload.sideBar

getWrapperPaddingTop :: ConfigPayload -> String
getWrapperPaddingTop (ConfigPayload confPayload)= do
    wrapperPadding.wrapperPaddingTop
    where
        (Wrapper wrapper) = confPayload.wrapper
        (WrapperPadding wrapperPadding) = wrapper.wrapperPadding
getCvvBoxType :: ConfigPayload -> String
getCvvBoxType (ConfigPayload configPayload) = configPayload.cvvInputBoxType

ifMobileNumberVerify :: ConfigPayload -> Boolean
ifMobileNumberVerify (ConfigPayload configPayload) = String.toLower configPayload.verifyMobile == "true"

ifOfferVisible :: ConfigPayload -> Boolean
ifOfferVisible (ConfigPayload configPayload) = ((String.toLower configPayload.offers) == "visible")

getContainerPadding :: ConfigPayload -> Int
getContainerPadding (ConfigPayload configPayload) = if (getOS unit == "WEB") then fromMaybe 15 $ IntUtils.fromString configPayload.containerPadding else fromMaybe 15 $ IntUtils.fromString configPayload.containerPaddingMobile

getButtonText :: ConfigPayload -> String
getButtonText (ConfigPayload configPayload) = fromMaybe "Pay" configPayload.buttonText

getGuestUserAllowed :: ConfigPayload -> String
getGuestUserAllowed (ConfigPayload configPayload) = configPayload.guestUserAllowed
getBtnGradient :: ConfigPayload -> ButtonGradient
getBtnGradient (ConfigPayload configPayload)= configPayload.buttonGradient

getTextBoxConfig :: ConfigPayload -> TextBox
getTextBoxConfig (ConfigPayload configPayload)= configPayload.textBox

getButtonConfig :: ConfigPayload -> Button
getButtonConfig (ConfigPayload configPayload)= configPayload.button

getSideBarConfig :: ConfigPayload -> SideBar
getSideBarConfig (ConfigPayload configPayload)= configPayload.sideBar

getErrorMessage :: ConfigPayload -> String -> String
getErrorMessage (ConfigPayload configPayload) paymentMethod = do
    let paymentOptionErrors = configPayload.paymentOptionErrors
    let filterArray= filter (\(PaymentOptionError payOpt) -> payOpt.paymentMethod == paymentMethod) paymentOptionErrors
    let poError = filterArray !! 0
    case poError of
        Just payOptError -> do
            let (PaymentOptionError poe) = payOptError
            poe.errorMessage
        Nothing -> ""

getUseBackgroundImage :: ConfigPayload -> Boolean
getUseBackgroundImage (ConfigPayload configPayload)= if (configPayload.useBackgroundImage) == "true" then true else false

getGradientType :: ButtonGradient -> String
getGradientType (ButtonGradient btnGrad) = btnGrad.gradtype

getGradientAngle :: ButtonGradient -> Number
getGradientAngle (ButtonGradient btnGrad) = toNumber $ fromMaybe 0 $ fromString btnGrad.angle

getGradientColors :: ButtonGradient -> Array String
getGradientColors (ButtonGradient btnGrad)=btnGrad.values

getMainHeaderConfig :: ConfigPayload -> FontConfig
getMainHeaderConfig (ConfigPayload configPayload) = configPayload.mainHeader
getOutageConfig :: ConfigPayload -> OutageConfig
getOutageConfig (ConfigPayload configPayload) = configPayload.outageConfig

disableForOutage :: ConfigPayload -> Boolean
disableForOutage (ConfigPayload configPayload) =
    case outageType of
        "restrict" -> true
        _ -> false
    where
        (OutageConfig outageConfig) = getOutageConfig (ConfigPayload configPayload)
        outageType = outageConfig.outageType

getOutageMessage :: ConfigPayload -> String -> String
getOutageMessage (ConfigPayload configPayload) name =
    case outageType of
        "restrict" -> outageMessage <> name
        "warn" -> outageMessage <> name
        _ -> ""
    where
        (OutageConfig outageConfig) = getOutageConfig (ConfigPayload configPayload)
        outageType = outageConfig.outageType
        outageMessage = outageConfig.message

getOutageMessageForPopularNB :: ConfigPayload -> Array String -> Boolean -> String
getOutageMessageForPopularNB (ConfigPayload configPayload) outages allBankOutage =
    case outageType of
        "restrict" -> outageMessage <> if (allBankOutage) then " all banks" else (unwordWithSeparator outages ", ")
        "warn" -> outageMessage <> if (allBankOutage) then " all banks" else (unwordWithSeparator outages ", ")
        _ -> ""
    where
        (OutageConfig outageConfig) = getOutageConfig (ConfigPayload configPayload)
        outageType = outageConfig.outageType
        outageMessage = outageConfig.message


getOfferOptions :: ConfigPayload -> Array PaymentOptions
getOfferOptions (ConfigPayload configPayload) = configPayload.offerOptions

constructPO :: String -> PaymentOptions
constructPO po =
    PaymentOptions
        { group : ""
             , isLast :""
             , onlyDisable :Nothing
             , onlyEnable : Nothing
             , po : ""
             , visibility : ""
        }
