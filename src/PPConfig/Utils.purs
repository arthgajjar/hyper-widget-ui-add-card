module PPConfig.Utils where

import Remote.Types
import Data.Array (length, elem, elemIndex, (!!), filter, updateAt, foldl, find)
import Data.Int (fromString) as IntUtils
import Data.Maybe (fromMaybe, isJust, Maybe(..))
import Data.Number (fromString) as NumUtils
import Data.String (Pattern(..), stripPrefix, toLower) as String
import Prelude
import PrestoDOM (Gravity(..), Length(..), Margin(..), Padding(..), Position(..), Visibility(..))
import Remote.Types (PaymentOptions(..),OutageViewProps(..))
import Payments.Wallets.Utils (dummyPO)

fm = fromMaybe -- alias

-------- Fonts
getPrimaryFont :: ConfigPayload -> String
getPrimaryFont (ConfigPayload configPayload) = configPayload.primaryFont

fontBold :: ConfigPayload ->  String
fontBold configPayload  = getPrimaryFont  configPayload <> "-Bold"

fontRegular :: ConfigPayload ->  String
fontRegular (ConfigPayload configPayload) = (getPrimaryFont $ ConfigPayload configPayload) <> "-Regular"

fontSemiBold :: ConfigPayload ->  String
fontSemiBold configPayload = getPrimaryFont configPayload <>"-SemiBold"

getFontSize :: ConfigPayload -> Int
getFontSize (ConfigPayload configPayload) = fm 16 $ IntUtils.fromString configPayload.fontSize

fontSize :: ConfigPayload ->  Int
fontSize configPayload = getFontSize configPayload

fontSizeSmall :: ConfigPayload -> Int
fontSizeSmall configPayload= (fontSize configPayload) - 2

fontSizeVerySmall :: ConfigPayload ->  Int
fontSizeVerySmall configPayload = (fontSize configPayload) -4

fontSizeLarge :: ConfigPayload -> Int
fontSizeLarge configPayload = (fontSize configPayload) + 2

fontSizeVeryLarge :: ConfigPayload -> Int
fontSizeVeryLarge configPayload = (fontSize configPayload) + 4

getFontColor :: ConfigPayload -> String
getFontColor (ConfigPayload configPayload) = configPayload.fontColor

fontColor :: ConfigPayload -> String
fontColor configPayload = getFontColor configPayload

fontColorAlpha :: ConfigPayload -> String -> String
fontColorAlpha configPayload alpha = "#" <> alpha <> fm "000000" (String.stripPrefix (String.Pattern "#") (getFontColor configPayload))

tertiaryFontColor :: ConfigPayload -> String
tertiaryFontColor (ConfigPayload configPayload) = configPayload.tertiaryFontColor

-------- Icons

getIconSize :: ConfigPayload -> Int
getIconSize (ConfigPayload configPayload) = fm 40 $ IntUtils.fromString configPayload.iconSize

iconSize :: ConfigPayload -> Int
iconSize configPayload = getIconSize configPayload

iconSizeLarge :: ConfigPayload -> Int
iconSizeLarge configPayload = (iconSize configPayload) + 20

iconSizeSmall :: ConfigPayload -> Int
iconSizeSmall configPayload = (iconSize configPayload) - 10

getRadioIconSize :: ConfigPayload -> Int
getRadioIconSize (ConfigPayload configPayload) = fm 22 $ IntUtils.fromString configPayload.checkboxSize

radioIconSize :: ConfigPayload -> Int
radioIconSize configPayload = getRadioIconSize configPayload

-------- Other UI Utils

headerSize :: ConfigPayload -> Int
headerSize (ConfigPayload configPayload) = let
    HeaderProps headerConfig = configPayload.headerProps
    in (fm 12 $ IntUtils.fromString headerConfig.size)

headerColor :: ConfigPayload -> String
headerColor (ConfigPayload configPayload) = let
    HeaderProps headerConfig = configPayload.headerProps
    in headerConfig.color

headerFontFace :: ConfigPayload -> String
headerFontFace conf@(ConfigPayload configPayload) = do
    let HeaderProps headerConfig = configPayload.headerProps
    case (String.toLower headerConfig.fontFace) of
        "bold"     -> fontBold conf
        "semibold" -> fontSemiBold conf
        _          -> fontRegular conf

headerMargin :: ConfigPayload -> Int
headerMargin (ConfigPayload configPayload) = let
    HeaderProps headerConfig = configPayload.headerProps
    in fm 10 $ IntUtils.fromString headerConfig.bottomMargin

primaryColor :: ConfigPayload -> String
primaryColor (ConfigPayload configPayload) = configPayload.primaryColor

getBackgroundColor :: ConfigPayload -> String
getBackgroundColor (ConfigPayload configPayload) = configPayload.backgroundColor

backgroundColor :: ConfigPayload -> String
backgroundColor configPayload = getBackgroundColor configPayload


showLineSeparator :: ConfigPayload -> Boolean
showLineSeparator (ConfigPayload configPayload) = String.toLower configPayload.lineSeparator == "true"

lineSepVisibility :: ConfigPayload -> Visibility
lineSepVisibility configPayload =  if showLineSeparator configPayload then VISIBLE else GONE

getListItemHeight :: ConfigPayload -> Int
getListItemHeight (ConfigPayload configPayload) = fm 12 $ IntUtils.fromString configPayload.listItemHeight

listItemHeight :: ConfigPayload -> Int
listItemHeight configPayload = getListItemHeight configPayload

topRowVisibility :: ConfigPayload -> Visibility
topRowVisibility (ConfigPayload configPayload) = if configPayload.topRowVisibility == "false" then GONE else VISIBLE

getInputViewType :: ConfigPayload -> String
getInputViewType (ConfigPayload configPayload) = let
    InputField inputFieldConfig = configPayload.inputField
    in (String.toLower inputFieldConfig.type)

getInputFieldFocusColor :: ConfigPayload -> String
getInputFieldFocusColor (ConfigPayload configPayload) = let
    InputField inputFieldConfig = configPayload.inputField
    in inputFieldConfig.focusColor

getInputFieldFontStyle :: ConfigPayload -> String
getInputFieldFontStyle conf@(ConfigPayload configPayload) = do
    let InputField inputFieldConfig = configPayload.inputField
    case (String.toLower inputFieldConfig.fontStyle) of
        "bold"     -> fontBold conf
        "semibold" -> fontSemiBold conf
        _          -> fontRegular conf

inputFieldlabelFontFace :: ConfigPayload -> String
inputFieldlabelFontFace c@(ConfigPayload conf) = let
    InputField inputFieldConfig = conf.inputField
    in mapToFontFamily inputFieldConfig.labelFontFace c

inputFieldlabelFontSize :: ConfigPayload -> Int
inputFieldlabelFontSize (ConfigPayload conf) = let
    InputField inputFieldConfig = conf.inputField
    in fm 12 $ IntUtils.fromString inputFieldConfig.labelFontSize

labelFontColor :: ConfigPayload -> String
labelFontColor c@(ConfigPayload conf) = let
  InputField inputFieldConfig = conf.inputField
  in inputFieldConfig.labelFontColor

isMaterialInputField :: ConfigPayload -> Boolean
isMaterialInputField (ConfigPayload configPayload) =
    let InputField inputFieldConfig = configPayload.inputField
    in (String.toLower inputFieldConfig.useMaterialView) == "true"
-------- Cards

isCardNumber :: String -> Boolean
isCardNumber card = isJust $ NumUtils.fromString card

getDisabledCards :: ConfigPayload -> Maybe (Array String)
getDisabledCards configPayload = do
    (PaymentOptions disCards) <- findElementInRecord "cards" configPayload
    disCards.onlyDisable

arrayToMaybeArray :: ∀ a. Array a -> Maybe (Array a)
arrayToMaybeArray [] = Nothing
arrayToMaybeArray a = Just a

getBlockedCardNumbers :: ConfigPayload -> Maybe (Array String)
getBlockedCardNumbers configPayload =
    arrayToMaybeArray <<< filter isCardNumber =<< getDisabledCards configPayload

getDisabledCardTypes :: ConfigPayload -> Maybe (Array String)
getDisabledCardTypes configPayload =
    arrayToMaybeArray <<< filter (\cType ->
                                    cType == "CREDIT" ||
                                    cType == "DEBIT")
        =<< getDisabledCards configPayload

getDisabledCardNetworks :: ConfigPayload -> Maybe (Array String)
getDisabledCardNetworks configPayload =
    arrayToMaybeArray <<< filter (\cType ->
                                    cType == "VISA" ||
                                    cType == "MASTER" ||
                                    cType == "RUPAY" ||
                                    cType == "AMEX" ||
                                    cType == "DINERS" ||
                                    cType == "JCB" ||
                                    cType == "DISCOVER" ||
                                    cType == "MAESTRO")
        =<< getDisabledCards configPayload

-------- Wallets

ifCombinedWallets :: ConfigPayload -> Boolean
ifCombinedWallets (ConfigPayload configPayload) = ((String.toLower configPayload.combineWallets) == "true")

ifWalletExpanded :: ConfigPayload -> Boolean
ifWalletExpanded (ConfigPayload configPayload) = String.toLower configPayload.expandedWalletView == "true"

getDisabledWallets :: ConfigPayload -> Maybe (Array String)
getDisabledWallets configPayload = do
    (PaymentOptions disWallets) <- findElementInRecord "wallets" configPayload
    disWallets.onlyDisable

getDisabledPayLaterWallets :: ConfigPayload -> Maybe (Array String)
getDisabledPayLaterWallets configPayload = do
    (PaymentOptions disWallets) <- findElementInRecord "payLater" configPayload
    disWallets.onlyDisable

getEnabledWallets :: ConfigPayload -> Maybe (Array String)
getEnabledWallets (ConfigPayload configPayload) = do
    let findWallets = findElementInRecord "wallets" (ConfigPayload configPayload)
    case findWallets of
        Just wallets -> do
            let (PaymentOptions enWallets) = wallets
            enWallets.onlyEnable
        Nothing -> Nothing

-- Use only if ineligible wallets need to be shown as "Disabled"
-- addIneligibleWallets :: ConfigPayload -> Maybe (Array String) -> ConfigPayload
-- addIneligibleWallets configPayload@(ConfigPayload cp) disWallets = fm configPayload do
--     wallets@(PaymentOptions payLater) <- findElementInRecord "payLater" configPayload
--     index <- elemIndex wallets cp.paymentOptions
--     let updatedPL = PaymentOptions $ payLater {onlyDisable = payLater.onlyDisable <> disWallets}
--     updatedPO <- updateAt index updatedPL cp.paymentOptions
--     pure $ ConfigPayload $ cp { paymentOptions = updatedPO}

-------- UPI

getUpiViewType :: ConfigPayload -> Boolean
getUpiViewType (ConfigPayload configPayload) = String.toLower configPayload.expandUpiView == "true"

getDisableUpiApps :: ConfigPayload -> Maybe (Array String)
getDisableUpiApps configPayload = do
    (PaymentOptions disApps) <- findElementInRecord "upi" configPayload
    disApps.onlyDisable

doVerifyVpa :: ConfigPayload -> Boolean
doVerifyVpa (ConfigPayload configPayload) = ((String.toLower configPayload.verifyVPA) == "true")

-------- NB
getNBViewType :: ConfigPayload -> Boolean
getNBViewType (ConfigPayload configPayload) = String.toLower configPayload.expandPopularNBView == "true"

getDisabledBanks :: ConfigPayload -> Maybe (Array String)
getDisabledBanks configPayload = do
    (PaymentOptions disBanks) <- findElementInRecord "nb" configPayload
    disBanks.onlyDisable

getPopularBanks :: ConfigPayload -> Array String
getPopularBanks (ConfigPayload configPayload) = configPayload.popularBanks

--------
findElementInRecord :: String -> ConfigPayload -> Maybe PaymentOptions
findElementInRecord poType (ConfigPayload configPayload) =
    filter (\(PaymentOptions payOpt) -> payOpt.po == poType) configPayload.paymentOptions !! 0

findElementInRecord' :: String -> Array PaymentOptions -> Maybe PaymentOptions
findElementInRecord' poType paymentOptions =
    filter (\(PaymentOptions payOpt) -> payOpt.po == poType) paymentOptions !! 0

getMoreOptions :: ConfigPayload -> MoreOption
getMoreOptions (ConfigPayload configPayload) = (configPayload.moreOption)

updatePOVisibility :: String -> Boolean -> Array PaymentOptions -> Array PaymentOptions
updatePOVisibility poType hidePO paymentOptions = paymentOptions --changed


ifSeperatedSections :: ConfigPayload -> Boolean
ifSeperatedSections (ConfigPayload configPayload) = ((String.toLower configPayload.useSeperatedSections) == "true")

ifMobileNumberVerify :: ConfigPayload -> Boolean
ifMobileNumberVerify (ConfigPayload configPayload) = String.toLower configPayload.verifyMobile == "true"

ifOfferVisible :: ConfigPayload -> Boolean
ifOfferVisible (ConfigPayload configPayload) = ((String.toLower configPayload.offers) == "visible")

getWidgetKey :: String -> ConfigPayload -> String
getWidgetKey inputWidget (ConfigPayload configPayload) = fm inputWidget do
    Widgets widgets <- configPayload.widgets
    pure $ if inputWidget `elem` widgets.allowed
        then inputWidget
        else widgets.primary

getSavedConfig :: ConfigPayload -> Saved
getSavedConfig (ConfigPayload configPayload) = configPayload.saved

savedOptionsConfig :: ConfigPayload -> { otherSaved :: String, preffered :: String, saved :: String}
savedOptionsConfig (ConfigPayload configPayload) = let
  Saved savedOptionsConfig = getSavedConfig (ConfigPayload configPayload)
  in savedOptionsConfig

getPaymentOptions :: ConfigPayload -> Array PaymentOptions
getPaymentOptions (ConfigPayload configPayload) = configPayload.paymentOptions

getDisabledPaymentOptions :: ConfigPayload -> String -> Array String
getDisabledPaymentOptions cP payOpt = let
    poArray = filter(\(PaymentOptions po) -> po.po == payOpt) (getPaymentOptions cP)
    PaymentOptions po = fromMaybe dummyPO $ poArray !! 0
    in
    fromMaybe [] po.onlyDisable

getHighlight :: ConfigPayload -> Array PaymentOptions
getHighlight (ConfigPayload configPayload) = configPayload.highlight

ifHighlightVisible :: ConfigPayload -> Boolean
ifHighlightVisible configPayload = let
  highlightItems = getHighlight configPayload
  ifVisible x = ((String.toLower x) == "visible")
  in
  if length highlightItems == 0
    then false
    else foldl (\acc (PaymentOptions po) -> acc || (ifVisible po.visibility)) false highlightItems

getScreenTransitionConfig :: ConfigPayload -> ScreenTransition
getScreenTransitionConfig (ConfigPayload configPayload) = configPayload.screenTransition

getScreenTransitionDuration :: ConfigPayload -> Int
getScreenTransitionDuration configPayload = let
  ScreenTransition screenTransitionConfig = getScreenTransitionConfig configPayload
  in fm 200 $ IntUtils.fromString screenTransitionConfig.duration

getScreenTransitionCurve :: ConfigPayload -> Array Number
getScreenTransitionCurve configPayload = let
  ScreenTransition screenTransitionConfig = getScreenTransitionConfig configPayload
  beizerArray = screenTransitionConfig.curve
  in map (\a -> fm 0.0 $ NumUtils.fromString a) beizerArray

getHighlightViewType :: ConfigPayload -> String
getHighlightViewType (ConfigPayload configPayload) = String.toLower configPayload.highlightViewType

collectUpiWithGodel :: ConfigPayload -> Boolean
collectUpiWithGodel (ConfigPayload configPayload) = String.toLower configPayload.upiCollectWithGodel == "true"

ifModalView :: ConfigPayload -> Boolean
ifModalView (ConfigPayload configPayload) = configPayload.modalView == "true"

getDOTPScreenPayload :: ConfigPayload -> DOTPScreenConfigPayload
getDOTPScreenPayload (ConfigPayload config) = let
    (Toolbar toolbarConfig) = config.toolbar
    in DOTPScreenConfigPayload
        { primaryColor : config.primaryColor
        , secondaryColor : Just config.primaryColor
        , backgroundColor : config.backgroundColor
        , primaryFont : config.primaryFont
        , toolbarTextSize : fromMaybe 0 $ IntUtils.fromString toolbarConfig.textSize
        , toolbarTextFontFace : ""
        , cornerRadius : 0.0 -- fm 0.0 $ NumUtils.fromString config.cornerRadius -- Fix this later
        , otpEditTextBackground : "#FFFFFF"
        }

isBtnAtBottom :: ConfigPayload -> Boolean
isBtnAtBottom (ConfigPayload configPayload) = do
    let Button buttonConfig = configPayload.button
    buttonConfig.atBottom == "true"

btnBackground :: ConfigPayload -> String
btnBackground (ConfigPayload configPayload) = do
    let Button buttonConfig = configPayload.button
    buttonConfig.background

changeBtnText :: ConfigPayload -> Boolean
changeBtnText (ConfigPayload configPayload) = do
    let Button buttonConfig = configPayload.button
    buttonConfig.changeText == "true"

editTextCornerRadius :: ConfigPayload -> Number
editTextCornerRadius (ConfigPayload configPayload) = do
    let InputField inputFieldConfig = configPayload.inputField
    let cr = fm "0.0" inputFieldConfig.cornerRadius
    fm 0.0 $ NumUtils.fromString cr

editTextPadding :: ConfigPayload -> Int
editTextPadding (ConfigPayload configPayload) = do
    let InputField inputFieldConfig = configPayload.inputField
    fm 0 $ IntUtils.fromString inputFieldConfig.textPadding

btnCornerRadius :: ConfigPayload -> Number
btnCornerRadius (ConfigPayload configPayload) = do
    let Button buttonConfig = configPayload.button
    fm 0.0 $ NumUtils.fromString buttonConfig.cornerRadius

btnTranslation :: ConfigPayload -> Number
btnTranslation (ConfigPayload configPayload) = do
    let Button buttonConfig = configPayload.button
    fm 0.0 $ NumUtils.fromString buttonConfig.translation

btnColor :: ConfigPayload -> String
btnColor (ConfigPayload configPayload) = do
    let Button buttonConfig = configPayload.button
    buttonConfig.color

horizontalSpace :: ConfigPayload -> Int
horizontalSpace (ConfigPayload configPayload) = do
    let ContainerAttribs contAttr = configPayload.containerAttribs
    fm 16 $ IntUtils.fromString contAttr.horizontalSpacing

verticalSpace :: ConfigPayload -> Int
verticalSpace (ConfigPayload configPayload) = do
    let ContainerAttribs contAttr = configPayload.containerAttribs
    fm 10 $ IntUtils.fromString contAttr.verticalSpacing

sectionSpace :: ConfigPayload -> Int
sectionSpace (ConfigPayload configPayload) = do
    let ContainerAttribs contAttr = configPayload.containerAttribs
    fm 10 $ IntUtils.fromString contAttr.sectionSpacing

uiCardHorizontalPadding :: ConfigPayload -> Int
uiCardHorizontalPadding (ConfigPayload configPayload) = do
    let UICard uiCard = configPayload.uiCard
    fm 10 $ IntUtils.fromString uiCard.horizontalPadding

uiCardVerticalPadding :: ConfigPayload -> Int
uiCardVerticalPadding (ConfigPayload configPayload) = do
    let UICard uiCard = configPayload.uiCard
    fm 16 $ IntUtils.fromString uiCard.verticalPadding

uiCardCornerRadius :: ConfigPayload -> Number
uiCardCornerRadius (ConfigPayload configPayload) = do
    let UICard uiCard = configPayload.uiCard
    fm 0.0 $ NumUtils.fromString uiCard.cornerRadius

uiCardTranslation :: ConfigPayload -> Number
uiCardTranslation (ConfigPayload configPayload) = do
    let UICard uiCard = configPayload.uiCard
    fm 0.0 $ NumUtils.fromString uiCard.translation

amountBarVerticalPadding :: ConfigPayload -> Int
amountBarVerticalPadding (ConfigPayload configPayload) = do
    let AmountBar amountBarConfig = configPayload.amountBar
    fm 0 $ IntUtils.fromString amountBarConfig.verticalPadding

amountBarTranslation :: ConfigPayload -> Number
amountBarTranslation (ConfigPayload configPayload) = do
    let AmountBar amountBarConfig = configPayload.amountBar
    fm 0.0 $ NumUtils.fromString amountBarConfig.translation

attachAmountToAllScreens :: ConfigPayload -> Boolean
attachAmountToAllScreens (ConfigPayload configPayload) = do
    let AmountBar amountBarConfig = configPayload.amountBar
    String.toLower amountBarConfig.attachOnAllScreens == "true"

attachAmountBarAtTop :: ConfigPayload -> Boolean
attachAmountBarAtTop (ConfigPayload configPayload) = do
    let AmountBar amountBarConfig = configPayload.amountBar
    String.toLower amountBarConfig.attachOnTop == "true"

isAmountBarVisible :: ConfigPayload -> Boolean
isAmountBarVisible (ConfigPayload configPayload) = do
    let AmountBar amountBarConfig = configPayload.amountBar
    amountBarConfig.visibility == "visible"

attachPOHeaders :: ConfigPayload -> Boolean
attachPOHeaders (ConfigPayload configPayload) =
    String.toLower configPayload.attachPOHeaders == "true"

useQuickPayRanking :: ConfigPayload -> Boolean
useQuickPayRanking (ConfigPayload configPayload) =
    String.toLower configPayload.useQuickPayRanking == "true"

uiCardColor :: ConfigPayload -> String
uiCardColor (ConfigPayload configPayload) = do
    let UICard uiCard = configPayload.uiCard
    uiCard.color

inputAreaBackground :: ConfigPayload -> String
inputAreaBackground (ConfigPayload configPayload) = let
    InputField inputFieldConfig = configPayload.inputField
    in inputFieldConfig.background

-- keys yet to be exposed

expandInAccordion :: ConfigPayload -> Boolean
expandInAccordion cp = false

expandableOtherOptions :: ConfigPayload -> Boolean
expandableOtherOptions _ = false

placeExpansionSelectorAtLeft :: ConfigPayload -> Boolean
placeExpansionSelectorAtLeft cp = false

expandCardView :: ConfigPayload -> Boolean
expandCardView configPayload = false

-- | if true then checks for lastUsedApp present in paymentSource
-- and shows on PaymentPage
showLastUsedApp :: ConfigPayload -> Boolean
showLastUsedApp _ = false

showVPASaveOption :: ConfigPayload -> Boolean
showVPASaveOption _ = false

showVPAError :: ConfigPayload -> Boolean
showVPAError _ = false

showVPAMetadata :: ConfigPayload -> Boolean
showVPAMetadata _ = true

addUPICollectToExpandedUPI :: ConfigPayload -> Boolean
addUPICollectToExpandedUPI cp = false

secondaryButtonHorizontalPadding :: ConfigPayload -> Int
secondaryButtonHorizontalPadding cP = 0

secondaryButtonVerticalPadding :: ConfigPayload -> Int
secondaryButtonVerticalPadding cP = 0

secondaryButtonHorizontalMargin :: ConfigPayload -> Int
secondaryButtonHorizontalMargin cP = 16

secondaryButtonVerticalMargin :: ConfigPayload -> Int
secondaryButtonVerticalMargin cP = 0

secondaryButtonFontFace :: ConfigPayload -> String
secondaryButtonFontFace cP = fontSemiBold cP

secondaryButtonGravity :: ConfigPayload -> Gravity
secondaryButtonGravity cP = CENTER_VERTICAL

mapToFontFamily :: String -> ConfigPayload ->  String
mapToFontFamily family cP =
  case String.toLower family of
    "bold" -> fontBold cP
    "semibold" -> fontSemiBold cP
    _ -> fontRegular cP

pOPrimaryTextFont :: ConfigPayload -> String
pOPrimaryTextFont c@(ConfigPayload cP) = mapToFontFamily cP.paymentOptionFontFace c

-- | when this field will go in configPayload, "regular" will be configurable
editTextErrorFont :: ConfigPayload -> String
editTextErrorFont = mapToFontFamily "semibold" -- "regular"

editTextErrorColor :: ConfigPayload -> String
editTextErrorColor _ = "#D44C59"

editTextHideHint :: ConfigPayload -> Boolean
editTextHideHint _ = false

editTextLineUnfocused :: ConfigPayload -> String
editTextLineUnfocused _ = "#E0E0E0"

editTextLineFocused :: ConfigPayload -> String
editTextLineFocused (ConfigPayload cP) = let
    InputField inputField = cP.inputField
    in inputField.focusColor

appendCardNumToPrimaryText :: ConfigPayload -> Boolean
appendCardNumToPrimaryText _ = false

listItemFont :: ConfigPayload -> String
listItemFont c@(ConfigPayload cP) = mapToFontFamily cP.paymentOptionFontFace c

primaryLogoFirst :: ConfigPayload -> Boolean
primaryLogoFirst _ = false

primaryLogoSize :: ConfigPayload -> Maybe Int
primaryLogoSize _ = Nothing -- Just 32

indentExpandedSection :: ConfigPayload -> Boolean
indentExpandedSection _ = false

showSavedVPAinPP :: ConfigPayload -> Boolean
showSavedVPAinPP _ = false

secondaryButtonUseArrow :: ConfigPayload -> Boolean
secondaryButtonUseArrow (ConfigPayload cP) = let
    SecondaryButton secondaryButton = cP.secondaryButton
    in (String.toLower secondaryButton.useNavigationArrow) == "true"

secondaryButtonWidth :: ConfigPayload -> Length
secondaryButtonWidth (ConfigPayload cP) = do
    let (SecondaryButton secondaryButton) = cP.secondaryButton
    case (String.toLower secondaryButton.width) of
        "match_parent" -> MATCH_PARENT
        _              -> WRAP_CONTENT

secondaryButtonHeight :: ConfigPayload -> Int
secondaryButtonHeight (ConfigPayload cP) = let
    SecondaryButton secondaryButton = cP.secondaryButton
    in fm 10 $ IntUtils.fromString secondaryButton.height

secondaryButtonAlignment :: ConfigPayload -> Gravity
secondaryButtonAlignment (ConfigPayload cP) = do
    let SecondaryButton secondaryButton = cP.secondaryButton
    case (String.toLower secondaryButton.buttonAlignment) of
        "center" -> CENTER
        "left"   -> LEFT
        "right"  -> RIGHT
        _        -> CENTER_VERTICAL

secondaryButtonBackground :: ConfigPayload -> String
secondaryButtonBackground (ConfigPayload cP) = let
    SecondaryButton secondaryButton = cP.secondaryButton
    in secondaryButton.buttonBackground

secondaryButtonCornerRadius :: ConfigPayload -> Number
secondaryButtonCornerRadius (ConfigPayload cP) = let
    SecondaryButton secondaryButton = cP.secondaryButton
    in fm 0.0 $ NumUtils.fromString secondaryButton.buttonCornerRadius

secondaryButtonTextColor :: ConfigPayload -> String
secondaryButtonTextColor (ConfigPayload cP) = let
    SecondaryButton secondaryButton = cP.secondaryButton
    in secondaryButton.textColor

secondaryButtonTextSize :: ConfigPayload -> Int
secondaryButtonTextSize (ConfigPayload cP) = let
    SecondaryButton secondaryButton = cP.secondaryButton
    in fm 10 $ IntUtils.fromString secondaryButton.textSize

secondaryButtonFont :: ConfigPayload -> String
secondaryButtonFont c@(ConfigPayload cP) = let
  SecondaryButton secondaryButton = cP.secondaryButton
  in mapToFontFamily secondaryButton.fontFace c

termsAndConditionsInPP :: ConfigPayload -> Visibility
termsAndConditionsInPP _ = GONE

showCvvInfoLogo :: ConfigPayload -> Boolean
showCvvInfoLogo _ = true

gridViewPadding :: ConfigPayload -> Boolean
gridViewPadding _ = true

labelButtonTextColor :: ConfigPayload -> String
labelButtonTextColor (ConfigPayload cP) = let
    LabelButton labelButton = cP.labelButton
    in labelButton.textColor

labelButtonBackgroundColor :: ConfigPayload -> String
labelButtonBackgroundColor (ConfigPayload cP) = let
    LabelButton labelButton = cP.labelButton
    in labelButton.color

labelButtonCornerRadius :: ConfigPayload -> Number
labelButtonCornerRadius (ConfigPayload cP) = let
    LabelButton labelButton = cP.labelButton
    in fm 0.0 $ NumUtils.fromString labelButton.cornerRadius

labelButtonFontFamily :: ConfigPayload -> String
labelButtonFontFamily c@(ConfigPayload cP) =
    let LabelButton labelButton = cP.labelButton
    in mapToFontFamily labelButton.textFontFace c

labelButtonFontSize :: ConfigPayload -> Int
labelButtonFontSize (ConfigPayload cP) = let
    LabelButton labelButton = cP.labelButton
    in fm 10 $ IntUtils.fromString labelButton.textSize

labelButtonHeight :: ConfigPayload -> Int
labelButtonHeight (ConfigPayload cP) = let
    LabelButton labelButton = cP.labelButton
    in fm 20 $ IntUtils.fromString labelButton.height

useLogoTick :: ConfigPayload -> Boolean
useLogoTick (ConfigPayload cP) = let
    GridProps gridProps = cP.gridProps
    in (String.toLower gridProps.useTick) == "true"

gridStroke :: ConfigPayload -> Boolean
gridStroke (ConfigPayload cP) = let
    GridProps gridProps = cP.gridProps
    in (String.toLower gridProps.useStroke) == "true"

gridViewBackground :: ConfigPayload -> String
gridViewBackground (ConfigPayload cP) = let
    GridProps gridProps = cP.gridProps
    in gridProps.gridViewBackground

horizontalFade :: ConfigPayload -> Boolean
horizontalFade (ConfigPayload cP) = let
    GridProps gridProps = cP.gridProps
    in (String.toLower gridProps.horizontalFade) == "true"

fadingEdgeLength :: ConfigPayload -> Int
fadingEdgeLength (ConfigPayload cP) = let
    GridProps gridProps = cP.gridProps
    in fm 0 $ IntUtils.fromString gridProps.fadingEdgeLength

gridItemSize :: ConfigPayload -> Int
gridItemSize (ConfigPayload cP) = let
    GridProps gridProps = cP.gridProps
    in fm 0 $ IntUtils.fromString gridProps.itemSize

gridSelectedStroke :: ConfigPayload -> String
gridSelectedStroke (ConfigPayload cP) = let
    GridProps gridProps = cP.gridProps
    in gridProps.gridSelectedStroke

gridLogoStroke :: ConfigPayload -> String
gridLogoStroke (ConfigPayload cP) = let
    GridProps gridProps = cP.gridProps
    in gridProps.gridLogoStroke

useButtonForGridSelection :: ConfigPayload -> Boolean
useButtonForGridSelection (ConfigPayload cP) = let
    GridProps gridProps = cP.gridProps
    in (String.toLower gridProps.useButtonForSelection) == "true"

strokeCornerRadius :: ConfigPayload -> Number
strokeCornerRadius (ConfigPayload cP) = let
    GridProps gridProps = cP.gridProps
    in fm 0.0 $ NumUtils.fromString gridProps.strokeCornerRadius

gridFontSize :: ConfigPayload -> Int
gridFontSize (ConfigPayload cP) = let
    GridProps gridProps = cP.gridProps
    in fm 10 $ IntUtils.fromString gridProps.gridFontSize

gridIconSize :: ConfigPayload -> Int
gridIconSize (ConfigPayload cP) = let
    GridProps gridProps = cP.gridProps
    in fm 10 $ IntUtils.fromString gridProps.gridIconSize

useGridForNb :: ConfigPayload -> Boolean
useGridForNb _ = true

offerHeader :: ConfigPayload -> String
offerHeader (ConfigPayload cP) = "All Offers"

offerHeaderTextSize :: ConfigPayload -> Int
offerHeaderTextSize (ConfigPayload cP) = 32

-- offer proper configs
offerTextColor :: ConfigPayload -> String
offerTextColor (ConfigPayload cP) = let
  OfferViewProps offerViewProps = cP.offerViewProps
  in offerViewProps.textColor

offerLayoutColor :: ConfigPayload -> String
offerLayoutColor (ConfigPayload cP) = let
  OfferViewProps offerViewProps = cP.offerViewProps
  in offerViewProps.backgroundColor

offerLayoutCornerRadius :: ConfigPayload -> Number
offerLayoutCornerRadius (ConfigPayload cP) = let
  OfferViewProps offerViewProps = cP.offerViewProps
  in fm 0.0 $ NumUtils.fromString offerViewProps.cornerRadius

offerLayoutPadding :: ConfigPayload -> Padding
offerLayoutPadding (ConfigPayload cP) = let
  toInt a = fm 10 $ IntUtils.fromString a
  OfferViewProps offerViewProps = cP.offerViewProps
  left = toInt offerViewProps.padding.left
  right = toInt offerViewProps.padding.right
  vertical = toInt offerViewProps.padding.vertical
  in Padding left vertical right vertical

mandateRowTextColor :: ConfigPayload -> String
mandateRowTextColor (ConfigPayload cP) = let
    MandateViewProps mandateViewProps = cP.mandateViewProps
    in mandateViewProps.textColor

mandateRowBackground :: ConfigPayload -> String
mandateRowBackground (ConfigPayload cP) = let
    MandateViewProps mandateViewProps = cP.mandateViewProps
    in mandateViewProps.backgroundColor

mandateRowCornerRadius ::ConfigPayload -> Number
mandateRowCornerRadius (ConfigPayload cP) = let
  MandateViewProps mandateViewProps = cP.mandateViewProps
  in fm 0.0 $ NumUtils.fromString mandateViewProps.cornerRadius

mandateRowPadding :: ConfigPayload -> Padding
mandateRowPadding (ConfigPayload cP) = let
  toInt a = fm 10 $ IntUtils.fromString a
  MandateViewProps mandateViewProps = cP.mandateViewProps
  left = toInt mandateViewProps.padding.left
  right = toInt mandateViewProps.padding.right
  vertical = toInt mandateViewProps.padding.vertical
  in Padding left vertical right vertical

isInstrumentEnabled :: ConfigPayload -> String -> Boolean
isInstrumentEnabled (ConfigPayload cp) instrument =
  isJust $ cp.mandateInstruments #
    find \inst -> String.toLower inst == String.toLower instrument

highlightMessageTextColor :: ConfigPayload -> String
highlightMessageTextColor (ConfigPayload cP) = let
    HighlightMessage highlightMessage = cP.highlightMessage
    in highlightMessage.textColor

highlightMessageBackground :: ConfigPayload -> String
highlightMessageBackground (ConfigPayload cP) = let
    HighlightMessage highlightMessage = cP.highlightMessage
    in highlightMessage.backgroundColor

highlightMessagePadding :: ConfigPayload -> Padding
highlightMessagePadding (ConfigPayload cP) = let
  toInt a = fm 10 $ IntUtils.fromString a
  HighlightMessage highlightMessage = cP.highlightMessage
  horizontal = toInt highlightMessage.padding.horizontal
  vertical = toInt highlightMessage.padding.vertical
  in Padding horizontal vertical horizontal vertical

highlightMessageTextFontFace :: ConfigPayload -> String
highlightMessageTextFontFace c@(ConfigPayload cP) = let
    HighlightMessage highlightMessage = cP.highlightMessage
    in mapToFontFamily highlightMessage.textFontFace c

getAnimationColor :: ConfigPayload -> String
getAnimationColor (ConfigPayload cP) = "#4DFFFFFF"

outageRowCornerRadius ::ConfigPayload -> Number
outageRowCornerRadius (ConfigPayload cP) = let
  OutageViewProps outageViewProps = cP.outageViewProps
  in fm 0.0 $ NumUtils.fromString outageViewProps.cornerRadius

outageRowPadding :: ConfigPayload -> Padding
outageRowPadding (ConfigPayload cP) = let
  toInt a = fm 10 $ IntUtils.fromString a
  OutageViewProps outageViewProps = cP.outageViewProps
  left = toInt outageViewProps.padding.left
  right = toInt outageViewProps.padding.right
  vertical = toInt outageViewProps.padding.vertical
  in Padding left vertical right vertical

outageRowTextSize ::ConfigPayload -> Int
outageRowTextSize (ConfigPayload cP) = let
  OutageViewProps outageViewProps = cP.outageViewProps
  in fm 12 $ IntUtils.fromString outageViewProps.textSize

showOutageRow :: ConfigPayload -> Boolean
showOutageRow (ConfigPayload cP) = let
  OutageViewProps outageViewProps = cP.outageViewProps
  in String.toLower outageViewProps.showOutageView == "true"

restrictOutagePayment :: ConfigPayload -> Boolean
restrictOutagePayment (ConfigPayload cP) = let
  OutageViewProps outageViewProps = cP.outageViewProps
  in String.toLower outageViewProps.restrictPayment == "true"

outageRowMessage :: ConfigPayload -> String
outageRowMessage (ConfigPayload cP) = let
  OutageViewProps outageViewProps = cP.outageViewProps
  in outageViewProps.outageMessage
