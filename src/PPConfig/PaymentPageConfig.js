var ideaConfig = function () {
  if(window.getUIConfig){
    console.log("loading config from ui_config");
    return window.getUIConfig();
  }
  var ideaConfigPayload = {
    "backgroundColor": "#f5f6f8",
    "checkboxSize": "18",
    "combineWallets": "false",
    "expandPopularNBView": "true",
    "expandUpiView": "false",
    "expandedWalletView": "true",
    "fontColor": "#040404",
    "tertiaryFontColor": "#00b137",
    "fontSize": "14",
    "highlight": [],
    "highlightViewType": "list",
    "iconSize": "38",
    "buttonText": "Proceed to Pay",
    "lineSeparator": "true",
    "lineSeparatorColor": "#FFFFFF",
    "listItemHeight": "60",
    "modalView": "false",
    "moreOption": {
      "icon": "wallet_icon",
      "name": "WalletFlow",
      "view": {
        "action": "payWithWallet",
        "content": "editText",
        "footer": "button",
        "toolbar": {
          "back": "VISIBLE",
          "pageTitle": "MoreOptionTitle",
          "textSize": 18,
          "textFontFace": "Bold",
          "textColor": "#333333",
          "drawFromStatusBar": "false",
          "color": "#FBBA19",
          "useImageBackground": "true",
          "translation": "0.0",
          "height": "64"
        }
      },
      "visibility": "gone"
    },
    "offers": "visible",
    "offerOptions" : [
      {
          "group": "others",
          "po": "savedWallets",
          "visibility": "VISIBLE"
      },
      {
          "group": "others",
          "po": "nb",
          "visibility": "VISIBLE"
      },
      {
          "group": "others",
          "po": "upi",
          "visibility": "VISIBLE"
      },
      {
          "group": "others",
          "po": "savedVPAs",
          "visibility": "VISIBLE"
      },
      {
          "group": "others",
          "po": "unlinkedWallets",
          "visibility": "VISIBLE"
      }
    ],
    "paymentOptions": [{
        "group": "others",
        "po": "askAFriend",
        "visibility": "gone"
      },
      {
        "group": "others",
        "po": "wallets",
        "visibility": "VISIBLE"
      },
      {
        "group": "others",
        "onlyDisable": [
          "SHAREit",
          "WhatsApp"
        ],
        "po": "upiAppsWithOther",
        "visibility": "visible"
      },
      {
        "group": "others",
        "po": "payLater",
        "visibility": "VISIBLE"
      },
      {
        "group": "others",
        "onlyDisable": [],
        "po": "cards",
        "visibility": "VISIBLE"
      },
      {
        "group": "others",
        "onlyDisable": [
          "NB_DUMMY",
          "NB_SBM",
          "NB_SBT"
        ],
        "po": "nb",
        "visibility": "VISIBLE"
      }
    ],
    "popularBanks": [
      "NB_AXIS",
      "NB_ICICI",
      "NB_SBI",
      "NB_OBC"
    ],
    "primaryColor": "#0071BC",
    "primaryFont": "Arial",
    "saved": {
      "otherSaved": "visible",
      "preffered": "visible",
      "saved": "VISIBLE"
    },
    "screenTransition": {
      "curve": [
        "0.1",
        "0.4",
        "0.4",
        "0.9"
      ],
      "duration": "200"
    },
    "toolbar": {
      "back": "VISIBLE",
      "pageTitle": "Payment Methods",
      "textSize": 18,
      "textFontFace": "Regular",
      "textColor": "#333333",
      "drawFromStatusBar": "true",
      "color": "#FBBA19",
      "useImageBackground": "true",
      "translation": "5.0",
      "height": "64"
    },
    "amountBar": {
      "amountBarData": [{
        "leftString": "Mobile Number",
        "textFontFace": "Regular",
        "fontSize": "14",
        "color": "#6b6b6b"
      }, {
        "leftString": "phoneNumber",
        "textFontFace": "Bold",
        "fontSize": "16",
        "color": "#000000"
      }],
      "placeAmountAtRight": "true",
      "amountTextSize": "20",
      "attachOnTop": "true",
      "attachOnAllScreens": "true",
      "visibility": "visible",
      "verticalPadding": "8",
      "translation": "5.0",
      "color": "#ffffff",
      "addDivider": "false",
      "dividerColor": "#000000"
    },
    "button": {
      "background": "#ffffff",
      "color": "#0071BC",
      "useGradient": "false",
      "gradient": {
        "from": "",
        "to": "",
        "direction": "HORIZONTAL"
      },
      "atBottom": "true",
      "cornerRadius": "5.0",
      "changeText": "true",
      "translation": "0.0"
    },
    "headerProps": {
      "size": "16",
      "color": "#333333",
      "fontFace": "Bold",
      "bottomMargin": "0"
    },
    "containerAttribs": {
      "horizontalSpacing": "12",
      "verticalSpacing": "24",
      "sectionSpacing": "24"
    },
    "uiCard": {
      "translation": "4.0",
      "cornerRadius": "5.0",
      "horizontalPadding": "12",
      "verticalPadding": "16",
      "color": "#ffffff"
    },
    "inputField": {
      "type": "boxed",
      "useMaterialView": "false",
      "cornerRadius": "4.0",
      "focusColor": "#0071BC",
      "textPadding": "5",
      "background": "#ffffff",
      "fontStyle": "Bold",
      "labelFontFace" : "Bold",
      "labelFontSize" : "12",
      "labelFontColor" : "#131313"
    },
    "secondaryButton": {
      "useNavigationArrow": "false",
      "width": "WRAP_CONTENT",
      "height": "27",
      "buttonAlignment": "LEFT",
      "buttonBackground": "#ebebeb",
      "buttonCornerRadius": "12.0",
      "textColor": "#000000",
      "textSize": "15",
      "fontFace": "Regular"
    },
    "labelButton": {
      "color": "#ebebeb",
      "textColor": "#000000",
      "textFontFace": "Regular",
      "cornerRadius": "12.0",
      "textSize": "15",
      "height": "27"
    },
    "attachPOHeaders": "true",
    "upiCollectWithGodel": "false",
    "verifyMobile": "true",
    "useSeperatedSections": "true",
    "verifyVpa": "false",
    "language": "english",
    "labelBackground": "#EBEBEB",
    "topRowVisibility": "false",
    "showSearchBoxStroke": "true",
    "showProcessingPayment": "true",
    "bottomMargin": "16",
    "widgets": {
      "allowed": ["quickPay", "paymentPage"],
      "primary": "quickPay"
      },
    "useQuickPayRanking": "false",
    "paymentOptionFontFace": "SemiBold",
    "gridProps": {
      "useTick": "false",
      "useStroke": "false",
      "itemSize": "60",
      "gridSelectedStroke": "",
      "gridViewBackground": "#ffffff",
      "horizontalFade": "true",
      "fadingEdgeLength": "150",
      "gridLogoStroke": "",
      "useButtonForSelection": "false",
      "strokeCornerRadius": "",
      "gridFontSize": "14",
      "gridIconSize": "38"
    },
    "mandateViewProps": {
      "textColor" : "#a58a21",
      "backgroundColor" : "#f4efd3",
      "cornerRadius" : "5.0",
      "padding" : { "left" : "3" , "right" : "6" , "vertical" : "10"  },
      "textSize" : "12"
    },
    "offerViewProps": {
      "textColor" : "#a58a21",
      "backgroundColor" : "#f4efd3",
      "cornerRadius" : "4.0",
      "padding" : { "left" : "10" , "right" : "8" , "vertical" : "10" },
      "textSize" : "12"
    },
    "mandateInstruments" : [],
    "highlightMessage" : {
      "textColor" : "#000000",
      "backgroundColor" : "#f0f0f0",
      "textFontFace" : "Regular",
      "padding" : { "vertical" : "10" , "horizontal" : "14" }
    },
    "outageViewProps": {
      "showOutageView" : "true",
      "textColor" : "#FF1E0C",
      "backgroundColor" : "#ffffff",
      "cornerRadius" : "4.0",
      "padding" : { "left" : "0" , "right" : "0" , "vertical" : "0"  },
      "textSize" : "12",
      "outageMessage" : "Experiencing low success rates for",
      "restrictPayment" : "false"
    }
  };
  return JSON.stringify(ideaConfigPayload);
}

var zee5config = function () {
  var zee5ConfigPayload = {
    "backgroundColor": "#1c001e",
    "checkboxSize": "18",
    "combineWallets": "false",
    "expandPopularNBView": "true",
    "expandUpiView": "false",
    "expandedWalletView": "true",
    "fontColor": "#ffffff",
    "tertiaryFontColor": "#00b137",
    "fontSize": "16",
    "highlight": [],
    "highlightViewType": "list",
    "iconSize": "38",
    "buttonText": "Proceed",
    "lineSeparator": "true",
    "lineSeparatorColor": "#000000",
    "listItemHeight": "60",
    "modalView": "false",
    "moreOption": {
      "icon": "wallet_icon",
      "name": "WalletFlow",
      "view": {
        "action": "payWithWallet",
        "content": "editText",
        "footer": "button",
        "toolbar": {
          "back": "VISIBLE",
          "pageTitle": "MoreOptionTitle",
          "textSize": 18,
          "textFontFace": "Bold",
          "textColor": "#ffffff",
          "drawFromStatusBar": "false",
          "color": "#2a0a2b",
          "useImageBackground": "true",
          "translation": "0.0",
          "height": "64"
        }
      },
      "visibility": "gone"
    },
    "offers": "visible",
    "offerOptions" : [
      {
          "group": "others",
          "po": "savedWallets",
          "visibility": "VISIBLE"
      },
      {
          "group": "others",
          "po": "nb",
          "visibility": "VISIBLE"
      },
      {
          "group": "others",
          "po": "upi",
          "visibility": "VISIBLE"
      },
      {
          "group": "others",
          "po": "savedVPAs",
          "visibility": "VISIBLE"
      },
      {
          "group": "others",
          "po": "unlinkedWallets",
          "visibility": "VISIBLE"
      }
    ],
    "paymentOptions": [{
        "group": "others",
        "po": "askAFriend",
        "visibility": "gone"
      },
      {
        "group": "others",
        "po": "wallets",
        "visibility": "VISIBLE"
      },
      {
        "group": "others",
        "onlyDisable": [
          "SHAREit",
          "WhatsApp"
        ],
        "po": "upi",
        "visibility": "visible"
      },
      {
        "group": "others",
        "onlyDisable": [],
        "po": "cards",
        "visibility": "VISIBLE"
      },
      {
        "group": "others",
        "onlyDisable": [
          "NB_DUMMY",
          "NB_SBM",
          "NB_SBT"
        ],
        "po": "nb",
        "visibility": "VISIBLE"
      }
    ],
    "popularBanks": [
      "NB_AXIS",
      "NB_ICICI",
      "NB_SBI",
      "NB_OBC"
    ],
    "primaryColor": "#FFFFFF",
    "primaryFont": "Arial",
    "saved": {
      "otherSaved": "visible",
      "preffered": "visible",
      "saved": "VISIBLE"
    },
    "screenTransition": {
      "curve": [
        "0.1",
        "0.4",
        "0.4",
        "0.9"
      ],
      "duration": "200"
    },
    "toolbar": {
      "back": "VISIBLE",
      "pageTitle": "Payment",
      "textSize": 18,
      "textFontFace": "Bold",
      "textColor": "#ffffff",
      "drawFromStatusBar": "true",
      "color": "#2a0a2b",
      "useImageBackground": "false",
      "translation": "5.0",
      "height": "64"
    },
    "amountBar": {
      "amountBarData": [{
        "leftString": "Mobile Number",
        "textFontFace": "Regular",
        "fontSize": "14",
        "color": "#6b6b6b"
      }, {
        "leftString": "phoneNumber",
        "textFontFace": "Bold",
        "fontSize": "16",
        "color": "#000000"
      }],
      "placeAmountAtRight": "true",
      "amountTextSize": "20",
      "attachOnTop": "true",
      "attachOnAllScreens": "true",
      "visibility": "GONE",
      "verticalPadding": "8",
      "translation": "5.0",
      "color": "#ffffff",
      "addDivider": "false",
      "dividerColor": "#000000"
    },
    "button": {
      "background": "#1c001e",
      "color": "#ec0032",
      "useGradient": "true",
      "gradient": {
        "from": "#ec0032",
        "to": "#b300d0",
        "direction": "HORIZONTAL"
      },
      "atBottom": "true",
      "cornerRadius": "30.0",
      "changeText": "true",
      "translation": "0.0"
    },
    "headerProps": {
      "size": "16",
      "color": "#ffffff",
      "fontFace": "Bold",
      "bottomMargin": "0"
    },
    "containerAttribs": {
      "horizontalSpacing": "12",
      "verticalSpacing": "24",
      "sectionSpacing": "24"
    },
    "uiCard": {
      "translation": "4.0",
      "cornerRadius": "5.0",
      "horizontalPadding": "12",
      "verticalPadding": "16",
      "color": "#2b0a2c"
    },
    "inputField": {
      "type": "boxed",
      "useMaterialView": "false",
      "cornerRadius": "4.0",
      "focusColor": "#FFFFFF",
      "textPadding": "5",
      "background": "2b0a2c",
      "fontStyle": "Bold",
      "labelFontFace" : "Bold",
      "labelFontSize" : "12",
      "labelFontColor" : "#131313"
    },
    "secondaryButton": {
      "useNavigationArrow": "false",
      "width": "WRAP_CONTENT",
      "height": "27",
      "buttonAlignment": "CENTER",
      "buttonBackground": "#2b0a2c",
      "buttonCornerRadius": "12.0",
      "textColor": "#FFFFFF",
      "textSize": "15",
      "fontFace": "Regular"
    },
    "labelButton": {
      "color": "#2b0a2c",
      "textColor": "#FFFFFF",
      "textFontFace": "Regular",
      "cornerRadius": "12.0",
      "textSize": "15",
      "height": "27"
    },
    "attachPOHeaders": "true",
    "upiCollectWithGodel": "false",
    "verifyMobile": "true",
    "useSeperatedSections": "true",
    "verifyVpa": "false",
    "language": "english",
    "labelBackground": "#2b0a2c",
    "topRowVisibility": "false",
    "showSearchBoxStroke": "true",
    "showProcessingPayment": "true",
    "bottomMargin": "16",
    // TODO :: Confirm this
    "widgets": {
      "allowed": ["quickPay", "paymentPage", "delinkWallet"],
      "primary": "quickPay"
    },
    "useQuickPayRanking": "false",
    "paymentOptionFontFace": "Bold",
    "gridProps": {
      "useTick": "false",
      "useStroke": "false",
      "itemSize": "60",
      "gridSelectedStroke": "",
      "gridViewBackground": "#2a0a2b",
      "horizontalFade": "true",
      "fadingEdgeLength": "150",
      "gridLogoStroke": "",
      "useButtonForSelection": "false",
      "strokeCornerRadius": "",
      "gridFontSize": "14",
      "gridIconSize": "38"
    },
    "mandateViewProps": {
      "textColor" : "#a58a21",
      "backgroundColor" : "#f4efd3",
      "cornerRadius" : "5.0",
      "padding" : { "left" : "3" , "right" : "6" , "vertical" : "10"  },
      "textSize" : "12"
    },
    "offerViewProps": {
      "textColor" : "#a58a21",
      "backgroundColor" : "#f4efd3",
      "cornerRadius" : "4.0",
      "padding" : { "left" : "10" , "right" : "8" , "vertical" : "10" },
      "textSize" : "12"
    },
    "mandateInstruments" : [],
    "highlightMessage" : {
      "textColor" : "#FFFFFF",
      "backgroundColor" : "#f0f0f0",
      "textFontFace" : "Regular",
      "padding" : { "vertical" : "10" , "horizontal" : "14" }
    },
    "outageViewProps": {
      "showOutageView" : "true",
      "textColor" : "#FF1E0C",
      "backgroundColor" : "#ffffff",
      "cornerRadius" : "4.0",
      "padding" : { "left" : "0" , "right" : "0" , "vertical" : "0"  },
      "textSize" : "12",
      "outageMessage" : "Experiencing low success rates for",
      "restrictPayment" : "false"
    }
  };

  return JSON.stringify(zee5ConfigPayload);
}
var curefitConfig = function () {
  if(window.getUIConfig){
    console.log("loading config from ui_config");
    return window.getUIConfig();
  }
  var curefitConfigPayload = {
    "backgroundColor": "#F7F7F7",
    "checkboxSize": "18",
    "combineWallets": "false",
    "expandPopularNBView": "true",
    "expandUpiView": "false",
    "expandedWalletView": "true",
    "fontColor": "#585858",
    "tertiaryFontColor": "#EB4979",
    "fontSize": "14",
    "highlight": [],
    "highlightViewType": "list",
    "iconSize": "30",
    "buttonText": "Proceed to Pay",
    "lineSeparator": "true",
    "lineSeparatorColor": "#FFFFFF",
    "listItemHeight": "65",
    "modalView": "false",
    "moreOption": {
      "icon": "wallet_icon",
      "name": "WalletFlow",
      "view": {
        "action": "payWithWallet",
        "content": "editText",
        "footer": "button",
        "toolbar": {
          "back": "VISIBLE",
          "pageTitle": "MoreOptionTitle",
          "textSize": 18,
          "textFontFace": "Bold",
          "textColor": "#585858",
          "drawFromStatusBar": "false",
          "color": "#FBBA19",
          "useImageBackground": "true",
          "translation": "0.0",
          "height": "64"
        }
      },
      "visibility": "gone"
    },
    "offers": "gone",
    "offerOptions" : [
      {
          "group": "others",
          "po": "savedWallets",
          "visibility": "VISIBLE"
      },
      {
          "group": "others",
          "po": "nb",
          "visibility": "VISIBLE"
      },
      {
          "group": "others",
          "po": "upi",
          "visibility": "VISIBLE"
      },
      {
          "group": "others",
          "po": "savedVPAs",
          "visibility": "VISIBLE"
      },
      {
          "group": "others",
          "po": "unlinkedWallets",
          "visibility": "VISIBLE"
      }
    ],
    "paymentOptions": [{
        "group": "others",
        "po": "askAFriend",
        "visibility": "gone"
      },
      {
        "group": "others",
        "po": "wallets",
        "visibility": "VISIBLE"
      },
      {
        "group": "others",
        "onlyDisable": [
          "NB_DUMMY",
          "NB_SBM",
          "NB_SBT",
          "NB_CANR"
        ],
        "po": "nb",
        "visibility": "VISIBLE"
      },
      {
        "group": "others",
        "onlyDisable": [
          "SHAREit",
          "WhatsApp"
        ],
        "po": "upi",
        "visibility": "visible"
      },
      {
        "group": "others",
        "onlyDisable": [],
        "po": "cards",
        "visibility": "VISIBLE"
      }
    ],
    "popularBanks": [
      "NB_AXIS",
      "NB_ICICI",
      "NB_SBI",
      "NB_HDFC"
    ],
    "primaryColor": "#EB4979",
    "primaryFont": "BrandonText",
    "saved": {
      "otherSaved": "visible",
      "preffered": "visible",
      "saved": "VISIBLE"
    },
    "screenTransition": {
      "curve": [
        "0.1",
        "0.4",
        "0.4",
        "0.9"
      ],
      "duration": "200"
    },
    "spacing": "12",
    "toolbar": {
      "back": "VISIBLE",
      "pageTitle": "Payment Methods",
      "textSize": 18,
      "textFontFace": "Bold",
      "textColor": "#1e1e1e",
      "drawFromStatusBar": "false",
      "color": "#ffffff",
      "useImageBackground": "false",
      "translation": "0.0",
      "height": "60"
    },
    "amountBar": {
      "amountBarData": [{
        "leftString": "Total Amount Payable",
        "textFontFace": "semibold",
        "fontSize": "16",
        "color": "#222222",
        "textColor" : "#2c2c2c"
      }],
      "placeAmountAtRight": "true",
      "amountTextSize": "24",
      "attachOnTop": "true",
      "attachOnAllScreens": "true",
      "visibility": "visible",
      "verticalPadding": "10",
      "translation": "0.0",
      "color": "#ffffff",
      "addDivider": "false",
      "dividerColor": "#000000"
    },
    "button": {
      "background": "#ffffff",
      "color": "#EB4979",
      "useGradient": "false",
      "gradient": {
        "from": "",
        "to": "",
        "direction": "HORIZONTAL"
      },
      "atBottom": "false",
      "cornerRadius": "24.0",
      "changeText": "false",
      "translation": "24.0"
    },
    "headerProps": {
      "size": "16",
      "color": "#222222",
      "fontFace": "bold",
      "bottomMargin": "10"
    },
    "containerAttribs": {
      "horizontalSpacing": "0",
      "verticalSpacing": "12",
      "sectionSpacing": "14"
    },
    "uiCard": {
      "translation": "5.0",
      "cornerRadius": "0.0",
      "horizontalPadding": "24",
      "verticalPadding": "16",
      "color": "#ffffff"
    },
    "inputField": {
      "type": "underlined",
      "useMaterialView": "false",
      "cornerRadius": "4.0",
      "focusColor": "#ff0077",
      "textPadding": "10",
      "background": "#ffffff",
      "fontStyle": "Bold",
      "labelFontFace" : "regular",
      "labelFontSize" : "14",
      "labelFontColor" : "#585858"
    },
    "secondaryButton": {
      "useNavigationArrow": "false",
      "width": "WRAP_CONTENT",
      "height": "24",
      "buttonAlignment": "LEFT",
      "buttonBackground": "",
      "buttonCornerRadius": "",
      "textColor": "#000000",
      "textSize": "17",
      "fontFace": "semibold"
    },
    "labelButton": {
      "color": "#eff0f4",
      "textColor": "#000000",
      "textFontFace": "Regular",
      "cornerRadius": "3.0",
      "textSize": "12",
      "height": "30"
    },
    "attachPOHeaders": "false",
    "upiCollectWithGodel": "false",
    "verifyMobile": "false",
    "useSeperatedSections": "true",
    "verifyVpa": "false",
    "language": "english",
    "labelBackground": "#EBEBEB",
    "topRowVisibility": "false",
    "showSearchBoxStroke": "true",
    "showProcessingPayment": "true",
    "bottomMargin": "16",
    "widgets": {
      "allowed": ["emi"],
      "primary": "emi"
    },
    "useQuickPayRanking": "false",
    "paymentOptionFontFace": "Regular",
    "gridProps": {
      "useTick": "false",
      "useStroke": "false",
      "itemSize": "60",
      "gridSelectedStroke": "",
      "gridViewBackground": "#ffffff",
      "horizontalFade": "true",
      "fadingEdgeLength": "150",
      "gridLogoStroke": "",
      "useButtonForSelection": "false",
      "strokeCornerRadius": "",
      "gridFontSize": "14",
      "gridIconSize": "38"
    },
    "mandateViewProps": {
      "textColor" : "#898989",
      "backgroundColor" : "#f8f8f8",
      "cornerRadius" : "4.0",
      "padding" : { "left" : "" , "right" : "" , "vertical" : ""  },
      "textSize" : "12"
      },
    "offerViewProps": {
      "textColor" : "#898989",
      "backgroundColor" : "#f8f8f8",
      "cornerRadius" : "4.0",
      "padding" : { "left" : "" , "right" : "" , "vertical" : ""  },
      "textSize" : "12"
    },
    "mandateInstruments" : [],
    "highlightMessage" : {
      "textColor" : "#000000",
      "backgroundColor" : "#f0f0f0",
      "textFontFace" : "Regular",
      "padding" : { "vertical" : "10" , "horizontal" : "14" }
    },
    "outageViewProps": {
      "showOutageView" : "true",
      "textColor" : "#FF1E0C",
      "backgroundColor" : "#ffffff",
      "cornerRadius" : "4.0",
      "padding" : { "left" : "0" , "right" : "0" , "vertical" : "0"  },
      "textSize" : "12",
      "outageMessage" : "Experiencing low success rates for",
      "restrictPayment" : "false"
    }
  };

  return JSON.stringify(curefitConfigPayload);
}

var jiosaavnConfig = function () {
  if(window.getUIConfig){
    console.log("loading config from ui_config");
    return window.getUIConfig();
  }
  var jiosaavnConfigPayload = {
    "backgroundColor": "#f4f5f4",
    "checkboxSize": "18",
    "combineWallets": "false",
    "expandPopularNBView": "true",
    "expandUpiView": "true",
    "expandedWalletView": "true",
    "fontColor": "#333333",
    "tertiaryFontColor": "#2BC5B4",
    "fontSize": "15",
    "highlight": [],
    "highlightViewType": "list",
    "iconSize": "38",
    "buttonText": "Proceed to Pay",
    "lineSeparator": "true",
    "lineSeparatorColor": "#FFFFFF",
    "listItemHeight": "60",
    "modalView": "false",
    "moreOption": {
      "icon": "wallet_icon",
      "name": "WalletFlow",
      "view": {
        "action": "payWithWallet",
        "content": "editText",
        "footer": "button",
        "toolbar": {
          "back": "VISIBLE",
          "pageTitle": "MoreOptionTitle",
          "textSize": 18,
          "textFontFace": "Bold",
          "textColor": "#333333",
          "drawFromStatusBar": "false",
          "color": "#FBBA19",
          "useImageBackground": "true",
          "translation": "0.0",
          "height": "64"
        }
      },
      "visibility": "gone"
    },
    "offers": "visible",
    "offerOptions" : [
      {
          "group": "others",
          "po": "savedWallets",
          "visibility": "VISIBLE"
      },
      {
          "group": "others",
          "po": "nb",
          "visibility": "VISIBLE"
      },
      {
          "group": "others",
          "po": "upi",
          "visibility": "VISIBLE"
      },
      {
          "group": "others",
          "po": "savedVPAs",
          "visibility": "VISIBLE"
      },
      {
          "group": "others",
          "po": "unlinkedWallets",
          "visibility": "VISIBLE"
      }
    ],
    "paymentOptions": [{
        "group": "others",
        "po": "wallets",
        "visibility": "VISIBLE"
      },
      {
        "group": "others",
        "onlyDisable": [
          "SHAREit",
          "WhatsApp"
        ],
        "po": "upi",
        "visibility": "visible"
      },
      {
        "group": "others",
        "onlyDisable": [],
        "po": "cards",
        "visibility": "VISIBLE"
      },
      {
        "group": "others",
        "onlyDisable": [
          "NB_DUMMY",
          "NB_SBM",
          "NB_SBT"
        ],
        "po": "nb",
        "visibility": "VISIBLE"
      }
    ],
    "popularBanks": [
      "NB_AXIS",
      "NB_ICICI",
      "NB_SBI",
      "NB_HDFC",
      "NB_OBC"
    ],
    "primaryColor": "#5ec3b5",
    "primaryFont": "Lato",
    "saved": {
      "otherSaved": "visible",
      "preffered": "visible",
      "saved": "VISIBLE"
    },
    "screenTransition": {
      "curve": [
        "0.1",
        "0.4",
        "0.4",
        "0.9"
      ],
      "duration": "200"
    },
    "toolbar": {
      "back": "VISIBLE",
      "pageTitle": "Payment Methods",
      "textSize": 18,
      "textFontFace": "SemiBold",
      "textColor": "#333333",
      "drawFromStatusBar": "false",
      "color": "#f4f5f4",
      "useImageBackground": "false",
      "translation": "0.0",
      "height": "64"
    },
    "amountBar": {
      "amountBarData": [{
          "leftString": "",
          "textFontFace": "SemiBold",
          "fontSize": "18",
          "color": "#B8B8B8"
        },
        {
          "leftString": "orderDescription",
          "textFontFace": "SemiBold",
          "fontSize": "16",
          "color": "#B8B8B8"
        },
        {
          "leftString": "amount",
          "textFontFace": "Bold",
          "fontSize": "30",
          "color": "#333333"
        },
        {
          "leftString": "",
          "textFontFace": "Bold",
          "fontSize": "10",
          "color": "#333333"
        }
      ],
      "placeAmountAtRight": "false",
      "amountTextSize": "16",
      "attachOnTop": "false",
      "attachOnAllScreens": "false",
      "visibility": "visible",
      "verticalPadding": "8",
      "translation": "0.0",
      "color": "#f4f5f4",
      "addDivider": "true",
      "dividerColor": "#4DB8B8B8"
    },
    "button": {
      "background": "#f7f7f7",
      "color": "#5ec3b5",
      "useGradient": "false",
      "gradient": {
        "from": "",
        "to": "",
        "direction": "HORIZONTAL"
      },
      "atBottom": "true",
      "cornerRadius": "4.0",
      "changeText": "true",
      "translation": "0.0"
    },
    "headerProps": {
      "size": "14",
      "color": "#333333",
      "fontFace": "semibold",
      "bottomMargin": "10"
    },
    "containerAttribs": {
      "horizontalSpacing": "16",
      "verticalSpacing": "24",
      "sectionSpacing": "31"
    },
    "uiCard": {
      "translation": "0.0",
      "cornerRadius": "5.0",
      "horizontalPadding": "16",
      "verticalPadding": "16",
      "color": "#ffffff"
    },
    "inputField": {
      "type": "boxed",
      "useMaterialView": "false",
      "cornerRadius": "5.0",
      "focusColor": "#5ec3b5",
      "textPadding": "2",
      "background": "#ffffff",
      "fontStyle": "SemiBold",
      "labelFontFace" : "Regular",
      "labelFontSize" : "13",
      "labelFontColor" : "#999999"
    },
    "secondaryButton": {
      "useNavigationArrow": "false",
      "width": "WRAP_CONTENT",
      "height": "24",
      "buttonAlignment": "LEFT",
      "buttonBackground": "",
      "buttonCornerRadius": "",
      "textColor": "#5ec3b5",
      "textSize": "16",
      "fontFace": "semibold"
    },
    "labelButton": {
      "color": "#eff0f4",
      "textColor": "#000000",
      "textFontFace": "Bold",
      "cornerRadius": "3.0",
      "textSize": "12",
      "height": "30"
    },
    "attachPOHeaders": "true",
    "upiCollectWithGodel": "false",
    "verifyMobile": "true",
    "useSeperatedSections": "true",
    "verifyVpa": "false",
    "language": "english",
    "labelBackground": "#EBEBEB",
    "topRowVisibility": "false",
    "widgets": {
      "allowed": ["quickPay", "paymentPage","delinkWallet"],
      "primary": "quickPay"
    },
    "useQuickPayRanking": "false",
    "paymentOptionFontFace": "SemiBold",
    "gridProps": {
      "useTick": "false",
      "useStroke": "false",
      "itemSize": "60",
      "gridSelectedStroke": "",
      "gridViewBackground": "#ffffff",
      "horizontalFade": "true",
      "fadingEdgeLength": "150",
      "gridLogoStroke": "",
      "useButtonForSelection": "false",
      "strokeCornerRadius": "",
      "gridFontSize": "14",
      "gridIconSize": "38"
    },
    "mandateViewProps": {
      "textColor" : "#898989",
      "backgroundColor" : "#f8f8f8",
      "cornerRadius" : "4.0",
      "padding" : { "left" : "" , "right" : "" , "vertical" : ""  },
      "textSize" : "12"
    },
    "offerViewProps": {
      "textColor" : "#a58a21",
      "backgroundColor" : "#f4efd3",
      "cornerRadius" : "4.0",
      "padding" : { "left" : "" , "right" : "" , "vertical" : ""  },
      "textSize" : "12"
    },
    "mandateInstruments" : [ "wallets", "cards" ],
    "highlightMessage" : {
      "textColor" : "#000000",
      "backgroundColor" : "#f0f0f0",
      "textFontFace" : "Regular",
      "padding" : { "vertical" : "10" , "horizontal" : "14" }
    },
    "outageViewProps": {
      "showOutageView" : "true",
      "textColor" : "#FF1E0C",
      "backgroundColor" : "#ffffff",
      "cornerRadius" : "4.0",
      "padding" : { "left" : "0" , "right" : "0" , "vertical" : "0"  },
      "textSize" : "12",
      "outageMessage" : "Experiencing low success rates for",
      "restrictPayment" : "false"
    }
  };

  return JSON.stringify(jiosaavnConfigPayload);
}

var olaConfigPayload = function () {
  var olaConfig = {
    "backgroundColor": "#ffffff",
    "checkboxSize": "16",
    "combineWallets": "false",
    "expandPopularNBView": "true",
    "expandUpiView": "true",
    "expandedWalletView": "true",
    "fontColor": "#1E1E1E",
    "tertiaryFontColor": "#39A32b",
    "fontSize": "16",
    "headerSize": "16",
    "highlight": [],
    "highlightViewType": "list",
    "iconSize": "25",
    "buttonText": "Pay",
    "lineSeparator": "true",
    "lineSeparatorColor": "#FFFFFF",
    "listItemHeight": "64",
    "modalView": "false",
    "moreOption": {
      "icon": "wallet_icon",
      "name": "WalletFlow",
      "view": {
        "action": "payWithWallet",
        "content": "editText",
        "footer": "button",
        "toolbar": {
          "back": "VISIBLE",
          "pageTitle": "MoreOptionTitle",
          "textSize": 18,
          "textFontFace": "Bold",
          "textColor": "#333333",
          "drawFromStatusBar": "false",
          "color": "#FBBA19",
          "useImageBackground": "true",
          "translation": "0.0",
          "height": "48"
        }
      },
      "visibility": "gone"
    },
    "offers": "gone",
    "offerOptions" : [
      {
          "group": "others",
          "po": "savedWallets",
          "visibility": "GONE"
      },
      {
          "group": "others",
          "po": "nb",
          "visibility": "VISIBLE"
      },
      {
          "group": "others",
          "po": "upi",
          "visibility": "VISIBLE"
      },
      {
          "group": "others",
          "po": "savedVPAs",
          "visibility": "VISIBLE"
      },
      {
          "group": "others",
          "po": "unlinkedWallets",
          "visibility": "VISIBLE"
      }
    ],
    "paymentOptions": [{
        "group": "others",
        "po": "askAFriend",
        "visibility": "gone"
      },
      {
        "group": "others",
        "po": "wallets",
        "visibility": "GONE"
      },
      {
        "group": "others",
        "onlyDisable": [
          "SHAREit",
          "WhatsApp"
        ],
        "po": "upiApps",
        "visibility": "visible"
      },
      {
        "group": "others",
        "po": "upiCollect",
        "visibility": "visible"
      },
      {
        "group": "others",
        "onlyDisable": [],
        "po": "cards",
        "visibility": "VISIBLE"
      },
      {
        "group": "others",
        "onlyDisable": [
          "NB_DUMMY",
          "NB_SBM",
          "NB_SBT",
          "NB_CANR",
          "NB_HDFC"
        ],
        "po": "nb",
        "visibility": "VISIBLE"
      }
    ],
    "popularBanks": [
      "NB_AXIS",
      "NB_ICICI",
      "NB_SBI",
      "NB_OBC"
    ],
    "primaryColor": "#39A32b",
    "primaryFont": "Roboto",
    "saved": {
      "otherSaved": "visible",
      "preffered": "visible",
      "saved": "VISIBLE"
    },
    "screenTransition": {
      "curve": [
        "0.1",
        "0.4",
        "0.4",
        "0.9"
      ],
      "duration": "200"
    },
    "toolbar": {
      "back": "VISIBLE",
      "pageTitle": "",
      "textSize": 18,
      "textFontFace": "Bold",
      "textColor": "#1E1E1E",
      "drawFromStatusBar": "false",
      "color": "#ffffff",
      "useImageBackground": "false",
      "translation": "7.0",
      "height": "48"
    },
    "amountBar": {
      "amountBarData": [{
        "leftString": "Postpaid Bill",
        "textFontFace": "SemiBold",
        "fontSize": "24",
        "color": "#1E1E1E"
      }],
      "placeAmountAtRight": "true",
      "amountTextSize": "24",
      "attachOnTop": "true",
      "attachOnAllScreens": "false",
      "visibility": "visible",
      "verticalPadding": "8",
      "translation": "7.0",
      "color": "#ffffff",
      "addDivider": "true",
      "dividerColor": "#cacaca"
    },
    "button": {
      "background": "#ffffff",
      "color": "#52B32C",
      "useGradient": "false",
      "gradient": {
        "from": "",
        "to": "",
        "direction": "HORIZONTAL"
      },
      "atBottom": "true",
      "cornerRadius": "6.0",
      "changeText": "true",
      "translation": "12.0"
    },
    "headerProps": {
      "size": "18",
      "color": "#333333",
      "fontFace": "semiBold",
      "bottomMargin": "10"
    },
    "containerAttribs": {
      "horizontalSpacing": "0",
      "verticalSpacing": "12",
      "sectionSpacing": "8"
    },
    "uiCard": {
      "translation": "0.0",
      "cornerRadius": "0.0",
      "horizontalPadding": "12",
      "verticalPadding": "0",
      "color": "#ffffff"
    },
    "inputField": {
      "type": "underlined",
      "useMaterialView": "true",
      "cornerRadius": "4.0",
      "focusColor": "#1665C0",
      "textPadding": "3",
      "background": "#ffffff",
      "fontStyle": "SemiBold",
      "labelFontFace" : "regular",
      "labelFontSize" : "14",
      "labelFontColor" : "#1E1E1E"
    },
    "labelButton": {
      "color": "#eff0f4",
      "textColor": "#000000",
      "textFontFace": "Regular",
      "cornerRadius": "3.0",
      "textSize": "12",
      "height": "30"
    },
    "secondaryButton": {
      "useNavigationArrow": "false",
      "width": "WRAP_CONTENT",
      "height": "24",
      "buttonAlignment": "LEFT",
      "buttonBackground": "",
      "buttonCornerRadius": "",
      "textColor": "#000000",
      "textSize": "15",
      "fontFace": "semibold"
    },
    "attachPOHeaders": "false",
    "upiCollectWithGodel": "false",
    "verifyMobile": "true",
    "useSeperatedSections": "false",
    "verifyVpa": "false",
    "language": "english",
    "labelBackground": "#EBEBEB",
    "topRowVisibility": "false",
    "showSearchBoxStroke": "true",
    "showProcessingPayment": "true",
    "bottomMargin": "16",
    "widgets": {
      "allowed": ["offers", "quickPay", "paymentPage", "delinkWallet"],
      "primary": "quickPay"
    },
    "useQuickPayRanking": "false",
    "paymentOptionFontFace": "regular",
    "gridProps": {
      "useTick": "true",
      "useStroke": "true",
      "itemSize": "60",
      "gridSelectedStroke": "2,#54A624",
      "gridLogoStroke": "2,#aaaaaa",
      "gridViewBackground": "#ffffff",
      "horizontalFade": "true",
      "fadingEdgeLength": "150",
      "useButtonForSelection": "true",
      "strokeCornerRadius": "4.0",
      "gridFontSize": "14",
      "gridIconSize": "25"
    },
    "mandateViewProps": {
      "textColor" : "#898989",
      "backgroundColor" : "#f8f8f8",
      "cornerRadius" : "4.0",
      "padding" : { "left" : "" , "right" : "" , "vertical" : ""  },
      "textSize" : "12"
      },
    "offerViewProps": {
      "textColor" : "#898989",
      "backgroundColor" : "#f8f8f8",
      "cornerRadius" : "4.0",
      "padding" : { "left" : "" , "right" : "" , "vertical" : ""  },
      "textSize" : "12"
    },
    "mandateInstruments" : [],
    "highlightMessage" : {
      "textColor" : "#000000",
      "backgroundColor" : "#f0f0f0",
      "textFontFace" : "Regular",
      "padding" : { "vertical" : "10" , "horizontal" : "14" }
    },
    "outageViewProps": {
      "showOutageView" : "true",
      "textColor" : "#FF1E0C",
      "backgroundColor" : "#ffffff",
      "cornerRadius" : "4.0",
      "padding" : { "left" : "0" , "right" : "0" , "vertical" : "0"  },
      "textSize" : "12",
      "outageMessage" : "Experiencing low success rates for",
      "restrictPayment" : "false"
    }
  };

  return JSON.stringify(olaConfig);
}

var hyperBetaSDKConfig = function () {
  if(window.getUIConfig){
    console.log("loading config from ui_config");
    return window.getUIConfig();
  }

  var hyperBetaConfigPayload = {
    "backgroundColor": "#f5f6f8",
    "checkboxSize": "18",
    "combineWallets": "false",
    "expandPopularNBView": "true",
    "expandUpiView": "false",
    "expandedWalletView": "true",
    "fontColor": "#131313",
    "tertiaryFontColor": "#00b137",
    "fontSize": "14",
    "highlight": [],
    "highlightViewType": "list",
    "iconSize": "38",
    "buttonText": "Proceed to Pay",
    "lineSeparator": "true",
    "lineSeparatorColor": "#FFFFFF",
    "listItemHeight": "60",
    "modalView": "false",
    "moreOption": {
      "icon": "wallet_icon",
      "name": "WalletFlow",
      "view": {
        "action": "payWithWallet",
        "content": "editText",
        "footer": "button",
        "toolbar": {
          "back": "VISIBLE",
          "pageTitle": "MoreOptionTitle",
          "textSize": 18,
          "textFontFace": "Bold",
          "textColor": "#333333",
          "drawFromStatusBar": "false",
          "color": "#FBBA19",
          "useImageBackground": "true",
          "translation": "0.0",
          "height": "64"
        }
      },
      "visibility": "gone"
    },
    "offers": "gone",
    "offerOptions" : [
      {
          "group": "others",
          "po": "savedWallets",
          "visibility": "VISIBLE"
      },
      {
          "group": "others",
          "po": "nb",
          "visibility": "VISIBLE"
      },
      {
          "group": "others",
          "po": "upi",
          "visibility": "VISIBLE"
      },
      {
          "group": "others",
          "po": "savedVPAs",
          "visibility": "VISIBLE"
      },
      {
          "group": "others",
          "po": "unlinkedWallets",
          "visibility": "VISIBLE"
      }
    ],
    "paymentOptions": [{
      "group": "others",
      "onlyDisable": [],
      "po": "cards",
      "visibility": "VISIBLE"
    }],
    "popularBanks": [
      "NB_AXIS",
      "NB_ICICI",
      "NB_SBI",
      "NB_OBC"
    ],
    "primaryColor": "#1261d5",
    "primaryFont": "Arial",
    "saved": {
      "otherSaved": "visible",
      "preffered": "visible",
      "saved": "VISIBLE"
    },
    "screenTransition": {
      "curve": [
        "0.1",
        "0.4",
        "0.4",
        "0.9"
      ],
      "duration": "200"
    },
    "toolbar": {
      "back": "VISIBLE",
      "pageTitle": "Payment Methods",
      "textSize": 18,
      "textFontFace": "Bold",
      "textColor": "#ffffff",
      "drawFromStatusBar": "true",
      "color": "#1261d5",
      "useImageBackground": "false",
      "translation": "0.0",
      "height": "64"
    },
    "amountBar": {
      "amountBarData": [{
        "leftString": "Payable Amount",
        "textFontFace": "SemiBold",
        "fontSize": "14",
        "color": "#000000"
      }],
      "placeAmountAtRight": "true",
      "amountTextSize": "20",
      "attachOnTop": "true",
      "attachOnAllScreens": "true",
      "visibility": "visible",
      "verticalPadding": "8",
      "translation": "5.0",
      "color": "#ffffff",
      "addDivider": "false",
      "dividerColor": "#000000"
    },
    "button": {
      "background": "#ffffff",
      "color": "#1261d5",
      "useGradient": "false",
      "gradient": {
        "from": "",
        "to": "",
        "direction": "HORIZONTAL"
      },
      "atBottom": "true",
      "cornerRadius": "5.0",
      "changeText": "true",
      "translation": "0.0"
    },
    "headerProps": {
      "size": "16",
      "color": "#333333",
      "fontFace": "Bold",
      "bottomMargin": "0"
    },
    "containerAttribs": {
      "horizontalSpacing": "12",
      "verticalSpacing": "10",
      "sectionSpacing": "12"
    },
    "uiCard": {
      "translation": "4.0",
      "cornerRadius": "5.0",
      "horizontalPadding": "12",
      "verticalPadding": "16",
      "color": "#ffffff"
    },
    "inputField": {
      "type": "boxed",
      "useMaterialView": "false",
      "cornerRadius": "4.0",
      "focusColor": "#1261d5",
      "textPadding": "5",
      "background": "#ffffff",
      "fontStyle": "Bold",
      "labelFontColor": "#131313",
      "labelFontFace" : "Bold",
      "labelFontSize" : "12"
    },
    "secondaryButton": {
      "useNavigationArrow": "false",
      "width": "WRAP_CONTENT",
      "height": "27",
      "buttonAlignment": "LEFT",
      "buttonBackground": "#ebebeb",
      "buttonCornerRadius": "12.0",
      "textColor": "#000000",
      "textSize": "15",
      "fontFace": "Regular"
    },
    "labelButton": {
      "color": "#ebebeb",
      "textColor": "#000000",
      "textFontFace": "Regular",
      "cornerRadius": "12.0",
      "textSize": "15",
      "height": "27"
    },
    "attachPOHeaders": "true",
    "upiCollectWithGodel": "false",
    "verifyMobile": "true",
    "useSeperatedSections": "true",
    "verifyVpa": "false",
    "language": "english",
    "labelBackground": "#EBEBEB",
    "topRowVisibility": "false",
    "showSearchBoxStroke": "true",
    "showProcessingPayment": "true",
    "bottomMargin": "16",
    "widgets": {
      "allowed": ["quickPay", "paymentPage"],
      "primary": "quickPay"
    },
    "useQuickPayRanking": "false",
    "paymentOptionFontFace": "Regular",
    "gridProps": {
      "useTick": "false",
      "useStroke": "false",
      "itemSize": "60",
      "gridSelectedStroke": "",
      "gridLogoStroke": "",
      "useButtonForSelection": "false",
      "gridViewBackground": "#ffffff",
      "horizontalFade": "true",
      "fadingEdgeLength": "150",
      "strokeCornerRadius": "",
      "gridFontSize": "14",
      "gridIconSize": "38"
    },
    "mandateViewProps": {
      "textColor" : "#898989",
      "backgroundColor" : "#f8f8f8",
      "cornerRadius" : "4.0",
      "padding" : { "left" : "" , "right" : "" , "vertical" : ""  },
      "textSize" : "12"
    },
    "offerViewProps": {
      "textColor" : "#898989",
      "backgroundColor" : "#f8f8f8",
      "cornerRadius" : "4.0",
      "padding" : { "left" : "" , "right" : "" , "vertical" : ""  },
      "textSize" : "12"
    },
    "mandateInstruments" : [],
    "highlightMessage" : {
      "textColor" : "#000000",
      "backgroundColor" : "#f0f0f0",
      "textFontFace" : "Regular",
      "padding" : { "vertical" : "10" , "horizontal" : "14" }
    },
    "outageViewProps": {
      "showOutageView" : "true",
      "textColor" : "#FF1E0C",
      "backgroundColor" : "#ffffff",
      "cornerRadius" : "4.0",
      "padding" : { "left" : "0" , "right" : "0" , "vertical" : "0"  },
      "textSize" : "12",
      "outageMessage" : "Experiencing low success rates for",
      "restrictPayment" : "false"
    }
  };
  return JSON.stringify(hyperBetaConfigPayload);
}

exports["getCureFitConfig"] = curefitConfig;
exports["getIdeaConfig"] = ideaConfig;
exports["getJiosaavnConfig"] = jiosaavnConfig;
exports["getOlaConfig"] = olaConfigPayload;
exports["getHyperBetaSDKConfig"] = hyperBetaSDKConfig;
exports["getZee5Config"] = zee5config;
// var common = require("./../Engineering.Helpers.Commons/foreign.js");
var config =  require("./../../config_values/getConfig.js");
var qrC = require("qr-generator");

var getQRData=  function(str){
    var typeNumber = 10;
    var errorCorrectionLevel = 'H';
    var qr = qrC(typeNumber, errorCorrectionLevel);
    qr.addData(str);
    qr.make();
    var a = (qr.createImgTag())
    a=a.substring(10)
    a=a.split('"')

    return a[0]
}
exports["getQRData"]=getQRData
var getMerchantName = function(mName){
    mName=mName.split("_");
    mName.pop();
    mName=mName.join("_")
    mName=mName.charAt(0).toUpperCase() + mName.slice(1);
    return mName;
}

exports ["unwordWithSeparator"] = function(array){
    return function(separator){
        var str = "";
        for (var i=0;i< array.length; i++){
            str += array[i];
            if(i<array.length-1){
                str+= separator;
            }
        }
        return str;
    }
}

exports["getCustomConfig"] = function () {
    var merchantName = window.__payload.merchant_ui;

    var configPayload;
    var parsedConfigPayload;
    // configPayload = customConfig();
    // parsedConfigPayload = JSON.parse(configPayload);
    if (merchantName.indexOf("web") != -1) {
        merchantName = getMerchantName(merchantName)
    }
    merchantName =merchantName.toLowerCase()
    // common.getConfig(merchantName)
};


exports.someFFI = function(cb){
    return function(){

    var merchantName = window.__payload.merchant_ui;
    var configPayload;
    var parsedConfigPayload;

    if (merchantName.indexOf("web") != -1) {
        merchantName = getMerchantName(merchantName)
    }
    merchantName =merchantName.toLowerCase()
    parsedConfigPayload = config.getConfig(merchantName).then(function (val) {
        console.warn("got value", val)
        cb(JSON.stringify(val))()
    });
    return;
    }
}


function capsFirstLetter(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
}

exports['getIconClose'] = iconClose;
var iconClose = function(color){
    return 'data:image/svg+xml;utf8,<svg width="31" height="30" viewBox="0 0 31 30" fill="none" xmlns="http://www.w3.org/2000/svg"> <g opacity="0.65"> <path d="M24.25 8.0125L22.4875 6.25L15.5 13.2375L8.5125 6.25L6.75 8.0125L13.7375 15L6.75 21.9875L8.5125 23.75L15.5 16.7625L22.4875 23.75L24.25 21.9875L17.2625 15L24.25 8.0125Z" fill="black"/> </g> </svg>'
}

var getImageUrl = function (color) {
    color = encodeURIComponent(color);
    return 'data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="' + color + '" d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm-1.25 16.518l-4.5-4.319 1.396-1.435 3.078 2.937 6.105-6.218 1.421 1.409-7.5 7.626z"/></svg>'
}
exports['getTick'] = getImageUrl;

var getBackIcon1 = function (color) {
    color = encodeURIComponent(color);
    return 'data:image/svg+xml;utf8,<svg width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd"><path fill="' + color + '" d="M2.117 12l7.527 6.235-.644.765-9-7.521 9-7.479.645.764-7.529 6.236h21.884v1h-21.883z"/></svg>';
}
exports['getBackIcon1'] = getBackIcon1;
var getBackIcon2 = function (color) {
    color = encodeURIComponent(color);
    return 'data:image/svg+xml;utf8,<svg width="15" height="24" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd"><path fill="' + color + '" d="M20 .755l-14.374 11.245 14.374 11.219-.619.781-15.381-12 15.391-12 .609.755z"/></svg>';
}
exports['getBackIcon2'] = getBackIcon2;



var getMobileTopIcon = function (color) {
    return function (sat) {
        return function (backcolor) {
           return 'data:image/svg+xml;utf8,'+encodeURIComponent('<svg width="375px" height="65px" viewBox="0 0 375 65" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> <!-- Generator: Sketch 60.1 (88133) - https://sketch.com --> <title>Oval Copy + Oval Copy 4 Mask</title> <desc>Created with Sketch.</desc> <defs> <rect id="path-1" x="0" y="0" width="375" height="65"></rect> <ellipse id="path-3" cx="211" cy="96.259542" rx="211" ry="96.259542"></ellipse> </defs> <g id="Final-File" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"> <g id="Idea_Payment-Page_Change" transform="translate(0.000000, -23.000000)"> <g id="Group-7" transform="translate(0.000000, 6.000000)"> <g id="Group-64-Copy-2" transform="translate(0.000000, 17.000000)"> <g id="Group-62"> <g id="Oval-Copy-+-Oval-Copy-4-Mask"> <mask id="mask-2" fill="white"> <use xlink:href="#path-1"></use> </mask> <use id="Mask" fill="#D8D8D8" xlink:href="#path-1"></use> <g mask="url(#mask-2)"> <g transform="translate(-24.000000, -73.931298)"> <mask id="mask-4" fill="white"> <use xlink:href="#path-3"></use> </mask> <use id="Mask" stroke="none" fill="#FEF200" fill-rule="evenodd" xlink:href="#path-3"></use> <path d="M363.800628,171.577176 C435.252165,167.814209 591.575034,104.869962 595.26148,69.5581488 C598.947926,34.2463356 268.305462,57.1789679 196.853925,60.9419352 C125.402388,64.7049025 162.258088,108.136111 182.215441,124.18687 C202.172794,140.237629 292.349091,175.340143 363.800628,171.577176 Z" id="Oval-Copy" stroke="none" fill="#FBBA19" fill-rule="evenodd" opacity="0.214518229" mask="url(#mask-4)" transform="translate(375.033621, 111.005048) rotate(6.000000) translate(-375.033621, -111.005048) "></path> <path d="M520.473351,247.717508 C591.924888,243.954541 748.247758,181.010294 751.934204,145.698481 C755.62065,110.386668 424.978186,133.3193 353.526649,137.082267 C282.075112,140.845234 318.930812,184.276443 338.888165,200.327202 C358.845517,216.377961 449.021814,251.480475 520.473351,247.717508 Z" id="Oval-Copy-4" stroke="none" fill="#F57C00" fill-rule="evenodd" opacity="0.1" mask="url(#mask-4)" transform="translate(531.706345, 187.145380) rotate(6.000000) translate(-531.706345, -187.145380) "></path> </g> </g> </g> <rect id="Rectangle-Copy-3" stroke="#979797" fill="#FFFFFF" opacity="0" x="327.5" y="59.5" width="23" height="11"></rect> </g> </g> <g id="UI/Bars/Status/Black"> <g id="Pin-Right" stroke-width="1" fill-rule="evenodd" transform="translate(298.000000, 3.000000)"> <g id="Battery" transform="translate(9.000000, 0.000000)"> <g transform="translate(36.000000, 1.500000)"> <path d="M3.2048565,0.5 C2.26431807,0.5 1.89540921,0.571239588 1.5147423,0.774822479 C1.19446913,0.946106445 0.946106445,1.19446913 0.774822479,1.5147423 C0.571239588,1.89540921 0.5,2.26431807 0.5,3.2048565 L0.5,8.2951435 C0.5,9.23568193 0.571239588,9.60459079 0.774822479,9.9852577 C0.946106445,10.3055309 1.19446913,10.5538936 1.5147423,10.7251775 C1.89540921,10.9287604 2.26431807,11 3.2048565,11 L22.0738202,11 C22.8614775,11 23.5,10.3614775 23.5,9.57382015 L23.5,3.2048565 C23.5,2.26431807 23.4287604,1.89540921 23.2251775,1.5147423 C23.0538936,1.19446913 22.8055309,0.946106445 22.4852577,0.774822479 C22.1045908,0.571239588 21.7356819,0.5 20.7951435,0.5 L3.2048565,0.5 Z" id="Border" stroke="#000000" opacity="0.400000006"></path> <path d="M25.0004626,4.00011912 C25.8628415,4.22230136 26.5,5.00523813 26.5,5.93699126 C26.5,6.86874438 25.8628415,7.65168116 25.0004626,7.8738634 Z" id="Nub" fill="#000000" opacity="0.400000006"></path> <path d="M2.5,2 L21.5,2 C21.7761424,2 22,2.22385763 22,2.5 L22,9 C22,9.27614237 21.7761424,9.5 21.5,9.5 L2.5,9.5 C2.22385763,9.5 2,9.27614237 2,9 L2,2.5 C2,2.22385763 2.22385763,2 2.5,2 Z" id="Charge" fill="#000000"></path> </g> <text id="100%" font-family="Helvetica" font-size="12" font-weight="normal" fill="#030303"> <tspan x="2.30859375" y="11.5">100%</tspan> </text> </g> <polyline id="Bluetooth" stroke="#000000" points="0.5 4 6.5 9.5 3.5 12 3.5 2 6.5 4.5 0.5 10"></polyline> </g> <text id="Time" font-family="Helvetica" font-size="12" font-weight="normal" fill="#030303"> <tspan x="165.155273" y="14.5">9:41 AM</tspan> </text> <g id="Overrides/Status-Bar/Signal---Black" fill-rule="evenodd" stroke-width="1"> <g id="Group" transform="translate(6.000000, 3.000000)"> <text id="Carrier" font-family="Helvetica" font-size="12" font-weight="normal" fill="#030303"> <tspan x="20" y="11.5">VIL</tspan> </text> <path d="M2,7.5 C2.55228475,7.5 3,7.94771525 3,8.5 L3,11 C3,11.5522847 2.55228475,12 2,12 L1,12 C0.44771525,12 6.76353751e-17,11.5522847 0,11 L0,8.5 C-6.76353751e-17,7.94771525 0.44771525,7.5 1,7.5 L2,7.5 Z M6.5,6 C7.05228475,6 7.5,6.44771525 7.5,7 L7.5,11 C7.5,11.5522847 7.05228475,12 6.5,12 L5.5,12 C4.94771525,12 4.5,11.5522847 4.5,11 L4.5,7 C4.5,6.44771525 4.94771525,6 5.5,6 L6.5,6 Z M11,4 C11.5522847,4 12,4.44771525 12,5 L12,11 C12,11.5522847 11.5522847,12 11,12 L10,12 C9.44771525,12 9,11.5522847 9,11 L9,5 C9,4.44771525 9.44771525,4 10,4 L11,4 Z M15.5,2 C16.0522847,2 16.5,2.44771525 16.5,3 L16.5,11 C16.5,11.5522847 16.0522847,12 15.5,12 L14.5,12 C13.9477153,12 13.5,11.5522847 13.5,11 L13.5,3 C13.5,2.44771525 13.9477153,2 14.5,2 L15.5,2 Z" id="Mobile-Signal" fill="#000000"></path> <path d="M65.3295628,4.82956276 C67.2063161,3.07441257 69.7276868,2 72.5,2 C75.2723132,2 77.7936839,3.07441257 79.6704372,4.82956276 L78.2552384,6.24476162 C76.7412564,4.85107918 74.7200183,4 72.5,4 C70.2799817,4 68.2587436,4.85107918 66.7447616,6.24476162 L65.3295628,4.82956276 L65.3295628,4.82956276 Z M67.8065309,7.30653087 C69.0481225,6.18377399 70.6942093,5.5 72.5,5.5 C74.3057907,5.5 75.9518775,6.18377399 77.1934691,7.30653087 L75.7767384,8.72326155 C74.8991992,7.96124278 73.7534641,7.5 72.5,7.5 C71.2465359,7.5 70.1008008,7.96124278 69.2232616,8.72326155 L67.8065309,7.30653087 Z M70.2877088,9.78770884 C70.890654,9.29532392 71.6608387,9 72.5,9 C73.3391613,9 74.109346,9.29532392 74.7122912,9.78770884 L72.5,12 L70.2877088,9.78770884 L70.2877088,9.78770884 Z" id="Wifi" fill="#000000"></path> </g> </g> </g> </g> </g> </g> </svg>') }
    }
}
exports['getMobileTopIcon'] = getMobileTopIcon;

var getCardIcon = function (color) {
    color = encodeURIComponent(color);
    return 'data:image/svg+xml;utf8,<svg width="20" height="16" viewBox="0 0 20 16" fill="none" xmlns="http://www.w3.org/2000/svg"><rect opacity="0.3" x="2" y="2" width="16" height="2" fill="' + color + '"/><rect opacity="0.3" x="2" y="8" width="16" height="6" fill="' + color + '"/><path fill-rule="evenodd" clip-rule="evenodd" d="M18 0H2C0.89 0 0.01 0.89 0.01 2L0 14C0 15.11 0.89 16 2 16H18C19.11 16 20 15.11 20 14V2C20 0.89 19.11 0 18 0ZM18 14H2V8H18V14ZM2 4H18V2H2V4Z" fill="' + color + '"/></svg>';
}
exports['getCardIcon'] = getCardIcon;

var getUpiIcon = function (leftColor) {
    var c1 = encodeURIComponent("#919294")
    var color = encodeURIComponent(leftColor)
    return 'data:image/svg+xml;utf8, <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M15.7492 1L9.5 23L21.4167 12.003L15.7492 1Z" fill="' + color + '"/><path d="M10.4949 3.4379L14.803 11.806L5.73999 20.1696L10.4949 3.4379Z" fill="' + c1 + '" stroke="' + color + '" stroke-width="1.8"/></svg>'
}
exports['getUpiIcon'] = getUpiIcon;

var getWalletIcon = function (color) {
    var c4 = encodeURIComponent(color)//"hsl(" + color + ", " + sat + "%, 22.4%)";
    return 'data:image/svg+xml;utf8, ' +encodeURIComponent('<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><g opacity="0.9"><path opacity="0.3" fill-rule="evenodd" clip-rule="evenodd" d="M12 4C7.59 4 4 7.59 4 12C4 16.41 7.59 20 12 20C16.41 20 20 16.41 20 12C20 7.59 16.41 4 12 4ZM9 10.5V13H13V15H9V17.5L5.5 14L9 10.5ZM15 11V13.5L18.5 10L15 6.5V9H11V11H15Z" fill="'+ color +'"/><path fill-rule="evenodd" clip-rule="evenodd" d="M2 12C2 6.48 6.48 2 12 2C17.52 2 22 6.48 22 12C22 17.52 17.52 22 12 22C6.48 22 2 17.52 2 12ZM4 12C4 16.41 7.59 20 12 20C16.41 20 20 16.41 20 12C20 7.59 16.41 4 12 4C7.59 4 4 7.59 4 12Z" fill="'+ color +'"/><path d="M15 6.5V9H11V11H15V13.5L18.5 10L15 6.5Z" fill="'+ color +'"/><path d="M9 10.5L5.5 14L9 17.5V15H13V13H9V10.5Z" fill="'+ color +'"/></g></svg>')
}
exports['getWalletIcon'] = getWalletIcon;


var getNetBankingIcon = function (color) {
    var color = encodeURIComponent(color)
    return 'data:image/svg+xml;utf8, <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><g opacity="0.9"><path opacity="0.3" d="M6.29004 6.00001L11.5 3.26001L16.71 6.00001H6.29004Z" fill="black"/><path d="M6.5 10H4.5V17H6.5V10Z" fill="'+ color +'"/><path d="M12.5 10H10.5V17H12.5V10Z" fill="'+ color +'"/><path d="M21 19H2V21H21V19Z" fill="'+ color +'"/><path d="M18.5 10H16.5V17H18.5V10Z" fill="'+ color +'"/><path fill-rule="evenodd" clip-rule="evenodd" d="M2 6L11.5 1L21 6V8H2V6ZM11.5 3.26L6.29 6H16.71L11.5 3.26Z" fill="'+ color +'"/></g></svg>'
}
exports['getNetBankingIcon'] = getNetBankingIcon;

var getJuspayLogoIcon = function (color) {
    var c1 = encodeURIComponent("#0099FF");
    var c2 = encodeURIComponent("#0585DD");
    return 'data:image/svg+xml;utf8, <svg width="16px" height="16px" viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> <!-- Generator: Sketch 61.2 (89653) - https://sketch.com --> <title>Logo</title> <desc>Created with Sketch.</desc> <g id="Logo" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"> <path d="M8.01942728,16.0044604 C7.63398677,16.0151296 7.25925296,15.9831217 6.87381245,15.9297752 C5.83526445,15.780405 4.86095651,15.4496566 3.96159534,14.9055221 C1.97015275,13.7105602 0.696057771,11.9714638 0.171430422,9.68823292 C0.0536569366,9.17610638 0.000123533749,8.65331053 0.000123533749,8.13051468 C-0.0105831468,6.03933128 0.67464441,4.20421116 2.06651289,2.6464929 C3.27636779,1.30216072 4.77530306,0.480624387 6.54190536,0.13920669 C7.03441266,0.0431829625 7.52691998,-0.0101635527 8.01942728,0.0111750533 L8.01942728,2.40109893 C7.99801391,2.41176825 7.97660056,2.42243755 7.95518719,2.44377614 C7.66610682,2.6464929 7.37702644,2.85987896 7.09865275,3.08393434 C6.46695859,3.59606087 5.86738448,4.14019534 5.3641705,4.79102281 C4.68964962,5.65523637 4.23996904,6.60480434 4.17572895,7.72508116 C4.14360891,8.25854631 4.207849,8.78134216 4.37915588,9.28279941 C4.79671643,10.5311079 6.09222478,11.8220935 7.91236047,11.8541014 C8.01942728,11.8541014 8.04084063,11.8754401 8.04084063,11.9928024 C8.03013395,12.5369368 8.03013395,13.6252057 8.03013395,13.6252057 L8.03013395,13.7212295 C8.01942728,14.47875 8.01942728,15.2362705 8.01942728,16.0044604 L8.01942728,16.0044604 Z" id="Shape" fill="' + c1 + '" fill-rule="nonzero"></path> <path d="M8.00025332,2.39214474 L8.00025332,0.00163433768 C8.43906876,-0.00903758369 8.86718138,0.0336501019 9.29529399,0.0976816309 C10.0980052,0.225744687 10.8686079,0.460526959 11.5963993,0.83404421 C13.1054963,1.61309448 14.2935088,2.7229743 15.09622,4.23838715 C15.5564411,5.11348471 15.8561199,6.04194187 15.9738508,7.01308672 C16.1129875,8.15498231 16.0059594,9.28620599 15.6634693,10.3854139 C15.2674651,11.6340287 14.6038905,12.7332366 13.6727455,13.6616938 C12.5382471,14.7822455 11.1896923,15.5079362 9.62708127,15.8387657 C9.0919405,15.9454849 8.54609692,16.0095165 8.00025332,15.9988446 L8.00025332,13.7043815 L8.00025332,13.6190061 C8.00025332,13.6190061 8.03236177,13.5976623 8.04306458,13.5869903 C8.87788419,13.0000346 9.66989255,12.3597194 10.3548727,11.591341 C10.804391,11.0897607 11.1896923,10.5561646 11.4572627,9.93719319 C11.8318613,9.06209563 11.9817007,8.16565424 11.78905,7.21585323 C11.4465599,5.48700195 9.89465166,4.19569946 8.10728148,4.15301177 C7.98955051,4.15301177 8.00025332,4.05696448 8.00025332,4.05696448 L8.00025332,2.39214474 L8.00025332,2.39214474 Z" id="Shape" fill="' + c2 + '" fill-rule="nonzero"></path> </g> </svg>'
}

exports['getJuspayLogoIcon'] = getJuspayLogoIcon;


var upiWaitingLogo = function(){
    var url = '<svg width="180" height="180" viewBox="0 0 180 180" fill="none" xmlns="http://www.w3.org/2000/svg"><g filter="url(#filter0_iiii)"><circle cx="90" cy="90" r="80" stroke="#EAF8DD" stroke-width="20"/></g><g filter="url(#filter1_iiii)"><mask id="path-2-inside-1" fill="white"><path d="M174.42 58.8039C168.054 41.5756 156.569 26.7075 141.507 16.1961C126.445 5.68463 108.528 0.0330398 90.1612 0.000144348L90 90L174.42 58.8039Z"/></mask><path d="M174.42 58.8039C168.054 41.5756 156.569 26.7075 141.507 16.1961C126.445 5.68463 108.528 0.0330398 90.1612 0.000144348L90 90L174.42 58.8039Z" stroke="#6AC600" stroke-width="40" mask="url(#path-2-inside-1)"/></g><circle cx="90" cy="90" r="70" fill="white"/><g filter="url(#filter2_d)"><circle cx="90" cy="90" r="51" fill="#FCFEFA"/><circle cx="90" cy="90" r="50.5" stroke="#F4FBED"/></g><path d="M75.2917 91V74.992H72.3157L67.2757 80.056L69.2197 82.096L71.8837 79.384V91H75.2917ZM79.9509 83.128C81.0069 83.128 81.8949 82.24 81.8949 81.184C81.8949 80.128 81.0069 79.24 79.9509 79.24C78.8949 79.24 78.0069 80.128 78.0069 81.184C78.0069 82.24 78.8949 83.128 79.9509 83.128ZM79.9509 91.264C81.0069 91.264 81.8949 90.376 81.8949 89.32C81.8949 88.264 81.0069 87.376 79.9509 87.376C78.8949 87.376 78.0069 88.264 78.0069 89.32C78.0069 90.376 78.8949 91.264 79.9509 91.264ZM90.1292 91.288C93.9932 91.288 96.5372 89.128 96.5372 85.744C96.5372 82.504 94.1132 80.56 91.4012 80.56C89.9372 80.56 88.6412 81.136 87.8732 81.904V77.992H95.4572V74.992H84.4652V84.16L86.8412 84.808C87.8012 83.896 88.8812 83.512 90.2012 83.512C92.0252 83.512 93.0812 84.472 93.0812 85.888C93.0812 87.184 92.0012 88.264 90.1052 88.264C88.4252 88.264 86.9372 87.64 85.8572 86.512L83.9612 88.888C85.3292 90.376 87.3452 91.288 90.1292 91.288ZM103.699 91.264C108.427 91.264 111.163 87.832 111.163 82.984C111.163 78.472 109.123 74.728 104.251 74.728C100.603 74.728 98.1307 77.104 98.1307 80.224C98.1307 83.632 100.699 85.432 103.579 85.432C105.331 85.432 106.939 84.448 107.707 83.368C107.707 83.488 107.707 83.608 107.707 83.728C107.707 86.008 106.339 88.24 103.699 88.24C102.283 88.24 101.371 87.808 100.507 86.992L98.9947 89.608C100.123 90.616 101.755 91.264 103.699 91.264ZM104.539 82.504C103.051 82.504 101.587 81.784 101.587 80.104C101.587 79 102.595 77.752 104.443 77.752C106.675 77.752 107.539 79.456 107.683 80.992C106.915 81.952 105.763 82.504 104.539 82.504Z" fill="#70CC00"/><path d="M77.4775 106H78.4675V102.161C78.4675 101.094 77.9395 100.555 76.9605 100.555C76.1795 100.555 75.4755 101.05 75.1675 101.545C75.0025 100.973 74.5295 100.555 73.7265 100.555C72.9345 100.555 72.2305 101.105 72.0105 101.435V100.687H71.0205V106H72.0105V102.205C72.2745 101.82 72.7915 101.435 73.3085 101.435C73.9795 101.435 74.2435 101.842 74.2435 102.458V106H75.2335V102.194C75.4865 101.809 76.0145 101.435 76.5535 101.435C77.2025 101.435 77.4775 101.842 77.4775 102.458V106ZM80.528 99.796C80.88 99.796 81.166 99.521 81.166 99.169C81.166 98.817 80.88 98.531 80.528 98.531C80.187 98.531 79.89 98.817 79.89 99.169C79.89 99.521 80.187 99.796 80.528 99.796ZM80.044 106H81.034V100.687H80.044V106ZM86.1973 106H87.1873V102.271C87.1873 101.149 86.6043 100.555 85.4823 100.555C84.6573 100.555 83.9533 101.006 83.6013 101.435V100.687H82.6113V106H83.6013V102.205C83.8873 101.809 84.4483 101.435 85.0643 101.435C85.7463 101.435 86.1973 101.721 86.1973 102.601V106ZM92.3419 106H93.3319V100.687H92.3419V104.504C92.0449 104.9 91.4839 105.252 90.8679 105.252C90.1859 105.252 89.7459 104.988 89.7459 104.108V100.687H88.7559V104.438C88.7559 105.56 89.3169 106.132 90.4499 106.132C91.2639 106.132 91.9349 105.714 92.3419 105.274V106ZM96.3634 106.132C96.8804 106.132 97.1884 105.989 97.3974 105.791L97.1444 105.043C97.0454 105.153 96.8364 105.252 96.6054 105.252C96.2644 105.252 96.0884 104.977 96.0884 104.603V101.556H97.1664V100.687H96.0884V99.235H95.0984V100.687H94.2184V101.556H95.0984V104.823C95.0984 105.659 95.5274 106.132 96.3634 106.132ZM97.9327 103.338C97.9327 105.01 99.0987 106.132 100.683 106.132C101.541 106.132 102.289 105.857 102.817 105.34L102.355 104.691C101.959 105.098 101.343 105.318 100.782 105.318C99.7147 105.318 99.0547 104.57 98.9777 103.668H103.18V103.426C103.18 101.798 102.19 100.555 100.595 100.555C99.0437 100.555 97.9327 101.798 97.9327 103.338ZM100.595 101.369C101.717 101.369 102.19 102.249 102.212 102.953H98.9667C99.0217 102.227 99.5277 101.369 100.595 101.369ZM103.956 105.307C104.495 105.857 105.276 106.132 106.167 106.132C107.564 106.132 108.312 105.417 108.312 104.504C108.312 103.283 107.212 103.03 106.288 102.821C105.628 102.667 105.056 102.513 105.056 102.051C105.056 101.622 105.474 101.358 106.145 101.358C106.816 101.358 107.41 101.633 107.718 102.007L108.158 101.314C107.707 100.885 107.047 100.555 106.134 100.555C104.836 100.555 104.11 101.281 104.11 102.128C104.11 103.272 105.166 103.514 106.068 103.723C106.75 103.877 107.355 104.053 107.355 104.581C107.355 105.032 106.959 105.34 106.211 105.34C105.496 105.34 104.792 104.977 104.429 104.581L103.956 105.307Z" fill="#70CC00"/><g filter="url(#filter3_d)"><circle cx="165" cy="62" r="10" fill="white"/></g><defs><filter id="filter0_iiii" x="-2" y="-2" width="184" height="184" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB"><feFlood flood-opacity="0" result="BackgroundImageFix"/><feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape"/><feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/><feOffset dx="-2"/><feGaussianBlur stdDeviation="1.5"/><feComposite in2="hardAlpha" operator="arithmetic" k2="-1" k3="1"/><feColorMatrix type="matrix" values="0 0 0 0 0.427451 0 0 0 0 0.792157 0 0 0 0 0 0 0 0 0.12 0"/><feBlend mode="normal" in2="shape" result="effect1_innerShadow"/><feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/><feOffset dx="2"/><feGaussianBlur stdDeviation="1.5"/><feComposite in2="hardAlpha" operator="arithmetic" k2="-1" k3="1"/><feColorMatrix type="matrix" values="0 0 0 0 0.427451 0 0 0 0 0.792157 0 0 0 0 0 0 0 0 0.12 0"/><feBlend mode="normal" in2="effect1_innerShadow" result="effect2_innerShadow"/><feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/><feOffset dy="-2"/><feGaussianBlur stdDeviation="1.5"/><feComposite in2="hardAlpha" operator="arithmetic" k2="-1" k3="1"/><feColorMatrix type="matrix" values="0 0 0 0 0.427451 0 0 0 0 0.792157 0 0 0 0 0 0 0 0 0.12 0"/><feBlend mode="normal" in2="effect2_innerShadow" result="effect3_innerShadow"/><feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/><feOffset dy="2"/><feGaussianBlur stdDeviation="1.5"/><feComposite in2="hardAlpha" operator="arithmetic" k2="-1" k3="1"/><feColorMatrix type="matrix" values="0 0 0 0 0.427451 0 0 0 0 0.792157 0 0 0 0 0 0 0 0 0.12 0"/><feBlend mode="normal" in2="effect3_innerShadow" result="effect4_innerShadow"/></filter><filter id="filter1_iiii" x="88" y="-1.99986" width="88.4204" height="93.9999" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB"><feFlood flood-opacity="0" result="BackgroundImageFix"/><feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape"/><feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/><feOffset dx="-2"/><feGaussianBlur stdDeviation="1.5"/><feComposite in2="hardAlpha" operator="arithmetic" k2="-1" k3="1"/><feColorMatrix type="matrix" values="0 0 0 0 0.427451 0 0 0 0 0.792157 0 0 0 0 0 0 0 0 0.12 0"/><feBlend mode="normal" in2="shape" result="effect1_innerShadow"/><feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/><feOffset dx="2"/><feGaussianBlur stdDeviation="1.5"/><feComposite in2="hardAlpha" operator="arithmetic" k2="-1" k3="1"/><feColorMatrix type="matrix" values="0 0 0 0 0.427451 0 0 0 0 0.792157 0 0 0 0 0 0 0 0 0.12 0"/><feBlend mode="normal" in2="effect1_innerShadow" result="effect2_innerShadow"/><feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/><feOffset dy="-2"/><feGaussianBlur stdDeviation="1.5"/><feComposite in2="hardAlpha" operator="arithmetic" k2="-1" k3="1"/><feColorMatrix type="matrix" values="0 0 0 0 0.427451 0 0 0 0 0.792157 0 0 0 0 0 0 0 0 0.12 0"/><feBlend mode="normal" in2="effect2_innerShadow" result="effect3_innerShadow"/><feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/><feOffset dy="2"/><feGaussianBlur stdDeviation="1.5"/><feComposite in2="hardAlpha" operator="arithmetic" k2="-1" k3="1"/><feColorMatrix type="matrix" values="0 0 0 0 0.427451 0 0 0 0 0.792157 0 0 0 0 0 0 0 0 0.12 0"/><feBlend mode="normal" in2="effect3_innerShadow" result="effect4_innerShadow"/></filter><filter id="filter2_d" x="24" y="39" width="132" height="133" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB"><feFlood flood-opacity="0" result="BackgroundImageFix"/><feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/><feOffset dy="16"/><feGaussianBlur stdDeviation="7.5"/><feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.08 0"/><feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/><feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/></filter><filter id="filter3_d" x="153" y="52" width="24" height="24" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB"><feFlood flood-opacity="0" result="BackgroundImageFix"/><feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/><feOffset dy="2"/><feGaussianBlur stdDeviation="1"/><feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.08 0"/><feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/><feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/></filter></defs></svg>'

    return 'data:image/svg+xml;utf8,'+ encodeURIComponent(url)
}

exports['upiWaitingLogo']=upiWaitingLogo;

var getIcBankIcon = function (color) {
    color = encodeURIComponent(color);
    return '<?xml version="1.0" encoding="UTF-8"?> <svg width="32px" height="32px" viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> <!-- Generator: Sketch 61.1 (89650) - https://sketch.com --> <title>ic_logo_hdfc</title> <desc>Created with Sketch.</desc> <g id="Desktop-Web" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"> <g id="Cards-1" transform="translate(-612.000000, -363.000000)"> <rect id="bg" fill="#F3F3F3" x="0" y="182" width="1440" height="842" rx="4"></rect> <g id="card" transform="translate(588.000000, 283.000000)"> <rect id="Rectangle-Copy-29" fill="#FFFFFF" x="0" y="5.68434189e-14" width="587" height="411"></rect> <g id="card-1" transform="translate(24.000000, 77.000000)"> <g id="text" transform="translate(0.000000, 3.000000)"> <image id="ic_logo_hdfc" x="0" y="0" width="32" height="32" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFoAAABaCAYAAAA4qEECAAAABGdBTUEAALGN5fIAKQAAA01JREFUeAHt3Mtr1FAUB+CTMYKSDrVOrSg6LVMEX6ADdQQVF75w5cKFuBD3+reI7kpBcCe484GICnWkYqE+0E2HIhURWmotHUbpQp1HzOCm3GByzeGcjNNfoNCbnEfyzSGLkBnHz2er5FCGOnW7O/6WiodORJ7elUtP6NGDUmRMmgcduuUS+ZvID6g7dWs218WeWqOxMbiOvti4tAL8jNe5k5wWilBfQAvBmmUBbYoIrQEtBGuWBbQpIrQGtBCsWRbQpojQGtBCsGZZQJsiQmtAC8GaZQFtigitAS0Ea5YFtCkitA6e3jE2r2eFhoa/MCrEp/ZtXh8btO/AD5qfm42NSxLQbGRoZrqQJHV1juPne1rBjmSPSc9ffEM3bo6sLtiF/y/TYDbHu67MGG4dPEHrbEBbU/ECAc3zs84GtDUVLxDQPD/rbEBbU/ECAc3zs84GtDUVLxDQPD/rbEBbU/ECAc3zs84GtDUVLxDQPD/rbEBbU/ECec+jeb3pztQneve5yqxil3715G4azHl2wQJRqUI/fD9Htyc/ClxWuOS54s5UoXHrCH8mInsALcIaLgrosInIHkCLsIaLAjpsIrIH0CKs4aKADpuI7HHpXvlFwrc6iLbtCL521vWbR/fLE6yrrH5ddKk4cpxVpPuTN9BBtlEFtw6lQQE0oJUElNpgogGtJKDUBhMNaCUBpTYuzc5MUKuVbLJzAx7l+otK55pWm5/0ofKa1XxxYcmlU6U95PtbEhX688Z/otT/KGmFTh8+xjtfZzrZJPO6rslsQCt97IAGtJKAUhtMNKCVBJTaYKKVoFN9U+ny0WEqFfpVLrUwkFXp87cmqUKf2b+d2n9rYcOtQ+lTBjSglQSU2mCiAa0koNQGEw1oJQGlNphoQCsJKLXBRANaSUCpDSYa0EoCSm0w0UrQvMek44930YWzFdFzvTZWo/zQkcgeo9fL9Pzp1siYpAcb9fYwMn/JkYgH/a3WS1Mve5Neg1Xe8tKzADo69NVkNjiPvdFB6R7FrUPJH9CAVhJQaoOJBrSSgFIbTDSglQSU2mCiAa0koNQGEw1oJQGlNphoQCsJKLUJnt75C0GvX0r9/r1Nvf49SJqPSWz/HGRcTEwJ0cO137EJa3UaCIXZAAAAAElFTkSuQmCC"></image> </g> </g> </g> </g> </g> </svg> '
}
exports['getIcBankIcon'] = getIcBankIcon;

var getGreenPlusIcon = function (color) {
    color = encodeURIComponent(color);
    return function (bgcolor) {
        var c1 = encodeURIComponent(bgcolor);
        var c2 = encodeURIComponent("#F3F3F3");
        var p = encodeURIComponent("#path-1");
        return 'data:image/svg+xml;utf8, <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> <!-- Generator: Sketch 61.1 (89650) - https://sketch.com --> <title>Group 975</title> <desc>Created with Sketch.</desc> <defs> <path d="M10,0 C15.514,0 20,4.486 20,10 C20,15.514 15.514,20 10,20 C4.486,20 0,15.514 0,10 C0,4.486 4.486,0 10,0 Z M10,1 C5.0373,1 1,5.0374 1,10 C1,14.9626 5.0373,19 10,19 C14.9626,19 19,14.9626 19,10 C19,5.0374 14.9626,1 10,1 Z M10,5.125 C10.2761,5.125 10.5,5.2809 10.5,5.4732 L10.5,9.5 L14.5268,9.5 C14.7191,9.5 14.875,9.7239 14.875,10 C14.875,10.2761 14.7191,10.5 14.5268,10.5 L10.5,10.5 L10.5,14.5268 C10.5,14.7191 10.2761,14.875 10,14.875 C9.7239,14.875 9.5,14.7191 9.5,14.5268 L9.5,10.5 L5.4732,10.5 C5.2809,10.5 5.125,10.2761 5.125,10 C5.125,9.7239 5.2809,9.5 5.4732,9.5 L9.5,9.5 L9.5,5.4732 C9.5,5.2809 9.7239,5.125 10,5.125 Z" id="path-1"></path> </defs> <g id="Desktop-Web" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"> <g id="Cards-1" transform="translate(-620.000000, -724.000000)"> <rect id="bg" fill="' + c2 + '" x="0" y="182" width="1440" height="842" rx="4"></rect> <g id="button" transform="translate(589.000000, 702.000000)"> <rect id="Rectangle-Copy-29" fill="' + c1 + '" x="0" y="0" width="586" height="64"></rect> <g id="text" transform="translate(31.000000, 22.000000)"> <g id="Group-975"> <mask id="mask-2" fill="white"> <use xlink:href="' + p + '"></use> </mask> <use id="Clip-974" fill="' + color + '" xlink:href="' + p + '"></use> </g> </g> </g> </g> </g> </svg> '
    }
}
exports['getGreenPlusIcon'] = getGreenPlusIcon;

var getMasterCardIcon = function (color) {
    color = encodeURIComponent(color);
    var c1 = encodeURIComponent("#F3F3F3");
    var c2 = encodeURIComponent("#000066");
    var c3 = encodeURIComponent("#CC0000");
    var c4 = encodeURIComponent("#FF9900");
    return 'data:image/svg+xml;utf8, <svg width="37px" height="26px" viewBox="0 0 27 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> <!-- Generator: Sketch 61.1 (89650) - https://sketch.com --> <title>Icons/Payments/PayTM Copy 3</title> <desc>Created with Sketch.</desc> <g id="Desktop-Web" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"> <g id="Cards-1" transform="translate(-791.000000, -363.000000)"> <rect id="bg" fill="' + c1 + '" x="0" y="182" width="1440" height="842" rx="4"></rect> <g id="card" transform="translate(588.000000, 283.000000)"> <rect id="Rectangle-Copy-29" fill="' + color + '" x="0" y="5.68434189e-14" width="587" height="411"></rect> <g id="card-1" transform="translate(24.000000, 77.000000)"> <g id="text" transform="translate(0.000000, 3.000000)"> <g id="Icons/Payments/MasterCard-dark" transform="translate(179.000000, 0.000000)"> <g id="MasterCard-dark"> <rect id="Rectangle" fill="' + c2 + '" x="0" y="0" width="26.6666667" height="16" rx="4"></rect> <path d="M14.9676322,8.12944057 C14.9676322,11.0036421 12.6378587,13.3333333 9.76373948,13.3333333 C6.88953798,13.3333333 4.5596,11.0036421 4.5596,8.12944057 C4.5596,5.25532132 6.88953798,2.92538333 9.76373948,2.92538333 C12.6378587,2.92538333 14.9676322,5.25532132 14.9676322,8.12944057" id="Fill-333" fill="' + c3 + '"></path> <path d="M16.7504283,2.92538333 C15.4054774,2.92538333 14.1804321,3.4355173 13.2567138,4.27239029 C13.0687957,4.4429558 12.893378,4.62717313 12.7314477,4.82331524 L13.7828846,4.82331524 C13.9264754,4.99815722 14.0587993,5.18196335 14.1796097,5.3744869 L12.3346403,5.3744869 C12.2242744,5.55138488 12.1239417,5.735191 12.0347937,5.92565856 L14.4792918,5.92565856 C14.5628476,6.10403686 14.63637,6.28776074 14.6996947,6.4765835 L11.8143908,6.4765835 C11.7543557,6.65603092 11.703696,6.83991929 11.6632339,7.02742621 L14.8506871,7.02742621 C14.9270057,7.38270248 14.9676322,7.75130161 14.9676322,8.12944057 C14.9676322,8.70717572 14.8734676,9.26328177 14.6996947,9.7826266 L11.8143908,9.7826266 C11.8777155,9.97128488 11.951238,10.1552555 12.0346292,10.3338805 L14.4792918,10.3338805 C14.3899793,10.5238546 14.2901401,10.7078252 14.1791163,10.8849699 L12.3346403,10.8849699 C12.4549572,11.0769178 12.5875279,11.2609706 12.7314477,11.4355659 L13.7825556,11.4355659 C13.6209542,11.6318725 13.4453721,11.8160898 13.2568783,11.9866553 C14.1805144,12.8235283 15.4054774,13.3333333 16.7504283,13.3333333 C19.6244653,13.3333333 21.9543211,11.0036421 21.9543211,8.12944057 C21.9543211,5.25556804 19.6244653,2.92538333 16.7504283,2.92538333" id="Fill-334" fill="' + c4 + '"></path> <path d="M8.5484808,8.69919845 C8.48918585,8.69245478 8.46303357,8.69031654 8.42232483,8.69031654 C8.10208275,8.69031654 7.94015243,8.80018902 7.94015243,9.01722006 C7.94015243,9.15085986 8.01918495,9.23581365 8.14254476,9.23581365 C8.37273418,9.23581365 8.53861202,9.01672662 8.5484808,8.69919845 L8.5484808,8.69919845 Z M8.95943347,9.65573047 L8.49115961,9.65573047 L8.50193303,9.4332716 C8.3591646,9.60893598 8.16861481,9.69282065 7.90980591,9.69282065 C7.60370908,9.69282065 7.39375068,9.45383157 7.39375068,9.10653256 C7.39375068,8.58373366 7.75881349,8.27887043 8.3855636,8.27887043 C8.44979295,8.27887043 8.53186835,8.2847917 8.6159175,8.29540065 C8.63335236,8.22450987 8.63795779,8.19441008 8.63795779,8.15616853 C8.63795779,8.01405802 8.5395989,7.96076658 8.27569113,7.96076658 C7.9992829,7.95797043 7.77131396,8.02680521 7.67764273,8.05756292 C7.68356401,8.02187081 7.75601734,7.5747326 7.75601734,7.5747326 C8.0374422,7.49199928 8.22313985,7.46107708 8.43211138,7.46107708 C8.91732666,7.46107708 9.17432627,7.67884828 9.1737506,8.09054111 C9.17473747,8.20098927 9.15639798,8.33676731 9.12802522,8.51588576 C9.07901026,8.8267525 8.97374321,9.49347119 8.95943347,9.65573047 L8.95943347,9.65573047 Z" id="Fill-336" fill="' + c2 + '"></path> <polyline id="Fill-337" fill="' + c2 + '" points="7.15731103 9.65573047 6.59224082 9.65573047 6.91610146 7.6267082 6.19329518 9.65573047 5.80833031 9.65573047 5.76071342 7.6380573 5.42056928 9.65573047 4.89176687 9.65573047 5.33347725 7.0160771 6.14592501 7.0160771 6.19502221 8.49359875 6.69068196 7.0160771 7.5942515 7.0160771 7.15731103 9.65573047"></polyline> <path d="M17.4484804,8.69919845 C17.3891854,8.69245478 17.3633621,8.69031654 17.3227356,8.69031654 C17.0022468,8.69031654 16.840481,8.80018902 16.840481,9.01722006 C16.840481,9.15085986 16.9191845,9.23581365 17.0428733,9.23581365 C17.2728983,9.23581365 17.4388583,9.01672662 17.4484804,8.69919845 L17.4484804,8.69919845 Z M17.8596798,9.65573047 L17.3915704,9.65573047 L17.4021793,9.4332716 C17.2594109,9.60893598 17.0689434,9.69282065 16.8102989,9.69282065 C16.5037909,9.69282065 16.2940792,9.45383157 16.2940792,9.10653256 C16.2940792,8.58373366 16.6593065,8.27887043 17.2858922,8.27887043 C17.3500393,8.27887043 17.4319502,8.2847917 17.5160816,8.29540065 C17.5335987,8.22450987 17.5381219,8.19441008 17.5381219,8.15616853 C17.5381219,8.01405802 17.439763,7.96076658 17.1758552,7.96076658 C16.899447,7.95797043 16.6715603,8.02680521 16.5775601,8.05756292 C16.5838103,8.02187081 16.6564281,7.5747326 16.6564281,7.5747326 C16.9376885,7.49199928 17.1235506,7.46107708 17.3321932,7.46107708 C17.8177375,7.46107708 18.0744081,7.67884828 18.0739147,8.09054111 C18.0749016,8.20098927 18.0567265,8.33676731 18.0281893,8.51588576 C17.9793388,8.8267525 17.873825,9.49347119 17.8596798,9.65573047 L17.8596798,9.65573047 Z" id="Fill-338" fill="' + c2 + '"></path> <path d="M11.4702169,9.62308124 C11.3155237,9.67176724 11.1950423,9.69282065 11.0642809,9.69282065 C10.7755367,9.69282065 10.6178829,9.52686058 10.6178829,9.22117495 C10.6138531,9.12627013 10.659414,8.8766721 10.6952706,8.64903212 C10.728002,8.44844906 10.9403454,7.18409318 10.9403454,7.18409318 L11.5017971,7.18409318 L11.4362519,7.50902293 L11.7754914,7.50902293 L11.698926,8.02499593 L11.3585352,8.02499593 C11.293319,8.43315244 11.2003057,8.94172385 11.1992366,9.00916055 C11.1992366,9.11985542 11.2582848,9.16821247 11.392747,9.16821247 C11.4571408,9.16821247 11.5069782,9.16163328 11.5450552,9.14798146 L11.4702169,9.62308124" id="Fill-339" fill="' + c2 + '"></path> <path d="M13.191991,9.60572862 C12.9990563,9.66469462 12.8131119,9.69347857 12.6158184,9.69282065 C11.9870946,9.69199825 11.6593687,9.36386114 11.6593687,8.73513727 C11.6593687,8.00139308 12.0762426,7.46107708 12.6422997,7.46107708 C13.105228,7.46107708 13.4010448,7.76355536 13.4010448,8.23775049 C13.4010448,8.39540434 13.3808138,8.54902843 13.3317166,8.76581275 L12.2134187,8.76581275 C12.1756706,9.07725517 12.3748556,9.20694745 12.7017591,9.20694745 C12.9025067,9.20694745 13.0839278,9.16549856 13.2854978,9.07199181 L13.191991,9.60572862 L13.191991,9.60572862 Z M12.8763544,8.33314875 C12.8795617,8.28832802 12.9358138,7.94991092 12.6148316,7.94991092 C12.4362066,7.94991092 12.3081591,8.08626464 12.2562657,8.33314875 L12.8763544,8.33314875 L12.8763544,8.33314875 Z" id="Fill-340" fill="' + c2 + '"></path> <path d="M9.29826177,8.18774865 C9.29826177,8.459058 9.42984558,8.64631821 9.72837633,8.78703064 C9.95708543,8.89468264 9.9926953,8.92650947 9.9926953,9.02396373 C9.9926953,9.15760353 9.89170473,9.21829656 9.66825898,9.21829656 C9.49958499,9.21829656 9.34283578,9.19181532 9.16199029,9.13334276 C9.16199029,9.13334276 9.08789216,9.6067155 9.08435585,9.62933147 C9.2127323,9.65712855 9.32721021,9.68303411 9.67228874,9.69282065 C10.2683634,9.69282065 10.5437025,9.46583859 10.5437025,8.97527772 C10.5437025,8.68036552 10.4285667,8.50725057 10.145497,8.37698261 C9.90872838,8.26834373 9.88134251,8.24400072 9.88134251,8.14383255 C9.88134251,8.02779208 9.97534269,7.96849713 10.157833,7.96849713 C10.2687746,7.96849713 10.4201782,7.98033967 10.5638513,8.00065292 L10.6441996,7.5027727 C10.4979771,7.47949882 10.2760939,7.46107708 10.1469773,7.46107708 C9.51504609,7.46107708 9.29620577,7.79110571 9.29826177,8.18774865" id="Fill-341" fill="' + c2 + '"></path> <path d="M15.9397076,7.51774036 C16.0966213,7.51774036 16.2431727,7.55853134 16.4445782,7.66026207 L16.5370981,7.08737908 C16.4540358,7.05481209 16.1628244,6.86393333 15.9161048,6.86393333 C15.5381303,6.86393333 15.2182171,7.05176921 14.99362,7.36107339 C14.6656474,7.25251675 14.5306918,7.47193275 14.3655541,7.69069082 L14.2187559,7.72490261 C14.2298583,7.65294272 14.2399738,7.58147627 14.2367664,7.50902293 L13.7176683,7.50902293 C13.646942,8.17360339 13.5213617,8.84649007 13.4230851,9.51131724 L13.397344,9.65573047 L13.9625787,9.65573047 C14.0569078,9.04271442 14.1084722,8.65034796 14.1398879,8.38454867 L14.3527247,8.26661669 C14.384716,8.14810903 14.4839795,8.10830492 14.6836579,8.11315708 C14.6575879,8.25312935 14.6434426,8.40058545 14.6434426,8.55314042 C14.6434426,9.25555122 15.0228974,9.69282065 15.6308146,9.69282065 C15.7874816,9.69282065 15.922026,9.67226068 16.1303396,9.61551517 L16.2293564,9.0140127 C16.042014,9.10603912 15.8886366,9.14937954 15.749569,9.14937954 C15.4211029,9.14937954 15.2223291,8.90685414 15.2223291,8.50626369 C15.2223291,7.92491 15.5177348,7.51774036 15.9397076,7.51774036" id="Fill-342" fill="' + c2 + '"></path> <path d="M20.7229434,7.0160771 L20.5973631,7.77868749 C20.4431633,7.57547275 20.2772032,7.42826337 20.0581162,7.42826337 C19.7730728,7.42826337 19.513606,7.64438977 19.3436984,7.9626581 C19.1070943,7.91356089 18.8625951,7.83008742 18.8625951,7.83008742 L18.8624306,7.83214341 C18.8812636,7.65425856 18.8892408,7.54603088 18.8873493,7.50902293 L18.3683334,7.50902293 C18.2978539,8.17360339 18.1721091,8.84649007 18.0739147,9.51131724 L18.0479269,9.65573047 L18.613326,9.65573047 C18.6896446,9.16031744 18.7479527,8.74846013 18.7909642,8.42229678 C18.9842279,8.24761928 19.080942,8.09572222 19.2757682,8.10559101 C19.1893341,8.31464478 19.1389211,8.55527866 19.1389211,8.80216277 C19.1389211,9.3386135 19.4101482,9.69282065 19.8210186,9.69282065 C20.0280987,9.69282065 20.1869861,9.6213542 20.341926,9.45588757 L20.3152803,9.65556599 L20.8498395,9.65556599 L21.2802008,7.0160771 L20.7229434,7.0160771 L20.7229434,7.0160771 Z M20.0164206,9.159824 C19.8241438,9.159824 19.7267717,9.01722006 19.7267717,8.73661759 C19.7267717,8.31464478 19.9087686,8.01553834 20.1649458,8.01553834 C20.3591142,8.01553834 20.4643812,8.16324116 20.4643812,8.4359486 C20.4643812,8.86153997 20.2793415,9.159824 20.0164206,9.159824 L20.0164206,9.159824 Z" id="Fill-343" fill="' + c2 + '"></path> <polyline id="Fill-344" fill="' + color + '" points="7.31981703 9.4990635 6.7549113 9.4990635 7.0786897 7.47004123 6.3560479 9.4990635 5.97091855 9.4990635 5.9233839 7.48180153 5.58315752 9.4990635 5.05435511 9.4990635 5.49606549 6.85965686 6.30851325 6.85965686 6.33121145 8.49359875 6.87942248 6.85965686 7.75683974 6.85965686 7.31981703 9.4990635"></polyline> <path d="M8.71106904,8.54286044 C8.65185633,8.53595229 8.62570405,8.53406077 8.58499531,8.53406077 C8.26475322,8.53406077 8.10274066,8.64376877 8.10274066,8.86063533 C8.10274066,8.99411065 8.18185543,9.07955788 8.30529748,9.07955788 C8.53540466,9.07955788 8.7012825,8.86047085 8.71106904,8.54286044 L8.71106904,8.54286044 Z M9.12193947,9.4990635 L8.65391233,9.4990635 L8.66460351,9.27676911 C8.52183508,9.45235125 8.33128529,9.53631817 8.07247639,9.53631817 C7.76629732,9.53631817 7.55642115,9.29700012 7.55642115,8.94978336 C7.55642115,8.42690221 7.92156621,8.12245018 8.54831632,8.12245018 C8.61238119,8.12245018 8.69445659,8.12828921 8.77858798,8.13914488 C8.79602284,8.06808962 8.80062827,8.03790759 8.80062827,7.99950157 C8.80062827,7.8574733 8.70226937,7.80459305 8.43836161,7.80459305 C8.16187114,7.80130346 7.93398443,7.87013824 7.84023097,7.90081371 C7.84623448,7.86528608 7.91860558,7.41814787 7.91860558,7.41814787 C8.20011268,7.33557903 8.38581032,7.30441012 8.59478185,7.30441012 C9.07991489,7.30441012 9.33683227,7.52242803 9.33642107,7.93395638 C9.33732571,8.04424006 9.3191507,8.18067602 9.2906957,8.35930103 C9.24159849,8.66992105 9.13633145,9.33705094 9.12193947,9.4990635 L9.12193947,9.4990635 Z" id="Fill-345" fill="' + color + '"></path> <path d="M16.6996041,6.93071211 L16.6070842,7.50351286 C16.4053498,7.40211109 16.2592095,7.36107339 16.1024603,7.36107339 C15.680652,7.36107339 15.3847529,7.76873647 15.3847529,8.35017241 C15.3847529,8.75076285 15.5836089,8.99279481 15.912075,8.99279481 C16.0512249,8.99279481 16.2049312,8.94978336 16.3918624,8.85742797 L16.2925989,9.45893044 C16.0846143,9.51559372 15.9499876,9.53631817 15.7934029,9.53631817 C15.1850745,9.53631817 14.8061954,9.09896649 14.8061954,8.39672018 C14.8061954,7.45309982 15.3299811,6.79361823 16.078693,6.79361823 C16.3254126,6.79361823 16.6167885,6.89814512 16.6996041,6.93071211" id="Fill-346" fill="' + color + '"></path> <path d="M17.6109864,8.54286044 C17.5521026,8.53595229 17.5258681,8.53406077 17.4852416,8.53406077 C17.165164,8.53406077 17.0030692,8.64376877 17.0030692,8.86063533 C17.0030692,8.99411065 17.0821017,9.07955788 17.2054616,9.07955788 C17.4354865,9.07955788 17.6015288,8.86047085 17.6109864,8.54286044 L17.6109864,8.54286044 Z M18.0221858,9.4990635 L17.5541586,9.4990635 L17.5647676,9.27676911 C17.4219992,9.45235125 17.2314494,9.53631817 16.9728872,9.53631817 C16.6663792,9.53631817 16.4567497,9.29700012 16.4567497,8.94978336 C16.4567497,8.42690221 16.8215658,8.12245018 17.4484804,8.12245018 C17.5125453,8.12245018 17.5944562,8.12828921 17.6785053,8.13914488 C17.6960224,8.06808962 17.7006279,8.03790759 17.7006279,7.99950157 C17.7006279,7.8574733 17.6025157,7.80459305 17.3384434,7.80459305 C17.0621997,7.80130346 16.8343952,7.87013824 16.7404773,7.90081371 C16.7463986,7.86528608 16.8188519,7.41814787 16.8188519,7.41814787 C17.100359,7.33557903 17.2858922,7.30441012 17.4948637,7.30441012 C17.9799967,7.30441012 18.2371608,7.52242803 18.2365029,7.93395638 C18.2376543,8.04424006 18.2193148,8.18067602 18.1908598,8.35930103 C18.1419271,8.66992105 18.03666,9.33705094 18.0221858,9.4990635 L18.0221858,9.4990635 Z" id="Fill-347" fill="' + color + '"></path> <path d="M11.6327229,9.46649651 C11.4781942,9.51518252 11.3575483,9.53631817 11.2269514,9.53631817 C10.9381249,9.53631817 10.7803889,9.37027585 10.7803889,9.06491919 C10.7764413,8.96968541 10.8220845,8.72049858 10.8580233,8.49261187 C10.890508,8.29186433 11.1029337,7.02767293 11.1029337,7.02767293 L11.6645498,7.02767293 L11.5988401,7.35252045 L11.8871731,7.35252045 L11.8103611,7.86832896 L11.5211234,7.86832896 C11.4559072,8.27689667 11.3628939,8.78513912 11.3617425,8.85257582 C11.3617425,8.96359966 11.4210375,9.01162774 11.5553352,9.01162774 C11.619729,9.01162774 11.6694842,9.00529527 11.7077257,8.99131449 L11.6327229,9.46649651" id="Fill-348" fill="' + color + '"></path> <path d="M13.354826,9.44947285 C13.161809,9.50860333 12.9754534,9.53648265 12.7784067,9.53631817 C12.1496006,9.53590697 11.8221214,9.20735865 11.8221214,8.57871703 C11.8221214,7.84472612 12.2389131,7.30441012 12.804559,7.30441012 C13.2679807,7.30441012 13.563633,7.60688839 13.563633,8.0816592 C13.563633,8.23898409 13.5430731,8.39252594 13.4945515,8.6093925 L12.3760892,8.6093925 C12.3381766,8.92083492 12.5376083,9.05077393 12.8641829,9.05077393 C13.0650949,9.05077393 13.2464338,9.00916055 13.4482505,8.91540709 L13.354826,9.44947285 L13.354826,9.44947285 Z M13.0391071,8.17648178 C13.04215,8.13157881 13.0986488,7.79332619 12.7777488,7.79332619 C12.5989593,7.79332619 12.4709118,7.92992663 12.4188539,8.17648178 L13.0391071,8.17648178 L13.0391071,8.17648178 Z" id="Fill-349" fill="' + color + '"></path> <path d="M9.46076777,8.03108168 C9.46076777,8.30288448 9.59251605,8.48973348 9.89112905,8.63061039 C10.1198382,8.73818015 10.1552835,8.77000698 10.1552835,8.86754348 C10.1552835,9.00134776 10.0543752,9.06171183 9.83076498,9.06171183 C9.66225547,9.06171183 9.50542402,9.03555955 9.32449629,8.97725148 C9.32449629,8.97725148 9.2504804,9.45029525 9.24702633,9.47258226 C9.37532054,9.5007083 9.48988069,9.52636714 9.83479473,9.53631817 C10.4310339,9.53631817 10.706373,9.30941834 10.706373,8.81885747 C10.706373,8.52378079 10.5910727,8.35074808 10.3080852,8.22080908 C10.0712344,8.11184124 10.044013,8.08774495 10.044013,7.9874123 C10.044013,7.87137184 10.1379309,7.81166568 10.3205857,7.81166568 C10.4312806,7.81166568 10.5828487,7.82400167 10.7264395,7.84423268 L10.8068701,7.34643469 C10.6605653,7.32332529 10.4388466,7.30441012 10.3096478,7.30441012 C9.67755209,7.30441012 9.45887625,7.63452099 9.46076777,8.03108168" id="Fill-350" fill="' + color + '"></path> <path d="M21.0124278,9.4990635 L20.478033,9.4990635 L20.5045143,9.29930284 C20.3494921,9.46485171 20.1906869,9.53631817 19.9836069,9.53631817 C19.5730654,9.53631817 19.3014271,9.18252221 19.3014271,8.64549581 C19.3014271,7.93148919 19.7225775,7.32965776 20.2207044,7.32965776 C20.4397915,7.32965776 20.6056693,7.41913475 20.760198,7.62243173 L20.8855316,6.85965686 L21.4427068,6.85965686 L21.0124278,9.4990635 L21.0124278,9.4990635 Z M20.1789266,9.0030748 C20.4421764,9.0030748 20.6268872,8.70495524 20.6268872,8.27977507 C20.6268872,8.00698539 20.5217024,7.85878913 20.327534,7.85878913 C20.0712746,7.85878913 19.88936,8.15797781 19.88936,8.5801151 C19.88936,8.86104653 19.9868142,9.0030748 20.1789266,9.0030748 L20.1789266,9.0030748 Z" id="Fill-351" fill="' + color + '"></path> <path d="M18.5310862,7.35252045 C18.4604421,8.01693642 18.3347796,8.6898231 18.2365029,9.35473252 L18.2107618,9.4990635 L18.775832,9.4990635 C18.9779777,8.18635057 19.0265814,7.93017335 19.3436984,7.96216466 C19.3941114,7.69348698 19.4881116,7.45819869 19.5580978,7.33960878 C19.3214936,7.29026486 19.1893341,7.4239869 19.016137,7.67810812 C19.0299533,7.56815341 19.0548719,7.46148828 19.0501843,7.35252045 L18.5310862,7.35252045" id="Fill-352" fill="' + color + '"></path> <path d="M13.8805033,7.35252045 C13.8095303,8.01693642 13.6837033,8.6898231 13.5855911,9.35473252 L13.5599322,9.4990635 L14.1251669,9.4990635 C14.3273948,8.18635057 14.3758341,7.93017335 14.6924576,7.96216466 C14.7431996,7.69348698 14.8373643,7.45819869 14.9071859,7.33960878 C14.6708285,7.29026486 14.5384223,7.4239869 14.3655541,7.67810812 C14.3792059,7.56815341 14.4042891,7.46148828 14.3993547,7.35252045 L13.8805033,7.35252045" id="Fill-353" fill="' + color + '"></path> </g> </g> </g> </g> </g> </g> </g> </svg> '
}
exports['getMasterCardIcon'] = getMasterCardIcon;

var getVisaIcon = function (color) {
    color = encodeURIComponent(color);
    var c1 = encodeURIComponent("#F3F3F3");
    var c2 = encodeURIComponent("#FFFFFE");
    var c3 = encodeURIComponent("#26337A");
    var c4 = encodeURIComponent("#EC982D");
    return 'data:image/svg+xml;utf8, <svg width="36px" height="26px" viewBox="0 0 26 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> <!-- Generator: Sketch 61.1 (89650) - https://sketch.com --> <title>Icons/Payments/PayTM Copy 2</title> <desc>Created with Sketch.</desc> <g id="Desktop-Web" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"> <g id="Cards-1" transform="translate(-794.000000, -633.000000)"> <rect id="bg" fill="' + c1 + '" x="0" y="182" width="1440" height="842" rx="4"></rect> <g id="card" transform="translate(588.000000, 283.000000)"> <rect id="Rectangle-Copy-29" fill="' + color + '" x="0" y="5.68434189e-14" width="587" height="411"></rect> <g id="line-copy-2" transform="translate(24.000000, 350.000000)"> <g id="text"> <g id="Icons/Payments/Visa-dark" transform="translate(182.000000, -0.000000)"> <g id="Visa-dark"> <rect id="Rectangle" fill="' + c3 + '" x="0" y="0" width="26" height="15.6" rx="4"></rect> <polyline id="Fill-3" fill="' + c2 + '" points="10.7873976 10.2821577 11.5868293 5.32289577 12.8646922 5.32289577 12.0653335 10.2821577 10.7873976 10.2821577"></polyline> <path d="M16.7005465,5.44461972 C16.4478828,5.34508732 16.0505057,5.23654648 15.5554837,5.23654648 C14.2920922,5.23654648 13.4027628,5.90954366 13.3953809,6.87322817 C13.3873413,7.58606761 14.0300002,7.98361127 14.5144976,8.22054085 C15.0124431,8.46340282 15.1797406,8.6188169 15.1777672,8.83597183 C15.1743321,9.16811268 14.7803171,9.32030423 14.4129059,9.32030423 C13.901293,9.32030423 13.6294804,9.24538028 13.2092269,9.06023099 L13.044853,8.98127887 L12.8661539,10.091369 C13.1641319,10.2294986 13.7165276,10.3493915 14.2898265,10.3556901 C15.632518,10.3556901 16.5049641,9.69082254 16.5151964,8.66173521 C16.5198009,8.09647324 16.179286,7.66802254 15.4418325,7.31427606 C14.9952676,7.08430423 14.7211892,6.9319662 14.724332,6.69921127 C14.7246243,6.49311549 14.9558734,6.27244507 15.456377,6.27244507 C15.8743647,6.26570704 16.1769472,6.36223662 16.4127276,6.46301408 L16.5274751,6.51970141 L16.7005465,5.44461972" id="Fill-4" fill="' + c2 + '"></path> <path d="M18.4020979,8.52389859 C18.5080018,8.2388507 18.9111527,7.13769577 18.9111527,7.13769577 C18.9036978,7.15080563 19.0164719,6.8504507 19.0810082,6.66456901 L19.1672515,7.09228732 C19.1672515,7.09228732 19.4124603,8.2759831 19.4629638,8.52389859 L18.4020979,8.52389859 L18.4020979,8.52389859 Z M19.9785965,5.3279493 L18.9910374,5.3279493 C18.6847275,5.3279493 18.4554518,5.41569014 18.321263,5.73904225 L16.4235446,10.2839155 L17.7657245,10.2839155 C17.7657245,10.2839155 17.9846948,9.67251268 18.0343944,9.53833803 C18.1807888,9.53833803 19.4849631,9.54090141 19.6711171,9.54090141 C19.709415,9.7139662 19.8267206,10.2839155 19.8267206,10.2839155 L21.0127854,10.2839155 L19.9785965,5.3279493 L19.9785965,5.3279493 Z" id="Fill-5" fill="' + c2 + '"></path> <path d="M9.71534931,5.32707042 L8.46460194,8.70809577 L8.33063244,8.0208169 C8.09755622,7.22895211 7.37201604,6.37065915 6.5604518,5.94059718 L7.7047106,10.2778366 L9.05712276,10.277031 L11.0695156,5.32707042 L9.71534931,5.32707042" id="Fill-6" fill="' + c2 + '"></path> <path d="M7.303533,5.32370141 L5.24282931,5.32370141 L5.225873,5.42645634 C6.82934092,5.83725634 7.89027991,6.82884507 8.33063244,8.0208169 L7.88282498,5.74211831 C7.80557143,5.42784789 7.58104645,5.33468732 7.303533,5.32370141" id="Fill-7" fill="' + c4 + '"></path> </g> </g> </g> </g> </g> </g> </g> </svg> '
}
exports['getVisaIcon'] = getVisaIcon;

var getJuspayIcon = function (color) {
    var c1 = encodeURIComponent("#0099FF");
    var c2 = encodeURIComponent("#0585DD");
    var c3 = encodeURIComponent(color);
    return 'data:image/svg+xml;utf8, <svg width="68px" height="16px" viewBox="0 0 68 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> <!-- Generator: Sketch 61.1 (89650) - https://sketch.com --> <title>Symbols</title> <desc>Created with Sketch.</desc> <g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"> <g id="powered-by" transform="translate(-68.000000, 0.000000)" fill-rule="nonzero"> <g transform="translate(0.000000, -1.000000)" id="Symbols"> <g transform="translate(68.000000, 1.000000)"> <g id="AxisPay--juspay"> <g id="Juspay_Logo_V3"> <g id="Group" transform="translate(0.000000, 0.492235)"> <path d="M7.27080721,14.5777941 C6.92180846,14.5874546 6.58250412,14.5584729 6.23350537,14.5101701 C5.29314765,14.3749222 4.41095637,14.0754446 3.59662597,13.5827558 C1.79346577,12.5007724 0.639831034,10.9261001 0.164804963,8.858739 C0.0581664577,8.39503184 0.00969440962,7.92166411 0.00969440962,7.44829637 C-2.08704581e-14,5.55482544 0.620442216,3.89320809 1.88071547,2.48276545 C2.97618375,1.26553414 4.33340109,0.521670562 5.93297868,0.212532452 C6.37892152,0.125587358 6.82486437,0.0772845279 7.27080721,0.0966056597 L7.27080721,2.26057244 C7.25141838,2.27023301 7.23202957,2.27989357 7.21264074,2.2992147 C6.95089169,2.48276545 6.68914263,2.67597677 6.43708798,2.87884867 C5.86511781,3.34255582 5.32223087,3.8352447 4.86659363,4.42453921 C4.25584582,5.20704506 3.84868062,6.06683543 3.79051415,7.08119486 C3.76143092,7.56422316 3.81959739,8.03759089 3.97470794,8.4916375 C4.35278992,9.62192371 5.52581348,10.7908522 7.17386311,10.8198339 C7.27080721,10.8198339 7.29019602,10.839155 7.29019602,10.9454213 C7.28050161,11.4381101 7.28050161,12.4234878 7.28050161,12.4234878 L7.28050161,12.5104329 C7.27080721,13.1963331 7.27080721,13.8822333 7.27080721,14.5777941 L7.27080721,14.5777941 Z" id="Shape" fill="' + c1 + '"></path> <path d="M7.27080721,2.25091187 L7.27080721,0.0869450938 C7.668278,0.0772845279 8.05605439,0.115926792 8.44383077,0.173890188 C9.17091149,0.289816979 9.86890898,0.50234943 10.5281288,0.84046924 C11.8950406,1.54569056 12.9711201,2.55038941 13.6982008,3.92218979 C14.1150604,4.7143562 14.3865039,5.55482544 14.4931423,6.43393695 C14.6191697,7.4676175 14.5222256,8.4916375 14.2120045,9.48667579 C13.8533113,10.616962 13.2522579,11.6120003 12.4088443,12.4524695 C11.3812369,13.466829 10.1597413,14.1237475 8.74435746,14.423225 C8.25963699,14.5198306 7.7652221,14.5777941 7.27080721,14.5681336 L7.27080721,12.4911118 L7.27080721,12.4138273 C7.27080721,12.4138273 7.29989044,12.3945062 7.30958485,12.3848456 C8.0657488,11.8535144 8.78313511,11.2738805 9.40357733,10.5783197 C9.81074253,10.1242731 10.1597413,9.64124485 10.4021015,9.08093202 C10.7414058,8.28876561 10.8771276,7.47727807 10.7026282,6.6174877 C10.3924071,5.052476 8.98671771,3.88354752 7.36775131,3.84490526 C7.2611128,3.84490526 7.27080721,3.75796016 7.27080721,3.75796016 L7.27080721,2.25091187 L7.27080721,2.25091187 Z" id="Shape" fill="' + c2 + '"></path> </g> <g id="Group" transform="translate(16.480497, 2.327743)" fill="' + c3 + '"> <path d="M6.1559501,7.69947109 C6.1559501,8.10521485 6.08808923,8.4916375 5.96206191,8.86839956 C5.83603458,9.24516164 5.63245198,9.57362089 5.38039733,9.86343787 C5.11864828,10.1532548 4.79873276,10.3754479 4.41095637,10.549338 C4.02317999,10.7135677 3.56754273,10.8005128 3.06343343,10.8005128 C2.24910303,10.8005128 1.58018876,10.6073014 1.06638506,10.2305394 C0.542886938,9.85377729 0.203582602,9.25482221 0.0387776385,8.4336741 L1.75468814,8.02793033 C1.8128546,8.36605014 1.94857634,8.63654598 2.17154776,8.83941788 C2.38482477,9.04228976 2.65626823,9.13889542 2.97618375,9.13889542 C3.49968187,9.13889542 3.85837502,8.96500522 4.0425688,8.60756428 C4.22676259,8.25978391 4.32370669,7.76709505 4.32370669,7.14881882 L4.32370669,0.280156413 L6.1559501,0.280156413 L6.1559501,7.69947109 L6.1559501,7.69947109 Z" id="Shape"></path> <path d="M16.2575249,6.77205675 C16.2575249,7.39033298 16.1508864,7.95064581 15.9376094,8.44333466 C15.7243323,8.9456841 15.4335001,9.36108843 15.0651126,9.70886881 C14.696725,10.0566492 14.2604765,10.327145 13.7563672,10.5106958 C13.2522579,10.7039071 12.6996766,10.7908522 12.1083176,10.7908522 C11.5169586,10.7908522 10.9643773,10.6942465 10.4699624,10.5106958 C9.96585308,10.327145 9.52960465,10.0566492 9.15152268,9.70886881 C8.77344069,9.36108843 8.4826084,8.93602354 8.27902581,8.44333466 C8.0657488,7.95064581 7.9688047,7.39033298 7.9688047,6.77205675 L7.9688047,0.280156413 L9.80104812,0.280156413 L9.80104812,6.71409336 C9.80104812,6.96526806 9.83982576,7.22610336 9.91738103,7.50625977 C9.99493631,7.77675561 10.1209636,8.03759089 10.3051574,8.26944448 C10.4796568,8.50129806 10.722017,8.69450938 11.0225437,8.83941788 C11.313376,8.99398692 11.6817636,9.06161089 12.118012,9.06161089 C12.5542604,9.06161089 12.922648,8.98432636 13.2134803,8.83941788 C13.5043126,8.69450938 13.7466728,8.50129806 13.9308666,8.26944448 C14.105366,8.03759089 14.2410877,7.78641617 14.318643,7.50625977 C14.3961983,7.22610336 14.434976,6.96526806 14.434976,6.71409336 L14.434976,0.280156413 L16.2672194,0.280156413 L16.2672194,6.77205675 L16.2575249,6.77205675 Z" id="Shape"></path> <path d="M23.1987222,2.48276545 C23.0048341,2.20260904 22.7430849,1.99973716 22.4231694,1.86448924 C22.0935595,1.7292413 21.7542551,1.67127792 21.3955621,1.67127792 C21.182285,1.67127792 20.9787024,1.70025962 20.7751197,1.74856244 C20.5715372,1.79686528 20.3970377,1.8741498 20.2419272,1.98041602 C20.0771223,2.08668225 19.951095,2.22193017 19.8541508,2.39582036 C19.7572068,2.56971055 19.7087347,2.76292187 19.7087347,2.99477545 C19.7087347,3.34255582 19.8250676,3.61305168 20.0771223,3.78694186 C20.3194825,3.97049262 20.6200092,4.13472223 20.9787024,4.26030959 C21.3373956,4.39555751 21.725172,4.53080545 22.1517259,4.65639281 C22.57828,4.78198016 22.9660564,4.95587034 23.3344439,5.17806336 C23.6931371,5.40025638 23.9936637,5.69973393 24.2360241,6.05717487 C24.4783843,6.42427637 24.6044115,6.91696524 24.6044115,7.51592033 C24.6044115,8.06657259 24.5074675,8.54960089 24.303885,8.95534466 C24.1003023,9.370749 23.8288588,9.70886881 23.4798601,9.97936465 C23.1308613,10.2498605 22.7333905,10.4527324 22.2777533,10.5879803 C21.822116,10.7232282 21.3373956,10.7908522 20.8429806,10.7908522 C20.2031496,10.7908522 19.5827074,10.684586 19.0010428,10.4720535 C18.4096839,10.2595211 17.8958802,9.90208013 17.4693261,9.39973069 L18.8556266,8.06657259 C19.0785981,8.4046924 19.3694304,8.66552768 19.737818,8.858739 C20.1062056,9.05195032 20.4842876,9.13889542 20.8914527,9.13889542 C21.1047298,9.13889542 21.3180068,9.10991372 21.5312837,9.05195032 C21.7445607,8.99398692 21.938449,8.90704184 22.1129483,8.79111504 C22.2874477,8.67518824 22.4231694,8.53027976 22.5298079,8.34672901 C22.6364465,8.17283882 22.694613,7.96030637 22.694613,7.72845279 C22.694613,7.35169071 22.5685856,7.06187374 22.3262254,6.85900184 C22.0838652,6.65612996 21.7833384,6.48223977 21.4246452,6.34699185 C21.065952,6.21174393 20.6684813,6.07649599 20.2419272,5.95090865 C19.8153733,5.82532129 19.4179025,5.6514311 19.0689037,5.43889864 C18.7102105,5.2263662 18.4096839,4.93654922 18.1673236,4.56944771 C17.9249633,4.20234619 17.7989361,3.7193179 17.7989361,3.11070224 C17.7989361,2.57937111 17.9055746,2.12532452 18.128546,1.73890188 C18.341823,1.35247924 18.6326552,1.03368056 18.9913484,0.772845279 C19.3403472,0.521670562 19.7475124,0.328459243 20.2031496,0.202871886 C20.6587869,0.0772845279 21.1241185,0.019321132 21.5991446,0.019321132 C22.1420315,0.019321132 22.6655296,0.106266226 23.1793334,0.260835281 C23.6834426,0.425064903 24.1487744,0.69556075 24.5559396,1.07232282 L23.1987222,2.48276545 L23.1987222,2.48276545 Z" id="Shape"></path> <path d="M26.1361282,0.280156413 L29.5970326,0.280156413 C30.0914474,0.280156413 30.5664734,0.328459243 31.0318051,0.425064903 C31.4874424,0.521670562 31.8946077,0.685900184 32.2436063,0.908093202 C32.5926051,1.13994679 32.8737429,1.43942433 33.0773256,1.8161864 C33.2809082,2.20260904 33.3875467,2.67597677 33.3875467,3.24595016 C33.3875467,3.89320809 33.2712138,4.41487865 33.0482424,4.81096186 C32.825271,5.20704506 32.5247442,5.51618318 32.1466623,5.72871563 C31.7685803,5.94124807 31.3323318,6.09581713 30.8282226,6.17310167 C30.3241132,6.25038619 29.8103095,6.28902845 29.2674226,6.28902845 L27.9586773,6.28902845 L27.9586773,10.5396775 L26.1264338,10.5396775 L26.1264338,0.280156413 L26.1361282,0.280156413 Z M29.0735344,4.73367733 C29.3352834,4.73367733 29.606727,4.72401677 29.8878648,4.70469563 C30.1690027,4.68537451 30.4307518,4.62741111 30.6634176,4.53080545 C30.8960835,4.43419979 31.0899716,4.28929129 31.2450821,4.09607997 C31.3904983,3.90286866 31.4680536,3.6323728 31.4680536,3.28459243 C31.4680536,2.96579375 31.4001927,2.71461905 31.2644709,2.52140771 C31.1287493,2.3281964 30.9542498,2.18328791 30.7409729,2.08668225 C30.5276959,1.98041602 30.2853357,1.92245264 30.0235865,1.89347094 C29.7618375,1.86448924 29.5097829,1.8451681 29.2674226,1.8451681 L27.9586773,1.8451681 L27.9586773,4.73367733 L29.0735344,4.73367733 L29.0735344,4.73367733 Z" id="Shape"></path> <path d="M36.9066174,0.280156413 L38.4868062,0.280156413 L42.9268458,10.5396775 L40.8328532,10.5396775 L39.8731067,8.19215995 L35.4136783,8.19215995 L34.483015,10.5396775 L32.4278002,10.5396775 L36.9066174,0.280156413 L36.9066174,0.280156413 Z M39.2332757,6.62714826 L37.6433925,2.45378375 L36.0341205,6.62714826 L39.2332757,6.62714826 L39.2332757,6.62714826 Z" id="Shape"></path> <polygon id="Shape" points="45.15656 6.15378053 41.2884905 0.280156413 43.5860655 0.280156413 46.1163066 4.36657583 48.6756307 0.280156413 50.8568727 0.280156413 46.9888034 6.15378053 46.9888034 10.549338 45.15656 10.549338"></polygon> </g> </g> </g> </g> </g> </g> </g> </svg> '
}
exports['getJuspayIcon'] = getJuspayIcon;

var getAxisBankIcon = function (color) {
    color = encodeURIComponent(color);
    return ''
}
exports['getAxisBankIcon'] = getAxisBankIcon;

var getIcPaytmIcon = function (color) {
   return "https://payments.juspay.in/web/images/paytm_icon"
}
exports['getIcPaytmIcon'] = getIcPaytmIcon;

exports['getPCIIcon'] = function(){
    return "https://payments.juspay.in/web/images/pcidss"
}

exports['getSecuredMasterCardIcon'] = function(){
    return "https://payments.juspay.in/web/images/master_card_secure"
}

exports['getVerifiedVisaIcon'] = function(){
    return "https://payments.juspay.in/web/images/verified_visa"
}

var getQuestionIcon = function (color) {
    color = encodeURIComponent(color);
    var c1 = encodeURIComponent("#000000");
    var c2= encodeURIComponent("#ffffff");
    var c3= encodeURIComponent("#28B3E3");
    var c4= encodeURIComponent("#EBEBEB");
    var c5 = encodeURIComponent("#999999");
    return 'data:image/svg+xml;utf8, <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> <!-- Generator: Sketch 61.1 (89650) - https://sketch.com --> <title>Group 11 Copy 6</title> <desc>Created with Sketch.</desc> <g id="Desktop-Web" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"> <g id="Add-Card-2" transform="translate(-919.000000, -537.000000)"> <rect id="bg" x="0" y="182" width="1440" height="842" rx="4"></rect> <g id="add-card-component" transform="translate(589.000000, 283.000000)"> <rect id="Rectangle-Copy-29" fill="' + c2 + '" x="0" y="0" width="587" height="483"></rect> <g id="add-card---filled" transform="translate(24.000000, 112.000000)"> <g id="cvv-copy" transform="translate(207.000000, 104.000000)"> <g id="input" transform="translate(0.000000, 24.000000)"> <rect id="Rectangle-2-Copy" stroke="'+c3+'" fill="'+c2+'" x="0.5" y="0.5" width="130" height="47" rx="4"></rect> <g id="Group-11-Copy-6" transform="translate(99.000000, 14.000000)" fill-rule="nonzero"> <circle id="Oval" fill="'+c4+'" cx="10" cy="10" r="10"></circle> <circle id="Oval" fill="'+c5+'" cx="10" cy="15.22375" r="1"></circle> <path d="M7.8440155,6.95921973 C7.84985795,6.92791076 7.87034556,6.85244234 7.9103837,6.74834663 C7.9797318,6.56804757 8.07800856,6.38707438 8.20882404,6.21950672 C8.58481198,5.7378862 9.15153936,5.45205456 9.9975531,5.45619483 C10.8431371,5.46036934 11.4802459,5.94194883 11.7717758,6.70176178 C12.0465928,7.41801599 11.9353193,8.20661356 11.4985547,8.65061237 C11.2572077,8.89585202 11.0202499,9.13351742 10.7583481,9.39376744 C10.6196385,9.53160216 10.2182194,9.92874826 10.176559,9.97008941 C9.74420396,10.3938686 9.49954347,10.9768893 9.50000014,11.5853752 L9.5,12.6025 C9.5,12.8786424 9.72385763,13.1025 10,13.1025 C10.2761424,13.1025 10.5,12.8786424 10.5,12.6025 L10.5,11.585 C10.499745,11.2450829 10.6362683,10.9197509 10.8787499,10.6820716 C10.9217902,10.6393743 11.3235386,10.2419025 11.4632144,10.1031076 C11.7275802,9.84040916 11.9670907,9.60018342 12.2113704,9.35196374 C12.9426551,8.60856666 13.1125352,7.40462253 12.7054117,6.34353842 C12.2718424,5.21352859 11.278271,4.46250534 10.0024684,4.45615153 C8.84082083,4.45052197 7.98370298,4.88281341 7.42057891,5.60414535 C7.22498944,5.85468504 7.07999672,6.12168403 6.9770421,6.38935714 C6.91369861,6.55404474 6.87743386,6.68763 6.8609845,6.77578027 C6.81032909,7.04723678 6.98932375,7.30836009 7.26078027,7.3590155 C7.53223678,7.40967091 7.79336009,7.23067625 7.8440155,6.95921973 Z" id="Path" fill="'+c5+'"></path> <path d="M10,19.75 C4.52560204,19.7504277 0.068429958,15.3489703 0,9.875 C0,9.91625 0,9.9575 0,10 C0,15.5228475 4.4771525,20 10,20 C15.5228475,20 20,15.5228475 20,10 C20,9.9575 20,9.91625 20,9.875 C19.93157,15.3489703 15.474398,19.7504277 10,19.75 Z" id="Path" fill="'+c1+'" opacity="0.06"></path> </g> </g> </g> </g> </g> </g> </g> </svg>'
}


var getQuestionIconBB = function (color) {
    var c1= encodeURIComponent("#ffffff")
    var c2 = encodeURIComponent("#DFDFDF");
    var c3 = encodeURIComponent("#f7f7f7");
    var c4 = encodeURIComponent("#C8C8C8");
    var c5 = encodeURIComponent("#9B9B9B");
    var c6 = encodeURIComponent("#979797")
   return 'data:image/svg+xml;utf8,<svg width="12px" height="12px" viewBox="0 0 12 12" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><defs><polygon id="path-1" points="20 0 540 0 540 298 20 298"></polygon><filter x="-7.8%" y="-11.9%" width="115.6%" height="127.2%" filterUnits="objectBoundingBox" id="filter-2"><feMorphology radius="0.5" operator="dilate" in="SourceAlpha" result="shadowSpreadOuter1"></feMorphology><feOffset dx="0" dy="5" in="shadowSpreadOuter1" result="shadowOffsetOuter1"></feOffset><feGaussianBlur stdDeviation="12.5" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur><feComposite in="shadowBlurOuter1" in2="SourceAlpha" operator="out" result="shadowBlurOuter1"></feComposite><feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.15 0" type="matrix" in="shadowBlurOuter1"></feColorMatrix></filter></defs><g id="5b.-Card---Add-Card" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="7.-PW---Add-Card-(CVV-Entered)-&lt;f&gt;" transform="translate(-608.000000, -1001.000000)"><rect fill="'+c1+'" x="0" y="0" width="1440" height="1460"></rect><g id="Payment-Options" transform="translate(140.000000, 370.000000)"><g id="Payments---Tab" transform="translate(0.000000, 71.000000)"><path d="M799.5,95.5 L799.5,694.5 L0.5,694.5 L0.5,95.5 L405.5,95.5 L405.5,20.5 L779.5,20.5 L779.5,95.5 L799.5,95.5 Z" id="Selected-Tab" stroke="'+c2+'" fill="'+c1+'"></path><g id="Tabs-Bar" transform="translate(20.000000, 115.000000)"><path d="M759.5,559.5 L0.5,559.5 L0.5,0.5 L212.383213,0.5 L759.5,0.5 L759.5,559.5 Z" id="Grey-Box" stroke="'+c2+'" fill="'+c3+'"></path><g id="Add-New-Card" transform="translate(179.000000, 242.000000)"><g id="Selected-Box"><use fill="black" fill-opacity="1" ></use><use stroke="'+c4+'" stroke-width="1" fill="'+c1+'" fill-rule="evenodd" ></use></g><path d="M559.5,297.5 L0.5,297.5 L0.5,0.5 L559.5,0.5 L559.5,297.5 Z" id="Selected-Box" stroke="'+c2+'" fill="'+c1+'"></path><g id="Save-this-card" transform="translate(60.000000, 201.000000)" fill="'+c5+'" stroke="'+c6+'"><g id="Info-Icon-Grey" transform="translate(209.000000, 2.000000)"><path d="M11.5,6.00003636 C11.5,7.51638283 10.8829692,8.89124824 9.88710262,9.88710878 C8.89123188,10.8829735 7.5163527,11.5 6,11.5 C4.4836473,11.5 3.10876812,10.8829735 2.11289738,9.88710878 C1.11703081,8.89124824 0.5,7.51638283 0.5,6.00003636 C0.5,4.48368283 1.11703598,3.10879502 2.1129094,2.11291556 C3.10877866,1.11704027 4.4836527,0.5 6,0.5 C7.5163473,0.5 8.89122134,1.11704027 9.8870906,2.11291556 C10.882964,3.10879502 11.5,4.48368283 11.5,6.00003636 Z M11.4090909,6.00003636 C11.4090909,4.50874386 10.8026736,3.15633318 9.82317717,2.17682948 C8.84368942,1.19733447 7.49129924,0.590909091 6,0.590909091 C4.50870076,0.590909091 3.15631058,1.19733447 2.17682283,2.17682948 C1.19732639,3.15633318 0.590909091,4.50874386 0.590909091,6.00003636 C0.590909091,7.49132636 1.19732364,8.84371495 2.17681557,9.82319963 C3.15630326,10.8026801 4.50869622,11.4090909 6,11.4090909 C7.49128864,11.4090909 8.84368614,10.802677 9.82317975,9.82319067 C10.8026711,8.84370654 11.4090909,7.49132148 11.4090909,6.00003636 Z" id="Info"></path></g></g></g></g></g></g></g></g></svg>'
}


exports['getQuestionIcon'] = getQuestionIcon;



var selectedRadioBBIcon = function(){
    var c1 =  encodeURIComponent("#DFDFDF");
    var c2 = encodeURIComponent("#ffffff");
    var c3 = encodeURIComponent("#f7f7f7");
    var c4 = encodeURIComponent("#c8c8c8");
    var c5 = encodeURIComponent("#dfdfdf");
    var c6 = encodeURIComponent("#84C225");
    var c7 = encodeURIComponent("#222222");
    return 'data:image/svg+xml;utf8,<svg width="16px" height="16px" viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><title>Radio Button</title><desc>Created with Sketch.</desc><g id="5b.-Card---Add-Card" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="7.-PW---Add-Card-(CVV-Entered)-&lt;f&gt;" transform="translate(-359.000000, -603.000000)"><g id="Payment-Options" transform="translate(140.000000, 370.000000)"><g id="Payments---Tab" transform="translate(0.000000, 71.000000)"><path d="M799.5,95.5 L799.5,694.5 L0.5,694.5 L0.5,95.5 L405.5,95.5 L405.5,20.5 L779.5,20.5 L779.5,95.5 L799.5,95.5 Z" id="Selected-Tab" stroke="'+c1+'" fill="'+c2+'"></path><g id="Tabs-Bar" transform="translate(20.000000, 115.000000)"><path d="M759.5,559.5 L0.5,559.5 L0.5,0.5 L212.383213,0.5 L759.5,0.5 L759.5,559.5 Z" id="Grey-Box" stroke="'+c1+'" fill="'+c3+'"></path><g id="Bank-1" transform="translate(179.000000, 20.000000)"><path d="M559.5,95.5 L0.5,95.5 L0.5,0.5 L559.5,0.5 L559.5,95.5 Z" id="Box" stroke="'+c1+'" fill="'+c2+'"></path><g id="Radio-Button" transform="translate(16.000000, 23.000000)" stroke="'+c6+'" stroke-width="1.5"><circle id="Unselected" cx="12" cy="12" r="7.25"></circle></g></g></g></g></g></g></g></svg>';

}
exports["selectedRadioBBIcon"] = selectedRadioBBIcon;






var getLocalIcon = function(wallet){
    return function (theme){
        if (theme=="dark"){
            if (wallet.toLowerCase() == "new_label"){
                return 'data:image/svg+xml;utf8,'+encodeURIComponent('<svg width="35px" height="15px" viewBox="0 0 35 15" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="3.-Default-Option" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="3.-PW---No-DPO+-No-PWO" transform="translate(-745.000000, -484.000000)"><rect fill="#FFFFFF" x="0" y="0" width="1440" height="1240"></rect><g id="Payment-Options" transform="translate(140.000000, 370.000000)"><g id="Payments---Tab" transform="translate(0.000000, 71.000000)"><g id="Other-Payment-Options" transform="translate(405.000000, 20.000000)"><g id="Tag" transform="translate(200.000000, 23.000000)"><g id="Group-15"><rect id="Rectangle" fill="#D30019" x="0" y="0" width="35" height="15" rx="7.5"></rect><text id="New" font-family="Helvetica" font-size="10" font-weight="normal" fill="#FFFFFF"><tspan x="7.49755859" y="12">New</tspan></text></g></g></g></g></g></g></g></svg>')
            }

            if (wallet.toLowerCase() == "amazonpay"){
                var c1 = encodeURIComponent("#ffffff");
                var c2 = encodeURIComponent("#F7971F")
                var c3 = encodeURIComponent("#path-1");
                var c4 = encodeURIComponent("#mask-2")
                return 'data:image/svg+xml;utf8, <svg width="42px" height="42px" viewBox="0 0 42 42" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><defs><polygon id="path-1" points="0.098767184 0.0670283531 25.4479483 0.0670283531 25.4479483 17.4756322 0.098767184 17.4756322"></polygon></defs><g id="Icons" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="icons" transform="translate(-19.000000, -269.000000)"><g id="Group-4" transform="translate(19.000000, 269.000000)"><g id="Amazon_pay_logo_circle"><rect id="Rectangle" fill="'+c2+'" x="0" y="0" width="42" height="42" rx="21"></rect><g id="Logo" transform="translate(7.148936, 9.829787)"><g id="Group-3" transform="translate(2.173893, 0.295178)"><mask id="mask-2" fill="white"><use xlink:href="'+c3+'"></use></mask><g id="Clip-2"></g><path d="M25.4130211,14.8216717 L25.4130211,14.8213094 L23.5637632,2.32360126 C23.3967358,1.20368429 22.2956592,0.0500719913 21.1304528,0.0671008162 C21.0065409,0.0689123933 20.8858899,0.0808688022 20.7677751,0.101520781 L20.7677751,0.10079615 L0.560357032,4.36887182 C0.299489928,4.40292947 0.098767184,4.62394188 0.098767184,4.89350455 C0.098767184,5.18371921 0.33572147,5.41958655 0.6277477,5.41958655 C0.650935887,5.41958655 18.4181596,2.18447214 18.4181596,2.18447214 L18.4181596,2.18483446 C21.0761055,1.69317243 21.5387823,3.21852036 21.7507368,4.61597094 L23.7191965,17.4756322 C24.6459993,17.1691134 25.462296,16.2169484 25.4479935,15.1879726 C25.4463541,15.0633361 25.4351223,14.9401489 25.4130211,14.8216717" id="Fill-1" fill="'+c1+'" mask="url('+c4+')"></path></g><path d="M20.2114397,15.8561547 C20.0447746,15.9960085 19.8864428,15.9217338 19.9599928,15.7373153 C20.2041934,15.1282631 20.7502028,13.7641455 20.4911472,13.4333515 C20.2328163,13.1021952 18.78283,13.2764689 18.1313869,13.3543668 C17.9342873,13.3779173 17.9034905,13.2058174 18.0813874,13.0808186 C19.2382605,12.2681451 21.1331702,12.5025632 21.3534579,12.7750244 C21.5759196,13.0489348 21.2954875,14.9500039 20.2114397,15.8561547 M19.6505755,14.3927627 C17.6299424,15.8844153 14.7002598,16.6778861 12.1770952,16.6778861 C8.64162136,16.6778861 5.45759343,15.3706521 3.04819588,13.1953103 C2.85906723,13.0242974 3.02790621,12.7909663 3.25507798,12.9228491 C5.85469114,14.4358783 9.06951587,15.3467393 12.3897744,15.3467393 C14.629246,15.3467393 17.0911793,14.8818886 19.3567377,13.9210281 C19.6984011,13.7761019 19.9849926,14.1463883 19.6505755,14.3927627 M24.6943684,18.656853 L24.6940061,18.6535921 C24.6940061,18.6525052 24.6932815,18.6521429 24.6932815,18.6514182 L22.8161253,6.24791206 C22.6654021,5.12654583 21.70273,4.26097429 20.5382482,4.26097429 C20.4146987,4.26097429 20.2922361,4.27003217 20.1733966,4.28959721 L3.29529499,6.8595005 L1.59277483,7.11855602 C1.00111374,7.20913488 0.433727792,7.03594811 3.62315422e-05,6.68921225 C3.62315422e-05,6.68921225 1.92248186,19.3742375 1.92936585,19.403585 L1.93262669,19.4303964 L1.93371364,19.4314833 C2.13190017,20.5006761 3.06739859,21.3093642 4.1952865,21.3093642 C4.20035892,21.3093642 4.20398207,21.3090018 4.20905449,21.3090018 C4.21122838,21.3090018 4.21521385,21.3093642 4.21775006,21.3093642 L22.4164914,21.3028425 L22.417216,21.3028425 L22.4186653,21.3028425 C23.691117,21.3028425 24.7208175,20.2716928 24.7208175,19.0028642 C24.7208175,18.8858363 24.711035,18.7702577 24.6943684,18.656853" id="Fill-4" fill="'+c1+'"></path></g></g></g></g></g></svg>'
            }
            if (wallet.toLowerCase() == "paytm"){
                var c1 = encodeURIComponent("#EBEBEB")
                var c2 = encodeURIComponent("#02B9EF")
                var c3 = encodeURIComponent("#06306F")
                var c4 = encodeURIComponent("")
                var c5 = encodeURIComponent("")
                var c6 = encodeURIComponent("")


                return 'data:image/svg+xml;utf8,<svg width="40px" height="40px" viewBox="0 0 40 40" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Icons" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="icons" transform="translate(-133.000000, -269.000000)"><g id="Group-5" transform="translate(133.000000, 269.000000)"><rect id="Rectangle" fill="'+c1+'" x="0" y="0" width="40" height="40" rx="20"></rect><g id="paytm_icon-icons.com_62731" transform="translate(6.000000, 15.000000)" fill-rule="nonzero"><path d="M22.5475069,1.95678898 C22.6703822,1.86009575 22.7318199,1.81174914 22.7932576,1.76340252 C23.6472415,1.05028995 24.7408323,1.1288532 25.4719408,1.97491896 C25.545666,2.05952554 25.5825286,2.06556887 25.6562539,1.99304894 C25.705404,1.938659 25.7606979,1.89635571 25.8098481,1.84196577 C26.3812186,1.29202302 27.1369021,1.1288532 27.8434355,1.43706287 C28.5868316,1.76340252 28.9861765,2.33751858 28.9923203,3.14128105 C29.0046078,4.87571587 28.9984641,6.60410736 28.9984641,8.33854218 C28.9984641,8.95496153 28.6114066,9.34173445 27.990886,9.34173445 C27.7451352,9.34173445 27.4993845,9.32360447 27.2536337,9.34777777 C27.0017392,9.37195108 26.928014,9.29338783 26.928014,9.03352477 C26.9403015,7.34139324 26.9341578,5.64926171 26.9341578,3.95713018 C26.9341578,3.88461026 26.9341578,3.81813366 26.9341578,3.74561374 C26.928014,3.35279749 26.768276,3.18962767 26.3873623,3.17149769 C26.0433113,3.15336771 25.8037043,3.35884082 25.7668417,3.70331045 C25.7606979,3.7818737 25.7668417,3.86648028 25.7668417,3.94504353 C25.7668417,5.40752864 25.7668417,6.86397042 25.7668417,8.32645553 C25.7668417,8.96704818 25.3920718,9.34173445 24.7408323,9.33569112 C24.4090688,9.32964779 23.9605737,9.48073097 23.7701168,9.26921452 C23.6042351,9.08791472 23.7148229,8.65883851 23.7148229,8.33854218 C23.7148229,6.83979711 23.7148229,5.33500872 23.7148229,3.83626365 C23.7148229,3.30445088 23.3646281,3.03250117 22.9099892,3.21380098 C22.6273759,3.32862419 22.5352193,3.55222728 22.5352193,3.84230697 C22.5413631,5.24435881 22.5352193,6.64036732 22.5352193,8.04241916 C22.5352193,8.15119905 22.5352193,8.26602226 22.5352193,8.37480214 C22.516788,8.96100485 22.1358744,9.32964779 21.5460726,9.33569112 C21.3003218,9.34173445 21.0545711,9.31756114 20.8088203,9.34173445 C20.5446382,9.36590775 20.4647693,9.29338783 20.470913,9.01539479 C20.4832006,6.64036732 20.4770568,4.26533986 20.4770568,1.88426906 C20.4770568,1.7815325 20.4832006,1.68483927 20.4770568,1.58210271 C20.4647693,1.44914952 20.5200632,1.40684623 20.6552261,1.40684623 C21.2204528,1.41288956 21.7795358,1.41288956 22.3447625,1.40684623 C22.4860692,1.40684623 22.5536506,1.4431062 22.5413631,1.59418937 C22.5290756,1.70296925 22.5413631,1.79966248 22.5475069,1.95678898 Z" id="Path" fill="'+c2+'"></path><path d="M9.8061992,5.40686296 C9.8061992,6.20610952 9.8061992,6.99952216 9.8061992,7.79876872 C9.80029445,8.83720586 9.25115306,9.37392617 8.20010824,9.37976009 C7.73953804,9.37976009 7.27306309,9.38559401 6.81249289,9.37976009 C5.87954299,9.36809226 5.19459244,8.75553103 5.14735447,7.83377222 C5.11192599,7.17453966 5.10602124,6.50947318 5.14144972,5.85024062 C5.18868769,4.90514615 5.92087622,4.24007967 6.88925459,4.22841183 C7.14315867,4.22257792 7.40296751,4.22841183 7.65687159,4.22841183 C7.90487093,4.22257792 7.99934687,4.08256392 7.99344212,3.84920726 C7.99344212,3.6158506 7.88715669,3.51084011 7.65096684,3.52250794 C7.38525327,3.52834186 7.11953969,3.52834186 6.85382611,3.52250794 C6.20430403,3.51084011 5.84411452,3.16080512 5.85001926,2.53074214 C5.85001926,2.27404981 5.73192434,1.92984874 5.90316198,1.77816691 C6.05078063,1.64982075 6.38735116,1.73149558 6.64125525,1.73149558 C7.30258682,1.72566166 7.95801365,1.73149558 8.61934522,1.73149558 C9.32201001,1.73149558 9.80029445,2.20404282 9.8061992,2.90411279 C9.81210394,3.73252893 9.8061992,4.57261291 9.8061992,5.40686296 Z M7.99934687,6.73116201 C7.99934687,6.63198543 7.99934687,6.53864276 7.99934687,6.43946618 C7.99934687,5.8444067 7.99934687,5.8444067 7.39706276,5.86774237 C7.0959207,5.8794102 6.93058781,6.03109203 6.92468307,6.3402896 C6.91877832,6.58531409 6.91877832,6.82450467 6.92468307,7.06952916 C6.93058781,7.48957115 7.11953969,7.60041556 7.73363329,7.61791731 C8.18829875,7.62958514 7.95801365,7.31455365 7.99934687,7.13953616 C8.02296585,7.01119 7.99344212,6.86534209 7.99934687,6.73116201 Z" id="Shape" fill="'+c3+'"></path><path d="M15.3405479,4.75718257 C15.3405479,5.65294341 15.3523572,6.5545589 15.3346433,7.45031975 C15.322834,8.14116798 15.1575043,8.76761511 14.4843765,9.13645781 C14.2127635,9.28282396 13.9175319,9.364789 13.6104911,9.37064365 C12.9314586,9.38235294 12.2524261,9.37064365 11.5733936,9.38235294 C11.408064,9.38235294 11.3726362,9.32380648 11.3785408,9.17744033 C11.3903501,8.9315452 11.3726362,8.69150471 11.3844454,8.44560957 C11.3962547,7.97723789 11.7564372,7.62010448 12.2288076,7.60254054 C12.5358484,7.59083125 12.8369845,7.5966859 13.1440253,7.60254054 C13.3920198,7.60254054 13.5278263,7.50301156 13.5278263,7.23955249 C13.5278263,6.97023877 13.3979244,6.87656443 13.14993,6.87070979 C12.7366058,6.8590005 12.3232817,6.91754696 11.9158622,6.80045404 C11.2132112,6.59554142 10.6995083,6.01593147 10.6817944,5.29581 C10.6463666,4.15415402 10.6699851,3.01249805 10.6640805,1.87084207 C10.6640805,1.74203985 10.7054129,1.70105733 10.8353148,1.70691198 C11.3194945,1.71276662 11.7977696,1.71862127 12.2819493,1.70691198 C12.494516,1.70105733 12.4649928,1.81815025 12.4649928,1.94695247 C12.4649928,2.80758543 12.4649928,3.66236376 12.4649928,4.52299673 C12.4649928,4.89769407 12.6421317,5.09675204 12.9727911,5.10846133 C13.3624967,5.12017062 13.5337309,4.96209518 13.5337309,4.56983389 C13.5337309,3.70334628 13.5396356,2.83100402 13.5278263,1.9645164 C13.5278263,1.75960379 13.580968,1.70105733 13.7876301,1.70691198 C14.2186681,1.72447592 14.6556107,1.73033056 15.0866488,1.70691198 C15.322834,1.69520269 15.3582618,1.78302238 15.3523572,1.98793499 C15.3346433,2.90711442 15.3405479,3.83214849 15.3405479,4.75718257 Z" id="Path" fill="'+c3+'"></path><path d="M0.0402278717,5.88785991 C0.0402278717,4.88794033 0.0402278717,3.88188627 0.0402278717,2.88196669 C0.0402278717,1.90045054 0.616128007,1.28700295 1.56224966,1.2992719 C2.17928552,1.30540637 2.79632138,1.22565819 3.4074807,1.34834771 C4.18905946,1.50784408 4.68268815,2.1396951 4.68856468,2.98011831 C4.69444121,3.86961732 4.68856468,4.75911633 4.68856468,5.64861534 C4.68856468,6.76508997 4.05977576,7.4460168 2.99024693,7.47668918 C2.66703767,7.48895813 2.34382841,7.48282365 2.02061915,7.47668918 C1.87370585,7.4705547 1.809064,7.51963051 1.81494053,7.68526136 C1.8266936,7.9306404 1.82081707,8.17601943 1.81494053,8.42139847 C1.80318747,8.9489634 1.44471902,9.32930091 0.945213799,9.33543539 C0.651387199,9.34156986 0.281165683,9.46425938 0.0931166597,9.28635958 C-0.0831793001,9.12072873 0.0461044037,8.72812227 0.0402278717,8.43366742 C0.0343513397,7.58710974 0.0402278717,6.73441759 0.0402278717,5.88785991 Z M1.81494053,4.3849133 C1.81494053,4.58121653 1.81494053,4.77138528 1.81494053,4.96768851 C1.81494053,5.6608843 1.81494053,5.6608843 2.47898865,5.59953954 C2.76693872,5.57500163 2.90209895,5.42777421 2.90797548,5.12718489 C2.91385202,4.78365424 2.89622242,4.43398911 2.91385202,4.09045845 C2.94911121,3.09667335 2.77281525,3.19482497 1.99711303,3.18869049 C1.85607626,3.18869049 1.809064,3.23163182 1.809064,3.37885924 C1.82081707,3.71012094 1.81494053,4.04751712 1.81494053,4.3849133 Z" id="Shape" fill="'+c3+'"></path><path d="M16.9078167,5.98974347 C16.9078167,5.1860647 16.9017631,4.37634323 16.9138704,3.57266446 C16.9138704,3.38534084 16.8714947,3.30678577 16.6656697,3.31282847 C16.3932544,3.32491386 16.0300339,3.40346893 15.878692,3.27052959 C15.6910281,3.09529136 15.8363163,2.72064411 15.8121016,2.43059463 C15.8121016,2.41246654 15.8121016,2.38829575 15.8121016,2.37016766 C15.8121016,2.08616087 15.7212965,1.75381251 15.84237,1.54231809 C15.9634435,1.33082368 16.3327176,1.46376303 16.5930256,1.39729335 C17.107588,1.26435401 17.4950232,0.968261829 17.8098143,0.557358397 C18.0277465,0.279394311 18.3001619,0.0799852921 18.651275,0.0135156193 C18.838939,-0.0227405658 18.9539588,0.00143022427 18.9418514,0.243138125 C18.9236904,0.581529187 18.9418514,0.925962946 18.9357977,1.27039671 C18.9297441,1.41542145 18.9842271,1.46376303 19.1295153,1.45772033 C19.3716623,1.45167763 19.6138093,1.46376303 19.8559563,1.45167763 C20.0012445,1.44563493 20.0496739,1.50001921 20.0436202,1.64504395 C20.0375665,2.14054515 20.0375665,2.63000365 20.0436202,3.12550485 C20.0436202,3.26448689 20.0072982,3.33699926 19.8559563,3.31887117 C19.8256879,3.31282847 19.7954196,3.31887117 19.7651512,3.31887117 C19.4987895,3.34304196 19.1295153,3.19801722 18.9902808,3.37929814 C18.8571,3.54849367 18.9418514,3.89897013 18.9418514,4.17089152 C18.9418514,5.81450525 18.9357977,7.45207628 18.9479051,9.09569 C18.9479051,9.32531251 18.8873683,9.39782488 18.6573287,9.37969679 C18.4393964,9.3615687 18.2154105,9.37969679 17.9914245,9.37365409 C17.3376276,9.3494833 16.9078167,8.90836638 16.9078167,8.25575505 C16.9078167,7.50041786 16.9078167,6.74508067 16.9078167,5.98974347 Z" id="Path" fill="'+c2+'"></path></g></g></g></g></svg>'
            }
            if (wallet.toLowerCase() == "mobikwik"){
                return "https://payments.juspay.in/web/images/mobiwik_zee"
            }
            else if (wallet== "amex"){
                var c1 = encodeURIComponent("#ffffff");
                var c2 = encodeURIComponent("#306FC5");
                var c3 = encodeURIComponent("#1678CF");
                var c4 = encodeURIComponent("#f1f1f1")
                return 'data:image/svg+xml;utf8,<svg width="40px" height="23px" viewBox="0 0 40 23" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Icons" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="icons" transform="translate(-314.000000, -152.000000)"><g id="ic_-amex" transform="translate(314.000000, 152.000000)"><path d="M3.8458278,-1.55376046e-16 L36.1541722,1.55376046e-16 C37.4914503,-9.02779573e-17 37.9763797,0.139238417 38.4652686,0.400699056 C38.9541574,0.662159695 39.3378403,1.04584256 39.5993009,1.53473144 C39.8607616,2.02362033 40,2.50854969 40,3.8458278 L40,19.1541722 C40,20.4914503 39.8607616,20.9763797 39.5993009,21.4652686 C39.3378403,21.9541574 38.9541574,22.3378403 38.4652686,22.5993009 C37.9763797,22.8607616 37.4914503,23 36.1541722,23 L3.8458278,23 C2.50854969,23 2.02362033,22.8607616 1.53473144,22.5993009 C1.04584256,22.3378403 0.662159695,21.9541574 0.400699056,21.4652686 C0.139238417,20.9763797 6.01853049e-17,20.4914503 -1.03584031e-16,19.1541722 L1.03584031e-16,3.8458278 C-6.01853049e-17,2.50854969 0.139238417,2.02362033 0.400699056,1.53473144 C0.662159695,1.04584256 1.04584256,0.662159695 1.53473144,0.400699056 C2.02362033,0.139238417 2.50854969,9.02779573e-17 3.8458278,-1.55376046e-16 Z" id="Rectangle" fill="'+c3+'"></path><g id="Group" transform="translate(6.000000, 7.000000)" fill-rule="nonzero"><polygon id="Path" fill="'+c4+'" points="2.45572727 2.22047059 3.42923636 2.22047059 2.94260909 1.07517647"></polygon><polygon id="Path" fill="'+c4+'" points="7.63292727 6.06511765 7.63292727 6.59694118 9.27022727 6.59694118 9.27022727 7.16970588 7.63292727 7.16970588 7.63292727 7.78341176 9.44713636 7.78341176 10.2878364 6.92423529 9.49142727 6.06511765"></polygon><polygon id="Path" fill="'+c4+'" points="20.4207818 1.07517647 19.8897364 2.22047059 20.9076 2.22047059"></polygon><polygon id="Path" fill="'+c4+'" points="12.3676 8.11029412 12.3676 5.77888235 11.2170545 6.92423529"></polygon><path d="M14.5358182,6.43341176 C14.4915273,6.188 14.3143636,6.06511765 14.0487455,6.06511765 L13.1197182,6.06511765 L13.1197182,6.80135296 L14.0932909,6.80135296 C14.3587182,6.80141176 14.5358182,6.67876471 14.5358182,6.43341176 Z" id="Path" fill="'+c4+'"></path><path d="M17.633,6.71976471 C17.7215182,6.67876471 17.7659364,6.55594118 17.7659364,6.43347059 C17.8102273,6.27 17.7215182,6.18805882 17.633,6.14711765 C17.5447364,6.10617647 17.4118,6.10617647 17.2792455,6.10617647 L16.3940636,6.10617647 L16.3940636,6.76070588 L17.2791818,6.76070588 C17.4117364,6.76070588 17.5446727,6.76070588 17.633,6.71976471 Z" id="Path" fill="'+c4+'"></path><path d="M24.0047182,0.0116470588 L24.0047182,0.502470588 L23.7394182,0.0116470588 L21.6599091,0.0116470588 L21.6599091,0.502470588 L21.3942909,0.0116470588 L18.5623455,0.0116470588 C18.0756545,0.0116470588 17.6772273,0.0935294118 17.3234091,0.257117647 L17.3234091,0.0116470588 L15.3322909,0.0116470588 L15.3322909,0.0525882353 L15.3322909,0.257117647 C15.1110909,0.0935882353 14.8456,0.0116470588 14.4915909,0.0116470588 L7.3675,0.0116470588 L6.88055455,1.03423529 L6.39392727,0.0116470588 L4.80104545,0.0116470588 L4.13725455,0.0116470588 L4.13725455,0.502470588 L3.91599091,0.0116470588 L3.8717,0.0116470588 L1.96903636,0.0116470588 L1.08391818,1.934 L0.0663090909,4.02 L0.0476,4.061 L0.0663090909,4.061 L2.32304545,4.061 L2.3513,4.061 L2.36733636,4.02 L2.63295455,3.40658824 L3.20816364,3.40658824 L3.47365455,4.061 L6.03985455,4.061 L6.03985455,4.02 L6.03985455,3.57017647 L6.26124545,4.06094118 L7.54434545,4.06094118 L7.76548182,3.57017647 L7.76548182,4.02 L7.76548182,4.061 L8.78321818,4.061 L13.8719,4.061 L13.9161909,4.061 L13.9161909,2.99747059 L14.0049,2.99747059 C14.0932909,2.99747059 14.0932909,2.99747059 14.0932909,3.12023529 L14.0932909,4.02005882 L17.2792455,4.02005882 L17.2792455,3.77464706 C17.5447364,3.89752941 17.9427182,4.02005882 18.4737636,4.02005882 L19.8014091,4.02005882 L20.0669,3.36576471 L20.6862091,3.36576471 L20.9517,4.02005882 L23.5180273,4.02005882 L23.5180273,3.61111765 L23.5180273,3.40664706 L23.9164545,4.02005882 L24.0047182,4.02005882 L24.0491364,4.02005882 L25.9960909,4.02005882 L25.9960909,0.0116470588 L24.0048455,0.0116470588 L24.0048455,0.0116470588 L24.0047182,0.0116470588 L24.0047182,0.0116470588 Z M9.00460909,3.44758824 L8.56201818,3.44758824 L8.29659091,3.44758824 L8.29659091,3.20235294 L8.29659091,1.19788235 L8.2523,1.29170588 L8.2523,1.29058824 L7.22291818,3.44758824 L7.19033636,3.44758824 L6.95367273,3.44758824 L6.57077273,3.44758824 L5.50887273,1.19788235 L5.50887273,3.44758824 L4.00450909,3.44758824 L3.69479091,2.83405882 L2.19017273,2.83405882 L1.88039091,3.44758824 L1.10256364,3.44758824 L2.41156364,0.584352941 L3.51769091,0.584352941 L4.75662727,3.32476471 L4.75662727,0.584352941 L5.02205455,0.584352941 L5.92893636,0.584352941 L5.94980909,0.625352941 L5.95133636,0.625352941 L6.50961818,1.74852941 L6.91052727,2.59005882 L6.92471818,2.54764706 L7.80964545,0.584411765 L9.00460909,0.584411765 L9.00460909,3.44758824 L9.00460909,3.44758824 L9.00460909,3.44758824 Z M12.0578818,1.19788235 L10.3321273,1.19788235 L10.3321273,1.72958824 L12.0135909,1.72958824 L12.0135909,2.30217647 L10.3321273,2.30217647 L10.3321273,2.875 L12.0578818,2.875 L12.0578818,3.48835294 L9.57994545,3.48835294 L9.57994545,0.584411765 L12.0578818,0.584411765 L12.0578818,1.19788235 L12.0578818,1.19788235 Z M15.2139273,2.25258824 C15.2150727,2.25352941 15.2165364,2.25417647 15.2179364,2.25505882 C15.2346727,2.27141176 15.2489909,2.28782353 15.2612091,2.30352941 C15.3459727,2.40623529 15.4145091,2.559 15.4179455,2.783 C15.4182,2.78688235 15.4183909,2.79070588 15.4186455,2.79458824 C15.4186455,2.79682353 15.4190909,2.79876471 15.4190909,2.80105882 C15.4190909,2.80235294 15.4189636,2.80335294 15.4189636,2.80458824 C15.4199818,2.82711765 15.4206182,2.85011765 15.4206182,2.875 L15.4206182,3.44764706 L14.7570182,3.44764706 L14.7570182,3.12029412 C14.7570182,2.95670588 14.7570182,2.71123529 14.6240818,2.54764706 C14.5822727,2.509 14.5399545,2.48352941 14.4938818,2.46541176 C14.4275727,2.42570588 14.3018909,2.42505882 14.0932909,2.42505882 L13.2965636,2.42505882 L13.2965636,3.44764706 L12.5445091,3.44764706 L12.5445091,0.584411765 L14.2261,0.584411765 C14.6240818,0.584411765 14.8897,0.584411765 15.1110273,0.707117647 C15.3278364,0.827470588 15.4588,1.02705882 15.4637636,1.34311765 C15.4535182,1.77841176 15.1512455,2.01723529 14.9339273,2.09776471 C14.9339273,2.09776471 15.0815636,2.12523529 15.2139273,2.25258824 Z M16.7039727,3.44758824 L15.9516636,3.44758824 L15.9516636,0.584352941 L16.7039727,0.584352941 L16.7039727,3.44758824 Z M25.3325545,3.44758824 L24.3589182,3.44758824 L22.9430091,1.27964706 L22.9430091,3.07935294 L22.9383636,3.07511765 L22.9383636,3.44935294 L22.1908273,3.44935294 L22.1908273,3.44764706 L21.4385182,3.44764706 L21.1730273,2.83411765 L19.6241818,2.83411765 L19.3586909,3.48841176 L18.5179909,3.48841176 C18.1642364,3.48841176 17.7214545,3.40670588 17.4560909,3.16129412 C17.1906,2.91588235 17.0579818,2.58864706 17.0579818,2.057 C17.0579818,1.648 17.1463727,1.23870588 17.4560909,0.911588235 C17.6771636,0.666 18.0755909,0.584411765 18.5622182,0.584411765 L19.2703636,0.584411765 L19.2703636,1.19788235 L18.5622182,1.19788235 C18.2967273,1.19788235 18.1642364,1.23864706 17.9868818,1.36158824 C17.8543909,1.48423529 17.7658091,1.72958824 17.7658091,2.01588235 C17.7658091,2.34317647 17.8101,2.54764706 17.9868818,2.71123529 C18.1199455,2.83411765 18.2967909,2.87505882 18.5179909,2.87505882 L18.8277091,2.87505882 L19.8453182,0.625470588 L20.2880364,0.625470588 L20.9516364,0.625470588 L22.1907636,3.36588235 L22.1907636,3.22611765 L22.1907636,2.30229412 L22.1907636,2.22058824 L22.1907636,0.625529412 L23.2968909,0.625529412 L24.5798636,2.62964706 L24.5798636,0.625529412 L25.3324273,0.625529412 L25.3324273,3.44770588 L25.3325545,3.44770588 L25.3325545,3.44758824 L25.3325545,3.44758824 Z" id="Shape" fill="'+c1+'"></path><path d="M14.5829727,1.77688235 C14.5958909,1.76411765 14.6076,1.74964706 14.6175273,1.73305882 C14.6576182,1.67441176 14.7035636,1.56876471 14.6788091,1.42458824 C14.6777909,1.41135294 14.6751818,1.39911765 14.6721273,1.38729412 C14.6713,1.384 14.6714273,1.38111765 14.6704727,1.37782353 L14.6700273,1.37788235 C14.6480091,1.30782353 14.5918182,1.26476471 14.5357545,1.23864706 C14.4470455,1.19788235 14.3143,1.19788235 14.1816182,1.19788235 L13.2965,1.19788235 L13.2965,1.85241176 L14.1816182,1.85241176 C14.3143,1.85241176 14.4471091,1.85241176 14.5357545,1.81141176 C14.5490545,1.80517647 14.5608909,1.79670588 14.5722818,1.78711765 L14.5724091,1.78764706 C14.5724727,1.78758824 14.5769909,1.78323529 14.5829727,1.77688235 Z" id="Path" fill="'+c1+'"></path><path d="M27.9430455,7.53788235 C27.9430455,7.25158824 27.8546545,6.96517647 27.7219727,6.76070588 L27.7219727,4.92017647 L27.7199364,4.92017647 L27.7199364,4.79729412 C27.7199364,4.79729412 25.8652545,4.79729412 25.5892,4.79729412 C25.3132727,4.79729412 24.9784182,5.04264706 24.9784182,5.04264706 L24.9784182,4.79729412 L22.9430727,4.79729412 C22.6331636,4.79729412 22.2351818,4.87917647 22.0579545,5.04264706 L22.0579545,4.79729412 L18.4296636,4.79729412 L18.4296636,4.92017647 L18.4296636,5.04258824 C18.1643636,4.83823529 17.6772273,4.79723529 17.4561545,4.79723529 L15.0666727,4.79723529 L15.0666727,4.92011765 L15.0666727,5.04252941 C14.8454727,4.83817647 14.3142364,4.79717647 14.0486182,4.79717647 L11.3940909,4.79717647 L10.7744636,5.41076471 L10.1991909,4.79717647 L10.0139455,4.79717647 L9.7125,4.79717647 L6.21695455,4.79717647 L6.21695455,4.92005882 L6.21695455,5.24317647 L6.21695455,8.92852941 L10.1108636,8.92852941 L10.7506636,8.33694118 L11.3052545,8.92852941 L11.3496727,8.92852941 L13.5933636,8.92852941 L13.6950545,8.92852941 L13.7393455,8.92852941 L13.7833818,8.92852941 L13.7833818,8.51952941 L13.7833818,7.98782353 L14.0047727,7.98782353 C14.3142364,7.98782353 14.7126636,7.98782353 15.0223818,7.86494118 L15.0223818,8.88741176 L15.0223818,8.96941176 L17.0136273,8.96941176 L17.0136273,8.88741176 L17.0136273,7.90588235 L17.1018909,7.90588235 C17.2348273,7.90588235 17.2348273,7.90588235 17.2348273,8.02858824 L17.2348273,8.88741176 L17.2348273,8.96941176 L23.2524727,8.96941176 C23.6510273,8.96941176 24.0490091,8.88741176 24.2702091,8.72382353 L24.2702091,8.88741176 L24.2702091,8.96941176 L26.1730636,8.96941176 C26.5710455,8.96941176 26.9696,8.92852941 27.2350909,8.76482353 C27.6424909,8.53888235 27.8992636,8.13894118 27.9374455,7.66194118 C27.9392273,7.64782353 27.9414545,7.63370588 27.9430455,7.61952941 L27.9402455,7.61723529 C27.9419,7.59076471 27.9430455,7.56458824 27.9430455,7.53788235 Z M14.0048364,7.29252941 L13.1196545,7.29252941 L13.1196545,7.41494118 L13.1196545,7.66047059 L13.1196545,7.90594118 L13.1196545,8.35588235 L11.6652455,8.35588235 L10.8187545,7.45594118 L10.8158273,7.45894118 L10.7744636,7.41494118 L9.80101818,8.35588235 L6.96913636,8.35588235 L6.96913636,5.49276471 L9.84530909,5.49276471 L10.6310909,6.28835294 L10.7962909,6.45494118 L10.8186909,6.43347059 L11.7480364,5.49282353 L14.0931636,5.49282353 C14.5482909,5.49282353 15.0579545,5.59664706 15.2510909,6.02470588 C15.2743182,6.10947059 15.2878091,6.20370588 15.2878091,6.31076471 C15.2878091,7.12876471 14.6683091,7.29252941 14.0048364,7.29252941 Z M18.4296636,7.25152941 C18.5180545,7.37394118 18.5623455,7.53782353 18.5623455,7.78335294 L18.5623455,8.35582353 L17.8102273,8.35582353 L17.8102273,7.98782353 C17.8102273,7.82394118 17.8102273,7.53782353 17.6772909,7.41494118 C17.5890273,7.29252941 17.4118,7.29252941 17.1465,7.29252941 L16.3500909,7.29252941 L16.3500909,8.35588235 L15.5979091,8.35588235 L15.5979091,5.45182353 L17.2792455,5.45182353 C17.633,5.45182353 17.9427182,5.45182353 18.1643636,5.57441176 C18.3854364,5.69723529 18.5623455,5.90176471 18.5623455,6.229 C18.5623455,6.67882353 18.2527545,6.92423529 18.0314273,7.00617647 C18.2527545,7.08764706 18.3854364,7.16970588 18.4296636,7.25152941 Z M21.4829364,6.06511765 L19.7569909,6.06511765 L19.7569909,6.59688235 L21.4385182,6.59688235 L21.4385182,7.16964706 L19.7569909,7.16964706 L19.7569909,7.74235294 L21.4829364,7.74235294 L21.4829364,8.35582353 L19.005,8.35582353 L19.005,5.45176471 L21.4829364,5.45176471 L21.4829364,6.06511765 Z M23.3412455,8.35588235 L21.9252091,8.35588235 L21.9252091,7.74241176 L23.3412455,7.74241176 C23.4737364,7.74241176 23.5624455,7.74241176 23.6511545,7.66052941 C23.6954455,7.61952941 23.7394182,7.53788235 23.7394182,7.456 C23.7394182,7.374 23.6954455,7.29258824 23.6511545,7.25158824 C23.6067364,7.21070588 23.5180909,7.16976471 23.3854091,7.16976471 C22.6773909,7.12882353 21.8366909,7.16976471 21.8366909,6.27 C21.8366909,5.86094118 22.1021818,5.41094118 22.8987182,5.41094118 L24.3588545,5.41094118 L24.3588545,6.10617647 L22.9872364,6.10617647 C22.8545545,6.10617647 22.7659091,6.10617647 22.6773273,6.14711765 C22.5890636,6.18811765 22.5890636,6.27 22.5890636,6.35170588 C22.5890636,6.47447059 22.6773273,6.51541176 22.7658455,6.556 C22.8545545,6.597 22.9429455,6.597 23.0313364,6.597 L23.4296364,6.597 C23.8279364,6.597 24.0933636,6.67882353 24.2703364,6.84247059 C24.4031455,6.96523529 24.4916636,7.16976471 24.4916636,7.456 C24.4916636,8.06935294 24.0934273,8.35588235 23.3412455,8.35588235 Z M27.1466364,8.06935294 C26.9698545,8.23317647 26.6599455,8.35588235 26.2172909,8.35588235 L24.8016364,8.35588235 L24.8016364,7.74241176 L26.2172273,7.74241176 C26.3500364,7.74241176 26.4387455,7.74241176 26.5271364,7.66052941 C26.5712364,7.61952941 26.6156545,7.53788235 26.6156545,7.456 C26.6156545,7.374 26.5712364,7.29258824 26.5271364,7.25158824 C26.4828455,7.21070588 26.3944545,7.16976471 26.2616455,7.16976471 C25.5538818,7.12882353 24.7129273,7.16976471 24.7129273,6.27005882 C24.7129273,5.88129412 24.9535364,5.53011765 25.5469455,5.42582353 C25.6179,5.41676471 25.6928636,5.41094118 25.7749545,5.41094118 L27.2352818,5.41094118 L27.2352818,6.10617647 L26.2616455,6.10617647 L25.9077636,6.10617647 L25.8634727,6.10617647 C25.7306636,6.10617647 25.6422727,6.10617647 25.5538818,6.14711765 C25.5093364,6.18811765 25.4650455,6.27 25.4650455,6.35170588 C25.4650455,6.47447059 25.5093364,6.51541176 25.6422727,6.556 C25.7306636,6.597 25.8191818,6.597 25.9077636,6.597 L25.9517364,6.597 L26.3058727,6.597 C26.4992636,6.597 26.6455,6.61905882 26.7793273,6.66252941 C26.9018909,6.70352941 27.3076364,6.87129412 27.3983818,7.30923529 C27.4062727,7.35511765 27.4120636,7.403 27.4120636,7.456 C27.4120636,7.70147059 27.3235455,7.90594118 27.1466364,8.06935294 Z" id="Shape" fill="'+c1+'"></path></g></g></g></g></svg>'
            }
            if (wallet == "diners"){
                var c1 = encodeURIComponent("#FCFCFC")
                var c2 = encodeURIComponent("#006AC9")
                var c3 = encodeURIComponent("#004A97")
                var c4 = encodeURIComponent("#023563")
                return 'data:image/svg+xml;utf8,<svg width="40px" height="23px" viewBox="0 0 40 23" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Icons" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="icons" transform="translate(-20.000000, -193.000000)"><g id="ic_diners" transform="translate(20.000000, 193.000000)"><path d="M3.8458278,-1.55376046e-16 L36.1541722,1.55376046e-16 C37.4914503,-9.02779573e-17 37.9763797,0.139238417 38.4652686,0.400699056 C38.9541574,0.662159695 39.3378403,1.04584256 39.5993009,1.53473144 C39.8607616,2.02362033 40,2.50854969 40,3.8458278 L40,19.1541722 C40,20.4914503 39.8607616,20.9763797 39.5993009,21.4652686 C39.3378403,21.9541574 38.9541574,22.3378403 38.4652686,22.5993009 C37.9763797,22.8607616 37.4914503,23 36.1541722,23 L3.8458278,23 C2.50854969,23 2.02362033,22.8607616 1.53473144,22.5993009 C1.04584256,22.3378403 0.662159695,21.9541574 0.400699056,21.4652686 C0.139238417,20.9763797 6.01853049e-17,20.4914503 -1.03584031e-16,19.1541722 L1.03584031e-16,3.8458278 C-6.01853049e-17,2.50854969 0.139238417,2.02362033 0.400699056,1.53473144 C0.662159695,1.04584256 1.04584256,0.662159695 1.53473144,0.400699056 C2.02362033,0.139238417 2.50854969,9.02779573e-17 3.8458278,-1.55376046e-16 Z" id="Rectangle" fill="'+c1+'"></path><g id="diners-club" transform="translate(9.000000, 3.000000)" fill-rule="nonzero"><path d="M4,9 C4,11.3134328 5.65217391,13.2537313 8,14 L8,4 C5.65217391,4.74626866 4,6.68656716 4,9 Z" id="Path" fill="'+c2+'"></path><path d="M10,4 L10,14 C12.3478261,13.2537313 14,11.238806 14,9 C14,6.68656716 12.3478261,4.74626866 10,4 Z" id="Path" fill="'+c3+'"></path><g id="Group" fill="'+c4+'"><path d="M13.4334661,0.072 L9.18167331,0.072 C4.12350598,0.072 0.0183266932,3.744 0.0183266932,9.072 C0.0183266932,13.968 4.12350598,17.928 9.18167331,17.928 L13.4334661,17.928 C18.4183267,17.928 22.9633466,13.968 22.9633466,9.072 C22.8900398,3.744 18.3450199,0.072 13.4334661,0.072 Z M9.18167331,17.136 C4.56334661,17.136 0.898007968,13.464 0.898007968,9 C0.898007968,4.464 4.63665339,0.792 9.18167331,0.792 C13.8,0.792 17.5386454,4.464 17.5386454,9 C17.4653386,13.536 13.7266932,17.136 9.18167331,17.136 Z" id="Shape"></path></g><path d="M21,7.56315787 C21,5.25819547 20.0586207,3.1693233 18.4655172,1.51263157 C16.9448276,0.5762406 15.1344828,0 13.2517241,0 L9.05172414,0 C4.05517241,0 0,3.67353382 0,9.00375937 C0,11.2366917 0.868965517,13.2535338 2.24482759,14.8381954 C3.69310345,15.8466165 5.43103448,16.5669172 7.3137931,16.8550375 C3.62068966,16.0627067 0.868965517,12.8213533 0.868965517,8.85969922 C0.868965517,4.3218045 4.56206897,0.648270675 9.05172414,0.648270675 C13.6137931,0.648270675 17.3068966,4.3218045 17.3068966,8.85969922 C17.3068966,13.3255639 13.7586207,16.9270676 9.26896552,16.9990977 C9.4137931,16.9990977 9.4862069,16.9990977 9.63103448,16.9990977 C15.8586207,17.0711278 21,12.8213533 21,7.56315787 Z" id="Path" fill="'+c3+'"></path><path d="M9.10771939,0 C4.08025829,0 0,3.65822785 0,8.96624473 C0,11.1181435 0.801479306,13.1265823 2.18585265,14.6329114 C2.25871441,14.7046414 2.33157616,14.7763713 2.40443792,14.8481013 C3.86167302,15.8523207 5.53749339,16.4978903 7.43189902,16.8565401 C3.71594951,16.0675105 0.947202817,12.8396624 0.947202817,8.89451477 C0.947202817,4.37552743 4.66315233,0.717299578 9.18058115,0.717299578 C13.7708717,0.717299578 17.4868212,4.37552743 17.4868212,8.89451477 C17.4868212,13.3417722 13.9165952,16.92827 9.39916641,17 C9.54488992,17 9.61775168,17 9.76347519,17 C10.127784,17 10.4920927,17 10.8564015,16.92827 C15.6652774,15.7805907 18.7254711,10.9746835 17.85113,5.88185654 C17.4139595,3.51476793 16.2481714,1.50632911 14.572351,0.0717299578 C14.2080423,0 13.8437335,0 13.4794247,0 L9.10771939,0 Z" id="Path" fill="'+c2+'"></path></g></g></g></g></svg>'
            }
            if (wallet == "discover"){
                return 'https://payments.juspay.in/web/images/discover_zee'
            }
            if (wallet == "jcb"){
                var c1 = encodeURIComponent("#FCFCFC")
                var c2 = encodeURIComponent("#0F549D")
                var c3 = encodeURIComponent("#B41F36")
                var c4 = encodeURIComponent("#329947")
                return 'data:image/svg+xml;utf8,<svg width="40px" height="23px" viewBox="0 0 40 23" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Icons" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="icons" transform="translate(-137.000000, -153.000000)"><g id="jcb_ic" transform="translate(137.000000, 153.000000)"><g id="jcb"><rect id="Rectangle" fill="'+c1+'" x="0" y="0" width="40" height="23" rx="3"></rect><path d="M15.3152492,3.02921053 L11.3182597,3.02921053 C9.37638161,3.02921053 8.07173931,4.41189474 8.07173931,6.46973684 L8.07173931,12.624 C8.07173931,12.627 8.07203729,12.6300789 8.07255876,12.6330789 C8.22579631,13.4047105 8.98900423,13.9864737 9.84779007,13.9864737 C10.7958962,13.9864737 11.5106819,13.2805263 11.5106819,12.3442105 L11.5106819,9.27086842 L13.9541392,9.27086842 L13.9541392,12.3041053 C13.9541392,14.0338421 11.8529894,14.8503158 10.4334004,14.8574211 C9.59368541,14.8537105 8.77080648,14.5803947 8.17566074,14.1072632 C8.16321997,14.0973947 8.14630945,14.0957368 8.13230427,14.1031579 C8.13230427,14.1031579 8.13215528,14.1032368 8.13200629,14.1033158 C8.12031047,14.0970789 8.10637979,14.0969211 8.09460947,14.1031579 C8.08067879,14.1106579 8.07181381,14.1258158 8.07181381,14.1424737 L8.07181381,20.9358158 C8.07181381,20.9600526 8.09028873,20.9797895 8.11330788,20.9797895 L12.2235308,20.9797895 C13.6726202,20.9797895 15.3568923,19.5292895 15.3568923,17.6591842 L15.3568923,3.07318421 C15.3567433,3.04878947 15.3383429,3.02921053 15.3152492,3.02921053 Z" id="Path" fill="'+c2+'" fill-rule="nonzero"></path><path d="M23.3472956,3.02921053 L19.3880008,3.02921053 C17.415803,3.02921053 16.1414805,4.36405263 16.1414805,6.43002632 L16.1414805,9.90655263 C16.1414805,9.92360526 16.1507179,9.93907895 16.1652446,9.94634211 C16.1798457,9.95352632 16.1969797,9.95115789 16.2094205,9.94026316 C16.5550058,9.63489474 17.1121588,9.27094737 17.8042233,9.27094737 L22.0615007,9.27094737 L22.0615007,10.0916842 C21.0277991,9.85184211 20.0716474,9.74257895 19.6140951,9.74257895 C18.5307049,9.74257895 17.6493469,10.6766053 17.6493469,11.8245789 L17.6496449,11.8372895 L17.6493469,11.8645263 C17.6493469,13.0125789 18.5307049,13.9465263 19.6140951,13.9465263 C20.1690877,13.9465263 21.1352963,13.8508421 22.0615007,13.6373684 L22.0615007,14.3781316 L17.8042233,14.3781316 C17.1918691,14.3781316 16.6116225,14.1350526 16.2121023,13.7114211 C16.2001085,13.6987895 16.1823041,13.6950789 16.1668835,13.7019474 C16.1513139,13.7087368 16.141257,13.7247632 16.141257,13.7425263 L16.141257,20.9355789 C16.141257,20.9598158 16.1597319,20.9795526 16.182751,20.9795526 L20.2549067,20.9795526 C21.7042195,20.9795526 23.3885671,19.5290526 23.3885671,17.6589474 L23.3885671,3.07318421 C23.3887152,3.04878947 23.3702402,3.02921053 23.3472956,3.02921053 Z" id="Path" fill="'+c3+'" fill-rule="nonzero"></path><g id="Group" transform="translate(24.240051, 3.000000)" fill="'+c4+'" fill-rule="nonzero"><path d="M1.29056262,10.8627632 C1.29056262,10.887 1.30918653,10.9067368 1.3320567,10.9067368 L3.10423368,10.9067368 C3.58003739,10.9067368 4.01322959,10.4475789 4.01322959,9.94357895 C4.01322959,9.43965789 3.5799629,8.98057895 3.10423368,8.98057895 L1.3320567,8.98057895 C1.30926103,8.98057895 1.29056262,9.00023684 1.29056262,9.02455263 L1.29056262,10.8627632 Z" id="Path"></path><path d="M7.2523004,0.0292105263 L3.33040247,0.0292105263 C2.28649495,0.0292105263 1.43821299,0.403342105 0.870779638,1.06184211 L0.870779638,1.06176316 C0.870779638,1.06176316 0.0461873022,1.91423684 0.0461873022,3.32013158 C0.0461873022,3.37207895 0.0461873022,3.42142105 0.0461873022,3.46965789 L0.0461873022,4.72602632 L0.0461873022,6.22697368 C0.0461873022,6.25121053 0.0646622231,6.27094737 0.0876813786,6.27094737 L4.83908835,6.27094737 C5.49107427,6.27094737 6.04204409,6.85484211 6.04204409,7.54586842 C6.04204409,8.23689474 5.49107427,8.82071053 4.83908835,8.82071053 C4.81614369,8.82071053 4.79759427,8.84036842 4.79759427,8.86468421 C4.79759427,8.889 4.8160692,8.90865789 4.83908835,8.90865789 C5.57689326,8.90865789 6.15505399,9.45110526 6.15505399,10.1435526 C6.15505399,10.8476053 5.58925953,11.3785263 4.83908835,11.3785263 L0.0876813786,11.3785263 C0.0647367187,11.3785263 0.0461873022,11.3981842 0.0461873022,11.4225 L0.0461873022,17.9359737 C0.0461873022,17.9602105 0.0646622231,17.9799474 0.0876813786,17.9799474 L4.160284,17.9799474 C5.60944785,17.9799474 7.29379447,16.5294474 7.29379447,14.6593421 L7.29379447,0.0731842105 C7.29379447,0.0487894737 7.27509606,0.0292105263 7.2523004,0.0292105263 Z" id="Path"></path><path d="M1.3320567,8.62886842 L3.10423368,8.62886842 C3.5764616,8.62886842 3.9753858,8.20610526 3.9753858,7.70565789 C3.9753858,7.20165789 3.54234259,6.74257895 3.06683687,6.74257895 L1.3320567,6.74257895 C1.30926103,6.74257895 1.29056262,6.76223684 1.29056262,6.78655263 L1.29056262,8.58497368 C1.29056262,8.60921053 1.30918653,8.62886842 1.3320567,8.62886842 Z" id="Path"></path></g></g></g></g></g></svg>'

            }
            if (wallet == "laser"){
                return 'https://payments.juspay.in/web/images/laser_zee'
            }
            if (wallet=="maestro"){
                return "https://payments.juspay.in/web/images/maestro_zee"
            }
            if (wallet=="mastercard"){
                var c1 = encodeURIComponent("#230924")
                var c2 = encodeURIComponent("#E85145")
                var c3 = encodeURIComponent("#F9B14C")
                var c4 = encodeURIComponent("#230924");
                var c5 = encodeURIComponent("#fcfcfc")
                var c6 = encodeURIComponent("#931B1D")


                var c7 = encodeURIComponent("#F16622");
                var c8 = encodeURIComponent("#F7981D")
                var c9 = encodeURIComponent("#CB2026")

                var a1 = encodeURIComponent("#ffffff")
                var a2 = encodeURIComponent("")
                var a3 = encodeURIComponent("")
                return 'data:image/svg+xml;utf8,<svg width="40px" height="23px" viewBox="0 0 40 23" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Icons" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="icons" transform="translate(-80.000000, -153.000000)"><g id="ic_Mastercard" transform="translate(80.000000, 153.000000)"><path d="M3.8458278,-1.55376046e-16 L36.1541722,1.55376046e-16 C37.4914503,-9.02779573e-17 37.9763797,0.139238417 38.4652686,0.400699056 C38.9541574,0.662159695 39.3378403,1.04584256 39.5993009,1.53473144 C39.8607616,2.02362033 40,2.50854969 40,3.8458278 L40,19.1541722 C40,20.4914503 39.8607616,20.9763797 39.5993009,21.4652686 C39.3378403,21.9541574 38.9541574,22.3378403 38.4652686,22.5993009 C37.9763797,22.8607616 37.4914503,23 36.1541722,23 L3.8458278,23 C2.50854969,23 2.02362033,22.8607616 1.53473144,22.5993009 C1.04584256,22.3378403 0.662159695,21.9541574 0.400699056,21.4652686 C0.139238417,20.9763797 6.01853049e-17,20.4914503 -1.03584031e-16,19.1541722 L1.03584031e-16,3.8458278 C-6.01853049e-17,2.50854969 0.139238417,2.02362033 0.400699056,1.53473144 C0.662159695,1.04584256 1.04584256,0.662159695 1.53473144,0.400699056 C2.02362033,0.139238417 2.50854969,9.02779573e-17 3.8458278,-1.55376046e-16 Z" id="Rectangle" fill="'+c5+'"></path><g id="mastercard-3" transform="translate(8.000000, 4.000000)" fill-rule="nonzero"><path d="M7.43150685,0.0654109589 C3.33582192,0.0913013699 0.0197260274,3.43068493 0.0197260274,7.5409589 C0.0197260274,11.6673973 3.36143836,15.0160274 7.47938356,15.0160274 C9.4110274,15.0160274 11.1727397,14.2781507 12.4982192,13.0682877 L12.4979452,13.0680137 L12.4989726,13.0680137 C12.7708904,12.8202055 13.0241096,12.5521233 13.2565753,12.2673973 L11.7286301,12.2673973 C11.5243836,12.0206164 11.3388356,11.7627397 11.1722603,11.4969863 L13.8076712,11.4969863 C13.9686301,11.2402055 14.1131507,10.9734247 14.2426027,10.6971233 L10.7376712,10.6971233 C10.6180137,10.4403425 10.5141781,10.1786301 10.4257534,9.91184932 L14.5545205,9.91184932 C14.8039041,9.16657534 14.9384247,8.3690411 14.9384247,7.5409589 C14.9384247,6.9909589 14.8786986,6.45582192 14.7667123,5.93965753 L10.2066438,5.93965753 C10.2635616,5.67390411 10.3345205,5.41171233 10.4191096,5.15465753 L14.5499315,5.15465753 C14.4576712,4.88041096 14.350274,4.61363014 14.2282877,4.35424658 L10.7371918,4.35424658 C10.8630137,4.08438356 11.0057534,3.82273973 11.1649315,3.5689726 L13.7987671,3.5689726 C13.6232192,3.28883562 13.4294521,3.02130137 13.2200685,2.76910959 L11.7515753,2.76910959 C11.9789041,2.50075342 12.2284932,2.24835616 12.4993836,2.01438356 C11.1743836,0.803972603 9.41246575,0.0656164384 7.47979452,0.0656164384 C7.4630137,0.0654109589 7.44712329,0.0654109589 7.43150685,0.0654109589 Z" id="Path" fill="'+c6+'"></path><path d="M24.9445205,7.54328767 C24.9452055,11.6713014 21.6073973,15.0186301 17.4865753,15.0198634 C13.3675342,15.0211644 10.0271233,11.6750685 10.02589,7.54636986 L10.02589,7.54328767 C10.024589,3.4140411 13.3634932,0.0664383562 17.4832877,0.0654104502 C21.6028082,0.0639041096 24.9434932,3.41020548 24.9445205,7.53917808 C24.9445205,7.53917808 24.9445205,7.5410274 24.9445205,7.54328767 Z" id="Path" fill="'+c7+'"></path><path d="M24.9445205,7.62582192 C24.9452055,11.7540411 21.6073973,15.1019178 17.4865753,15.1026717 C13.3675342,15.1041781 10.0271233,11.7576712 10.02589,7.62965753 L10.02589,7.62582192 C10.024589,3.49732877 13.3634932,0.149452055 17.4832877,0.148424149 C21.6028082,0.146917808 24.9434932,3.49321918 24.9445205,7.62191781 C24.9445205,7.6219863 24.9445205,7.62431507 24.9445205,7.62582192 Z" id="Path" fill="'+c8+'"></path><path d="M7.43150685,0.150205479 C3.33582192,0.176643836 0.0197260274,3.51527397 0.0197260274,7.62582192 C0.0197260274,11.7525342 3.36143836,15.1013699 7.47938356,15.1013699 C9.4110274,15.1013699 11.1727397,14.3630137 12.4982192,13.1533562 L12.4979452,13.1526027 L12.4989726,13.1526027 C12.7708904,12.905 13.0241096,12.6377397 13.2565753,12.3519863 L11.7286301,12.3519863 C11.5243836,12.1056849 11.3388356,11.8483562 11.1722603,11.5826027 L13.8076712,11.5826027 C13.9686301,11.3255479 14.1131507,11.0580137 14.2426027,10.7819863 L10.7376712,10.7819863 C10.6180137,10.5249315 10.5141781,10.2632877 10.4257534,9.9969863 L14.5545205,9.9969863 C14.8039041,9.25116438 14.9384247,8.45390411 14.9384247,7.62582192 C14.9384247,7.07609589 14.8786986,6.54068493 14.7667123,6.02479452 L10.2066438,6.02479452 C10.2635616,5.75931507 10.3345205,5.49657534 10.4191096,5.23979452 L14.5499315,5.23979452 C14.4576712,4.96534247 14.350274,4.69849315 14.2282877,4.43890411 L10.7371918,4.43890411 C10.8630137,4.17006849 11.0057534,3.90739726 11.1649315,3.65390411 L13.7987671,3.65390411 C13.6232192,3.3740411 13.4294521,3.10671233 13.2200685,2.85349315 L11.7515753,2.85349315 C11.9789041,2.58568493 12.2284932,2.33321918 12.4993836,2.09876712 C11.1743836,0.888561644 9.41246575,0.150205479 7.47979452,0.150205479 C7.4630137,0.150205479 7.44712329,0.150205479 7.43150685,0.150205479 Z" id="Path" fill="'+c9+'"></path><g id="Group" transform="translate(0.684932, 5.684932)" fill="'+a1+'"><path d="M9.47650685,3.8440411 L9.57623288,3.16767123 C9.52191781,3.16767123 9.44191781,3.19075342 9.37143836,3.19075342 C9.09513699,3.19075342 9.06438356,3.04363014 9.08232877,2.93472603 L9.30554795,1.55410959 L9.72561644,1.55410959 L9.82712329,0.805479452 L9.43116438,0.805479452 L9.51191781,0.340547945 L8.71746575,0.340547945 C8.70027397,0.357945205 8.24849315,2.9580137 8.24849315,3.2739726 C8.24849315,3.74273973 8.51068493,3.95157534 8.88123288,3.94852675 C9.17075342,3.94582192 9.39705479,3.86561644 9.47650685,3.8440411 Z" id="Path"></path><path d="M9.72842466,2.55417808 C9.72842466,3.6790411 10.4688356,3.94657534 11.1000685,3.94657534 C11.6828767,3.94657534 11.9389041,3.81561644 11.9389041,3.81561644 L12.0791096,3.04780822 C12.0791096,3.04780822 11.6360274,3.24465753 11.2356849,3.24465753 C10.3830137,3.24465753 10.5324658,2.60705479 10.5324658,2.60705479 L12.1467808,2.60705479 C12.1467808,2.60705479 12.2505479,2.09089041 12.2505479,1.88075342 C12.2505479,1.35664384 11.9896575,0.717465753 11.1180137,0.717465753 C10.3191781,0.717671233 9.72842466,1.57952055 9.72842466,2.55417808 Z M11.1213699,1.42986301 C11.5693836,1.42986301 11.4865753,1.93472603 11.4865753,1.97547945 L10.6049308,1.97547945 C10.6046575,1.92349315 10.6880137,1.42986301 11.1213699,1.42986301 Z" id="Shape"></path><path d="M16.1502055,3.8440411 L16.2924658,2.97547945 C16.2924658,2.97547945 15.9026712,3.17232877 15.6353425,3.17232877 C15.0720548,3.17232877 14.8462329,2.74020548 14.8462329,2.27787671 C14.8462329,1.33780822 15.3308904,0.820616438 15.8708904,0.820616438 C16.2753425,0.820616438 16.600274,1.04869863 16.600274,1.04869863 L16.729726,0.206027397 C16.729726,0.206027397 16.2481507,0.0102054795 15.8355479,0.0102054795 C14.9185616,0.0102054795 14.0261644,0.806506849 14.0261644,2.30472603 C14.0261644,3.29657534 14.5082192,3.95287684 15.4559589,3.95287684 C15.7236986,3.9530137 16.1502055,3.8440411 16.1502055,3.8440411 Z" id="Path"></path><path d="M5.10842466,0.729726027 C4.56383562,0.729726027 4.14630137,0.904794521 4.14630137,0.904794521 L4.03123288,1.59061644 C4.03123288,1.59061644 4.37616438,1.45068493 4.89671233,1.45068493 C5.19246575,1.45068493 5.40849315,1.48376712 5.40849315,1.72493151 C5.40849315,1.8710274 5.38212329,1.92506849 5.38212329,1.92506849 C5.38212329,1.92506849 5.14917808,1.90534247 5.0410274,1.90534247 C4.35445205,1.90534247 3.63273973,2.1990411 3.63273973,3.08527397 C3.63273973,3.78287671 4.10582192,3.94328767 4.39931507,3.94328767 C4.95979452,3.94328767 5.20068493,3.57910959 5.2140411,3.57753425 L5.18787671,3.8819863 L5.88705479,3.8819863 L6.19917808,1.68945205 C6.19917808,0.759657534 5.38952055,0.729726027 5.10842466,0.729726027 Z M5.27883562,2.5139726 C5.2939726,2.6480137 5.19479452,3.27589041 4.71657534,3.27589041 C4.47,3.27589041 4.40568493,3.08678082 4.40568493,2.975 C4.40568493,2.75739726 4.52383562,2.49575342 5.10609589,2.49575342 C5.24116438,2.49575342 5.25575342,2.51061644 5.27883562,2.5139726 Z" id="Shape"></path><path d="M6.94260274,3.92657534 C7.12123288,3.92657534 8.14383562,3.97246575 8.14383562,2.915 C8.14383562,1.92623288 7.19739726,2.12130137 7.19739726,1.7240411 C7.19739726,1.5269863 7.35171233,1.46390411 7.63356164,1.46390411 C7.74554795,1.46390411 8.17636986,1.50006849 8.17636986,1.50006849 L8.27684932,0.796849315 C8.27684932,0.796849315 7.9980137,0.734794521 7.54438356,0.734794521 C6.95623288,0.734794521 6.36034247,0.969041096 6.36034247,1.77349315 C6.36034247,2.68561644 7.35472603,2.59335616 7.35472603,2.97726027 C7.35472603,3.23356164 7.07664384,3.25458904 6.86239726,3.25458904 C6.49232877,3.25458904 6.15890411,3.12719178 6.15732877,3.13308219 L6.05150685,3.82835616 C6.07,3.83486301 6.2760274,3.92657534 6.94260274,3.92657534 Z" id="Path"></path><path d="M22.7287671,0.0989726027 L22.5578082,1.16410959 C22.5578082,1.16410959 22.2586986,0.750753425 21.7910274,0.750753425 C21.0629452,0.750753425 20.4565753,1.62979452 20.4565753,2.64034247 C20.4565753,3.29260274 20.779726,3.9310274 21.4415068,3.9310274 C21.9173973,3.9310274 22.1806164,3.59863014 22.1806164,3.59863014 L22.1460274,3.88232877 L22.9184932,3.88232877 L23.5256164,0.097260274 L22.7287671,0.0989726027 Z M22.3602055,2.17671233 C22.3602055,2.59678082 22.1521233,3.15773973 21.7217808,3.15773973 C21.4357534,3.15773973 21.3022603,2.91732877 21.3022603,2.54006849 C21.3022603,1.92342466 21.5785616,1.51541096 21.9270548,1.51541096 C22.2133562,1.51547945 22.3602055,1.71280822 22.3602055,2.17671233 Z" id="Shape"></path><polygon id="Path" points="0.807671233 3.88561644 1.29178082 1.0280137 1.36226027 3.88561644 1.90965753 3.88561644 2.93041096 1.0280137 2.47808219 3.88561644 3.2910274 3.88561644 3.91767123 0.0964383562 2.66013699 0.0964383562 1.87719178 2.4209589 1.83643836 0.0964383562 0.677739726 0.0964383562 0.0423972603 3.88561644"></polygon><path d="M13.0447945,3.8909589 C13.2759589,2.57390411 13.3187671,1.50390411 13.8708219,1.6989726 C13.9674658,1.18945205 14.0604795,0.99239726 14.1655479,0.776369863 C14.1655479,0.776369863 14.1163699,0.765890411 14.0122603,0.765890411 C13.6573288,0.765890411 13.3935616,1.25280822 13.3935616,1.25280822 L13.4637671,0.805821918 L12.7241096,0.805821918 L12.2292466,3.8909589 L13.0447945,3.8909589 Z" id="Path"></path><path d="M17.9826027,0.729726027 C17.4380137,0.729726027 17.0207534,0.904794521 17.0207534,0.904794521 L16.9059589,1.59061644 C16.9059589,1.59061644 17.250137,1.45068493 17.7711644,1.45068493 C18.0666438,1.45068493 18.2832192,1.48376712 18.2832192,1.72493151 C18.2832192,1.8710274 18.2565753,1.92506849 18.2565753,1.92506849 C18.2565753,1.92506849 18.0236301,1.90534247 17.9146575,1.90534247 C17.2278082,1.90534247 16.5069178,2.1990411 16.5069178,3.08527397 C16.5069178,3.78287671 16.980274,3.94328767 17.2734932,3.94328767 C17.8339726,3.94328767 18.075137,3.57910959 18.0879452,3.57753425 L18.0620548,3.8819863 L18.7617123,3.8819863 L19.0736302,1.68945205 C19.0739041,0.759657534 18.2645205,0.729726027 17.9826027,0.729726027 Z M18.1532877,2.5139726 C18.1684247,2.6480137 18.069726,3.27589041 17.5907534,3.27589041 C17.3444521,3.27589041 17.2806164,3.08678082 17.2806164,2.975 C17.2806164,2.75739726 17.3987671,2.49575342 17.98,2.49575342 C18.1158904,2.49575342 18.13,2.51061644 18.1532877,2.5139726 Z" id="Shape"></path><path d="M19.7122603,3.8909589 C19.9436986,2.57390411 19.9865068,1.50390411 20.5384932,1.6989726 C20.635137,1.18945205 20.7281507,0.99239726 20.8337671,0.776369863 C20.8337671,0.776369863 20.7837671,0.765890411 20.680274,0.765890411 C20.3243151,0.765890411 20.0613699,1.25280822 20.0613699,1.25280822 L20.1321233,0.805821918 L19.3919863,0.805821918 L18.8968493,3.89075342 L19.7123288,3.89075342 L19.7122603,3.8909589 L19.7122603,3.8909589 Z" id="Path"></path></g></g></g></g></g></svg>'
            }
            if (wallet == "visa"){
                var c1 = encodeURIComponent("#FCFCFC")
                var c2 = encodeURIComponent("#293688")
                var c3 = encodeURIComponent("#F7981D")
                return 'data:image/svg+xml;utf8,<svg width="40px" height="23px" viewBox="0 0 40 23" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Icons" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="icons" transform="translate(-20.000000, -153.000000)"><g id="visa-2" transform="translate(20.000000, 153.000000)"><g id="ic_visa" fill="'+c1+'"><path d="M3.8458278,-1.55376046e-16 L36.1541722,1.55376046e-16 C37.4914503,-9.02779573e-17 37.9763797,0.139238417 38.4652686,0.400699056 C38.9541574,0.662159695 39.3378403,1.04584256 39.5993009,1.53473144 C39.8607616,2.02362033 40,2.50854969 40,3.8458278 L40,19.1541722 C40,20.4914503 39.8607616,20.9763797 39.5993009,21.4652686 C39.3378403,21.9541574 38.9541574,22.3378403 38.4652686,22.5993009 C37.9763797,22.8607616 37.4914503,23 36.1541722,23 L3.8458278,23 C2.50854969,23 2.02362033,22.8607616 1.53473144,22.5993009 C1.04584256,22.3378403 0.662159695,21.9541574 0.400699056,21.4652686 C0.139238417,20.9763797 6.01853049e-17,20.4914503 -1.03584031e-16,19.1541722 L1.03584031e-16,3.8458278 C-6.01853049e-17,2.50854969 0.139238417,2.02362033 0.400699056,1.53473144 C0.662159695,1.04584256 1.04584256,0.662159695 1.53473144,0.400699056 C2.02362033,0.139238417 2.50854969,9.02779573e-17 3.8458278,-1.55376046e-16 Z" id="Rectangle"></path></g><g id="Group" transform="translate(7.659280, 6.000000)" fill="'+c2+'" fill-rule="nonzero"><polygon id="Path" points="8.10108033 9.09531856 9.60797784 0.197202216 12.0476177 0.197202216 10.5408033 9.09531856"></polygon><path d="M19.225928,0.427229917 C18.7424377,0.246897507 17.9849584,0.053767313 17.0390028,0.053767313 C14.627867,0.053767313 12.9295014,1.26091413 12.9148753,2.99036011 C12.9014958,4.26864266 14.1272576,4.98166205 15.0532687,5.40764543 C16.0028809,5.84293629 16.3222438,6.12224377 16.31759,6.51124654 C16.3116898,7.10767313 15.5588643,7.38033241 14.8573961,7.38033241 C13.8810249,7.38 13.3620499,7.24537396 12.5606925,6.9132964 L12.2463158,6.77152355 L11.9036842,8.7634903 C12.4736842,9.01196676 13.5277562,9.22745152 14.6219668,9.23833795 C17.1863435,9.23775623 18.8518006,8.04556787 18.8704986,6.19819945 C18.8798061,5.18609418 18.2296122,4.41581717 16.8219391,3.78058172 C15.9689751,3.36947368 15.4465928,3.09490305 15.4522438,2.67764543 C15.4522438,2.30792244 15.8946814,1.91243767 16.8496122,1.91243767 C17.6474792,1.8999723 18.2255402,2.07290859 18.6761219,2.253241 L18.8944321,2.35587258 L19.225928,0.427229917 L19.225928,0.427229917 Z" id="Path"></path><path d="M23.3854571,0.197202216 C22.8009141,0.197202216 22.3785873,0.375706371 22.1217175,0.95501385 L18.5060111,9.09565097 L21.0890859,9.09565097 L21.5912742,7.73210526 L24.6774515,7.73210526 L24.9641551,9.09565097 L27.2605263,9.09565097 L25.2515235,0.197451524 L23.3855402,0.197451524 L23.3854571,0.197202216 L23.3854571,0.197202216 Z M22.2766205,5.95163435 C22.478144,5.43922438 23.248338,3.46462604 23.248338,3.46462604 C23.2343767,3.48797784 23.4488643,2.94972299 23.5720222,2.61573407 L23.7371468,3.38252078 C23.7371468,3.38252078 24.2038504,5.5066205 24.3018283,5.95188366 L22.2766205,5.95188366 L22.2766205,5.95163435 Z" id="Shape"></path><path d="M3.65202216,6.296759 L3.43711911,5.07689751 C2.99185596,3.65493075 1.6099446,2.11637119 0.0614127424,1.34551247 L2.21692521,9.09531856 L4.80058172,9.09531856 L8.67531856,0.197202216 L6.09224377,0.197202216 L3.65202216,6.296759 Z" id="Path"></path></g><g id="Group" transform="translate(5.000000, 6.166205)" fill="'+c3+'" fill-rule="nonzero"><path d="M0.0683933518,0.0309972299 L0.0683933518,0.17468144 C3.12972299,0.910969529 5.25506925,2.77238227 6.09648199,4.91077562 L5.21185596,0.790387812 C5.06418283,0.227285319 4.6166759,0.0506094183 4.08714681,0.0310803324 L0.0683933518,0.0309972299 L0.0683933518,0.0309972299 Z" id="Path"></path></g></g></g></g></svg>'
            }

            if (wallet=="icon_right"){
                var c1 = encodeURIComponent("#FF0091")

                return 'data:image/svg+xml;utf8,<svg width="8px" height="14px" viewBox="0 0 8 14" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Icons" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linejoin="round"><g id="icons" transform="translate(-157.000000, -363.000000)" stroke="'+c1+'" stroke-width="1.2"><g id="chevron_" transform="translate(158.000000, 364.000000)"><polyline id="Shape-Copy" points="0 12 6 6 0 0"></polyline></g></g></g></svg>'
            }
            if (wallet == "search"){
                var c1 = encodeURIComponent("#8F8990")
                var c2 = encodeURIComponent("#2C0B2D")
                var c3 = encodeURIComponent("#8F8990")
                var c4 = encodeURIComponent("")
                var c5 = encodeURIComponent("")
                return 'data:image/svg+xml;utf8,<svg width="16px" height="16px" viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Icons" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" opacity="0.7"><g id="icons" transform="translate(-293.000000, -462.000000)" fill="'+c1+'"><g id="Search-icon-2" transform="translate(293.000000, 462.000000)"><path d="M10.5170951,3.45844772 C8.56750686,1.5088595 5.40803594,1.5088595 3.45844772,3.45844772 C1.5088595,5.40803594 1.5088595,8.56750686 3.45844772,10.5170951 C5.40803594,12.4666833 8.56750686,12.4666833 10.5170951,10.5170951 C12.4666833,8.56750686 12.4666833,5.40803594 10.5170951,3.45844772 M15.708011,15.708011 C15.317694,16.0973297 14.6857999,16.0973297 14.2954829,15.708011 L11.151984,12.5635139 C8.41477415,14.6099326 4.53356626,14.4162715 2.04691789,11.9286249 C-0.682305965,9.2003993 -0.682305965,4.77614175 2.04691789,2.04691789 C4.7751435,-0.682305965 9.2003993,-0.682305965 11.9286249,2.04691789 C14.4162715,4.53356626 14.6099326,8.41477415 12.5635139,11.151984 L15.708011,14.2964812 C16.0973297,14.6857999 16.0973297,15.317694 15.708011,15.708011" id="Search-icon"></path></g></g></g></svg>'
            }
            if (wallet == "radio_selected"){

                var c1 = encodeURIComponent("#EF002A")
                var c2 = encodeURIComponent("#AD00DF")
                var c3 = encodeURIComponent("#filter-2")
                var c4 = encodeURIComponent("#path-1")
                var c5 = encodeURIComponent("#ffffff")
                var c6 = encodeURIComponent("#linearGradient-3");
                return 'data:image/svg+xml;utf8,<svg width="34px" height="34px" viewBox="0 0 34 34" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><defs><rect id="path-1" x="0" y="0" width="16" height="16" rx="8"></rect><filter x="-84.4%" y="-84.4%" width="268.8%" height="268.8%" filterUnits="objectBoundingBox" id="filter-2"><feMorphology radius="1" operator="dilate" in="SourceAlpha" result="shadowSpreadOuter1"></feMorphology><feOffset dx="0" dy="0" in="shadowSpreadOuter1" result="shadowOffsetOuter1"></feOffset><feGaussianBlur stdDeviation="3.5" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur><feColorMatrix values="0 0 0 0 0.843137255   0 0 0 0 0   0 0 0 0 0.423529412  0 0 0 0.433848505 0" type="matrix" in="shadowBlurOuter1"></feColorMatrix></filter><linearGradient x1="50%" y1="0%" x2="50%" y2="100%" id="linearGradient-3"><stop stop-color="'+c1+'" offset="0%"></stop><stop stop-color="'+c2+'" offset="100%"></stop></linearGradient></defs><g id="Icons" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="icons" transform="translate(-9.000000, -350.000000)"><g id="Selected" transform="translate(18.000000, 359.000000)"><g id="Base"><use fill="black" fill-opacity="1" filter="url('+c3+')" xlink:href="'+c4+'"></use><use fill="'+c5+'" fill-rule="evenodd" xlink:href="'+c4+'"></use></g><polyline id="Path-2" stroke="url('+c6+')" stroke-width="1.6" stroke-linecap="round" stroke-linejoin="round" fill-rule="nonzero" points="4.8 8.48791866 6.96045874 10.4 11.2 6.4"></polyline></g></g></g></svg>'
            }



            if (wallet == "rupay"){

               return 'https://payments.juspay.in/web/images/rupay_zee'
            }

            if (wallet == "default_zee"){
                var c1= encodeURIComponent("#230924")
                return 'data:image/svg+xml;utf8,<svg width="63px" height="57px" viewBox="0 0 63 57" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect id="Rectangle" fill="'+c1+'" x="0" y="0" width="63" height="57"></rect></g></svg>'
            }
            if (wallet=="cvv_info"){
                var c1=encodeURIComponent("#E3E3E3")
                var c2 = encodeURIComponent("#000000")
                return 'data:image/svg+xml;utf8,<svg width="16px" height="16px" viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Icons" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="icons" transform="translate(-261.000000, -47.000000)" fill-rule="nonzero"><g id="ic_cvv" transform="translate(261.000000, 47.000000)"><circle id="Oval" fill="'+c1+'" cx="8" cy="8" r="8"></circle><circle id="Oval" fill="'+c2+'" cx="8.1" cy="12.279" r="1"></circle><path d="M6.2752124,5.56737579 C6.27988636,5.54232861 6.29627645,5.48195387 6.32830696,5.3986773 C6.38378544,5.25443806 6.46240685,5.10965951 6.56705923,4.97560537 C6.86784958,4.59030896 7.32123149,4.36164365 7.99804248,4.36495586 C8.67450964,4.36829547 9.18419675,4.75355907 9.41742063,5.36140943 C9.63727421,5.93441279 9.54825545,6.56529084 9.19884376,6.92048989 C9.00576618,7.11668162 8.8161999,7.30681394 8.60667846,7.51501395 C8.49571081,7.62528173 8.17457554,7.94299861 8.14124723,7.97607153 C7.79536317,8.31509485 7.59963478,8.78151144 7.60000011,9.2683002 L7.6,10.082 C7.6,10.3029139 7.7790861,10.482 8,10.482 C8.2209139,10.482 8.4,10.3029139 8.4,10.082 L8.4,9.268 C8.39979603,8.99606629 8.50901463,8.73580068 8.70299989,8.54565725 C8.7374322,8.51149946 9.05883088,8.19352197 9.17057154,8.08248605 C9.38206414,7.87232733 9.57367259,7.68014673 9.76909633,7.48157099 C10.3541241,6.88685333 10.4900282,5.92369802 10.1643294,5.07483074 C9.81747389,4.17082287 9.02261682,3.57000427 8.00197471,3.56492122 C7.07265666,3.56041758 6.38696238,3.90625073 5.93646313,4.48331628 C5.77999155,4.68374803 5.66399738,4.89734722 5.58163368,5.11148571 C5.53095889,5.24323579 5.50194709,5.350104 5.4887876,5.42062421 C5.44826327,5.63778943 5.591459,5.84668808 5.80862421,5.8872124 C6.02578943,5.92773673 6.23468808,5.784541 6.2752124,5.56737579 Z" id="Path" fill="'+c2+'"></path></g></g></g></svg>'
            }
            if (wallet == "checkbox"){
                var c1=encodeURIComponent("#FF0091")
                var c2 = encodeURIComponent("#path-1")
                var c3 = encodeURIComponent("#mask-2")
                return 'data:image/svg+xml;utf8,<svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><defs><polygon id="path-1" points="0.00016 0 19.2 0 19.2 19.2 0.00016 19.2"></polygon></defs><g id="Icons" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="icons" transform="translate(-105.000000, -359.000000)"><g id="Checkbox-Copy" transform="translate(105.000000, 359.000000)"><g id="Group-3"><mask id="mask-2" fill="white"><use xlink:href="'+c2+'"></use></mask><g id="Clip-2"></g><path d="M2.47536,1.6 C1.99296,1.6024 1.60256,1.9928 1.60016,2.4752 L1.60016,16.7248 C1.60256,17.2072 1.99296,17.5976 2.47536,17.6 L16.72496,17.6 C17.20736,17.5976 17.59776,17.2072 17.60016,16.7248 L17.60016,2.4752 C17.59776,1.9928 17.20736,1.6024 16.72496,1.6 L2.47536,1.6 Z M16.72496,19.2 L2.47536,19.2 C1.10896,19.1976 0.00256,18.0904 0.00016,16.7248 L0.00016,2.4752 C0.00256,1.1096 1.10896,0.0024 2.47536,0 L16.72496,0 C18.09056,0.0024 19.19776,1.1096 19.20016,2.4752 L19.20016,16.7248 C19.19776,18.0904 18.09056,19.1976 16.72496,19.2 L16.72496,19.2 Z" id="Fill-1" fill="'+c1+'" mask="url('+c3+')"></path></g><path d="M8.12295171,13.2923077 C7.92726115,13.2923077 7.73895514,13.1992639 7.60086406,13.0326711 L4.64704431,9.48814433 C4.35830843,9.14166684 4.35904688,8.58074548 4.64704431,8.23426799 C4.93578019,7.88867663 5.40322217,7.88867663 5.69195805,8.23515412 L8.12295171,11.1522997 L13.5077651,4.69062735 C13.796501,4.34414986 14.263943,4.34414986 14.5526789,4.69062735 C14.8414147,5.03710484 14.8414147,5.5980262 14.5526789,5.9445037 L8.64503935,13.0335572 C8.50694827,13.20015 8.31864227,13.2923077 8.12295171,13.2923077" id="Fill-4" fill="'+c1+'"></path></g></g></g></svg>'
            }
            if (wallet == "airtel"){
                var c = encodeURIComponent('#D32A31');
                return 'data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" width="26" height="26" viewBox="0 0 26 26"> <path fill="' + c + '" fill-rule="evenodd" d="M15.794 1.038c1.764-.2 3.816.387 4.714 1.957.931 1.694.426 3.752-.564 5.315-1.068 1.857-2.851 3.207-4.66 4.37-1.076.65-2.256 1.332-3.567 1.32-1.017-.025-1.692-1.1-1.397-1.995.295-.95.8-1.938 1.711-2.488.577-.32 1.416-.432 1.94.025.27.225.237.606.04.856-.636.888-1.652 1.463-2.19 2.42-.098.194-.196.482.007.65.256.163.584.057.846-.025 1.173-.412 2.176-1.144 3.14-1.882 1.469-1.144 2.878-2.588 3.226-4.433.137-.8-.079-1.857-.958-2.189-.983-.25-1.96.226-2.792.682-1.246.713-2.498 1.42-3.639 2.282-.813.582-1.665 1.25-2.708 1.35-1.068.082-2.11-.9-1.92-1.937.111-.776.577-1.451 1.068-2.058 1.128-1.375 2.675-2.376 4.288-3.157 1.075-.507 2.21-.95 3.415-1.063zM7.786 16.036c.71-.209 1.398.532 1.17 1.242-.161.725-1.193.972-1.682.424-.535-.509-.245-1.581.512-1.666zm6.22 2.153c.393-.067.792-.134 1.19-.189-.017.5.006 1.005 0 1.505.253.006.51.006.763.012-.012.317-.012.634 0 .95-.258 0-.51 0-.763.007-.011.945 0 1.889-.011 2.827.011.22-.006.464.117.652.182.183.458.177.698.226-.012.274-.012.548 0 .816-.551.019-1.167 0-1.601-.42-.3-.262-.399-.695-.399-1.09.006-1.768.006-3.529.006-5.296zm8 .017c.387-.067.774-.14 1.167-.206.018 1.88-.012 3.765.012 5.644-.012.443.488.497.809.521-.006.273-.006.552.006.83-.56.019-1.184-.005-1.613-.436-.315-.285-.393-.733-.387-1.145.006-1.734-.006-3.474.006-5.208zm-18.585 1.38c.585-.366 1.232-.626 1.908-.58.75-.02 1.527.606 1.63 1.486.01 1.154.09 2.32-.006 3.468-.534.737-1.425 1.076-2.249 1.037-.97.006-1.84-1.108-1.686-2.223.073-.737.567-1.396 1.203-1.559.546-.136 1.113-.071 1.67-.084 0-.392-.085-.835-.437-.991-.58-.248-1.181.084-1.71.358-.107-.306-.215-.606-.323-.913zm6.762.394c.724-.83 1.83-1.147 2.817-.896-.137.382-.29.75-.445 1.12-.484-.06-.986-.014-1.414.283.006 1.502.012 3.01 0 4.513-.376-.007-.752-.007-1.129 0-.017-1.45.006-2.892-.011-4.342-.006-.237.011-.507.182-.678zm7.466-.276c.588-.833 1.738-.892 2.496-.36.747.623.832 1.797.855 2.781-.945-.006-1.89-.013-2.842 0 .029.663.193 1.424.747 1.739.584.295 1.206-.072 1.75-.328.107.282.22.558.34.827-.923.669-2.27.977-3.16.085-1.092-1.22-1.07-3.385-.186-4.744zm-9.643-.507c.326-.061.653-.13.984-.197.015 1.995-.005 3.998.01 5.993-.331 0-.668 0-.999.007 0-1.934-.005-3.869.005-5.803zm10.367 1.034c.331-.303.986-.315 1.296.006.211.215.254.5.331.763h-2c.092-.262.14-.565.373-.77zM4.316 22.217c.494-.336 1.12-.172 1.675-.207-.027.536.055 1.088-.034 1.625-.323.379-.913.45-1.353.272-.625-.265-.838-1.246-.288-1.69z"/> </svg>'
            }
            if (wallet == "phonpe"){
                var c1 = encodeURIComponent('#fff');
                var c2 = encodeURIComponent('#7437BC');
                return 'data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40"> <g fill="none" fill-rule="evenodd"> <path fill="'+ c1 +'" d="M3.846 0h32.308c1.337 0 1.822.14 2.311.4.49.262.873.646 1.134 1.135.262.489.401.974.401 2.31v32.31c0 1.336-.14 1.821-.4 2.31-.262.49-.646.873-1.135 1.134-.489.262-.974.401-2.31.401H3.844c-1.336 0-1.821-.14-2.31-.4-.49-.262-.873-.646-1.134-1.135-.262-.489-.401-.974-.401-2.31V3.844c0-1.336.14-1.821.4-2.31.262-.49.646-.873 1.135-1.134C2.024.139 2.509 0 3.845 0z"/> <path fill="'+ c2 +'" d="M25.487 16.34c0-.115-.03-.2-.09-.258-.061-.058-.152-.086-.272-.086h-2.403c-.096 0-.185-.027-.267-.08-.084-.054-.159-.135-.23-.244L19.5 11.287c-.051-.095-.117-.167-.2-.214-.083-.047-.181-.071-.296-.071H17.84c-.094 0-.167.024-.213.07-.048.049-.072.12-.072.215 0 .05.007.1.018.148.014.047.032.094.058.138l.534.791.534.792c.353.525.72 1.023 1.1 1.497.38.473.773.92 1.18 1.344h-5.616c-.121 0-.21.028-.27.085-.062.056-.092.142-.092.256v.8c0 .128.03.223.09.287.061.063.15.096.272.096h1.105c.057.005.1.028.129.066.029.039.042.093.042.163v4.157c0 .445.066.843.199 1.195.132.35.33.655.593.912.264.257.572.45.924.579.35.13.746.194 1.184.194h.552c.432 0 .796-.066 1.087-.2.293-.133.516-.333.668-.6h.077v3.527c0 .12.03.212.09.272.06.06.152.09.273.09h1.01c.12 0 .21-.03.272-.09.06-.06.09-.152.09-.272V17.75c0-.076.017-.133.052-.172.035-.038.088-.057.157-.057h1.26c.12 0 .21-.03.27-.09.062-.062.091-.151.091-.272v-.82zM32 20c0 6.628-5.373 12-12 12-6.628 0-12-5.372-12-12 0-6.627 5.372-12 12-12 6.627 0 12 5.373 12 12zm-10.08 1.697h.002c0 .235-.033.445-.099.632-.065.185-.163.346-.293.483-.13.137-.295.24-.493.309-.2.069-.432.101-.7.101h-.513c-.484 0-.845-.124-1.087-.372s-.363-.619-.363-1.115V17.75c0-.076.018-.133.052-.172.035-.038.088-.056.158-.056h3.126c.07 0 .123.018.158.056.035.039.053.096.053.172v3.947z"/> </g> </svg>'
            }
            if (wallet == "kotak"){
                var c1 = encodeURIComponent('#fff')
                var c2 = encodeURIComponent('#003874');
                var c3 = encodeURIComponent('#ED1C24');
                return 'data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40"> <g fill="none" fill-rule="evenodd"> <path fill="'+ c1 +'" d="M3.846 0h32.308c1.337 0 1.822.14 2.311.4.49.262.873.646 1.134 1.135.262.489.401.974.401 2.31v32.31c0 1.336-.14 1.821-.4 2.31-.262.49-.646.873-1.135 1.134-.489.262-.974.401-2.31.401H3.844c-1.336 0-1.821-.14-2.31-.4-.49-.262-.873-.646-1.134-1.135-.262-.489-.401-.974-.401-2.31V3.844c0-1.336.14-1.821.4-2.31.262-.49.646-.873 1.135-1.134C2.024.139 2.509 0 3.845 0z"/> <g> <path fill="'+ c2 +'" d="M12 0c6.628 0 12 4.478 12 10 0 5.523-5.372 10-12 10S0 15.523 0 10C0 4.478 5.372 0 12 0z" transform="translate(8 10)"/> <path fill="'+ c3 +'" d="M5.863 9.011L5 11.986 19.118 12 20 9 5.863 9.011" transform="translate(8 10) matrix(0 1 1 0 2 -2)"/> <path fill="'+ c1 +'" d="M20 10.576c-.218 2.199-1.228 4.42-3.432 4.424-1.287.003-2.292-1.074-2.988-2.39v-1.526c.87.528 1.685 1.095 2.673 1.113 1.217.022 2.317-.55 2.772-1.62H20zm-7.368 1.38c-1.011 1.49-1.989 3.01-3.82 3.01C6.228 14.966 5 11.912 5 9.358 5 6.904 5.959 4 8.478 4c1.093 0 2.145.816 2.772 1.68l-.002 2.083c-.524-.43-1.697-.854-2.463-.869-1.598-.033-3.006.813-2.982 2.74.017 1.326 1.092 2.228 2.163 2.228 1.639 0 2.632-1.83 3.461-3.218.232-.363.885-1.43.987-1.6.922-1.574 1.988-3.01 3.82-3.01 2.156 0 3.367 2.123 3.71 4.31h-.973c-.391-.71-1.145-1.143-1.887-1.143-1.694 0-2.707 1.904-3.554 3.307 0 0-.66 1.097-.898 1.448z" transform="translate(8 10)"/> </g> </g> </svg>'
            }
            if (wallet == "hdfc"){
                var c1 = encodeURIComponent('#fff');
                var c2 = encodeURIComponent('#ED232A');
                var c3 = encodeURIComponent('#004C8F');
                return 'data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40"> <g fill="none" fill-rule="evenodd"> <path fill="'+ c1 +'" d="M3.846 0h32.308c1.337 0 1.822.14 2.311.4.49.262.873.646 1.134 1.135.262.489.401.974.401 2.31v32.31c0 1.336-.14 1.821-.4 2.31-.262.49-.646.873-1.135 1.134-.489.262-.974.401-2.31.401H3.844c-1.336 0-1.821-.14-2.31-.4-.49-.262-.873-.646-1.134-1.135-.262-.489-.401-.974-.401-2.31V3.844c0-1.336.14-1.821.4-2.31.262-.49.646-.873 1.135-1.134C2.024.139 2.509 0 3.845 0z"/> <g fill-rule="nonzero"> <path fill="'+ c2 +'" d="M0 13.197l4.2-.001v6.603h6.6V24H0V13.197zm24 0V24H13.2l-.001-4.201h6.603v-6.603H24zM24 0v10.797h-4.198v-6.6H13.2V0H24zM10.8 4.197H4.2v6.6H0V0h10.8v4.197z" transform="translate(8 8)"/> <path fill="'+ c3 + '" d="M8 15L15 15 15 8 8 8z" transform="translate(8 8)"/> </g> </g> </svg>'
            }
            if (wallet == "axis"){
                var c = encodeURIComponent('#AB1B5C');
                return 'data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" width="26" height="26" viewBox="0 0 26 26"> <path fill="' + c + '" fill-rule="evenodd" d="M20.465 15.9L25 23.804h-7.465L13.003 15.9h7.462zM12.975 3l3.745 6.439-8.27 14.365H1L12.975 3z"/> </svg>'
            }

        }
        else {
            if (wallet=="bhim"){
                return "https://payments.juspay.in/web/images/bhim"
            }
            if (wallet=="upi_home"){

                return "https://payments.juspay.in/web/images/bhim"
            }
        }
    }
}

exports["getLocalIcon"] = getLocalIcon;

//#F3B598
var getUPIHomeQRIcon = function(color){
    return function (color2){
        return 'data:image/svg+xml;utf8,'+encodeURIComponent('<svg width="142" height="147" viewBox="0 0 142 147" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M99.6086 12.0445C116.106 14.3812 136.805 23.2577 140.563 36.3332C144.324 49.4428 131.152 66.7192 118.613 80.5416C106.078 94.396 94.1882 104.798 79.621 111.807C65.0557 118.785 47.8453 122.419 36.2616 115.837C24.7099 109.302 18.8249 92.5678 11.9708 77.8304C5.09467 63.0609 -2.76465 50.2557 0.957765 41.2791C4.68818 32.3365 20.0003 27.2204 31.518 24.0026C43.0356 20.7848 50.7247 19.4292 60.7526 16.686C70.7464 13.9067 83.077 9.70769 99.6086 12.0445Z" fill="#F2F2F2" fill-opacity="0.65"/><g filter="url(#filter0_d)"><path d="M47.1084 15.9951C44.4633 15.9951 43.6363 20.128 43.6363 21.78C43.6363 24.5833 46.6118 36.161 49.4212 36.161C52.2305 36.161 55.208 24.7475 55.208 23.4339C55.208 22.1204 51.7359 15.9951 47.1084 15.9951Z" fill="'+color2+'"/></g><g filter="url(#filter1_d)"><path d="M99.0577 16.7782L59.2958 8.97724C56.5448 8.43753 53.8772 10.2301 53.3375 12.9811L38.1645 90.3181C37.6248 93.069 39.4174 95.7366 42.1683 96.2763L81.9303 104.077C84.6813 104.617 87.3489 102.824 87.8886 100.074L103.062 22.7365C103.601 19.9856 101.809 17.3179 99.0577 16.7782Z" fill="'+color+'"/></g><path d="M89.9472 15.01L89.4446 17.571C89.3202 18.1976 88.9526 18.7493 88.4223 19.1055C87.892 19.4616 87.2422 19.5932 86.6152 19.4713L69.4709 16.1073C68.8446 15.9824 68.2932 15.6147 67.9371 15.0846C67.5811 14.5545 67.4493 13.9049 67.5706 13.278L68.0832 10.7169" fill="#D8D8D8"/><path d="M39.0027 87.8846L89.0027 96.8846" stroke="#D8D8D8" stroke-width="3" stroke-miterlimit="10"/><path d="M62.2103 97.8862C63.3195 97.8862 64.2187 96.987 64.2187 95.8778C64.2187 94.7686 63.3195 93.8694 62.2103 93.8694C61.1011 93.8694 60.202 94.7686 60.202 95.8778C60.202 96.987 61.1011 97.8862 62.2103 97.8862Z" fill="#D8D8D8"/><g filter="url(#filter2_d)"><path d="M43.9687 38.3196C40.4966 38.3196 38.1818 40.9627 38.1818 43.774C38.1818 53.029 43.4721 60.632 45.7908 60.632C50.7487 60.632 52.5669 57.9869 52.5669 56.1687C52.5628 54.8371 51.9021 38.3196 43.9687 38.3196Z" fill="'+color2+'"/></g><path d="M42.1506 62.6064C43.1418 63.5976 50.7448 71.6972 50.7448 77.3158C50.7448 81.1203 49.9178 84.0919 42.9756 84.0919C39.6696 84.0919 30.4147 71.0344 30.4147 65.7461C30.4147 62.7706 32.2328 55.8284 37.3569 55.8284C38.8447 55.8284 42.1506 58.4735 42.6452 59.1343C43.1545 59.6618 43.6948 60.1586 44.2631 60.622C44.0548 61.6973 42.9055 67.354 42.9055 67.354" fill="'+color2+'"/><g filter="url(#filter3_d)"></g><g filter="url(#filter4_d)"><path d="M40.9151 95.8578C40.4205 97.1814 40.0841 109.961 41.5719 117.067C40.5707 122.191 36.612 138.893 36.612 138.893H72.4885L76.1248 124.348C76.1248 124.348 98.263 104.999 100.412 101.531C102.56 98.0625 107.602 77.8125 107.53 75.9122C107.472 74.5906 83.3974 54.2585 82.7366 53.4316C82.0759 52.6046 70.4501 64.2444 89.789 77.6402C89.6228 79.2922 89.6328 86.5889 89.6328 86.5889C89.6328 86.5889 76.1809 91.2123 74.8153 92.2095C73.4496 93.2067 70.8365 100.233 70.364 101.809C68.5358 101.452 40.9151 95.8578 40.9151 95.8578Z" fill="'+color2+'"/></g><defs><filter id="filter0_d" x="35.6363" y="7.99512" width="27.5717" height="36.1659" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB"><feFlood flood-opacity="0" result="BackgroundImageFix"/><feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/><feOffset/><feGaussianBlur stdDeviation="4"/><feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.15 0"/><feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/><feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/></filter><filter id="filter1_d" x="29.1873" y="0" width="82.8515" height="113.055" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB"><feFlood flood-opacity="0" result="BackgroundImageFix"/><feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/><feOffset/><feGaussianBlur stdDeviation="4"/><feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.15 0"/><feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/><feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/></filter><filter id="filter2_d" x="30.1818" y="30.3196" width="30.385" height="38.3124" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB"><feFlood flood-opacity="0" result="BackgroundImageFix"/><feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/><feOffset/><feGaussianBlur stdDeviation="4"/><feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.15 0"/><feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/><feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/></filter><filter id="filter3_d" x="22.4147" y="47.8284" width="36.3301" height="44.2635" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB"><feFlood flood-opacity="0" result="BackgroundImageFix"/><feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/><feOffset/><feGaussianBlur stdDeviation="4"/><feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.15 0"/><feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/><feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/></filter><filter id="filter4_d" x="28.612" y="45.39" width="86.9188" height="101.503" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB"><feFlood flood-opacity="0" result="BackgroundImageFix"/><feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/><feOffset/><feGaussianBlur stdDeviation="4"/><feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.15 0"/><feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/><feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/></filter></defs></svg>')

    }
}
exports["getUPIHomeQRIcon"]= getUPIHomeQRIcon

var getCheckBoxForSavedCard = function (color) {
    color = encodeURIComponent(color);
    return '<?xml version="1.0" encoding="UTF-8"?> <svg width="16px" height="16px" viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> <!-- Generator: Sketch 61.1 (89650) - https://sketch.com --> <title>checkbox</title> <desc>Created with Sketch.</desc> <defs> <rect id="path-1" x="0" y="0" width="328" height="230" rx="10"></rect> <filter x="-2.9%" y="-3.7%" width="105.8%" height="108.3%" filterUnits="objectBoundingBox" id="filter-2"> <feOffset dx="0" dy="1" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset> <feGaussianBlur stdDeviation="3" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur> <feColorMatrix values="0 0 0 0 0.125490196   0 0 0 0 0.129411765   0 0 0 0 0.141176471  0 0 0 0.15 0" type="matrix" in="shadowBlurOuter1"></feColorMatrix> </filter> </defs> <g id="Mobile-Web" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"> <g id="300_Add-Card" transform="translate(-40.000000, -381.000000)"> <g id="card-entry" transform="translate(16.000000, 191.000000)"> <g id="Rectangle-Copy-29"> <use fill="black" fill-opacity="1" filter="url(#filter-2)" xlink:href="#path-1"></use> <use fill="#FFFFFF" fill-rule="evenodd" xlink:href="#path-1"></use> </g> <g id="checker" transform="translate(24.000000, 190.000000)"> <g id="checkbox"> <rect id="Checkbox" fill="#1BB934" x="0" y="0" width="16" height="16" rx="2"></rect> <polyline id="Path-2" stroke="#FFFFFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" points="4 8.8 6.4 11.2 12 5.6"></polyline> </g> </g> </g> </g> </g> </svg> '
}
exports['getCheckBoxForSavedCard'] = getCheckBoxForSavedCard;

var getUpi1 = function (color) {
    return function (sat) {
        return function (secColor) {


            var c1 = "hsl(" + color + ", " + sat + "%, 95.3%)";
            var c2 = "hsl(" + color + ", " + sat + "%, 22.4%)";
            var c3 = encodeURIComponent(secColor);
            return 'data:image/svg+xml;utf8, <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> <defs> <rect id="path-1" x="0" y="0" width="328" height="314" rx="10"></rect> <filter x="-2.9%" y="-2.7%" width="105.8%" height="106.1%" filterUnits="objectBoundingBox" id="filter-2"> <feOffset dx="0" dy="1" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset> <feGaussianBlur stdDeviation="3" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur> <feColorMatrix values="0 0 0 0 0.125490196   0 0 0 0 0.129411765   0 0 0 0 0.141176471  0 0 0 0.15 0" type="matrix" in="shadowBlurOuter1"></feColorMatrix> </filter> </defs> <g id="Mobile-Web" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"> <g id="403_UPI-Collect-Pending" transform="translate(-40.000000, -298.000000)"> <g id="collect-card" transform="translate(16.000000, 191.000000)"> <g id="Rectangle-Copy-29"> <use fill="black" fill-opacity="1" ></use> <use fill="' + c1 + '" fill-rule="evenodd"></use> </g> <g id="steps" transform="translate(24.000000, 107.000000)"> <g id="line"> <g id="counter"> <circle id="Oval-3" stroke="' + c1 + '" fill-opacity="0.1" fill="' + c3 + '" cx="12" cy="12" r="11.5"></circle> <text id="1" font-family="Arial-BoldMT, Arial" font-size="13" font-weight="bold" fill="' + c2 + '"> <tspan x="8.38500977" y="17">1</tspan> </text> </g> </g> </g> </g> </g> </g> </svg>'

        }
    }
}

exports['getUpi1'] = getUpi1;

var getUpi2 = function (color) {
    return function (sat) {
        return function (secColor) {


            var c1 = "hsl(" + color + ", " + sat + "%, 95.3%)";
            var c2 = "hsl(" + color + ", " + sat + "%, 22.4%)";
            var c3 = encodeURIComponent(secColor);
            return 'data:image/svg+xml;utf8, <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> <!-- Generator: Sketch 61.1 (89650) - https://sketch.com --> <title>counter</title> <desc>Created with Sketch.</desc> <defs> <rect id="path-1" x="0" y="0" width="328" height="314" rx="10"></rect> <filter x="-2.9%" y="-2.7%" width="105.8%" height="106.1%" filterUnits="objectBoundingBox" id="filter-2"> <feOffset dx="0" dy="1" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset> <feGaussianBlur stdDeviation="3" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur> <feColorMatrix values="0 0 0 0 0.125490196   0 0 0 0 0.129411765   0 0 0 0 0.141176471  0 0 0 0.15 0" type="matrix" in="shadowBlurOuter1"></feColorMatrix> </filter> </defs> <g id="Mobile-Web" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"> <g id="403_UPI-Collect-Pending" transform="translate(-40.000000, -338.000000)"> <g id="collect-card" transform="translate(16.000000, 191.000000)"> <g id="Rectangle-Copy-29"> <use fill="black" fill-opacity="1" ></use> <use fill="' + c1 + '" fill-rule="evenodd" ></use> </g> <g id="steps" transform="translate(24.000000, 107.000000)"> <g id="line-copy" transform="translate(0.000000, 40.000000)"> <g id="counter"> <circle id="Oval-3" stroke="' + c1 + '" fill="' + c3 + '" opacity="0.1" cx="12" cy="12" r="11.5"></circle> <text id="2" font-family="Arial-BoldMT, Arial" font-size="13" font-weight="bold" fill="' + c2 + '"> <tspan x="8.38500977" y="17">2</tspan> </text> </g> </g> </g> </g> </g> </g> </svg> '
        }
    }
}
exports['getUpi2'] = getUpi2;

var getUpi3 = function (color) {
    return function (sat) {
        return function (secColor) {


            var c1 = "hsl(" + color + ", " + sat + "%, 95.3%)";
            var c2 = "hsl(" + color + ", " + sat + "%, 22.4%)";
            var c3 = encodeURIComponent(secColor);
            return 'data:image/svg+xml;utf8, <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> <!-- Generator: Sketch 61.1 (89650) - https://sketch.com --> <title>counter</title> <desc>Created with Sketch.</desc> <defs> <rect id="path-1" x="0" y="0" width="328" height="314" rx="10"></rect> <filter x="-2.9%" y="-2.7%" width="105.8%" height="106.1%" filterUnits="objectBoundingBox" id="filter-2"> <feOffset dx="0" dy="1" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset> <feGaussianBlur stdDeviation="3" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur> <feColorMatrix values="0 0 0 0 0.125490196   0 0 0 0 0.129411765   0 0 0 0 0.141176471  0 0 0 0.15 0" type="matrix" in="shadowBlurOuter1"></feColorMatrix> </filter> </defs> <g id="Mobile-Web" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"> <g id="403_UPI-Collect-Pending" transform="translate(-40.000000, -374.000000)"> <g id="collect-card" transform="translate(16.000000, 191.000000)"> <g id="Rectangle-Copy-29"> <use fill="black" fill-opacity="1" ></use> <use fill="' + c1 + '" fill-rule="evenodd" ></use> </g> <g id="steps" transform="translate(24.000000, 107.000000)"> <g id="line-copy-2" transform="translate(0.000000, 76.000000)"> <g id="counter"> <circle id="Oval-3" stroke="' + c1 + '" fill="' + c3 + '" opacity="0.100000001" cx="12" cy="12" r="11.5"></circle> <text id="3" font-family="Arial-BoldMT, Arial" font-size="13" font-weight="bold" fill="' + c2 + '"> <tspan x="8.38500977" y="17">3</tspan> </text> </g> </g> </g> </g> </g> </g> </svg> '
        }
    }
}
exports['getUpi3'] = getUpi3;


var getAddCardIcon = function (color) {
    var c1 = encodeURIComponent(c1);
    var c2 = encodeURIComponent("#F3F3F3")
    var c3 = encodeURIComponent("#6B6B6B");
    var c4 = encodeURIComponent("#3E4154");
    return 'data:image/svg+xml;utf8, <svg width="43px" height="32px" viewBox="0 0 43 32" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Desktop-Web" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="Add-Card-1" transform="translate(-1036.000000, -419.000000)"><rect id="bg" fill="' + c2 + '" x="0" y="182" width="1440" height="842" rx="4"></rect><g id="add-card-component" transform="translate(589.000000, 283.000000)"><rect id="Rectangle-Copy-29" fill="' + c1 + '" x="0" y="0" width="587" height="483"></rect><g id="add-card" transform="translate(24.000000, 112.000000)" opacity="0.5"><g id="Group-5" transform="translate(423.000000, 24.000000)"><path d="M4.13408105,32 L38.865919,32 C41.1820364,32 43,30.1467701 43,27.7903186 L43,4.21165686 C43,1.85318439 41.1800517,0.00197549858 38.865919,0.00197549858 L4.13408105,0 C1.81796363,0 0,1.85322987 0,4.20968136 L0,27.7883431 C0,30.1468156 1.81801325,32 4.13408105,32 L4.13408105,32 Z M38.865919,30.3146117 L4.13408105,30.3146117 C2.72893012,30.3146117 1.65323548,29.2191952 1.65323548,27.7883482 L1.65323548,14.3165594 L41.3467645,14.3165594 L41.3467645,27.7903692 C41.3467645,29.2212162 40.2710699,30.3146117 38.865919,30.3146117 L38.865919,30.3146117 Z M4.13408105,1.68548932 L38.865919,1.68548932 C40.2710699,1.68548932 41.3467645,2.78086034 41.3467645,4.21170739 L41.3467645,7.58066506 L1.65323548,7.57868956 L1.65323548,4.20973189 C1.65323548,2.77888484 2.72893012,1.68548932 4.13408105,1.68548932 L4.13408105,1.68548932 Z" id="Fill-1" fill="' + c3 + '"></path><path d="M9.79961603,18 L8.20038397,18 C6.43988481,18 5,19.4398848 5,21.200384 L5,22.799616 C5,24.5601152 6.43988481,26 8.20038397,26 L9.79961603,26 C11.5601152,26 13,24.5601152 13,22.799616 L13,21.200384 C12.9981234,19.4398848 11.5582433,18 9.79961603,18 Z" id="Fill-2" fill="' + c4 + '"></path><polygon id="Fill-3" fill="' + c4 + '" points="15 21 37 21 37 19 15 19"></polygon><polygon id="Fill-4" fill="' + c4 + '" points="15 25 33 25 33 23 15 23"></polygon></g></g></g></g></g></svg>'
}

exports['getAddCardIcon'] = getAddCardIcon;

var getFreechargeIcon = function (color) {
    color = encodeURIComponent(color);
    return ''
}
exports['getFreechargeIcon'] = getFreechargeIcon;
