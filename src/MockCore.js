// var callbackMapper = require("presto-ui").helpers.android.callbackMapper;


// exports["showPanel"] = function(screenInput) {
//   return function(configData) {
//     return function(fn) {
//       return function() {
//         console.log("screenInput", screenInput);
//         console.log("configData", configData);

//         var cb = callbackMapper.map(function(data) {
//           var newConfig = JSON.parse(data);

//           screenInput[configData.selectedComponent] = newConfig;

//           console.log(screenInput);

//           fn(screenInput)();
//         });

//         JBridge.showPanel(JSON.stringify(configData), cb);

//         return;
//       }
//     }
//   }
// }

exports["getDevelopmentData"] = function(){
  var response =
  {
    "wallets": [],
    "vpas": [],
    "offers": [],
    "nbMethods": [],
    "merchantPaymentMethods": [
      {
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_AXIS",
        "description": "Axis Bank"
      },
      {
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_BOM",
        "description": "Bank of Maharashtra"
      },
      {
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_CBI",
        "description": "Central Bank Of India"
      },
      {
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_CORP",
        "description": "Corporation Bank"
      },
      {
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_DCB",
        "description": "Development Credit Bank"
      },
      {
        "supportedReferenceIds": [
          "vodafone",
          "prepaid_app",
          "postpaid_web",
          "postpaid_app",
          "prepaid_web"
        ],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_HDFC",
        "description": "HDFC Bank"
      },
      {
        "supportedReferenceIds": [
          "vodafone",
          "prepaid_app",
          "postpaid_web",
          "postpaid_app",
          "prepaid_web"
        ],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_ICICI",
        "description": "ICICI Netbanking"
      },
      {
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_IDBI",
        "description": "Industrial Development Bank of India"
      },
      {
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_INDB",
        "description": "Indian Bank "
      },
      {
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_INDUS",
        "description": "IndusInd Bank"
      },
      {
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_IOB",
        "description": "Indian Overseas Bank"
      },
      {
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_JNK",
        "description": "Jammu and Kashmir Bank"
      },
      {
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_KARN",
        "description": "Karnataka Bank"
      },
      {
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_KVB",
        "description": "Karur Vysya "
      },
      {
        "supportedReferenceIds": [
          "vodafone",
          "prepaid_app",
          "postpaid_web",
          "postpaid_app",
          "prepaid_web"
        ],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_SBI",
        "description": "State Bank of India"
      },
      {
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_SOIB",
        "description": "South Indian Bank"
      },
      {
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_UBI",
        "description": "Union Bank of India"
      },
      {
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_UNIB",
        "description": "United Bank Of India"
      },
      {
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_VJYB",
        "description": "Vijaya Bank"
      },
      {
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_CUB",
        "description": "CityUnion"
      },
      {
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_CANR",
        "description": "Canara Bank"
      },
      {
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_KOTAK",
        "description": "Kotak Bank"
      },
      {
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_CSB",
        "description": "Catholic Syrian Bank"
      },
      {
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_OBC",
        "description": "Oriental Bank Of Commerce"
      },
      {
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_SCB",
        "description": "Standard Chartered Bank"
      },
      {
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_TMB",
        "description": "Tamilnad Mercantile Bank"
      },
      {
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_ANDHRA",
        "description": "Andhra Bank"
      },
      {
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_SARASB",
        "description": "Saraswat Bank"
      },
      {
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_JSB",
        "description": "Janata Sahakari Bank"
      },
      {
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_LVB",
        "description": "Lakshmi Vilas Bank Retail"
      },
      {
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_PMCB",
        "description": "Punjab and Maharashtra Coop Bank"
      },
      {
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_PNJSB",
        "description": "Punjab and Sind Bank"
      },
      {
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_SVCB",
        "description": "Shamrao Vithal Coop Bank"
      },
      {
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_DENA",
        "description": "DENA Bank"
      },
      {
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_PNB",
        "description": "Punjab National Bank Retail"
      },
      {
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_SYNB",
        "description": "Syndicate Bank"
      },
      {
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_UCOB",
        "description": "UCO Bank"
      },
      {
        "walletDirectDebitSupport": true,
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "WALLET",
        "paymentMethod": "PAYTM",
        "description": "Paytm Wallet"
      },
      {
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_DBS",
        "description": "DBS Bank Ltd"
      },
      {
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_AXISCORP",
        "description": "Axis Corporate Netbanking"
      },
      {
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_COSMOS",
        "description": "COSMOS Bank"
      },
      {
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_IDFC",
        "description": "IDFC Bank"
      },
      {
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_BHARAT",
        "description": "Bharat Bank"
      },
      {
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_RBL",
        "description": "RBL"
      },
      {
        "supportedReferenceIds": [
          "vodafone",
          "prepaid_app",
          "postpaid_web",
          "postpaid_app",
          "prepaid_web"
        ],
        "paymentMethodType": "CARD",
        "paymentMethod": "MASTER",
        "description": "Mastercard"
      },
      {
        "supportedReferenceIds": [
          "vodafone",
          "prepaid_app",
          "postpaid_web",
          "postpaid_app",
          "prepaid_web"
        ],
        "paymentMethodType": "CARD",
        "paymentMethod": "VISA",
        "description": "Visa"
      },
      {
        "supportedReferenceIds": [
          "vodafone",
          "prepaid_app",
          "postpaid_web",
          "postpaid_app",
          "prepaid_web"
        ],
        "paymentMethodType": "CARD",
        "paymentMethod": "RUPAY",
        "description": "Rupay"
      },
      {
        "supportedReferenceIds": [
          "vodafone",
          "prepaid_app",
          "postpaid_web",
          "postpaid_app",
          "prepaid_web"
        ],
        "paymentMethodType": "CARD",
        "paymentMethod": "AMEX",
        "description": "American Express"
      },
      {
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "CARD",
        "paymentMethod": "DISCOVER",
        "description": "Discover"
      },
      {
        "supportedReferenceIds": [
          "vodafone",
          "prepaid_app",
          "postpaid_web",
          "postpaid_app",
          "prepaid_web"
        ],
        "paymentMethodType": "CARD",
        "paymentMethod": "DINERS",
        "description": "Diners"
      },
      {
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "CARD",
        "paymentMethod": "MAESTRO",
        "description": "Maestro"
      },
      {
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "CARD",
        "paymentMethod": "JCB",
        "description": "JCB"
      },
      {
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "UPI",
        "paymentMethod": "UPI",
        "description": "UPI"
      },
      {
        "supportedReferenceIds": [],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_FED",
        "description": "Federal Bank"
      },
      {
        "supportedReferenceIds": [],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_ALLB",
        "description": "Allahabad Bank"
      },
      {
        "walletDirectDebitSupport": true,
        "supportedReferenceIds": [
          "vodafone"
        ],
        "paymentMethodType": "WALLET",
        "paymentMethod": "LAZYPAY",
        "description": "Lazy Pay Wallet"
      },
      {
        "supportedReferenceIds": [
          "prepaid_app",
          "postpaid_web",
          "prepaid_web"
        ],
        "paymentMethodType": "NB",
        "paymentMethod": "NB_BILLDESKTEST",
        "description": "Billdesk Test Bank"
      }
    ],
  //  "lastUsedPaymentMethod": [],
    "emandatePaymentMethods": [],
    "cards": [],
    "appsUsed": []
  };
  return JSON.stringify(response);
}




exports["setHeight"] = function(bool){
  window.isPaymentLocked = bool;
}
var payLaterMap = ["LAZYPAY", "SIMPL", "EPAYLATER", "OLAPOSTPAID", "LOANTAP"]

exports["getSideBarTab"] = function(payOps) {
  return function(sideTabs) {
    return function(gateway_id){
      return function(payLaterEligiblity){
      const merchantPO = payOps.merchantPaymentMethods
        .map(function (paymentMethod) {
          const paymentMethodType = paymentMethod.paymentMethodType
          return paymentMethodType === "NB" ? "NET_BANKING" : paymentMethodType
        });
      var payLaterExists = false;
      for(var po in payOps.merchantPaymentMethods){
        if (payLaterMap.includes(payOps.merchantPaymentMethods[po].paymentMethod)){
          payLaterExists = true;
        }
      }
      const tabs= sideTabs.filter(function (sideTab) {
        return (merchantPO.includes(sideTab) || (sideTab == "PAY_LATER" && payLaterExists))
      })

      const supported_gateway = payOps.merchantPaymentMethods.find(function(obj) {
        return obj.paymentMethodType === "UPI"
      })
      if (supported_gateway && supported_gateway.supportedReferenceIds && supported_gateway.supportedReferenceIds.value0 && (gateway_id.length !== 0)){
        var flag = false;
        for (var i =0;i< gateway_id.length;i++){
          if (supported_gateway.supportedReferenceIds.value0.includes(gateway_id[i])){
            flag=true
          }
        }
        if (flag==false){
          const index =tabs.indexOf("UPI");
          if (index>=0){
            tabs.splice(index, 1)
          }
        }

      }



      /// CHANGES HERE
      const orderDetails = JSON.parse(window.__payload.order_details);
      const gateway = orderDetails["metadata.JUSPAY:gateway_reference_id"];
      if (gateway =="bigbasket_b2c_checkout"){
        tabs.push("MORE_OPTIONS");
      }
      // if (sideTabs.indexOf("PAY_LATER")!=-1){
      //   tabs.push("PAY_LATER");
      // }
      return tabs;
    }
  }
}
}

exports["getOtherPaymentOptions"] = function(payOps) {
  return function(sideTabs) {
    for(var i=0; i< payOps.length; i++){
      var po = payOps[i].po;
      if(po == "cards"){
        if(!sideTabs.includes("CARD")){
          payOps[i]["visibility"] = "gone"
        }
      } else if(po == "nb") {
        if(!sideTabs.includes("NET_BANKING")){
          payOps[i]["visibility"] = "gone"
        }
      } else if(po == "upi") {
        if(!sideTabs.includes("UPI")){
          payOps[i]["visibility"] = "gone"
        }
      } else if(po == "wallets") {
        if(!sideTabs.includes("WALLET")){
          payOps[i]["visibility"] = "gone"
        }
      } else if (po == "cards"){
        if(sideTabs.includes("CARD")){
          payOps[i]["visibility"] = "VISIBLE"
        }
      }
    }
    return payOps;
  }
}
exports["detectBrowser"]= function(){
  var userAgent = navigator.userAgent;
  if (userAgent.includes("Firefox")){
    return "Firefox";
  }
  else if (userAgent.includes("Seamonkey")){
    return "Seamonkey"
  }
  else if (userAgent.includes("Chrome")){
    return "Chrome"
  }
  else if (userAgent.includes("Chromium")){
    return "Chromium"
  }
  else if (userAgent.includes("Safari")){
    return "Safari"
  }
  else if (userAgent.includes("OPR") || userAgent.includes("Opera")){
    return "Opera"
  }
  else if (userAgent.includes("MSIE") || userAgent.includes("Trident")){
    return "IE"
  }
  return userAgent;

}

exports["deviceDetect"]= function(){
  var userAgent = navigator.userAgent;
  if (userAgent.includes("iPhone")){
    return "iPhone";
  }

  if(userAgent.includes("Mac OS")){
    return "MAC"
  }
  if (userAgent.includes("Android") ){
    return "Android";
  }
  return "Desktop"
}

exports["getDummyOutages"] = function(){
  var outages = [
    {
    "status": "DOWN",
    "startTime": "2019-10-03T10:49:31Z",
    "scope": "merchant_gateway",
    "paymentInstrumentGroup": "NA",
    "gateway": "AXIS_UPI",
    "endTime": "2019-10-03T11:48:18Z",
    "bank": "NA"
    },
    {
    "status": "DOWN",
    "startTime": "2019-10-03T10:49:31Z",
    "scope": "merchant_gateway",
    "paymentInstrumentGroup": "NA",
    "gateway": "GOOGLEPAY",
    "endTime": "2019-10-03T11:48:18Z",
    "bank": "NA"
    },
    {
    "status": "DOWN",
    "startTime": "2019-10-03T10:49:31Z",
    "scope": "merchant_bank",
    "paymentInstrumentGroup": "NA",
    "gateway": "NA",
    "endTime": "2019-10-03T11:48:18Z",
    "bank": "UPI"
    },
    {
    "status": "DOWN",
    "startTime": "2019-10-03T10:49:31Z",
    "scope": "merchant_bank",
    "paymentInstrumentGroup": "NA",
    "gateway": "NA",
    "endTime": "2019-10-03T11:48:18Z",
    "bank": "GOOGLEPAY"
    },
    {
    "status": "DOWN",
    "startTime": "2019-10-03T10:49:31Z",
    "scope": "merchant_pig",
    "paymentInstrumentGroup": "UPI",
    "gateway": "NA",
    "endTime": "2019-10-03T11:48:18Z",
    "bank": "NA"
    },
    {
    "status": "DOWN",
    "startTime": "2019-10-03T10:49:31Z",
    "scope": "merchant_pig",
    "paymentInstrumentGroup": "WALLET",
    "gateway": "NA",
    "endTime": "2019-10-03T11:48:18Z",
    "bank": "NA"
    },
    {
    "status": "DOWN",
    "startTime": "2019-10-03T10:49:31Z",
    "scope": "merchant_gateway_bank",
    "paymentInstrumentGroup": "NA",
    "gateway": "AXIS_UPI",
    "endTime": "2019-10-03T11:48:18Z",
    "bank": "UPI"
    },
    {
    "status": "DOWN",
    "startTime": "2019-10-03T10:49:31Z",
    "scope": "merchant_gateway_bank",
    "paymentInstrumentGroup": "NA",
    "gateway": "GOOGLEPAY",
    "endTime": "2019-10-03T11:48:18Z",
    "bank": "GOOGLEPAY"
    },
    {
    "status": "DOWN",
    "startTime": "2019-10-03T10:49:31Z",
    "scope": "merchant_bank_pig",
    "paymentInstrumentGroup": "UPI",
    "gateway": "NA",
    "endTime": "2019-10-03T11:48:18Z",
    "bank": "UPI"
    },
    {
    "status": "DOWN",
    "startTime": "2019-10-03T10:49:31Z",
    "scope": "merchant_bank_pig",
    "paymentInstrumentGroup": "WALLET",
    "gateway": "NA",
    "endTime": "2019-10-03T11:48:18Z",
    "bank": "GOOGLEPAY"
    }
  ]
  return JSON.stringify(outages);
}

exports["getDummyData"] = function () {
  var obj = {
    "wallets": [],
    "vpas": [],
    "offers": [],
    "nbMethods": [],
    "outages" :[],
    "merchantPaymentMethods" : [{
        "paymentMethodType": "CARD",
        "paymentMethod": "JCB",
        "description": "JCB"
    }],
    "emandatePaymentMethods":[],
    "lastUsedPaymentMethod": {},
    "cards": [],
    "appsUsed": []
    // "merchantPaymentMethods": [{
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_DUMMY",
    //     "description": "Dummy Bank"
    //   },
    //   {
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_AXIS",
    //     "description": "Axis Bank"
    //   },
    //   {
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_BOI",
    //     "description": "Bank of India"
    //   },
    //   {
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_BOM",
    //     "description": "Bank of Maharashtra"
    //   },
    //   {
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_CBI",
    //     "description": "Central Bank Of India"
    //   },
    //   {
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_CORP",
    //     "description": "Corporation Bank"
    //   },
    //   {
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_DCB",
    //     "description": "Development Credit Bank"
    //   },
    //   {
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_FED",
    //     "description": "Federal Bank"
    //   },
    //   {
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_HDFC",
    //     "description": "HDFC Bank"
    //   },
    //   {
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_ICICI",
    //     "description": "ICICI Netbanking"
    //   },
    //   {
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_IDBI",
    //     "description": "Industrial Development Bank of India"
    //   },
    //   {
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_INDB",
    //     "description": "Indian Bank "
    //   },
    //   {
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_INDUS",
    //     "description": "IndusInd Bank"
    //   },
    //   {
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_IOB",
    //     "description": "Indian Overseas Bank"
    //   },
    //   {
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_JNK",
    //     "description": "Jammu and Kashmir Bank"
    //   },
    //   {
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_KARN",
    //     "description": "Karnataka Bank"
    //   },
    //   {
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_KVB",
    //     "description": "Karur Vysya "
    //   },
    //   {
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_SBBJ",
    //     "description": "State Bank of Bikaner and Jaipur"
    //   },
    //   {
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_SBH",
    //     "description": "State Bank of Hyderabad"
    //   },
    //   {
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_SBI",
    //     "description": "State Bank of India"
    //   },
    //   {
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_SBM",
    //     "description": "State Bank of Mysore"
    //   },
    //   {
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_SBT",
    //     "description": "State Bank of Travancore"
    //   },
    //   {
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_SOIB",
    //     "description": "South Indian Bank"
    //   },
    //   {
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_UBI",
    //     "description": "Union Bank of India"
    //   },
    //   {
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_UNIB",
    //     "description": "United Bank Of India"
    //   },
    //   {
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_VJYB",
    //     "description": "Vijaya Bank"
    //   },
    //   {
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_YESB",
    //     "description": "Yes Bank"
    //   },
    //   {
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_CUB",
    //     "description": "CityUnion"
    //   },
    //   {
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_CANR",
    //     "description": "Canara Bank"
    //   },
    //   {
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_SBP",
    //     "description": "State Bank of Patiala"
    //   },
    //   {
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_DEUT",
    //     "description": "Deutsche Bank"
    //   },
    //   {
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_KOTAK",
    //     "description": "Kotak Bank"
    //   },
    //   {
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_DLS",
    //     "description": "Dhanalaxmi Bank"
    //   },
    //   {
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_ING",
    //     "description": "ING Vysya Bank"
    //   },
    //   {
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_ANDHRA",
    //     "description": "Andhra Bank"
    //   },
    //   {
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_COSMOS",
    //     "description": "COSMOS Bank"
    //   },
    //   {
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_JSB",
    //     "description": "Janata Sahakari Bank"
    //   },
    //   {
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_PMCB",
    //     "description": "Punjab and Maharashtra Coop Bank"
    //   },
    //   {
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_PNJSB",
    //     "description": "Punjab and Sind Bank"
    //   },
    //   {
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_SYNB",
    //     "description": "Syndicate Bank"
    //   },
    //   {
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_TMB",
    //     "description": "Tamilnad Mercantile Bank"
    //   },
    //   {
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_NAIB",
    //     "description": "Nainital Bank"
    //   },
    //   {
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_UCOB",
    //     "description": "UCO Bank"
    //   },
    //   {
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_BHARAT",
    //     "description": "Bharat Bank"
    //   },
    //   {
    //     "paymentMethodType": "NB",
    //     "paymentMethod": "NB_IDFC",
    //     "description": "IDFC Bank"
    //   },
    //   {
    //     "paymentMethodType": "UPI",
    //     "paymentMethod": "UPI",
    //     "description": "UPI"
    //   },
    //   {
    //     "paymentMethodType": "CARD",
    //     "paymentMethod": "MASTER",
    //     "description": "Mastercard"
    //   },
    //   {
    //     "paymentMethodType": "CARD",
    //     "paymentMethod": "VISA",
    //     "description": "Visa"
    //   },
    //   {
    //     "paymentMethodType": "CARD",
    //     "paymentMethod": "RUPAY",
    //     "description": "Rupay"
    //   },
    //   {
    //     "paymentMethodType": "CARD",
    //     "paymentMethod": "AMEX",
    //     "description": "American Express"
    //   },
    //   {
    //     "paymentMethodType": "CARD",
    //     "paymentMethod": "DISCOVER",
    //     "description": "Discover"
    //   },
    //   {
    //     "paymentMethodType": "CARD",
    //     "paymentMethod": "DINERS",
    //     "description": "Diners"
    //   },
    //   {
    //     "paymentMethodType": "CARD",
    //     "paymentMethod": "MAESTRO",
    //     "description": "Maestro"
    //   },
    //   {
    //     "paymentMethodType": "CARD",
    //     "paymentMethod": "JCB",
    //     "description": "JCB"
    //   }
    // ],

  };
  return JSON.stringify(obj);
}

exports["checkContent"] = function(key) {
  const className = "sidebarItem_content_" + key;
  //const items = document.querySelectorAll('.sidebarItem_content');
  const styleElem = document.getElementById('dynamicCSS');

  /* Add CSS */
  const css = '.sidebarItem_content{opacity: 0 !important;}';
  if(styleElem.styleSheet) {
    styleElem.styleSheet.cssText = css;
  } else {
    styleElem.innerHTML = '';
    styleElem.appendChild(document.createTextNode(css));
  }
  /* Add CSS End */

  var iterateCount = 0;
  var iterateCheck = setInterval(function() {
    const object = document.querySelectorAll('.' + className);

    console.log('iterating...');

    if (object && object.length > 0) {
      clearInterval(iterateCheck);

      object.forEach(function(item) {
        item.classList.add('start');
      });

      setTimeout(function() {
        /* Remove CSS */
        if(styleElem.styleSheet) {
          styleElem.styleSheet.cssText = '';
        } else {
          styleElem.innerHTML = '';
        }
        /* Remove CSS End */

        object.forEach(function(item) {
          item.classList.remove('start');
        });
      }, 300);
    } else if (iterateCount > 3) {
      clearInterval(iterateCheck);
      /* Remove CSS */
      if(styleElem.styleSheet) {
        styleElem.styleSheet.cssText = '';
      } else {
        styleElem.innerHTML = '';
      }
      /* Remove CSS End */
    }

    iterateCount++;
  }, 500);

  return key;
}

exports["checkTab"] = function(key) {
  const className = "sidebarItemWrap_" + key;
  const object = document.querySelectorAll('.sidebar_wrapper .' + className);
  const items = document.querySelectorAll('.sidebar_wrapper .sidebarItemWrap');

  if (items) {
    items.forEach(function(item) {
      item.classList.remove('active');
    });
  }

  if (object) {
    object.forEach(function(item) {
      item.classList.add('active');
    });
  }

  return key;
}
