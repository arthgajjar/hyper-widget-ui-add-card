module Remote.Types where

import Prelude

import Data.Generic.Rep (class Generic)
import Data.Maybe (Maybe(..), fromMaybe, isJust)
import Data.Newtype (class Newtype)
import Effect.Class (liftEffect)
import Service.EC.Types.Response (OutagesStatus(..))
import Engineering.Helpers.Network (urlEncodedMakeRequest)
import Foreign.Class (class Decode, class Encode)
import Payments.Core.Commons (Checkout, checkoutDetails, getCheckoutDetails, getValueFromPayload')
import Engineering.Helpers.Commons(PaymentOffer(..))
import Presto.Core.Types.API (class RestEndpoint, Header(..), Headers(..), Method(..), URL, defaultDecodeResponse, defaultMakeRequest, defaultMakeRequest_)
import Presto.Core.Types.Language.Flow (APIResult, Control, Flow, await, callAPI, doAff)
import Presto.Core.Utils.Encoding (defaultDecode, defaultEncode)

baseUrl :: String
baseUrl = if checkoutDetails.environment == "sandbox"
              then "https://sandbox.juspay.in"
              else if checkoutDetails.environment == "production_static1"
                      then "https://api-ns1.juspay.in"
                      else "https://api.juspay.in"


-------------------------------------------------- Card Info --------------------------------------------------

newtype CardInfo =
  CardInfo
    { id :: String
    , object :: String
    , bank :: String
    , country :: String
    , "type" :: String
    , direct_otp_support :: Maybe Boolean
    , mandate_support :: Maybe Boolean
    }

derive instance cardInfoNewtype :: Newtype CardInfo _
derive instance cardInfoGeneric :: Generic CardInfo _

instance encodeCardInfo :: Encode CardInfo where encode = defaultEncode
instance decodeCardInfo :: Decode CardInfo where decode = defaultDecode

newtype CardInfoReq =
  CardInfoReq
    { merchant_id :: String
    , fstsix :: String
    , "options.check_direct_otp_support" :: Boolean
    , environment :: String
    }
derive instance cardInfoReqNewtype :: Newtype CardInfoReq _
derive instance cardInfoReqGeneric :: Generic CardInfoReq _

instance encodeCardInfoReq :: Encode CardInfoReq where encode = defaultEncode
instance decodeCardInfoReq :: Decode CardInfoReq where decode = defaultDecode


instance getCardInfoApi :: RestEndpoint CardInfoReq CardInfo where
  makeRequest (CardInfoReq reqBody) headers = let
    url = (baseUrl <> "/cardbins/" <> reqBody.fstsix <> "?options.check_direct_otp_support=true&merchant_id=" <> reqBody.merchant_id)
    in defaultMakeRequest_ GET url headers
  decodeResponse body = defaultDecodeResponse body


callCardInfo :: CardInfoReq -> Flow (APIResult CardInfo)
callCardInfo req = (callAPI (Headers []) req)

getCardInfoPayload :: Checkout -> String -> CardInfoReq
getCardInfoPayload checkout cardFstSix =
  CardInfoReq
    { merchant_id : checkout.merchant_id
    , fstsix : cardFstSix
    , "options.check_direct_otp_support" : true
    , environment : checkout.environment
    }

getCardInfoResp :: Control Unit -> String -> Flow (APIResult CardInfo)
getCardInfoResp sessionInfoFiber cardFstSix = do
  _ <- await sessionInfoFiber
  checkout <- doAff do liftEffect getCheckoutDetails
  callCardInfo $ getCardInfoPayload checkout cardFstSix

-------------------------------------------------- PaymentPageConfig --------------------------------------------------


newtype Gradient =
  Gradient
    { from :: String
    , to :: String
    , direction :: String
    }

newtype UICard =
  UICard
    { translation :: String
    , cornerRadius :: String
    , horizontalPadding :: String
    , verticalPadding :: String
    , attachPOHeaders :: Maybe String
    , color :: String
    }

newtype InputField =
  InputField
    { type :: String
    , useMaterialView :: String
    , cornerRadius :: Maybe String
    , focusColor :: String
    , textPadding :: String
    , background :: String
    , fontStyle :: String
    , labelFontFace :: String
    , labelFontSize :: String
    , labelFontColor :: String
    }
newtype SecondaryButton =
  SecondaryButton
    { useNavigationArrow :: String
    , width :: String
    , height :: String
    , buttonAlignment :: String
    , buttonBackground :: String
    , buttonCornerRadius :: String
    , textColor :: String
    , textSize :: String
    , fontFace :: String
    }

newtype HeaderProps =
  HeaderProps
    { size :: String
    , color :: String
    , fontFace :: String
    , bottomMargin :: String
    }

newtype ContainerAttribs =
  ContainerAttribs
    { horizontalSpacing :: String
    , verticalSpacing :: String
    , sectionSpacing :: String
    }

newtype Toolbar =
  Toolbar
    { back :: String
    , pageTitle :: String
    , textColor :: String
    , color :: String
    , toolbarIconColor :: String
    , toolbarIcon :: String
    , textFont :: String
    , textSize :: String
    --, toolbarGravity :: String
    }

newtype AmountBar =
  AmountBar
    { amountBarData :: Array AmountBarLine
    , placeAmountAtRight :: String
    , attachOnTop :: String
    , attachOnAllScreens :: String
    , visibility :: String
    , verticalPadding :: String
    , translation :: String
    , amountTextSize :: String
    , color :: String
    , addDivider :: String
    , dividerColor :: String
    }

newtype AmountBarLine =
  AmountBarLine
    { leftString :: Maybe String
    , centerString :: Maybe String
    , rightString :: Maybe String
    , textFontFace :: String
    , fontSize :: String
    , color :: String
    }

newtype LabelButton =
  LabelButton
    { color :: String
    , textColor :: String
    , textFontFace :: String
    , cornerRadius :: String
    , textSize :: String
    , height :: String
    }

newtype Saved =
  Saved
    { saved :: String
    , preffered :: String
    , otherSaved :: String
    }

newtype ScreenTransition =
  ScreenTransition
    { duration :: String
    , curve :: Array String
    }

newtype PaymentOptions =
  PaymentOptions
    { group :: String
    , po :: String
    , visibility :: String
    , onlyEnable :: Maybe (Array String)
    , onlyDisable :: Maybe (Array String)
    , isLast :: String
    }

instance eqPaymentOptions :: Eq PaymentOptions where
  eq (PaymentOptions p1) (PaymentOptions p2) =
    p1.group == p2.group && p1.po == p2.po

newtype TopBar =
  TopBar
  {
    backgroundColor :: String,
    height :: String,
    visibility :: String,
    primaryFontSize :: String,
    secondaryFontSize :: String,
    summaryTitleOne :: String,
    summaryTitleTwo ::String,
    summaryTextOne :: String,
    summaryTextTwo :: String,
    fontColor :: String,
    font :: String
  }

-- instance eqPaymentOptions :: Eq PaymentOptions where
--   eq (PaymentOptions p1) (PaymentOptions p2) =
--     p1.group == p2.group && p1.po == p2.po

type VisiblePayOption =
  { po :: String
  , hideDivider :: Boolean
  }

newtype ConfigPayload =
  ConfigPayload
    { primaryColor :: String
    , amountBar :: AmountBar
    , language :: Maybe String
    , showSavedVPAs :: String
    , isCardLayout :: String
    , radioButtonPosition :: String
    , upiQREnable :: String
    , useLocalIcons :: String
    , errorTextColor :: String
    , showTopBar :: String
    , redirectUPI :: String
    , modalView :: String
    , upiCollectWithGodel :: String
    , screenTransition :: ScreenTransition
    , popularBanks :: Array String
    , highlight :: Array PaymentOptions
    , primaryFont :: String
    , toolbar :: Toolbar
    , saved :: Saved
    , paymentOptions :: Array PaymentOptions
    , offerOptions :: Array PaymentOptions
    , moreOption :: MoreOption
    , highlightViewType :: String
    , expandedWalletView :: String
    , expandPopularNBView :: String
    , expandUpiView :: String
    , fontSize:: String
    , iconSize :: String
    , lineSeparator :: String
    , gridFontSize :: String
    , gridIconSize :: String
    , checkboxSize :: String
    , checkboxSizeMobile :: String
    , addCardFontColor :: String
    , spacing :: String
    , listItemHeight :: String
    , fontColor :: String
    , backgroundColor :: String
    , backgroundColorMobile :: String
    , toolbarColor :: String
    , combineWallets :: String
    , cornerRadius :: String
    , toolbarIconLocation :: String
    , wrapper :: Wrapper
    , wrapperRadius :: String
    , cvvInputBoxRadius :: String
    , cvvInputBoxType :: String
    , verifyMobile :: String
    , offers :: String
    , containerPadding :: String
    , containerPaddingMobile :: String
    , buttonText :: Maybe String
    , useCurvedPaymentOption :: String
    , paymentOptionShadow :: String
    , useSeperatedSections :: String
    , paymentOptionFontFace :: String  -- defaults to regular
    , searchBoxCornerRadius :: String
    , strechedConfirmButton :: String
    , showlineSepOnSeprateSections :: String
    , hideLastDivider :: String
    , textGravity ::String
    , cvvToolTip :: String
    , saveCardToolTip :: String
    , defaultOptionSelect :: String
    , sideBarTabs :: Array String
    , savedOptionsWeb :: String
    , topBar :: TopBar
    , topBarMobile :: TopBar
    , sideBarNav :: String
    , sideBarNavDirection :: String
    , isInputBoxed :: String
    , verifyVPA :: String
    , guestUserAllowed :: String
    , wrapperColor :: String
    , curvedWrapperColor :: String
    , isButtonGradient :: String
    , buttonGradient :: ButtonGradient
    , mainBackground :: String
    , lineSeparatorColor :: String
    , addIconBackground :: String
    , addIconColor :: String
    , iconUrl :: String
    , iconUrlNonSelected :: String
    , nbLayout :: String
    , backIcon :: String
    , backIconMobile :: String
    , timerColor :: String
    , disabledFontColor :: String
    , logoMargin :: String
    , logoBackground :: String
    , logoRadius :: String
    , showUPIForGuest :: String
    , useBackgroundImage :: String
    , hideOrderSummary :: String
    , hideTopToolBar :: String
    , internationalCardsAllowed :: String
    , button :: Button
    , textBox :: TextBox
    , sideBar :: SideBar
    , sectionHeader :: FontConfig
    , mainHeader :: FontConfig
    , paymentOption :: PaymentOption
    , ppBorderContainer :: PPBorderContainer
    , paymentOptionErrors :: Array PaymentOptionError
    , outageConfig :: OutageConfig
    , addCardConfig :: AddCardConfig
    , inputField :: InputField
    , containerAttribs :: ContainerAttribs
    , uiCard :: UICard
    , useQuickPayRanking :: String
    , gridProps :: GridProps
    , mandateViewProps :: MandateViewProps
    , offerViewProps :: OfferViewProps
    , topRowVisibility :: String
    , widgets :: Maybe Widgets
    , tertiaryFontColor :: String
    , outageViewProps :: OutageViewProps
    , secondaryButton :: SecondaryButton
    , labelButton :: LabelButton
    , mandateInstruments :: Array String
    , highlightMessage :: HighlightMessage
    , headerProps :: HeaderProps
    , attachPOHeaders :: String
    -- , language :: String
  }




newtype Widgets =
  Widgets
    { allowed :: Array String
    , primary :: String
    }

newtype MoreOption =
  MoreOption
    { visibility :: String
    , icon :: String
    , name :: String
    , view ::
      { toolbar :: Toolbar
      , content :: String
      , footer :: String
      , action :: String
      }
    }

newtype DOTPScreenConfigPayload = DOTPScreenConfigPayload
  { primaryColor :: String
  , secondaryColor :: Maybe String
  , backgroundColor :: String
  , primaryFont :: String
  , toolbarTextSize :: Int
  , toolbarTextFontFace :: String
  , cornerRadius :: Number
  , otpEditTextBackground :: String
  }


derive instance toolbarGeneric :: Generic Toolbar  _
derive instance toolbarNewtype :: Newtype Toolbar _
instance toolbarDecode :: Decode Toolbar where decode = defaultDecode
instance toolbarEncode :: Encode Toolbar where encode = defaultEncode

derive instance genericButton :: Generic Button _
instance decodeButton :: Decode Button where decode = defaultDecode
instance encodeButton :: Encode Button where encode = defaultEncode

derive instance genericGradient :: Generic Gradient _
instance decodeGradient :: Decode Gradient where decode = defaultDecode
instance encodeGradient :: Encode Gradient where encode = defaultEncode

derive instance genericContainerAttribs :: Generic ContainerAttribs _
instance decodeContainerAttribs :: Decode ContainerAttribs where decode = defaultDecode
instance encodeContainerAttribs :: Encode ContainerAttribs where encode = defaultEncode

derive instance genericHeaderProps :: Generic HeaderProps _
instance decodeHeaderProps :: Decode HeaderProps where decode = defaultDecode
instance encodeHeaderProps :: Encode HeaderProps where encode = defaultEncode

derive instance genericUICard :: Generic UICard _
instance decodeUICard :: Decode UICard where decode = defaultDecode
instance encodeUICard :: Encode UICard where encode = defaultEncode


newtype OutageViewProps =
  OutageViewProps
    { showOutageView :: String
    , textColor :: String
    , backgroundColor :: String
    , cornerRadius :: String
    , padding :: { left :: String, right :: String, vertical :: String }
    , textSize :: String
    , outageMessage :: String
    , restrictPayment :: String
    }

derive instance genericOutageViewProps :: Generic OutageViewProps _
instance decodeOutageViewProps :: Decode OutageViewProps where decode = defaultDecode
instance encodeOutageViewProps :: Encode OutageViewProps where encode = defaultEncode



newtype HighlightMessage =
  HighlightMessage
    { textColor :: String
    , backgroundColor :: String
    , textFontFace :: String
    , padding :: { vertical :: String, horizontal :: String }
    }


derive instance genericHighlightMessage :: Generic HighlightMessage _
instance decodeHighlightMessage :: Decode HighlightMessage where decode = defaultDecode
instance encodeHighlightMessage :: Encode HighlightMessage where encode = defaultEncode


newtype MandateViewProps =
  MandateViewProps
    { textColor :: String
    , backgroundColor :: String
    , cornerRadius :: String
    , padding :: { left :: String, right :: String, vertical :: String }
    , textSize :: String
    }


derive instance genericMandateViewProps :: Generic MandateViewProps _
instance decodeMandateViewProps :: Decode MandateViewProps where decode = defaultDecode
instance encodeMandateViewProps :: Encode MandateViewProps where encode = defaultEncode


newtype OfferViewProps =
  OfferViewProps
    { textColor :: String
    , backgroundColor :: String
    , cornerRadius :: String
    , padding :: { left :: String, right :: String, vertical :: String }
    , textSize :: String
    }


derive instance genericOfferViewProps :: Generic OfferViewProps _
instance decodeOfferViewProps :: Decode OfferViewProps where decode = defaultDecode
instance encodeOfferViewProps :: Encode OfferViewProps where encode = defaultEncode

newtype GridProps =
  GridProps
    { useTick :: String
    , useStroke :: String
    , itemSize :: String
    , gridSelectedStroke :: String
    , gridLogoStroke :: String
    , useButtonForSelection :: String
    , gridViewBackground :: String
    , horizontalFade :: String
    , fadingEdgeLength :: String
    , strokeCornerRadius :: String
    , gridFontSize :: String
    , gridIconSize :: String
    }


derive instance genericGridProps :: Generic GridProps _
instance decodeGridProps :: Decode GridProps where decode = defaultDecode
instance encodeGridProps :: Encode GridProps where encode = defaultEncode

derive instance genericAmountBar :: Generic AmountBar _
instance decodeAmountBar :: Decode AmountBar where decode = defaultDecode
instance encodeAmountBar :: Encode AmountBar where encode = defaultEncode

derive instance genericInputField :: Generic InputField _
instance decodeInputField :: Decode InputField where decode = defaultDecode
instance encodeInputField :: Encode InputField where encode = defaultEncode

derive instance genericSecondaryButton :: Generic SecondaryButton _
instance decodeSecondaryButton :: Decode SecondaryButton where decode = defaultDecode
instance encodeSecondaryButton :: Encode SecondaryButton where encode = defaultEncode

derive instance genericAmountBarLine :: Generic AmountBarLine _
instance decodeAmountBarLine :: Decode AmountBarLine where decode = defaultDecode
instance encodeAmountBarLine :: Encode AmountBarLine where encode = defaultEncode

derive instance genericLabelButton :: Generic LabelButton _
instance decodeLabelButton :: Decode LabelButton where decode = defaultDecode
instance encodeLabelButton :: Encode LabelButton where encode = defaultEncode

derive instance savedGeneric :: Generic Saved  _
derive instance savedNewtype :: Newtype Saved _
instance savedDecode :: Decode Saved where decode = defaultDecode
instance savedEncode :: Encode Saved where encode = defaultEncode

derive instance configPayloadGeneric :: Generic ConfigPayload  _
derive instance configPayloadNewtype :: Newtype ConfigPayload _
instance configPayloadDecode :: Decode ConfigPayload where decode = defaultDecode
instance configPayloadEncode :: Encode ConfigPayload where encode = defaultEncode

derive instance paymentOptionsGeneric :: Generic PaymentOptions  _
derive instance paymentOptionsNewType :: Newtype PaymentOptions _
instance paymentOptionsDecode :: Decode PaymentOptions where decode = defaultDecode
instance paymentOptionsEncode :: Encode PaymentOptions where encode = defaultEncode

derive instance moreOptionGeneric :: Generic MoreOption  _
derive instance moreOptionNewType :: Newtype MoreOption _
instance moreOptionDecode :: Decode MoreOption where decode = defaultDecode
instance moreOptionEncode :: Encode MoreOption where encode = defaultEncode

derive instance dOTPScreenConfigPayloadGeneric :: Generic DOTPScreenConfigPayload  _
derive instance dOTPScreenConfigPayloadNewType :: Newtype DOTPScreenConfigPayload _
instance dOTPScreenConfigPayloadDecode :: Decode DOTPScreenConfigPayload where decode = defaultDecode
instance dOTPScreenConfigPayloadEncode :: Encode DOTPScreenConfigPayload where encode = defaultEncode

derive instance widgetsGeneric :: Generic Widgets  _
derive instance widgetsNewType :: Newtype Widgets _
instance widgetsDecode :: Decode Widgets where decode = defaultDecode
instance widgetsEncode :: Encode Widgets where encode = defaultEncode

derive instance screenTransitionGeneric :: Generic ScreenTransition _
derive instance screenTransitionNewtype :: Newtype ScreenTransition _
instance screenTransitionDecode :: Decode ScreenTransition where decode = defaultDecode
instance screenTransitionEncode :: Encode ScreenTransition where encode = defaultEncode

-------------------------------------------------- Merchant Offer Applied --------------------------------------------------

newtype MerchantOffer =
  MerchantOffer
    { applied :: String
    , payment_method_type :: Maybe String
    , payment_method :: Maybe String
    , payment_card_type :: Maybe String
    , payment_card_issuer :: Maybe String
    , offer_code :: Maybe String
    , amount :: Maybe String
    }

derive instance merchantOffersGeneric :: Generic MerchantOffer  _
derive instance merchantOffersNewtype :: Newtype MerchantOffer _
instance merchantOffersDecode :: Decode MerchantOffer where decode = defaultDecode

defaultMerchantOffer :: MerchantOffer
defaultMerchantOffer =
    MerchantOffer
    { applied : "false"
    , payment_method_type : Nothing
    , payment_method : Nothing
    , payment_card_type : Nothing
    , payment_card_issuer : Nothing
    , offer_code : Nothing
    , amount : Nothing
    }

-------------------------------------------------- Return Status for Payment Page --------------------------------------------------

newtype ExitResponse =
  ExitResponse
    { requestId :: String
    , payload :: ReturnStatus
    , service :: String
    , error :: Boolean
    , errorMessage :: String
    , errorCode  :: String
    }

newtype ReturnStatus =
  ReturnStatus
    { status :: Maybe String
    , message :: Maybe String
    , paymentInstrument :: Maybe String
    , paymentInstrumentGroup :: Maybe String
    , action :: String
    }

derive instance exitResponseGeneric :: Generic ExitResponse _
derive instance exitResponseNewtype :: Newtype ExitResponse _

instance encodeExitResponse :: Encode ExitResponse where encode = defaultEncode

derive instance returnStatusGeneric :: Generic ReturnStatus _
derive instance returnStatusNewtype :: Newtype ReturnStatus _

instance encodeReturnStatus :: Encode ReturnStatus where encode = defaultEncode


getExitResponse
  :: String
  -> Boolean
  -> String
  -> Maybe String
  -> Maybe String
  -> Maybe String
  -> Maybe String
  -> String
  -> ExitResponse
getExitResponse requestId hasFailed action status msg pi pig errorCode =
  ExitResponse
    { requestId : requestId
    , payload : if hasFailed then emptyPayload else returnPayload
    , service : getValueFromPayload' "service"
    , error : hasFailed
    , errorMessage : if hasFailed then "Unable to process" else ""
    , errorCode : errorCode
    }

  where
    status' = case _ of
                Just s  -> if s == "" then Just "api_failure" else status
                Nothing -> Just "api_failure"
    returnPayload = ReturnStatus
                      { status : status' status
                      , message : msg
                      , action
                      , paymentInstrument : pi
                      , paymentInstrumentGroup : pig
                      }

    emptyPayload = ReturnStatus
                      { status : Nothing
                      , message : Nothing
                      , action : ""
                      , paymentInstrument : Nothing
                      , paymentInstrumentGroup : Nothing
                      }
-------------------------------------------------- PaymentPageConfig --------------------------------------------------

newtype PPConfigReq
  = PPConfigReq {}
    -- { merchantId :: String
    -- , environment :: String
    -- }

newtype ButtonGradient = ButtonGradient {
  gradtype :: String,
  angle :: String,
  values ::Array String
}

newtype AddCardConfig = AddCardConfig {
  cardInBoxIcon :: String
  , cardInputWidth :: String
  , checkBoxMessage :: String
  , showSecurityIcons :: String
}

derive instance addCardConfigGeneric :: Generic AddCardConfig  _
derive instance addCardConfigNewtype :: Newtype AddCardConfig _

instance addCardConfigDecode :: Decode AddCardConfig where decode = defaultDecode
instance addCardConfigEncode :: Encode AddCardConfig where encode = defaultEncode

newtype PPBorderContainer = PPBorderContainer {
  paddingTop :: String
  , paddingBottom :: String
  , paddingHorizontal :: String
  , backgroundColor :: String
}

derive instance pPBorderContainerGeneric :: Generic PPBorderContainer  _
derive instance pPBorderContainerNewtype :: Newtype PPBorderContainer _
instance pPBorderContainerDecode :: Decode PPBorderContainer where decode = defaultDecode
instance pPBorderContainerEncode :: Encode PPBorderContainer where encode = defaultEncode

newtype PaymentOptionError = PaymentOptionError {
  paymentMethod :: String
  , errorMessage :: String
}

derive instance paymentOptionErrorGeneric :: Generic PaymentOptionError  _
derive instance paymentOptionErrorNewtype :: Newtype PaymentOptionError _
instance paymentOptionErrorDecode :: Decode PaymentOptionError where decode = defaultDecode
instance paymentOptionErrorEncode :: Encode PaymentOptionError where encode = defaultEncode

newtype PaymentOption =
  PaymentOption {
    primary :: FontConfig
    , secondary :: FontConfig
    , tertiary :: FontConfig
    , border :: String
    , paddingHorizontal :: String
    , paddingVertical :: String
    , paddingSecondaryTop :: String
    , paddingTertiaryTop :: String
    , paddingBottom :: String
    , paddingOuterVertical :: String
    , paddingOuterHorizontal :: String
    , shadow :: String
}

derive instance paymentOptionGeneric :: Generic PaymentOption  _
derive instance paymentOptionNewtype :: Newtype PaymentOption _
instance paymentOptionDecode :: Decode PaymentOption where decode = defaultDecode
instance paymentOptionEncode :: Encode PaymentOption where encode = defaultEncode

newtype FontConfig =
  FontConfig {
    fontColor :: String
    , fontSize :: String
    , fontWeight :: String
    , letterSpacing :: String
}

derive instance fontConfigGeneric :: Generic FontConfig  _
derive instance fontConfigNewtype :: Newtype FontConfig _
instance fontConfigDecode :: Decode FontConfig where decode = defaultDecode
instance fontConfigEncode :: Encode FontConfig where encode = defaultEncode

newtype OutageConfig = OutageConfig {
  outageType :: String --warn, restrict, none
  , message :: String
}

derive instance outageConfigGeneric :: Generic OutageConfig  _
derive instance outageConfigNewtype :: Newtype OutageConfig _
instance outageConfigDecode :: Decode OutageConfig where decode = defaultDecode
instance outageConfigEncode :: Encode OutageConfig where encode = defaultEncode

newtype Wrapper = Wrapper {
  wrapperPadding :: WrapperPadding
}

newtype WrapperPadding = WrapperPadding {
  wrapperPaddingTop :: String
  , wrapperPaddingBottom :: String
  ,wrapperPaddingHorizontal :: String
}

derive instance wrapperGeneric :: Generic Wrapper  _
derive instance wrapperNewtype :: Newtype Wrapper _
instance wrapperDecode :: Decode Wrapper where decode = defaultDecode
instance wrapperEncode :: Encode Wrapper where encode = defaultEncode

derive instance wrapperPaddingGeneric :: Generic WrapperPadding  _
derive instance wrapperPaddingNewtype :: Newtype WrapperPadding _
instance wrapperPaddingDecode :: Decode WrapperPadding where decode = defaultDecode
instance wrapperPaddingEncode :: Encode WrapperPadding where encode = defaultEncode
newtype Button = Button {
  colorActive :: String
  , alphaActive :: String
  , colorInactive :: String
  , alphaInactive :: String
  , width :: String
  , height :: String
  , cornerRadius :: String
  , atBottom :: String
  , translation :: String
  , text :: Maybe String
  , color :: String
  , background :: String
  , changeText :: String -- new added
}

derive instance buttonGeneric :: Generic Button  _
derive instance buttonNewtype :: Newtype Button _

newtype SideBar = SideBar {
    selectedColor :: String
    , notSelectedColor :: String
    , iconVisibility ::String
    , selectedFontColor :: String
    , notSelectedFontColor :: String
    , tabColor :: String
    , fontSize :: String
    , tabHeight :: String
    , width :: String
    , border :: String
    , tabBorder :: String
    , tabPaddingHorizontalOuter :: String
    , tabPaddingVerticalOuter :: String
    , selectedFontWeight :: String
    , notSelectedFontWeight :: String
    , tabCornerRadius ::  String
}

derive instance sideBarGeneric :: Generic SideBar  _
derive instance sideBarNewtype :: Newtype SideBar _
instance sideBarDecode :: Decode SideBar where decode = defaultDecode
instance sideBarEncode :: Encode SideBar where encode = defaultEncode



newtype TextBox = TextBox {
  activeColor :: String
  , inactiveColor :: String
  , errorColor :: String
  , borderWidth :: String
  , maxWidth :: String
  , height :: String
}

derive instance textBoxGeneric :: Generic TextBox  _
derive instance textBoxNewtype :: Newtype TextBox _
instance textBoxDecode :: Decode TextBox where decode = defaultDecode
instance textBoxEncode :: Encode TextBox where encode = defaultEncode

derive instance ppConfigReqGeneric :: Generic PPConfigReq _

instance encodePPConfigReq :: Encode PPConfigReq where encode = defaultEncode

derive instance topbarGeneric :: Generic TopBar  _
derive instance topbarNewtype :: Newtype TopBar _
instance topbarDecode :: Decode TopBar where decode = defaultDecode
instance topbarEncode :: Encode TopBar where encode = defaultEncode

derive instance buttonGradientGeneric :: Generic ButtonGradient  _
derive instance buttonGradientNewtype :: Newtype ButtonGradient _
instance buttonGradientDecode :: Decode ButtonGradient where decode = defaultDecode
instance buttonGradientEncode :: Encode ButtonGradient where encode = defaultEncode


jsonUrl :: URL
-- jsonUrl = "https://euler.sandbox.juspay.in/ec/v1/paymentPage/config/list"
jsonUrl = "https://hyperwidget-ppconfig.herokuapp.com"

instance callPPConfig :: RestEndpoint PPConfigReq ConfigPayload where
  makeRequest reqBody headers = defaultMakeRequest GET jsonUrl headers reqBody
  decodeResponse body = defaultDecodeResponse body

-------------------------------------------------- Outage API --------------------------------------------------

newtype OutageReq =
  OutageReq
    { client_auth_token :: String
    , merchant_id :: String
    , customer_id :: String
    }



newtype OutageResp = OutageResp {outages :: Array OutagesStatus}

derive instance outageReqGeneric :: Generic OutageReq _
derive instance outageRespGeneric :: Generic OutageResp _
derive instance outageRespNew :: Newtype OutageResp _

instance encodeOutageReq :: Encode OutageReq where encode = defaultEncode
instance decodeOutageResp :: Decode OutageResp where decode = defaultDecode

instance callOutage :: RestEndpoint OutageReq OutageResp where
  makeRequest (OutageReq reqBody) headers = urlEncodedMakeRequest POST (baseUrl <> "/ecr/analytics/outage") headers (OutageReq $ reqBody)
  decodeResponse body = defaultDecodeResponse body
