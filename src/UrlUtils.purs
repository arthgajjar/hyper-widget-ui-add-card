module UrlUtils where

import HyperPrelude.External(Unit)

foreign import pushHistory :: String -> String
foreign import popHistory :: Unit -> String
