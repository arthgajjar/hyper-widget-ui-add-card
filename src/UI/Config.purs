module UI.Config where

import Prelude

import Data.Int as IntUtils
import Data.Maybe (Maybe(..),fromMaybe)
import Data.Newtype (unwrap)
import Data.Number (fromString) as NumUtils
import Data.Number.Format (toString)
import Data.String (toLower)
import JBridge as JB
import PPConfig.Utils as CPUtils
import PaymentPageConfig as PPConfig
import PrestoDOM (Gradient(..), InputType(..), Length(..), Margin(..), Padding(..), Visibility(..))
import PrestoDOM.Types.DomAttributes (Corners(..))
import Remote.Types as RTypes
import Payments.Core.Commons (checkoutDetails)
import UI.Components.AddCard.Config as AddCard
import UI.Components.AmountBar.Config as AmountBar
import UI.Components.EditText.Config as EditText
import UI.Components.ListItem.Config as ListItemConfig
import UI.Components.Message.Config as MessageConfig
import UI.Components.PaymentOptionsConfig as PaymentOptionsConfig
import UI.Components.Popup.Config as Popup
import UI.Components.PrimaryButton.Config as PrimaryButton
import UI.Components.SearchBox.Config as SearchBox
import UI.Components.ToolBar.Config as ToolBar
import UI.Constant.Str.Default as STR
import UI.Utils (boolToVisibility)
import UI.Utils as UIUtils
import Remote.Types (ConfigPayload(..))
import Remote.Types
import Payments.Core.Commons (Checkout)

-- alias
fm = fromMaybe

-- vertical button margin
vMargin = 10

guestUserAllowed :: ConfigPayload -> String
guestUserAllowed configPayload = (PPConfig.getGuestUserAllowed configPayload)

ifGuestUserAllowed :: String -> Checkout -> Boolean
ifGuestUserAllowed guestUserAllowed checkout=
    guestUserAllowed == "true" && checkout.customer_id == ""

-- TODO :: Gradient to be implemented later on
primaryButtonConfig :: RTypes.ConfigPayload -> PrimaryButton.Config
primaryButtonConfig cP@(RTypes.ConfigPayload configPayload) = let
  atBottom = CPUtils.isBtnAtBottom cP
  RTypes.Button buttonConfig = configPayload.button
  PrimaryButton.Config config = PrimaryButton.defConfig
  hSpace = CPUtils.horizontalSpace cP
  uiPadding = CPUtils.uiCardHorizontalPadding cP
  tM = UIUtils.translationMargin
  uiCardHasShadow = CPUtils.uiCardTranslation cP == 0.0
  btnMargin =
    if hSpace == 0
      then (Margin uiPadding vMargin uiPadding vMargin)
      else
        if atBottom then (Margin hSpace vMargin hSpace vMargin)
        else
          if uiCardHasShadow then (Margin tM vMargin tM vMargin )
          else (Margin 0 vMargin 0 vMargin )

  primaryBtnConfig =
    config
      { text = fm "Pay" buttonConfig.text
      , background = buttonConfig.color
      , height = V 50
      , width = MATCH_PARENT
      , margin = btnMargin
      , textSize = CPUtils.fontSizeLarge cP
      , font = CPUtils.fontBold cP
      , cornerRadius = CPUtils.btnCornerRadius cP
      , translation = CPUtils.btnTranslation cP
      , animationColor = CPUtils.getAnimationColor cP
      }
  in PrimaryButton.Config primaryBtnConfig



isGuestUser :: Boolean
isGuestUser = checkoutDetails.customer_id == ""

editTextConfig :: RTypes.ConfigPayload -> EditText.Config
editTextConfig cP@(RTypes.ConfigPayload configPayload) =
  let
    EditText.Config config = EditText.defConfig
    RTypes.InputField inputField = configPayload.inputField
    hPadding = CPUtils.editTextPadding cP
    isMaterialView = CPUtils.isMaterialInputField cP
    editTextConfig =
      config
        { stroke =
            case (toLower inputField.type), config.focus of
              "boxed", true  -> "1," <> inputField.focusColor
              "boxed", false -> "1,#aaaaaa"
              _      , _     -> ""
        , cornerRadius =
            if (toLower inputField.type) == "boxed"
              then CPUtils.editTextCornerRadius cP
              else 0.0
        -- focus color
        , editTextPadding = Padding hPadding 0 hPadding 0
        , lineSeparatorHeight = 1
        , hint = STR.getNumberHint $ fromMaybe "" configPayload.language
        , hintColor = "#54232323"
        , margin = (Margin 0 0 0 0)
        , useMaterialView = isMaterialView
        , headerTextFont = CPUtils.fontRegular cP
        , inputType = TypeText
        , visibility = VISIBLE
        , iconUrl = ""
        , iconText = "CHANGE"
        , font = CPUtils.fontBold cP
        , editTextFont = CPUtils.getInputFieldFontStyle cP
        , textSize = (CPUtils.fontSizeLarge cP)
        , iconWidth = V (CPUtils.iconSizeLarge cP)
        , iconHeight = V (CPUtils.iconSizeLarge cP)
        , iconTextColor = CPUtils.primaryColor cP
        , textColor = CPUtils.fontColor cP
        , background = CPUtils.inputAreaBackground cP
        , errorTextSize = CPUtils.fontSizeSmall cP
        , errorTextFont = CPUtils.editTextErrorFont cP
        , errorTextColor = CPUtils.editTextErrorColor cP
        , hideHint = CPUtils.editTextHideHint cP
        , lineSeparatorFocusedColor = CPUtils.editTextLineFocused cP
        , lineSeparatorColor = CPUtils.editTextLineUnfocused cP
        }
  in
    (EditText.Config editTextConfig)

appBarConfig :: RTypes.ConfigPayload -> RTypes.Toolbar
appBarConfig (RTypes.ConfigPayload configPayload) =
  let
    RTypes.Toolbar appBarConfig = configPayload.toolbar
  in
    RTypes.Toolbar appBarConfig

-- is Long, but worth it.
addCardConfig :: RTypes.ConfigPayload -> AddCard.Config
addCardConfig configPayload@(RTypes.ConfigPayload conf) =
  let
    language = fromMaybe "" (configPayload # unwrap # _.language)
    RTypes.Button buttonConfig = conf.button
    atBottom = CPUtils.isBtnAtBottom configPayload
    AddCard.Config defaultConfig = AddCard.defConfig
    hSpace = CPUtils.horizontalSpace configPayload
    vSpace = CPUtils.verticalSpace configPayload
    uiPadding = CPUtils.uiCardHorizontalPadding configPayload
    RTypes.InputField inputFieldConfig = conf.inputField
    labelVisibility = boolToVisibility $ not CPUtils.isMaterialInputField configPayload
    btnPadding =
      if hSpace == 0
        then (Padding uiPadding vMargin uiPadding vMargin)
        else
          if atBottom then (Padding hSpace vMargin hSpace vMargin)
          else (Padding hSpace vMargin hSpace vMargin )

    updatedConfig =
      defaultConfig
        { cardPadding = UIUtils.uiCardPadding configPayload
        , cardMargin = (Margin hSpace vSpace hSpace vSpace)
        , cardCornerRadius = if hSpace == 0 then 0.0 else CPUtils.uiCardCornerRadius configPayload
        , cardTranslation = if hSpace == 0 then 0.0 else CPUtils.uiCardTranslation configPayload
        , cardColor = CPUtils.uiCardColor configPayload
        , buttonAtBottom = (not CPUtils.ifModalView configPayload && CPUtils.isBtnAtBottom configPayload )
        , labelFont = CPUtils.inputFieldlabelFontFace configPayload
        , labelSize = CPUtils.inputFieldlabelFontSize configPayload
        , labelVisibility = labelVisibility
        , labelColor = CPUtils.labelFontColor configPayload
        , inputTextFont = CPUtils.fontSemiBold configPayload -- TODO :: add util for this
        , inputFocusStrokeColor = "1," <> (inputFieldConfig.focusColor)
        , inputOutOfFocusStrokeColor = "1,#aaaaaa"
        , inputTextMargin = (Margin 12 0 12 0)
        , inputFieldMargin = (MarginBottom 12)
        , saveCardTextSize = CPUtils.fontSizeSmall configPayload
        , saveCardFontStyle = CPUtils.fontSemiBold configPayload
        , checkboxSize = (V 18)
        , infoIconSize = (V 16)
        , infoTextSize = CPUtils.fontSizeSmall configPayload
        , infoFontStyle = CPUtils.fontRegular configPayload
        , buttonCornerRadius = (fm 0.0 (NumUtils.fromString buttonConfig.cornerRadius))
        , buttonTextSize = CPUtils.fontSizeLarge configPayload
        , buttonColor = buttonConfig.color
        , buttonBackground = buttonConfig.background
        , buttonTextColor = "#ffffff"
        , buttonPadding = btnPadding
        , buttonFontStyle = CPUtils.fontBold configPayload
        , buttonTranslation = CPUtils.btnTranslation configPayload
        , useButtonGradient = true --buttonConfig.useGradient == "true"
        , buttonGradient = (Linear 20.0 [ "#ED1B72", "#FF4067" ]) -- get it from buttonConfig when graidnent is implemented
        , cardNumberLabel = STR.getCardNumberText language
        , expiryDateLabel = STR.getExpiryText language
        , cvvLabel = STR.getCvvText language
        , saveCardText = STR.getCheckboxMsg language
        , saveCardToolTipMsg = STR.getSaveCardInfoText language
        , useInputStroke = (toLower inputFieldConfig.type == "boxed")
        }
  in
    (AddCard.Config updatedConfig)




stockPrimaryButton :: ConfigPayload -> PrimaryButton.Config
stockPrimaryButton (ConfigPayload configPayload) = let
    PrimaryButton.Config config = PrimaryButton.defConfig
    lprimaryButton = config
      -- { text = STR.getSendOTP
      -- , background = CPUtils.primaryColor (ConfigPayload configPayload)
      -- , height = V 40
      -- , margin = (Margin 16 10 16 16)
      -- , textSize = (CPUtils.fontSizeLarge (ConfigPayload configPayload))
      -- , font = CPUtils.fontBold (ConfigPayload configPayload)
      -- , cornerRadius = getButtonRadius (ConfigPayload configPayload)
      -- --, secondaryTextVisibility = GONE
      -- --, secondaryText = STR.paymentRs200
      -- --, secondaryTextSize = CPUtils.fontSizeSmall (ConfigPayload configPayload)
      -- --, secondaryFont = CPUtils.fontRegular (ConfigPayload configPayload)
      -- -- , fontStyle = "Roboto-Bold"
      -- }
    in (PrimaryButton.Config lprimaryButton)



getButtonRadius :: ConfigPayload -> Number
getButtonRadius (ConfigPayload configPayload) =
    fromMaybe 0.0 $  NumUtils.fromString button.cornerRadius
    where
        (Button button) = configPayload.button

toolBarConfig :: RTypes.ConfigPayload -> ToolBar.Config
toolBarConfig cP@(RTypes.ConfigPayload configPayload) =
  let
    ToolBar.Config config = ToolBar.defConfig

    RTypes.Toolbar barConfig = appBarConfig (RTypes.ConfigPayload configPayload)

    toolConfig = config
        -- { background = barConfig.color
        -- , textColor = barConfig.textColor
        -- , imageUrl =
        --   if (toLower barConfig.back) == "visible" then
        --     "toolbar_back_arrow"
        --   else
        --     ""
        -- , backIconVisibility =
        --   if (toLower barConfig.back) == "visible" then
        --     VISIBLE
        --   else
        --     GONE
        -- , text = barConfig.pageTitle
        -- , font = fontFam barConfig.textFontFace
        -- , textSize = ((CPUtils.fontSizeVeryLarge (RTypes.ConfigPayload configPayload)) + 2)
        -- , contentMargin = contentMargin barConfig
        -- , useBackgroundImage = (toLower barConfig.useImageBackground == "true")
        -- , backgroundImage = "toolbar_background"
        -- , translation = (fm 0.0 (NumUtils.fromString barConfig.translation))
        -- , height = V $ fm 0 $ IntUtils.fromString barConfig.height
        -- , backIconSize = V 16
        -- }
  in
    (ToolBar.Config toolConfig)
  where
  contentMargin cnf =
    if toLower cnf.drawFromStatusBar == "true" then
      (Margin space statusBarHeight 0 0)
    else
      (Margin space 0 0 0)
  space = 16
  statusBarHeight = JB.getStatusBarHeight unit
  fontFam family = CPUtils.mapToFontFamily family cP


amountBarConfig :: RTypes.ConfigPayload -> Number -> String -> String -> String -> AmountBar.Config
amountBarConfig cP@(RTypes.ConfigPayload cPayload) amount phoneNumber orderDesc customerName =
  let
    hspace = CPUtils.horizontalSpace cP
    uiPadding = CPUtils.uiCardHorizontalPadding cP
    vPadding = CPUtils.amountBarVerticalPadding cP
    tM = if CPUtils.uiCardTranslation cP == 0.0 then 0 else UIUtils.translationMargin
    AmountBar.Config config = AmountBar.defConfig
    RTypes.AmountBar amountBar = cPayload.amountBar
    aConfig = config
      { padding =
            if not CPUtils.attachAmountBarAtTop cP then Padding 0 0 0 0
            else if hspace == 0 then (Padding uiPadding vPadding uiPadding vPadding)
            else (Padding (tM + hspace) vPadding (tM + hspace) vPadding)
      , lineStrings = generateAmountBarLines cP amount phoneNumber orderDesc customerName amountBar.amountBarData
      , rightTextVisibility = if toLower amountBar.placeAmountAtRight == "true" then VISIBLE else GONE
      , rightTextSize = fm 0 $ IntUtils.fromString amountBar.amountTextSize
      , rightText = "₹"<> (toString amount)
      , translation = CPUtils.amountBarTranslation cP
      , visibility = if CPUtils.isAmountBarVisible cP then VISIBLE else GONE
      , background = amountBar.color
      , dividerColor = amountBar.dividerColor
      , dividerVisibility = if toLower amountBar.addDivider == "true" then VISIBLE else GONE
      }
  in
    AmountBar.Config aConfig

generateAmountBarLines :: RTypes.ConfigPayload -> Number -> String -> String -> String -> Array RTypes.AmountBarLine -> Array AmountBar.LineItemData
generateAmountBarLines cP amount phoneNumber orderDesc customerName arr =
  map
    (\(RTypes.AmountBarLine line) ->
        { leftText : textMap $ fm "" line.leftString
        , centerText : textMap $ fm "" line.centerString
        , rightText : textMap $ fm "" line.rightString
        , font : fontFam line.textFontFace
        , textSize : fm 0 $ IntUtils.fromString line.fontSize
        , textColor :  line.color
        }
    )
  arr

  where
  textMap str =
      case toLower str of
          "customername" -> customerName
          "amount" -> "₹" <> toString amount
          "phonenumber" -> phoneNumber
          "orderdescription" -> orderDesc
          a -> str

  fontFam family = CPUtils.mapToFontFamily family cP

paymentCardConfig :: RTypes.ConfigPayload -> Boolean -> Boolean -> PaymentOptionsConfig.Config
paymentCardConfig configPayload hideDivider isDown =
  let
    PaymentOptionsConfig.Config config = PaymentOptionsConfig.defConfig configPayload

    MessageConfig.Config defConf = config.outageMessageConfig
    messageConf = defConf
      { visibility = if isDown then VISIBLE else GONE }

    pCardConfig =
      config
        { logoUrl = "ic_card"
        , primaryText = STR.getCardOpt $ fromMaybe "" ((configPayload # unwrap # _.language) :: Maybe String)
        , radioButtonVisibility = VISIBLE
        , radioButtonIconUrl = "right_arrow"
        , inputAreaVisibility = GONE
        , secondaryTextVisibility = GONE
        , ifClickable = not isDown
        , outageMessageConfig = MessageConfig.Config messageConf
        , lineSeparatorVisibility =
          if hideDivider then
            GONE
          else
            if CPUtils.ifSeperatedSections configPayload then
              GONE
            else
              VISIBLE
        }
  in
    (PaymentOptionsConfig.Config pCardConfig)

upiCardConfig :: RTypes.ConfigPayload -> Boolean -> Boolean -> PaymentOptionsConfig.Config
upiCardConfig configPayload hideDivider isDown =
  let
    PaymentOptionsConfig.Config config = PaymentOptionsConfig.defConfig configPayload
    MessageConfig.Config defConf = config.outageMessageConfig
    messageConf = defConf
      { visibility = UIUtils.boolToVisibility isDown }

    upiConfig =
      config
        { logoUrl = "ic_upi_icon"
        , primaryText = STR.getUpi $ fromMaybe "" ((configPayload # unwrap # _.language) :: Maybe String)
        , radioButtonVisibility = VISIBLE
        , radioButtonIconUrl = "right_arrow"
        , inputAreaVisibility = GONE
        , secondaryTextVisibility = GONE
        , ifClickable = not (isDown && CPUtils.restrictOutagePayment configPayload )
        , outageMessageConfig = MessageConfig.Config messageConf
        , lineSeparatorVisibility =
          if hideDivider then GONE
          else UIUtils.boolToVisibility $ not CPUtils.ifSeperatedSections configPayload

        }
  in
    (PaymentOptionsConfig.Config upiConfig)

emiCardConfig :: RTypes.ConfigPayload -> Boolean -> PaymentOptionsConfig.Config
emiCardConfig configPayload hideDivider = let
  PaymentOptionsConfig.Config config = PaymentOptionsConfig.defConfig configPayload
  emiConfig =
    config
      { logoUrl = "ic_emi"
      , primaryText = STR.getEMI $ fromMaybe "" ((configPayload # unwrap # _.language) :: Maybe String)
      , radioButtonVisibility = VISIBLE
      , radioButtonIconUrl = "right_arrow"
      , inputAreaVisibility = GONE
      , secondaryTextVisibility = GONE
      , ifClickable = true
      , lineSeparatorVisibility =
        if hideDivider then GONE
        else UIUtils.boolToVisibility $ not CPUtils.ifSeperatedSections configPayload
      }
  in PaymentOptionsConfig.Config emiConfig

mealCardConfig :: RTypes.ConfigPayload -> Boolean -> PaymentOptionsConfig.Config
mealCardConfig configPayload hideDivider =
  let
    PaymentOptionsConfig.Config config = PaymentOptionsConfig.defConfig configPayload

    upiConfig =
      config
        { logoUrl = "bb_meal_card"
        , primaryText = "Meal Cards"
        , radioButtonVisibility = VISIBLE
        , radioButtonIconUrl = "right_arrow"
        , inputAreaVisibility = GONE
        , secondaryTextVisibility = GONE
        , ifClickable = true
        , lineSeparatorVisibility =
          if hideDivider then
            GONE
          else
            if CPUtils.ifSeperatedSections configPayload then
              GONE
            else
              VISIBLE
        }
  in
    (PaymentOptionsConfig.Config upiConfig)

payLaterCardConfig :: RTypes.ConfigPayload -> Boolean -> PaymentOptionsConfig.Config
payLaterCardConfig configPayload hideDivider =
  let
    PaymentOptionsConfig.Config config = PaymentOptionsConfig.defConfig configPayload
    payLaterConfig =
      config
        { logoUrl = "pay_later_icon"
        , primaryText = "Pay Later"
        , radioButtonVisibility = VISIBLE
        , radioButtonIconUrl = "right_arrow"
        , inputAreaVisibility = GONE
        , secondaryTextVisibility = GONE
        , ifClickable = true
        , lineSeparatorVisibility =
          if hideDivider then
            GONE
          else
            if CPUtils.ifSeperatedSections configPayload then
              GONE
            else
              VISIBLE
        }
  in
    (PaymentOptionsConfig.Config payLaterConfig)

walletCardConfig :: RTypes.ConfigPayload -> Boolean -> Boolean -> PaymentOptionsConfig.Config
walletCardConfig configPayload hideDivider isDown =
  let
    PaymentOptionsConfig.Config config = PaymentOptionsConfig.defConfig configPayload

    MessageConfig.Config defConf = config.outageMessageConfig
    messageConf = defConf
      { visibility = if isDown then VISIBLE else GONE }

    walletConfig =
      config
        { logoUrl = "wallet_icon"
        , primaryText = STR.getWallet $ fromMaybe "" ((configPayload # unwrap # _.language) :: Maybe String)
        , radioButtonVisibility = VISIBLE
        , radioButtonIconUrl = "right_arrow"
        , inputAreaVisibility = GONE
        , secondaryTextVisibility = GONE
        , ifClickable = not isDown
        , outageMessageConfig = MessageConfig.Config messageConf
        , lineSeparatorVisibility =
          if hideDivider then
            GONE
          else
            if CPUtils.ifSeperatedSections configPayload then
              GONE
            else
              VISIBLE
        }
  in
    (PaymentOptionsConfig.Config walletConfig)

netBankingCardConfig :: RTypes.ConfigPayload -> Boolean -> Boolean -> PaymentOptionsConfig.Config
netBankingCardConfig configPayload hideDivider isDown =
  let
    PaymentOptionsConfig.Config config = PaymentOptionsConfig.defConfig configPayload

    MessageConfig.Config defConf = config.outageMessageConfig
    messageConf = defConf
      { visibility = if isDown then VISIBLE else GONE }

    nbCardConfig =
      config
        { logoUrl = "net_banking_icon"
        , primaryText = "Net Banking" --STR.getNetbanking
        , radioButtonVisibility = VISIBLE
        , radioButtonIconUrl = "right_arrow"
        , inputAreaVisibility = GONE
        , secondaryTextVisibility = GONE
        , ifClickable = not isDown
        , outageMessageConfig = MessageConfig.Config messageConf
        , lineSeparatorVisibility =
          if hideDivider then
            GONE
          else
            if CPUtils.ifSeperatedSections configPayload then
              GONE
            else
              VISIBLE
        }
  in
    (PaymentOptionsConfig.Config nbCardConfig)

searchBoxConfig :: RTypes.ConfigPayload -> SearchBox.Config
searchBoxConfig cP@(RTypes.ConfigPayload configPayload) =
  let
    SearchBox.Config config = SearchBox.defConfig
    RTypes.InputField inputFieldConfig = configPayload.inputField
    hSpace = CPUtils.horizontalSpace cP
    uiCardHSpace = CPUtils.uiCardHorizontalPadding cP
    uiCardTranslation = CPUtils.uiCardTranslation cP
    hPadding = CPUtils.editTextPadding cP
    tM = if CPUtils.uiCardTranslation cP == 0.0 then 0 else UIUtils.translationMargin
    searchConfig =
      config
        { height = (V 40)
        , hint = STR.getSearchHint $ fromMaybe "" configPayload.language
        , searchIconVisibility = VISIBLE
        , tickVisibility = GONE
        , lineVisibility = GONE
        , textSize = CPUtils.fontSizeLarge cP
        , font = CPUtils.fontRegular cP
        , searchIconSize = V 16
        , margin =
            if hSpace == 0
              then (Margin uiCardHSpace 0 uiCardHSpace 0)
              else (Margin tM 0 tM 0)
        , padding = Padding hPadding 0 hPadding 0
        , cancelVisibility = VISIBLE
        , cornerRadius =
            if (toLower inputFieldConfig.type) == "boxed"
              then CPUtils.editTextCornerRadius cP
              else 0.0
        , textColor = CPUtils.fontColor cP
        , background = CPUtils.inputAreaBackground cP
        }
  in (SearchBox.Config searchConfig)

genericCardConfig :: String -> String -> RTypes.ConfigPayload -> PaymentOptionsConfig.Config
genericCardConfig label icon (RTypes.ConfigPayload configPayload) =
  let
    PaymentOptionsConfig.Config config = PaymentOptionsConfig.defConfig (RTypes.ConfigPayload configPayload)

    genericCard =
      config
        { logoUrl = icon
        , primaryText = label
        , radioButtonVisibility = VISIBLE
        , radioButtonIconUrl = "right_arrow"
        , inputAreaVisibility = GONE
        , secondaryTextVisibility = GONE
        }
  in
    (PaymentOptionsConfig.Config genericCard)

genericToolBarConfig :: String -> String -> RTypes.ConfigPayload -> ToolBar.Config
genericToolBarConfig back title (RTypes.ConfigPayload configPayload) =
  let
    ToolBar.Config config = ToolBar.defConfig

    toolConfig =
      config
        { imageUrl = if (toLower back) == "visible" then "back_arrow_white" else ""
        , backIconVisibility = if (toLower back) == "visible" then VISIBLE else GONE
        , text = title
        , font = CPUtils.fontRegular (RTypes.ConfigPayload configPayload)
        , textSize = (CPUtils.fontSizeVeryLarge (RTypes.ConfigPayload configPayload))
        }
  in
    (ToolBar.Config toolConfig)

codConfig :: RTypes.ConfigPayload -> Boolean -> PaymentOptionsConfig.Config
codConfig configPayload hideDivider =
  let
    PaymentOptionsConfig.Config config = PaymentOptionsConfig.defConfig configPayload

    cardOnDelConfig =
      config
        { logoUrl = "ic_cod"
        , primaryText = STR.getCOD $ fromMaybe "" (configPayload # unwrap # _.language)
        , radioButtonVisibility = VISIBLE
        , radioButtonIconUrl = "tick"
        , ifClickable = true
        , cvvInputVisibility = GONE
        , secondaryTextVisibility = GONE
        , lineSeparatorVisibility =
          if hideDivider then
            GONE
          else
            if CPUtils.ifSeperatedSections configPayload then
              GONE
            else
              VISIBLE
        , inputAreaHeight = V 60
        }
  in
    (PaymentOptionsConfig.Config cardOnDelConfig)

askAFConfig :: RTypes.ConfigPayload -> Boolean -> PaymentOptionsConfig.Config
askAFConfig configPayload hideDivider =
  let
    PaymentOptionsConfig.Config config = PaymentOptionsConfig.defConfig configPayload

    askafConfig =
      config
        { logoUrl = "ic_cod"
        , primaryText = STR.getAAF $ fromMaybe "" (configPayload # unwrap # _.language)
        , radioButtonVisibility = VISIBLE
        , radioButtonIconUrl = "right_arrow"
        , inputAreaVisibility = GONE
        , secondaryTextVisibility = GONE
        , ifClickable = true
        , lineSeparatorVisibility =
          if hideDivider then
            GONE
          else
            if CPUtils.ifSeperatedSections configPayload then
              GONE
            else
              VISIBLE
        }
  in
    (PaymentOptionsConfig.Config askafConfig)

cardNumberConfig :: RTypes.ConfigPayload -> EditText.Config
cardNumberConfig cP@(RTypes.ConfigPayload configPayload) =
  let
    EditText.Config config = editTextConfig cP
    editTextConfig =
      config
        { hint = STR.getCardNumberHolder $ fromMaybe "" configPayload.language
        , separator = " "
        , separatorRepeat = "4"
        , inputType = Numeric
        , visibility = VISIBLE
        , iconWidth = V (CPUtils.iconSize (RTypes.ConfigPayload configPayload))
        , iconHeight = V (CPUtils.iconSize (RTypes.ConfigPayload configPayload))
        , iconTextSize = CPUtils.fontSizeVerySmall (RTypes.ConfigPayload configPayload)
        , pattern = "^([0-9]| )+$,24"
        }
  in
    (EditText.Config editTextConfig)

expirydateConfig :: RTypes.ConfigPayload -> EditText.Config
expirydateConfig cP@(RTypes.ConfigPayload configPayload) =
  let
    EditText.Config config = editTextConfig cP
    editTextConfig =
      config
        { hint = "MM / YY"
        , inputType = Numeric
        , visibility = VISIBLE
        , pattern = "^([0-9]|\\/)+$,5"
        , iconTextVisibility = GONE
        }
  in
    (EditText.Config editTextConfig)

cvvConfig :: RTypes.ConfigPayload -> EditText.Config
cvvConfig cP@(RTypes.ConfigPayload configPayload) =
  let
    EditText.Config config = editTextConfig cP
    editTextConfig =
      config
        { hint = STR.getSecurityCode $ fromMaybe "" configPayload.language
        , inputType = NumericPassword
        , visibility = VISIBLE
        , pattern = "^[0-9]+$,3"
        , iconTextVisibility = GONE
        , iconWidth = (V 16)
        , iconHeight = (V 16)
        , useToolTip = true
        }
  in
    (EditText.Config editTextConfig)

listItemConfig :: RTypes.ConfigPayload -> ListItemConfig.Config
listItemConfig configPayload =
  let
    ListItemConfig.Config defConf = ListItemConfig.defaultConfig

    updatedConfig =
      defConf
        { textFont = CPUtils.fontRegular configPayload
        , textSize = CPUtils.fontSize configPayload
        , textColor = CPUtils.fontColor configPayload
        , padding = (Padding 0 0 0 0)
        , margin = (Margin 0 0 0 0)
        , buttonTextSize = CPUtils.fontSize configPayload
        , buttonVisibility = VISIBLE
        , buttonColor = CPUtils.primaryColor configPayload
        , buttonFont = CPUtils.fontRegular configPayload
        , radioButtonSize = (V $ CPUtils.radioIconSize configPayload)
        }
  in
    ListItemConfig.Config updatedConfig

popupConfig :: RTypes.ConfigPayload -> Popup.Config
popupConfig configPayload@(RTypes.ConfigPayload cP) = let
  Popup.Config defConfig = Popup.defConfig
  hspace = CPUtils.horizontalSpace configPayload
  uiPadding = CPUtils.uiCardHorizontalPadding configPayload
  vMargin = 10
  btnMargin =
    if hspace == 0
      then (Margin uiPadding vMargin uiPadding vMargin)
      else (Margin hspace vMargin hspace vMargin)
  RTypes.Button buttonConfig = cP.button
  updatedConfig =
    defConfig
      { headerSize = (CPUtils.fontSizeVeryLarge configPayload)
      , headerPadding = (Padding 0 0 0 0)
      , headerMargin = (Margin 24 32 24 16)
      , headerImageSize = V 16
      , headerImageUrl = "cross"
      , headerFont = CPUtils.fontSemiBold configPayload
      , listItemFont = CPUtils.fontRegular configPayload
      , listItemTextSize = (CPUtils.fontSize configPayload)
      , contentMargin = (Margin 24 16 24 0)
      , listItemMargin = (Margin 0 0 0 10)
      , dividerVisibility = GONE
      , listItemColor = CPUtils.fontColor configPayload
      , buttonsViewMargin = btnMargin
      , buttonOneBackground = buttonConfig.color
      , buttonOneTextColor = "#ffffff"
      , buttonOneTextFont = CPUtils.fontBold configPayload
      , buttonOneTextSize = CPUtils.fontSizeLarge configPayload
      , buttonOneCornerRadius = CPUtils.btnCornerRadius configPayload
      , buttonOneTranslation = CPUtils.btnTranslation configPayload
      }
  in Popup.Config updatedConfig

--TODO
deletePopupConfig :: RTypes.ConfigPayload -> Popup.Config
deletePopupConfig configPayload = let
    Popup.Config defConfig = Popup.defConfig
    updatedConfig = defConfig
        { overlayBackground = "#AA000000"
        ,   margin = Margin 15 15 15 15
        ,   padding = Padding 15 15 15 15
        ,   corners = Corners 8.0 true true true true
        ,   headerSize = CPUtils.fontSizeVeryLarge configPayload
        ,   headerPadding = Padding 0 0 0 0
        ,   headerImageVisibility = GONE
        ,   headerFont = CPUtils.fontSemiBold configPayload
        ,   dividerVisibility = GONE
        ,   useList = false
        ,   contentTextSize = CPUtils.fontSize configPayload
        ,   contentTextFont = CPUtils.fontRegular configPayload
        ,   contentTextColor = CPUtils.fontColor configPayload
        ,   termsVisibility = GONE
        ,   checkBoxVisibility = GONE
        ,   buttonOneText = "Yes"
        ,   buttonTwoText = "No"
        ,   buttonOneBackground = CPUtils.primaryColor configPayload
        ,   buttonTwoBackground = "#ffffff"
        ,   buttonOneStroke = "0,#ffffff"
        ,   buttonTwoStroke = "2," <> CPUtils.primaryColor configPayload
        ,   buttonOneTextColor = "#ffffff"
        ,   buttonTwoTextColor = CPUtils.primaryColor configPayload
        ,   buttonOneTextFont = CPUtils.fontSemiBold configPayload
        ,   buttonTwoTextFont = CPUtils.fontSemiBold configPayload
        ,   buttonOneTextSize = CPUtils.fontSize configPayload
        ,   buttonTwoTextSize = CPUtils.fontSize configPayload
        }
    in Popup.Config updatedConfig

outageMessageConfig :: RTypes.ConfigPayload -> MessageConfig.Config
outageMessageConfig cP@(RTypes.ConfigPayload configPayload) = let
  MessageConfig.Config defaultConfig = MessageConfig.defConfig
  RTypes.OutageViewProps outageViewProps = configPayload.outageViewProps
  outageConfig =
    defaultConfig
      { imageUrl = ""
      , imageSize = V 16
      , imageMargin = (Margin 0 0 0 0)
      , imageVisibility = GONE
      , text = outageViewProps.outageMessage <> " this option"
      , textColor = outageViewProps.textColor
      , textFont = CPUtils.fontRegular cP
      , textMargin = (Margin 0 0 0 0)
      , textSize = CPUtils.outageRowTextSize cP
      , textFromHTML = false
      }
  in MessageConfig.Config outageConfig

offerMessageConfig :: String -> String -> RTypes.ConfigPayload -> MessageConfig.Config
offerMessageConfig offerText offerDesc cP@(RTypes.ConfigPayload configPayload) = let
  MessageConfig.Config defaultConfig = MessageConfig.defConfig
  offerMessageConfig =
    defaultConfig
      { background = CPUtils.offerLayoutColor cP
      , padding = CPUtils.offerLayoutPadding cP
      , cornerRadius = CPUtils.offerLayoutCornerRadius cP
      , imageUrl =  "ic_offer_icon"
      , imageSize = V 17
      , imageMargin = Margin 0 2 10 0
      , imageVisibility = VISIBLE
      , text = if offerDesc == "" then offerText else UIUtils.offerDescHTML offerText cP
      , textColor = CPUtils.offerTextColor cP
      , textFont = CPUtils.fontRegular cP
      , textMargin = (Margin 0 0 0 0)
      , textSize = CPUtils.fontSize cP
      , textFromHTML = offerDesc /= ""
      , visibility = boolToVisibility $ offerText /= ""
      , margin = (Margin 8 0 8 12)
      }
  in MessageConfig.Config offerMessageConfig
