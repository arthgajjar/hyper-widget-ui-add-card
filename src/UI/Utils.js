
exports['markDownToHTML'] = function(markdownText){
  try {
    const showdown = require('showdown');
    const converter = new showdown.Converter();
    return converter.makeHtml(markdownText);
  } catch (e){
    console.error("error in showdown convert: ", e);
    return markdownText;
  }
}

const state = {
  simpleCvv: "",
  setUpiPin: "",
  cardNumber: "",
  cardNumberHandler: null,
  cardNumberCallbackHandler: null,
  expiryNumber: "",
  expiryNumberHandler: null,
  expiryNumberCallbackHandler: null,
  screenHeight: null,
  prevTime: 0
};

exports["_cvvHandler"] = function (id) {
  return function(str) {
    return function (push) {
      return function () {
        if (state.simpleCvv === str) {
          return;
        }
        state.simpleCvv = str
        push(str)()
      }
    }
  }
}


const FRAME_DURATION = 16;

exports.getOs = function () {
  var userAgent = navigator.userAgent;
  if (!userAgent)
    return console.error(new Error("UserAgent is null"));
  if (userAgent.indexOf("Android") != -1 && userAgent.indexOf("Version") != -1)
    return "ANDROID";
  if (userAgent.indexOf("iPhone") != -1 && userAgent.indexOf("Version") == -1)
    return "IOS";
  return "WEB";
}

exports.getUPIId = function(){
  return window.__upi
}

exports.redirectUPIScreen= function(bool){
  return function(upi){
    if (bool){
      var baseLoc=window.location.href;
    var new_str = baseLoc.split("&widget_name=")[0];
    new_str+="&widget_name=upiWaitingScreen&upi_id="+upi
    parent.location.href = new_str;
    }
  }
}

exports.screenHeight = function () {
  if (state.screenHeight === null) {
    state.screenHeight = window.__HEIGHT;
    if (window.__OS == "ANDROID") {
      state.screenHeight = (parseInt(window.__HEIGHT)) / JBridge.getPixels() - parseInt( JBridge.getResourceByName("status_bar_height","dimen","android")) ;
    }
  }
  return state.screenHeight;
}

exports.statusBarHeight = function(x){
  if (window.__OS == "ANDROID")
    return parseInt( JBridge.getResourceByName("status_bar_height","dimen","android"));
  else
    return 0;
}

exports.date = function (x){
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
    if(dd<10) {
        dd = '0'+dd
    }
    if(mm<10) {
        mm = '0'+mm
    }
    today = dd + '-' + mm + '-' + yyyy;
    return today;
}

exports["_cvvHandler"] = function (id) {
  return function(str) {
    return function (push) {
      return function () {
        if (state.simpleCvv === str) {
          return;
        }
        state.simpleCvv = str
        push(str)()
      }
    }
  }
}

exports["_expiryHandler"] = function (id) {
  return function (str) {
    return function (push) {
      return function () {
        if (state.expiryNumberHandler != null) {
          clearTimeout(state.expiryNumberHandler);
        }
        if (window.__OS == "ANDROID")
        {
          state.expiryNumberHandler = setTimeout(expiryNumber.bind(this, id, str, push), FRAME_DURATION);
        }
        else
        {
          state.expiryNumberHandler = setTimeout(expiryNumberIOS.bind(this, id, str, push), FRAME_DURATION);
        }
      }
    }
  }
}

function expiryNumberIOS(id, str, push) {
  state.expiryNumberHandler = null;
  if (str === state.expiryNumber) {
    return;
  }
  var rawDate = "";
  for (var i = 0; i < str.length; i++) {
    if (str[i] >= '0' && str[i] <= '9') {
      rawDate += str[i];
      if (rawDate.length == 4){
        break;
      }
    }
  }

  var month = rawDate.substring(0, 2);
  var year = rawDate.substring(2,4);
  var formattedDate = "";

  if (month.length == 1 && month > 1) {
    month = "0" + month;
    formattedDate = month;
    formattedDate += year;
    setTextIOS(id, formattedDate);
  } else if (month.length == 2 && (month > 12 || month == 0)) {
    month = "12";
    formattedDate = month;
    formattedDate += year;
    setTextIOS(id, formattedDate);
  }
    formattedDate = month;
    formattedDate += "/";
    formattedDate += year;

  state.expiryNumber = formattedDate;
  if (state.expiryNumberCallbackHandler != null) {
    clearTimeout(state.expiryNumberCallbackHandler);
  }
  state.expiryNumberCallbackHandler = setTimeout(callbackHelper.bind(this, formattedDate, push), FRAME_DURATION);


}

function callbackHelper(str, fn) {
  state.cardNumberCallbackHandler = null;
  fn(str)();
}

function expiryNumber(id, str, push) {
  state.expiryNumberHandler = null;
  if (str === state.expiryNumber) {
    return;
  }
  var sExpiry = state.expiryNumber;
  var sLen = sExpiry.length;
  var len = str.length;
  var cursorPos = parseInt(JBridge.cursorPosition(parseInt(id)));

  // Trailing forward slash removed by the user
  if (sExpiry[cursorPos] == '/' && str[cursorPos] != '/') {
    state.expiryNumber = sExpiry.substring(0,sExpiry.length-2);
    setText(id, sExpiry.substring(0,sExpiry.length-2), --cursorPos);
    return;
  }

  var rawDate = "";
  for (var i = 0; i < str.length; i++) {
    if (str[i] >= '0' && str[i] <= '9') {
      rawDate += str[i];
      if (rawDate.length == 4){
        break;
      }
    }
  }

  var month = rawDate.substring(0, 2);
  var year = rawDate.substring(2,4);
  var formattedDate = "";

  if (month.length == 1 && month > 1) {
    month = "0" + month;
  } else if (month.length == 2 && (month > 12 || month == 0)) {
    month = "12";
  }
  formattedDate = month;

  if (formattedDate.length == 2) {
    if(cursorPos<3){
      cursorPos = 3;
    }
    formattedDate += '/';
  }

  formattedDate += year;

  state.expiryNumber = formattedDate;
  if (state.expiryNumberCallbackHandler != null) {
    clearTimeout(state.expiryNumberCallbackHandler);
  }
  state.expiryNumberCallbackHandler = setTimeout(callbackHelper.bind(this, formattedDate, push), FRAME_DURATION);

  if (str == formattedDate) {
    setText(id, formattedDate, cursorPos);
    return;
  }

  if (formattedDate[cursorPos] == '/') {
    cursorPos++;
  }
  setText(id, formattedDate, cursorPos);
}

exports ["setText'"] = function (id) {
  return function (text) {
    return function (){
        setText(id, text, text.length);
    }
  }
}

function setText(id, text, pos) {
  console.log("Recived Id is",id);
  if (__OS === "ANDROID") {
    var cmd = "set_view=ctx->findViewById:i_" + id + ";";
    cmd += "get_view->setText:cs_" + text + ";";
    cmd += "get_view->setSelection:i_" + pos + ";";
    Android.runInUI(cmd, null);
  } else {
    Android.runInUI({id: id, text: text});
    Android.runInUI({id: id, cursorPosition: pos});
  }
}

function setTextIOS(id, text) {
  console.log("Recived Id is",id);
  if (window.__OS === "IOS") {
    Android.runInUI({id: id, text: text});
  }
}

// exports.os = function(x){return window.__OS}

exports["screenWidth"] =
  function (x)
  {
    if(window.__OS == "ANDROID")
    {
      if(window.__android_screenWidth) {
        return window.__android_screenWidth
      } else {
        return window.__android_screenWidth = JSON.parse(Android.getScreenDimensions()).width / JBridge.getPixels();
      }
    }
    else
    {
      if (window.__ios_screenWidth) {
        return window.__ios_screenWidth;
      } else {
        return window.__ios_screenWidth = JSON.parse(Android.getScreenDimensions()).width;
      }
    }
  }

  exports["resetTextFields"]= function(arr) {
    //"987654321" ID Used for edittexts
    return function(){
      arr.map(function(a) {
        if (window._OS ==  "IOS")
        {
          setTextIOS(a, "");
        }
        else{
          setText(a, "", 0);
        }
      });
    }
  }

exports.loaderAfterRender = function(id) {
  if(window.__payload && window.__payload.customBrandingLayout && window.__OS=="ANDROID") {
    var cmd = "set_VIEW=ctx->findViewById:i_" + id + ";set_inflator=android.view.LayoutInflater->from:ctx_ctx;set_inflatedLayout=get_inflator->inflate:i_" + window.__payload.customBrandingLayout + ",null_null,b_false;get_VIEW->addView:get_inflatedLayout;"
    Android.runInUI(cmd, null);
    setTimeout(function() {
      if(typeof JBridge.runInJuspayBrowser == "function") {
        JBridge.runInJuspayBrowser("onStartWaitingDialogCreated", "" + id, "");
      }
    }, 200);
  }
}


exports.safeMarginTop = function () {
  if (top.__DEVICE_DETAILS && top.__DEVICE_DETAILS.safe_area_frame) {
    return top.__DEVICE_DETAILS.safe_area_frame.y
  }
  return 0;
}

exports.safeMarginBottom = function () {
  var d = top.__DEVICE_DETAILS;
  if (!d || !d.safe_area_frame) {
    return 0;
  }
  return (d.screen_height - d.safe_area_frame.height - d.safe_area_frame.y);
}

exports["getLoaderConfig"] = {
  customLoader: (window.__payload.customBrandingLayout != undefined) && (window.__OS == "ANDROID"),
  parentId: "14314314"
}

exports.resetListKeyBoard = function() {
  if(window.__OS == "ANDROID" && Android.runInUI)
  {
    Android.runInUI("set_window=ctx->getWindow;get_window->setSoftInputMode:i_16;",null);
  }
}
