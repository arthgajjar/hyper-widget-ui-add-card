module UI.Components.PrimaryButton.Config where

import HyperPrelude.Internal (Length(..), Margin(..), Typeface(..), Visibility(..))

data Config
  = Config
    { background :: String
    , height :: Length
    , width :: Length
    , text :: String
    , color :: String
    , cornerRadius :: Number
    , textSize :: Int
    , margin :: Margin
    , typeface :: Typeface
    , stroke :: String
    , font :: String
    , alpha :: Number
    , visible :: Visibility
    , animationColor :: String
    , startAnimation :: Boolean
    , translation :: Number
    , isClickable :: Boolean
    , useGradient :: Boolean
    }

defConfig :: Config
defConfig =
  Config
    { background: "#aaaaaa"
    , height: V 50
    , width: V 50
    , text: "PAY"
    , color: "#ffffff"
    , cornerRadius: 4.0
    , textSize: 18
    , margin: (Margin 4 4 4 4)
    , typeface: NORMAL
    , stroke: "0,#ffffff"
    , font: "Roboto-Bold" --"Roboto-Regular"
    , alpha: 1.0
    , visible: VISIBLE
    , animationColor: "#33000000"
    , startAnimation: false
    , translation: 0.0
    , isClickable: true
    , useGradient: false
    }
