module UI.Components.PrimaryButton.View where

import HyperPrelude.Internal (Gradient(..), Gravity(..), Length(..), Orientation(..), PrestoDOM, PropName(..), Visibility(..), alpha, background, clickable, color, cornerRadius, fontStyle, gradient, gravity, height, linearLayout, onClick, orientation, prop, relativeLayout, stroke, text, textSize, textView, translationZ, visibility, weight, width)
import HyperPrelude.External (Effect, Unit, const, negate, unit, ($), (+), (<>))

import PrestoDOM.Animation as PrestoAnim
import UI.Components.PrimaryButton.Config (Config(..))
import UI.Components.PrimaryButton.Controller (Action(..))
import UI.Utils as UiUtil

view :: ∀ w. (Action -> Effect Unit) -> Config -> PrestoDOM (Effect Unit) w
view push (Config config) =
  relativeLayout
    ( [ width MATCH_PARENT
      , height config.height
      , orientation VERTICAL
      --, margin config.margin
      , gravity CENTER
      , background config.background
      , cornerRadius config.cornerRadius
      , stroke config.stroke
      , onClick push $ const Clicked
      , visibility config.visible
      , alpha config.alpha
      , translationZ config.translation
      , clickable config.isClickable
      ]
        <> if config.useGradient then -- TODO :: To be exposed properly from config when gradient is added to Presto UI then
            [ gradient (Linear 20.0 [ "#ED1B72", "#FF4067" ])
            , prop (PropName "gradientAngle") "LEFT_RIGHT"
            ]
          else
            []
    )
    [ PrestoAnim.animationSet
        [ UiUtil.loaderAnim (-((UiUtil.screenWidth unit) + 16)) 0 5000 config.startAnimation ]
        $ linearLayout
            [ height MATCH_PARENT
            , width MATCH_PARENT
            , cornerRadius config.cornerRadius
            , gravity CENTER
            , background config.animationColor
            , visibility if config.startAnimation then VISIBLE else GONE
            ]
            []
    , linearLayout
        [ height MATCH_PARENT
        , width MATCH_PARENT
        , orientation HORIZONTAL
        ]
        [ textView
            [ weight 1.0
            , width MATCH_PARENT
            , gravity CENTER
            , text config.text
            , color config.color
            , textSize config.textSize
            , fontStyle config.font
            ]
        ]
    ]
