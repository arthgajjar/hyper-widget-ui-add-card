module UI.Components.Popup.View where

import HyperPrelude.Internal (Gravity(..), Length(..), Margin(..), Orientation(..), Padding(..), PrestoDOM, Visibility(..), background, clickable, color, cornerRadius, fontStyle, gravity, height, imageUrl, imageView, lineHeight, linearLayout, margin, onClick, orientation, padding, scrollBarY, scrollView, stroke, text, textFromHtml, textSize, textView, translationZ, visibility, weight, width)
import HyperPrelude.External (Effect, Maybe(..), Unit, const, map, ($), (<>), (==), (||))

import PrestoDOM.Properties (cornerRadii)
import UI.Components.Popup.Config (Config(..))
import UI.Components.Popup.Controller (Action(..))
import UI.Utils (multipleLine, isVisible, boolToVisibility)

view :: ∀ w. (Action  -> Effect Unit) -> Config -> Maybe (PrestoDOM (Effect Unit) w ) -> Maybe (PrestoDOM (Effect Unit) w -> PrestoDOM (Effect Unit) w) -> PrestoDOM (Effect Unit) w
view push config@(Config c) addedContent popupAnimation =
  linearLayout (
    [ height MATCH_PARENT
    , width MATCH_PARENT
    , orientation VERTICAL
    , clickable true
    , visibility if c.open then VISIBLE else GONE
    ] <>
    ( if isVisible c.overlayVisibility then
        [ background c.overlayBackground ]
        else [] ))
    [ overlayBackground push
    , case popupAnimation of
        Just anim -> anim (popupView push config addedContent)
        Nothing   -> popupView push config addedContent
    ]


popupView :: ∀ w. (Action  -> Effect Unit) -> Config -> Maybe (PrestoDOM (Effect Unit) w) -> PrestoDOM (Effect Unit) w
popupView push config@(Config cnf) addedContent =
  linearLayout
    [ width MATCH_PARENT
    , height WRAP_CONTENT
    , orientation VERTICAL
    , cornerRadii cnf.corners
    , background cnf.background
    --, margin cnf.margin
    , padding cnf.padding
    ](
    [ headerView push config
    , linearLayout
        [ width MATCH_PARENT
        , height $ V 1
        , background cnf.dividerColor
        , visibility cnf.dividerVisibility
        ] []
    ] <>
    ( case addedContent of
        Just c -> [c]
        _ -> []
    ) <>
    [ contentsView config
    , bottomView push config
    ])

headerView :: ∀ w. (Action  -> Effect Unit) -> Config -> PrestoDOM (Effect Unit) w
headerView push (Config config) =
  linearLayout
    [ width MATCH_PARENT
    , height WRAP_CONTENT
    , orientation HORIZONTAL
    , padding config.headerPadding
    , margin config.headerMargin
    , gravity CENTER
    , visibility config.headerVisibility
    ]
    [ textView
        [ text config.header
        , textSize config.headerSize
        , width WRAP_CONTENT
        , height WRAP_CONTENT
        , fontStyle config.headerFont
        , color config.headerColor
        ]
    , linearLayout[height MATCH_PARENT,weight 1.0][]
    , imageView
        [ height config.headerImageSize
        , width config.headerImageSize
        , visibility config.headerImageVisibility
        , imageUrl config.headerImageUrl
        , onClick push $ const HeaderIconClick
        , margin (Margin 5 0 0 0)
        ]
    ]

contentsView :: ∀ w. Config -> PrestoDOM (Effect Unit) w
contentsView c@(Config config) =
  linearLayout
    [ width MATCH_PARENT
    , height WRAP_CONTENT
    , orientation VERTICAL
    , margin config.contentMargin
    ]
    if config.useList then
    [ scrollView
        [ width MATCH_PARENT
        , height config.listScrollHeight
        , scrollBarY false
        ]
        [ linearLayout
            [ width MATCH_PARENT
            , height WRAP_CONTENT
            , orientation VERTICAL
            ]
            (map (\str -> listItem str c) config.listStrings)
        ]
    ]
    else
      [ textView
          [ width MATCH_PARENT
          , height WRAP_CONTENT
          , textFromHtml config.content
          , textSize config.contentTextSize
          , fontStyle config.contentTextFont
          , color config.contentTextColor
          , multipleLine "true"
          , lineHeight "20"
          ]
      ]

listItem :: ∀ w. String -> Config -> PrestoDOM (Effect Unit) w
listItem str (Config config) =
  linearLayout
    [ width MATCH_PARENT
    , height WRAP_CONTENT
    , margin config.listItemMargin
    ]
    [ imageView
        [ height config.bulletSize
        , width config.bulletSize
        , visibility if config.useBullets then VISIBLE else GONE
        , imageUrl config.bulletIconUrl
        , margin config.bulletMargin
        ]
    , textView
        [ if config.listTextFromHtml then textFromHtml str else text str
        , fontStyle config.listItemFont
        , textSize config.listItemTextSize
        , height WRAP_CONTENT
        , width WRAP_CONTENT
        , multipleLine "true"
        , color config.listItemColor
        ]
    ]

bottomView :: ∀ w. (Action  -> Effect Unit) -> Config -> PrestoDOM (Effect Unit) w
bottomView push c@(Config config) =
  linearLayout
    [ height WRAP_CONTENT
    , width MATCH_PARENT
    , orientation VERTICAL
    , margin config.bottomViewMargin
    ]
    [ textView
        [ width WRAP_CONTENT
        , height WRAP_CONTENT
        , visibility config.termsVisibility
        , textFromHtml config.termsText
        , textSize config.termsTextSize
        , fontStyle config.termsTextFont
        , onClick push $ const ViewTerms
        ]
    , linearLayout
        [ width MATCH_PARENT
        , height WRAP_CONTENT
        , orientation HORIZONTAL
        , margin config.checkBoxMargin
        , onClick push $ const CheckBoxClick
        , visibility config.checkBoxVisibility
        ]
        [ imageView
            [ height config.checkBoxImageSize
            , width config.checkBoxImageSize
            , imageUrl config.checkBoxImageUrl
            , margin $ Margin 0 0 5 0
            ]
        , textView
            [ text config.checkBoxText
            , height MATCH_PARENT
            , width MATCH_PARENT
            , color config.checkBoxTextColor
            , textSize config.checkBoxTextSize
            , fontStyle config.checkBoxTextFont
            , gravity CENTER_VERTICAL
            ]
        ]
    , getTextView push c

    ]


getTextView :: ∀ w. (Action -> Effect Unit) -> Config -> PrestoDOM (Effect Unit) w
getTextView push (Config config) = linearLayout
    [ height $ V 50
    , width MATCH_PARENT
    , orientation HORIZONTAL
    , margin config.buttonsViewMargin
    , visibility $ boolToVisibility $ isVisible config.buttonOneVisibility || isVisible config.buttonTwoVisibility
    ]
    [ textView (
        [ height MATCH_PARENT
        , width $ V 0
        , weight 1.0
        , gravity CENTER
        , padding $ Padding 2 2 2 2
        , visibility config.buttonOneVisibility
        , margin config.buttonOneMargin
        , background config.buttonOneBackground
        , cornerRadius config.buttonOneCornerRadius
        , translationZ config.buttonOneTranslation
        , text config.buttonOneText
        , textSize config.buttonOneTextSize
        , fontStyle config.buttonOneTextFont
        , color config.buttonOneTextColor
        , onClick push $ const ButtonOneClick
        ] <> addStroke config.buttonOneStroke)
    , textView (
        [ height MATCH_PARENT
        , width $ V 0
        , weight 1.0
        , gravity CENTER
        , padding $ Padding 2 2 2 2
        , visibility config.buttonTwoVisibility
        , margin config.buttonTwoMargin
        , background config.buttonTwoBackground
        , cornerRadius config.buttonTwoCornerRadius
        , translationZ config.buttonTwoTranslation
        , text config.buttonTwoText
        , textSize config.buttonTwoTextSize
        , fontStyle config.buttonTwoTextFont
        , color config.buttonTwoTextColor
        , onClick push $ const ButtonTwoClick
        ] <> addStroke config.buttonTwoStroke)
    ]
    where
    addStroke param = if param == "" then [] else [stroke param]

overlayBackground :: ∀ w. (Action -> Effect Unit) -> PrestoDOM (Effect Unit) w
overlayBackground push =
  linearLayout
    [ weight 1.0
    , width MATCH_PARENT
    , gravity CENTER_HORIZONTAL
    , onClick push $ const OverlayClick
    ]
    []
