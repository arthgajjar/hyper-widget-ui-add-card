module UI.Components.Popup.Config where


import HyperPrelude.Internal (Length(..), Padding(..), Visibility(..))
import PrestoDOM.Types.DomAttributes (Corners(..), Margin(..))

data Config = Config
    {   open :: Boolean
    ,   close :: Boolean
    ,   overlayBackground :: String
    ,   margin :: Margin
    ,   padding :: Padding
    ,   header :: String
    ,   headerSize :: Int
    ,   content :: String
    ,   contentTextSize :: Int
    ,   contentTextFont :: String
    ,   contentTextColor :: String
    ,   useBullets :: Boolean
    ,   corners :: Corners
    ,   background :: String
    ,   headerPadding :: Padding
    ,   headerMargin :: Margin
    ,   headerImageSize :: Length
    ,   headerImageUrl :: String
    ,   headerImageVisibility :: Visibility
    ,   headerVisibility ::Visibility
    ,   headerFont :: String
    ,   bulletIconUrl :: String
    ,   useList :: Boolean
    ,   listStrings :: Array String
    ,   dividerColor :: String
    ,   dividerVisibility :: Visibility
    ,   bulletMargin :: Margin
    ,   bulletSize :: Length
    ,   listItemFont :: String
    ,   listItemTextSize :: Int
    ,   contentMargin :: Margin
    ,   addedContentVisibility :: Visibility
    ,   listItemMargin :: Margin
    ,   headerColor :: String
    ,   listItemColor :: String
    ,   termsVisibility :: Visibility
    ,   termsText :: String
    ,   termsTextSize :: Int
    ,   termsTextFont :: String
    ,   checkBoxVisibility :: Visibility
    ,   checkBoxMargin :: Margin
    ,   checkBoxImageUrl :: String
    ,   checkBoxImageSize :: Length
    ,   checkBoxText :: String
    ,   checkBoxTextSize :: Int
    ,   checkBoxTextColor :: String
    ,   checkBoxTextFont :: String
    ,   bottomViewMargin :: Margin
    ,   buttonsViewMargin :: Margin
    ,   buttonOneText :: String
    ,   buttonTwoText :: String
    ,   buttonOneMargin :: Margin
    ,   buttonTwoMargin :: Margin
    ,   buttonOneBackground :: String
    ,   buttonTwoBackground :: String
    ,   buttonOneStroke :: String
    ,   buttonTwoStroke :: String
    ,   buttonOneTranslation :: Number
    ,   buttonTwoTranslation :: Number
    ,   buttonOneTextSize :: Int
    ,   buttonTwoTextSize :: Int
    ,   buttonOneTextFont :: String
    ,   buttonTwoTextFont :: String
    ,   buttonOneTextColor :: String
    ,   buttonTwoTextColor :: String
    ,   buttonOneCornerRadius :: Number
    ,   buttonTwoCornerRadius :: Number
    ,   buttonOneVisibility :: Visibility
    ,   buttonTwoVisibility :: Visibility
    ,   overlayVisibility :: Visibility
    ,   listScrollHeight :: Length
    ,   listTextFromHtml :: Boolean
    }

defConfig :: Config
defConfig = Config
    {   open : false
    ,   close : false
    ,   overlayBackground : "#BF000000"
    ,   margin : Margin 0 0 0 0
    ,   padding : Padding 0 0 0 0
    ,   header : "Popup Header"
    ,   headerSize : 16
    ,   content : ""
    ,   contentTextSize : 12
    ,   contentTextFont : "ProximaNova-Regular"
    ,   contentTextColor : "#000000"
    ,   corners : (Corners 20.0 true true false false)
    ,   background : "#ffffff"
    ,   headerPadding : (Padding 5 5 5 5)
    ,   headerMargin : MarginTop 5
    ,   headerImageSize : (V 16)
    ,   headerImageUrl : "cross"
    ,   headerImageVisibility :  VISIBLE
    ,   headerVisibility : VISIBLE
    ,   headerFont : "Arial-Bold"
    ,   useBullets : true
    ,   bulletIconUrl : "bullet"
    ,   useList : true
    ,   listStrings : []
    ,   dividerColor : "#cccccc"
    ,   dividerVisibility : VISIBLE
    ,   bulletMargin : (Margin 0 8 8 0)
    ,   bulletSize : (V 5)
    ,   listItemFont : "Arial-Regular"
    ,   listItemTextSize : 16
    ,   contentMargin : Margin 0 10 0 10
    ,   addedContentVisibility : GONE
    ,   listItemMargin : (MarginBottom 5)
    ,   headerColor : "#000000"
    ,   listItemColor : "#cccccc"
    ,   termsVisibility : VISIBLE
    ,   termsText : "<u>Terms & Conditions</u>"
    ,   termsTextSize : 12
    ,   termsTextFont : "ProximaNova-Regular"
    ,   checkBoxVisibility : VISIBLE
    ,   checkBoxMargin : MarginTop 20
    ,   checkBoxImageUrl : "checkbox"
    ,   checkBoxImageSize : V 16
    ,   checkBoxText : "Select"
    ,   checkBoxTextSize : 14
    ,   checkBoxTextColor : "#898989"
    ,   checkBoxTextFont : "ProximaNova-Regular"
    ,   bottomViewMargin : Margin 0 0 0 0
    ,   buttonsViewMargin : MarginTop 15
    ,   buttonOneText : "Cancel"
    ,   buttonTwoText : "Continue"
    ,   buttonOneMargin : MarginRight 5
    ,   buttonTwoMargin : MarginLeft 5
    ,   buttonOneBackground : "#ffffff"
    ,   buttonTwoBackground : "#e56167"
    ,   buttonOneStroke : ""
    ,   buttonTwoStroke : ""
    ,   buttonOneTranslation : 0.0
    ,   buttonTwoTranslation : 12.0
    ,   buttonOneTextSize : 12
    ,   buttonTwoTextSize : 12
    ,   buttonOneTextFont : "ProximaNova-SemiBold"
    ,   buttonTwoTextFont : "ProximaNova-SemiBold"
    ,   buttonOneTextColor : "#e56167"
    ,   buttonTwoTextColor : "#ffffff"
    ,   buttonOneCornerRadius : 3.0
    ,   buttonTwoCornerRadius : 3.0
    ,   buttonOneVisibility : VISIBLE
    ,   buttonTwoVisibility : VISIBLE
    ,   overlayVisibility : VISIBLE
    ,   listScrollHeight : WRAP_CONTENT
    ,   listTextFromHtml : false
    }
