module UI.Components.Popup.Controller where

data Action
  = Close
  | OverlayClick
  | HeaderIconClick
  | ViewTerms
  | CheckBoxClick
  | ButtonOneClick
  | ButtonTwoClick
  | BackPressed

type State =
  { popupIndex :: Int }
