module UI.Components.Popup.StackView where

import HyperPrelude.Internal (Length(..), Orientation(..), PrestoDOM, Visibility(..), background, clickable, height, linearLayout, orientation, text, textView, visibility, width)
import HyperPrelude.External (Effect, Maybe, fromMaybe, (!!), Unit, ($), (<))

import UI.Components.Popup.Config (Config(..))
import UI.Components.Popup.Controller (Action, State)
import UI.Components.Popup.View (overlayBackground)

view :: ∀ w. (Action  -> Effect Unit) -> Config -> State -> Array (PrestoDOM (Effect Unit) w ) -> Maybe (PrestoDOM (Effect Unit) w -> PrestoDOM (Effect Unit) w) -> PrestoDOM (Effect Unit) w
view push config@(Config c) state popupViews popupAnimation =
    linearLayout
    [ height MATCH_PARENT
    , width MATCH_PARENT
    , orientation VERTICAL
    , background c.overlayBackground
    , clickable true
    , visibility if c.open then VISIBLE else GONE
    ]
    [ overlayBackground push
    , if state.popupIndex < 0
        then  emptyView
      else
        fromMaybe emptyView $ popupViews !! state.popupIndex
    ]
    where
    emptyView = textView [text "No popup left"]
