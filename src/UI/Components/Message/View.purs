module UI.Components.Message where

import HyperPrelude.Internal (Gravity(..), Length(..), PrestoDOM, background, color, cornerRadius, fontStyle, gravity, height, imageUrl, imageView, linearLayout, margin, onClick, padding, text, textFromHtml, textSize, textView, visibility, width)
import HyperPrelude.External (Effect, Unit, const, ($))

import UI.Utils (multipleLine)
import UI.Components.Message.Config (Config(..))
import UI.Components.Message.Controller (Action(..))

view :: ∀ w. (Action  -> Effect Unit) -> Config -> PrestoDOM (Effect Unit) w
view push (Config config) =
  linearLayout
  [ width MATCH_PARENT
  , height WRAP_CONTENT
  , margin config.margin
  , visibility config.visibility
  , background config.background
  , cornerRadius config.cornerRadius
  , padding config.padding
  , onClick push $ const Click
  , gravity CENTER_VERTICAL
  ]
  [ imageView
    [ imageUrl config.imageUrl
    , height config.imageSize
    , width config.imageSize
    , margin config.imageMargin
    , visibility config.imageVisibility
    ]
  , textView
    [ (if config.textFromHTML then textFromHtml else text ) config.text
    , height WRAP_CONTENT
    , width MATCH_PARENT
    , color config.textColor
    , fontStyle config.textFont
    , margin config.textMargin
    , multipleLine "true"
    , gravity CENTER_VERTICAL
    , textSize config.textSize
    ]
  ]
