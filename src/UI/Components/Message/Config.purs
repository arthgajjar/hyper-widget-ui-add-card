module UI.Components.Message.Config where

import HyperPrelude.Internal (Length(..), Margin(..), Padding(..), Visibility(..))


data Config = Config
  { margin :: Margin
  , visibility :: Visibility
  , background :: String
  , padding :: Padding
  , cornerRadius :: Number
  , imageUrl :: String
  , imageSize :: Length
  , imageMargin :: Margin
  , imageVisibility :: Visibility
  , text :: String
  , textColor :: String
  , textFont :: String
  , textMargin :: Margin
  , textSize :: Int
  , textFromHTML :: Boolean
  }

defConfig :: Config
defConfig =
  Config
    { margin : (Margin 0 0 0 0)
    , visibility : GONE
    , background : ""
    , padding : Padding 0 0 0 0
    , cornerRadius : 0.0
    , imageUrl : "alert"
    , imageSize : V 20
    , imageMargin : (Margin 0 0 0 0)
    , imageVisibility : INVISIBLE
    , text : "Message"
    , textColor : "#000000"
    , textFont : "Roboto-Regular"
    , textMargin : (Margin 0 0 0 0)
    , textSize : 16
    , textFromHTML : false
    }
