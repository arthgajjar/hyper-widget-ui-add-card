module UI.Components.PaymentOptions.BottomView where

import HyperPrelude.Internal (Gravity(..), InputType(..), Length(..), Margin(..), Orientation(..), PrestoDOM, Visibility(..), alpha, background, clickable, color, cornerRadius, editText, fontStyle, gravity, height, hint, id, imageUrl, imageView, inputType, letterSpacing, linearLayout, margin, onChange, onClick, orientation, padding, pattern, relativeLayout, stroke, text, textSize, textView, visibility, weight, width)
import HyperPrelude.External (Effect, Maybe(..), Unit, const, negate, not, ($), (&&), (+), (<>), (==))

import PrestoDOM.Animation as PrestoAnim
import UI.Components.PaymentOptionsConfig (Config(..))
import UI.Components.PaymentOptionsController (PaymentOptionAction(..))
import UI.Utils (accordionLayout, defaultExpand, expand, expandAlpha, expandDuration, loaderAnim, multipleLine)


view :: ∀ w. Config -> (PaymentOptionAction -> Effect Unit) -> Maybe (Array (PrestoDOM (Effect Unit) w)) -> PrestoDOM (Effect Unit) w
view c@(Config config) push accordionContent =
  accordionLayout
    [ height WRAP_CONTENT
    , width MATCH_PARENT
    , defaultExpand config.bottomDefaultExpand
    , expand case config.inputAreaVisibility of
                VISIBLE -> true
                _ -> false
    , expandAlpha 0.0
    , margin config.bottomLayoutMargin
    , expandDuration 150
    , orientation VERTICAL
    ] (
      case accordionContent of
        Just accordionCont -> accordionCont
        _ -> [ linearLayout [width  MATCH_PARENT, height WRAP_CONTENT] [w]]
      )

  where
  w =
    linearLayout
      [ width MATCH_PARENT
      , height WRAP_CONTENT
      , orientation VERTICAL
      , margin (MarginTop 8)
      ]
      [ topRow push c
      , bottomRow push c
      ]


topRow :: ∀ w. (PaymentOptionAction -> Effect Unit) -> Config -> PrestoDOM (Effect Unit) w
topRow push (Config config) =
  linearLayout
    [ width MATCH_PARENT
    , height WRAP_CONTENT
    --, margin (config.inputAreaMargin)
    , margin (MarginBottom 16)
    , onClick push $ const if config.useTopRowForSI then EnableSI else SetDefault
    , visibility config.topRowVisibility
    , clickable config.isTopRowClickable
    , background config.topRowBackground
    , cornerRadius config.topRowCornerRadius
    , padding config.topRowPadding
    ]
    [ imageView
        [ imageUrl config.topRowImage
        , height config.topRowImageSize
        , width config.topRowImageSize
        , margin (Margin 7 3 12 0)
        , visibility config.topRowImageVisibility
        ]
    , textView
        [ text config.topRowText
        , height WRAP_CONTENT
        , width MATCH_PARENT
        , color config.topRowTextColor
        , fontStyle config.topRowTextFontStyle
        , multipleLine "true"
        , textSize config.topRowTextSize
        ]
    ]

bottomRow :: ∀ w. (PaymentOptionAction -> Effect Unit) -> Config -> PrestoDOM (Effect Unit) w
bottomRow push c@(Config config) =
  linearLayout
    [ width config.inputAreaWidth
    , height WRAP_CONTENT
    --, margin config.inputAreaMargin
    , margin (MarginBottom 16)
    ]
    [ linearLayout (
        [ width WRAP_CONTENT
        , height WRAP_CONTENT
        , orientation VERTICAL
        , visibility config.cvvInputVisibility
        --, margin  config.cvvMargin
        ] <> strokeProps) (
        [ getEditTextView c push addStroke
        ] <> if addStroke
              then []
              else [ linearLayout
                      [ height (V 1)
                      , width MATCH_PARENT
                      , visibility $ config.lineSeparatorVisibility
                      , background $ config.lineSeparatorFocusedColor
                      ] []
                   ])
    , linearLayout
        [ height
            if config.cvvType == "boxed" then
                modifyHeight config.buttonHeight 2
            else config.buttonHeight -- increasing height in case of stroke for input
        , width $ V 0
        , weight config.buttonWeight
        ]
        [ buttonContent c push ]
    ]
    where
    addStroke = config.cvvType == "boxed"
    strokeProps =
      if addStroke then
          [ stroke config.cvvStroke
          , cornerRadius config.cvvCornerRadius]
      else []



getEditTextView ::  ∀ w. Config -> (PaymentOptionAction -> Effect Unit) -> Boolean -> PrestoDOM (Effect Unit) w
getEditTextView (Config config) push addStroke = linearLayout
    [ width WRAP_CONTENT
    , height WRAP_CONTENT
    , gravity CENTER_VERTICAL
    , padding config.cvvPadding
    ]
    [ editText (
        [ height config.cvvHeight
        , width config.cvvWidth
        , inputType NumericPassword
        , hint config.cvvText
        , textSize config.cvvTextSize
        , fontStyle config.cvvFont
        , background config.cvvBackground
        , letterSpacing config.cvvLetterSpacing
        , onChange push CVVChanged
        , id config.cvvId
        , margin
            if not config.showCvvInfoLogo && addStroke
            then MarginRight 4
            else MarginRight 0
        ] <> patternProps )
    , imageView
        [ width config.cvvInfoLogoWidth
        , height config.cvvInfoLogoHeight
        , imageUrl config.cvvInfoLogoUrl
        , margin $ if addStroke then MarginRight 8 else Margin 0 0 2 0
        , onClick push $ const $ ToastCVVInfo
        , visibility if config.showCvvInfoLogo then VISIBLE else GONE
        ]
    ]
    where
    patternProps =
      if config.cvvPattern == "" then []
      else [ pattern config.cvvPattern ]

buttonContent :: ∀ w. Config -> (PaymentOptionAction -> Effect Unit) -> PrestoDOM (Effect Unit) w
buttonContent (Config config) push =
  relativeLayout
    [ height MATCH_PARENT
    , width MATCH_PARENT
    , background config.buttonBackground
    , cornerRadius config.buttonCornerRadius
    , onClick push $ const $ ButtonClicked
    , clickable config.buttonClickable
    , alpha config.buttonAlpha
    , visibility config.buttonVisibility
    ]
    [ PrestoAnim.animationSet
        [loaderAnim (getFromX (Config config)) 0 5000 config.startAnimation] $
        linearLayout
          [ height MATCH_PARENT
          , width  MATCH_PARENT
          , gravity CENTER
          , background config.animationColor
          , visibility if config.startAnimation then VISIBLE else GONE
          ] []
    , textView
        [ height MATCH_PARENT
        , width MATCH_PARENT
        , text config.buttonText
        , gravity CENTER
        , color config.buttonTextColor
        , textSize config.buttonTextSize
        , fontStyle config.buttonFontStyle
        ]
    ]

getFromX :: Config -> Int
getFromX (Config config) = case config.cvvInputVisibility of
  VISIBLE -> (-220)
  _ -> (-320)

divider :: ∀ w. Config -> Boolean ->  PrestoDOM (Effect Unit) w
divider (Config config) addMargin =
  linearLayout
    [ height (V 1)
    , width MATCH_PARENT
    , visibility $ config.lineSeparatorVisibility
    , background $ config.lineSeparatorColor
    , margin config.lineSeparatorMargin
    ] []

modifyHeight :: Length -> Int -> Length
modifyHeight MATCH_PARENT y = MATCH_PARENT
modifyHeight WRAP_CONTENT y = WRAP_CONTENT
modifyHeight (V x) y = V (x + y)
