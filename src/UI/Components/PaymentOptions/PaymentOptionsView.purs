module UI.Components.PaymentOptionsView where

import HyperPrelude.Internal (Length(..), PrestoDOM, background, height, width)
import HyperPrelude.External (Effect, Maybe, Tuple(..), Unit, ($), (<<<))

import UI.Components.PaymentOptionsConfig (Config(..))
import UI.Components.PaymentOptionsController (Action(..))
import UI.Utils (keyedSwypeScroll)
import UI.Components.PaymentOptions.TopView as TopView
import UI.Components.PaymentOptions.BottomView as BottomView
import UI.Components.PaymentOptions as ListView
import UI.Components.Message as MessageView

view :: ∀ w. (Action -> Effect Unit) -> Config -> Boolean -> Maybe (Array (PrestoDOM (Effect Unit) w))-> PrestoDOM (Effect Unit) w
view push config@(Config c) useListView accordionContent =
  if useListView then ListView.view push config
  else
    keyedSwypeScroll
      [ width MATCH_PARENT
      , height WRAP_CONTENT
      , background c.background
      ]
      $ expandableView push config useListView accordionContent

expandableView :: ∀ w. (Action -> Effect Unit) -> Config -> Boolean -> Maybe (Array (PrestoDOM (Effect Unit) w)) -> Array (Tuple String (PrestoDOM (Effect Unit) w))
expandableView push (Config config) useListView accordionContent =
  [ (Tuple "Card" $ TopView.view (Config config) useListView push)
  , (Tuple "Pay Button" $ BottomView.view (Config config) (push <<< PaymentListAction) accordionContent)
  , (Tuple "OutageMessage" $ MessageView.view (push <<< OutageMessageClick) config.outageMessageConfig)
  , (Tuple "Divider" $ BottomView.divider (Config config) true)
  ]
