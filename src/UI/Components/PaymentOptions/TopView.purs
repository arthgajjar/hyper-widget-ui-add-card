module UI.Components.PaymentOptions.TopView where

import HyperPrelude.External (Unit, const, ($), (<>), (==), Effect) 
import HyperPrelude.Internal (Gravity(..), Length(..), Margin(..), Orientation(..), Padding(..), PrestoDOM, Prop, Visibility(..), alpha, background, clickable, color, cornerRadius, fontStyle, gravity, height, imageUrl, imageView, linearLayout, margin, onClick, orientation, padding, text, textFromHtml, textSize, textView, visibility, weight, width)
import UI.Components.PaymentOptionsConfig (Config(..), ImagePosition(..), SelectionType(..), addBottomMargin, getGravity, getLayoutTopPadding, modifyDisplayAreaHeight)
import UI.Components.PaymentOptionsController (Action(..))
import UI.Utils (packageIcon, multipleLine)

{-- View Usage Guide
  # Logo layout is for left most image
  # In central view
    * place primary Data like instrument name in primary text
    * place meta data for instrument in secondary text
    * place text for SI message in tertiary text
  # In quantLayout
    * place data regarding offers
--}

view :: ∀ w. Config -> Boolean -> (Action -> Effect Unit) -> PrestoDOM (Effect Unit) w
view c@(Config config) useListView push =
  linearLayout (
    [ orientation HORIZONTAL
    , width MATCH_PARENT
    , height $ modifyDisplayAreaHeight config.displayAreaHeight config.secondaryTextVisibility config.tertiaryVisibility config.quantVisibility
    , margin config.displayAreaMargin
    , padding $ getLayoutTopPadding config.secondaryTextVisibility config.tertiaryVisibility config.quantVisibility config.space
    , alpha config.displayAreaAlpha
    , gravity CENTER_VERTICAL
    , clickable config.ifClickable
    ] <> selectionAction config.selectionType push )
    [ linearLayout
        [ width MATCH_PARENT
        , height WRAP_CONTENT
        , orientation VERTICAL
        ]
        [ linearLayout
            [ width MATCH_PARENT
            , height WRAP_CONTENT
            , gravity $ getGravity config.secondaryTextVisibility config.tertiaryVisibility config.quantVisibility
            ]
            [ primaryLogoView push c
            , linearLayout
                [ width $ V 0
                , height WRAP_CONTENT
                , orientation VERTICAL
                , weight 1.0
                , margin config.textMargin
                , padding config.logoPadding
                ]
                (centralView push c)
            , radioButtonView push c
            ]
        , quantLayout push c
        ]
    ]

primaryLogoView :: ∀ w. (Action -> Effect Unit) -> Config -> PrestoDOM (Effect Unit) w
primaryLogoView push (Config config)  =
  imageView (
    [ width config.logoSize
    , height config.logoSize
    , visibility config.logoVisibility
    ] <> if config.usePackageIcon
        then [ packageIcon config.logoUrl]
        else [ imageUrl config.logoUrl]
    )

centralView :: ∀ w. (Action -> Effect Unit) -> Config -> Array (PrestoDOM (Effect Unit) w)
centralView push c@(Config config) =
  [
  -- first line view
    linearLayout
      [ width MATCH_PARENT
      , height WRAP_CONTENT
      , margin $ addBottomMargin config.secondaryTextVisibility
      , gravity if config.primaryLogoPosition == Left then CENTER_VERTICAL else START
      ]
      ( if config.primaryLogoPosition == Left
          then [ primaryLogo, primaryText ]
          else [ primaryText, primaryLogo ]
      )
  -- second line view
  , linearLayout (
      [ width MATCH_PARENT
      , height WRAP_CONTENT
      , visibility config.secondaryTextVisibility
      , margin $ addBottomMargin config.tertiaryVisibility
      ] <>
        if config.enableSecondLineClick
          then [ onClick push $ const SecondaryTextTwoClick ]
          else [])
      ( if config.secondaryLogoPosition == Left
          then [ secondaryLogo, secondaryText ]
          else [ secondaryText, secondaryLogo ]
      )
  -- third line view
  , linearLayout
      [ width MATCH_PARENT
      , height WRAP_CONTENT
      , visibility config.tertiaryVisibility
      , margin $ case config.secondaryTextVisibility of
          VISIBLE -> MarginTop 0
          _       -> MarginTop 3
      ]
      ( if config.tertiaryLogoPosition == Left
          then [ tertiaryLogo, tertiaryText ]
          else [ tertiaryText, tertiaryLogo ]
      )
  ]
  where
  primaryText = getPrimaryText push c
  primaryLogo = getPrimaryLogo c
  secondaryText = getSecondaryText push c
  secondaryLogo = getSecondaryLogo c
  tertiaryText = getTertiaryText c
  tertiaryLogo = getTertiaryText c

getTertiaryLogo :: ∀ w. Config -> PrestoDOM (Effect Unit) w
getTertiaryLogo (Config config) =
  imageView
      [ imageUrl config.tertiaryLogo
      , width  config.tertiaryLogoSize
      , height config.tertiaryLogoSize
      , margin $ Margin 0 0 5 0
      ]

getTertiaryText :: ∀ w. Config -> PrestoDOM (Effect Unit) w
getTertiaryText (Config config) =
  textView
      [ text config.tertiaryText
      , textSize config.tertiaryTextSize
      , width MATCH_PARENT
      , height WRAP_CONTENT
      , color config.tertiaryTextColor
      , fontStyle config.tertiaryTextFont
      ]


getSecondaryLogo :: ∀ w. Config -> PrestoDOM (Effect Unit) w
getSecondaryLogo (Config config) =
  imageView
      [ imageUrl config.secondaryLogo
      , height config.secondaryLogoSize
      , width config.secondaryLogoSize
      , visibility config.secondaryLogoVisibility
      , margin $ MarginRight 5
      ]

getPrimaryLogo :: ∀ w. Config -> PrestoDOM (Effect Unit) w
getPrimaryLogo (Config config)=
  imageView $
  [ height config.primaryLogoSize
  , width config.primaryLogoSize
  , visibility config.primaryLogoVisibility
  , margin $ MarginRight 5
  ] <> if config.primaryLogoUsePackageIcon
        then [ packageIcon config.primaryLogo ]
        else [ imageUrl config.primaryLogo ]

getSecondaryText :: ∀ w. (Action -> Effect Unit) -> Config -> PrestoDOM (Effect Unit) w
getSecondaryText push (Config config) = linearLayout
    [ height WRAP_CONTENT
    , width WRAP_CONTENT
    , margin config.secondaryTextMargin
    , alpha config.secondaryTextAlpha
    ]
    [ textView
        [ width WRAP_CONTENT
        , height WRAP_CONTENT
        , fontStyle config.secondaryTextFont
        , color config.secondaryTextColor
        , margin config.secondaryTextFirstMargin
        , text config.secondaryText
        , textSize config.secondaryTextSize
        ]
    , textView
        [ width WRAP_CONTENT
        , height WRAP_CONTENT
        , fontStyle config.secondaryText2Font
        , color config.secondaryText2Color
        , text config.secondaryText2
        , textSize config.secondaryTextSize
        ]
    ]


getPrimaryText :: ∀ w. (Action -> Effect Unit) -> Config -> PrestoDOM (Effect Unit) w
getPrimaryText push (Config config) = linearLayout
      [ width WRAP_CONTENT
      , height WRAP_CONTENT
      , margin config.primaryTextMargin
      ]
      [ textView
          [ width WRAP_CONTENT
          , height WRAP_CONTENT
          , text config.primaryText
          , textSize config.primaryTextSize
          , color config.primaryTextColor
          , fontStyle config.primaryTextFont
          ]
      , textView
          [ width WRAP_CONTENT
          , height WRAP_CONTENT
          , text config.primaryText2
          , textSize config.primaryText2Size
          , fontStyle config.primaryText2Font
          , color config.primaryText2Color
          , margin config.primaryText2Margin
          , visibility config.primaryText2Visibility
          , onClick push $ const $ PrimaryTextTwoClick
          ]
      ]

radioButtonView :: ∀ w. (Action -> Effect Unit) -> Config -> PrestoDOM (Effect Unit) w
radioButtonView push (Config config)  =
  linearLayout
    [ width WRAP_CONTENT
    , height config.logoSize
    , orientation VERTICAL
    , gravity config.radioButtonGravity
    , margin config.radioButtonMargin
    , visibility config.radioButtonVisibility
    ]
    [ case config.selectionType of
        Checkbox ->
          imageView
            [ width (config.radioButtonSize)
            , height (config.radioButtonSize)
            , imageUrl config.radioButtonIconUrl
            ]

        Label ->
          textView
            [ text config.selectionLabel
            , textSize config.selectionLabelSize
            , width MATCH_PARENT
            , height config.labelButtonHeight
            , gravity CENTER_VERTICAL
            , color config.labelColor
            , fontStyle config.labelFont
            , padding $ Padding 8 2 8 2
            , background config.labelBackground
            , cornerRadius config.labelCornerRadius
            , onClick push $ const $ PaymentListItemButtonClick
            ]
      ]

quantLayout :: ∀ w. (Action -> Effect Unit) -> Config -> PrestoDOM (Effect Unit) w
quantLayout push (Config config) =
  -- fourth line
  linearLayout (
    [ width MATCH_PARENT
    , height WRAP_CONTENT
    , visibility config.quantVisibility
    , cornerRadius config.quantRadius
    , background config.quantLayoutColor
    , padding config.quantLayoutPadding
    , margin config.quantLayoutMargin
    ] <> if config.enableQuantLayoutClick
          then [ onClick push $ const QuantLayoutClick ]
          else [])
    ( if config.quantLogoPosition == Left
        then [ quantLogo, quantText ]
        else [ quantText, quantLogo]
    )
  where
  quantLogo =
    imageView
      [ imageUrl config.quantLogo
      , width  config.quantLogoSize
      , height config.quantLogoSize
      , margin config.quantLogoMargin
      ]

  quantText =
    textView (
      [ width WRAP_CONTENT
      , height WRAP_CONTENT
      , textSize config.quantTextSize
      , color config.quantTextColor
      , fontStyle config.quantTextFont
      , multipleLine "true"
      ] <> if config.quantTextFromHTML
            then [ textFromHtml config.quantText ]
            else [ text config.quantText ])

selectionAction :: ∀ i. SelectionType -> (Action -> Effect Unit) -> Array (Prop (Effect Unit))
selectionAction selectionType push = case selectionType of
  Checkbox -> [ onClick push $ const $ PaymentListItemSelection ]
  Label    -> [ onClick push $ const $ PaymentListItemButtonClick ]
