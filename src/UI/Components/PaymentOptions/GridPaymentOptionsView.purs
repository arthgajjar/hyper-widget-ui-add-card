module UI.Components.PaymentOptions.GridPaymentOptionView where

import HyperPrelude.External (Effect, Maybe, Unit, const, not, ($), (*), (+), (-), (/), (<>), (==))
import HyperPrelude.Internal (Gravity(..), Length(..), Margin(..), Orientation(..), PrestoDOM, alpha, background, clickable, color, cornerRadius, fontStyle, gravity, height, imageUrl, imageView, linearLayout, margin, onClick, orientation, padding, relativeLayout, stroke, text, textSize, textView, visibility, width)

import UI.Components.PaymentOptionsConfig (Config(..))
import UI.Components.PaymentOptionsController (Action(..))
import UI.Utils (packageIcon)

gridView :: ∀ w . (Action -> Effect Unit) -> Config -> Boolean -> Maybe (Array (PrestoDOM (Effect Unit) w)) -> PrestoDOM (Effect Unit) w
gridView push (Config config) _ _=
	linearLayout
    [ width parentWidth
    , height WRAP_CONTENT
    , background config.gridViewBackground
    , orientation VERTICAL
    , gravity CENTER
    , padding config.gridItemPadding
    , margin config.gridItemMargin
    , clickable config.allowGridClick
    , onClick push $ const $ GridClick
    ]
    [ relativeLayout
        [ width WRAP_CONTENT
        , height WRAP_CONTENT
        ]
        [ linearLayout (
            [ height WRAP_CONTENT
            , width $ V imageWrapperWidth
            , margin imageWrapperMargin
            , alpha config.gridItemAlpha
            ] <> if config.gridLogoStroke == "" then []
                else [ stroke config.gridLogoStroke
                    , cornerRadius config.gridStrokeCornerRadius])
            [ imageView (
                [ width config.gridLogoSize
                , height config.gridLogoSize
                , visibility config.logoVisibility
                , margin gridLogoMargin
                ] <> if config.usePackageIcon
					then [ packageIcon config.logoUrl ]
					else [ imageUrl config.logoUrl ])
            ]
        , imageView
            [ width config.gridTickSize
            , height config.gridTickSize
            , margin tickMargin
            , visibility config.tickVisibility
            , imageUrl config.tickImageUrl
            ]
			]
    , textView
        [ width MATCH_PARENT
        , height WRAP_CONTENT
        , text (config.primaryText)
        , color (config.primaryTextColor)
        , gravity CENTER
        , textSize config.gridTextSize
        , fontStyle (config.primaryTextFont)
        , padding (config.gridTextPadding)
        , alpha config.gridItemAlpha
        ]
    ]
	where
	toInt = case _ of
                V x -> x
                _ -> 0
	hMargin = case _ of
		Margin a _ b _ -> a + b
		_ -> 0
	useStroke = not $ config.gridLogoStroke == ""
	imageSize = toInt config.gridLogoSize
	imageMargins = if useStroke then hMargin config.gridLogoMargin else 0
	strokeWidth = if useStroke then 2 else 0
	imageWrapperWidth = imageSize + 2*strokeWidth + imageMargins
	gridLogoMargin = if useStroke then config.gridLogoMargin else Margin 0 0 0 0
	tickSize = toInt config.gridTickSize
	hTickS = tickSize / 2
	imageWrapperMargin = if useStroke then Margin hTickS hTickS hTickS hTickS else Margin 0 hTickS 0 hTickS
	tickMargin = MarginLeft $ if useStroke then strokeWidth + imageSize + tickSize else imageSize - tickSize
	parentWidth = if useStroke then WRAP_CONTENT else config.gridItemSize
