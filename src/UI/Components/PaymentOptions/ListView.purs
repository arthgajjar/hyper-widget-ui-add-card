module UI.Components.PaymentOptions where


import HyperPrelude.Internal (Gravity(..), Length(..), Orientation(..), Padding(..), PrestoDOM, background, color, fontStyle, gravity, height, imageUrl, imageView, linearLayout, margin, orientation, padding, text, textSize, textView, visibility, weight, width)
import HyperPrelude.External (Effect, Unit, ($))

import UI.Components.PaymentOptionsController (Action)
import PrestoDOM.List as PrestoList
import UI.Components.PaymentOptionsConfig (Config(..))
import UI.Components.Message.Config as MessageConfig
import UI.Utils (multipleLine)

view :: ∀ w. (Action -> Effect Unit) -> Config -> PrestoDOM (Effect Unit) w
view push c@(Config config) =
  linearLayout
    [ orientation VERTICAL
    , width MATCH_PARENT
    , height WRAP_CONTENT
    , PrestoList.backgroundHolder "containerBackground"
    ]
    [ linearLayout
        [ width MATCH_PARENT
        , height config.displayAreaHeight
        , margin config.displayAreaMargin
        , gravity CENTER_VERTICAL
        , padding $ Padding config.space 0 config.space 0
        , PrestoList.alphaHolder "alpha"
        ]
        [ primaryLogoView push c
        , centralView push c
        , radioButtonView push c
        ]
    , messageView config.outageMessageConfig
    , linearLayout
        [ height (V 1)
        , width MATCH_PARENT
        , PrestoList.visibilityHolder "dividerVisibility"
        , background $ config.lineSeparatorColor
        ] []
    ]

primaryLogoView :: ∀ w. (Action -> Effect Unit) -> Config -> PrestoDOM (Effect Unit) w
primaryLogoView push (Config config) =
  imageView
    [ width config.logoSize
    , height config.logoSize
    , visibility config.logoVisibility
    , PrestoList.imageUrlHolder "image"
    ]

centralView :: ∀ w. (Action -> Effect Unit) -> Config -> PrestoDOM (Effect Unit) w
centralView push (Config config) =
  linearLayout
    [ width $ V 0
    , weight 1.0
    , height WRAP_CONTENT
    , orientation VERTICAL
    , margin config.textMargin
    , padding config.logoPadding
    ]
    [ textView
        [ width WRAP_CONTENT
        , height WRAP_CONTENT
        , PrestoList.colorHolder "textColor"
        , PrestoList.fontStyleHolder "font"
        , PrestoList.textHolder "text"
        , PrestoList.textSizeHolder "syze"
        ]
    , textView
        [ width WRAP_CONTENT
        , height WRAP_CONTENT
        , PrestoList.fontStyleHolder "font"
        , color config.secondaryTextColor
        , PrestoList.textHolder "secondaryText"
        , PrestoList.textSizeHolder "secondarySyze"
        , PrestoList.visibilityHolder "secondaryVisibility"
        ]
    , textView
        [ text config.tertiaryText
        , textSize config.tertiaryTextSize
        , width MATCH_PARENT
        , height WRAP_CONTENT
        , color config.tertiaryTextColor
        , fontStyle config.tertiaryTextFont
        , PrestoList.textHolder "tertiaryText"
        , PrestoList.visibilityHolder "tertiaryVisibility"
        ]
    ]

radioButtonView :: ∀ w. (Action -> Effect Unit) -> Config -> PrestoDOM (Effect Unit) w
radioButtonView push (Config config) =
  linearLayout
    [ width WRAP_CONTENT
    , height config.logoSize
    , gravity config.radioButtonGravity
    , margin config.radioButtonMargin
    ]
    [	imageView
        [ width config.radioButtonSize
        , height config.radioButtonSize
        , PrestoList.imageUrlHolder "radioUrl"
        ]
    ]

messageView :: ∀ w. MessageConfig.Config -> PrestoDOM (Effect Unit) w
messageView (MessageConfig.Config config) =
  linearLayout
    [ width MATCH_PARENT
    , height WRAP_CONTENT
    , margin config.margin
    , PrestoList.visibilityHolder "messageVisibility"
    , padding config.padding
    ]
    [ imageView
        [ imageUrl config.imageUrl
        , height config.imageSize
        , width config.imageSize
        , margin config.imageMargin
        , visibility config.imageVisibility
        ]
    , textView
        [ PrestoList.textHolder "messageText"
        , height WRAP_CONTENT
        , width MATCH_PARENT
        , color config.textColor
        , fontStyle config.textFont
        , margin config.textMargin
        , multipleLine "true"
        , textSize config.textSize
        ]
    ]
