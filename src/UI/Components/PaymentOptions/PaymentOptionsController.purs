module UI.Components.PaymentOptionsController where

import UI.Components.Message.Controller as Message

data Action
  = PaymentListItemSelection
  | PaymentListItemButtonClick
  | GridClick
  | PaymentListAction PaymentOptionAction
  | SecondaryTextTwoClick
  | PrimaryTextTwoClick
  | QuantLayoutClick
  | OutageMessageClick Message.Action

data PaymentOptionAction
  = ButtonClicked
  | CVVChanged String
  | SetDefault
  | ToastCVVInfo
  | CvvEditTextFocused String
  | EnableSI

type State = { }
