module UI.Components.PaymentOptionsConfig where

import HyperPrelude.Internal (Gravity(..), Length(..), Margin(..), Padding(..), Visibility(..))
import HyperPrelude.External (class Eq, Maybe, fromMaybe, (#), ($), (*), (+), (-), (/), (<>))

import PPConfig.Utils (btnColor, btnCornerRadius, btnTranslation, editTextCornerRadius, editTextLineFocused, fontBold, fontColor, fontRegular, fontSemiBold, fontSize, fontSizeSmall, getInputFieldFocusColor, getInputViewType, gridFontSize, gridIconSize, gridItemSize, gridLogoStroke, gridStroke, gridViewBackground, iconSize, inputAreaBackground, labelButtonBackgroundColor, labelButtonCornerRadius, labelButtonFontFamily, labelButtonFontSize, labelButtonHeight, labelButtonTextColor, lineSepVisibility, listItemHeight, mandateRowBackground, mandateRowCornerRadius, mandateRowPadding, mandateRowTextColor, offerLayoutColor, offerLayoutCornerRadius, offerLayoutPadding, offerTextColor, outageRowCornerRadius, outageRowTextSize, pOPrimaryTextFont, primaryColor, primaryLogoSize, radioIconSize, strokeCornerRadius, tertiaryFontColor, topRowVisibility, uiCardColor, uiCardHorizontalPadding)
import Data.Newtype (unwrap)
import Remote.Types (ConfigPayload(..), OutageViewProps(..))
import UI.Components.Message.Config as MessageConfig
import UI.Constant.Str.Default as STR
import UI.Utils (animationColor)

data SelectionType = Label | Checkbox

data ImagePosition = Left | Right
derive instance eqImagePosition :: Eq ImagePosition

data PaymentOptionViewType = TypeOne | TypeTwo
derive instance eqPaymentOptionViewType :: Eq PaymentOptionViewType

data Config
  = Config
    { background :: String
    , displayAreaHeight :: Length
    , displayAreaWidth :: Length
    , displayAreaMargin :: Margin
    , displayAreaAlpha :: Number
    , logoVisibility :: Visibility
    , logoSize :: Length
    , logoMargin :: Margin
    , logoPadding :: Padding
    , logoUrl :: String
    , usePackageIcon :: Boolean
    , textMargin :: Margin
    , primaryText :: String
    , primaryTextColor :: String
    , primaryTextSize :: Int
    , primaryTextMargin :: Margin
    , primaryTextFont :: String
    , primaryText2 :: String
    , primaryText2Color :: String
    , primaryText2Size :: Int
    , primaryText2Font :: String
    , primaryText2Visibility :: Visibility
    , primaryText2Margin :: Margin
    , secondaryText :: String
    , secondaryText2 :: String
    , secondaryTextColor :: String
    , secondaryTextFont :: String
    , secondaryText2Font :: String
    , secondaryTextSize :: Int
    , secondaryTextMargin :: Margin
    , secondaryTextVisibility :: Visibility
    , radioButtonVisibility :: Visibility
    , radioButtonSize :: Length
    , radioButtonWidth :: Length
    , radioButtonMargin :: Margin
    , radioButtonIconUrl :: String
    , inputAreaWidth :: Length
    , inputAreaHeight :: Length
    , inputAreaVisibility :: Visibility
    , inputAreaMargin :: Margin
    , cvvHeight :: Length
    , cvvWidth :: Length
    , cvvText :: String
    , cvvTextSize :: Int
    , cvvFont :: String
    , cvvPadding :: Padding
    , cvvType :: String
    , cvvStroke :: String
    , cvvCornerRadius :: Number
    , cvvTint :: String
    , cvvMargin :: Margin
    , cvvLetterSpacing :: Number
    , cvvInputVisibility :: Visibility
    , cvvPattern :: String
    , cvvId :: String
    , cvvInfoLogoUrl :: String
    , cvvInfoLogoMargin :: Margin
    , cvvInfoLogoWidth :: Length
    , cvvInfoLogoHeight :: Length
    , showCvvInfoLogo :: Boolean
    , cvvWeight :: Number
    , buttonWidth :: Length
    , buttonHeight :: Length
    , buttonWeight :: Number
    , buttonText :: String
    , buttonBackground :: String
    , buttonTextColor :: String
    , buttonCornerRadius :: Number
    , buttonTextSize :: Int
    , buttonFontStyle :: String
    , buttonTranslation :: Number
    , lineSeparatorVisibility :: Visibility
    , lineSeparatorColor :: String
    , lineSeparatorFocusedColor :: String
    , animationColor :: String
    , startAnimation :: Boolean
    , buttonClickable :: Boolean
    , buttonAlpha :: Number
    , ifClickable :: Boolean
    , gridItemSize :: Length
    , gridLogoSize :: Length
    , gridTextSize :: Int
    , gridTextHeight :: Length
    , gridTextPadding :: Padding
    , gridItemPadding :: Padding
    , gridViewBackground :: String
    , gridItemMargin :: Margin
    , gridLogoStroke :: String
    , tickVisibility :: Visibility
    , tickImageUrl :: String
    , gridTickSize :: Length
    , gridStrokeCornerRadius :: Number
    , gridLogoMargin :: Margin
    , displayAreaPadding :: Padding
    , tertiaryText :: String
    , tertiaryTextSize :: Int
    , tertiaryTextColor :: String
    , tertiaryTextFont :: String
    , tertiaryHeight :: Length
    , tertiaryMargin :: Margin
    , selectionType :: SelectionType
    , selectionLabel :: String
    , selectionLabelSize :: Int
    , labelColor :: String
    , labelFont :: String
    , labelButtonHeight :: Length
    , space :: Int
    -- central images related config
    , primaryLogo :: String
    , primaryLogoSize :: Length
    , primaryLogoVisibility :: Visibility
    , primaryLogoPosition :: ImagePosition
    , primaryLogoUsePackageIcon :: Boolean
    , secondaryLogo :: String
    , secondaryLogoSize :: Length
    , secondaryLogoVisibility :: Visibility
    , secondaryLogoPosition :: ImagePosition
    , tertiaryVisibility :: Visibility
    , tertiaryLogo :: String
    , tertiaryLogoSize :: Length
    , tertiaryLogoPosition :: ImagePosition
    , topRowVisibility :: Visibility
    , topRowImage :: String
    , topRowImageSize :: Length
    , topRowText :: String
    , topRowTextColor :: String
    , topRowTextFontStyle :: String
    , bottomLayoutMargin :: Margin
    , useTopRowForSI :: Boolean
    , secondaryText2Color :: String
    , secondaryTextFirstMargin :: Margin
    , isSecondaryTextClickable :: Boolean
    , cvvBackground :: String
    , isTopRowClickable :: Boolean
    , topRowBackground :: String
    , topRowImageVisibility :: Visibility
    , topRowCornerRadius :: Number
    , topRowPadding :: Padding
    , radioButtonGravity :: Gravity
    , topRowTextSize :: Int
    , buttonVisibility :: Visibility
    , bottomDefaultExpand :: Boolean
    , secondaryTextAlpha :: Number
    , labelBackground :: String
    , labelCornerRadius :: Number
    , gridStrokeVisibility :: Boolean
    , lineSeparatorMargin :: Margin
    , listItemFont :: String
    , quantVisibility :: Visibility
    , quantRadius :: Number
    , quantLayoutColor :: String
    , quantLayoutPadding :: Padding
    , quantLayoutMargin :: Margin
    , quantLogo :: String
    , quantLogoSize :: Length
    , quantLogoMargin :: Margin
    , quantTextSize :: Int
    , quantTextColor :: String
    , quantTextFont :: String
    , quantText :: String
    , quantLogoPosition :: ImagePosition
    , quantTextFromHTML :: Boolean
    , enableSecondLineClick :: Boolean
    , enableQuantLayoutClick :: Boolean
    , outageMessageConfig :: MessageConfig.Config
    , gridItemAlpha :: Number
    , allowGridClick :: Boolean
    }

defConfig :: ConfigPayload -> Config
defConfig configPayload@(ConfigPayload confPayload) =
  Config
    { displayAreaHeight: (V (listItemHeight configPayload))
    , primaryTextColor: fontColor configPayload
    , primaryTextSize: fontSize configPayload
    , primaryText2 : ""
    , primaryText2Color : "#000000"
    , primaryText2Size : fontSizeSmall configPayload
    , primaryText2Font : fontSemiBold configPayload
    , primaryText2Visibility : GONE
    , primaryText2Margin : Margin 0 0 0 0
    , tertiaryTextSize: fontSizeSmall configPayload
    , tertiaryTextColor: tertiaryFontColor configPayload
    , tertiaryTextFont: fontSemiBold configPayload
    , gridLogoSize: V (gridIconSize configPayload)
    , gridTextSize: (gridFontSize configPayload)
    , labelFont: (labelButtonFontFamily configPayload)
    , primaryTextFont: pOPrimaryTextFont configPayload
    , secondaryTextFont: fontRegular configPayload
    , secondaryText2Font: fontSemiBold configPayload
    , secondaryTextSize: fontSizeSmall configPayload
    , logoSize: V (iconSize configPayload)
    , lineSeparatorVisibility: lineSepVisibility configPayload
    , lineSeparatorFocusedColor: editTextLineFocused configPayload
    , radioButtonSize: V (radioIconSize configPayload)
    , cvvType: getInputViewType configPayload
    , selectionLabelSize: (labelButtonFontSize configPayload)
    , labelButtonHeight : V (labelButtonHeight configPayload)
    , displayAreaWidth: MATCH_PARENT
    , secondaryTextVisibility: VISIBLE
    , radioButtonVisibility: VISIBLE
    , inputAreaVisibility: VISIBLE
    , logoVisibility: VISIBLE
    , cvvInputVisibility: VISIBLE
    , cvvPattern: "^([0-9])+$,3"
    , secondaryLogoVisibility: GONE
    , tertiaryVisibility: GONE
    , inputAreaMargin: (getInputAreaMargin)
    , textMargin: (Margin 0 0 0 0)
    , primaryTextMargin: (Margin 0 0 4 0)
    , displayAreaMargin: (Margin 0 0 0 0)
    , radioButtonMargin: (Margin 0 0 0 0)
    , logoMargin: (Margin 0 0 0 0)
    , gridItemPadding: (Padding 0 0 0 0)
    , gridViewBackground: gridViewBackground configPayload
    , gridTextPadding: (Padding 0 0 0 0)
    , secondaryTextMargin: (Margin 0 0 5 0)
    , cvvPadding: (Padding x 0 0 0)
    , cvvMargin: (Margin 0 0 10 0)
    , gridItemMargin: (Margin 0 0 (space) 0)
    , displayAreaPadding: (Padding space 0 space 0)
    , logoPadding: (Padding space 0 0 0)
    , usePackageIcon: false
    , startAnimation: false
    , ifClickable: true
    , selectionType: Checkbox
    , displayAreaAlpha: 1.0
    , radioButtonWidth: (V 28)
    , inputAreaWidth: MATCH_PARENT
    , inputAreaHeight: (V 64)
    , cvvHeight: (V 40)
    , cvvWidth: (V 60)
    , cvvWeight: 0.30
    , cvvTextSize: 14
    , cvvCornerRadius: editTextCornerRadius configPayload
    , cvvLetterSpacing: 1.75
    , cvvId: "default"
    , buttonBackground: btnColor configPayload
    , buttonTextSize: (fontSize configPayload)
    , buttonFontStyle: fontBold configPayload
    , buttonTranslation: btnTranslation configPayload
    , buttonClickable: true
    , buttonWidth: (V 0)
    , buttonHeight: (V 40)
    , buttonWeight: 1.0
    , buttonCornerRadius: btnCornerRadius configPayload
    , buttonTextColor: "#ffffff"
    , buttonAlpha: 1.0
    , gridItemSize: V (gridItemSize configPayload)
    , gridTextHeight: V 16
    , secondaryLogoSize: V 16
    , tertiaryLogoSize: (V 16)
    , tertiaryHeight: (V 30)
    , space: space
    , background: (uiCardColor configPayload)
    , tertiaryText: "Tertiary Text"
    , tertiaryLogo: "ic_offer_icon"
    , secondaryLogo: "failure_red"
    , selectionLabel: STR.getLink $ fromMaybe "" ((configPayload # unwrap # _.language) :: Maybe String)
    , labelColor: (labelButtonTextColor configPayload)
    , primaryLogo: "wallet_icon"
    , primaryLogoSize: V (fromMaybe 16 $ primaryLogoSize configPayload)
    , primaryLogoVisibility: GONE
    , primaryLogoPosition : Right
    , primaryLogoUsePackageIcon : false
    , secondaryLogoPosition : Left
    , tertiaryLogoPosition : Right
    , lineSeparatorColor: "#efefef"
    , animationColor: (animationColor configPayload)
    , buttonText: STR.getProceed $ fromMaybe "" ((configPayload # unwrap # _.language) :: Maybe String)
    , secondaryText: "Secondary Text"
    , secondaryText2: ""
    , secondaryTextColor: "#FF0000"
    , logoUrl: "right_arrow"
    , primaryText: "PrimaryText"
    , radioButtonIconUrl: "circular_radio_button"
    , cvvText: STR.getSecurityCode "english"
    , cvvFont: fontSemiBold configPayload
    , cvvStroke: "1," <> (getInputFieldFocusColor configPayload)
    , cvvTint: (primaryColor configPayload)
    , cvvInfoLogoUrl: "ic_question_mark"
    , cvvInfoLogoMargin: (Margin 0 0 8 0)
    , cvvInfoLogoWidth: (V 16)
    , cvvInfoLogoHeight: (V 16)
    , showCvvInfoLogo: true
    , tertiaryMargin: (Margin 0 0 0 0)
    , topRowVisibility: topRowVisibility configPayload
    , topRowImage: "checkbox"
    , topRowImageSize: V 16
    , topRowText: "Make this as my default payment option"
    , topRowTextFontStyle: fontRegular configPayload
    , bottomLayoutMargin: (Margin 0 0 0 0)
    , secondaryText2Color: primaryColor configPayload
    , secondaryTextFirstMargin: (MarginRight 5)
    , isSecondaryTextClickable: false
    , gridStrokeVisibility : gridStroke configPayload
    , gridLogoStroke : gridLogoStroke configPayload
    , tickVisibility : GONE
    , tickImageUrl : ""
    , gridTickSize : (V 16)
    , gridStrokeCornerRadius : strokeCornerRadius configPayload
    , gridLogoMargin : (Margin 8 8 8 8)
    , useTopRowForSI : false
    , cvvBackground : (inputAreaBackground configPayload)
    , isTopRowClickable : true
    , topRowBackground : mandateRowBackground configPayload
    , topRowTextColor: mandateRowTextColor configPayload
    , topRowImageVisibility : VISIBLE
    , topRowCornerRadius : mandateRowCornerRadius configPayload
    , topRowPadding : mandateRowPadding configPayload
    , radioButtonGravity : CENTER_VERTICAL
    , topRowTextSize : fontSizeSmall configPayload
    , buttonVisibility : VISIBLE
    , bottomDefaultExpand : false
    , secondaryTextAlpha : 0.75
    , labelBackground : (labelButtonBackgroundColor configPayload)
    , labelCornerRadius : (labelButtonCornerRadius configPayload)
    , lineSeparatorMargin : Margin 0 0 0 0
    , listItemFont : fontSemiBold configPayload
    , quantVisibility : GONE
    , quantRadius : offerLayoutCornerRadius configPayload
    , quantLayoutColor : offerLayoutColor configPayload
    , quantLayoutPadding : offerLayoutPadding configPayload
    , quantLayoutMargin : Margin (is + space) 6 0 0 -- add key for changing top margin. left margin aligns this section
    , quantLogo : "ic_offer_icon"
    , quantLogoSize : V 17
    , quantLogoMargin : Margin 2 2 10 0
    , quantTextSize : fontSize configPayload
    , quantTextColor : offerTextColor configPayload
    , quantTextFont : fontRegular configPayload
    , quantText : "Offer Text"
    , quantLogoPosition : Left
    , quantTextFromHTML : false
    , enableSecondLineClick : false
    , enableQuantLayoutClick : false
    , outageMessageConfig : (MessageConfig.Config updateOutageConfig)
    , gridItemAlpha : 1.0
    , allowGridClick : true
    }
  where
  space = uiCardHorizontalPadding configPayload
  x = 8 --editTextPadding configPayload :: TODO : figure out a way to fix this hard code
  is = iconSize configPayload
  leftSpace = (2 * space + is)
  getInputAreaMargin = (Margin leftSpace 0 space 16)
  OutageViewProps outageViewProps = confPayload.outageViewProps
  MessageConfig.Config outMessageConfig =  MessageConfig.defConfig
  updateOutageConfig =
    outMessageConfig
      { margin = Margin space 0 space 0
      , padding = Padding 0 0 0 16
      , imageMargin = (MarginRight (is - toInt outMessageConfig.imageSize + space))
      , textColor = outageViewProps.textColor
      , cornerRadius = outageRowCornerRadius configPayload
      , textSize = outageRowTextSize configPayload
      }

getTextHeight :: Config -> Length
getTextHeight (Config config) =
  case config.secondaryTextVisibility of
    VISIBLE -> case config.displayAreaHeight of
      V val -> (V $ val / 2 )
      _ -> config.displayAreaHeight
    _ -> MATCH_PARENT

getLayoutTopPadding :: Visibility -> Visibility -> Visibility -> Int-> Padding
getLayoutTopPadding secVisibility terVisibility quaVisibility space =
  case secVisibility, terVisibility, quaVisibility of
    VISIBLE, _ , _ -> Padding space 16 space 16
    _, VISIBLE, _  -> Padding space 16 space 16
    _, _, VISIBLE  -> Padding space 16 space 16
    _, _ , _       -> Padding space 0 space 0

getLayoutBottomPadding :: Visibility -> Padding
getLayoutBottomPadding visibility =
  case visibility of
    VISIBLE -> (PaddingBottom 17)
    _ -> (PaddingBottom 0)

getGravity :: Visibility -> Visibility -> Visibility -> Gravity
getGravity secVisibility terVisibility quaVisibility =
  case secVisibility, terVisibility, quaVisibility of
    VISIBLE, _ , _ -> START
    _, VISIBLE, _  -> START
    _, _, VISIBLE  -> CENTER_VERTICAL
    _, _, _        -> CENTER_VERTICAL

modifyDisplayAreaHeight :: Length -> Visibility -> Visibility -> Visibility -> Length
modifyDisplayAreaHeight actual secondaryTextVisibility tertiaryVisible quantVisibility =
  case secondaryTextVisibility, tertiaryVisible, quantVisibility of
    VISIBLE, _, _   -> WRAP_CONTENT
    _ , VISIBLE, _  -> WRAP_CONTENT
    _ , _, VISIBLE  -> WRAP_CONTENT
    _, _, _         -> actual

addBottomMargin :: Visibility -> Margin
addBottomMargin = case _ of
  VISIBLE -> MarginBottom 3
  _ -> MarginBottom 0

toInt :: Length -> Int
toInt (V x) = x
toInt _ = 0
