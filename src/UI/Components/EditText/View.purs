module UI.Components.EditText.View where

import Prelude (Unit, const, discard, not, otherwise, unit, ($), (&&), (+), (-), (/), (/=), (<>), (==), (||))

import HyperPrelude.External (Effect, launchAff_, liftEffect)
import HyperPrelude.Internal (Gravity(..), InputType(..), Length(..), Margin(..), Orientation(..), Padding(..), PrestoDOM, Visibility(..), background, color, cornerRadius, editText, fontStyle, gravity, height, hint, hintColor, id, imageUrl, imageView, inputType, inputTypeI, letterSpacing, linearLayout, margin, onChange, onClick, orientation, padding, pattern, pivotY, progressBar, relativeLayout, stroke, text, textSize, textView, typeface, visibility, weight, width)

import Engineering.Helpers.Events (onFocus)
import JBridge as JBridge
import PrestoDOM.Animation as PrestoAnim
import UI.Components.EditText.Config (Config(..))
import UI.Components.EditText.Controller (Action(..), Overrides(..), overrides)
import UI.Constant.Str.Default as STR
import UI.Utils (focus, materialEditTextAnimation, separator, separatorRepeat)
import Utils (getOS)

view :: ∀ w. (Action -> Effect Unit) -> Config -> PrestoDOM (Effect Unit) w
view push config =
    linearLayout
        [ height WRAP_CONTENT
        , width MATCH_PARENT
        , orientation VERTICAL
        ]
        [ editTextLayout push config
        , errorMsgLayout push config
        ]

editTextLayout :: ∀ w. (Action -> Effect Unit) -> Config -> PrestoDOM (Effect Unit) w
editTextLayout push c@(Config config) =
    if config.useMaterialView
    then relativeLayout
        [ height WRAP_CONTENT
        , width WRAP_CONTENT
        ] $
        [ mainView
        , PrestoAnim.animationSet
            [ materialEditTextAnimation
                (config.textSize / 2)
                (config.focus || not config.isEmpty)
                (config.stroke == "") ]
            floatingHeader
        ]
    else mainView
    where
    mainView = linearLayout (
        [ width config.cardWidth
        , height config.cardHeight
        , background config.background
        , orientation VERTICAL
        , margin
            if config.useMaterialView
            then addTopMargin config.margin
            else config.margin
        , visibility config.visibility
        , cornerRadius config.cornerRadius
        ] <> if config.stroke == "" then [] else [stroke config.stroke])
        [  linearLayout
            [ width MATCH_PARENT
            , height $ V $ getHeight config.cardHeight
            , orientation HORIZONTAL
            , gravity CENTER
            , margin config.inputTextMargin
            ]
            [ editText
                (((getInputType) <>
                [ width $ V 0
                , height MATCH_PARENT
                , hint getHint
                , color config.textColor
                , weight 1.0
                , background config.background
                , hintColor config.hintColor
                , visibility config.editTextVisibility
                , gravity config.gravity
                , textSize config.textSize
                , fontStyle config.editTextFont
                , padding config.editTextPadding
                , focus config.focus
                , onChange push OnChanged
                , onFocus push OnFocused
                , id config.editTextId
                , letterSpacing config.letterSpacing
                -- , alpha 0.56
                ] <> addSeparator <> addPattern
                ) <> overrides OnChangeEventHandler push config.editTextId)
            , sideLabelView push c
            ]
        , linearLayout
            [ height $ V 1
            , width MATCH_PARENT
            , background lineColor
            ][]
        ]
    getHint
        | config.hideHint || config.useMaterialView && (not config.focus) = ""
        | otherwise = config.hint
    getInputType = case getOS unit of
                    "ANDROID" -> case config.inputType of
                                    Numeric -> [inputTypeI 3]
                                    NumericPassword -> [inputTypeI 18 ]
                                    _       -> [inputType config.inputType]
                    _         -> [inputType config.inputType]

    addSeparator =
        if config.separator /= ""
            then [ separator config.separator
                , separatorRepeat config.separatorRepeat
                ]
            else []

    addPattern = if config.pattern == "" then [] else [pattern config.pattern]

    showError = isError config.errorVisibility && config.errorMsg /= ""
    lineColor
      | config.useMaterialView && showError = config.errorTextColor
      | config.focus = config.lineSeparatorFocusedColor
      | otherwise = config.lineSeparatorColor

    floatingHeader = getFloatingHeader showError c


getFloatingHeader :: ∀ w. Boolean -> Config -> PrestoDOM (Effect Unit) w
getFloatingHeader showError (Config config) = linearLayout
            [ width config.cardWidth
            , height config.cardHeight
            , pivotY 0.0
            , gravity CENTER_VERTICAL
            ]
            [ textView $
                [ background config.background
                , width WRAP_CONTENT
                , height WRAP_CONTENT
                , margin config.inputTextMargin
                , padding (Padding 4 0 8 0)
                , text config.headerText
                , textSize config.textSize
                ]
                <> if showError
                    then [ color config.errorTextColor
                         , fontStyle config.errorTextFont ]
                    else [ color config.headerTextColor
                         , fontStyle config.headerTextFont ]
            ]

sideLabelView :: ∀ w. (Action -> Effect Unit) -> Config -> PrestoDOM (Effect Unit) w
sideLabelView push (Config config) = do
    let showLoader = config.showLoader
    if showLoader
        then
            progressBar
                [ width config.progressBarSize
                , height config.progressBarSize
                , margin config.progressBarMargin
                ]
        else do
            case config.iconVisibility of
                VISIBLE ->
                    imageView (
                    [ width $ addLength (extractRightMargin config.inputTextMargin) config.iconWidth
                    , height $ addLength (extractRightMargin config.inputTextMargin) config.iconHeight
                    , visibility config.iconVisibility
                    , imageUrl config.iconUrl
                    , gravity CENTER
                    , padding (Padding (extractRightMargin config.inputTextMargin) 0 0 0)
                    ] <> (if config.useToolTip then
                            [onClick (resetToolTip push) (const ToggleToolTip)]
                        else []))

                _ -> textView
                    [ width WRAP_CONTENT
                    , height config.iconHeight
                    , visibility config.iconTextVisibility
                    , text config.iconText
                    , typeface config.iconTextTypeface
                    , textSize config.iconTextSize
                    , color config.iconTextColor
                    , gravity CENTER
                    , fontStyle config.font
                    , onClick push $ const $ EditTextButtonClick
                    ]

extractRightMargin :: Margin -> Int
extractRightMargin (Margin _ _ x _) = x
extractRightMargin (MarginRight x) = x
extractRightMargin _ = 0

addLength :: Int -> Length -> Length
addLength l (V w) = V (w + l)
addLength l x = x

getHeight :: Length -> Int
getHeight = case _ of
                (V x) -> x - 2
                _ -> 40

errorMsgLayout :: ∀ w. (Action -> Effect Unit) -> Config -> PrestoDOM (Effect Unit) w
errorMsgLayout push (Config config) =
    linearLayout
        [ height WRAP_CONTENT
        , width MATCH_PARENT
        , visibility config.errorVisibility
        , margin $ MarginTop 3
        , orientation VERTICAL
        ]
        [ textView
            [ height WRAP_CONTENT
            , width MATCH_PARENT
            , text config.errorMsg
            , textSize config.errorTextSize
            , fontStyle config.errorTextFont
            , color config.errorTextColor
            ]
        ]

-- TODO :: move this to Utils & make use of it configurable
resetToolTip :: (Action -> Effect Unit) -> Action -> Effect Unit
resetToolTip push action = do
    launchAff_ $ do
        -- delay $ Milliseconds 3000.0
        -- TODO :: This is hacked for now. Change it later
        liftEffect $ JBridge.toast $ STR.getCvvInfoText "english"
        -- liftEffect $ push ToggleToolTip
    push action

isError :: Visibility -> Boolean
isError VISIBLE = true
isError _ = false

addTopMargin :: Margin -> Margin
addTopMargin (Margin l t r b) = Margin l (t + 8) r b
addTopMargin (MarginRight r) = Margin 0 8 r 0
addTopMargin (MarginTop t) = Margin 0 (t+8) 0 0
addTopMargin (MarginLeft l) = Margin l 8 0 0
addTopMargin (MarginBottom b) = Margin 0 8 0 b
addTopMargin (MarginHorizontal l r) = Margin l 8 r 0
addTopMargin (MarginVertical t b) = Margin 0 (t+8) 0 b
