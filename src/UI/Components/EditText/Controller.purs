module UI.Components.EditText.Controller where

import HyperPrelude.External (Effect, Unit, (==))
import HyperPrelude.Internal (Props)

import Engineering.Helpers.Events (cardNumberOnChange, expiryOnChange)
import UI.Utils (FieldType(..), getFieldTypeID)

data Action = OnChanged String
    | EditTextButtonClick
    | OnFocused Boolean
    | EditTextIconClick
    | ToggleToolTip

data Overrides = OnChangeEventHandler

overrides
    :: Overrides
    -> (Action -> Effect Unit)
    -> String
    -> Props (Effect Unit)
overrides OnChangeEventHandler push id =
    if (id == getFieldTypeID CardNumber)
        then [cardNumberOnChange push OnChanged]
        else if (id == getFieldTypeID ExpiryDate)
            then [expiryOnChange push OnChanged]
            else []
