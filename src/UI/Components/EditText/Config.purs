module UI.Components.EditText.Config where

import HyperPrelude.Internal

data Config = Config
  { cardHeight :: Length
  , cardWidth :: Length
  , background :: String
  , margin :: Margin
  , visibility :: Visibility
  , hint :: String
  , hideHint :: Boolean
  , headerText :: String
  , headerTextFont :: String
  , headerTextColor :: String
  , iconUrl :: String
  , iconWidth :: Length
  , iconHeight :: Length
  , iconText :: String
  , iconTextColor :: String
  , iconTextSize :: Int
  , iconTextTypeface:: Typeface
  , iconVisibility :: Visibility
  , lineSeparatorColor :: String
  , lineSeparatorFocusedColor :: String
  , lineSeparatorHeight :: Int
  , textColor :: String
  , hintColor :: String
  , cardNumberType ::  String
  , stroke :: String
  , cornerRadius :: Number
  , cardBackgroundTint :: String
  , editTextPadding :: Padding
  , editTextVisibility :: Visibility
  , editTextFont :: String
  , lineSeparatorMargin :: Margin
  , gravity :: Gravity
  , textSize :: Int
  , font :: String
  , inputType :: InputType
  , editTextId :: String
  , iconTextVisibility :: Visibility
  , separator :: String
  , separatorRepeat :: String
  , pattern :: String
  , letterSpacing :: Number
  , focus :: Boolean
  , isEmpty :: Boolean
  , useMaterialView :: Boolean
  , showCursor :: Boolean
  , longClickable :: Boolean
  , showLoader :: Boolean
  , progressBarSize :: Length
  , progressBarMargin :: Margin
  , inputTypeI :: Int
  , inputTextMargin :: Margin
  , errorVisibility :: Visibility
  , errorMsg :: String
  , errorTextSize :: Int
  , errorTextColor :: String
  , errorStrokeColor :: String
  , errorTextFont :: String
  , useToolTip :: Boolean
  }

defConfig :: Config
defConfig = Config
  { cardWidth : MATCH_PARENT
  , cardHeight :(V 45)
  , background : "#FFFFFF"
  , margin: (Margin 0 0 0 0)
  , visibility: VISIBLE
  , iconUrl : "white_img"
  , iconWidth :(V 40)
  , iconHeight :(V 40)
  , iconText : ""
  , iconTextColor : "#000000"
  , iconTextSize : 18
  , iconTextTypeface : NORMAL
  , iconVisibility : GONE
  , lineSeparatorColor : "#E0E0E0"
  , lineSeparatorFocusedColor : "#1665C0"
  , lineSeparatorHeight : 1
  , textColor : "#000000"
  , textSize : 18
  , hintColor : "#aaaaaa"
  , hideHint : false
  , inputType: Numeric
  , cardNumberType : ""
  , stroke : ""
  , cornerRadius : 4.0
  , cardBackgroundTint : ""
  , editTextVisibility: VISIBLE
  , editTextPadding : (Padding 0 0 0 0)
  , editTextFont : "Arial-Regular"
  , lineSeparatorMargin: (Margin 0 0 0 0)
  , gravity : CENTER_VERTICAL
  , font : "Arial-Regular"
  , editTextId : ""
  , iconTextVisibility : GONE
  , separatorRepeat : ""
  , separator : ""
  , pattern : ""
  , headerText : ""
  , headerTextFont : "Arial-Regular"
  , headerTextColor : "#9E9E9E"
  , letterSpacing : 0.0
  , focus : true
  , isEmpty : true
  , useMaterialView : false
  , showCursor : false
  , longClickable : false
  , hint :""
  , showLoader : false
  , progressBarSize : V 16
  , progressBarMargin : (Margin 0 0 0 0)
  , inputTypeI : 0
  , inputTextMargin : (Margin 0 0 0 0)
  , errorVisibility : GONE
  , errorMsg : ""
  , errorTextSize : 12
  , errorTextColor : "#ff0000"
  , errorStrokeColor : "#ff0000"
  , errorTextFont : "OpenSans-Regular"
  , useToolTip : false
  }
