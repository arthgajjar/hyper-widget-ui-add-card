module UI.Components.AddCard.Types where

import Prelude
import Data.Maybe (Maybe)
import Engineering.Helpers.Commons (PaymentOffer)
import JBridge (CardDetails)
import Payments.Wallets.Types (MandateType)
import Remote.Types (CardInfo, ConfigPayload, MerchantOffer)
import Service.EC.Types.Instruments as Instruments
import UI.Components.EditText.Controller as EditText
import UI.Components.Message.Controller as Message
import UI.Utils (FieldType)
import Validation (ValidationState)

type ScreenInput
  = { supportedMethods :: Array Instruments.MerchantPaymentMethod
    , cardMethod :: Method
    , configPayload :: ConfigPayload
    , merchantOffer :: MerchantOffer
    , amount :: Number
    , mandateType :: MandateType
    , cardBinOffers :: Array PaymentOffer
    , outages :: Array String
    }

type State
  = { formState :: FormState
    , supportedMethods :: Array Instruments.MerchantPaymentMethod
    , cvvFocusIndex :: Int
    , cardMethod :: Method
    , cardInfo :: Maybe CardInfo
    , configPayload :: ConfigPayload
    , disabledNetworks :: Maybe (Array String)
    , disabledTypes :: Maybe (Array String)
    , blockedNumbers :: Maybe (Array String)
    , startButtonAnimation :: Boolean
    , saveCard :: Boolean
    , merchantOffer :: MerchantOffer
    , amount :: Number
    , underFocus :: FieldType
    , mandate :: MandateType
    , cardBinOffers :: Array PaymentOffer
    , enableSI :: Boolean
    , toolTipAt :: ToolTipInfo
    , outages :: Array String
    }

data Method
  = AddNewCard
  | SavedCard Instruments.StoredCard

data ToolTipInfo
  = CVVInfo
  | SaveCardInfo
  | Null

derive instance eqToolTipInfo :: Eq ToolTipInfo

data Action
  = SubmitCard Method
  | HideCardOverLay
  | CardNumberChanged EditText.Action
  | CVVChanged EditText.Action
  | ExpiryDateChanged EditText.Action
  | Focus FieldType Boolean
  | CardInfoAction String
  | SaveThisCard
  | ShowToolTip ToolTipInfo
  | HideToolTip
  | EnableSI
  | OutageMessageClick Message.Action

type FormFieldState a
  = { status :: ValidationState
    , value :: String
    | a
    }

type FormState
  = { cardNumber :: FormFieldState ( maxLength :: Int, cardDetails :: CardDetails )
    , expiryDate :: FormFieldState ()
    , cvv :: FormFieldState ( maxLength :: Int )
    , name :: FormFieldState ()
    , savedForLater :: Boolean
    }

data Overrides
  = AddCardLabelOne
  | AddCardLabelTwo
  | CardImage
  | CardNumberEditField
  | ExpiryDateEditField
  | CvvEditField
  | CvvEditFieldSaved
  | ErrorMsg
  | BtnPay
  | Space
  | ExpiryAndCvv
  | EnableSICheckBox
