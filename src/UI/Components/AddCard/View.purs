module UI.Components.AddCard.View where

import Prelude (Unit, const, discard, negate, not, pure, unit, (#), ($), (&&), (+), (/=), (<<<), (<>), (==), (||))

import HyperPrelude.External (Effect, elem, fromMaybe, launchAff_, length, liftEffect)
import HyperPrelude.Internal (Gravity(..), Length(..), Margin(..), Orientation(..), PrestoDOM, Visibility(..), alignParentBottom, alpha, background, clickable, color, cornerRadius, fontStyle, gradient, gravity, height, imageUrl, imageView, letterSpacing, linearLayout, margin, onClick, orientation, padding, relativeLayout, singleLine, text, textSize, textView, translationZ, visibility, weight, width)

import Data.Int as IntUtils
import Data.Newtype (unwrap)
import PaymentPageConfig (getUseLocalIcons)
import Validation (ValidationState(..), getCardIcon)
import JBridge as JBridge
import PPConfig.Utils as CPUtils
import PrestoDOM.Animation as PrestoAnim
import PrestoDOM.Utils ((<>>))
import JBridge (CardDetails(..))
import Tracker.Tracker as Tracker
import UI.Components.AddCard.Config (Config(..))
import UI.Components.AddCard.Controller (getCardNumberError, getCardStatus, getExpiryError, interpretCNStatus, overrides)
import UI.Components.AddCard.Types (Action(..), Overrides(..), State, ToolTipInfo(..))
import UI.Components.EditText.Config as EditTextConfig
import UI.Components.EditText.View as EditText
import UI.Components.Message as Message
import UI.Components.Message.Config as MessageConfig
import UI.Config as UIConfig
import UI.Constant.Str.Default as STR
import UI.Utils (FieldType(..), animationColor, getFieldTypeID, loaderAnim, multipleLine, screenWidth, wrapInScroll)
import Utils (arrayJoin)
import Payments.Wallets.Types (MandateType(..))


---made changes in this view

view :: ∀ w . Config -> (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
view config@(Config conf) push state =
  renderLayout (
    [ height MATCH_PARENT
    , width MATCH_PARENT
    , orientation VERTICAL
    ] <>> overrides Space push state) $
    wrap
    [ cardLayout config push state
    , payButton config push state
    ]
  where
  renderLayout = if conf.buttonAtBottom then relativeLayout else linearLayout
  wrap a = if conf.buttonAtBottom then a else wrapInScroll a


cardLayout :: ∀ w. Config -> (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
cardLayout config@(Config conf) push state =
  linearLayout
    [ width MATCH_PARENT
    , height MATCH_PARENT
    , background conf.cardColor
    , cornerRadius conf.cardCornerRadius
    , padding conf.cardPadding
    --, margin conf.cardMargin
    , translationZ conf.cardTranslation
    , orientation VERTICAL
    ]
    [ linearLayout
      [ height MATCH_PARENT
      , width MATCH_PARENT
      , orientation VERTICAL
      ]
      [ linearLayout
        [ height MATCH_PARENT
        , width MATCH_PARENT
        , orientation VERTICAL
        ]
        [ cardNumberView config push state
        , linearLayout (
          [ height MATCH_PARENT
          , width MATCH_PARENT
          , margin conf.inputFieldMargin -- Bottom Margin for expiry & cvv
          ] <>> overrides (ExpiryAndCvv) push state)
          [ expiryDateView config push state
          , linearLayout
              [ height MATCH_PARENT
              , width $ V 24
              ] []
          , cvvView config push state
          ]
        , saveOptionView config push state
        , enableSIView config push state -- TODO :: make changes to support ui with config
        , warningMsg config state
        , Message.view (push <<< OutageMessageClick) (MessageConfig.Config messageConf)
        ]
      ]
    , toolTipView config state
    ]
  where
  MessageConfig.Config defConf = UIConfig.outageMessageConfig state.configPayload
  messageConf = defConf
    { margin = Margin 0 20 0 0
    , visibility = if length state.outages == 0 then GONE else VISIBLE
    , text = if ("" `elem` state.outages) then (CPUtils.outageRowMessage state.configPayload) <> " this option" else (CPUtils.outageRowMessage state.configPayload) <> " " <> (arrayJoin state.outages ", ")
    }

inputLabel :: ∀ w. String -> Config -> PrestoDOM (Effect Unit) w
inputLabel label (Config config) =
    textView
      [ text label
      , fontStyle config.labelFont
      , color config.labelColor
      , textSize config.labelSize
      , letterSpacing config.labelLetterSpacing
      , visibility config.labelVisibility
      , margin
        if config.useInputStroke
          then (MarginBottom 5)
          else (MarginBottom 0)
      ]

cardNumberView :: ∀ w. Config -> (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
cardNumberView config@(Config conf) push state =
  linearLayout
  [ height MATCH_PARENT
  , width MATCH_PARENT
  , orientation VERTICAL
  , margin (Margin 0 0 0 0) --conf.inputFieldMargin
  ]
  [ inputLabel conf.cardNumberLabel config
  , ( EditText.view
      (push <<< CardNumberChanged)
      (EditTextConfig.Config cardNumberConfig)
    )
  ]
  where
    EditTextConfig.Config editTextConfig = UIConfig.cardNumberConfig $ state.configPayload
    CardDetails cd = (state.formState).cardNumber.cardDetails
    cardType = cd.card_type
    isFocused = state.underFocus == CardNumber
    getCnError = getCardNumberError state
    useLocal = getUseLocalIcons (state.configPayload)
    errorMsg = if getCnError == "" || interpretCNStatus state.formState.cardNumber.status state.mandate
                then "" else getCnError
    getValidStateStrokeColor =
      if isFocused
        then conf.inputFocusStrokeColor
        else conf.inputOutOfFocusStrokeColor
    cardIcon = getCardIcon useLocal "visa"
    cardNumberConfig =
      editTextConfig
        { focus = isFocused
        , editTextId = getFieldTypeID CardNumber
        , iconUrl = cardIcon
        , headerText = conf.cardNumberLabel
        , isEmpty = state.formState.cardNumber.value == ""
        , iconVisibility =
            if cardType == "undefined" || cardIcon == ""
              then GONE
              else VISIBLE
        , letterSpacing = 0.0
        , stroke =
            if conf.useInputStroke
              then if errorMsg == ""
                   then getValidStateStrokeColor
                   else "1," <> conf.errorColor
              else ""
        , lineSeparatorColor =
            if not conf.useInputStroke
              then if errorMsg == ""
                   then editTextConfig.lineSeparatorColor
                   else conf.errorColor
              else editTextConfig.lineSeparatorColor
        , inputTextMargin =
            if conf.useInputStroke
              then conf.inputTextMargin
              else editTextConfig.inputTextMargin
        , errorVisibility = if errorMsg == "" then INVISIBLE else VISIBLE
        , errorMsg = errorMsg
        }
    _ = do
      if isFocused
        then Tracker.trackEvent "hyperpay" "info" "add_card_form" "card_number_focussed"
        else pure unit

expiryDateView :: ∀ w. Config -> (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
expiryDateView config@(Config conf) push state=
  linearLayout
  [ height MATCH_PARENT
  , orientation VERTICAL
  , weight 5.0
  ]
  [ inputLabel conf.expiryDateLabel config
  , EditText.view
      (push <<< ExpiryDateChanged)
      (EditTextConfig.Config expiryEditTextConfig)
  ]
  where
    EditTextConfig.Config defaultConfig = UIConfig.expirydateConfig $ state.configPayload
    isFocused = state.underFocus == ExpiryDate
    errorMsg = getExpiryError state
    getValidStateStrokeColor =
      if isFocused
        then conf.inputFocusStrokeColor
        else conf.inputOutOfFocusStrokeColor
    expiryEditTextConfig =
      defaultConfig
        { focus = isFocused
        , editTextId = getFieldTypeID ExpiryDate
        , cardWidth = MATCH_PARENT
        , headerText = conf.expiryDateLabel
        , isEmpty = state.formState.expiryDate.value == ""
        , stroke =
            if conf.useInputStroke
              then if errorMsg == ""
                   then getValidStateStrokeColor
                   else "1," <> conf.errorColor
              else ""
        , inputTextMargin =
            if conf.useInputStroke
              then conf.inputTextMargin
              else defaultConfig.inputTextMargin
        , lineSeparatorColor =
            if not conf.useInputStroke
              then if errorMsg == ""
                   then defaultConfig.lineSeparatorColor
                   else conf.errorColor
              else defaultConfig.lineSeparatorColor
        , errorVisibility = if errorMsg == "" then INVISIBLE else VISIBLE
        , errorMsg = errorMsg
        }
    _ = do
      if isFocused
        then Tracker.trackEvent "hyperpay" "info" "add_card_form" "expiry_date_focussed"
        else pure unit

cvvView :: ∀ w. Config -> (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
cvvView config@(Config conf) push state =
  linearLayout
  [ height MATCH_PARENT
  , orientation VERTICAL
  , weight 5.0
  ]
  [ inputLabel conf.cvvLabel config
  , EditText.view
      (push <<< CVVChanged)
      (EditTextConfig.Config cvvEditTextConfig)
  ]
  where
    EditTextConfig.Config defaultConfig = UIConfig.cvvConfig $ state.configPayload
    isFocused = state.underFocus == CVV
    cvvEditTextConfig =
      defaultConfig
        { focus = isFocused
        , editTextId = getFieldTypeID CVV
        , pattern = ("^[0-9]+$,"<> cvvLengthStr)
        , headerText = conf.cvvLabel
        , isEmpty = state.formState.cvv.value == ""
        , cardWidth = MATCH_PARENT
        , letterSpacing = 0.0
        , iconVisibility =
            if CPUtils.showCvvInfoLogo state.configPayload
              then VISIBLE
              else GONE
        , iconUrl = "ic_question_mark"
        , stroke =
            if conf.useInputStroke
              then if isFocused
                then conf.inputFocusStrokeColor
                else conf.inputOutOfFocusStrokeColor
              else ""
        , inputTextMargin =
            if conf.useInputStroke
              then conf.inputTextMargin
              else defaultConfig.inputTextMargin
        }
    cvvLengthStr = (IntUtils.toStringAs IntUtils.decimal cvvLengthVal)
    cvvLengthVal = 3 --fromMaybe 3 (((state.formState).cardNumber.cardDetails.cvv_length) !! 0)
    _ = do
      if isFocused
        then Tracker.trackEvent "hyperpay" "info" "add_card_form" "cvv_focussed"
        else pure unit


saveOptionView :: ∀ w. Config -> (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
saveOptionView (Config config) push state =
  linearLayout
  [ height MATCH_PARENT
  , width MATCH_PARENT
  , gravity CENTER_VERTICAL
  , orientation HORIZONTAL
  , visibility if state.mandate == None then VISIBLE else GONE
  ]
  [ linearLayout
    [ height MATCH_PARENT
    , orientation HORIZONTAL
    , gravity LEFT
    , onClick push $ const (SaveThisCard)
    , margin (Margin 0 0 5 0)
    ]
    [ imageView
      [ imageUrl $ if state.saveCard then "checkbox1" else "checkbox"
      , height config.checkboxSize
      , width config.checkboxSize
      , margin (Margin 0 0 12 0)
      ]
    , textView
      [ text config.saveCardText
      , fontStyle config.saveCardFontStyle
      , textSize config.saveCardTextSize
      , multipleLine "true"
      , color $ CPUtils.fontColor state.configPayload
      ]
    ]
  , linearLayout
    [ height MATCH_PARENT
    , onClick (resetToolTip push) $ const (ShowToolTip SaveCardInfo)
    ]
    [ imageView
        [ height config.infoIconSize
        , width config.infoIconSize
        , imageUrl "info_icon"
        ]
    ]
  ]

enableSIView :: ∀ w. Config -> (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
enableSIView (Config config) push state =
    linearLayout (
    [ width MATCH_PARENT
    , height MATCH_PARENT
    , visibility $ if state.mandate /= None then VISIBLE else GONE
    , margin (Margin 0 2 0 12)
    , clickable enableSICnStatus
    , alpha if enableSICnStatus then 1.0 else 0.3
    ] <>> (overrides EnableSICheckBox push state ) )
    [ imageView
        [ imageUrl $ if state.enableSI && enableSICnStatus then "checkbox1" else "checkbox"
        , height config.checkboxSize
        , width config.checkboxSize
        , margin (Margin 0 2 10 0)
        ]
    , textView
        [ text "Automatically debit my card to renew subscription for every billing cycle"
        , height MATCH_PARENT
        , width MATCH_PARENT
        , color "#898989"
        , fontStyle config.saveCardFontStyle -- add key for this
        , textSize config.saveCardTextSize
        , multipleLine "true"
        ]
    ]
  where enableSICnStatus = not $ interpretCNStatus state.formState.cardNumber.status state.mandate


payButton :: ∀ w. Config -> (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
payButton c@(Config config) push state =
  relativeLayout ((
    [ height MATCH_PARENT
    , width MATCH_PARENT
    , clickable isValid
    , padding config.buttonPadding
    , translationZ config.cardTranslation -- added to stop button being overlapped by ui card
    ] <>( if config.buttonAtBottom
            then [alignParentBottom "true,-1"]
            else [])
      <> (if config.buttonBackground == ""
          then []
          else if config.buttonAtBottom
            then [ background config.buttonBackground]
            else [])
      )
      <>> (overrides BtnPay push state))
    [ payButtonTextView c isValid buttonAnim state
    , PrestoAnim.animationSet
        [ loaderAnim (-((screenWidth unit) +16)) 0 5000 buttonAnim] $
            linearLayout
            [ height $ V 50
            , width MATCH_PARENT
            , gravity CENTER
            , background (animationColor state.configPayload)
            , visibility if buttonAnim then VISIBLE else GONE
            ] []
    ]
    where
      buttonAnim = (state.startButtonAnimation)
      isValid =
        case getCardStatus state of
          VALID -> true
          _ -> false


payButtonTextView ::  ∀ w. Config -> Boolean -> Boolean -> State -> PrestoDOM (Effect Unit) w
payButtonTextView (Config config) isValid buttonAnim state = textView ((
    [ height $ V 50
    , width MATCH_PARENT
    , padding config.buttonPadding
    , cornerRadius config.buttonCornerRadius
    , textSize config.buttonTextSize
    , gravity CENTER
    , color config.buttonTextColor
    , fontStyle config.buttonFontStyle
    , text btnText
    , alpha if isValid then 1.0 else 0.5
    ] <> if config.useButtonGradient
          then
            [ gradient config.buttonGradient
            --  prop (PropName "gradientAngle") "LEFT_RIGHT" --  :: TODO :: expose this from config
            ]
          else
            [ background config.buttonColor ])
  )
  where
    lang = fromMaybe "" (state.configPayload # unwrap # _.language)
    changeBtnText = CPUtils.changeBtnText $ state.configPayload
    btnText =
      if buttonAnim && changeBtnText then
        STR.getProcessingPayment lang
      else STR.getProceedToPay lang

toolTipView :: ∀ w. Config -> State -> PrestoDOM (Effect Unit) w
toolTipView (Config config) state =
  linearLayout
    [ height MATCH_PARENT
    , width MATCH_PARENT
    , visibility vis
    , background "#000000"
    , alpha 0.75
    , margin (Margin 0 130 0 20) -- This margin manages to be on top of info icon
    , cornerRadius config.cardCornerRadius
    ]
    [ textView
      [ width MATCH_PARENT
      , singleLine false
      , color "#FFFFFF"
      , text msg
      , textSize config.infoTextSize
      , margin (Margin 5 5 5 5)
      , fontStyle config.infoFontStyle
      ]
    ]
    where
    lang = fromMaybe "" (state.configPayload # unwrap # _.language)
    tTip = state.toolTipAt
    msg = case tTip of
        CVVInfo -> STR.getCvvInfoText lang
        SaveCardInfo -> STR.getSaveCardInfoText lang
        _ -> ""

    vis = if tTip == Null then GONE else VISIBLE

resetToolTip :: (Action -> Effect Unit) -> Action -> Effect Unit
resetToolTip push action = do
    launchAff_ $ do
        -- delay $ Milliseconds 3000.0
        -- TODO :: This is hacked for now. Change it later
        liftEffect $ JBridge.toast $ STR.getSaveCardInfoText "english"
        -- liftEffect $ push ToggleToolTip
    push action

warningMsg :: ∀ w. Config -> State -> PrestoDOM (Effect Unit) w
warningMsg (Config config) state =
      textView
      [ height MATCH_PARENT
      , width MATCH_PARENT
      , textSize 13
      , text STR.getMandateSupportWarning
      , color config.warningColor
      , fontStyle config.saveCardFontStyle
      , visibility if interpretCNStatus state.formState.cardNumber.status state.mandate then VISIBLE else GONE
      ]
