module UI.Components.AddCard.Config where

import HyperPrelude.Internal (Gradient(..), Length(..), Margin(..), Padding(..), Visibility(..))


data Config =
  Config
    { cardPadding :: Padding
    , cardMargin :: Margin
    , cardCornerRadius :: Number
    , cardTranslation :: Number
    , cardColor :: String
    , buttonAtBottom :: Boolean
    , labelFont :: String
    , labelSize :: Int
    , labelLetterSpacing :: Number
    , labelVisibility :: Visibility
    , labelColor :: String
    , inputTextFont :: String
    , useInputStroke :: Boolean
    , inputFocusStrokeColor :: String
    , inputOutOfFocusStrokeColor :: String
    , inputTextMargin :: Margin
    , inputFieldMargin :: Margin
    , animationColor :: String
    , saveCardTextSize :: Int
    , saveCardFontStyle :: String
    , checkboxSize :: Length
    , infoIconSize :: Length
    , infoTextSize :: Int
    , infoFontStyle :: String
    , buttonCornerRadius :: Number
    , buttonTextSize :: Int
    , buttonColor :: String
    , buttonTextColor :: String
    , buttonBackground :: String
    , buttonPadding :: Padding
    , buttonFontStyle :: String
    , useButtonGradient :: Boolean
    , buttonGradient :: Gradient
    , cardNumberLabel :: String
    , expiryDateLabel :: String
    , cvvLabel :: String
    , saveCardText :: String
    , saveCardToolTipMsg :: String
    , buttonTranslation :: Number
    , errorColor :: String
    , warningColor :: String
    }

defConfig :: Config
defConfig = Config
  { cardPadding : (Padding 16 16 16 16)
  , cardMargin : (Margin 0 0 0 0)
  , cardCornerRadius : 0.0
  , cardTranslation : 0.0
  , cardColor : "#ffffff"
  , buttonAtBottom : false
  , animationColor : "#4DFFFFFF"
  , labelFont : "OpenSans-Regular"
  , labelSize : 14
  , labelLetterSpacing : 0.7
  , labelVisibility : VISIBLE
  , labelColor : "#000000"
  , inputTextFont : "OpenSans-SemiBold"
  , useInputStroke : true
  , inputFocusStrokeColor : "1,#00B137"
  , inputOutOfFocusStrokeColor : "1,#aaaaaa"
  , inputTextMargin : (Margin 12 0 12 0)
  , inputFieldMargin : (MarginBottom 2)
  , saveCardTextSize : 14
  , saveCardFontStyle : "OpenSans-SemiBold"
  , checkboxSize : (V 18)
  , infoIconSize : (V 10)
  , infoTextSize : 14
  , infoFontStyle : "OpenSans-Regular"
  , buttonCornerRadius : 0.0
  , buttonTextSize : 16
  , buttonColor : "#00B137"
  , buttonBackground : "#ffffff"
  , buttonTextColor : "#ffffff"
  , buttonPadding : (Padding 10 10 10 10)
  , buttonFontStyle : "OpenSans-Bold"
  , buttonTranslation : 0.0
  , useButtonGradient : false
  , buttonGradient : (Linear 20.0 ["#ED1B72", "#FF4067"])
  , errorColor : "#ff0000"
  , warningColor : "#e68750"
  , cardNumberLabel : "Card Number"
  , expiryDateLabel : "Expiry"
  , cvvLabel : "CVV"
  , saveCardText : "Securely save this card for future payments"
  , saveCardToolTipMsg : "Your card information will be encrypted and securely saved. We do not store the CVV number"
  }
