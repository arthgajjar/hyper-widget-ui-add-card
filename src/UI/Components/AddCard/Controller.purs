module UI.Components.AddCard.Controller where

import HyperPrelude.External (class Eq, Effect, Maybe(..), Unit, bind, const, eq, filter, fromMaybe, hush, identity, negate, not, pure, runExcept, spy, (!!), (#), ($), (&&), (/=), (<), (<<<), (<>), (==))
import HyperPrelude.Internal (Props, alpha, focus, id, imageUrl, onClick, pattern, text)
import PPConfig.Utils (getBlockedCardNumbers, getDisabledCardNetworks, getDisabledCardTypes)
import Data.Newtype (unwrap) -- *?
import Data.String as StrUtils
import Data.String.CodePoints (length)
import PaymentPageConfig (getUseLocalIcons)
import Engineering.Helpers.Commons (PaymentOffer, log)
import Engineering.Helpers.Events (onFocus, getFromStoreEvent)
import Engineering.Helpers.Store as Store
import Validation (InvalidState(ERROR, INPROGRESS, BLANK), ValidationState(VALID, INVALID), getAllValidation, getCardIcon, getCardNumberStatus, getCvvStatus, getErrorText, getExpiryDateStatus)
import Service.EC.Types.Instruments as Instruments
import JBridge (CardDetails(..), defaultValidatorOutput, getCardValidation)
import Presto.Core.Utils.Encoding (defaultDecodeJSON)
import Remote.Types (CardInfo, ConfigPayload, MerchantOffer, PaymentOptions(..))
import Payments.Wallets.Utils (dummyPO)
import UI.Components.EditText.Controller as EditText
import UI.Constant.Str.Default as STR
import UI.Utils (FieldType(CVV, SavedCardCVV, ExpiryDate, CardNumber), getFieldTypeID, os)
import UI.Components.Message.Controller as Message
import Payments.Wallets.Types (MandateType(..))
import UI.Components.AddCard.Types



initialFormState :: FormState
initialFormState =
  { cardNumber:
      { status: INVALID BLANK
      , value: ""
      , maxLength: 19
      , cardDetails: defaultValidatorOutput
      }
  , expiryDate:
      { status: INVALID BLANK
      , value: ""
      }
  , cvv:
      { status: INVALID BLANK
      , value: ""
      , maxLength: 3
      }
  , name:
      { status: INVALID BLANK
      , value: ""
      }
  , savedForLater: true
  }

initialState :: ScreenInput -> State
initialState input =
  { formState: initialFormState
  , supportedMethods: input.supportedMethods
  , cardMethod: input.cardMethod
  , cvvFocusIndex: -1
  , cardInfo: Nothing
  , configPayload: input.configPayload
  , disabledNetworks: getDisabledCardNetworks input.configPayload
  , disabledTypes: getDisabledCardTypes input.configPayload
  , blockedNumbers: getBlockedCardNumbers input.configPayload
  , startButtonAnimation: false
  , saveCard: true
  , merchantOffer: input.merchantOffer
  , cardBinOffers: input.cardBinOffers
  , amount: input.amount
  , underFocus: CardNumber
  , mandate: input.mandateType
  , enableSI
  , toolTipAt: Null
  , outages: input.outages
  }
  where
  enableSI = case input.mandateType of
    None -> false
    _ -> true

eval :: Action -> State -> State
eval = case _ of
  SubmitCard _ -> identity
  HideCardOverLay -> identity
  CardNumberChanged (EditText.OnChanged cardNumber) -> (\a -> a { cvvFocusIndex = -1 }) <<< updateFocus CardNumber <<< updateCardNumberStatus <<< updateCardDetails <<< invalidateCardInfo cardNumber <<< updateCardNumber cardNumber
  CardNumberChanged (EditText.OnFocused f) -> if f then \a -> a { underFocus = CardNumber } else identity
  CVVChanged (EditText.OnChanged newCvv) -> (\state -> state { cvvFocusIndex = (length $ state.formState.cvv.value) }) <<< updateCvvStatus <<< updateCvvValue newCvv
  CVVChanged (EditText.OnFocused f) -> if f then \a -> a { underFocus = CVV } else identity
  ExpiryDateChanged (EditText.OnChanged date) -> (\state -> state { cvvFocusIndex = -1 }) <<< updateFocus ExpiryDate <<< updateExpiryDateStatus <<< updateExpiryDateValue date
  ExpiryDateChanged (EditText.OnFocused f) -> if f then \state -> state { underFocus = ExpiryDate } else identity
  Focus action bool ->
    ( if action == CVV && bool then
        (\state -> state { cvvFocusIndex = (length $ state.formState.cvv.value) })
      else
        (\state -> state { cvvFocusIndex = -1 })
    )
      <<< handleFocus action bool
  CardInfoAction cardInfo ->
    let
      (dInfo :: Maybe CardInfo) = (hush <<< runExcept <<< defaultDecodeJSON $ cardInfo)
    in
      updateCardNumberStatus <<< (\state -> state { cardInfo = dInfo })
  SaveThisCard -> \state -> state { saveCard = not state.saveCard }
  ShowToolTip SaveCardInfo -> identity
  -- (\state -> state {toolTipAt = SaveCardInfo} )
  HideToolTip -> identity -- \state -> state {toolTipAt = Null}
  EnableSI ->
    ( \state ->
        let
          newState = state { enableSI = (not $ state.enableSI) }
        in
          updateCardNumberStatus newState
    )
  _ -> log "CHECK STATE" identity

handleFocus :: FieldType -> Boolean -> State -> State
handleFocus CardNumber bool state =
  let
    cnStatus = state.formState.cardNumber.status

    lang = fromMaybe "" ((state.configPayload # unwrap # _.language) :: Maybe String)
  in
    case cnStatus, bool of
      INVALID INPROGRESS, false -> state { formState = state.formState { cardNumber = state.formState.cardNumber { status = INVALID (ERROR $ STR.cardNumberError lang) } } }
      _, _ -> state

handleFocus ExpiryDate bool state =
  let
    status = state.formState.expiryDate.status

    lang = fromMaybe "" ((state.configPayload # unwrap # _.language) :: Maybe String)
  in
    case status, bool of
      INVALID INPROGRESS, false -> state { formState = state.formState { expiryDate = state.formState.expiryDate { status = INVALID (ERROR $ STR.expiryDateError lang) } } }
      _, _ -> state

handleFocus CVV bool state =
  let
    status = state.formState.cvv.status

    lang = fromMaybe "" ((state.configPayload # unwrap # _.language) :: Maybe String)
  in
    case status, bool of
      INVALID INPROGRESS, false -> state { formState = state.formState { cvv = state.formState.cvv { status = INVALID (ERROR $ STR.cvvError lang) } } }
      _, _ -> state

handleFocus _ bool state = state

getFocus :: State -> FieldType -> Boolean
getFocus state = eq state.underFocus

invalidateCardInfo :: String -> State -> State
invalidateCardInfo cN state =
  if StrUtils.length cN < 6 then
    state { cardInfo = Nothing }
  else
    state

updateCardNumber :: String -> State -> State
updateCardNumber cardNumber state = state { formState = state.formState { cardNumber = state.formState.cardNumber { value = StrUtils.replaceAll (StrUtils.Pattern " ") (StrUtils.Replacement "") cardNumber } } }

updateCardDetails :: State -> State
updateCardDetails state =
  let
    cardNumber = state.formState.cardNumber.value
  in
    state { formState = state.formState { cardNumber = state.formState.cardNumber { cardDetails = (getCardValidation cardNumber) } } }

updateCardNumberStatus :: State -> State
updateCardNumberStatus state =
  let
    cardNumber = state.formState.cardNumber.value

    cardDetails = state.formState.cardNumber.cardDetails

    cardInfo = state.cardInfo

    supportedMethods = state.supportedMethods

    disabledNetworks = state.disabledNetworks

    disabledTypes = state.disabledTypes

    blockedNumbers = state.blockedNumbers

    merchantOffer = state.merchantOffer

    mandate = state.mandate

    enableSI = state.enableSI

    cardBinOffers = state.cardBinOffers

    lang = fromMaybe "" ((state.configPayload # unwrap # _.language) :: Maybe String)

    paymentOptions = state.configPayload # unwrap # _.paymentOptions

    cardsPO = filter (\(PaymentOptions po) -> po.po == "cards") paymentOptions

    (PaymentOptions cardsPOExtracted) = fromMaybe dummyPO (cardsPO !! 0)

    onlyEnabledCards = fromMaybe [] cardsPOExtracted.onlyEnable

    cardNumberStatus =
      ( getCardNumberStatus
          lang
          onlyEnabledCards
          cardNumber
          cardDetails
          cardInfo
          supportedMethods
          disabledNetworks
          disabledTypes
          blockedNumbers
          merchantOffer
          cardBinOffers
          mandate
          enableSI
      )
  in
    state { formState = state.formState { cardNumber = state.formState.cardNumber { status = cardNumberStatus } } }

updateFocus :: FieldType -> State -> State
updateFocus field state =
  let
    card = updateCNStatus state.formState.cardNumber.status state.mandate

    expiry = state.formState.expiryDate.status

    cvv = state.formState.cvv.status
  in
    case field, card, expiry, cvv, os of
      CardNumber, VALID, (INVALID _), _, _ -> state { underFocus = ExpiryDate }
      CardNumber, VALID, VALID, (INVALID _), _ -> state { underFocus = CVV }
      ExpiryDate, _, VALID, (INVALID _), _ -> state { underFocus = CVV }
      ExpiryDate, (INVALID _), VALID, VALID, _ -> state { underFocus = CardNumber }
      CVV, (INVALID _), _, VALID, _ -> state { underFocus = CardNumber }
      CVV, VALID, (INVALID _), VALID, _ -> state { underFocus = ExpiryDate }
      _, _, _, _, _ -> state

updateExpiryDateValue :: String -> State -> State
updateExpiryDateValue expDate state = state { formState = state.formState { expiryDate = state.formState.expiryDate { value = expDate } } }

updateExpiryDateStatus :: State -> State
updateExpiryDateStatus state =
  let
    expiryDate = state.formState.expiryDate.value

    lang = state.configPayload # unwrap # _.language
  in
    state { formState = state.formState { expiryDate = state.formState.expiryDate { status = (getExpiryDateStatus expiryDate) } } }

updateCvvValue :: String -> State -> State
updateCvvValue cvv state = state { formState = state.formState { cvv = state.formState.cvv { value = cvv } } }

updateCvvStatus :: State -> State
updateCvvStatus state =
  let
    cvv = state.formState.cvv.value

    cardType = state.formState.cardNumber.cardDetails

    CardDetails carddetails = cardType
  in
    state { formState = state.formState { cvv = state.formState.cvv { status = (getCvvStatus cvv carddetails.card_type) } } }

getCardStatus :: State -> ValidationState
getCardStatus state =
  let
    cnStatus = state.formState.cardNumber.status

    isMaestro = false --state.formState.cardNumber.cardDetails.card_type == "maestro"

    expStatus = state.formState.expiryDate.status

    cvvStatus = state.formState.cvv.status

    enableSI = state.enableSI

    mandate = state.mandate

    updatedCnStatus = updateCNStatus cnStatus mandate
  in
    case state.cardMethod of
      AddNewCard -> getAllValidation updatedCnStatus expStatus cvvStatus isMaestro
      SavedCard _ -> cvvStatus

getCardNumberError :: State -> String
getCardNumberError state = getErrorText $ state.formState.cardNumber.status

getExpiryError :: State -> String
getExpiryError state = getErrorText $ state.formState.expiryDate.status

-------------------------- OVERRIDES ----------------------------------

overrides :: Overrides -> (Action -> Effect Unit) -> State -> Props (Effect Unit)
overrides BtnPay push state =
  let
    cardMethod = state.cardMethod
  in
    [ onClick push $ const (SubmitCard cardMethod) ]

overrides AddCardLabelOne push state = do
  let
    lang = fromMaybe "" ((state.configPayload # unwrap # _.language) :: Maybe String)
  case state.cardMethod of
    AddNewCard -> [ text $ STR.getAdd lang ]
    SavedCard _ -> [ text $ STR.getPay lang ]

overrides AddCardLabelTwo push state = do
  let
    lang = fromMaybe "" ((state.configPayload # unwrap # _.language) :: Maybe String)
  case state.cardMethod of
    AddNewCard -> [ text $ STR.getDebitCard lang ]
    SavedCard _ -> [ text $ STR.getYourBill lang ]

overrides CardImage push state = do
  let
    useLocal = getUseLocalIcons (state.configPayload)
  let
    CardDetails cd = state.formState.cardNumber.cardDetails
  _ <- pure $ spy "Function called" "cardType"
  case state.cardMethod of
    AddNewCard ->
      [ imageUrl $ (getCardIcon useLocal (cd.card_type)) --changes to be done
      ]
    SavedCard (Instruments.StoredCard card) -> [ imageUrl $ getCardIcon useLocal $ card.cardBrand ]

overrides CvvEditField push state =
  [ id $ getFieldTypeID CVV
  , focus $ getFocus state CVV
  , onFocus push (Focus CVV)
  , pattern "^[0-9]+$,3"
  ]

overrides CvvEditFieldSaved push state =
  [ id $ getFieldTypeID SavedCardCVV
  , focus $ getFocus state SavedCardCVV
  , onFocus push (Focus CVV)
  , pattern "^[0-9]+$,3"
  ]

overrides ErrorMsg push state =
  let
    cardStatus = getCardStatus state
  in
    [ text $ getErrorText cardStatus ]

overrides Space push state =
  [ getFromStoreEvent (Store.Key "cardInfo") push CardInfoAction
  -- , onClick push (const HideCardOverLay)
  ]

overrides ExpiryAndCvv push state =
  let
    CardDetails cd = state.formState.cardNumber.cardDetails
  in
    [ alpha
        $ case cd.card_type of
            "maestro" -> 0.4
            _ -> 1.0
    ]

overrides EnableSICheckBox push state = case state.mandate of
  Optional -> [ onClick push $ const EnableSI ]
  _ -> []

overrides _ push state = []

formattedCardNumber :: String -> String
formattedCardNumber cardNo =
  if StrUtils.length cardNo == 18 then
    "XXXX XXXX XXXX " <> StrUtils.drop 14 cardNo
  else
    "XXXX XXXX XXXX XXXX " <> StrUtils.drop 17 cardNo

-- returns true if eMandateSupport is Optional and is not supported for the card
interpretCNStatus :: ValidationState -> MandateType -> Boolean
interpretCNStatus cnStatus mandate = case cnStatus of
  INVALID err@(ERROR e) ->
    if e == STR.getMandateSupportError && mandate == Optional then
      true
    else
      false
  _ -> false

updateCNStatus :: ValidationState -> MandateType -> ValidationState
updateCNStatus cnStatus mandate = case cnStatus of
  INVALID err@(ERROR e) ->
    if e == STR.getMandateSupportError && mandate /= Optional then
      INVALID err
    else
      if e == STR.getMandateSupportError then
        VALID
      else
        cnStatus
  _ -> cnStatus
