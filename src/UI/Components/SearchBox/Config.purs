module UI.Components.SearchBox.Config where

import PrestoDOM

data Config = Config
    { hint :: String
    , stroke :: String
    , fieldErrorMessage :: String
    , imageUrl :: String
    , height :: Length
    , padding :: Padding
    , textSize :: Int
    , cornerRadius :: Number
    , searchIconAlpha :: Number
    , searchIconPadding :: Padding
    , searchIconMargin :: Margin
    , editTextPadding :: Padding
    , editTextMargin :: Margin
    , searchIconVisibility :: Visibility
    , tickVisibility :: Visibility
    , lineVisibility :: Visibility
    , searchIconSize :: Length
    , font :: String
    , cancelVisibility :: Visibility
    , cancelUrl :: String
    , margin :: Margin
    , searchBoxFocus :: Boolean
    , hintColor :: String
    , textColor :: String
    , background :: String
    , cancelMargin :: Margin
    }

defConfig :: Config
defConfig = Config
    { hint : ""
    , stroke : "0,#ff00ff"
    , fieldErrorMessage : ""
    , imageUrl : "ic_search_gray"
    , height : V 52
    , padding: (Padding 0 0 0 0)
    , textSize: 24
    , cornerRadius: 0.0
    , searchIconAlpha : 1.0
    , searchIconVisibility : GONE
    , searchIconPadding : (Padding 0 0 0 0)
    , searchIconMargin : (Margin 5 0 0 0)
    , editTextPadding : (Padding 5 0 0 0)
    , editTextMargin : (Margin 0 0 0 0)
    , tickVisibility : GONE
    , lineVisibility : GONE
    , searchIconSize : V 24
    , font : "Roboto-Regular"
    , cancelVisibility : GONE
    , cancelUrl : "ic_search_cancel"
    , margin : (Margin 0 0 0 0)
    , searchBoxFocus : false
    , hintColor : "#54232323"
    , textColor : "#000000"
    , background : "#FFFFFF"
    , cancelMargin : (Margin 0 0 5 0)
    }
