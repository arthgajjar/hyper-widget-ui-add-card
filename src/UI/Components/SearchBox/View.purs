module UI.Components.SearchBox.View where

import HyperPrelude.Internal (Gravity(..), Length(..), Orientation(..), PrestoDOM, alpha, background, backgroundTint, color, cornerRadius, editText, focus, fontStyle, gravity, height, hint, hintColor, id, imageUrl, imageView, linearLayout, margin, onChange, onClick, orientation, padding, stroke, textSize, visibility, weight, width)
import HyperPrelude.External (Effect, Unit, const, ($), (<>), (==))

import Engineering.Helpers.Events (onFocus)
import JBridge as JBridge
import UI.Components.SearchBox.Config (Config(..))
import UI.Components.SearchBox.Controller (Action(..))

view :: ∀ w . (Action  -> Effect Unit) -> Config  -> PrestoDOM (Effect Unit) w
view push c@(Config config) =
  linearLayout (
    [ height config.height
    , width MATCH_PARENT
    , gravity CENTER_VERTICAL
    , cornerRadius config.cornerRadius
    , orientation HORIZONTAL
    , padding config.padding
    , background config.background
    , margin config.margin
    ] <> if config.stroke == ""
            then []
            else [ stroke config.stroke] )
    [ imageView
        [ height config.searchIconSize
        , width config.searchIconSize
        , imageUrl config.imageUrl
        , padding config.searchIconPadding
        , margin config.searchIconMargin
        , visibility config.searchIconVisibility
        , alpha config.searchIconAlpha
        ]
    , getEditTextView push c
    , linearLayout
        [ height MATCH_PARENT
        , width WRAP_CONTENT
        , onClick push (const Canceled)
        ]
        [   imageView
            [ height MATCH_PARENT
            , width $ V 12
            , imageUrl $ config.cancelUrl
            , visibility $ config.cancelVisibility
            , margin $ config.cancelMargin
            ]
        ]
    ]

getEditTextView :: ∀ w . (Action -> Effect Unit) -> Config -> PrestoDOM (Effect Unit) w
getEditTextView push (Config config) =  linearLayout
    [ height MATCH_PARENT
    , width $ V 0
    , weight 1.0
    , gravity CENTER_VERTICAL
    ]
    [ editText
        [ height MATCH_PARENT
        , width MATCH_PARENT
        , textSize config.textSize
        , id $ JBridge.getNewIDWithTag JBridge.SEARCHBOX
        , color config.textColor
        , hint config.hint
        , hintColor config.hintColor
        , fontStyle config.font
        , padding config.editTextPadding
        , gravity CENTER_VERTICAL
        , onChange push Searched
        , onFocus push Clicked
        , focus config.searchBoxFocus
        , backgroundTint config.background
        ]
    ]
