module UI.Components.SearchBox.Controller where

data Action = Clicked Boolean | Searched String | Canceled

type State = {}
