module UI.Components.AmountBar.Config where

import HyperPrelude.Internal

type LineItemData =
  { leftText :: String
  , centerText :: String
  , rightText :: String
  , font :: String
  , textSize :: Int
  , textColor :: String
  }


data Config = Config
  { padding :: Padding
  , margin :: Margin
  , rightTextVisibility :: Visibility
  , rightText :: String
  , rightTextSize :: Int
  , rightTextFont :: String
  , rightTextColor :: String
  , lineStrings :: Array LineItemData
  , translation :: Number
  , background :: String
  , visibility :: Visibility
  , dividerColor :: String
  , dividerVisibility :: Visibility
  }

defConfig :: Config
defConfig = Config
  { padding : (Padding 0 0 0 0)
  , margin : (Margin 0 0 0 0)
  , rightTextVisibility : VISIBLE
  , rightText : "Rs.0"
  , rightTextSize : 14
  , rightTextFont : "Arial-Bold"
  , rightTextColor : "#000000"
  , lineStrings : [{ leftText : "Some text" , centerText : "center text", rightText : "righttext", font : "Arial", textSize : 14, textColor : "#2D2D2D"}]
  , translation : 0.0
  , background : "#ffffff"
  , visibility : VISIBLE
  , dividerColor : "#cccccc"
  , dividerVisibility : GONE
  }
