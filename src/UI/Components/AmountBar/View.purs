module UI.Components.AmountBar.View where

import HyperPrelude.External (Effect, Unit, map, ($))
import HyperPrelude.Internal (Gravity(..), Length(..), Margin(..), Orientation(..), PrestoDOM, background, color, fontStyle, gravity, height, linearLayout, margin, orientation, padding, text, textSize, textView, translationZ, visibility, weight, width)

import UI.Components.AmountBar.Config (Config(..), LineItemData)
import UI.Utils (multipleLine)



view :: ∀ w . Config -> PrestoDOM (Effect Unit) w
view conf@(Config config) =
  linearLayout
  [ width MATCH_PARENT
  , height WRAP_CONTENT
  , orientation VERTICAL
  , background config.background
  , translationZ config.translation
  ]
  [ linearLayout
    [ height WRAP_CONTENT
    , width MATCH_PARENT
    , padding config.padding
    , margin config.margin
    , background config.background
    , gravity CENTER
    , visibility config.visibility
    ]
    [ leftSectionView conf
    , rightSectionView conf
    ]
  , divider conf
  ]

leftSectionView :: ∀ w. Config -> PrestoDOM (Effect Unit) w
leftSectionView (Config config) =
  linearLayout
    [ height WRAP_CONTENT
    , weight 10.0
    , orientation VERTICAL
    ]
    ( map (\item -> lineView item) config.lineStrings)

lineView :: ∀ w. LineItemData -> PrestoDOM (Effect Unit) w
lineView lItem =
  linearLayout
  [ width MATCH_PARENT
  , orientation HORIZONTAL
  , height WRAP_CONTENT
  , margin (MarginBottom 2)
  ]
  [ textView
    [ height WRAP_CONTENT
    , weight 1.0
    , text lItem.leftText
    , textSize lItem.textSize
    , fontStyle lItem.font
    , color lItem.textColor
    , multipleLine "true"
    ]
  , textView
    [ weight 1.0
    , height WRAP_CONTENT
    , gravity CENTER_HORIZONTAL
    , text lItem.centerText
    , textSize lItem.textSize
    , fontStyle lItem.font
    , color lItem.textColor
    ]
  , textView
    [ height WRAP_CONTENT
    , weight 1.0
    , text lItem.rightText
    , textSize lItem.textSize
    , fontStyle lItem.font
    , color lItem.textColor
    ]
  ]

rightSectionView :: ∀ w. Config -> PrestoDOM (Effect Unit) w
rightSectionView (Config config) =
  textView
  [ height WRAP_CONTENT
  , weight 2.0 -- was WRAP_CONTENT
  , text config.rightText
  , textSize config.rightTextSize
  , color config.rightTextColor
  , visibility config.rightTextVisibility
  , fontStyle config.rightTextFont
  --, margin (MarginLeft 5 )
  ]

divider :: ∀ w. Config -> PrestoDOM (Effect Unit) w
divider (Config config ) =
  linearLayout
    [ width MATCH_PARENT
    , height $ V 1
    , background config.dividerColor
    , visibility config.dividerVisibility
    ][]
