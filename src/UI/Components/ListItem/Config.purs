module UI.Components.ListItem.Config where


import HyperPrelude.Internal

import PrestoDOM.Types.DomAttributes (Corners(..))

data Config = Config
    { height :: Length
    , width :: Length
    , translation :: Number
    , background :: String
    , text :: String
    , textColor :: String
    , textSize :: Int
    , textFont :: String
    , textMargin :: Margin
    , buttonText :: String
    , buttonVisibility :: Visibility
    , buttonPadding :: Padding
    , buttonColor :: String
    , buttonFont :: String
    , sideButtonText :: String
    , sideButtonVisibility :: Visibility
    , sideButtonColor :: String
    , sideButtonFont :: String
    , sidePadding :: Padding
    , radioButtonSize :: Length
    , radioButtonUrl :: String
    , radioButtonVisibility :: Visibility
    , padding :: Padding
    , margin :: Margin
    , buttonTextSize :: Int
    , corners :: Corners
    , visibility :: Visibility
    , imageVisibility :: Visibility
    , imageSize :: Length
    , imageUrl :: String
    , centerTextPadding :: Padding
    }

defaultConfig :: Config
defaultConfig = Config
    { height : WRAP_CONTENT
    , width : MATCH_PARENT
    , translation : 0.0
    , background : "#ffffff"
    , text : "Text"
    , textColor : "#000000"
    , textSize : 16
    , textFont : "Arial-Regular"
    , textMargin : (Margin 0 0 5 0)
    , buttonText : "Details"
    , buttonVisibility : VISIBLE
    , buttonColor : "#ff0000"
    , buttonFont : "Arial-Regular"
    , buttonPadding : Padding 0 5 0 18
    , sideButtonText : "Side Details"
    , sideButtonVisibility : GONE
    , sideButtonColor : "#ff0000"
    , sideButtonFont : "Arial-Regular"
    , sidePadding : Padding 16 5 0 18
    , radioButtonSize : (V 16)
    , radioButtonUrl : "circular_radio_button"
    , radioButtonVisibility : VISIBLE
    , padding : (Padding 5 5 5 5)
    , margin : (Margin 0 0 0 5)
    , buttonTextSize : 16
    , corners : (Corners 8.0 false false false false)
    , visibility : VISIBLE
    , imageVisibility : GONE
    , imageSize : (V 16)
    , imageUrl : "circular_radio_button"
    , centerTextPadding : Padding 0 5 0 21
    }
