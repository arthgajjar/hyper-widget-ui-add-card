module UI.Components.ListItem.View where

import Prelude (Unit, const, ($))

import HyperPrelude.External (Effect)
import HyperPrelude.Internal (Gravity(..), Length(..), Padding(..), PrestoDOM, background, color, fontStyle, gravity, height, imageUrl, imageView, linearLayout, margin, onClick, padding, text, textSize, textView, translationZ, visibility, weight, width)

import UI.Components.ListItem.Config (Config(..))
import UI.Components.ListItem.Controller (Action(..))


view :: ∀ w . (Action  -> Effect Unit) -> Config -> PrestoDOM (Effect Unit) w
view push conf@(Config config) =
    linearLayout
    [ height config.height
    , width config.width
    , padding config.padding
    , margin config.margin
    , gravity CENTER
    -- , cornerRadii config.corners -- [TODO] Required but enabling this breaks translationZ
	, translationZ config.translation
    , background config.background
    , visibility config.visibility
    ]
    [ leftImageView conf
    , centralView push conf
    , linearLayout
        [ width $ V 0
        , height MATCH_PARENT
        , weight 1.0
        , padding $ Padding 0 5 0 18
        , onClick push $ const RadioClick
        ][]
    , sideView push conf
    ]

leftImageView :: ∀ w . Config -> PrestoDOM (Effect Unit) w
leftImageView (Config config) =
    linearLayout
    [ height MATCH_PARENT
    , width WRAP_CONTENT
    , gravity CENTER
    ]
    [ imageView
        [ height config.imageSize
        , width config.imageSize
        , imageUrl config.imageUrl
        , visibility config.imageVisibility
        ]
    ]

centralView :: ∀ w . (Action  -> Effect Unit) -> Config -> PrestoDOM (Effect Unit) w
centralView push (Config config) =
    linearLayout
        [ height WRAP_CONTENT
        , width WRAP_CONTENT
        , gravity CENTER
        ]
        [ textView
            [ height WRAP_CONTENT
            , width WRAP_CONTENT
            , padding config.centerTextPadding
            , text config.text
            , color config.textColor
            , fontStyle config.textFont
            , textSize config.textSize
            , margin config.textMargin
            ]
        , textView
            [ height WRAP_CONTENT
            , width WRAP_CONTENT
            , padding config.buttonPadding
            , text config.buttonText
            , textSize config.buttonTextSize
            , visibility config.buttonVisibility
            , fontStyle config.buttonFont
            , color config.buttonColor
            , onClick push $ const ButtonClicked
            ]
        ]

sideView :: ∀ w . (Action  -> Effect Unit) -> Config -> PrestoDOM (Effect Unit) w
sideView push (Config config) =
    linearLayout
        [ height MATCH_PARENT
        , width WRAP_CONTENT
        , gravity CENTER
        , visibility config.radioButtonVisibility
        , padding config.sidePadding
        , onClick push $ const RadioClick
        ]
        [ imageView
            [ height config.radioButtonSize
            , width config.radioButtonSize
            , imageUrl config.radioButtonUrl
            , visibility config.radioButtonVisibility
            , onClick push $ const RadioClick
            ]
        , textView
            [ height WRAP_CONTENT
            , width WRAP_CONTENT
            , text config.sideButtonText
            , textSize config.buttonTextSize
            , visibility config.sideButtonVisibility
            , fontStyle config.sideButtonFont
            , color config.sideButtonColor
            , onClick push $ const ButtonClicked
            ]
        ]
