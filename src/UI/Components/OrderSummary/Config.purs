module UI.Components.OrderSummary.Config where

import HyperPrelude.Internal

data Config = Config
  { background :: String
  , height :: Length
  , typeface :: Typeface
  , font :: String
  , title :: String
  , padding :: Padding
  , shadow :: Number
  , primaryText :: String
  , secondaryText :: String
  , textColor :: String
  , primaryTextSize :: Int
  , secondaryTextSize :: Int
  , titleTextSize :: Int
  }

defConfig :: Config
defConfig = Config
  { background : "#ffffff"
  , height : V 72
  , typeface : BOLD
  , font : "Roboto-Regular"
  , title : "Mobile Recharge"
  , padding : (Padding 20 18 20 18)
  , shadow : 8.0
  , primaryText : ""
  , secondaryText : ""
  , textColor : "#000000"
  , primaryTextSize : 20
  , secondaryTextSize : 22
  , titleTextSize : 13
}
