module UI.Components.OrderSummary.View where

import HyperPrelude.Internal (Gravity(..), Length(..), Orientation(..), PrestoDOM, background, color, fontStyle, gravity, height, linearLayout, orientation, padding, text, textSize, textView, translationZ, typeface, weight, width)
import HyperPrelude.External (Effect, Unit, ($))

import UI.Components.OrderSummary.Config (Config(..))
import UI.Components.OrderSummary.Controller (Action)

view :: ∀ w . (Action -> Effect Unit) -> Config -> PrestoDOM (Effect Unit) w
view push (Config config) =
  linearLayout
    [ width MATCH_PARENT
    , height config.height
    , orientation VERTICAL
    , background config.background
    , padding config.padding
    , translationZ config.shadow
    ]
    [ linearLayout
        [ width MATCH_PARENT
        , height MATCH_PARENT
        , orientation HORIZONTAL
        ]
        [ getTextView (Config config)
        , linearLayout
            [ width $ V 0
            , weight 1.0
            , height MATCH_PARENT
            , orientation VERTICAL
            , gravity RIGHT
            ]
            [ textView
                [ width WRAP_CONTENT
                , height MATCH_PARENT
                , gravity CENTER
                , fontStyle config.font
                , textSize config.secondaryTextSize
                , typeface config.typeface
                , text config.secondaryText
                , color config.textColor
                ]
            ]
        ]
    ]


getTextView :: ∀ w . Config -> PrestoDOM (Effect Unit) w
getTextView (Config config) = linearLayout
    [ width $ V 0
    , weight 1.0
    , height MATCH_PARENT
    , orientation VERTICAL
    ]
    [ textView
        [ width MATCH_PARENT
        , height WRAP_CONTENT
        , text config.title
        , textSize config.titleTextSize
        ]
    , textView
        [ width MATCH_PARENT
        , height WRAP_CONTENT
        , text config.primaryText
        , fontStyle config.font
        , textSize config.primaryTextSize
        , color config.textColor
        ]
    ]
