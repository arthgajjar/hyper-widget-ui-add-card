module UI.Components.ToolBar.Config where

import HyperPrelude.Internal (Gravity(..), Length(..), Margin(..), Padding(..), Visibility(..))
import UI.Constant.Str.Default as STR
data Config = Config
  { background :: String
  , backIconVisibility :: Visibility
  , text :: String
  , textColor :: String
  , textSize :: Int
  , textGravity :: Gravity
  , textVisibility :: Visibility
  , textPadding :: Padding
  , height :: Length
  , imageUrl :: String
  , contentMargin :: Margin
  , font :: String
  , useBackgroundImage :: Boolean
  , backgroundImage :: String
  , translation :: Number
  , backIconSize :: Length
}

defConfig :: Config
defConfig = Config
  { background : "#C41C22"
  , backIconVisibility : VISIBLE
  , text : STR.getPayment "english"
  , textColor : "#FFFFFF"
  , textSize : 22
  , textVisibility : VISIBLE
  , textGravity : CENTER_VERTICAL
  , textPadding : (Padding 12 0 0 0)
  , height : V 64
  , imageUrl : ""
  , contentMargin : (Margin 0 10 0 0)
  , font : "Roboto-Regular"
  , useBackgroundImage : false
  , backgroundImage : ""
  , translation : 0.0
  , backIconSize : V 20
}
