module UI.Components.ToolBar.View where

import HyperPrelude.Internal (Gravity(..), Length(..), Margin(..), Orientation(..), PrestoDOM, Visibility(..), background, backgroundDrawable, clickable, color, fontStyle, gravity, height, imageUrl, imageView, linearLayout, margin, onClick, orientation, padding, text, textSize, textView, translationZ, visibility, weight, width)
import HyperPrelude.External (Effect, Unit, const, ($), (+), (<>))

import UI.Components.ToolBar.Config (Config(..))
import UI.Components.ToolBar.Controller (Action(..))

view :: ∀ w . (Action -> Effect Unit) -> Config  -> PrestoDOM (Effect Unit) w
view push c@(Config config) =
    linearLayout
    ([ height (getParentHeight config.height config.contentMargin)
    , width MATCH_PARENT
    , translationZ config.translation
    ] <> if config.useBackgroundImage
                then [ backgroundDrawable $ config.backgroundImage]
                else [ background $ config.background])
        -- , gradient (Linear 20.0 ["#F70000", "#A90000"])
        -- , prop (PropName "gradientAngle") "BR_TL"
    [
        linearLayout
        [ height config.height
        , width MATCH_PARENT
        , orientation HORIZONTAL
        , gravity $ CENTER
        , margin (refineContentMargin config.contentMargin)
        ]
        [ getImageView push c
        , textView
            [ height WRAP_CONTENT
            , weight 1.0
            , padding $ config.textPadding
            , gravity $ config.textGravity
            , color config.textColor
            , textSize config.textSize
            , text $ config.text
            , fontStyle config.font
            ]
        ]
    ]

getImageView :: ∀ w . (Action -> Effect Unit) -> Config  -> PrestoDOM (Effect Unit) w
getImageView push (Config config) = linearLayout
    [ height MATCH_PARENT
    , width WRAP_CONTENT
    , orientation HORIZONTAL
    , gravity CENTER
    , onClick push (const Clicked)
    , visibility config.backIconVisibility
    , clickable case config.backIconVisibility of
                    VISIBLE -> true
                    _ -> false
    ]
    [ imageView
        [ height config.backIconSize
        , width config.backIconSize
        , margin (deriveImageMargin config.contentMargin)
        , imageUrl $ config.imageUrl
        ]
    ]

getParentHeight :: Length -> Margin -> Length
getParentHeight h m =
    case h, m of
        WRAP_CONTENT, _         -> WRAP_CONTENT
        MATCH_PARENT, _         -> MATCH_PARENT
        V x, (Margin l t r b)   -> V (x + t)
        V x, _                  -> V (x + 10) -- Case to handle all other Margin Constructors.


refineContentMargin :: Margin -> Margin
refineContentMargin m =
    case m of
        Margin l t r b  -> Margin 0 t r b
        MarginLeft l    -> MarginLeft 0
        _               -> m

deriveImageMargin :: Margin -> Margin
deriveImageMargin m =
    case m of
        Margin l t r b  -> MarginLeft l
        MarginLeft l    -> m
        _               -> Margin 0 0 0 0
