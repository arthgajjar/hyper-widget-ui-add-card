module UI.Constant.Str.Default where


foreign import getTextValue':: String -> String -> String

getOtpSentHeader :: String -> String
getOtpSentHeader language = getTextValue' language "otp_sent_text"

getFreqBankHeader :: String -> String
getFreqBankHeader language = getTextValue' language "freq_header"

getMoreBanksHeader :: String -> String
getMoreBanksHeader language = getTextValue' language "more_banks"

getSavedPayHeader :: String -> String
getSavedPayHeader language = getTextValue' language "saved_options"

getUpiAppsHeader :: String -> String
getUpiAppsHeader language = getTextValue' language "upi_apps_header"

getSavedUpiHeader :: String -> String
getSavedUpiHeader language = getTextValue' language "saved_upi"

getPayLaterHeader :: String -> String
getPayLaterHeader language = getTextValue' language "pay_later_header"

getAddUpiText :: String -> String
getAddUpiText language = getTextValue' language "add_upi"

getPaymentReqText :: String -> String
getPaymentReqText language = getTextValue' language "payment_req_text"

getOtpReqText :: String -> String
getOtpReqText language = getTextValue' language "otp_req_text"

getDeafultPaymentText :: String -> String
getDeafultPaymentText language = getTextValue' language "default_payment"

getCardNumberText :: String -> String
getCardNumberText language = getTextValue' language "card_number"

getExpiryText :: String -> String
getExpiryText language = getTextValue' language "expiry"

getExpiryHintText :: String -> String
getExpiryHintText language = getTextValue' language "expiry_hint"

getCvvText :: String -> String
getCvvText language = getTextValue' language "cvv"

getCvvHintText :: String -> String
getCvvHintText language = getTextValue' language "cvv_hint"

getCvvInfoText :: String -> String
getCvvInfoText language = getTextValue' language "cvv_information_text"

getSaveCardText :: String -> String
getSaveCardText language = getTextValue' language "save_card"

getSaveCardInfoText :: String -> String
getSaveCardInfoText language = getTextValue' language "save_card_information_text"

getDeafultPaymentOptionHeader :: String -> String
getDeafultPaymentOptionHeader language = getTextValue' language "def_pay_option"

getPayOfferHeader :: String -> String
getPayOfferHeader language = getTextValue' language "pay_offers"

getOtherPayHeader :: String -> String
getOtherPayHeader language = getTextValue' language "other_pay"

getAddCardHeader :: String -> String
getAddCardHeader language = getTextValue' language "enter_card_details"

getLinkWalletHeader :: String -> String
getLinkWalletHeader language = getTextValue' language "link_wallet_header"

getMoreWalletHeader :: String -> String
getMoreWalletHeader language = getTextValue' language "more_wallet_header"

getPaymentSuccessFul :: String -> String
getPaymentSuccessFul language = getTextValue' language "payment_successfull"

getRecommendedPayHeader :: String -> String
getRecommendedPayHeader language = getTextValue' language "recomended_pay_header"

getUpiCollectReq :: String -> String
getUpiCollectReq language =  getTextValue' language "upi_collect_req"

getUpiCollect :: String -> String
getUpiCollect language = getTextValue' language "upi_collect"

getUpiApp :: String -> String
getUpiApp language = getTextValue' language "upi_apps"

getUsernameHint :: String -> String
getUsernameHint language = getTextValue' language "username_hint"

getCvvHide :: String -> String
getCvvHide language = getTextValue' language "cvv_hide"

getUpiIntent :: String -> String
getUpiIntent language = getTextValue' language "upi_intent"

getCheckboxMsg :: String -> String
getCheckboxMsg language = getTextValue' language "save_card_intent"

getValidThru :: String -> String
getValidThru language = getTextValue' language "valid_thru"

getSecurityCode :: String -> String
getSecurityCode language = getTextValue' language "security_code"

getCardNumberHolder :: String -> String
getCardNumberHolder language = getTextValue' language "enter_card_num"

getSearchHint :: String -> String
getSearchHint language = getTextValue' language "search"

getNetBanking :: String -> String
getNetBanking language = getTextValue' language "netbanking"

getWallet:: String -> String
getWallet language = getTextValue' language "wallet"

getWallets:: String -> String
getWallets language = getTextValue' language "wallets"

getUpi :: String -> String
getUpi language = getTextValue' language "upi"

getEMI :: String -> String
getEMI language = getTextValue' language "emi"

getUpiHeader :: String -> String
getUpiHeader language = getTextValue' language "upi_header"

getEMIHeader :: String -> String
getEMIHeader language = getTextValue' language "emi_header"

getProcessingPayment :: String -> String
getProcessingPayment language = getTextValue' language "processing_payment"

getCardOpt :: String -> String
getCardOpt language = getTextValue' language "cardopt"

getCard :: String -> String
getCard language = getTextValue' language "card"

getNumberHint :: String -> String
getNumberHint language = getTextValue' language "number_hint"

getSendOTP :: String -> String
getSendOTP language = getTextValue' language "send_otp"

getDelink :: String -> String
getDelink language = getTextValue' language "delink"

getNoLink :: String -> String
getNoLink language = getTextValue' language "noLink"

getDelinkWallet :: String -> String
getDelinkWallet language = getTextValue' language "delink_wallet"

getEnterOtp :: String -> String
getEnterOtp language = getTextValue' language "enter_otp"

getProceed :: String -> String
getProceed language = getTextValue' language "proceed"

getLinkNow :: String -> String
getLinkNow language = getTextValue' language "link_now"

getCardOnDelivery :: String -> String
getCardOnDelivery language = getTextValue' language "card_on_delivery"

getPayNow :: String -> String
getPayNow language = getTextValue' language "pay_now"

getNoBank :: String -> String
getNoBank language = getTextValue' language "no_bank"

getWalletHint :: String -> String
getWalletHint language = getTextValue' language "wallet_hint"

getCOD :: String -> String
getCOD language = getTextValue' language "cod"

getAAF :: String -> String
getAAF language = getTextValue' language "aaf"


getNone :: String -> String
getNone language = getTextValue' language "none"

getLink :: String -> String
getLink language = getTextValue' language "link"

getSubmit :: String -> String
getSubmit language = getTextValue' language "submit"

getPay :: String -> String
getPay language = getTextValue' language "pay"

getAdd :: String -> String
getAdd language = getTextValue' language "add"

getProceedToPay :: String -> String
getProceedToPay language = getTextValue' language "proceed_to_pay"

getYourBill :: String -> String
getYourBill language = getTextValue' language "your_bill"

getDebitCard :: String -> String
getDebitCard language = getTextValue' language "debit_card"

getDebitCardNum :: String -> String
getDebitCardNum language = getTextValue' language "debit_card_num"

getCollectText :: String -> String
getCollectText language = getTextValue' language "collectText"

getSuccessText:: String -> String
getSuccessText language = getTextValue' language "successText"

getPaymentTimeOut :: String -> String
getPaymentTimeOut language = getTextValue' language "paymentTimedOut"

getOtherError :: String -> String
getOtherError language = getTextValue' language "otherError"

getPayment :: String -> String
getPayment language = getTextValue' language "payment"

cardNumberError :: String -> String
cardNumberError language = getTextValue' language "recheckCardNum"

expiryDateError :: String -> String
expiryDateError language = getTextValue' language "recheckExpiry"

cvvError :: String -> String
cvvError language = getTextValue' language "recheckCvv"

getVoucherAppliedHeader :: String -> String
getVoucherAppliedHeader language = getTextValue' language "voucher_applied_header"

cardNotSupError :: String -> String
cardNotSupError language = getTextValue' language "card_not_sup"

cardExpired :: String -> String
cardExpired language = getTextValue' language "cardExpired"

invalidExpiry :: String -> String
invalidExpiry language = getTextValue' language "invalid_expiry"

invalidMonth :: String -> String
invalidMonth language = getTextValue' language "invalidMonth"

invalidCVV:: String -> String
invalidCVV language = getTextValue' language "invalidCVV"

paybill :: String -> String
paybill language = getTextValue' language "paybill"

verifyCard :: String -> String
verifyCard language = getTextValue' language "verifyCard"

addNewCard :: String -> String
addNewCard language = getTextValue' language "addNewCard"

unknownError :: String -> String
unknownError language = getTextValue' language "unknownError"

downMsg :: String -> String
downMsg language = getTextValue' language "downMsg"

downMsg1 :: String -> String
downMsg1 language = getTextValue' language "downMsg1"

downMsg2 :: String -> String
downMsg2 language = getTextValue' language "downMsg2"

delinkSuccess :: String -> String
delinkSuccess language = getTextValue' language "delinkSuccess"

delinkFail :: String -> String
delinkFail language = getTextValue' language "delinkFail"

linkSuccess :: String -> String
linkSuccess language = getTextValue' language "linkSuccess"

unableToLink :: String -> String
unableToLink language = getTextValue' language "unableToLink"

placeOrder :: String -> String
placeOrder language = getTextValue' language "placeOrder"

balance :: String -> String
balance language = getTextValue' language "balance"

paymentRs200 :: String -> String
paymentRs200 language = getTextValue' language "paymentRs200"

disabled :: String -> String
disabled language = getTextValue' language "disabled"

itemRs250 :: String -> String
itemRs250 language = getTextValue' language "itemRs250"

errorText4 :: String -> String
errorText4 language = getTextValue' language "errorText4"

aafProcessingText :: String -> String
aafProcessingText language = getTextValue' language "aaf_processingText"

aafRefresh :: String -> String
aafRefresh language = getTextValue' language "aaf_refresh"

aafProcessing :: String -> String
aafProcessing language = getTextValue' language "aaf_processing"

getMobileNumberText :: String -> String
getMobileNumberText language = getTextValue' language "mobile_number"

getUPIText :: String -> String
getUPIText language = getTextValue' language "get_upi_textbox_heading"

getMandateSupportError :: String
getMandateSupportError = "Card not eligible for autopay. Please try other card"

getMandateSupportWarning :: String
getMandateSupportWarning = "Autopay will not be enabled for this card number for automatic subscription renewals."

getPayLaterError :: String
getPayLaterError = "You are not eligible to use "

getLoginMobile :: String
getLoginMobile =  "login_mobile"

getLoginOtp :: String
getLoginOtp =  "login_otp"

getSiNotSupported :: String
getSiNotSupported =  "si_not_supported"
