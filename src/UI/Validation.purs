module UI.Validation where

import Prelude
import Data.Either (hush)
import Data.Maybe (Maybe(..))
import Data.String as StrUtils
import Data.String.Regex (Regex, test, regex)
import Data.String.Regex.Flags (global, ignoreCase)

validateVPA :: String -> Boolean
validateVPA vpa =
  case vpaRegex of
    Just reg -> test reg vpa
    Nothing -> false
  where
  vpaRegex :: Maybe Regex
  vpaRegex =
    hush $
    regex
      "^[a-zA-Z0-9]([a-zA-Z0-9\\.\\-]{1,23})[a-zA-Z0-9]@[a-zA-Z0-9]{3,25}$"
      (global <> ignoreCase)

validateMobileNumber :: String -> Boolean
validateMobileNumber mobileNum = (StrUtils.length mobileNum == 10)

validateWalletOtp :: String -> String -> Boolean
validateWalletOtp walletName otp = do
    let otpLength = getWalletOtpLength walletName
    let otpLengthStatus = if otpLength == 0
                            then "OtpLengthUnknown"
                            else "OtpLengthKnown"
    case otpLengthStatus of
            "OtpLengthUnknown" -> (StrUtils.length otp) >= 4
            "OtpLengthKnown" -> (StrUtils.length otp) == otpLength
            _ -> (StrUtils.length otp) >= 4


getWalletOtpLength :: String -> Int
getWalletOtpLength walletName = do
  case walletName of
    "FREECHARGE" -> 4
    "MOBIKWIK" -> 6
    "PHONEPE" -> 5
    "PAYTM" -> 6
    _ -> 0

data ValidationType = VPA | MOBILE |  WALLET_OTP String

validateData :: ValidationType -> String -> Boolean
validateData validationType dataValue = do
    case validationType of
        VPA -> validateVPA dataValue
        MOBILE -> validateMobileNumber dataValue
        WALLET_OTP walletName-> validateWalletOtp walletName dataValue
