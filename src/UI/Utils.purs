module UI.Utils where

import Prelude

import Data.Array ((!!))
import Data.Int (toNumber)
import Data.Maybe (Maybe(..), fromMaybe)
import Effect (Effect)
import Engineering.Helpers.Commons (AffSuccess)
import PPConfig.Utils as CPUtils
import PrestoDOM (ElemName(..), Leaf, Length(..), Orientation(..), Prop, PropName(..), VDom(..), PrestoDOM, height, orientation, prop, scrollView, scrollBarY, linearLayout, width)
import PrestoDOM as PD
import PrestoDOM.Animation as PrestoAnim
import PrestoDOM.Elements.Elements (Node, keyed)
import PrestoDOM.Elements.Keyed (KeyedNode)
import PrestoDOM.Elements.Keyed as Keyed
import Remote.Types (ConfigPayload(..))
import Type.Data.Boolean (kind Boolean)
import Web.Event.Event (Event) as DOM

type MicroAPPInvokeSignature = ∀ a. {|a} -> AffSuccess { code:: Int, status :: String } -> (∀ val. val -> Effect Unit) -> Effect Unit

foreign import _expiryHandler :: ∀ a. String -> DOM.Event -> (a ->  Effect Unit) -> Effect Unit
foreign import getOs :: Unit -> String
foreign import resetTextFields :: Array String -> Effect Unit
foreign import setText' :: String -> String -> Effect Unit
foreign import date :: ∀ a. a ->  String
foreign import screenHeight :: Unit -> Int
foreign import screenWidth :: Unit -> Int
foreign import loaderAfterRender :: String -> Unit
foreign import resetListKeyBoard :: Effect Unit
foreign import getLoaderConfig :: forall a. {|a}
foreign import markDownToHTML :: String -> String
foreign import getUPIId :: Unit -> String
foreign import redirectUPIScreen :: Boolean -> String -> Unit
foreign import safeMarginTop :: Unit -> Int
foreign import safeMarginBottom :: Unit -> Int

-- data ProcessStatus
--     = NoProcess
--     | Processing
--     | ProcessingDone

mapDom'
  :: ∀ a b state w
   . ((a -> Effect Unit) -> state -> PD.PrestoDOM (Effect Unit) w)
  -> (b -> Effect Unit)
  -> state
  -> (a -> b)
  -> PD.PrestoDOM (Effect Unit) w
mapDom' view push state actionMap = view (push <<< actionMap) state

data FieldType = CardNumber | ExpiryDate | CVV | SavedCardCVV | NONE -- | Name | SavedForLater

derive instance eqFieldType :: Eq FieldType

-- Generate ids instead of hard coding.
getFieldTypeID :: FieldType -> String
getFieldTypeID =
    case _ of
         CardNumber -> "987654321"
         ExpiryDate -> "987654322"
         CVV        -> "987654323"
         SavedCardCVV -> "987654329"
         NONE       -> "987654320" -- replace with text field selection condition

resetText :: Array String -> Effect Unit
resetText = resetTextFields
os :: String
os = getOs unit

ifAndroidThen :: ∀ a. a -> a -> a
ifAndroidThen a b = if os == "ANDROID" then a else b

ifIOSThen :: ∀ a. a -> a -> a
ifIOSThen a b = if os == "IOS" then a else b

isiPhone5Then :: Int -> Int -> Int
isiPhone5Then x y = if (screenWidth unit) <= 320 then x else y

-- | Boolean String
multipleLine :: ∀ i. String -> Prop i
multipleLine = prop (PropName "multipleLine")

-- | Boolean String
swipeEnabled :: ∀ i. String -> Prop i
swipeEnabled = prop (PropName "swipeEnable")

-- | Boolean String
becomeFirstResponder :: ∀ i. String -> Prop i
becomeFirstResponder = prop (PropName "becomeFirstResponder")

-- | String
placeHolder :: ∀ i. String -> Prop i
placeHolder = prop (PropName "placeHolder")

-- | String
autoCorrectionType :: ∀ i. String -> Prop i
autoCorrectionType = prop (PropName "autoCorrectionType")

-- | String
imeOptions :: ∀ i. Int -> Prop i
imeOptions = prop (PropName "imeOptions")

bringToFront :: ∀ i. Boolean -> Prop i
bringToFront = prop (PropName "bringSubViewToFront")

clipToOutline :: ∀ i. Boolean -> Prop i
clipToOutline = prop (PropName "clipToOutline")

backgroundTint :: ∀ i. String -> Prop i
backgroundTint = prop (PropName "backgroundTint")

showCursor :: ∀ i. Boolean -> Prop i
showCursor = prop (PropName "cursor")

longClickable :: ∀ i. Boolean -> Prop i
longClickable = prop (PropName "longClickable")

userInteraction :: ∀ i. Boolean -> Prop i
userInteraction = prop (PropName "userInteraction")

-- | Boolie
baseAlign :: ∀ i. Boolean -> Prop i
baseAlign = prop (PropName "baseAlign")

androidShadow :: ∀ i. String -> Prop i
androidShadow = prop (PropName "androidShadow")

shadowTag :: ∀ i. String -> Prop i
shadowTag = prop (PropName "shadowTag")

packageIcon :: ∀ i. String -> Prop i
packageIcon = prop (PropName "packageIcon")

scrollEnabled :: ∀ i. String -> Prop i
scrollEnabled =  prop (PropName "scrollEnabled")

scrollTo :: ∀ i. String -> Prop i
scrollTo =  prop (PropName "scrollTo")

focusString :: ∀ i. String -> Prop i
focusString = prop (PropName "focus")

focus :: ∀ i. Boolean -> Prop i
focus = prop (PropName "focus")

focusX :: ∀ i. Boolean -> Prop i
focusX true = prop (PropName "focus") true
focusX false = prop (PropName "focusOut") true

contentMode :: ∀ i. String -> Prop i
contentMode = prop (PropName "contentMode")

separator :: ∀ i. String -> Prop i
separator = prop (PropName "separator")

separatorRepeat :: ∀ i. String -> Prop i
separatorRepeat = prop (PropName "separatorRepeat")

editFieldType :: ∀ i. String -> Prop i
editFieldType = prop (PropName "editFieldType")

lottieAnimationView :: ∀ i p. Leaf (Prop i) p
lottieAnimationView = leaf $ case os of
                                "ANDROID" -> "lottieAnimationView"
                                _ -> "linearLayout"

leaf :: ∀ i p. String -> Leaf (Prop i) p
leaf elem props = element (ElemName elem) props []

element :: ∀ i p. ElemName -> Array (Prop i) -> Array (VDom (Array (Prop i)) p) -> VDom (Array (Prop i)) p
element elemName = Elem Nothing elemName

-------------------------------------------------

node :: ∀ i p. String -> Node (Prop i) p
node elem = element (ElemName elem)

keyedNode :: ∀ i p. String -> KeyedNode (Prop i) p
keyedNode elem = keyed (ElemName elem)

accordionLayout :: ∀ i p. Node (Prop i) p
accordionLayout = node "accordionLayout"

defaultExpand :: ∀ i. Boolean -> Prop i
defaultExpand = prop (PropName "defaultExpand")

expandAlpha :: ∀ i. Number -> Prop i
expandAlpha = prop (PropName "expandAlpha")

expandScrollParent :: ∀ i. Int -> Prop i
expandScrollParent = prop (PropName "expandScrollParent")

expandDuration :: ∀ i. Int -> Prop i
expandDuration = prop (PropName "expandDuration")

expand :: ∀ i. Boolean -> Prop i
expand = prop (PropName "expand")

swypeLayout :: ∀ i p. Node (Prop i) p
swypeLayout = node "swypeLayout"

swypeScroll :: ∀ i p. Node (Prop i) p
swypeScroll props children =
    if os == "IOS"
        then node "listView" props children
        else androidSwype
                props
                [ PD.linearLayout
                    [ height WRAP_CONTENT
                    , width MATCH_PARENT
                    , orientation VERTICAL
                    ]
                    children
                ]

androidSwype :: ∀ i p. Node (Prop i) p
androidSwype = node "swypeScroll"

keyedSwypeScroll :: ∀ i p. KeyedNode (Prop i) p
keyedSwypeScroll props children =
    if os == "IOS"
        then keyedNode "listView" props children
        else node "scrollView"
                props
                [ Keyed.linearLayout
                    [ height WRAP_CONTENT
                    , width MATCH_PARENT
                    , orientation VERTICAL
                    ]
                    children
                ]

animateTime :: ConfigPayload -> Int
animateTime (ConfigPayload configPayload) = CPUtils.getScreenTransitionDuration (ConfigPayload configPayload)

animationColor :: ConfigPayload -> String
animationColor (ConfigPayload configPayload) = CPUtils.getAnimationColor (ConfigPayload configPayload)

delayTime :: ConfigPayload -> Number
delayTime (ConfigPayload configPayload) = toNumber $ CPUtils.getScreenTransitionDuration (ConfigPayload configPayload)

getInterpolator :: ConfigPayload -> PrestoAnim.Interpolator
getInterpolator (ConfigPayload configPayload) =
    let beizerArray = CPUtils.getScreenTransitionCurve (ConfigPayload configPayload) in
    PrestoAnim.Bezier (fromMaybe 0.0 $ beizerArray !! 0) (fromMaybe 0.0 $ beizerArray !! 1) (fromMaybe 0.0 $ beizerArray !! 2) (fromMaybe 0.0 $ beizerArray !! 3)

translateInXAnim :: ConfigPayload -> Int -> Boolean -> PrestoAnim.Animation
translateInXAnim (ConfigPayload configPayload) x =
    PrestoAnim.Animation
    [ PrestoAnim.duration $ animateTime (ConfigPayload configPayload)
    , PrestoAnim.fromX x
    , PrestoAnim.toX 0
    , PrestoAnim.tag "slideIn"
    , PrestoAnim.interpolator $ getInterpolator (ConfigPayload configPayload)
    ]
--  PrestoAnim.Bounce --

translateOutXAnim :: ConfigPayload -> Int -> Boolean -> PrestoAnim.Animation
translateOutXAnim (ConfigPayload configPayload) x =
    PrestoAnim.Animation
    [ PrestoAnim.duration $ animateTime (ConfigPayload configPayload)
    , PrestoAnim.toX x
    , PrestoAnim.tag "slideOut"
    , PrestoAnim.interpolator $ getInterpolator (ConfigPayload configPayload)
    ]

translateInYAnim :: ConfigPayload -> Int -> Boolean -> PrestoAnim.Animation
translateInYAnim (ConfigPayload configPayload) y =
    PrestoAnim.Animation
    [ PrestoAnim.duration $ animateTime (ConfigPayload configPayload)
    , PrestoAnim.fromY y
    , PrestoAnim.toY 0
    , PrestoAnim.tag "slideIn"
    , PrestoAnim.interpolator $ getInterpolator (ConfigPayload configPayload)
    ]

translateOutYAnim :: ConfigPayload -> Int -> Boolean -> PrestoAnim.Animation
translateOutYAnim (ConfigPayload configPayload) y =
    PrestoAnim.Animation
    [ PrestoAnim.duration $ animateTime (ConfigPayload configPayload)
    , PrestoAnim.toY y
    , PrestoAnim.tag "slideOut"
    , PrestoAnim.interpolator $ getInterpolator (ConfigPayload configPayload)
    ]

fadeInAnim :: ConfigPayload -> Boolean -> PrestoAnim.Animation
fadeInAnim (ConfigPayload configPayload) =
    PrestoAnim.Animation
    [ PrestoAnim.duration $ animateTime (ConfigPayload configPayload)
    , PrestoAnim.fromAlpha 0.0
    , PrestoAnim.toAlpha 1.0
    , PrestoAnim.tag "fadeIn"
    -- , PrestoAnim.interpolator $ getInterpolator (ConfigPayload configPayload)
    ]

fadeInAnimTemp :: Boolean -> PrestoAnim.Animation
fadeInAnimTemp =
    PrestoAnim.Animation
    [ PrestoAnim.duration 200
    , PrestoAnim.fromAlpha 0.0
    , PrestoAnim.toAlpha 1.0
    , PrestoAnim.tag "fadeIn"
    -- , PrestoAnim.interpolator $ getInterpolator (ConfigPayload configPayload)
    ]

fadeOutAnimTemp :: Boolean -> PrestoAnim.Animation
fadeOutAnimTemp =
    PrestoAnim.Animation
    [ PrestoAnim.duration 200
    , PrestoAnim.fromAlpha 1.0
    , PrestoAnim.toAlpha 0.0
    , PrestoAnim.tag "fadeOut"
    -- , PrestoAnim.interpolator $ getInterpolator (ConfigPayload configPayload)
    ]

fadeOutAnim :: ConfigPayload -> Boolean -> PrestoAnim.Animation
fadeOutAnim (ConfigPayload configPayload) =
    PrestoAnim.Animation
    [ PrestoAnim.duration $ animateTime (ConfigPayload configPayload)
    , PrestoAnim.fromAlpha 1.0
    , PrestoAnim.toAlpha 0.0
    , PrestoAnim.tag "fadeOut"
    -- , PrestoAnim.interpolator $ getInterpolator (ConfigPayload configPayload)
    ]

scaleAnim :: ConfigPayload ->  Boolean -> PrestoAnim.Animation
scaleAnim (ConfigPayload configPayload) =
  PrestoAnim.Animation
    [ PrestoAnim.duration 3000
    , PrestoAnim.toScaleX 0.0
    , PrestoAnim.fromScaleX 1.0
    , PrestoAnim.toScaleY 0.0
    , PrestoAnim.fromScaleY 1.0
    , PrestoAnim.repeatMode PrestoAnim.Reverse
    , PrestoAnim.repeatCount PrestoAnim.Infinite
    , PrestoAnim.interpolator $ getInterpolator (ConfigPayload configPayload)
    ]

rotateAnim :: Boolean -> PrestoAnim.Animation
rotateAnim  =
  PrestoAnim.Animation
    [ PrestoAnim.duration 3000
    , PrestoAnim.fromRotation 0
    , PrestoAnim.fromRotationY 0
    , PrestoAnim.toRotation 360
    , PrestoAnim.toRotationY 360
    , PrestoAnim.fromRotationX 0
    , PrestoAnim.toRotationX 360
    , PrestoAnim.repeatMode PrestoAnim.Restart
    , PrestoAnim.repeatCount PrestoAnim.Infinite
    -- , PrestoAnim.interpolator $ getInterpolator configPayload
    ]

loaderAnim :: Int -> Int -> Int -> Boolean -> PrestoAnim.Animation
loaderAnim fromXValue toXValue duration ifAnim =
   PrestoAnim.Animation
   [ PrestoAnim.duration duration
    , PrestoAnim.fromX fromXValue
    , PrestoAnim.toX toXValue
    , PrestoAnim.repeatCount (PrestoAnim.Repeat 0)
    , PrestoAnim.interpolator $ PrestoAnim.Bezier 0.94 0.94 1.0 1.0
   ] ifAnim


popupAnimation ::
    forall w. ConfigPayload
    -> Boolean
    -> Maybe (PrestoDOM (Effect Unit) w
    -> PrestoDOM (Effect Unit) w)
popupAnimation configPayload showPopup =
    Just $ PrestoAnim.animationSet
        [ translateInYAnim configPayload (screenHeight unit) showPopup
        , translateOutYAnim configPayload (-(screenHeight unit)) (not showPopup)
        ]

materialEditTextAnimation :: Int → Boolean -> Boolean → PrestoAnim.Animation
materialEditTextAnimation yTrans isFocused underlined =
    if isFocused
    then PrestoAnim.Animation
            [ PrestoAnim.duration 100
            , PrestoAnim.toY (- yTrans)
            , PrestoAnim.toX if underlined then 0 else 4
            , PrestoAnim.tag "slideOut"
            , PrestoAnim.toScaleX 0.75
            , PrestoAnim.fromScaleX 1.0
            , PrestoAnim.toScaleY 0.75
            , PrestoAnim.fromScaleY 1.0
            ] true
    else PrestoAnim.Animation
            [ PrestoAnim.duration 100
            , PrestoAnim.toY yTrans
            , PrestoAnim.tag "slideIn"
            , PrestoAnim.toScaleX 1.0
            , PrestoAnim.fromScaleX 0.75
            , PrestoAnim.toScaleY 1.0
            , PrestoAnim.fromScaleY 0.75
            ] true

-- generatePayload :: ∀ a. Payment -> {|a}
-- generatePayload (Payment payment) = do
--   let (Authentication auth) = payment.authentication
--   case auth.method of
--     "GET" ->
--       generatePayload' auth.url "null"
--     _ ->
--       generatePayload' auth.url (fromMaybe "null" $  auth.params)

----------------------------------------------------------------------------------------------------------------

data ModalAction = ClickedOutside

data LineItemAction = InfoClick


-- Margins & Padding managers

-- This thing is applicable for element's config :: like SearchBox
uiCardPadding :: ConfigPayload -> PD.Padding
uiCardPadding configPayload = let
    verticalPadding = CPUtils.uiCardVerticalPadding configPayload
    horizontalPadding = CPUtils.uiCardHorizontalPadding configPayload
    in (PD.Padding horizontalPadding verticalPadding horizontalPadding verticalPadding)

uiCardHorPadding :: ConfigPayload -> PD.Padding
uiCardHorPadding configPayload = let
    horizontalPadding = CPUtils.uiCardHorizontalPadding configPayload
    in (PD.Padding horizontalPadding 0 horizontalPadding 0)

cardPadding :: ∀ i. ConfigPayload -> Prop i
cardPadding cp = PD.padding $ uiCardPadding cp

containerPadding :: ∀ i. ConfigPayload -> Prop i
containerPadding configPayload = let
    hSpace = CPUtils.horizontalSpace configPayload
    in PD.padding $ PD.Padding hSpace 0 hSpace 0

contentMargin :: ∀ i. ConfigPayload -> Prop i
contentMargin configPayload = let
    vSpace = CPUtils.verticalSpace configPayload
    in
    if (CPUtils.horizontalSpace configPayload) == 0
    then PD.margin $ PD.Margin 0 vSpace 0 0
    else PD.margin $ PD.Margin 0 vSpace 0 vSpace

sectionSpace :: ConfigPayload -> Int
sectionSpace cP = CPUtils.sectionSpace cP

sectionMargin :: ∀ i. ConfigPayload -> Prop i
sectionMargin cP = PD.margin $ PD.Margin 0 0 0 $ sectionSpace cP

translationMargin :: Int
translationMargin = 4

isVisible :: PD.Visibility -> Boolean
isVisible PD.VISIBLE = true
isVisible _ = false

horizontalFade :: ∀ i. Boolean -> Prop i
horizontalFade = prop (PropName "horizontalFade")

fadingEdgeLength :: ∀ i. Int -> Prop i
fadingEdgeLength = prop (PropName "fadingEdgeLength")

boolToVisibility :: Boolean -> PD.Visibility
boolToVisibility true = PD.VISIBLE
boolToVisibility false = PD.GONE

offerDescHTML :: String -> ConfigPayload -> String
offerDescHTML offerText configPayload =
  offerText <> " " <>
  "<span style=\"color:" <>
  CPUtils.primaryColor configPayload <>
  ";\"><u>T&C</u></span>"

wrapInScroll :: ∀ i. Array (PrestoDOM (Effect Unit) i) -> Array (PrestoDOM (Effect Unit) i)
wrapInScroll child =
  [ scrollView
      [ width MATCH_PARENT
      , height MATCH_PARENT
      , scrollBarY true
      ]
      [ linearLayout
          [ width MATCH_PARENT
          , height MATCH_PARENT
          , orientation VERTICAL
          ] child
      ]
  ]



foreign import _cvvHandler :: ∀ a. String -> DOM.Event -> (a ->  Effect Unit) -> Effect Unit
