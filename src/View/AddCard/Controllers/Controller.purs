module View.AddCard.Controllers.Controller where

import HyperPrelude.Internal (Eval, Props, afterRender, continue, continueWithCmd, updateAndExit,onBackPressed)
import HyperPrelude.External (Effect, Maybe, Milliseconds(..), Unit, bind, const, delay, launchAff_, liftEffect, not, pure, ($), (&&))

import Engineering.Helpers.Events (addCustomBackPress)
import JBridge as JBridge
import Engineering.Helpers.Commons (PaymentOffer)
import Remote.Types (ConfigPayload, MerchantOffer)
import Type.Data.Boolean (kind Boolean)
import UI.Components.AddCard.Controller as AddNewCard
import UI.Components.AddCard.Types as AddNewCardTypes
import UI.Components.ToolBar.Controller as ToolBar
import UI.Utils (FieldType(CardNumber), ModalAction(..), getFieldTypeID)
import UI.Components.Popup.Controller as Popup
import Service.EC.Types.Instruments as Instruments
import UI.Components.AddCard.Types (FormFieldState)
import Payments.NetBanking.Utils (Bank)
import Payments.Wallets.Types (MandateType)

type ScreenInput  =
  { configPayload :: ConfigPayload
  , merchantOffer :: MerchantOffer
  , amount :: Number
  , savedCards ::  Array Instruments.StoredCard
  , cardNumber ::  FormFieldState ()
  , cVV :: FormFieldState ()
  , expiry ::FormFieldState ()
  , mandateFeature :: Maybe Boolean
  , cardTypes :: Array String
  , mid :: String
  , allBanks :: Array Bank
  , allCards :: Array Instruments.StoredCard
  , allWallets :: Array Instruments.StoredWallet
  , creditOutages :: Array String
  , debitOutages :: Array String
  , mandateType :: MandateType
  , cardBinOffers :: Array PaymentOffer
  , phoneNumber :: String
  , orderDesc :: String
  , outages :: Array String
  , customerName :: String
  }

data ScreenOutput = AddCard AddNewCardTypes.State | OnBackPress | Switch String | OnGuestLogin | SwitchNav String | PayUsingNB String | PayUsingWallet  String String Boolean Boolean

data Action
  = AddCardAction AddNewCardTypes.Action
  | BackPressed
  | AddCardScreenRendered String
  | ContinueCommand
  | ToolBarAction ToolBar.Action
  | OverlayClick ModalAction
  | HideCvvToolTip
  | HideInfoToolTip
  | OfferPopupAction Popup.Action

type State =
    { addCardState :: AddNewCardTypes.State
    , entry :: Boolean
    , configPayload :: ConfigPayload
    , amount :: Number
    , phoneNumber :: String
    , orderDesc :: String
    , customerName :: String
    }

initialState ::  ScreenInput -> State
initialState input =
    { addCardState
    , entry: true
    , configPayload : input.configPayload
    , amount : input.amount
    , phoneNumber : input.phoneNumber
    , orderDesc : input.orderDesc
    , customerName : input.customerName
    }
    where
    addCardState =
        AddNewCard.initialState
            { supportedMethods : []
            , cardMethod : AddNewCardTypes.AddNewCard
            , configPayload : input.configPayload
            , merchantOffer : input.merchantOffer
            , amount : input.amount
            , cardBinOffers : input.cardBinOffers
            , mandateType : input.mandateType
            , outages : input.outages
            }


eval
  :: Action
  -> State
  -> Eval Action ScreenOutput State

eval action state = case action of

  BackPressed ->
    updateAndExit (state { entry = false }) $ OnBackPress

  ToolBarAction ToolBar.Clicked ->
    updateAndExit (state { entry = false }) $ OnBackPress

  OverlayClick ClickedOutside ->
    updateAndExit (state { entry = false }) $ OnBackPress

  AddCardAction (AddNewCardTypes.SubmitCard AddNewCardTypes.AddNewCard) -> do
    let updatedAddCardState = state.addCardState {
                                startButtonAnimation = true
                              , enableSI = state.addCardState.enableSI &&
                                            (not $ AddNewCard.interpretCNStatus state.addCardState.formState.cardNumber.status state.addCardState.mandate)}
    let updatedState = state { addCardState = updatedAddCardState }
    updateAndExit updatedState $ AddCard updatedState.addCardState

  AddCardAction act ->
    let updatedAddCardState = AddNewCard.eval act state.addCardState
    in continue $ state { addCardState = updatedAddCardState }

  AddCardScreenRendered id ->
    continueWithCmd state [
        do
        _ <- launchAff_ do
            _ <- delay (Milliseconds 300.0)
            liftEffect $ JBridge.requestKeyboardShow id
        pure $ ContinueCommand
    ]

  _ ->  continue state


overrides :: String -> (Action -> Effect Unit) -> State -> Props (Effect Unit)

overrides "MainLayout" push state =
  [ addCustomBackPress push $ const BackPressed
  , afterRender push $ const (AddCardScreenRendered (getFieldTypeID CardNumber))
  , onBackPressed push $ const BackPressed ]

overrides _ push state = []
