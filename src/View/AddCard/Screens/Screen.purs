module View.AddCard.Screens.Screen where

import HyperPrelude.External(Maybe(..),Effect,Unit,($),const,(<<<))
import HyperPrelude.Internal(Length(..), Orientation(..), PrestoDOM, Screen, background, height, linearLayout, orientation, width)
import Engineering.Helpers.Events (addCustomBackPress)
import PrestoDOM.Utils ((<>>))
import PPConfig.Utils as CPUtils
import UI.Components.AddCard.View as AddCard
import UI.Config as UIConfig
import View.AddCard.Controllers.Controller (Action(..), State, ScreenInput, ScreenOutput, eval, initialState, overrides)
import View.Stock.Container.Commons as UICommons

screen :: ScreenInput -> Screen Action State ScreenOutput
screen input =
    { initialState : (initialState input)
    , name : "AddCardScreen"
    , view
    , globalEvents : []
    , eval : eval
    }

view :: ∀ w  . (Action  -> Effect Unit) -> State  -> PrestoDOM (Effect Unit) w
view push state =
  UICommons.getParentLayout
    parentInput
    (UICommons.AddCardAction push)
    (addCardView)
    Nothing
  where
    parentInput =
      { modalView : ifModal
      , modalHeight : MATCH_PARENT
      , modalAnimationTrigger : entry
      , toolbarHeader : "Add Card"
      , configPayload : state.configPayload
      , useContainerPadding : true
      , useRelativeLayout : false
      , amount : state.amount
      , showToolbar : true
      , phoneNumber : state.phoneNumber
      , showAmountBar : true
      , orderDescription : state.orderDesc
      , customerName : state.customerName
      }

    addNewCardState = state.addCardState
    ifModal = CPUtils.ifModalView state.configPayload
    entry = state.entry
    addCardConfig = UIConfig.addCardConfig state.configPayload
    addCardView =
      [ linearLayout (
        [ height MATCH_PARENT
        , width MATCH_PARENT
        , orientation VERTICAL
        , addCustomBackPress push $ const BackPressed
        , background $ CPUtils.backgroundColor state.configPayload -- Background for scr
        ] <>> overrides "MainLayout" push state )
        [ AddCard.view addCardConfig (push <<< AddCardAction) addNewCardState ]
      ]
