module View.Stock.Container.Commons where

import HyperPrelude.External (drop,length,fromNumber, toNumber,Maybe(..), fromMaybe,Effect,(-),Unit,(/=),(&&),(<>),(<<<),($),not,(==),(>=),const,unit,(*),(>),negate)
import HyperPrelude.Internal (Gravity(..), Length(..), Margin(..), Orientation(..), Padding(..), PrestoDOM, Visibility(..), alignParentBottom, background, clickable, scrollBarX, horizontalScrollView, cornerRadius, gravity, height, imageUrl, imageView, linearLayout, margin, onClick, orientation, padding, relativeLayout, translationZ, visibility, weight, width)
import Data.String (length, split, replaceAll, Replacement(..), Pattern(..)) as S
import PPConfig.Utils as CPUtils
import Payments.Wallets.Types (Wallet)
import PrestoDOM.Animation as PrestoAnim
import PrestoDOM.Types.DomAttributes (Corners(..))
import Remote.Types (ConfigPayload)
import Service.EC.Types.Instruments as Instruments
import UI.Components.AmountBar.View as AmountBar
import UI.Components.Popup.Config as PopupConfig
import UI.Components.Popup.View as Popup
import UI.Components.ToolBar.Config as ToolBarConfig
import UI.Components.ToolBar.View as ToolBar
import UI.Config as UIConfig
import UI.Utils (ModalAction(..), cardPadding, containerPadding, fadeInAnim, fadeOutAnim, screenHeight, screenWidth, translateInXAnim, translateInYAnim, translateOutXAnim, translateOutYAnim)
import UI.Utils as UIUtils
import View.AddCard.Controllers.Controller as AddCard
import View.EMI.Controllers.CheckoutController as EMICheckout
import View.EMI.Controllers.InstrumentsController as EMIInstruments
import View.EMI.Controllers.PlansController as EMIPlans
import View.NB.Controller.Controller as NetBanking
import View.OffersPreview.Controllers.Controller as Offers
import View.PaymentManagement.Controllers.PaymentManagementController as PaymentManagement
import View.PaymentPage.Controllers.Types as PaymentPage
import View.QuickPay.Controllers.Controller as QuickPay
import View.UPI.Controllers.AddController as AddUPI
import View.UPI.Controllers.HomeController as UPIHome
import View.Wallet.Controllers.Controller as Wallet
import View.Wallet.Controllers.DelinkController as DelinkWallet
import View.Wallet.Controllers.OtpScreenController as Otp
import View.Wallet.Controllers.StatusController as Status
import View.Wallet.Controllers.VerifyNumberController as VerifyNumber
import UI.Components.Popup.Controller (Action(..))

type ParentLayoutInput =
  { modalView :: Boolean
  , modalHeight :: Length
  , modalAnimationTrigger :: Boolean
  , toolbarHeader :: String
  , useRelativeLayout :: Boolean
  , amount :: Number
  , configPayload :: ConfigPayload
  , useContainerPadding :: Boolean
  , showToolbar :: Boolean
  , phoneNumber :: String
  , showAmountBar :: Boolean
  , orderDescription :: String
  , customerName :: String
  }

data ActionType
  = PPAction (PaymentPage.Action -> Effect Unit)
  | PMAction (PaymentManagement.Action -> Effect Unit)
  | AddCardAction (AddCard.Action -> Effect Unit)
  | NetBankingAction (NetBanking.Action -> Effect Unit)
  | AddUpiAction (AddUPI.Action -> Effect Unit)
  | UpiHomeAction (UPIHome.Action -> Effect Unit)
  | WalletAction (Wallet.Action -> Effect Unit)
  | DelinkWalletAction (DelinkWallet.Action -> Effect Unit)
  | OtpAction (Otp.Action -> Effect Unit)
  | StatusAction (Status.Action -> Effect Unit)
  | VerifyNumberAction (VerifyNumber.Action -> Effect Unit)
  | QuickPayAction (QuickPay.Action -> Effect Unit)
  | EMIInstrumentsAction (EMIInstruments.Action -> Effect Unit)
  | EMIPlansAction (EMIPlans.Action -> Effect Unit)
  | EMICheckoutAction (EMICheckout.Action -> Effect Unit)
  | OffersAction (Offers.Action -> Effect Unit)

parentLayout :: ∀ w. Array (PrestoDOM (Effect Unit) w) -> Boolean -> PrestoDOM (Effect Unit) w
parentLayout child modalView=
  linearLayout (
    [ height MATCH_PARENT
    , width MATCH_PARENT
    , orientation VERTICAL
    , visibility VISIBLE
    , clickable true
    ] <> if modalView
          then [background "#BF000000"]
          else [] )
    (child)

gridScrollView :: ∀ w. ConfigPayload -> Array (PrestoDOM (Effect Unit) w) -> PrestoDOM (Effect Unit) w
gridScrollView configPayload   =
  horizontalScrollView
    [ height WRAP_CONTENT
    , width MATCH_PARENT
    , scrollBarX false
    , UIUtils.horizontalFade (CPUtils.horizontalFade configPayload)
    , UIUtils.fadingEdgeLength (CPUtils.fadingEdgeLength configPayload)
    , background (CPUtils.uiCardColor configPayload)
    ]

toolbarLayout :: ∀ w. Boolean -> String -> ActionType -> ConfigPayload -> Array (PrestoDOM (Effect Unit) w)
toolbarLayout modalView toolbarHeader push configPayload =
  if modalView then []
  else [ ToolBar.view action (ToolBarConfig.Config updatedToolbarConfig)]
  where
  action = case push of
    PPAction x              -> (x <<< PaymentPage.ToolBarAction)
    PMAction x              -> (x <<< PaymentManagement.ToolBarAction)
    AddCardAction x         -> (x <<< AddCard.ToolBarAction )
    NetBankingAction x      -> (x <<< NetBanking.ToolBarAction)
    AddUpiAction x          -> (x <<< AddUPI.ToolBarAction )
    UpiHomeAction x         -> (x <<< UPIHome.ToolBarAction)
    WalletAction x          -> (x <<< Wallet.ToolBarAction)
    DelinkWalletAction x    -> (x <<< DelinkWallet.ToolBarAction)
    OtpAction x             -> (x <<< Otp.ToolBarAction)
    StatusAction x          -> (x <<< Status.ToolBarAction )
    VerifyNumberAction x    -> (x <<< VerifyNumber.ToolBarAction)
    QuickPayAction x        -> (x <<< QuickPay.ToolBarAction)
    EMIInstrumentsAction x  -> (x <<< EMIInstruments.ToolBarAction)
    EMIPlansAction x        -> (x <<< EMIPlans.ToolBarAction)
    EMICheckoutAction x     -> (x <<< EMICheckout.ToolBarAction)
    OffersAction x          -> (x <<< Offers.ToolBarAction)

  ToolBarConfig.Config toolbarConfig = (UIConfig.toolBarConfig configPayload)
  updatedToolbarConfig = toolbarConfig { text = toolbarHeader }

amountBarLayout :: ∀ w. Boolean -> Boolean -> Number -> String -> String -> String -> ConfigPayload -> Array (PrestoDOM (Effect Unit) w)
amountBarLayout isModal attachAmountBar amount phoneNumber orderDesc customerName cP =
   if attachAmountBar
     && CPUtils.attachAmountToAllScreens cP
     && not isModal
     && CPUtils.attachAmountBarAtTop cP && amount >= 0.0  then
     [ AmountBar.view $ UIConfig.amountBarConfig cP amount phoneNumber orderDesc customerName]
   else []

overlayBackground :: ∀ w. Boolean -> ActionType -> Array (PrestoDOM (Effect Unit) w)
overlayBackground modalView push =
  if modalView then
    [  relativeLayout
        [ height (V 0)
        , weight 1.0
        , width MATCH_PARENT
        , gravity CENTER_HORIZONTAL
        , onClick action $ const ClickedOutside
        ]
        [ imageView
            [ height $ V 48
            , width $ V 48
            , margin $ MarginBottom 2
            , imageUrl "ic_arrow_down"
            , visibility GONE
            , alignParentBottom "true,-1"
            ]
        ]
      ]
  else []
  where
  action = case push of
    QuickPayAction x        -> (x <<< QuickPay.OverlayClick)
    PPAction x              -> (x <<< PaymentPage.OverlayClick)
    PMAction x              -> (x <<< PaymentManagement.OverlayClick)
    AddCardAction x         -> (x <<< AddCard.OverlayClick)
    NetBankingAction x      -> (x <<< NetBanking.OverlayClick)
    AddUpiAction x          -> (x <<< AddUPI.OverlayClick )
    UpiHomeAction x         -> (x <<< UPIHome.OverlayClick)
    WalletAction x          -> (x <<< Wallet.OverlayClick)
    DelinkWalletAction x    -> (x <<< DelinkWallet.OverlayClick)
    OtpAction x             -> (x <<< Otp.OverlayClick)
    StatusAction x          -> (x <<< Status.OverlayClick)
    VerifyNumberAction x    -> (x <<< VerifyNumber.OverlayClick)
    EMIInstrumentsAction x  -> (x <<< EMIInstruments.OverlayClick)
    EMIPlansAction x        -> (x <<< EMIPlans.OverlayClick)
    EMICheckoutAction x     -> (x <<< EMICheckout.OverlayClick)
    OffersAction x          -> (x <<< Offers.OverlayClick)


childLayout :: ∀ w. ParentLayoutInput -> Array (PrestoDOM (Effect Unit) w) -> PrestoDOM (Effect Unit) w
childLayout input contentLayout =
  if input.useRelativeLayout then
    relativeLayout (
      [ width MATCH_PARENT
      , height if input.modalView
              then input.modalHeight
              else MATCH_PARENT
      , background (CPUtils.backgroundColor input.configPayload)
      ] <>  if input.useContainerPadding then
              [ containerPadding input.configPayload ]
            else [])
      contentLayout
  else
    linearLayout (
      [ width MATCH_PARENT
      , height
          if input.modalView then
              input.modalHeight
          else MATCH_PARENT
      , orientation VERTICAL
      ] <>
        if input.modalView then
          []
        else [background (CPUtils.backgroundColor input.configPayload)] <>
        if input.useContainerPadding then
          [ containerPadding input.configPayload]
        else []
      )
    contentLayout

getParentLayout :: ∀ w. ParentLayoutInput -> ActionType -> Array (PrestoDOM (Effect Unit) w) -> Maybe (PrestoDOM (Effect Unit) w) -> PrestoDOM (Effect Unit) w
getParentLayout input push contentLayout inScreenPopup =
  case inScreenPopup of
    Just popup ->
      relativeLayout
        [ width MATCH_PARENT
        , height MATCH_PARENT
        , orientation VERTICAL
        ]
        [ completeLayout input push contentLayout
        , popup
        ]
    Nothing -> completeLayout input push contentLayout

completeLayout ::  ∀ w. ParentLayoutInput -> ActionType -> Array (PrestoDOM (Effect Unit) w) -> PrestoDOM (Effect Unit) w
completeLayout input push contentLayout =
  if input.modalView then
    PrestoAnim.entryAnimationSetForward [fadeInAnim input.configPayload true]
    $ PrestoAnim.exitAnimationSetForward [fadeOutAnim input.configPayload true]
    $ PrestoAnim.entryAnimationSetBackward [fadeInAnim input.configPayload true]
    $ PrestoAnim.exitAnimationSetBackward [fadeOutAnim input.configPayload true]
    $ parentLayout (childView input push contentLayout) input.modalView
  else parentLayout (childView input push contentLayout) input.modalView

childView ::  ∀ w. ParentLayoutInput -> ActionType -> Array (PrestoDOM (Effect Unit) w) -> Array (PrestoDOM (Effect Unit) w)
childView input push contentLayout =
    (toolbarLayout input.modalView input.toolbarHeader push input.configPayload) <>
    (amountBarLayout input.modalView input.showAmountBar input.amount input.phoneNumber input.orderDescription input.customerName input.configPayload) <>
    (overlayBackground input.modalView push) <>
    [ if not input.modalView then
        PrestoAnim.entryAnimationSetForward [ translateInXAnim input.configPayload (screenWidth unit) true]
        $ PrestoAnim.exitAnimationSetForward [translateOutXAnim input.configPayload (-50) true]
        $ PrestoAnim.entryAnimationSetBackward [translateInXAnim input.configPayload (-(screenWidth unit)) true]
        $ PrestoAnim.exitAnimationSetBackward [translateOutXAnim input.configPayload (screenWidth unit) true]
        $ childLayout input contentLayout
      else
        PrestoAnim.animationSet
          [ translateInYAnim input.configPayload (screenHeight unit) input.modalAnimationTrigger
          , translateOutYAnim input.configPayload (screenHeight unit) (not input.modalAnimationTrigger)
          ]
          $ childLayout input contentLayout
    ]


data PayOptionType
  = StoredWalletItem Instruments.StoredWallet
  | StoredCardItem Instruments.StoredCard
  | WalletItem Wallet
  | StringItem String

isLastItem :: PayOptionType -> Array PayOptionType -> Boolean
isLastItem item allItems = let
  lastIndex = length allItems - 1
  in
  case item of
    StoredWalletItem strWallet -> true
    _ -> true

uiCardLayout :: ∀ w. ConfigPayload -> Array (PrestoDOM (Effect Unit) w ) -> PrestoDOM (Effect Unit) w
uiCardLayout cP child =
  linearLayout (
    [ orientation VERTICAL
    , height WRAP_CONTENT
    , width MATCH_PARENT
    , background $ CPUtils.uiCardColor cP
    , cardPadding cP
    ] <>
      ( if hSpace /=0 then
        [ cornerRadius $ CPUtils.uiCardCornerRadius cP] <>
        ( if translation == 0.0 then []
          else
            [ translationZ translation
            , margin (Margin 4 4 4 4)
            ]
        )
        else []
      )
    )
    child
  where
  translation = CPUtils.uiCardTranslation cP
  hSpace = CPUtils.horizontalSpace cP

addCurvedWrapper :: ∀ w. ConfigPayload ->  Array (PrestoDOM (Effect Unit) w) -> Array (PrestoDOM (Effect Unit) w)
addCurvedWrapper configPayload childLayout = let
  hSpace = (CPUtils.horizontalSpace configPayload)
  sectionSpace = CPUtils.sectionSpace configPayload
  cardCornerRadius = CPUtils.uiCardCornerRadius configPayload
  isCurved = cardCornerRadius /= 0.0
  translation = CPUtils.uiCardTranslation configPayload
  tM = UIUtils.translationMargin
  in
  if not isCurved && translation == 0.0 then
    childLayout
  else
    [ linearLayout (
      [ width MATCH_PARENT
      , height WRAP_CONTENT
      , orientation VERTICAL
      , background $ CPUtils.uiCardColor configPayload
      ] <>
      ( if isCurved then
          [ padding (Padding 2 2 2 2)
          , cornerRadius cardCornerRadius
          ]
        else [] ) <>
      ( if translation == 0.0 then []
        else
          [ margin if hSpace == 0 then (Margin 0 tM 0 tM) else (Margin tM tM tM tM)
          , translationZ translation
          ]
      )
    )
      childLayout
    ]

offerPopup :: ∀ w. ActionType -> ConfigPayload -> String -> Boolean -> PrestoDOM (Effect Unit) w
offerPopup push configPayload offerDescription showPopup =
  Popup.view action (PopupConfig.Config updatedConfig) Nothing anim
  where
  anim =
    Just $ PrestoAnim.animationSet
      [ translateInYAnim configPayload (screenHeight unit) showPopup
      , translateOutYAnim configPayload (screenHeight unit) (not showPopup)
      ]

  percentageHeight = V $ fromMaybe 500 $ fromNumber $ toNumber (screenHeight unit) * 0.70
  offerText = UIUtils.markDownToHTML offerDescription
  popupHeight = if S.length offerDescription > 100 then percentageHeight else WRAP_CONTENT
  action = getActionsForOfferPopUp push
  PopupConfig.Config config = UIConfig.popupConfig configPayload
  updatedConfig = config
      { listStrings = [offerText]
      , open = showPopup
      , close = not showPopup
      , header = "Terms And Conditions"
      , buttonOneVisibility = VISIBLE
      , buttonOneBackground = CPUtils.primaryColor configPayload
      , buttonOneText = "Got it"
      , buttonTwoVisibility = GONE
      , termsVisibility = GONE
      , checkBoxVisibility = GONE
      , dividerVisibility = VISIBLE
      , headerImageVisibility = VISIBLE
      , corners = (Corners 5.0 true true false false)
      , headerMargin = (Margin 24 16 24 16)
      , listScrollHeight = popupHeight
      , listTextFromHtml = true
      , useBullets = false
      }



getActionsForOfferPopUp :: ActionType -> (Action  -> Effect Unit)
getActionsForOfferPopUp push =
    case push of
      PPAction x              -> (x <<< PaymentPage.OfferPopupAction)
      PMAction x              -> (x <<< PaymentManagement.OfferPopupAction)
      AddCardAction x         -> (x <<< AddCard.OfferPopupAction )
      NetBankingAction x      -> (x <<< NetBanking.OfferPopupAction)
      AddUpiAction x          -> (x <<< AddUPI.OfferPopupAction )
      UpiHomeAction x         -> (x <<< UPIHome.OfferPopupAction)
      WalletAction x          -> (x <<< Wallet.OfferPopupAction)
      DelinkWalletAction x    -> (x <<< DelinkWallet.OfferPopupAction)
      OtpAction x             -> (x <<< Otp.OfferPopupAction)
      StatusAction x          -> (x <<< Status.OfferPopupAction )
      VerifyNumberAction x    -> (x <<< VerifyNumber.OfferPopupAction)
      QuickPayAction x        -> (x <<< QuickPay.OfferPopupAction)
      EMIInstrumentsAction x  -> (x <<< EMIInstruments.OfferPopupAction)
      EMIPlansAction x        -> (x <<< EMIPlans.OfferPopupAction)
      EMICheckoutAction x     -> (x <<< EMICheckout.OfferPopupAction)
      OffersAction x          -> (x <<< Offers.OfferPopupAction)
