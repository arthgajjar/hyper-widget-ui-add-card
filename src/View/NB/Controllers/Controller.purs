module View.NB.Controller.Controller where

import HyperPrelude.External(pure,(<>),liftEffect,concat,discard, filter, find,not,(#),(&&),($),(/=),(>),(||),(==),bind,map, (!!), last,elem,Maybe(..), fromMaybe, isJust, maybe,unwrap,length,Milliseconds(..), delay, launchAff_)
import HyperPrelude.Internal(Eval, continue, continueWithCmd, updateAndExit,ListData(..),createListData)
import Data.String as S
import Engineering.Helpers.Commons (getNbIin)
import JBridge as JBridge
import PPConfig.Utils (ifOfferVisible)
import PPConfig.Utils as CPUtils
import Engineering.Helpers.Commons(PaymentOffer(..))
import Payments.NetBanking.Utils (Bank(..), filterBanks, sortBanks)
import Remote.Types (ConfigPayload)
import Service.EC.Types.Instruments (StoredCard(..), StoredWallet(..))
import Service.EC.Types.Instruments as Instruments
import UI.Components.Message.Controller as Message
import UI.Components.PaymentOptionsController as PayOpt
import UI.Components.Popup.Controller as Popup
import UI.Components.PrimaryButton.Controller as PrimaryButton
import UI.Components.SearchBox.Controller as SearchBox
import UI.Components.ToolBar.Controller as ToolBar
import UI.Constant.Str.Default as STR
import UI.Utils (ModalAction(..), resetText)

type ScreenInput =
  { otherBanks :: Array Bank
  , popularBanks :: Array Bank
  , downBanks :: Array String
  , configPayload :: ConfigPayload
  , offers :: Array PaymentOffer
  , defNB :: String
  , amount :: Number
  , phoneNumber :: String
  , orderDesc :: String
  , customerName :: String
  , mid :: String
  , allCards :: Array StoredCard
  , allWallets :: Array StoredWallet
  }

data ScreenOutput = OnBackPress | UseBank String Boolean | Switch String | SwitchNav String | PayUsingNB String | PayUsingWallet  String String Boolean Boolean

type State =
  { otherBanks :: Array Bank
  , filteredBanks :: Array Bank
  , disabledBanks :: Array String
  , downBanks :: Array String
  , popularBanks :: Array Bank
  , selectedBank :: Maybe Bank
  , noBank :: Boolean
  , isTopVisible :: Boolean
  , entry :: Boolean
  , buttonAnim :: Boolean
  , itemData :: ListData
  , firstSearch :: Boolean
  , configPayload :: ConfigPayload
  , offers :: Array PaymentOffer
  , defaultOption :: String
  , searchBoxFocus :: Boolean
  , amount :: Number
  , phoneNumber :: String
  , orderDesc :: String
  , showProcessingBtn :: Boolean
  , customerName :: String
  }

type ItemState =
  { text :: String
  , image :: String
  , textColor :: String
  , containerBackground :: String
  , selected :: String
  , radioUrl :: String
  , font :: String
  , syze :: Int
  , secondaryVisibility :: String
  , secondaryText :: String
  , secondarySyze :: Int
  , alpha :: String
  , tertiaryText :: String
  , tertiaryVisibility :: String
  , dividerVisibility :: String
  , messageVisibility :: String
  , messageText :: String
  }

item :: String -> String -> String -> String -> String -> String -> String -> Int -> String -> String -> Int -> String -> String -> String -> String -> String -> String -> ItemState
item text textColor containerBackground image selected radioUrl font syze secondaryVisibility secondaryText secondarySyze alpha tertiaryText tertiaryVisibility dividerVisibility messageVisibility messageText =
  { text
  , textColor
  , containerBackground
  , image
  , selected
  , radioUrl
  , font
  , syze
  , secondaryVisibility
  , secondaryText
  , secondarySyze
  , alpha
  , tertiaryText
  , tertiaryVisibility
  , dividerVisibility
  , messageVisibility
  , messageText
  }

buildListData :: Maybe Bank -> Array Bank -> Array String -> Array String -> ConfigPayload -> Array PaymentOffer -> Array ItemState
buildListData selectedBank banks disabledBanks downBanks configPayload offerNB =let
  lang = fromMaybe "" (configPayload # unwrap # _.language)
  isSelected b = (Just b) == selectedBank
  in
  map
  (\b@(Bank bank) -> let
    isDisabled = (isBankDisabled b disabledBanks)
    isDown = (isBankDown b downBanks)
    isAllDown = "" `elem` downBanks
    downMsg = STR.downMsg lang
    textColor = CPUtils.fontColor configPayload
    image = ("ic_bank_" <> (getNbIin bank.code))
    background = CPUtils.uiCardColor configPayload
    selected = "visible"
    radioUrl = if isSelected b then "tick" else "circular_radio_button"
    font = CPUtils.pOPrimaryTextFont configPayload
    syze = CPUtils.fontSize configPayload
    --filteredOfferNB = find (\offer -> offer.paymentMethod == bank.code) offerNB
    secondaryVisibility = if (CPUtils.ifOfferVisible configPayload) && (true) then "visible" else "gone"
    secondaryText = ""--maybe "" (\b -> b.offerText) filteredOfferNB
    secondarySyze = if isDown then (CPUtils.fontSizeVerySmall configPayload) else (CPUtils.fontSizeSmall configPayload)
    alpha = if isDisabled then "0.5" else "1.0"
    tertiaryText = ""
    tertiaryVisibility = "gone"
    dividerVisibility = if (last banks) == (Just b) then "gone" else "visible"
    messageVisibility = if isDown || isAllDown then "visible" else "gone"
    messageText = if isAllDown then (CPUtils.outageRowMessage configPayload) <> " this option" else (CPUtils.outageRowMessage configPayload) <> " " <> bank.name

    in
    item bank.name textColor background image selected radioUrl
      font syze secondaryVisibility secondaryText secondarySyze
      alpha tertiaryText tertiaryVisibility dividerVisibility
      messageVisibility messageText
  )
  banks

data Action
  = BackPressed
  | NBScreenRendered
  | ToolBarAction ToolBar.Action
  | SelectBank Int
  | PopularBankAction Bank PayOpt.Action
  | SearchBoxAction SearchBox.Action
  | PrimaryButtonAction PrimaryButton.Action
  | BankSelected Int Bank PayOpt.Action
  | SearchCanceled
  | SearchBarClicked
  | ContinueCommand
  | BankSelectedFromList PayOpt.Action
  | OverlayClick ModalAction
  | OfferPopupAction Popup.Action
  | OutageMessageClick Message.Action

initialState :: ScreenInput -> State
initialState input =
  let disBanks = fromMaybe [] (CPUtils.getDisabledBanks input.configPayload)
  in  { otherBanks : input.otherBanks
    , filteredBanks : input.otherBanks
    , disabledBanks : disBanks
    , downBanks : input.downBanks
    , popularBanks : input.popularBanks
    , selectedBank : Nothing
    , noBank : false
    , isTopVisible : true
    , entry : true
    , buttonAnim : false
    , itemData : createListData (buildListData Nothing input.otherBanks disBanks input.downBanks input.configPayload input.offers)
    , configPayload : input.configPayload
    , offers : input.offers
    , defaultOption : ""
    , firstSearch : true
    , searchBoxFocus: false
    , amount : input.amount
    , phoneNumber : input.phoneNumber
    , orderDesc : input.orderDesc
    , showProcessingBtn : true
    , customerName : input.customerName
    }

eval :: Action -> State -> Eval Action ScreenOutput State

eval (ToolBarAction ToolBar.Clicked) state =
  let updatedState = state {entry = not state.entry} in
  updateAndExit updatedState $ OnBackPress

eval BackPressed state =
  let updatedState = state {entry = not state.entry} in
  updateAndExit updatedState $ OnBackPress

eval (OverlayClick ClickedOutside) state =
  let updatedState = state { entry = not state.entry} in
  updateAndExit updatedState $ OnBackPress

eval NBScreenRendered state =
  continueWithCmd state [
    do
    _ <- launchAff_ do
      _ <- delay (Milliseconds 100.0)
      liftEffect $ resetText [JBridge.getNewIDWithTag JBridge.SEARCHBOX]
    pure $ ContinueCommand
  ]

eval (SelectBank index) state = let
  bank = state.filteredBanks !! index
  dBank = if state.selectedBank == bank then Nothing else bank
  isDisabled = case dBank of
    Just bnk  -> isBankDisabled bnk state.disabledBanks
    Nothing   -> false

  in if isDisabled then continue state
    else continue $ state {
      selectedBank = dBank,
      itemData = createListData' state dBank state.filteredBanks }

eval (PopularBankAction b@(Bank bank) action) state =
  case action of
    PayOpt.PaymentListAction PayOpt.ButtonClicked -> let
      newState = state { buttonAnim = true } in
      updateAndExit newState $ UseBank bank.code (state.defaultOption /= "")

    PayOpt.GridClick -> let
      newState = state { selectedBank = Just b, showProcessingBtn = false }
      restrictPayment = CPUtils.restrictOutagePayment state.configPayload
      isDown = (bank.code `elem` state.downBanks) || ("" `elem` state.downBanks)
      in
      if restrictPayment && isDown then continue state
      else updateAndExit newState $ UseBank bank.code (state.defaultOption /= "")

    PayOpt.PaymentListAction PayOpt.SetDefault ->
      let isDefOptNb = state.defaultOption == bank.code in
      continue state { defaultOption = (if isDefOptNb then "" else bank.code )}

    PayOpt.PaymentListItemSelection -> continue state { selectedBank = Just b }

    _ -> continue state


eval (SearchBoxAction action) state =
  case action of
    SearchBox.Searched str -> let
      allBanks = sortBanks $ concat [state.otherBanks, state.popularBanks]
      banks = if str == "" then allBanks else (filterBanks str allBanks)
      in continue $ state
        { isTopVisible = if (S.length str > 0 ) then false else state.isTopVisible
        , filteredBanks = banks
        , selectedBank = Nothing
        , noBank = length banks == 0
        , itemData = createListData' state Nothing banks
        }

    SearchBox.Clicked true -> let
      allBanks = sortBanks $ concat [state.otherBanks, state.popularBanks]
      in continue state { isTopVisible = false
            , searchBoxFocus = true
            , filteredBanks = allBanks
            , itemData = createListData' state Nothing allBanks
            }

    SearchBox.Canceled ->
      continueWithCmd state {
          isTopVisible = true
            , searchBoxFocus = false
            , filteredBanks = state.otherBanks
            , noBank = false
            , selectedBank = Nothing
            , itemData = createListData' state Nothing state.otherBanks
            }
        [ do
          resetText [JBridge.getNewIDWithTag JBridge.SEARCHBOX]
          launchAff_ $ liftEffect $ JBridge.requestKeyboardHide
          pure SearchCanceled
        ]

    _ -> continue state


eval (PrimaryButtonAction PrimaryButton.Clicked) state =
  case state.selectedBank of
    Nothing -> continue state
    Just (Bank bank) ->
      let newState = state {buttonAnim = true } in
      updateAndExit newState $ UseBank bank.code false

eval action state = continue state

isBankDisabled :: Bank -> Array String -> Boolean
isBankDisabled (Bank bank) disabledBanks = bank.code `elem` disabledBanks

isBankDown :: Bank -> Array String -> Boolean
isBankDown (Bank bank) downBanks = bank.code `elem` downBanks

createListData' :: State -> Maybe Bank -> Array Bank -> ListData
createListData' state selectedBank banks =
  createListData $ buildListData' state selectedBank banks

buildListData' :: State -> Maybe Bank -> Array Bank -> Array ItemState
buildListData' state bank banks =
  buildListData
    bank
    banks
    state.disabledBanks
    state.downBanks
    state.configPayload
    state.offers
