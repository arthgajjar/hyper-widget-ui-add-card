module View.NB.Screens.Utils where

import HyperPrelude.External((!!),fromMaybe, Maybe(..),Effect,(==),Unit,(&&),map,(||),($),not,(<<<),(<>))
import HyperPrelude.Internal(Length(..),PrestoDOM,Visibility(..),linearLayout,height,width,Padding(..),padding)
import View.PaymentPage.Controllers.Controller
import View.PaymentPage.Controllers.Types
import Data.Newtype (unwrap)
import Data.Number.Format (toString)
import Data.String (Pattern(..), split)
import Data.String as StrUtils
import Data.Foldable (elem)
import Engineering.Helpers.Commons (getIinFromName)
import PPConfig.Utils as CPUtils
import Payments.NetBanking.Utils (Bank(..))
import Remote.Types (ConfigPayload(..))
import UI.Components.PaymentOptions.GridPaymentOptionView as GridPayOption
import UI.Components.PaymentOptionsConfig as PaymentOptionsConfig
import UI.Components.PaymentOptionsView as PayOptView
import View.NB.Controller.Controller as NBScreenController
import View.PaymentPage.Controllers.Types (PaymentInstrument)
import View.PaymentPage.Controllers.Types as PPScreenController
import View.Stock.Container.Commons as UICommons


getNetBanksLayout :: ∀ w. ActionType -> NetBanksLayoutInput -> Array (PrestoDOM (Effect Unit) w)
getNetBanksLayout receivedAction input =
  if input.useGrid
    then [ scrollWrapper netBanksView ]
    else netBanksView
  where
  rendererView = if input.useGrid then GridPayOption.gridView else PayOptView.view
  netBanksView =
    map
      (\bank ->
        rendererView
        (action bank)
        (getPopularConfig bank input)
        false
        Nothing
      )
      input.popularBanks

  scrollWrapper child =
    UICommons.gridScrollView input.configPayload $
      [ linearLayout
          [ height WRAP_CONTENT
          , width MATCH_PARENT
          , padding (Padding 8 8 8 8)
          ] child
      ]

  action bank  =
    case receivedAction of
      PPScreenAction x ->
        (x <<< (PPScreenController.NetBankingAction $ NetBank bank))

      NBScreenAction y ->
        (y <<< (NBScreenController.PopularBankAction bank))

getPopularConfig :: Bank -> NetBanksLayoutInput -> PaymentOptionsConfig.Config
getPopularConfig b@(Bank bank) input = let
  isDown = bank.code `elem` input.downBanks
  restrictPayment = CPUtils.restrictOutagePayment input.configPayload
  isSelected =  (Just b) == input.selectedBank
  useTick = CPUtils.useLogoTick input.configPayload
  outageStatus = (isDown || input.isAllBanksDown) && restrictPayment
  PaymentOptionsConfig.Config defConfig = PaymentOptionsConfig.defConfig input.configPayload
  upConfig =
    defConfig
      { primaryText = if not input.useGrid then bank.name
        else if bank.name == "State Bank of India" then "SBI"
            else (fromMaybe "" ((StrUtils.split (Pattern " ")  bank.name ) !! 0))
      , logoUrl = "ic_bank_" <> getIinFromName bank.name
      , gridTextHeight = V 24
      , primaryTextFont = if isSelected && not useTick && input.useGrid then CPUtils.fontBold input.configPayload else CPUtils.fontRegular input.configPayload
      , gridLogoStroke = if isSelected && useTick then CPUtils.gridSelectedStroke input.configPayload else defConfig.gridLogoStroke
      , tickVisibility = if isSelected && useTick then VISIBLE else GONE
      , tickImageUrl = "grid_tick"
      , inputAreaVisibility = if isSelected then VISIBLE else GONE
      , cvvInputVisibility = GONE
      , radioButtonIconUrl = if isSelected then "tick" else "circular_radio_button"
      , secondaryTextVisibility = GONE
      , gridItemAlpha = if outageStatus then 0.4 else 1.0
      , allowGridClick = not outageStatus
      }
  in PaymentOptionsConfig.Config upConfig

data ActionType
  = NBScreenAction (NBScreenController.Action -> Effect Unit)
  | PPScreenAction (PPScreenController.Action -> Effect Unit)


type NetBanksLayoutInput =
  { configPayload :: ConfigPayload
  , selectedBank :: Maybe Bank
  , popularBanks :: Array Bank
  , useGrid :: Boolean
  , downBanks :: Array String
  , isAllBanksDown :: Boolean
  }
