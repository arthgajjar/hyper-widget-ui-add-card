module View.NB.Screens.Screen where

import HyperPrelude.External(filter, find, length, mapWithIndex,fromMaybe, Maybe(..), isJust, maybe,Effect,Unit,(/=),(#),($),const,(<<<),(<>),(==),map,(||),not)
import HyperPrelude.Internal(Length(..), Margin(..), Orientation(..), PrestoDOM, Screen, Visibility(..), alignParentBottom, background, color, fontStyle, gravity, height, linearLayout, margin, orientation, text, textSize, textView, visibility, weight, width)
import Data.Foldable (elem)
import Data.Newtype (unwrap)
import Data.String (toLower)
import Data.String.CodePoints as StringCode
import Engineering.Helpers.Events (addCustomBackPress)
import PPConfig.Utils as CPUtils
import Payments.NetBanking.Utils (Bank(..))
import PrestoDOM.List as PrestoList
import PrestoDOM.Properties (scrollBarY)
import PrestoDOM.Types.DomAttributes (Gravity(..))
import Remote.Types (ConfigPayload(..), InputField(..))
import UI.Components.Message as Message
import UI.Components.Message.Config as MessageConfig
import UI.Components.PaymentOptionsConfig (SelectionType(..))
import UI.Components.PaymentOptionsConfig as PaymentOptionsConf
import UI.Components.PaymentOptionsView as PaymentOptionsComp
import UI.Components.PrimaryButton.Config as PrimaryButtonConfig
import UI.Components.PrimaryButton.View as PrimaryButton
import UI.Components.SearchBox.Config as SearchBoxConfig
import UI.Components.SearchBox.View as SearchBox
import UI.Config as UIConfig
import UI.Constant.Str.Default as STR
import UI.Utils (contentMargin, sectionMargin)
import UI.Utils (scrollTo, contentMargin)
import Utils (arrayJoin)
import View.NB.Controller.Controller (Action(..), ScreenInput, ScreenOutput, State, eval, initialState)
import View.Stock.Container.Commons as UICommons
import View.NB.Screens.Utils as NBUtils
import View.PaymentPage.Screens.Utils as PPUtils

screen :: ScreenInput -> Screen Action State ScreenOutput
screen input =
    { initialState: (initialState input)
    , view
    , name: "NetBankingScreen"
    , globalEvents: []
    , eval: eval
    }

view :: ∀ w. (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
view push state =
    UICommons.getParentLayout
      parentInput
      (UICommons.NetBankingAction push)
      (netBankLayout push state)
      (Just $ payButtonView push state)
    where
    lang = fromMaybe "" (state.configPayload # unwrap # _.language)

    parentInput =
      { modalView: (CPUtils.ifModalView state.configPayload)
      , modalHeight: MATCH_PARENT
      , modalAnimationTrigger: state.entry
      , toolbarHeader: STR.getNetBanking lang
      , configPayload: state.configPayload
      , useContainerPadding: true
      , useRelativeLayout: false
      , amount: state.amount
      , showToolbar: true
      , phoneNumber : state.phoneNumber
      , showAmountBar : true
      , orderDescription : state.orderDesc
      , customerName : state.customerName
      }

netBankLayout :: ∀ w. (Action -> Effect Unit) -> State -> Array (PrestoDOM (Effect Unit) w)
netBankLayout push state = [
    linearLayout
      [ width MATCH_PARENT
      , height MATCH_PARENT
      , orientation VERTICAL
      , contentMargin state.configPayload
      , addCustomBackPress push (const BackPressed)
      -- <> (if state.isTopVisible then [ scrollTo "1,0" ] else [ scrollTo "0,0" ])
      ]
      [ popularNetBanksLayout push state -- if CPUtils.useGridForNb state.configPayload then () else (popularNBListLayout push state)
      , otherBanksLayout push state
      ]
    ]

popularNetBanksLayout :: ∀ w. (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
popularNetBanksLayout push state =
    if not state.isTopVisible
    then linearLayout [width $ V 0, height $ V 0] []
    else
      linearLayout
        [ width MATCH_PARENT
        , height WRAP_CONTENT
        , sectionMargin state.configPayload
        ] $
          UICommons.addCurvedWrapper state.configPayload $
            ( NBUtils.getNetBanksLayout
              (NBUtils.NBScreenAction push) $
              { configPayload : state.configPayload
              , selectedBank : state.selectedBank
              , popularBanks : state.popularBanks
              , useGrid : true -- TODO :: fetch this from config
              , downBanks
              , isAllBanksDown : isAllDown
              } ) <>
            [ Message.view (push <<< OutageMessageClick ) (MessageConfig.Config messageConf) ]
    where
    isDown = length downBanks /= 0
    isAllDown = "" `elem` state.downBanks
    popBanks = map(\(Bank b) -> b.code) state.popularBanks
    downBanks = filter(\bank -> bank `elem` popBanks) state.downBanks
    bankNames = map(\(Bank bank) -> bank.name) $ filter(\(Bank bank) -> bank.code `elem` downBanks) state.popularBanks
    space = CPUtils.uiCardHorizontalPadding state.configPayload
    MessageConfig.Config defConf = UIConfig.outageMessageConfig state.configPayload
    messageConf = defConf
      { margin = (Margin space 16 space 20)
      , text = if isAllDown then (CPUtils.outageRowMessage state.configPayload) <> " this option" else (CPUtils.outageRowMessage state.configPayload) <> " " <> (arrayJoin bankNames ", ")
      , visibility = if isDown || isAllDown then VISIBLE else GONE
      }

otherBanksLayout :: ∀ w. (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
otherBanksLayout push state =
  linearLayout
    [ width MATCH_PARENT
    , height WRAP_CONTENT
    , orientation VERTICAL
    ] $
    [ PPUtils.getSectionHeader header state.configPayload false
    , linearLayout
        [ width MATCH_PARENT
        , height MATCH_PARENT
        , margin (Margin 0 0 0 10)
        ]
        [ SearchBox.view
            (push <<< SearchBoxAction)
            (SearchBoxConfig.Config searchBoxConfig)
        ]
    ] <>
      UICommons.addCurvedWrapper state.configPayload (
        if state.noBank then
          [ noBanksText state.configPayload ]
        else
          [ PrestoList.list
              [ height $ V 0
              , weight 1.0
              , scrollBarY false
              , width MATCH_PARENT
              , orientation VERTICAL
              , margin (if isJust state.selectedBank then MarginBottom (CPUtils.listItemHeight state.configPayload) else MarginBottom 0)
              , PrestoList.listItem $ listItem push state
              , PrestoList.listData $ state.itemData
              , PrestoList.onItemClick push (SelectBank)
              ]
          ])
    where
    ConfigPayload confPayload = state.configPayload
    lang = fromMaybe "" (state.configPayload # unwrap # _.language)
    header = STR.getMoreBanksHeader lang
    SearchBoxConfig.Config searchConfig = UIConfig.searchBoxConfig state.configPayload
    InputField inputFieldConfig = confPayload.inputField
    searchBoxConfig =
      searchConfig
        { cancelVisibility =
          if state.isTopVisible then GONE
          else VISIBLE
        , searchBoxFocus = state.searchBoxFocus
        , stroke =
            case (toLower inputFieldConfig.type), state.searchBoxFocus of
              "boxed", true  -> "1," <> "#80" <> StringCode.drop 1 inputFieldConfig.focusColor
              "boxed", false -> "1,#54aaaaaa"
              _      , _     -> ""
        , searchIconMargin = (Margin 15 0 5 0)
        , cancelMargin = (Margin 5 0 15 0)
        }

listItem :: (Action -> Effect Unit) -> State -> PrestoList.ListItem
listItem push state =
    PrestoList.createListItem $
      PaymentOptionsComp.view
        (push <<< BankSelectedFromList)
        ( PaymentOptionsConf.Config
            poConfig
              { secondaryTextColor = "#B22222"
              , secondaryTextFont = CPUtils.fontRegular state.configPayload
              , primaryTextSize = (CPUtils.fontSize state.configPayload)
              , secondaryTextSize = (CPUtils.fontSizeSmall state.configPayload)
              , primaryTextColor = (CPUtils.fontColor state.configPayload)
              , selectionType = Checkbox
              , secondaryTextVisibility = GONE
              }
        )
        true
        Nothing
    where
    PaymentOptionsConf.Config poConfig = PaymentOptionsConf.defConfig state.configPayload

payButtonView :: ∀ w. (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
payButtonView push state =
  linearLayout (
    [ width MATCH_PARENT
    , height WRAP_CONTENT
    , alignParentBottom "true,-1"
    , visibility if state.showProcessingBtn then VISIBLE else GONE
    ]<> if buttonBackground == "" then []
        else [ background buttonBackground ]
    )
    [ PrimaryButton.view
        (push <<< PrimaryButtonAction)
        (PrimaryButtonConfig.Config updatedButtonConfig)
    ]
  where
  PrimaryButtonConfig.Config buttonConfig = UIConfig.primaryButtonConfig state.configPayload
  buttonBackground = CPUtils.btnBackground state.configPayload
  lang = fromMaybe "" (state.configPayload # unwrap # _.language)
  changeBtnText = CPUtils.changeBtnText state.configPayload
  space' = CPUtils.horizontalSpace state.configPayload
  space = if space' == 0 then CPUtils.uiCardHorizontalPadding state.configPayload else space'
  Bank selectedBank = fromMaybe (Bank { code : "", name : "" }) state.selectedBank
  selectedBankCode = selectedBank.code
  updatedButtonConfig =
    buttonConfig
      { text =
        ( if changeBtnText then
            if state.buttonAnim then
              STR.getProcessingPayment lang
            else
              STR.getPayNow lang
          else
            STR.getPayNow lang
        )
      , visible = if isJust state.selectedBank then VISIBLE else GONE
      , startAnimation = state.buttonAnim
      , alpha = if selectedBankCode `elem` state.downBanks then 0.4 else 1.0
      , isClickable = not (selectedBankCode `elem` state.downBanks)
      , margin = (Margin space UIConfig.vMargin space UIConfig.vMargin) -- In either case it will need space from side.
      }

noBanksText :: ∀ w. ConfigPayload -> PrestoDOM (Effect Unit) w
noBanksText cP =
  let lang = "english" in
  textView
    [ width MATCH_PARENT
    , height MATCH_PARENT
    , margin $ Margin 0 10 0 10
    , text $ STR.getNoBank lang
    , gravity CENTER_HORIZONTAL
    , textSize (CPUtils.fontSizeVeryLarge cP)
    , color "#75777E"
    , fontStyle $ CPUtils.fontRegular cP
    ]
