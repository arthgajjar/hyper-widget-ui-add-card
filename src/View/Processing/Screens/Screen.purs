module View.Processing.Screens.Screen where


import HyperPrelude.External(Maybe(..),Effect,Unit,(<<<),($),(==),(<>),const)
import HyperPrelude.Internal(Typeface(..), Visibility(..), Gravity(..), Length(..), Margin(..), Orientation(..), Padding(..), PrestoDOM, Screen, background, clickable, color, cornerRadius, gravity, height, linearLayout, margin, orientation, padding, progressBar, text, textSize, textView, translationZ, width, onClick, weight, visibility, imageUrl, imageView, typeface)
import PrestoDOM.Animation as PrestoAnim
import PrestoDOM.Utils ((<>>))
import UI.Components.ToolBar.View as ToolBar
import UI.Components.ToolBar.Config as ToolBarConfig
import UI.Config as UIConfig
import UI.Constant.Str.Default as STR
import UI.Utils (fadeOutAnimTemp, fadeInAnimTemp)
import View.Processing.Controllers.Controller (Action(..), ScreenInput, ScreenOutput, State, eval, initialState, overrides)

screen ::  ScreenInput  -> Screen Action State ScreenOutput
screen input =
    { initialState : (initialState input)
    , name : "Loader"
    , view
    , globalEvents : []
    , eval : eval
    }

view :: ∀ w  . (Action  -> Effect Unit) -> State  -> PrestoDOM (Effect Unit) w
view push state =
    PrestoAnim.entryAnimationSet [fadeInAnimTemp true]
    $ PrestoAnim.exitAnimationSet [fadeOutAnimTemp true]
    $ linearLayout
    ([ height MATCH_PARENT
    , width MATCH_PARENT
    , orientation VERTICAL
    , background  (if state.background == "" then "#99000000" else state.background)
    , clickable true
    , translationZ 13.0
    ] <>> overrides "MainLayout" push state)
    ( getToolBar <>
    [ linearLayout
      [ height MATCH_PARENT
      , width MATCH_PARENT
      , orientation VERTICAL
      , gravity CENTER
      ]
      [ linearLayout
        [ width MATCH_PARENT
        , height $ V 0
        , weight 1.0
        , visibility (if state.showJuspayLoader then VISIBLE else GONE)
        ] []
      , linearLayout
        ([ height $ V (if state.showJuspayLoader then 220 else 150)
        , width (V 304)
        , orientation VERTICAL
        , translationZ 22.0
        , gravity CENTER_HORIZONTAL
        , padding (Padding 14 41 14 0)
        -- , background "#FFFFFFFF"
        , cornerRadius 6.0
        , margin (Margin 0 0 0 0)
        ] <> (overrides "Dialog" push state))
        ((getLoader push state) <> [
        textView
            ([ height $ V (if state.showJuspayLoader then 30 else 17)
            , width MATCH_PARENT
            , margin (Margin 0 12 0 0)
            -- , textSize 12
            , color "#898989"
            , gravity CENTER
            , text (if state.processingText == "" then (STR.aafProcessing lang) else state.processingText)
            , textSize state.processingTextSize
            ] <>> overrides "Progress Text" push state)
        , textView
            [ height $ V 50
             , width MATCH_PARENT
             , text $ STR.aafRefresh lang
             , textSize 18
             , typeface BOLD
             , color "#3F51B5"
             , onClick push $ const Refresh
             , gravity CENTER
             , onClick push $ const BackPressed
             , visibility (if state.showRefreshButton then VISIBLE else GONE )
            ]
        ])
      ]
    ])
    where
    lang = "english" --(state.configPayload # unwrap # _.language)
    getToolBar = case state.configPayload of
        Just payload -> do
            let ToolBarConfig.Config toolbarConfig = UIConfig.toolBarConfig payload
                toolBarConfig' = toolbarConfig
            (if state.showToolBar then [ToolBar.view (push <<< ToolBarAction) (ToolBarConfig.Config toolBarConfig')] else [])
        Nothing -> []

getLoader :: forall w. (Action -> Effect Unit) -> State -> Array (PrestoDOM (Effect Unit) w)
getLoader push state = case state.showJuspayLoader of
    true -> [ PrestoAnim.animationSet
                [ PrestoAnim.Animation
                    [ PrestoAnim.duration 1000
                    , PrestoAnim.toRotation 360
                    , PrestoAnim.repeatCount PrestoAnim.Infinite
                    ] true
                ] $ linearLayout
                        [ width $ V 50
                        , height $ V 50
                        ]
                        [ imageView
                            [ width MATCH_PARENT
                            , height MATCH_PARENT
                            , imageUrl "juspay_logo_blue"
                            ]
                        ]
            , textView
                [ width MATCH_PARENT
                , height $ V 30
                , text "JUSPAY safe"
                , color "#898989"
                , gravity CENTER
                , textSize 12
                ]
            ]
    false -> [ progressBar
                ([ height $ V 34
                , width $ V 34
                , margin (Margin 121 0 121 0)
                ] <>> overrides "ProgressBar" push state)
            ]
