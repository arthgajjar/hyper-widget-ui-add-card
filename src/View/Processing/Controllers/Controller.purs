module View.Processing.Controllers.Controller where

import HyperPrelude.External(Effect,Maybe(..),Unit,($),const)
import HyperPrelude.Internal(Eval, Props, afterRender, exit, continue)
import UI.Components.ToolBar.Controller as ToolBar
import Remote.Types (ConfigPayload)
import Engineering.Helpers.Events (registerNewEvent)

type ScreenInput  = { processingText :: String
                    , processingTextSize :: Int
                    , showToolBar :: Boolean
                    , amount :: Number
                    , image_url :: String
                    , background :: String
                    , showRefreshButton :: Boolean
                    , configPayload :: Maybe ConfigPayload
                    , shouldWait :: Boolean
                    , showJuspayLoader :: Boolean
                    }

data ScreenOutput = OnBackPress | Proceed
data Action = BackPressed
            | DialogRendered
            | ToolBarAction ToolBar.Action
            | Refresh


type State = { processingText :: String
             , processingTextSize :: Int
             , showToolBar :: Boolean
             , amount :: Number
             , image_url :: String
             , background :: String
             , showRefreshButton :: Boolean
             , configPayload :: Maybe ConfigPayload
             , shouldWait :: Boolean
             , showJuspayLoader :: Boolean
             }


initialState ::  ScreenInput -> State
initialState input = { processingText : input.processingText
                     , processingTextSize : input.processingTextSize
                     , showToolBar : input.showToolBar
                     , amount : input.amount
                     , image_url : input.image_url
                     , background : input.background
                     , showRefreshButton : input.showRefreshButton
                     , configPayload : input.configPayload
                     , shouldWait : input.shouldWait
                     , showJuspayLoader : input.showJuspayLoader
                     }

eval
  :: Action
  -> State
  -> Eval Action ScreenOutput State
eval action state =
    case action of
        BackPressed -> exit OnBackPress
        (ToolBarAction _) -> exit OnBackPress
        DialogRendered -> if state.shouldWait then continue state else exit Proceed
        _ ->  continue state


overrides :: String -> (Action -> Effect Unit) -> State -> Props (Effect Unit)
overrides "MainLayout" push state = [
  registerNewEvent "onBackPressedEvent" push $ const BackPressed
  , afterRender push $ const DialogRendered
  ]
overrides _ push state = []
