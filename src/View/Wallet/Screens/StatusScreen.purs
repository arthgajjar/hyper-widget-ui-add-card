module View.Wallet.Screens.StatusScreen where

import HyperPrelude.External(Maybe(..),fromMaybe,Effect,Unit,(#),($),(<>),unit,negate,(<<<),not,const)
import HyperPrelude.Internal (Gravity(..), Length(..), Margin(..), Orientation(..), PrestoDOM, Screen, Visibility(..), alignParentBottom, background, clickable, cornerRadius, fontStyle, gravity, height, imageUrl, imageView, linearLayout, margin, orientation, relativeLayout, text, textSize, textView, visibility, weight, width)

import Data.Newtype (unwrap)
import Engineering.Helpers.Events (addCustomBackPress)
import PrestoDOM.Animation as PrestoAnim
import UI.Components.ToolBar.View as ToolBar
import UI.Components.ToolBar.Config as ToolBarConfig
import PPConfig.Utils as CPUtils
import UI.Config as MConfig
import UI.Utils (fadeInAnim, fadeOutAnim, screenHeight, screenWidth, translateInXAnim, translateInYAnim, translateOutXAnim, translateOutYAnim)
import View.Wallet.Controllers.StatusController (Action(..), ScreenOutput, State, eval, getDisplayMessage, overrides)

screen ::  State -> Screen Action State ScreenOutput
screen input =
    { initialState : input
    , name : "WalletLinkStatus"
    , view
    , globalEvents : []
    , eval : eval
    }

view :: ∀ w  . (Action  -> Effect Unit) -> State  -> PrestoDOM (Effect Unit) w
view push state =
    let ifModal = CPUtils.ifModalView state.configPayload in
    PrestoAnim.entryAnimationSetForward [if ifModal then fadeInAnim state.configPayload true else translateInXAnim state.configPayload (screenWidth unit) true]
    $ PrestoAnim.exitAnimationSetForward [if ifModal then fadeOutAnim state.configPayload true else translateOutXAnim state.configPayload (0) true]
    $ PrestoAnim.entryAnimationSetBackward [if ifModal then fadeInAnim (state.configPayload) true else translateInXAnim (state.configPayload) (-(screenWidth unit)) true]
    $ PrestoAnim.exitAnimationSetBackward[if ifModal then fadeOutAnim (state.configPayload) true else translateOutXAnim (state.configPayload) (screenWidth unit) true]
    $ linearLayout
        ([ height MATCH_PARENT
        , width MATCH_PARENT
        , orientation VERTICAL
        , background "#BF000000"
        , cornerRadius 0.00
        , margin (Margin 0 0 0 0)
        , clickable true
        , visibility VISIBLE
        ] <> overrides "entry" push state)
        ([] <> (if ifModal then [] else [ToolBar.view (push <<< ToolBarAction) (ToolBarConfig.Config updatedToolbarConfig)]) <>

       (if ifModal then ( [ relativeLayout
                [ weight 1.0
                , width MATCH_PARENT
                , gravity CENTER_HORIZONTAL

                ]
                [ imageView
                    ([ height $ V 48
                    , width $ V 48
                    , margin $ MarginBottom 2
                    , imageUrl "ic_arrow_down"
                    , visibility GONE
                    , alignParentBottom "true,-1"
                    ])
                ] ]) else [])
        <>  [ PrestoAnim.animationSet ([] <>
                    if ifModal
                        then [ translateInYAnim state.configPayload (screenHeight unit) state.entry
                             , translateOutYAnim state.configPayload (screenHeight unit) (not state.entry)]
                        else []
                    ) $ linearLayout
                            [ height if ifModal then (V 470) else MATCH_PARENT
                            , width MATCH_PARENT
                            , background "#ffffff"
                            , orientation VERTICAL
                            , margin (Margin 0 0 0 0)
                            , addCustomBackPress push (const BackPressed)
                            ] [(walletLinkStatusLayout push state)]
            ]
        )
        where

                (ToolBarConfig.Config toolbarConfig) = (MConfig.toolBarConfig state.configPayload)
                updatedToolbarConfig = toolbarConfig {
                                      text = "Wallet"
                                    }

walletLinkStatusLayout :: ∀ w  . (Action  -> Effect Unit) -> State  -> PrestoDOM (Effect Unit) w
walletLinkStatusLayout push state =
    linearLayout
        ([ height MATCH_PARENT
        , width MATCH_PARENT
        , background "#ffffff"
        , orientation VERTICAL
        --, margin (Margin 16 10 16 20)
        , gravity CENTER
        ] <> overrides "MainLayout" push state)
        [
            imageView
                [ width $ V 40
                , height $ V 40
                , imageUrl (if state.status then "success_green" else "failure_red")
                ]
        ,   textView
            [ width MATCH_PARENT
            , text (getDisplayMessage lang state)
            , fontStyle $ CPUtils.fontRegular state.configPayload
            , textSize (CPUtils.fontSizeVeryLarge state.configPayload)
            , gravity CENTER
            , margin (MarginTop 20)
            ]
        ]
        where
            lang = fromMaybe "" (state.configPayload # unwrap # _.language)
