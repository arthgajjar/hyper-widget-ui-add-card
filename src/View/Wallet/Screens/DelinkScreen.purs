module View.Wallet.Screens.DelinkScreen where

import HyperPrelude.External(length,fromMaybe, Maybe(..),toLower,Effect,Unit,(#),(==),(/=),(<>),const,($),map,(<<<))
import HyperPrelude.Internal (Gravity(..), Length(..), Margin(..), Orientation(..), PrestoDOM, Screen, Visibility(..), background, color, fontStyle, gravity, height, linearLayout, orientation, scrollView, text, textSize, textView, weight, width)

import Data.Newtype (unwrap)
import Service.EC.Types.Instruments as Instruments
import Engineering.Helpers.Events (addCustomBackPress)
import PPConfig.Utils as CPUtils
import UI.Components.PaymentOptionsConfig as PaymentOptionsConfig
import UI.Components.PaymentOptionsView as PaymentOptionsView
import UI.Components.PrimaryButton.View as PrimaryButton
import UI.Components.PrimaryButton.Config as PrimaryButtonConfig
import UI.Config as UIConfig
import UI.Constant.Str.Default as STR
import View.Wallet.Controllers.DelinkController (Action(..), ScreenInput, ScreenOutput, State, eval, initialState)
import View.Stock.Container.Commons as UICommons
import UI.Utils (contentMargin)

screen :: ScreenInput -> Screen Action State ScreenOutput
screen input =
    { initialState : (initialState input)
    , name : "DelinkWalletScreen"
    , view
    , globalEvents : []
    , eval : eval
    }

view :: ∀ w  . (Action  -> Effect Unit) -> State  -> PrestoDOM (Effect Unit) w
view push state =
  UICommons.getParentLayout
    parentInput
    (UICommons.DelinkWalletAction push)
    (delinkWalletLayout push state)
    Nothing
  where
  parentInput =
    { modalView : (CPUtils.ifModalView state.configPayload)
    , modalHeight : (V 500)
    , modalAnimationTrigger : state.entry
    , toolbarHeader : "Delink Wallet"
    , configPayload : state.configPayload
    , useContainerPadding : true
    , useRelativeLayout : false
    , amount : state.amount
    , showToolbar : true
    , phoneNumber : state.phoneNumber
    , showAmountBar : true
    , orderDescription : "" -- TODO :: add this later
    , customerName : ""
    }


delinkWalletLayout :: ∀ w  . (Action  -> Effect Unit) -> State  -> Array (PrestoDOM (Effect Unit) w)
delinkWalletLayout push state =
    [ scrollView
        [ height WRAP_CONTENT
        , width MATCH_PARENT
        , addCustomBackPress push (const BackPress)
        ]
        [ linearLayout
            [ width MATCH_PARENT
            , height WRAP_CONTENT
            , background "#ffffff"
            , orientation VERTICAL
            , gravity CENTER
            , contentMargin state.configPayload
            ]
            case (length state.storedWallets) of
                0 -> do
                    [ linearLayout
                        [ width MATCH_PARENT
                        , height MATCH_PARENT
                        , orientation VERTICAL
                        ]
                        [ textView
                            [ width MATCH_PARENT
                            , height $ V 40
                            , text $ STR.getNoLink lang
                            , textSize (CPUtils.fontSizeVeryLarge state.configPayload)
                            , color "#a50606"
                            , gravity CENTER
                            , fontStyle $ CPUtils.fontBold state.configPayload
                            ]
                        ]
                    ]
                _ ->
                (state.storedWallets #
                    map
                        (\(Instruments.StoredWallet (Instruments.Wallet item)) ->
                            PaymentOptionsView.view
                            (push <<< (WalletSelectAction (fromMaybe "" item.wallet) item.id))
                            (PaymentOptionsConfig.Config
                                wconf { primaryText = (fromMaybe "" item.wallet)
                                    , logoUrl = "ic_" <> (toLower (fromMaybe "" item.wallet))
                                    , radioButtonVisibility = if item.id == state.selectedWalletId then VISIBLE else GONE
                                    , secondaryTextVisibility = GONE
                                    , inputAreaVisibility = GONE
                                    , cvvId = item.id <> "_id"-- patch added due to repeating id  "default"
                                    , radioButtonIconUrl =
                                        if state.selectedWalletId == item.id
                                            then "tick"
                                            else "circular_radio_button"
                                    })
                            false
                            Nothing
                        )
                )
        ]
        , linearLayout [width MATCH_PARENT, height $ V 0, weight 1.0][]
        , PrimaryButton.view
            (push <<< PrimaryButtonAction)
            (PrimaryButtonConfig.Config newConfig)

    ]
    where
    lang = fromMaybe "" (state.configPayload # unwrap # _.language)
    PrimaryButtonConfig.Config pconf = UIConfig.primaryButtonConfig state.configPayload
    space' = CPUtils.horizontalSpace state.configPayload
    space = if space' == 0 then CPUtils.uiCardHorizontalPadding state.configPayload else 0 -- since in screen popup param is not being used
    newConfig =
      pconf
        { text= STR.getDelink lang
        , visible = if state.selectedWalletName /= "" then VISIBLE else GONE
        , startAnimation = state.buttonAnim
        , margin = (Margin space UIConfig.vMargin space UIConfig.vMargin)
        }

    PaymentOptionsConfig.Config w = PaymentOptionsConfig.defConfig state.configPayload
    wconf = w
