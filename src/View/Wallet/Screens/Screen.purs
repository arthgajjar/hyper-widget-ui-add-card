module View.Wallet.Screens.Screen where

import HyperPrelude.External(Maybe(..),fromMaybe,Effect,Unit,(&&),(#),(<>),($),(/=),not,const,length,(<<<))
import HyperPrelude.Internal (Length(..), Margin(..), Orientation(..), PrestoDOM, Screen, Visibility(..), height, linearLayout, orientation, scrollView, visibility, weight, width)

import Data.Newtype (unwrap)
import Engineering.Helpers.Events (addCustomBackPress)
import PPConfig.Utils as CPUtils
import PrestoDOM.Properties (scrollBarY)
import Payments.Wallets.Utils as WUtils
import UI.Components.PrimaryButton.View as PrimaryButton
import UI.Components.PrimaryButton.Config as PrimaryButtonConfig
import UI.Config as UIConfig
import UI.Constant.Str.Default as STR
import UI.Utils as UIUtils
import View.Wallet.Controllers.Controller (Action(..), ScreenInput, ScreenOutput, State, eval, initialState)
import View.Stock.Container.Commons (ActionType(..), addCurvedWrapper, getParentLayout, offerPopup) as UICommons
import View.PaymentPage.Screens.Utils as PPUtils
import View.Wallet.Screens.Utils as WalletViewUtils


screen :: ScreenInput -> Screen Action State ScreenOutput
screen input =
  { initialState: (initialState input)
  , name:
    if input.isPayLater then
      "PayLaterScreen"
    else
      "WalletListScreen"
  , view
  , globalEvents: []
  , eval: eval
  }

view :: ∀ w. (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
view push state =
  UICommons.getParentLayout
    parentInput
    action
    (walletLayout push state)
    (Just $ UICommons.offerPopup action state.configPayload state.activeOfferDesc state.showOfferDescPopup)
  where
  action = UICommons.WalletAction push
  parentInput =
    { modalView: (CPUtils.ifModalView state.configPayload)
    , modalHeight: WRAP_CONTENT
    , modalAnimationTrigger: state.entry
    , toolbarHeader:
      if state.isPayLater then
        "Pay Later"
      else
        "Wallets"
    , configPayload: state.configPayload
    , useContainerPadding: true
    , useRelativeLayout: false
    , amount: state.amount
    , showToolbar: true
    , phoneNumber : state.phoneNumber
    , showAmountBar : true
    , orderDescription : state.orderDesc
    , customerName : state.customerName
    }

walletLayout :: ∀ w. (Action -> Effect Unit) -> State -> Array (PrestoDOM (Effect Unit) w)
walletLayout push state =
  [ scrollView
      [ height WRAP_CONTENT
      , width MATCH_PARENT
      , scrollBarY false
      , addCustomBackPress push (const BackPress)
      ]
      [ linearLayout
          [ width MATCH_PARENT
          , height WRAP_CONTENT
          , orientation VERTICAL
          , UIUtils.contentMargin state.configPayload
          ]
          ( if CPUtils.ifCombinedWallets state.configPayload then
              [ PPUtils.getSectionHeader "Wallets" state.configPayload false ]
                <> ( UICommons.addCurvedWrapper state.configPayload
                      $ ( linkedWalletsView push state <>
                          unlinkedWalletsView push state useLabel
                        )
                  )
            else
              [ getLinkedSectionView push state
              , getUnlinkedSectionView push state useLabel
              ]
          )
      ]
  , linearLayout [ width MATCH_PARENT, height $ V 0, weight 1.0 ] []
  , PrimaryButton.view
      (push <<< PrimaryButtonAction)
      (PrimaryButtonConfig.Config newConfig)
  ]
  where
  lang = fromMaybe "" (state.configPayload # unwrap # _.language)

  PrimaryButtonConfig.Config pconf = UIConfig.primaryButtonConfig state.configPayload

  newConfig =
    pconf
      { text = STR.getLinkNow lang
      , visible =
        ( if not useLabel then
            ( if state.selected /= "" && not state.isSelectedLinked then
                VISIBLE
              else
                GONE
            )
          else
            GONE
        )
      , margin = (Margin 0 0 0 20)
      , startAnimation = state.buttonAnim
      }

  useLabel = true

getLinkedSectionView :: ∀ w. (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
getLinkedSectionView push state =
  linearLayout
    [ width MATCH_PARENT
    , height WRAP_CONTENT
    , orientation VERTICAL
    , visibility
        ( if length state.linkedWallets /= 0 then
            VISIBLE
          else
            GONE
        )
    , UIUtils.sectionMargin state.configPayload
    ]
    ( [ PPUtils.getSectionHeader header state.configPayload false ] <>
      ( UICommons.addCurvedWrapper state.configPayload $ linkedWalletsView push state)
    )
  where
  header = STR.getLinkWalletHeader $ fromMaybe "" (state.configPayload # unwrap # _.language)

linkedWalletsView :: ∀ w. (Action -> Effect Unit) -> State -> Array (PrestoDOM (Effect Unit) w)
linkedWalletsView push state = do
  WalletViewUtils.getLinkedWalletsView
    { pushReceived : (WalletViewUtils.WalletScreenAction (push))
    , storedWallets : state.linkedWallets
    , disabledWallets : WUtils.addDisableWallets state.payLaterEligibility
    , selectedWallet : state.selected
    , startBtnAnimation : state.buttonAnim
    , configPayload : state.configPayload
    , defaultOption : state.defaultOption
    , defWallet : state.defWallet
    , offers : state.offers
    , hideLastDivider : (true && not CPUtils.ifCombinedWallets state.configPayload)
    , amount : state.amount
    , delink : false
    , outages : state.outages
    }

getUnlinkedSectionView :: ∀ w. (Action -> Effect Unit) -> State -> Boolean -> PrestoDOM (Effect Unit) w
getUnlinkedSectionView push state useLabel =
  linearLayout
    [ width MATCH_PARENT
    , height WRAP_CONTENT
    , orientation VERTICAL
    , visibility
        ( if length state.unlinkedWallets /= 0 then
            VISIBLE
          else
            GONE
        )
    , UIUtils.sectionMargin state.configPayload
    ]
    ( [ (PPUtils.getSectionHeader header state.configPayload false) ]
        <> (UICommons.addCurvedWrapper state.configPayload $ unlinkedWalletsView push state useLabel)
    )
  where
  header = STR.getMoreWalletHeader $ fromMaybe "" (state.configPayload # unwrap # _.language)

unlinkedWalletsView :: ∀ w. (Action -> Effect Unit) -> State -> Boolean -> Array (PrestoDOM (Effect Unit) w)
unlinkedWalletsView push state useLabel =
  WalletViewUtils.getUnlinkedWalletsView
    { pushReceived : (WalletViewUtils.WalletScreenAction (push))
    , wallets : state.unlinkedWallets
    , disabledWallets : state.disabledWalletsFromConfig <> WUtils.addDisableWallets state.payLaterEligibility
    , selectedWalletCode : state.selected
    , configPayload : state.configPayload
    , ifVisible : true
    , payLaterEligibility : state.payLaterEligibility
    , useLabel
    , defaultOption : state.defaultOption
    , defWallet: state.defWallet
    , startButtonAnimation : state.buttonAnim
    , offers : state.offers
    , hideLastDivider : true
    , amount : state.amount
    , mandateType : state.mandateType
    , enableSI : state.enableSI
    , outages : state.outages
    }
