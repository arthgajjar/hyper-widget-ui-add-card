module View.Wallet.Screens.VerifyNumberScreen where

import HyperPrelude.External(Maybe(..),fromMaybe,toLower,Effect,(#),(==),Unit,(&&),(<<<),($),(<>),const)
import HyperPrelude.Internal (InputType(..), Length(..), Margin(..), Orientation(..), PrestoDOM, Screen, Visibility(..), alignParentBottom, alpha, background, color ,fontStyle, height, letterSpacing, linearLayout, margin, orientation, scrollBarY, scrollView, text, textSize, textView, visibility, width)

import Data.Newtype (unwrap)
import Data.String.Yarn (capWords)
import Engineering.Helpers.Events (addCustomBackPress)
import JBridge as JBridge
import PPConfig.Utils (fontColor)
import PPConfig.Utils as CPUtils
import Remote.Types (ConfigPayload(..), InputField(..))
import UI.Components.EditText.Config as EditTextConfig
import UI.Components.EditText.View as EditText
import UI.Components.PrimaryButton.Config as PrimaryButtonConfig
import UI.Components.PrimaryButton.View as PrimaryButton
import UI.Config as UIConfig
import UI.Constant.Str.Default as STR
import UI.Utils as UIUtils
import View.Wallet.Controllers.VerifyNumberController (Action(..), ScreenInput, ScreenOutput, State, eval, initialState, overrides)
import View.Stock.Container.Commons as UICommons
import View.Wallet.Screens.Utils as WUtils

screen :: ScreenInput -> Screen Action State ScreenOutput
screen input =
  { initialState: (initialState input)
  , name: "VerifyNumberScreen"
  , view
  , globalEvents: []
  , eval: eval
  }

view :: ∀ w. (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
view push state =
  UICommons.getParentLayout
    parentInput
    (UICommons.VerifyNumberAction push)
    (mobileNoValidationLayout push state)
    ( if btnAtBottom then
        Just $ proceedButton push state
      else
        Nothing
    )
  where
  btnAtBottom = CPUtils.isBtnAtBottom state.configPayload

  parentInput =
    { modalView: (CPUtils.ifModalView state.configPayload)
    , modalHeight: (V 500)
    , modalAnimationTrigger: state.entry
    , toolbarHeader: "Link Wallet"
    , configPayload: state.configPayload
    , useContainerPadding: true
    , useRelativeLayout: false
    , amount: state.amount
    , showToolbar: true
    , phoneNumber : state.checkoutPhoneNumber
    , showAmountBar : true
    , orderDescription : state.orderDesc
    , customerName : state.customerName
    }

mobileNoValidationLayout :: ∀ w. (Action -> Effect Unit) -> State -> Array (PrestoDOM (Effect Unit) w)
mobileNoValidationLayout push state =
  wrap
  [ linearLayout (
    [ width MATCH_PARENT
    , height MATCH_PARENT
    , orientation VERTICAL
    , addCustomBackPress push $ const BackPressed
    ] <> overrides "MainLayout" push state) $
    [ linearLayout
        [ width MATCH_PARENT
        , height MATCH_PARENT
        , orientation VERTICAL
        , UIUtils.contentMargin state.configPayload
        ] $
        [ linearLayout
            [ width MATCH_PARENT
            , height MATCH_PARENT
            , orientation VERTICAL
            , UIUtils.sectionMargin state.configPayload
            ]
            [ WUtils.labelView
                state.configPayload
                ((STR.getLink lang) <> " " <> (capWords $ toLower state.walletName))
                ("ic_" <> toLower state.walletName)
            , UICommons.uiCardLayout state.configPayload $
                [ numberView push state (EditTextConfig.Config newConfig)
                , textView
                    [ height MATCH_PARENT
                    , width MATCH_PARENT
                    , text $ STR.getOtpReqText lang
                    , fontStyle $ CPUtils.fontRegular state.configPayload
                    , color "#6B6B6B" -- TODO :: figure out way for font Alpha
                    , UIUtils.multipleLine "false"
                    , textSize $ CPUtils.fontSizeSmall state.configPayload
                    ]
                ]
            ]
        ] <> if btnAtBottom then [] else [ proceedButton push state ]
    ]
  ]
  where
  btnAtBottom = CPUtils.isBtnAtBottom state.configPayload
  ConfigPayload configPayload = state.configPayload
  lang = fromMaybe "" (state.configPayload # unwrap # _.language)
  wrap c = if btnAtBottom then c else UIUtils.wrapInScroll c
  EditTextConfig.Config editTextConfig = UIConfig.editTextConfig state.configPayload
  InputField inputFieldConfig = configPayload.inputField
  newConfig =
    editTextConfig
      { iconTextSize = (CPUtils.fontSizeVerySmall state.configPayload)
      , headerText = STR.getMobileNumberText lang
      , inputType = Numeric
      , margin = (Margin 0 0 0 24)
      , editTextId = (JBridge.getNewIDWithTag JBridge.MOBILE_NUM)
      , pattern = "^[0-9]+$,10"
      , inputTextMargin = if (toLower inputFieldConfig.type) == "boxed" then (Margin 12 0 12 0) else editTextConfig.inputTextMargin
      , hint = "e.g 1234567890"
      }

numberView :: ∀ w. (Action -> Effect Unit) -> State -> EditTextConfig.Config -> PrestoDOM (Effect Unit) w
numberView push state config =
  linearLayout
    [ height MATCH_PARENT
    , width MATCH_PARENT
    , orientation VERTICAL
    , margin (Margin 0 0 0 0)
    ]
    [ inputLabel (STR.getMobileNumberText lang) state
    , EditText.view
        (push <<< EditTextAction)
        config
    ]
  where
  lang = fromMaybe "" (state.configPayload # unwrap # _.language)

inputLabel :: ∀ w. String -> State -> PrestoDOM (Effect Unit) w
inputLabel label state =
  textView
    [ height MATCH_PARENT
    , width MATCH_PARENT
    , text label
    , fontStyle $ CPUtils.fontRegular state.configPayload
    , textSize $ CPUtils.fontSizeSmall state.configPayload
    , visibility
        if CPUtils.isMaterialInputField state.configPayload
        then GONE
        else VISIBLE
    , letterSpacing 0.7 -- TODO :: findout if this is required
    , margin (MarginBottom 8)
    ]

proceedButton :: ∀ w. (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
proceedButton push state =
  linearLayout (
    [ width MATCH_PARENT
    , height MATCH_PARENT
    ] <>
      if btnAtBottom then
        [ background $ CPUtils.btnBackground state.configPayload
        , alignParentBottom "true,-1"
        ]
      else [])
    [ PrimaryButton.view
        (push <<< PrimaryButtonAction)
        (PrimaryButtonConfig.Config updatedConfig)
    ]
  where
  btnAtBottom = CPUtils.isBtnAtBottom state.configPayload
  changeBtnText = CPUtils.changeBtnText state.configPayload
  PrimaryButtonConfig.Config newConfig = (UIConfig.primaryButtonConfig state.configPayload)
  updatedConfig =
    newConfig
      { startAnimation = state.buttonAnim
      , alpha = if state.isNumberValid then 1.0 else 0.5
      , text = if state.buttonAnim && changeBtnText then "Sending OTP ..." else "Send OTP"
      }
 -- TODO :: Extract these text from STR.
