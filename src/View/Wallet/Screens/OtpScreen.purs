module View.Wallet.Screens.OtpScreen where

import HyperPrelude.External(Maybe(..),fromMaybe,decimal, toStringAs,Effect,(-),(/),Unit,(*),(#),(==),($),(<>),(>),(<<<),const)
import HyperPrelude.Internal (Gravity(..), Margin(..), Orientation(..), Padding(..), PrestoDOM, Screen, Visibility(..), alignParentBottom, background, color, fontStyle, gravity, height, linearLayout, orientation, padding, text, textSize, textView, visibility, width)


import Data.Newtype (unwrap)
import Data.String (length, toLower)
import Data.String.Yarn (capWords)
import Engineering.Helpers.Events (addCustomBackPress)
import JBridge as JBridge
import PPConfig.Utils as CPUtils
import PrestoDOM.Types.DomAttributes (InputType(..), Length(..))
import Remote.Types (ConfigPayload(..), InputField(..))
import UI.Components.EditText.Config as EditTextConfig
import UI.Components.EditText.View as EditText
import UI.Components.PrimaryButton.Config as PrimaryButtonConfig
import UI.Components.PrimaryButton.View as PrimaryButton
import UI.Config as UIConfig
import UI.Constant.Str.Default as STR
import UI.Utils as UIUtils
import View.Wallet.Controllers.OtpScreenController (Action(..), Overrides(..), ScreenInput, ScreenOutput, State, eval, initialState, overrides)
import View.Stock.Container.Commons as UICommons
import View.Wallet.Screens.Utils as WUtils

screen :: ScreenInput -> Screen Action State ScreenOutput
screen input =
  { initialState: (initialState input)
  , view
  , name: "OtpScreen"
  , globalEvents: []
  , eval: eval
  }

view :: ∀ w. (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
view push state =
  UICommons.getParentLayout
    parentInput
    (UICommons.OtpAction push)
    (otpScreenLayout push state)
    ( if btnAtBottom then
        Just $ proceedButton push state
      else
        Nothing
    )
  where
  parentInput =
    { modalView: (CPUtils.ifModalView state.configPayload)
    , modalHeight: (V 500)
    , modalAnimationTrigger: state.entry
    , toolbarHeader: "Link Wallet"
    , configPayload: state.configPayload
    , useContainerPadding: true
    , useRelativeLayout: false
    , amount: state.amount
    , showToolbar: true
    , phoneNumber : state.mobileNumber
    , showAmountBar : true
    , orderDescription : state.orderDesc
    , customerName : state.customerName
    }

  btnAtBottom = CPUtils.isBtnAtBottom state.configPayload

otpScreenLayout :: ∀ w. (Action -> Effect Unit) -> State -> Array (PrestoDOM (Effect Unit) w)
otpScreenLayout push state =
  wrap
  [ linearLayout (
      [ height MATCH_PARENT
      , width MATCH_PARENT
      , orientation VERTICAL
      , addCustomBackPress push (const BackPressed)
      ] <> overrides MainLayout push state ) $
      [ linearLayout
          [ width MATCH_PARENT
          , height MATCH_PARENT
          , orientation VERTICAL
          , UIUtils.contentMargin state.configPayload
          ] $
          [ linearLayout
              [ width MATCH_PARENT
              , height MATCH_PARENT
              , orientation VERTICAL
              , UIUtils.sectionMargin state.configPayload
              ]
              [ WUtils.labelView
                  state.configPayload
                  ((STR.getLink lang) <> " " <> (capWords $ toLower state.walletName))
                  ("ic_" <> toLower state.walletName)
              , UICommons.uiCardLayout state.configPayload $
                  [ EditText.view
                      (push <<< EditTextAction)
                      (EditTextConfig.Config newConfig)
                  , textView
                      [ height MATCH_PARENT
                      , width MATCH_PARENT
                      , text ((STR.getOtpSentHeader lang) <> " " <> state.mobileNumber)
                      , fontStyle $ CPUtils.fontRegular state.configPayload
                      , color "#aaaaaa" --(CPUtils.fontColor state.configPayload)
                      , UIUtils.multipleLine "true"
                      , textSize (CPUtils.fontSize state.configPayload)
                      ]
                  , timerView push state
                  ]
              ]
          ] <> if btnAtBottom then [] else [ proceedButton push state ]
      ]
  ]
  where
  btnAtBottom = CPUtils.isBtnAtBottom state.configPayload
  lang = fromMaybe "" (state.configPayload # unwrap # _.language)
  wrap c = if btnAtBottom then c else UIUtils.wrapInScroll c
  ConfigPayload configPayload = state.configPayload
  EditTextConfig.Config editTextConfig = UIConfig.editTextConfig state.configPayload
  InputField inputFieldConfig = configPayload.inputField
  newConfig =
    editTextConfig
      { inputType = Numeric
      , hint = STR.getEnterOtp lang
      , headerText = "OTP"
      , textSize = (CPUtils.fontSizeVeryLarge state.configPayload)
      , margin = (Margin 0 0 0 24)
      , editTextId = (JBridge.getNewIDWithTag JBridge.OTP)
      , iconText = "Resend OTP" -- TODO :: expose this from STR
      , iconTextSize = (CPUtils.fontSizeSmall state.configPayload)
      , iconHeight = V 25
      , iconTextVisibility = if state.showLoader then GONE else VISIBLE
      , showLoader = state.showLoader
      , inputTextMargin = if (toLower inputFieldConfig.type) == "boxed" then (Margin 12 0 12 0) else editTextConfig.inputTextMargin
      }

timerView :: ∀ w. (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
timerView push state =
  linearLayout (
    [ height MATCH_PARENT
    , width MATCH_PARENT
    , orientation HORIZONTAL
    , padding $ PaddingVertical 32 16
    ] <> overrides TimerLayout push state )
    [ textView
        [ height $ V 32
        , width MATCH_PARENT
        , visibility
            if state.timeLeft > 0 then
              VISIBLE
            else
              GONE
        , text "Your OTP will expire in "
        , gravity CENTER_VERTICAL
        , color "#000000"
        , textSize $ CPUtils.fontSizeSmall state.configPayload
        , fontStyle $ CPUtils.fontRegular state.configPayload
        ]
    , textView
        [ height $ V 32
        , width WRAP_CONTENT
        , text
            if state.timeLeft > 0 then
              minutes' <> ":" <> seconds'
            else
              "OTP Expired"
        , gravity CENTER_VERTICAL
        , color $ CPUtils.primaryColor state.configPayload
        , textSize $ CPUtils.fontSizeSmall state.configPayload
        , fontStyle $ CPUtils.fontRegular state.configPayload
        ]
    , textView
        [ height $ V 32
        , width WRAP_CONTENT
        , visibility $ UIUtils.boolToVisibility $ state.timeLeft > 0
        , text "  mins"
        , gravity CENTER_VERTICAL
        , color "#000000"
        , textSize $ CPUtils.fontSizeSmall state.configPayload
        , fontStyle $ CPUtils.fontRegular state.configPayload
        ]
    ]
  where
  totalseconds = state.timeLeft
  minutes = secsToMin (totalseconds)
  seconds = minutesMinusSeconds minutes totalseconds

  minStr = toStringAs decimal minutes
  secStr = toStringAs decimal seconds

  minutes' = if length minStr == 2 then minStr else ("0" <> minStr)
  seconds' = if length secStr == 2 then secStr else ("0" <> secStr)

secsToMin :: Int -> Int
secsToMin secs = secs / 60

minutesMinusSeconds :: Int -> Int -> Int
minutesMinusSeconds minutes seconds = seconds - (minutes * 60)

proceedButton :: ∀ w. (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
proceedButton push state =
  linearLayout
    ( [ width MATCH_PARENT
      , height MATCH_PARENT
      ]
        <> if btnAtBottom then
            [ background $ CPUtils.btnBackground state.configPayload
            , alignParentBottom "true,-1"
            ]
          else
            []
    )
    [ PrimaryButton.view
        (push <<< PrimaryButtonAction)
        (PrimaryButtonConfig.Config updatedConfig)
    ]
  where
  btnAtBottom = CPUtils.isBtnAtBottom state.configPayload
  lang = state.configPayload # unwrap # _.language
  PrimaryButtonConfig.Config b = UIConfig.primaryButtonConfig state.configPayload

  updatedConfig =
    b
      { text = "Link Wallet" -- TODO :: expose this from STR
      , alpha = if state.isOtpValid then 1.0 else 0.5
      , startAnimation = state.buttonAnim
      }
