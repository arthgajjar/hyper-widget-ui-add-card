module View.Wallet.Screens.Utils where

import HyperPrelude.External(elem, find, length, (!!), filter,fromMaybe, Maybe(..), isJust, maybe, isNothing,toString,Effect,Unit,(/=),(&&),(<$>),(-),(#),(<>),(==),(||),($),(<<<),not,map)
import HyperPrelude.Internal (Gravity(..), Length(..), Margin(..), Orientation(..), Padding(..), PrestoDOM, Visibility(..),padding, color, fontStyle, gravity, height, imageUrl, imageView, linearLayout, margin, orientation, text, textSize, textView, width)

import Data.Foldable (elem)
import Data.Newtype (unwrap)
import Data.String (Pattern(..), split, null)
import Data.String as StrUtils
import Data.String as String
import Data.String.Yarn (capWords)
import PPConfig.Utils as CPUtils
import Engineering.Helpers.Commons (PaymentOffer)
import Payments.Wallets.Types as WalletTypes
import Payments.Wallets.Utils as WalletUtils
import Remote.Types (ConfigPayload(..))
import Service.EC.Types.Instruments as Instruments
import UI.Components.Message.Config as MessageConfig
import UI.Components.PaymentOptionsConfig as PayOptConfig
import UI.Components.PaymentOptionsView as PayOptView
import UI.Utils as UIUtils
import UI.Constant.Str.Default as STR
import View.PaymentManagement.Controllers.PaymentManagementController as PMScreenController
import View.PaymentPage.Controllers.Types (Action(..))
import View.PaymentPage.Controllers.Types as PPScreenController
import View.Wallet.Controllers.Controller as WalletScreenController
import View.PaymentPage.Screens.Utils (getDummyOffer, defaultOfferDesc)
import Payments.Wallets.Types (MandateType(..))
import Payments.Wallets.Utils (PaymentMethodsEligibility(..))

getUnlinkedWalletsView :: ∀ w . UnlinkedWalletsViewInput -> Array (PrestoDOM (Effect Unit) w)
getUnlinkedWalletsView input =
  wallets # map
    \i@(WalletTypes.Wallet item) ->
      PayOptView.view
        (pushAction i)
        (getUnlinkedWalletsConfig input i (ifLastItem item) enableMandate)
        false
        Nothing
  where
  -- enable mandate will change to
  -- enableMandate = if mandateType == Optional then CPUtils.isInstrumentEnabled input.configPayload "wallets" else true
  enableMandate = CPUtils.isInstrumentEnabled input.configPayload "wallets"
  wallets = disableDDSForPhonePe input.wallets -- TODO : TEMP CHANGE, REMOVE THIS LATER. (CONFIGURE IT FROM BACKEND)

  lastInd = (length wallets) - 1

  WalletTypes.Wallet lastWallet =
    fromMaybe WalletUtils.getDummyWallet $ wallets !! lastInd

  ifLastItem item = (item.code == lastWallet.code)

  pushAction walletData =
    case input.pushReceived of
      WalletScreenAction x ->
        (x <<< (WalletScreenController.WalletSelectAction walletData))
      PPScreenAction y ->
        (y <<< (PPScreenController.UnlinkedWalletAction walletData))
      PMScreenAction z ->
        (z <<< (PMScreenController.SelectUnlinkedWallet (PMScreenController.UnlinkedWallet (walletData))))

getLinkedWalletsView :: ∀ w. LinkedWalletsViewInput -> Array (PrestoDOM (Effect Unit) w)
getLinkedWalletsView input =
  map
    (\singleWallet ->
      PayOptView.view (pushAction singleWallet)
      (getLinkedWalletConfig input singleWallet (ifLastItem singleWallet))
      false
      Nothing)
  input.storedWallets
  where
  lastInd = (length input.storedWallets) - 1

  Instruments.StoredWallet (Instruments.Wallet lastWallet) =
    fromMaybe  WalletUtils.getDummyStoredWallet $ input.storedWallets !! lastInd

  ifLastItem (Instruments.StoredWallet (Instruments.Wallet item)) = item.id == lastWallet.id

  pushAction walletData =
    case input.pushReceived of
      WalletScreenAction x ->
        (x <<< (WalletScreenController.LinkedWalletSelectAction walletData))
      PPScreenAction y ->
        (y <<< (PPScreenController.StoredWalletAction $ PPScreenController.StoredWallet walletData))
      PMScreenAction z ->
        (z <<< (PMScreenController.SelectLinkedWallet (PMScreenController.SavedWallet walletData)))

getLinkedWalletConfig :: LinkedWalletsViewInput -> Instruments.StoredWallet -> Boolean -> PayOptConfig.Config
getLinkedWalletConfig input (Instruments.StoredWallet (Instruments.Wallet wallet)) ifLastItem = let
  --offerWallets = find (\offer -> StrUtils.toUpper (offer.paymentMethod) == (fromMaybe "" wallet.wallet)) input.offers
  offerText = "" --maybe "" (\b -> b.offerText) offerWallets
  offerDescription = "" --maybe "" (\b -> b.offerDescription) offerWallets
  offerText' =
    if offerDescription /= "" then
      UIUtils.offerDescHTML offerText input.configPayload
    else offerText
  isDown = (fromMaybe "NoName" wallet.wallet) `elem` input.outages
  restrictPayment = CPUtils.restrictOutagePayment input.configPayload
  PayOptConfig.Config config = (PayOptConfig.defConfig input.configPayload)
  MessageConfig.Config defMessageConf = config.outageMessageConfig
  messageConf = MessageConfig.Config defMessageConf
    { visibility = if isDown then VISIBLE else GONE
    , text = (CPUtils.outageRowMessage input.configPayload) <> " " <> (fromMaybe "NoName" wallet.wallet)
    , imageVisibility = VISIBLE
    }
  listPaymentOptionsNew = config
    { cvvInputVisibility = GONE
    , cvvId = wallet.id <> "_id"
    , primaryText = capWords $ StrUtils.toLower $ (fromMaybe "NoName" wallet.wallet) <> " Wallet"
    , radioButtonGravity = START
    , secondaryText = if ifDisabledWallet (fromMaybe "" wallet.wallet)
                        then STR.getPayLaterError <> (fromMaybe "" wallet.wallet)
                        else "Balance: "
    , secondaryText2 = if ifDisabledWallet (fromMaybe "" wallet.wallet)
                        then ""
                        else "₹ " <> (toString $ fromMaybe 0.0 wallet.currentBalance)
    , secondaryTextVisibility = if not (isNothing wallet.currentBalance) || ifDisabledWallet (fromMaybe "" wallet.wallet) then VISIBLE else GONE
    , logoUrl = "ic_" <> (String.toLower (fromMaybe "" wallet.wallet))
    , radioButtonIconUrl =
        if input.selectedWallet == (fromMaybe "" wallet.wallet)
            then "tick"
            else "circular_radio_button"
    , inputAreaVisibility = if input.delink
                                then GONE
                                else if input.selectedWallet == (fromMaybe "" wallet.wallet)
                                        then VISIBLE
                                        else GONE
    , startAnimation = input.startBtnAnimation
    , secondaryTextColor = "#5C5C5C"
    , secondaryLogoVisibility = GONE
    , quantText = offerText'
    , quantVisibility = if CPUtils.ifOfferVisible input.configPayload && true then VISIBLE else GONE
    , quantTextFromHTML = (offerDescription /= "")
    , enableQuantLayoutClick = (offerDescription /= "")
    , topRowVisibility =
        if conf.topRowVisibility == "false"
            then GONE
            else if input.defWallet /= (fromMaybe "" wallet.wallet)
                then VISIBLE
                else GONE
    , topRowImage =
        if input.defaultOption == (fromMaybe "" wallet.wallet)
            then "checkbox1"
            else "checkbox"
    , lineSeparatorVisibility =
        if input.hideLastDivider
            then if ifLastItem
                then GONE
                else CPUtils.lineSepVisibility input.configPayload
            else VISIBLE
    , buttonText =
        if CPUtils.changeBtnText input.configPayload &&  input.startBtnAnimation
            then "Processing Payment"
            else config.buttonText
    , ifClickable = not (ifDisabledWallet (fromMaybe "" wallet.wallet))
    , buttonAlpha = if restrictPayment && isDown then 0.4 else 1.0
    , buttonClickable = not (restrictPayment && isDown)
    , secondaryTextFont = CPUtils.fontRegular input.configPayload
    , radioButtonVisibility = if not (ifDisabledWallet (fromMaybe "" wallet.wallet)) then VISIBLE else GONE
    , selectionType = if input.delink
                        then PayOptConfig.Label
                        else PayOptConfig.Checkbox
    , selectionLabel = "Delink"
    , displayAreaAlpha = if (ifDisabledWallet (fromMaybe "" wallet.wallet)) then 0.5 else 1.0
    , labelFont = CPUtils.fontSemiBold input.configPayload
    , outageMessageConfig = messageConf
    }
  in (PayOptConfig.Config listPaymentOptionsNew )
  where
  ConfigPayload conf = input.configPayload
  ifDisabledWallet wallet = wallet `elem` input.disabledWallets

getUnlinkedWalletsConfig :: UnlinkedWalletsViewInput -> WalletTypes.Wallet -> Boolean -> Boolean -> PayOptConfig.Config
getUnlinkedWalletsConfig input (WalletTypes.Wallet item) ifLastItem mandateSupport = let
  enableMandate = item.mandateSupport && mandateSupport
  secondaryTextVal =
    if ifDisabledWallet item.code
      then STR.getPayLaterError <> item.code -- TODO: add according to condition:  STR.disabled
      else if enableMandate && input.mandateType /= None then "Autopay available" else ""
  --offerWallets = find (\offer -> StrUtils.toUpper (offer.paymentMethod) == item.code) input.offers
  offerDescription = "" --maybe "" (\b -> b.offerDescription) offerWallets
  offerVisible = CPUtils.ifOfferVisible input.configPayload && true
  offerText = "" --maybe "" (\b -> b.offerText) offerWallets
  offerText' =
    if offerDescription /= "" then
      UIUtils.offerDescHTML offerText input.configPayload
    else offerText
  isDown = item.code `elem` input.outages
  restrictPayment = CPUtils.restrictOutagePayment input.configPayload
  PayOptConfig.Config config = PayOptConfig.defConfig input.configPayload
  MessageConfig.Config defMessageConf = config.outageMessageConfig
  messageConf = MessageConfig.Config defMessageConf
    { visibility = if isDown then VISIBLE else GONE
    , text = (CPUtils.outageRowMessage input.configPayload) <> " " <> (capWords $ StrUtils.toLower $ item.code)
    }
  updateConfig = config
    { primaryText = (capWords $ StrUtils.toLower $ item.code)
    , secondaryTextColor = if enableMandate then (CPUtils.tertiaryFontColor input.configPayload)  else "#5C5C5C"
    , radioButtonGravity = if secondaryTextVal /= "" then START else config.radioButtonGravity
    , secondaryText = secondaryTextVal
    , secondaryTextAlpha = 1.0
    , displayAreaAlpha = if (ifDisabledWallet item.code) then 0.5 else 1.0
    , secondaryTextVisibility = if (ifDisabledWallet item.code) then VISIBLE else if enableMandate && input.mandateType /= None then VISIBLE else GONE
    , logoUrl = "ic_" <> (StrUtils.toLower item.code)
    , secondaryTextFont = CPUtils.fontSemiBold input.configPayload
    , radioButtonIconUrl =
        if item.code == input.selectedWalletCode
            then "tick"
            else "circular_radio_button"
    , radioButtonVisibility =
        if (ifDisabledWallet item.code)
          then GONE
          else if input.useLabel && item.directDebitSupport
                then VISIBLE
                else if item.code == input.selectedWalletCode
                    then VISIBLE
                    else if not item.directDebitSupport
                        then VISIBLE
                        else GONE
    , selectionType = if input.useLabel && item.directDebitSupport
                        then PayOptConfig.Label
                        else PayOptConfig.Checkbox
    , buttonText = config.buttonText
    , ifClickable = not (ifDisabledWallet item.code)
    , buttonAlpha = if restrictPayment && isDown then 0.4 else 1.0
    , buttonClickable = not (restrictPayment && isDown)
    , inputAreaVisibility = if not item.directDebitSupport && item.code == input.selectedWalletCode
                                then VISIBLE
                                else GONE
    , cvvInputVisibility = GONE
    , cvvId = item.code <> "_id" -- patch added due to repeating id  "default"
    , tertiaryVisibility = GONE
    , tertiaryText = if (enableMandate && input.mandateType /= None)
                            then "Autopay Available"
                            else ""
    , useTopRowForSI = input.mandateType /= None
    , topRowText = if input.mandateType == None  then "Make this default my option" else "Automatically debit to renew subscription for every billing cycle"
    , isTopRowClickable = input.mandateType /= Required
    , topRowVisibility = if input.mandateType /= None && enableMandate then VISIBLE
                            else GONE-- if defWallet /= item.code then VISIBLE else GONE
    , topRowImageVisibility = if input.mandateType == Required then GONE else VISIBLE
    , topRowImage = if input.enableSI
                        then "checkbox1"
                        else "checkbox"
    , tertiaryTextColor = "#689F38"
    , quantText = offerText'
    , quantTextFromHTML = (offerDescription /= "")
    , enableQuantLayoutClick = (offerDescription /= "")
    , quantVisibility = if (offerVisible) then VISIBLE else GONE
    , displayAreaHeight = V $ (CPUtils.listItemHeight input.configPayload)
    , startAnimation = input.startButtonAnimation
    , outageMessageConfig = messageConf
    , lineSeparatorVisibility =
        if input.hideLastDivider
          then if ifLastItem
            then GONE
            else CPUtils.lineSepVisibility input.configPayload
          else VISIBLE
    }
  in (PayOptConfig.Config updateConfig)
  where
  ifDisabledWallet wallet = wallet `elem` input.disabledWallets

disableDDSForPhonePe :: Array WalletTypes.Wallet -> Array WalletTypes.Wallet
disableDDSForPhonePe wallets =
  (\w@(WalletTypes.Wallet wallet) -> do
  if wallet.name == "PHONEPE" || wallet.code == "PHONEPE"
      then (WalletTypes.Wallet $ wallet {directDebitSupport = false})
      else w
  ) <$> wallets

data ActionType
  = WalletScreenAction (WalletScreenController.Action -> Effect Unit)
  | PPScreenAction (PPScreenController.Action -> Effect Unit)
  | PMScreenAction (PMScreenController.Action -> Effect Unit)

labelView :: ∀ w. ConfigPayload -> String -> String -> PrestoDOM (Effect Unit) w
labelView configPayload labelText labelImage =
  linearLayout
  [ height MATCH_PARENT
  , width MATCH_PARENT
  , orientation HORIZONTAL
  , gravity CENTER_VERTICAL
  , padding (Padding 12 16 12 16)
  --, margin m
  ]
  [ textView
      [ height MATCH_PARENT
      , width MATCH_PARENT
      , text labelText
      , fontStyle $ CPUtils.fontBold configPayload
      , color $ CPUtils.fontColor configPayload
      , textSize $ CPUtils.fontSize configPayload
      --, margin (MarginRight 8)
      ]
  , imageView
      [ width $ V (CPUtils.iconSizeSmall configPayload)
      , height $ V (CPUtils.iconSizeSmall configPayload)
      , imageUrl labelImage
      ]
  ]
  where
  lang = (configPayload # unwrap # _.language)
  hSpace = CPUtils.horizontalSpace configPayload
  uiPadding = CPUtils.uiCardHorizontalPadding configPayload
  tM = if CPUtils.uiCardTranslation configPayload == 0.0
        then 0
        else UIUtils.translationMargin
  m = if hSpace == 0
      then (Margin uiPadding 0 uiPadding 10)
      else (Margin tM 0 0 10)


type UnlinkedWalletsViewInput =
  { pushReceived :: ActionType
  , wallets :: Array WalletTypes.Wallet
  , disabledWallets :: Array String
  , selectedWalletCode :: String
  , configPayload :: ConfigPayload
  , ifVisible :: Boolean
  , payLaterEligibility :: Array PaymentMethodsEligibility
  , useLabel :: Boolean
  , defaultOption :: String
  , defWallet :: String
  , startButtonAnimation :: Boolean
  , hideLastDivider :: Boolean
  , offers :: Array PaymentOffer
  , amount :: Number
  , mandateType :: MandateType
  , enableSI :: Boolean
  , outages :: Array String
  }

type LinkedWalletsViewInput =
  { pushReceived :: ActionType
  , storedWallets :: Array Instruments.StoredWallet
  , disabledWallets :: Array String
  , selectedWallet :: String
  , startBtnAnimation :: Boolean
  , configPayload :: ConfigPayload
  , defaultOption :: String
  , defWallet :: String
  , offers :: Array PaymentOffer
  , hideLastDivider :: Boolean
  , amount :: Number
  , delink :: Boolean
  , outages :: Array String
  }
