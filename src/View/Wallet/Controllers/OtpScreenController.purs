module View.Wallet.Controllers.OtpScreenController where

import HyperPrelude.External(filter, (!!),Maybe(..), fromMaybe,launchAff_, delay, Milliseconds(..),Effect,not, (==),Unit,($),bind,const,(*),liftEffect,pure,(<=),(-))
import HyperPrelude.Internal (Eval, Props, afterRender, continue, continueWithCmd, updateAndExit)

import Engineering.Helpers.Events (attachTimer)
import JBridge as JBridge
import Remote.Types (ConfigPayload)
import UI.Components.EditText.Controller as EditText
import UI.Components.PrimaryButton.Controller as PrimaryButton
import UI.Components.ToolBar.Controller as ToolBar
import UI.Utils (ModalAction(..))
import UI.Validation as Validation
import Service.EC.Types.Instruments (Offer(..), StoredCard(..), StoredWallet(..),Wallet(..))
import Payments.NetBanking.Utils (Bank(..))
import UI.Components.Popup.Controller as Popup
import UI.Components.PaymentOptionsController as PaymentOptionsController
import Remote.Types (MerchantOffer(..))

type ScreenInput =
    { walletName :: String
    , mobileNumber :: Maybe String
    , configPayload :: ConfigPayload
    , amount :: Number
    , orderDesc :: String
    , customerName :: String
    , mid :: String
    , allBanks :: Array Bank
    , allCards :: Array StoredCard
    , allWallets :: Array StoredWallet
    , isOffer :: Boolean
    }

data ScreenOutput
  = VerifyOtp String
  | ResendOtp
  | OnBackPress
  | Switch String
  | SwitchNav String
  | PayUsingNB String
  | PayUsingWallet String String Boolean Boolean


data Action
  = StockAction
  | ToolBarAction ToolBar.Action
  | EditTextAction EditText.Action
  | PrimaryButtonAction PrimaryButton.Action
  | BackPressed
  | OtpScreenRendered String
  | ContinueCommand
  | TimerRun Int
  | OverlayClick ModalAction
  | OfferPopupAction Popup.Action
  | SideBarAction String PaymentOptionsController.Action
  | NavBarAction String PaymentOptionsController.Action
  | OfferApplied MerchantOffer
  | ConfigApplied ConfigPayload
  | DefaultOptionAction SelectedPaymentInstrument PaymentOptionsController.Action

data SelectedPaymentInstrument =  DefaultPaymentOptionCard StoredCard
            | DefaultPaymentOptionWallet StoredWallet
            | DefaultPaymentOptionNB
            | DefaultPaymentOptionVPA
            | DefaultPaymentOptionUPIApp
            | CashOnDelivery
            | CardOnDelivery
            | AskAF
            | SavedWalletItem StoredWallet
            | PrefferedPaymentOption String
            | UpiPrefferd String
            | SavedCardItem StoredCard
            | NetBank Bank
            | PrefferedNB String
            | Offers Offer

data Overrides
  = MainLayout
  | TimerLayout

type State =
    { textSize :: Int
    , focus :: Boolean
    , otp :: String
    , isOtpValid :: Boolean
    , walletName :: String
    , entry :: Boolean
    , buttonAnim :: Boolean
    , configPayload :: ConfigPayload
    , mobileNumber :: String
    , amount :: Number
    , showLoader :: Boolean
    , expireTTL :: Int
    , timeLeft :: Int
    , orderDesc :: String
    , customerName :: String
    , isClicked :: Boolean
    , mid :: String
    , allBanks :: Array Bank
    , isOffer :: Boolean
    , allCards :: Array StoredCard
    , allWallets :: Array StoredWallet
    }


initialState :: ScreenInput -> State
initialState input =
    { textSize : 18
    , focus : true
    , otp : ""
    , isOtpValid : false
    , walletName : input.walletName
    , entry : true
    , buttonAnim : false
    , configPayload : input.configPayload
    , mobileNumber : (fromMaybe "entered number" input.mobileNumber)
    , amount : input.amount
    , showLoader : false
    , expireTTL : 600
    , timeLeft : 600
    , orderDesc : input.orderDesc
    , customerName : input.customerName
    , isClicked : false
    , mid : input.mid
    , allBanks : input.allBanks
    , isOffer : input.isOffer
    , allCards : input.allCards
    , allWallets : input.allWallets
    }

eval :: Action -> State -> Eval Action ScreenOutput State

eval BackPressed state =
    let updatedState = state {entry = not state.entry} in
    updateAndExit updatedState $ OnBackPress

eval (OverlayClick ClickedOutside) state =
    let updatedState = state {entry = not state.entry} in
    updateAndExit updatedState $ OnBackPress

eval (EditTextAction action) state =
    case action of
        (EditText.OnChanged text) -> continue state { otp = text
                                                    , isOtpValid = Validation.validateData (Validation.WALLET_OTP state.walletName) text
                                                    }

        (EditText.EditTextButtonClick) -> updateAndExit state {showLoader = true} ResendOtp

        _ -> continue state

eval (ToolBarAction ToolBar.Clicked) state =
    let updatedState = state {entry = not state.entry} in
    updateAndExit updatedState $ OnBackPress

eval (PrimaryButtonAction (PrimaryButton.Clicked)) state =
    if state.isOtpValid
        then
            let newState = state {buttonAnim = true } in
            updateAndExit newState $ VerifyOtp state.otp
        else continue state

eval (OtpScreenRendered id) state =
    continueWithCmd state [
        do
        _ <- launchAff_ do
            _ <- delay (Milliseconds 300.0)
            liftEffect $ JBridge.requestKeyboardShow id
        pure $ ContinueCommand
    ]

eval (TimerRun _) state = do
    let remainingTime = if state.timeLeft <= 1
                        then 0
                        else state.timeLeft - 1
    continue state {timeLeft = remainingTime}

eval _ state = continue state

overrides :: Overrides -> (Action -> Effect Unit) -> State -> Props (Effect Unit) --differnet in Web
overrides MainLayout push state =
    [ afterRender push $ const (OtpScreenRendered (JBridge.getNewIDWithTag JBridge.OTP)) ]
overrides TimerLayout push state = [attachTimer (state.expireTTL * 1000) 1000 push TimerRun]

getWalletToken :: String -> State -> String
getWalletToken name state = do
    let selected = filter (\(StoredWallet (Wallet wallet)) -> wallet.wallet == Just name) (state.allWallets)
    let x = selected !! 0
    case x of
        Just (StoredWallet (Wallet wallet)) -> fromMaybe "" wallet.token
        _ -> ""
