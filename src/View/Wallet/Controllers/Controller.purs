module View.Wallet.Controllers.Controller where

import HyperPrelude.External(filter, (!!), find,Maybe, fromMaybe, maybe,(==),($),not,(||),(&&))
import HyperPrelude.Internal (Eval, continue, updateAndExit)
import Data.String as String
import Data.Foldable (elem)
import Service.EC.Types.Instruments (Offer(..), StoredCard(..), StoredWallet(..))
import Payments.NetBanking.Utils (Bank(..))
import PPConfig.Utils (getDisabledPayLaterWallets, getDisabledWallets)
import Engineering.Helpers.Commons(PaymentOffer(..))
import Payments.Wallets.Types (Wallet)
import Payments.Wallets.Types as WalletTypes
import Remote.Types (ConfigPayload)
import Service.EC.Types.Instruments as Instruments
import UI.Components.PaymentOptionsController as PaymentOptionsController
import UI.Components.PrimaryButton.Controller as PrimaryButton
import UI.Components.ToolBar.Controller as ToolBar
import UI.Utils (ModalAction(..))
import UI.Components.Popup.Controller as Popup
import PPConfig.Utils as CPUtils
import Payments.Wallets.Types (MandateType(..))
import Payments.Wallets.Utils (PaymentMethodsEligibility(..))

type ScreenInput  =
  { unlinkedWallets :: Array WalletTypes.Wallet
  , linkedWallets :: Array Instruments.StoredWallet
  , configPayload :: ConfigPayload
  , defWallet :: String
  , amount :: Number
  , isPayLater :: Boolean
  , payLaterEligibility :: Array PaymentMethodsEligibility
  , offers :: Array PaymentOffer
  , justLinked :: Maybe String
  , phoneNumber :: String
  , orderDesc :: String
  , mandateType :: MandateType
  , outages :: Array String
  , mandateFeature :: Maybe Boolean
  , mid :: String
  , customerName :: String
  , allBanks :: Array Bank
  , allCards :: Array StoredCard
  , allWallets :: Array StoredWallet
  }

data ScreenOutput
  = OnBackPress
  | UseUnLinkedWallet String
  | UseLinkedWallet String String Boolean Boolean
  | Switch String
  | SwitchNav String
  | OnGuestLogin
  | PayUsingNB String
  | PayUsingWallet String String Boolean Boolean

data Action
  = PrimaryButtonAction PrimaryButton.Action
  | WalletSelectAction WalletTypes.Wallet PaymentOptionsController.Action
  | BackPress
  | ToolBarAction ToolBar.Action
  | LinkedWalletSelectAction Instruments.StoredWallet PaymentOptionsController.Action
  | OverlayClick ModalAction
  | OfferPopupAction Popup.Action
type State =
  { unlinkedWallets :: Array WalletTypes.Wallet
  , linkedWallets :: Array Instruments.StoredWallet
  , selected :: String
  , selectedToken :: String
  , focus :: Boolean
  , disabledWalletsFromConfig :: Array String
  , entry :: Boolean
  , buttonAnim :: Boolean
  , configPayload :: ConfigPayload
  , isSelectedLinked :: Boolean
  , defWallet :: String
  , defaultOption :: String
  , amount :: Number
  , isPayLater :: Boolean
  , payLaterEligibility :: Array PaymentMethodsEligibility
  , offers :: Array PaymentOffer
  , phoneNumber :: String
  , orderDesc :: String
  , mandateType :: MandateType
  , enableSI :: Boolean
  , showOfferDescPopup :: Boolean
  , activeOfferDesc :: String
  , outages :: Array String
  , customerName :: String
  }


ifDisableWalletsFromConfig :: Array String -> String -> Boolean
ifDisableWalletsFromConfig disabledWalletsFromConfig wallet = wallet `elem` disabledWalletsFromConfig

-- | If same wallets is clicked collapse it or else expand it
patchSelectedWallet :: String -> State -> State
patchSelectedWallet walletCode state = state {
    isSelectedLinked = false,
    selected = if (ifDisableWalletsFromConfig state.disabledWalletsFromConfig walletCode)
                    then state.selected
                    else if state.selected == walletCode
                        then ""
                        else walletCode
    }


initialState :: ScreenInput -> State
initialState input =
    { selected : fromMaybe "" input.justLinked
    , selectedToken : getWalletToken input.justLinked input.linkedWallets
    , unlinkedWallets : input.unlinkedWallets
    , linkedWallets : input.linkedWallets
    , focus : false
    , disabledWalletsFromConfig :
        if input.isPayLater
            then fromMaybe [] $ getDisabledPayLaterWallets input.configPayload
            else fromMaybe [] $ getDisabledWallets input.configPayload
    , entry : true
    , buttonAnim : false
    , configPayload : input.configPayload
    , isSelectedLinked : false
    , defWallet : input.defWallet
    , defaultOption : ""
    , amount : input.amount
    , payLaterEligibility : input.payLaterEligibility
    , isPayLater : input.isPayLater
    , offers : input.offers
    , phoneNumber : input.phoneNumber
    , orderDesc : input.orderDesc
    , mandateType : input.mandateType
    , enableSI : false
    , showOfferDescPopup : false
    , activeOfferDesc : ""
    , outages : input.outages
    , customerName : input.customerName
    }
    where
    getWalletToken :: Maybe String -> Array Instruments.StoredWallet -> String
    getWalletToken walletName linkedWallets =
        maybe "" (\(Instruments.StoredWallet (Instruments.Wallet w)) -> fromMaybe "" w.token)
            $ filter (\(Instruments.StoredWallet (Instruments.Wallet w)) -> w.wallet == walletName) linkedWallets !! 0


eval :: Action -> State -> Eval Action ScreenOutput State --Either (Tuple (Maybe State) ScreenOutput) (Tuple State (Array (Effect w)))

eval (ToolBarAction ToolBar.Clicked) state =
    let updatedState = state {entry = not state.entry} in
    updateAndExit updatedState $ OnBackPress

eval BackPress state =
    let updatedState = state {entry = not state.entry} in
    updateAndExit updatedState $ OnBackPress

eval (OverlayClick ClickedOutside) state =
    let updatedState = state {entry = not state.entry} in
        updateAndExit updatedState $ OnBackPress

eval (LinkedWalletSelectAction (Instruments.StoredWallet (Instruments.Wallet savedWalletItem)) action) state =
  case action of
    PaymentOptionsController.PaymentListItemSelection -> let
      code = fromMaybe "" savedWalletItem.wallet
      in
      if state.isSelectedLinked then
        continue $ patchSelectedWallet code state
      else
        continue $
          state { selected = (fromMaybe "" savedWalletItem.wallet)
                , isSelectedLinked = true
                , selectedToken = fromMaybe "" savedWalletItem.token
                }

    PaymentOptionsController.PaymentListAction PaymentOptionsController.ButtonClicked -> let
      restrictPayment = CPUtils.restrictOutagePayment state.configPayload
      isDown = (state.selected `elem` state.outages) || ("" `elem` state.outages)
      in
      if restrictPayment && isDown then continue state
      else

        updateAndExit
          (state { selected = (fromMaybe "" savedWalletItem.wallet)
                , isSelectedLinked = true
                , selectedToken = fromMaybe "" savedWalletItem.token
                , buttonAnim = true
                })
          $ UseLinkedWallet state.selected state.selectedToken (state.selected == state.defaultOption) false -- hard coded false

    PaymentOptionsController.PaymentListAction PaymentOptionsController.SetDefault -> let
      walletCode = (fromMaybe "" savedWalletItem.wallet)
      defOpt = state.defaultOption == walletCode
      in continue state { defaultOption = if defOpt then "" else walletCode}

    PaymentOptionsController.QuantLayoutClick -> let
      --offerWallets = find (\offer -> String.toUpper (offer.paymentMethod) == (fromMaybe "" savedWalletItem.wallet)) state.offers
      offerDescription = "" --maybe "" (\b -> b.offerDescription) offerWallets
      in
      if offerDescription == "" then continue state
      else continue state {showOfferDescPopup = true, activeOfferDesc = offerDescription}


    _ -> continue state

eval (WalletSelectAction (WalletTypes.Wallet wallet) action) state = let
  code = wallet.code in
  case action of
    PaymentOptionsController.PaymentListAction PaymentOptionsController.SetDefault ->
      let defOpt = state.defaultOption == code
      in continue state { defaultOption = if defOpt then "" else code}

    PaymentOptionsController.PaymentListItemSelection ->
      continue $ patchSelectedWallet code state

    PaymentOptionsController.PaymentListItemButtonClick  ->  let
      restrictPayment = CPUtils.restrictOutagePayment state.configPayload
      isDown = (state.selected `elem` state.outages) || ("" `elem` state.outages)
      in
      if restrictPayment && isDown then continue state
      else
        if (ifDisableWalletsFromConfig state.disabledWalletsFromConfig code) then
          continue state
        else
          let newState = state {selected = code } in
          updateAndExit newState $ UseUnLinkedWallet code

    PaymentOptionsController.PaymentListAction PaymentOptionsController.ButtonClicked ->
      updateAndExit (state { buttonAnim = true }) $ UseLinkedWallet code "" (state.selected == state.defaultOption) (state.enableSI && wallet.mandateSupport)

    PaymentOptionsController.QuantLayoutClick -> let
      --offerWallets = find (\offer -> String.toUpper (offer.paymentMethod) == wallet.code) state.offers
      offerDescription = "" --maybe "" (\b -> b.offerDescription) offerWallets
      in
      if offerDescription == "" then continue state
      else continue state {showOfferDescPopup = true, activeOfferDesc = offerDescription}

    _ -> continue state

eval (PrimaryButtonAction (PrimaryButton.Clicked)) state = let
    restrictPayment = CPUtils.restrictOutagePayment state.configPayload
    isDown = (state.selected `elem` state.outages) || ("" `elem` state.outages)
    newState = state {buttonAnim = true }
    in
    if restrictPayment && isDown then continue state
    else updateAndExit newState $ UseUnLinkedWallet state.selected

eval (OfferPopupAction action) state = case action of
  Popup.OverlayClick -> continue state {showOfferDescPopup = false}
  Popup.HeaderIconClick -> continue state {showOfferDescPopup = false}
  Popup.ButtonOneClick -> continue state {showOfferDescPopup = false}
  _ -> continue state

eval action state = continue state
