module View.Wallet.Controllers.StatusController where

import HyperPrelude.External(Effect,(<>),Unit,not,($),const)
import HyperPrelude.Internal(Eval, Props, continue, exit, onAnimationEnd, updateAndExit)
import Engineering.Helpers.Events (addCustomBackPress)
import Remote.Types (ConfigPayload)
import UI.Components.ToolBar.Controller as ToolBar
import UI.Constant.Str.Default as STR
import UI.Utils (ModalAction(..))
import Service.EC.Types.Instruments (Offer(..), StoredCard(..), StoredWallet(..))
import Payments.NetBanking.Utils (Bank(..))
import UI.Components.Popup.Controller as Popup

type ScreenInput  = String

data ScreenOutput = OnBackPress | ExitAfterRender| Switch String | SwitchNav String

data Action
  = BackPressed
  | DialogRendered
  | ToolBarAction ToolBar.Action
  | OverlayClick ModalAction
  | OfferPopupAction Popup.Action

type State =
  { status :: Boolean
  , walletName :: String
  , delink :: Boolean
  , entry :: Boolean
  , configPayload :: ConfigPayload
  , mid :: String
  , allBanks :: Array Bank
  , allCards :: Array StoredCard
  , allWallets :: Array StoredWallet
  , isOffer :: Boolean
  }

initialState :: String -> Boolean -> Boolean -> ConfigPayload -> String -> Array (Bank)-> Array StoredCard -> Array StoredWallet -> Boolean-> State
initialState wallet status delink configPayload mid allBanks allCards allWallets isOffer = { status : status , walletName : wallet, delink : delink, entry : true, configPayload, mid, allBanks,allCards, allWallets, isOffer}

getDisplayMessage :: String -> State -> String
getDisplayMessage language state = do
    if state.delink
      then if state.status
              then state.walletName <> " " <> STR.delinkSuccess language
              else state.walletName <> " " <> STR.delinkFail language
      else if state.status
              then state.walletName <> " " <> STR.linkSuccess language
              else STR.unableToLink language <> " " <> state.walletName

eval
  :: Action
  -> State
  -> Eval Action ScreenOutput State
eval action state = case action of

  BackPressed ->
    let updatedState = state {entry = not state.entry} in
    updateAndExit updatedState $ OnBackPress

  OverlayClick ClickedOutside ->
    let updatedState = state {entry = not state.entry} in
    updateAndExit updatedState $ OnBackPress

  DialogRendered -> exit $ ExitAfterRender

  ToolBarAction ToolBar.Clicked ->
    let updatedState = state {entry = not state.entry} in
    updateAndExit updatedState $ OnBackPress
  _ ->  continue state


overrides :: String -> (Action -> Effect Unit) -> State -> Props (Effect Unit)
overrides "MainLayout" push state = [
  addCustomBackPress push $ const BackPressed
  -- , afterRender push $ const DialogRendered
  ]
overrides "entry" push state = [onAnimationEnd push $ const DialogRendered]
overrides _ push state = []
