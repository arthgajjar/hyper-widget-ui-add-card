module View.Wallet.Controllers.DelinkController where

import HyperPrelude.External(($),(==),not)
import HyperPrelude.Internal (Eval, continue, updateAndExit)
import Service.EC.Types.Instruments as Instruments
import Engineering.Helpers.Commons (log)
import Remote.Types (ConfigPayload)
import UI.Components.PaymentOptionsController as PaymentOptionsController
import UI.Components.PrimaryButton.Controller as PrimaryButton
import UI.Components.ToolBar.Controller as ToolBar
import UI.Utils (ModalAction(..))
import UI.Components.Popup.Controller as Popup

type ScreenInput =
  { storedWallets :: Array Instruments.StoredWallet
  , configPayload :: ConfigPayload
  , phoneNumber :: String
  , amount :: Number
  }

data ScreenOutput = OnBackPress | DelinkWallet String String | Switch String

data Action
  = WalletSelectAction String String PaymentOptionsController.Action
  | ToolBarAction ToolBar.Action
  | PrimaryButtonAction PrimaryButton.Action
  | BackPress
  | OverlayClick ModalAction
  | OfferPopupAction Popup.Action

type State =
  { storedWallets :: Array Instruments.StoredWallet
  , selectedWalletName :: String
  , selectedWalletId :: String
  , entry :: Boolean
  , buttonAnim :: Boolean
  , configPayload :: ConfigPayload
  , phoneNumber :: String
  , amount :: Number
  }

initialState :: ScreenInput -> State
initialState input =
  { storedWallets : input.storedWallets
  , selectedWalletName: ""
  , selectedWalletId: ""
  , entry : true
  , buttonAnim : false
  , configPayload : input.configPayload
  , phoneNumber : input.phoneNumber
  , amount : input.amount
  }


eval :: Action -> State -> Eval Action ScreenOutput State

eval BackPress state =
    let updatedState = state {entry = not state.entry} in
    updateAndExit updatedState $ OnBackPress

eval (OverlayClick ClickedOutside) state =
    let updatedState = state {entry = not state.entry} in
    updateAndExit updatedState $ OnBackPress

eval (ToolBarAction ToolBar.Clicked) state =
    let updatedState = state {entry = not state.entry} in
    updateAndExit updatedState $ OnBackPress

eval (WalletSelectAction
        wallet id
        (PaymentOptionsController.PaymentListItemSelection) ) state = do
    continue state
        { selectedWalletId = if state.selectedWalletId == id then "" else id
        , selectedWalletName = if state.selectedWalletName == wallet then "" else wallet
        }

eval (PrimaryButtonAction (PrimaryButton.Clicked)) state =
    let updatedState = state { buttonAnim = true} in
     updateAndExit updatedState $
        DelinkWallet state.selectedWalletName state.selectedWalletId

eval action state = continue state where _ = log "Action -> " action
