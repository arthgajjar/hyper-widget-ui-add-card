module View.Wallet.Controllers.VerifyNumberController where

import HyperPrelude.External(Effect,launchAff_,delay,Milliseconds(..),liftEffect,Unit,not,($),const,bind,discard,pure)
import HyperPrelude.Internal(Eval, Props, afterRender, continue, continueWithCmd, updateAndExit)
import JBridge as JBridge
import Remote.Types (ConfigPayload)
--import Type.Data.Boolean (kind Boolean)
import Service.EC.Types.Instruments (Offer(..), StoredCard(..), StoredWallet(..))
import Payments.NetBanking.Utils (Bank(..))
import UI.Components.EditText.Controller as EditText
import UI.Components.PrimaryButton.Controller as PrimaryButton
import UI.Components.ToolBar.Controller as ToolBar
import UI.Utils (ModalAction(..), setText')
import UI.Validation as Validation
import UI.Components.Popup.Controller as Popup

type ScreenInput =
  { configPayload :: ConfigPayload
  , walletName :: String
  , number :: String
  , amount :: Number
  , orderDesc :: String
  , customerName :: String
  , mid :: String
  , allBanks :: Array Bank
  , allCards :: Array StoredCard
  , allWallets :: Array StoredWallet
  , isOffer :: Boolean
  }




data ScreenOutput = SendOTPAt String | OnBackPress | Switch String | SwitchNav String | PayUsingNB String | PayUsingWallet String String Boolean Boolean

data Action
  = StockAction
  | EditTextAction EditText.Action
  | PrimaryButtonAction PrimaryButton.Action
  | BackPressed
  | ToolBarAction ToolBar.Action
  | VerifyNumberScreenRendered String
  | ContinueCommand
  | OverlayClick ModalAction
  | OfferPopupAction Popup.Action

type State =
  { number :: String
  , entry :: Boolean
  , configPayload :: ConfigPayload
  , walletName :: String
  , buttonAnim :: Boolean
  , amount :: Number
  , isNumberValid :: Boolean
  , checkoutPhoneNumber :: String
  , orderDesc :: String
  , customerName :: String
  }

initialState :: ScreenInput -> State
initialState input =
  { number : input.number
  , entry : true
  , configPayload : input.configPayload
  , walletName : input.walletName
  , buttonAnim : false
  , amount : input.amount
  , isNumberValid : Validation.validateData Validation.MOBILE input.number
  , checkoutPhoneNumber : input.number
  , orderDesc : input.orderDesc
  , customerName : input.customerName
  }


eval :: Action -> State -> Eval Action ScreenOutput State
eval BackPressed state =
    let updatedState = state {entry = not state.entry} in
    updateAndExit updatedState $ OnBackPress

eval (OverlayClick ClickedOutside) state =
    let updatedState = state {entry = not state.entry} in
    updateAndExit updatedState $ OnBackPress

eval (EditTextAction (EditText.OnChanged text)) state =
    continue state { number = text
                   , isNumberValid = Validation.validateData Validation.MOBILE text
                   }

eval (PrimaryButtonAction (PrimaryButton.Clicked)) state =
    if state.isNumberValid
        then updateAndExit state {buttonAnim = true} $ SendOTPAt state.number
        else continue state

eval (ToolBarAction ToolBar.Clicked) state =
    let updatedState = state {entry = not state.entry} in
    updateAndExit updatedState $ OnBackPress

eval (VerifyNumberScreenRendered id) state =
    continueWithCmd state [
        do
        _ <- launchAff_ do
            _ <- delay (Milliseconds 300.0)
            liftEffect $ JBridge.requestKeyboardShow id
            liftEffect $ setText' id state.number
        pure $ ContinueCommand
    ]

eval _ state = continue state

overrides :: String -> (Action -> Effect Unit) -> State -> Props (Effect Unit)
overrides "MainLayout" push state =
    [ afterRender push $ const (VerifyNumberScreenRendered (JBridge.getNewIDWithTag JBridge.MOBILE_NUM)) ]
overrides _ push state = []
