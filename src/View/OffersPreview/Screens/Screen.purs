module View.OffersPreview.Screens.Screen where

import HyperPrelude.External(Maybe(..), fromMaybe, isJust,head,Effect,(<>),($),Unit,(<<<),const,not,map,unit)
import HyperPrelude.Internal(Screen,PrestoDOM,Visibility(..),Length(..), Margin(..), Orientation(..),Padding(..),height,width,linearLayout,orientation,background,padding,textView,text,textSize,color,fontStyle,scrollView,scrollBarY)
import View.OffersPreview.Controllers.Controller
import Service.EC.Types.Instruments as Instruments
import Data.String as Str
import Engineering.Helpers.Commons (getNbIin)
import Engineering.Helpers.Events (addCustomBackPress)
import PrestoDOM.Animation (animationSet)
import PrestoDOM.Types.DomAttributes (Corners(..))
import PPConfig.Utils as PPUtils
import Remote.Types (ConfigPayload(..), Toolbar(..), Button(..))
import UI.Components.PaymentOptionsConfig as PaymentOptionsConfig
import UI.Components.PaymentOptionsView as PaymentOptionsView
import UI.Components.Popup.Config as PopupConfig
import UI.Components.Popup.View as PopupView
import UI.Components.PrimaryButton.Config as PrimaryButtonConfig
import UI.Config as UIConfig
import UI.Utils (screenHeight, translateInYAnim, translateOutYAnim)
import View.Stock.Container.Commons as UICommons
import View.PaymentPage.Screens.Utils as PPUtils

screen :: ScreenInput -> Screen Action State ScreenOutput
screen input =
  { initialState: (initialState input)
  , view
  , name: "OffersScreen"
  , globalEvents: []
  , eval: eval
  }

view :: ∀ w. (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
view push state =
  UICommons.getParentLayout
    parentInput
    (UICommons.OffersAction push)
    (offersView push state)
    (Just $ showPopup push state)
  where
  parentInput =
    { modalView: false
    , modalHeight: WRAP_CONTENT
    , modalAnimationTrigger: false
    , toolbarHeader: ""
    , configPayload: state.configPayload
    , useContainerPadding: true
    , useRelativeLayout: false
    , amount: 1.0 -- amount will not be visible in this use case and is not being passed here
    , showToolbar: true
    , phoneNumber: ""
    , showAmountBar: true
    , orderDescription: ""
    , customerName : ""
    }

offersView :: ∀ w. (Action -> Effect Unit) -> State -> Array (PrestoDOM (Effect Unit) w)
offersView push state =
  [ linearLayout
      [ width MATCH_PARENT
      , height MATCH_PARENT
      , orientation VERTICAL
      , addCustomBackPress push $ const OnBackPress
      ]
      $ [ linearLayout
            [ height WRAP_CONTENT
            , width MATCH_PARENT
            , orientation VERTICAL
            , background toolConfig.color
            , padding (Padding 12 12 12 12)
            ]
            [ textView
                [ width MATCH_PARENT
                , height WRAP_CONTENT
                , text $ PPUtils.offerHeader state.configPayload
                , textSize $ PPUtils.offerHeaderTextSize state.configPayload
                , color toolConfig.textColor
                , fontStyle $ PPUtils.primaryColor state.configPayload
                ]
            ]
        , divider
        ]
      <> [ scrollView
            [ width MATCH_PARENT
            , height MATCH_PARENT
            , scrollBarY true
            ]
            [ linearLayout
                [ width MATCH_PARENT
                , height WRAP_CONTENT
                , orientation VERTICAL
                ]
                (map (offerPane push state) state.offers)
            ]
        ]
  ]
  where
  (ConfigPayload config) = state.configPayload
  (Toolbar toolConfig) = config.toolbar

divider :: ∀ w. PrestoDOM (Effect Unit) w
divider =
  linearLayout
    [ height (V 1)
    , width MATCH_PARENT
    , background "#1f000000"
    ]
    []

offerPane :: ∀ w. (Action -> Effect Unit) -> State -> Instruments.Offer -> PrestoDOM (Effect Unit) w
offerPane push state offer@(Instruments.Offer offerData) =
  PaymentOptionsView.view
    (push <<< MoreInfo offer)
    (PaymentOptionsConfig.Config config{
      logoVisibility = GONE
    , primaryText = fromMaybe "" desc.offerDisplay1
    , primaryTextColor = "#242424"
    , primaryTextFont = PPUtils.fontBold state.configPayload
    , primaryTextSize = 18
    , primaryTextMargin = (Margin 0 5 0 0)
    , secondaryText = fromMaybe "" desc.offerDisplay2
    , secondaryTextColor = "#707070"
    , secondaryTextFont = PPUtils.fontRegular state.configPayload
    , secondaryTextSize = 16
    , secondaryTextMargin = (Margin 0 5 0 0)
    , tertiaryText = "More Details"
    , tertiaryTextColor = "#1665C0"
    , tertiaryTextFont = PPUtils.fontSemiBold state.configPayload
    , tertiaryTextSize = 16
    , radioButtonIconUrl = "ic_bank_508500" -- [TODO] Derive it from offer
    , radioButtonSize = V 20
    , tertiaryVisibility = VISIBLE
    , inputAreaVisibility = GONE
    , cvvId = offerData.voucherCode <> "_id" -- patch added due to repeating id  "default"
    , displayAreaHeight = WRAP_CONTENT
    , logoPadding = Padding 0 0 0 0
    -- , secondaryLayoutMargin = Margin 0 5 0 8 :: Todo :: breaking for now, will be fixed later
    , displayAreaMargin = Margin 0 0 0 12
    , radioButtonMargin = Margin 20 20 0 0
    })
    false
    Nothing
  where
  (PaymentOptionsConfig.Config config) = PaymentOptionsConfig.defConfig state.configPayload
  offerDesc@(Instruments.OfferDescription desc) = fromMaybe PPUtils.defaultOfferDesc offerData.offerDescription
  paymentMethod = getPaymentMethod offer
  paymentMethodType = getPaymentMethodType offer


getPaymentMethod :: Instruments.Offer -> String
getPaymentMethod ofr@(Instruments.Offer offer) = fromMaybe "" $ head $ fromMaybe [] offer.paymentMethod

getPaymentMethodType :: Instruments.Offer -> String
getPaymentMethodType ofr@(Instruments.Offer offer) = fromMaybe "" offer.paymentMethodType

showPopup :: ∀ w. (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
showPopup push state =
  PopupView.view
    (push <<< Popup)
    (PopupConfig.Config updatedConfig)
    (Just $ popUpHeader push state offer)
    (Just $ animationSet
      [ translateInYAnim state.configPayload (screenHeight unit) (isJust state.showOffer)
      , translateOutYAnim state.configPayload (screenHeight unit) (not $ isJust state.showOffer)
      ])
  where
  PrimaryButtonConfig.Config b = UIConfig.primaryButtonConfig state.configPayload
  (ConfigPayload configPayload) = state.configPayload
  (Button buttonConfig) = configPayload.button
  bConf = b {text = "Close"}
  PopupConfig.Config config = PopupConfig.defConfig
  offer@(Instruments.Offer offerData) = fromMaybe PPUtils.getDummyOffer state.showOffer
  offerDesc@(Instruments.OfferDescription desc) = fromMaybe PPUtils.defaultOfferDesc offerData.offerDescription
  updatedConfig = config { open = isJust state.showOffer
                        , close = (not $ isJust state.showOffer)
                        , listStrings = Str.split (Str.Pattern "\n") $ fromMaybe ""  desc.offerDescription
                        , headerVisibility = GONE
                        , bulletIconUrl = "ic_si_tick"
                        , dividerVisibility = GONE
                        , bulletSize = V 14
                        , corners = (Corners 8.0 true true false false)
                        , addedContentVisibility = VISIBLE
                        , contentMargin = (Margin 16 10 16 10)
                        , listItemMargin = MarginBottom 15
                        , bulletMargin = (Margin 0 4 8 0)
                        , buttonTwoVisibility = GONE
                        , termsVisibility = GONE
                        , checkBoxVisibility = GONE
                        , buttonOneBackground = PPUtils.btnColor state.configPayload
                        , buttonOneText = "Close"
                        , buttonOneTranslation = PPUtils.btnTranslation state.configPayload
                        , buttonOneMargin = Margin 5 5 5 5
                        , buttonOneStroke = "1," <> PPUtils.btnColor state.configPayload
                        , buttonOneCornerRadius = PPUtils.btnCornerRadius state.configPayload
                        , buttonOneTextFont = PPUtils.fontBold state.configPayload
                        , buttonOneTextSize = PPUtils.fontSizeLarge state.configPayload
                        , buttonOneTextColor = PPUtils.btnBackground state.configPayload
      }

popUpHeader :: ∀ w. (Action -> Effect Unit) -> State -> Instruments.Offer -> PrestoDOM (Effect Unit) w
popUpHeader push state offer@(Instruments.Offer offerData) =
  PaymentOptionsView.view
    (push <<< MoreInfo offer)
    (PaymentOptionsConfig.Config config{
      logoVisibility = GONE
    , primaryText = fromMaybe "" desc.offerDisplay1
    , primaryTextColor = "#242424"
    , primaryTextFont = PPUtils.fontBold state.configPayload
    , primaryTextSize = 24
    , primaryTextMargin = (Margin 0 10 0 0)
    , secondaryText = fromMaybe "" desc.offerDisplay2
    , secondaryTextColor = "#707070"
    , secondaryTextFont = PPUtils.fontRegular state.configPayload
    , secondaryTextSize = 16
    , secondaryTextMargin = (Margin 0 15 0 0)
    , radioButtonIconUrl = "ic_bank_508500" -- [TODO] Derive it from offer
    , radioButtonSize = V 34
    , tertiaryVisibility = GONE
    , inputAreaVisibility = GONE
    , displayAreaHeight = WRAP_CONTENT
    , logoPadding = Padding 0 0 0 0
    -- , secondaryLayoutMargin = Margin 0 5 0 8
    , displayAreaMargin = Margin 0 0 0 12
    , radioButtonMargin = Margin 20 20 0 0
    , lineSeparatorVisibility = GONE
    })
    false
    Nothing
  where
  (PaymentOptionsConfig.Config config) = PaymentOptionsConfig.defConfig state.configPayload
  offerDesc@(Instruments.OfferDescription desc) = fromMaybe PPUtils.defaultOfferDesc offerData.offerDescription
  paymentMethod = getPaymentMethod offer
  paymentMethodType = getPaymentMethodType offer

getPaymentMethodIcon :: String -> String -> String
getPaymentMethodIcon paymentMethodType paymentMethod =
  case paymentMethodType of
    "CARD"   -> ""
    "NB"     -> "ic_bank_" <> (getNbIin paymentMethod)
    "WALLET" -> "ic_" <> (Str.toLower paymentMethod)
    _ -> ""
