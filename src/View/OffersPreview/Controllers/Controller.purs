module View.OffersPreview.Controllers.Controller where


import HyperPrelude.External(Maybe(..),isJust)
import HyperPrelude.Internal(Eval,continue,updateAndExit,exit)
import Remote.Types (ConfigPayload(..))
import Service.EC.Types.Instruments as Instruments
import UI.Components.ToolBar.Controller as ToolBar
import UI.Utils (ModalAction(..))
import UI.Components.Popup.Controller as Popup
import UI.Components.PaymentOptionsController as PayOptController
import UI.Components.PrimaryButton.Controller as PrimaryButton

data Action
  = OnBackPress
  | ToolBarAction ToolBar.Action
  | OverlayClick ModalAction
  | MoreInfo Instruments.Offer PayOptController.Action
  | Popup Popup.Action
  | OfferPopupAction Popup.Action

type ScreenInput
  = { configPayload :: ConfigPayload
    , offers :: Array Instruments.Offer }

type State
  = { configPayload :: ConfigPayload
    , offers :: Array Instruments.Offer
    , showOffer :: Maybe Instruments.Offer }

data ScreenOutput
  = BackPressed

initialState :: ScreenInput -> State
initialState input =
  { configPayload: input.configPayload
  , offers: input.offers
  , showOffer : Nothing
  }

eval :: Action -> State -> Eval Action ScreenOutput State
eval OnBackPress state =
  if isJust state.showOffer
    then continue state {showOffer = Nothing}
  else updateAndExit state BackPressed

eval (ToolBarAction ToolBar.Clicked) state = exit BackPressed

eval (MoreInfo offers (PayOptController.PaymentListAction PayOptController.ButtonClicked)) state =
  continue state { showOffer = Just offers }

eval (OverlayClick ClickedOutside) state =
  continue state {showOffer = Nothing}

eval (Popup Popup.ButtonOneClick) state =
  continue state {showOffer = Nothing}

eval (Popup Popup.OverlayClick) state = continue state {showOffer = Nothing}
eval _ state = continue state

dummyOffer :: Instruments.Offer
dummyOffer = Instruments.Offer
    { voucherCode : "101"
          , visibleToCustomer : Just true
          , paymentMethodType : Just "WALLET"
          , paymentMethod : Just ["PHONEPE", "MOBIKWIK"]
          , paymentChannel : Nothing
  		    , paymentMethodFilter : Nothing
          , applicationMode : Nothing
          , calculationMode : Nothing
          , discountValue : Nothing
          , id :Nothing
          , offerToken : Nothing
          , offerDescription : Just (Instruments.OfferDescription
              { offerDisplay1: Just "Get 20% Supercash Using Mobikwik"
              , offerDisplay2: Just"Get 20% Supercash upto ₹ 60. No Code Required"
              , offerDescription: Just "Valid for more than ₹500\nOther T&C’s may apply\nOffer valid till 30 September 2018"
              , offerDisplay3: Nothing
              -- , imageUrl: "bank_hdfc"
              })
    }

dummyData :: Array Instruments.Offer
dummyData =
  [ Instruments.Offer
    { voucherCode : "Card Bin offer"
          , visibleToCustomer : Just true
          , paymentMethodType : Just "CARD"
          , paymentMethod : Just ["AMEX", "DINERS", "DISCOVER", "MAESTRO", "MASTERCARD","RUPAY", "VISA"]
          , paymentChannel : Nothing
  		    , paymentMethodFilter : Nothing
          , applicationMode : Nothing
          , calculationMode : Nothing
          , discountValue : Nothing
          , id :Nothing
          , offerToken : Nothing
          , offerDescription : Just (Instruments.OfferDescription
              { offerDisplay1: Just "Get 20% Supercash Using Mobikwik"
              , offerDisplay2: Just"Get 20% Supercash upto ₹ 60. No Code Required"
              , offerDescription: Just "Valid for more than ₹500\nOther T&C’s may apply\nOffer valid till 30 September 2018"
              , offerDisplay3: Just "Get 20% Supercash Using Mobikwik"
              -- , imageUrl: "bank_hdfc"
              })
    }
  , Instruments.Offer
    { voucherCode : "NB offer"
          , visibleToCustomer : Just true
          , paymentMethodType : Just "NB"
          , paymentMethod : Just ["NB_AXIS"]
          , paymentChannel : Nothing
  		    , paymentMethodFilter : Nothing
          , applicationMode : Nothing
          , calculationMode : Nothing
          , discountValue : Nothing
          , id :Nothing
          , offerToken : Nothing
          , offerDescription : Just (Instruments.OfferDescription
            { offerDisplay1: Just "Get 10X RewardPoints Using Axis cards"
            , offerDisplay2: Just"Get 10X reward points on your order using Axis card. Business or Corporate card."
            , offerDescription: Just "Valid for more than ₹500\nOther T&C’s may apply\nOffer valid till 30 September 2018"
            , offerDisplay3: Nothing
            -- , imageUrl: "bank_hdfc"
            })
      }
  , Instruments.Offer
    { voucherCode : "Card Bin offer"
          , visibleToCustomer : Just true
          , paymentMethodType : Just "CARD"
          , paymentMethod : Just ["MAESTRO", "AMEX", "DINERS", "DISCOVER", "MASTERCARD","RUPAY", "VISA"]
          , paymentChannel : Nothing
  		    , paymentMethodFilter : Nothing
          , applicationMode : Nothing
          , calculationMode : Nothing
          , discountValue : Nothing
          , id :Nothing
          , offerToken : Nothing
          , offerDescription : Just (Instruments.OfferDescription
              { offerDisplay1: Just "Get 20% Supercash Using Maestro"
              , offerDisplay2: Just"Get 20% Supercash upto ₹ 60. No Code Required"
              , offerDescription: Just "Valid for more than ₹500\nOther T&C’s may apply\nOffer valid till 30 September 2018"
              , offerDisplay3: Just "Get 20% Supercash Using Maestro"
              -- , imageUrl: "bank_hdfc"
              })
    }
  , Instruments.Offer
    { voucherCode : "Wallet offer"
          , visibleToCustomer : Just true
          , paymentMethodType : Just "CARD"
          , paymentMethod : Just ["MOBIKWIK", "PHONEPE"]
          , paymentChannel : Nothing
  		    , paymentMethodFilter : Nothing
          , applicationMode : Nothing
          , calculationMode : Nothing
          , discountValue : Nothing
          , id :Nothing
          , offerToken : Nothing
          , offerDescription : Just (Instruments.OfferDescription
              { offerDisplay1: Just "Get 20% Supercash Using Mobikwik"
              , offerDescription: Just "Valid for more than ₹500\nOther T&C’s may apply\nOffer valid till 30 September 2018"
              , offerDisplay2: Just"Get 20% Supercash upto ₹ 60. No Code Required"
              , offerDisplay3: Nothing
              -- , imageUrl: "bank_axis"
              })
      }
  , Instruments.Offer
    { voucherCode : "Card offer"
          , visibleToCustomer : Just true
          , paymentMethodType : Just "CARD"
          , paymentMethod : Just ["HDFC Bank", "AMEX", "SBI"]
          , paymentChannel : Nothing
  		    , paymentMethodFilter : Nothing
          , applicationMode : Nothing
          , calculationMode : Nothing
          , discountValue : Nothing
          , id :Nothing
          , offerToken : Nothing
          , offerDescription : Just (Instruments.OfferDescription
            { offerDisplay1: Just "Get 10X RewardPoints Using HDFC cards"
            , offerDisplay2: Just"Get 10X reward points on your order using HDFC card. Business or Corporate card."
            , offerDescription: Just "Valid for more than ₹500\nOther T&C’s may apply\nOffer valid till 30 September 2018"
            , offerDisplay3: Nothing
            })
      }
  -- , Instruments.Offer
  --   { voucherCode : "101"
  --         , visibleToCustomer : Just true
  --         , paymentMethodType : Just "WALLET"
  --         , paymentMethod : Just ["PHONEPE", "MOBIKWIK"]
  --         , offerDescription : Just (Instruments.OfferDescription
  --           { offerDisplay1: Just "Get 10X RewardPoints Using HDFC cards"
  --           , offerDisplay2: Just"Get 10X reward points on your order using HDFC card. Business or Corporate card."
  --           , offerDescription: Just "Valid for more than ₹500\nOther T&C’s may apply\nOffer valid till 30 September 2018"
  --           , offerDisplay3: Nothing
  --           -- , imageUrl: "bank_pnb"
  --           })
  --     }
  ]
