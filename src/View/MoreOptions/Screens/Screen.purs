module View.MoreOptions.Screens.Screen where

import HyperPrelude.External(fromMaybe, Maybe(..),Effect,Unit,($),(<<<),(==),unit,negate,(<>),const)
import HyperPrelude.Internal(InputType(..), Length(..), Margin(..), Orientation(..), PrestoDOM, Screen, Visibility(..), background, height, linearLayout, margin, orientation, scrollView, visibility, weight, width)
import Engineering.Helpers.Events (addCustomBackPress)
import JBridge as JBridge
import Remote.Types (ConfigPayload(..))
import PrestoDOM.Animation as PrestoAnim
import UI.Components.EditText.Config as EditTextConfig
import UI.Components.EditText.View as EditTextView
import UI.Components.PrimaryButton.View as PrimaryButton
import UI.Components.PrimaryButton.Config as PrimaryButtonConfig
import UI.Components.ToolBar.View as ToolBar
import UI.Config as UIConfig
import UI.Utils (screenWidth, translateInXAnim, translateOutXAnim)
import View.MoreOptions.Controllers.Controller (Action(..), ScreenInput, ScreenOutput, State, eval, initialState, overrides)
import UI.Constant.Str.Default as STR

screen :: ScreenInput -> Screen Action State ScreenOutput
screen input =
  { initialState: (initialState input)
  , name: "MoreOptionScreen"
  , view
  , globalEvents: []
  , eval: eval
  }

view :: ∀ w. (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
view push state =
  PrestoAnim.entryAnimationSetForward [ translateInXAnim state.configPayload (screenWidth unit) true ]
    $ PrestoAnim.exitAnimationSetForward [ translateOutXAnim state.configPayload (-50) true ]
    $ PrestoAnim.entryAnimationSetBackward [ translateInXAnim (state.configPayload) (-(screenWidth unit)) true ]
    $ PrestoAnim.exitAnimationSetBackward [ translateOutXAnim (state.configPayload) (screenWidth unit) true ]
    $ linearLayout
        ( [ width MATCH_PARENT
          , height MATCH_PARENT
          , background "#ffffff"
          , orientation VERTICAL
          , addCustomBackPress push (const BackPress)
          ]
            <> overrides "MainLayout" push state
        )
        [ ToolBar.view (push <<< ToolBarAction) (UIConfig.genericToolBarConfig state.back state.title state.configPayload)
        , contentView push state
        , linearLayout [ width MATCH_PARENT, height $ V 0, weight 1.0 ] []
        , footerView push state
        ]

contentView :: ∀ w. (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
contentView push state =
  scrollView
    [ height $ V 0
    , weight 1.0
    , width MATCH_PARENT
    --, margin (MarginTop 4)
    ]
    [ linearLayout
        [ width MATCH_PARENT
        , height $ V 100
        , background "#ffffff"
        , orientation VERTICAL
        --, margin (Margin 16 20 16 0)
        ]
        [ getElement push state.content state.configPayload ]
    ]

footerView :: ∀ w. (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
footerView push state =
  linearLayout
    [ width MATCH_PARENT
    , height $ V 50
    , background "#ffffff"
    , orientation VERTICAL
    , margin (Margin 16 0 16 20)
    , visibility if state.textValue == "" then GONE else VISIBLE
    ]
    [ getElement push state.footer state.configPayload ]

getElement :: ∀ w. (Action -> Effect Unit) -> String -> ConfigPayload -> PrestoDOM (Effect Unit) w
getElement push elementType (ConfigPayload configPayload) = case elementType of
  "editText" ->
    EditTextView.view
      (push <<< EditTextAction)
      (EditTextConfig.Config newConfig)
  "button" ->
    PrimaryButton.view
      (push <<< PrimaryButtonAction)
      (PrimaryButtonConfig.Config bConf)
  _ -> linearLayout [] []
  where
  EditTextConfig.Config pconf = UIConfig.editTextConfig (ConfigPayload configPayload)

  newConfig =
    pconf
      { hint = STR.getWalletHint $ fromMaybe "" configPayload.language
      , inputType = TypeText
      , iconHeight = V 25
      , cardHeight = V 30
      , iconText = "PAY"
      , iconTextSize = 14
      , editTextId = JBridge.getNewIDWithTag JBridge.MORE
      }

  PrimaryButtonConfig.Config b = UIConfig.primaryButtonConfig (ConfigPayload configPayload)

  bConf =
    b
      { text = STR.getProceed $ fromMaybe "" configPayload.language
      , margin = (Margin 0 0 0 20)
      }
