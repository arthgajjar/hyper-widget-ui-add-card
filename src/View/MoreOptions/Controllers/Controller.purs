module View.MoreOptions.Controllers.Controller where


import HyperPrelude.External(toUpper, toLower,Effect,launchAff_,delay,Milliseconds(..),liftEffect,Unit,not,($),const,bind,pure)
import HyperPrelude.Internal (Eval, Props, continue, exit, afterRender, continueWithCmd, updateAndExit)
import Engineering.Helpers.Commons (log)
import JBridge as JBridge
import Remote.Types (MoreOption(..), ConfigPayload, Toolbar(..))
import PPConfig.Utils (getMoreOptions)
import UI.Components.EditText.Controller as EditText
import UI.Components.PrimaryButton.Controller as PrimaryButton
import UI.Components.ToolBar.Controller as ToolBar
import Service.EC.Types.Instruments (Offer(..), StoredCard(..), StoredWallet(..))
import Payments.NetBanking.Utils (Bank(..))

type ScreenInput =
        { configPayload :: ConfigPayload,
         amount :: Number,
         mid :: String,
         allBanks :: Array Bank
        , allCards :: Array StoredCard
        , allWallets :: Array StoredWallet
        }

type State = {
        back :: String
    ,   title :: String
    ,   content :: String
    ,   footer :: String
    ,   textValue :: String
    ,   entry :: Boolean
    ,   action :: String
    ,   configPayload :: ConfigPayload
    }

data ScreenOutput = OnBackPressed | NextFlow NextFlowType | Switch String | Submit String | SwitchNav String | PayUsingNB String | PayUsingWallet String String Boolean Boolean

data NextFlowType = LinkWallet String | PayWallet String

data Action = NoAction
        | ToolBarAction ToolBar.Action
        | EditTextAction EditText.Action
        | PrimaryButtonAction PrimaryButton.Action
        | BackPress
        | ContinueCommand
        | MoreOptionsScreenRendered String

initialState :: ScreenInput -> State
initialState input =
    let (MoreOption moreOption) = getMoreOptions input.configPayload in
    {   back : (extractData moreOption.view.toolbar Back)
    ,   title   : (extractData moreOption.view.toolbar Title)
    ,   content : moreOption.view.content
    ,   footer : moreOption.view.footer
    ,   textValue : ""
    ,   entry : true
    ,   action : moreOption.view.action
    ,   configPayload : input.configPayload
    }


eval :: Action -> State -> Eval Action ScreenOutput State

eval BackPress state =
    let updatedState = state {entry = not state.entry} in
    updateAndExit updatedState $ OnBackPressed

eval (ToolBarAction ToolBar.Clicked) state =
    let updatedState = state {entry = not state.entry} in
    updateAndExit updatedState $ OnBackPressed

eval (EditTextAction (EditText.OnChanged str)) state = continue state { textValue = str}

eval (PrimaryButtonAction (PrimaryButton.Clicked)) state = do
    case (toLower state.action) of
        "paywithwallet" -> exit $ NextFlow $ PayWallet (toUpper state.textValue)
        "linkwallet" -> exit $ NextFlow $ LinkWallet (toUpper state.textValue)
        _ -> continue state

eval (MoreOptionsScreenRendered id) state =
    continueWithCmd state [
        do
        _ <- launchAff_ do
            _ <- delay (Milliseconds 100.0)
            liftEffect $ JBridge.requestKeyboardShow id
        pure $ ContinueCommand
    ]

eval _ state = continue state

data ExtType = Back | Title

extractData :: Toolbar -> ExtType -> String
extractData (Toolbar tlbr) extType =
    case extType of
        Back -> tlbr.back
        Title -> tlbr.pageTitle

overrides :: String -> (Action -> Effect Unit) -> State -> Props (Effect Unit)
overrides "MainLayout" push state = [ afterRender push $ const (MoreOptionsScreenRendered (JBridge.getNewIDWithTag JBridge.MORE))]
overrides _ push state = []
