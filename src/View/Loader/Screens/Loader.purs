module View.Loader.Screens.Screen where


import HyperPrelude.External(Effect,Unit, ($), (<>))
import HyperPrelude.Internal
import PrestoDOM.Properties (background)
import UI.Utils (bringToFront)
import View.Loader.Controllers.Controller (Action, ScreenOutput, State, eval, initialState, overrides)

screen :: State -> Screen Action State ScreenOutput
screen input =
    { initialState : initialState input
    , view
    , name : "LoadingScreen"
    , globalEvents : []
    , eval
    }

view :: ∀ w. (Action  -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
view push state =
    linearLayout_ (Namespace "LoadingScreen")
        ([ height MATCH_PARENT
        , width MATCH_PARENT
        , orientation HORIZONTAL
        , gravity CENTER
        , translationZ 200.0
        , bringToFront true
        , background "#9e9e9e"
        , clickable true
        ] <> overrides "MainContent" push state )
        if state.customLoader then
            [
                relativeLayout
                ([ height MATCH_PARENT
                , width MATCH_PARENT
                , id state.parentId
                , gravity CENTER
                ])
                -- if os == "IOS" then
                -- [
                --     progressBar
                --     ([ height MATCH_PARENT
                --     , width MATCH_PARENT
                --     ] <> overrides "ProgressBar" push state )
                -- ]
                -- else
                []
            ]
        else
            [
                linearLayout
                ([ height (V 150)
                , width (V 304)
                , orientation VERTICAL
                , gravity CENTER_HORIZONTAL
                , padding (Padding 14 41 14 0)
                , background "#FFFFFFFF"
                , cornerRadius 6.0
                , margin (Margin 0 0 0 0)
                ] <> (overrides "Dialog" push state))
                [ progressBar
                    ([ height $ V 34
                    , width $ V 34
                    , margin (Margin 121 0 121 0)
                    ] <> overrides "ProgressBar" push state )
                , textView
                    ([ height $ V 17
                    , width MATCH_PARENT
                    , margin (Margin 0 5 0 0)
                    , textSize 14
                    , color "#FF898989"
                    , lineHeight "15px"
                    , gravity CENTER
                    , text "Processing"
                    -- , fontStyle rOBOTOMEDIUM
                    ] <> overrides "Progress Text" push state )
                ]
            ]
