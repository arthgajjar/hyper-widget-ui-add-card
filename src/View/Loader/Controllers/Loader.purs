module View.Loader.Controllers.Controller where

import HyperPrelude.External(Effect, Unit, const, pure, unit, ($), (*>))
import HyperPrelude.Internal (afterRender,Eval, Props,continue, exit)
import UI.Utils (loaderAfterRender)

type ScreenInput = State

type ScreenOutput = Unit

data Action = BackPressed | AfterRender

type State = {
  customLoader :: Boolean
, parentId :: String
}


initialState :: State -> State
initialState input = {
  customLoader: input.customLoader
, parentId: input.parentId
}


eval :: Action -> State -> Eval Action ScreenOutput State
eval action state =
  case action of
    AfterRender -> (pure $ loaderAfterRender state.parentId) *> exit unit
    _ -> continue state

overrides :: String -> (Action -> Effect Unit) -> State -> Props (Effect Unit)
overrides "MainContent" push state = [
  afterRender push (const AfterRender)
]
overrides _ push state = []
