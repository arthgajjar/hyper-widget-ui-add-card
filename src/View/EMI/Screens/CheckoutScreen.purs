module View.EMI.Screens.CheckoutScreen where

import HyperPrelude.External(Maybe(..),Effect,Unit,(#),($),(<<<),(<>),const)
import HyperPrelude.Internal (Length(..), Orientation(..), Padding(..),Margin(..), PrestoDOM, Screen, background, height, linearLayout, orientation, scrollBarY, scrollView, width, margin)
import View.EMI.Controllers.CheckoutController (Action(..), ScreenInput, ScreenOutput, State, eval, initialState, overrides)
import Data.Newtype (unwrap)
import Engineering.Helpers.Events (addCustomBackPress)
import UI.Config as UIConfig
import View.Stock.Container.Commons as UICommons
import View.EMI.Screens.Utils as EMIUtils
import UI.Components.AddCard.View as AddCard
import UI.Components.AddCard.Config as AddCardConfig
import Service.EC.Types.Instruments (EMIData(..))
import PPConfig.Utils as CPUtils
import UI.Utils as UIUtils

screen ::  ScreenInput -> Screen Action State ScreenOutput
screen input =
  { initialState : (initialState input)
  , name : "CheckoutScreen"
  , view
  , globalEvents : []
  , eval : eval
  }

view :: ∀ w. (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
view push state =
  UICommons.getParentLayout
    parentInput
    (UICommons.EMICheckoutAction push)
    (emiCheckoutView push state)
    (Just $ EMIUtils.popupView (EMIUtils.EMICheckoutAction push) state.configPayload popupData state.showDetails)

  where
  parentInput =
    { modalView : false
    , modalHeight : WRAP_CONTENT
    , modalAnimationTrigger : state.entry
    , toolbarHeader : "Credit Card EMI"
    , configPayload : state.configPayload
    , useContainerPadding : true
    , useRelativeLayout : false
    , amount : state.amount
    , showToolbar : true
    , phoneNumber : "" -- TODO :: add phone number to state.
    , showAmountBar : true
    , orderDescription : "" -- TODO :: add order desc here
    , customerName : "" -- TODO :: add customer name here
    }
  (EMIData plan) = state.emiPlan
  popupData = { amount : state.amount, tenure : plan.tenure, emiAmount: plan.amount }

emiCheckoutView :: ∀ w. (Action -> Effect Unit) -> State -> Array (PrestoDOM (Effect Unit) w)
emiCheckoutView push state =
  [ scrollView (
      [   width MATCH_PARENT
      ,   height MATCH_PARENT
      ,   scrollBarY false
      ,   addCustomBackPress push $ const OnBackPress
      ] <> overrides "MainLayout" push state)
      [ linearLayout
          [ width MATCH_PARENT
          , height WRAP_CONTENT
          , orientation VERTICAL
          , UIUtils.contentMargin state.configPayload
          ](
          [ linearLayout [width MATCH_PARENT, height WRAP_CONTENT, margin (MarginBottom 12)](
             UICommons.addCurvedWrapper state.configPayload $ [ EMIUtils.getItemCard
                { instrument : bank , secondaryText : secondaryText, radioButton : false}
                (EMIUtils.EMICheckoutAction push)
                false
                state.configPayload
            ])
          ] <> addCardView <>
           (UICommons.addCurvedWrapper state.configPayload $ [ EMIUtils.amountGadget (EMIUtils.EMICheckoutAction push) state.amount state.subventionAmount (Just state.emiPlan) state.configPayload false ])
          )
      ]
  ]
  where
  bank = (state.emiPlan # unwrap # _.bank)
  secondaryText = EMIUtils.planText state.emiPlan state.subventionAmount
  wrapper = linearLayout [width MATCH_PARENT, height WRAP_CONTENT, UIUtils.sectionMargin state.configPayload]
  ifModal = CPUtils.ifModalView (state.configPayload)
  AddCardConfig.Config addCardConfig = UIConfig.addCardConfig state.configPayload
  updatedAddCardConfig =
    addCardConfig
      { buttonAtBottom = false
      , cardMargin = (Margin 0 0 0 0)
      }

  addCardView =
    UICommons.addCurvedWrapper state.configPayload $
    [ linearLayout
        [ height MATCH_PARENT
        , width MATCH_PARENT
        , background "#ffffff"
        --, UIUtils.sectionMargin state.configPayload
        , margin (MarginBottom 20)
        ]
        [ AddCard.view (AddCardConfig.Config updatedAddCardConfig) (push <<< AddCardAction) state.addCardState ]
    ]
    where
    bank = (state.emiPlan # unwrap # _.bank)
    secondaryText = EMIUtils.planText state.emiPlan state.subventionAmount
    wrapper = linearLayout [width MATCH_PARENT, height WRAP_CONTENT, UIUtils.sectionMargin state.configPayload]
    ifModal = CPUtils.ifModalView (state.configPayload)
    AddCardConfig.Config addCardConfig = UIConfig.addCardConfig state.configPayload
    updatedAddCardConfig = addCardConfig { buttonAtBottom = false }
    addCardView =
      [ linearLayout
          [ height WRAP_CONTENT
          , width MATCH_PARENT
          , background "#ffffff"
          , UIUtils.sectionMargin state.configPayload
          ]
          [ AddCard.view (AddCardConfig.Config updatedAddCardConfig) (push <<< AddCardAction) state.addCardState ]
      ]
