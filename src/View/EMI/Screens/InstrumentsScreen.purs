module View.EMI.Screens.InstrumentsScreen where

import HyperPrelude.External(filter, foldl, null, partition, (!!),fromMaybe, Maybe(..),Effect,Unit, const, map, ($), (&&), (<>), (==), (||))
import HyperPrelude.Internal(Gravity(..), Length(..), Orientation(..), PrestoDOM, Screen, Visibility(..), background, color, fontStyle, gravity, height, linearLayout, orientation, scrollBarY, scrollView, text, textSize, textView, visibility, width)
import Data.String as String
import Engineering.Helpers.Events (addCustomBackPress)
import Service.EC.Types.Instruments (EMIData(..), StoredCard(..))
import Foreign.Object as H
import PPConfig.Utils as CPUtils
import UI.Utils as UIUtils
import View.EMI.Controllers.InstrumentsController (Action(..), ScreenInput, ScreenOutput, State, eval, initialState)
import View.Stock.Container.Commons as UICommons
import View.EMI.Screens.Utils as EMIUtils
import View.PaymentPage.Screens.Utils as PPUtils

screen ::  ScreenInput -> Screen Action State ScreenOutput
screen input =
  { initialState : (initialState input)
  , name : "InstrumentSelectionScreen"
  , view
  , globalEvents : []
  , eval : eval
  }

view :: ∀ w. (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
view push state =
	UICommons.getParentLayout
		parentInput
		(UICommons.EMIInstrumentsAction push)
    (instrumentSelectionView push state)
    Nothing
	where
    parentInput =
        { modalView : false
        , modalHeight : WRAP_CONTENT
        , modalAnimationTrigger : state.entry
        , toolbarHeader : "Credit Card EMI"
        , configPayload : state.configPayload
        , useContainerPadding : true
        , useRelativeLayout : false
        , amount : state.amount
        , showToolbar : true
        , phoneNumber : state.phoneNumber
        , showAmountBar : true
        , orderDescription : "" -- TODO :: add order Description here
        , customerName : "" -- TODO :: add customer name here
        }

instrumentSelectionView :: ∀ w. (Action -> Effect Unit) -> State -> Array (PrestoDOM (Effect Unit) w)
instrumentSelectionView push state =
	[	scrollView
		[ width MATCH_PARENT
		, height WRAP_CONTENT
		, addCustomBackPress push (const OnBackPress)
    , scrollBarY false
		]
		[	linearLayout
			[  width MATCH_PARENT
			,  height WRAP_CONTENT
			,  orientation VERTICAL
      ,  UIUtils.contentMargin state.configPayload
			]
			if null noCostInstruments && null otherInstruments
				then UICommons.addCurvedWrapper state.configPayload [ noPlansView push state ]
				else [ noCostEmiView push state noCostInstruments noCostCards
            , costEmiView push state otherInstruments otherCards
            ]
		]
	]
	where
		decoupledArray = partition (\item -> item.noCost) $ H.toArrayWithKey flattenValues state.emiPlans
		noCostInstruments = decoupledArray.yes
		otherInstruments = decoupledArray.no

		noCostCards = getFilteredCards state.storedCards noCostInstruments
		otherCards = getFilteredCards state.storedCards otherInstruments


type Instrument = {name :: String, noCost :: Boolean}

flattenValues :: String -> Array EMIData -> Instrument
flattenValues name emiData = let
	noCost = foldl (\acc (EMIData bk) -> acc || bk.noCost) false emiData
	in {name, noCost}

getFilteredCards :: Array StoredCard -> Array Instrument -> Array StoredCard
getFilteredCards cards instruments =
	filter
		(\(StoredCard card) ->
			foldl
				(\acc instrument ->
					acc || (String.toLower instrument.name == String.toLower (fm (String.split (String.Pattern " ") card.cardIssuer !! 0))))
				false
				instruments
		)
		cards
	where
	fm = fromMaybe ""

noCostEmiView :: ∀ w. (Action -> Effect Unit) -> State -> Array Instrument ->  Array StoredCard -> PrestoDOM (Effect Unit) w
noCostEmiView push state instruments storedCards =
	linearLayout
	[ width MATCH_PARENT
	, height WRAP_CONTENT
	, orientation VERTICAL
	, visibility if null instruments then GONE else VISIBLE
  , UIUtils.sectionMargin state.configPayload
	] (
  [	PPUtils.getSectionHeader "NO COST EMI" state.configPayload false] <> (
    UICommons.addCurvedWrapper state.configPayload $
      ( EMIUtils.storedCardsView (EMIUtils.InstrumentAction push) state.configPayload storedCards false true true ) <>
      (	map
          (\item ->
              EMIUtils.getItemCard
                {instrument : item.name, secondaryText : "", radioButton : true}
                (EMIUtils.InstrumentAction push)
                true
                state.configPayload
          )
          instruments
      )
      )
  )

costEmiView :: ∀ w. (Action -> Effect Unit) -> State -> Array Instrument -> Array StoredCard -> PrestoDOM (Effect Unit) w
costEmiView push state instruments storedCards =
	linearLayout
	[ width MATCH_PARENT
	, height WRAP_CONTENT
	, orientation VERTICAL
	, visibility if null instruments then GONE else VISIBLE
  	, UIUtils.sectionMargin state.configPayload
	] (
  [	PPUtils.getSectionHeader "EMI WITH INTEREST" state.configPayload false] <> (
    UICommons.addCurvedWrapper state.configPayload $
      ( EMIUtils.storedCardsView (EMIUtils.InstrumentAction push) state.configPayload storedCards false true true) <>
      (	map
          (\item ->
              EMIUtils.getItemCard
                  {instrument : item.name, secondaryText : "", radioButton : true}
                  (EMIUtils.InstrumentAction push)
            true
                  state.configPayload
          )
          instruments
      )
    )
  )

noPlansView :: ∀ w. (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
noPlansView push state =
	textView
		[ height MATCH_PARENT
		, width MATCH_PARENT
		, text "No EMI plans available" -- TODO :: Expose this from STR
		, color $ CPUtils.fontColor state.configPayload
		, textSize $ CPUtils.fontSizeVeryLarge state.configPayload
		, fontStyle $ CPUtils.fontBold state.configPayload
		, UIUtils.cardPadding state.configPayload
		, gravity CENTER
		, background "#ffffff"
		]
