module View.EMI.Screens.PlansScreen where


import HyperPrelude.External(null, partition,Maybe(..), isJust,Effect,(==),Unit,(&&),(<>),($),map,(/=),(<<<),(>),(||),const,not)
import HyperPrelude.Internal (Length(..), Margin(..), Orientation(..), Padding(..), PrestoDOM, Screen, Visibility(..), alignParentBottom, background, height, linearLayout, orientation, padding, scrollBarY, scrollView, visibility, width, margin)
import Data.String as String
import Engineering.Helpers.Events (addCustomBackPress)
import PPConfig.Utils as CPUtils
import Remote.Types (ConfigPayload)
import Service.EC.Types.Instruments (EMIData(..))
import UI.Components.ListItem.View as ListItem
import UI.Components.PaymentOptionsConfig as PaymentOptionsConfig
import UI.Components.PaymentOptionsView as PaymentOptionsView
import UI.Components.PrimaryButton.Config as PrimaryButtonConfig
import UI.Components.PrimaryButton.View as PrimaryButton
import UI.Config as UIConfig
import UI.Utils as UIUtils
import View.EMI.Controllers.PlansController (Action(..), ScreenInput, ScreenOutput, State, eval, initialState, getPlanInputID)
import View.Stock.Container.Commons as UICommons
import View.EMI.Screens.Utils as EMIUtils
import View.PaymentPage.Screens.Utils as PPUtils

screen ::  ScreenInput -> Screen Action State ScreenOutput
screen input =
  { initialState : (initialState input)
  , name : "PlansScreen"
  , view
  , globalEvents : []
  , eval : eval
  }

view :: ∀ w. (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
view push state =
  UICommons.getParentLayout
    parentInput
    (UICommons.EMIPlansAction push)
    (plansSelectionView push state)
    (Just bottomView)
  where
  parentInput =
    { modalView : false
    , modalHeight : WRAP_CONTENT
    , modalAnimationTrigger : state.entry
    , toolbarHeader : "Credit Card EMI"
    , configPayload : state.configPayload
    , useContainerPadding : true
    , useRelativeLayout : true
    , amount : state.amount
    , showToolbar : true
    , phoneNumber : "" -- TODO :: add phone number to state.
    , showAmountBar : true
    , orderDescription : "" -- TODO :: add order Desc to state if required
    , customerName : "" -- TODO :: add customer name here
    }

  popupData = { amount : state.amount, tenure : 0, emiAmount: 0.0 }

  bottomView = EMIUtils.popupView (EMIUtils.PlansAction push) state.configPayload popupData state.showDetails

m :: Margin
m = (Margin 0 0 0 0)

p :: Padding
p = (Padding 16 0 16 0)

plansSelectionView :: ∀ w. (Action -> Effect Unit) -> State -> Array (PrestoDOM (Effect Unit) w)
plansSelectionView push state =
  [	scrollView
      [ width MATCH_PARENT
      , height WRAP_CONTENT
      , scrollBarY false
      ]
      [	linearLayout
          [ width MATCH_PARENT
          , height WRAP_CONTENT
          , orientation VERTICAL
          , addCustomBackPress push $ const OnBackPress
          , UIUtils.contentMargin state.configPayload
          ](
          [ linearLayout
              [ width MATCH_PARENT
              , height WRAP_CONTENT
              , margin (MarginBottom 12)
              ] (UICommons.addCurvedWrapper state.configPayload emiInstrumentDetails)
          ] <>
          [ plansView push state useExpandedCvv
          , linearLayout
              [ width MATCH_PARENT
              , height WRAP_CONTENT
              , UIUtils.sectionMargin state.configPayload
              ]
              [ EMIUtils.amountGadget (EMIUtils.PlansAction push) state.amount state.subventionAmount state.selectedPlan state.configPayload false
              ]
          ])
      ]
  -- , proceedButton push state useExpandedCvv
  ]
  where
  emiInstrumentDetails =
    case state.emiStoredCard of
      Just card ->
        EMIUtils.storedCardsView
          (EMIUtils.PlansAction push)
          state.configPayload
          [card]
          false
          false
          false
      Nothing -> [
        EMIUtils.getItemCard
          { instrument : state.instrument, secondaryText : "", radioButton : false}
          (EMIUtils.PlansAction push)
          false
          state.configPayload
        ]

  useExpandedCvv =
    case state.emiStoredCard of
      Just c -> true
      Nothing -> false

plansView :: ∀ w. (Action -> Effect Unit) -> State -> Boolean -> PrestoDOM (Effect Unit) w
plansView push state useExpandedCvv =
  linearLayout
    [ width MATCH_PARENT
    , height WRAP_CONTENT
    , orientation VERTICAL
    , UIUtils.sectionMargin state.configPayload
    ] ( UICommons.addCurvedWrapper state.configPayload $
    [	if state.subventionAmount > 0.0
            then noCostPlans push state state.configPayload emiPlans' state.selectedPlan useExpandedCvv
            else noCostPlans push state state.configPayload emiPlans.yes state.selectedPlan useExpandedCvv
    ,	otherPlans push state state.configPayload emiPlans.no state.selectedPlan useExpandedCvv
    ])
  where
  emiPlans = partition (\(EMIData i) -> i.noCost) state.emiPlans
  emiPlans' = EMIUtils.getNoCostEMIData state.emiPlans

noCostPlans :: ∀ w. (Action -> Effect Unit) -> State -> ConfigPayload -> Array EMIData -> Maybe EMIData -> Boolean -> PrestoDOM (Effect Unit) w
noCostPlans push state configPayload noCostPlans selectedPlan useExpandedCvv =
  linearLayout
    [ width MATCH_PARENT
    , height WRAP_CONTENT
    , orientation VERTICAL
    , visibility if null noCostPlans then GONE else VISIBLE
    , padding (PaddingTop 20)
    , UIUtils.sectionMargin configPayload
    ]
    [	PPUtils.getSectionHeader "No Cost EMI" configPayload false
    , EMIUtils.divider (Margin 0 0 0 0)
    , EMIUtils.wrapperLayout m p
      (	map
        (\e@(EMIData item) ->
          PaymentOptionsView.view
            (push <<< SelectPlanX e)
            (getEmiPlansConfig state e (isSelected e) configPayload useExpandedCvv)
            false
            Nothing
        )
        noCostPlans
    )
    ]
  where
  isSelected i = case selectedPlan of
                  Nothing -> false
                  Just sp -> sp == i


otherPlans :: ∀ w. (Action -> Effect Unit) -> State -> ConfigPayload -> Array EMIData -> Maybe EMIData -> Boolean -> PrestoDOM (Effect Unit) w
otherPlans push state configPayload otherPlans selectedPlan useExpandedCvv =
  linearLayout
    [ width MATCH_PARENT
    , height WRAP_CONTENT
    , orientation VERTICAL
    , visibility if null otherPlans then GONE else VISIBLE
    , padding (PaddingTop 20)
    , UIUtils.sectionMargin configPayload
    ]
    [ PPUtils.getSectionHeader "EMI with Interest" configPayload false
    , EMIUtils.divider (Margin 0 0 0 0)
    , EMIUtils.wrapperLayout m p
        (	map
           (\e@(EMIData item) -> do
              PaymentOptionsView.view
                (push <<< SelectPlanX e)
                (getEmiPlansConfig state e (isSelected e) configPayload useExpandedCvv)
                false
                Nothing
            )
            otherPlans
        )
    ]
  where
  isSelected i = case selectedPlan of
                      Nothing -> false
                      Just sp -> sp == i


getEmiPlansConfig :: State -> EMIData -> Boolean -> ConfigPayload -> Boolean -> PaymentOptionsConfig.Config
getEmiPlansConfig state emiData isSelected configPayload useExpandedCvv = let
  planData = EMIUtils.planText emiData state.subventionAmount
  PaymentOptionsConfig.Config defaultConfig =  PaymentOptionsConfig.defConfig configPayload
  isCvvValid = if String.length state.cvv == 3 then true else false
  updatedConfig = defaultConfig
      { secondaryTextVisibility = GONE
      , cvvInputVisibility = if useExpandedCvv && isSelected then VISIBLE else GONE
      , displayAreaPadding = (Padding 0 0 0 0 )
      , inputAreaVisibility = if isSelected then VISIBLE else GONE
      , inputAreaMargin = if useExpandedCvv then (Margin 10 0 0 8) else (Margin 0 0 0 0)
      , lineSeparatorColor = "#FFFFFF"
      , radioButtonIconUrl = (if isSelected then "tick" else "circular_radio_button")
      , primaryText = planData
      , primaryTextFont = CPUtils.fontSemiBold configPayload
      , primaryTextMargin = (Margin 10 2 0 0)
      , primaryText2 = "Details"
      , primaryText2Color = CPUtils.primaryColor configPayload
      , primaryText2Visibility = VISIBLE
      , primaryText2Margin = Margin 10 0 0 0
      , logoVisibility = GONE
      , gridLogoMargin = (Margin 0 0 0 0)
      , buttonAlpha = if isCvvValid || not useExpandedCvv then 1.0 else 0.5
      , buttonClickable = if isCvvValid || not useExpandedCvv then true else false
      , startAnimation = state.buttonAnim
      , logoPadding = (Padding 0 0 0 0 )
      , quantLayoutMargin = (Margin 0 0 0 0)
      , gridItemMargin = (Margin 0 0 0 0)
      , space = 0
      , lineSeparatorMargin = (Margin 0 0 0 0)
      , enableSecondLineClick = true
      , displayAreaHeight = (V 24)
      , displayAreaMargin = if isSelected then (Margin 0 0 0 7) else (Margin 0 0 0 16)
      , bottomLayoutMargin = (Margin 0 0 0 16)
      , cvvId = getPlanInputID emiData
      }
    in PaymentOptionsConfig.Config updatedConfig

-- Hacked solution for placing button at bottom. does not supports button background in case container padding is used.
proceedButton :: ∀ w. (Action -> Effect Unit) -> State -> Boolean -> PrestoDOM (Effect Unit) w
proceedButton push state useExpandedCvv =
  linearLayout (
    [ width MATCH_PARENT
    , height WRAP_CONTENT
    , alignParentBottom "true,-1"
    , visibility $
        if isJust state.selectedPlan && useExpandedCvv /= true
         then VISIBLE
         else GONE
    ] <> if btnBackground == "" || space' > 0 then [] else [background btnBackground])
    [ PrimaryButton.view
        (push <<< Proceed)
        (PrimaryButtonConfig.Config updatedConfig)
    ]
  where
  btnBackground = CPUtils.btnBackground state.configPayload
  PrimaryButtonConfig.Config defConfig = UIConfig.primaryButtonConfig state.configPayload
  space' = CPUtils.horizontalSpace state.configPayload
  space = if space' == 0 then CPUtils.uiCardHorizontalPadding state.configPayload else space'
  updatedConfig = defConfig
    { text = "Proceed to Pay"
    , startAnimation = state.buttonAnim
    , margin = (Margin space 10 space 10) -- overiding because button will always on bottom
    , alpha = if isValidCondition then 1.0 else 0.5
    , isClickable = isValidCondition
    }

  isValidCondition =
    case isJust state.emiStoredCard, String.length state.cvv == 3 of
      true, false -> false
      true, true  -> true
      _, _        -> true
