module View.EMI.Screens.Utils where

import HyperPrelude.External ((!!),toStringAs, decimal, toNumber,Maybe(..), fromMaybe,Unit,(/),(/=),(==),(<>),(*),(<<<),(>=),(||),($),(&&),(+),(>),const,map,unit,(-),(<=),not)
import HyperPrelude.Internal (Gravity(..), Length(..), Margin(..), Orientation(..), Padding(..), PrestoDOM, Visibility(..), background, color, fontStyle, gravity, height, imageUrl, imageView, linearLayout, margin, onClick, orientation, padding, text, textSize, textView, visibility, weight, width)
import Data.Number.Format (toString)
import Data.String (toUpper)
import Data.String as String
import Data.String.Yarn (capWords)
import Effect (Effect)
import Engineering.Helpers.Commons (emiBankIINMap, getFormattedIssuerName)
import Global (toFixed)
import Math (pow)
import PPConfig.Utils as CPUtils
import PPConfig.Utils
import PrestoDOM.Animation as PrestoAnim
import Remote.Types (ConfigPayload)
import Service.EC.Types.Instruments (EMIData(..), EMIPlan(..))
import Service.EC.Types.Instruments as Instruments
import UI.Components.PaymentOptionsConfig as PayOptConfig
import UI.Components.PaymentOptionsView as PayOpt
import UI.Components.Popup.Config as PopupConfig
import UI.Components.Popup.View as Popup
import UI.Config as UIConfig
import UI.Utils (LineItemAction(..), screenHeight, sectionMargin, translateInYAnim, translateOutYAnim)
import View.EMI.Controllers.CheckoutController as EMICheckout
import View.EMI.Controllers.InstrumentsController as Instrument
import View.EMI.Controllers.PlansController as Plans
import View.PaymentPage.Screens.Utils as PPUtils
import View.Stock.Container.Commons as UICommons

data Action
  = InstrumentAction (Instrument.Action -> Effect Unit)
  | PlansAction (Plans.Action -> Effect Unit)
  | EMICheckoutAction (EMICheckout.Action -> Effect Unit)

type CardInput =
  { instrument :: String
  , secondaryText :: String
  , radioButton :: Boolean
  }

getItemCard :: ∀ w. CardInput -> Action -> Boolean -> ConfigPayload -> PrestoDOM (Effect Unit) w
getItemCard input push lineSeperatorVisibility configPayload =
  PayOpt.view
    action
    (getItemConfig configPayload input input.radioButton lineSeperatorVisibility)
    false
    Nothing
  where
    action = do
      case push of
        InstrumentAction x  -> (x <<< Instrument.ProceedWith input.instrument)
        PlansAction x       -> (x <<< Plans.StoredCardAction) -- is equivalent to numb action
        EMICheckoutAction x -> (x <<< EMICheckout.NumbAction)

getItemConfig :: ConfigPayload -> CardInput -> Boolean -> Boolean -> PayOptConfig.Config
getItemConfig configPayload input showRadio lineSeperatorVisibility = let
  iName = input.instrument
  plan = input.secondaryText
  (PayOptConfig.Config defaultConfig) = PayOptConfig.defConfig configPayload
  updateConfig = defaultConfig {
        inputAreaVisibility = GONE
      , primaryText = (capWords iName <> " Credit Card")
      , logoUrl = if toUpper iName == "AMEX" then "card_type_amex" else "ic_bank_" <> (emiBankIINMap $ toUpper iName)
      , secondaryTextVisibility = (if plan == "" then GONE else VISIBLE)
      , secondaryText = plan
      , secondaryTextColor = CPUtils.fontColor configPayload
      , secondaryTextFont = CPUtils.fontSemiBold configPayload
      , radioButtonIconUrl = "right_arrow"
      , secondaryText2 = if plan == "" then "" else "Details"
      , radioButtonVisibility = if showRadio
                                  then VISIBLE
                                  else GONE
      , isSecondaryTextClickable = true--if plan == "" then false else true
      , primaryTextFont = CPUtils.fontSemiBold configPayload
      , lineSeparatorMargin = Margin 17 0 17 0
      , lineSeparatorVisibility = if lineSeperatorVisibility then VISIBLE else GONE
      }
  in (PayOptConfig.Config updateConfig)

divider :: ∀ w. Margin -> PrestoDOM (Effect Unit) w
divider m =
  linearLayout
    [ width MATCH_PARENT
    , height $ V 1
    , background "#D8D8D8"
    , margin m
    ][]

wrapperLayout :: ∀ w. Margin -> Padding ->  Array (PrestoDOM (Effect Unit) w ) -> PrestoDOM (Effect Unit) w
wrapperLayout m p child=
  linearLayout
    [   width MATCH_PARENT
    ,   height WRAP_CONTENT
    ,   margin m
    ,   padding p
    ,   orientation VERTICAL
    ] child

amountGadget :: ∀ w. Action -> Number -> Number -> Maybe EMIData -> ConfigPayload -> Boolean -> PrestoDOM (Effect Unit) w
amountGadget push amount subventionAmount plan configPayload addAdditionalMargin =
  case plan of
    Just ePlan ->
      linearLayout
        [ width MATCH_PARENT
        , height MATCH_PARENT
        , orientation VERTICAL
        , padding (PaddingTop 20)
        , if addAdditionalMargin then (margin (MarginBottom 0)) else (sectionMargin configPayload)
        ]
        [ PPUtils.getSectionHeader "Amount with selected EMI" configPayload false
        , costView push configPayload amount subventionAmount ePlan
        ]
    Nothing -> linearLayout[height $ V 0, width MATCH_PARENT][]

costView :: ∀ w. Action -> ConfigPayload -> Number -> Number -> EMIData -> PrestoDOM (Effect Unit) w
costView push configPayload amount subventionAmount (EMIData plan) =
  linearLayout
    [ width MATCH_PARENT
    , height WRAP_CONTENT
    , orientation VERTICAL
    ]
    (UICommons.addCurvedWrapper configPayload
    (
        [   linearLayout
            [ width MATCH_PARENT
            , height WRAP_CONTENT
            , orientation VERTICAL
            , background "#ffffff"
            , padding (Padding 0 8 0 8)
            ]
            ( map
                (\i -> lineText push i configPayload)
                listItems)
        ]
    ))
  where
  textColor = CPUtils.fontColor configPayload
  v = fromMaybe ""
  s = toFixed 2
  (EMIPlan emiPlan) = plan.emiPlan
  principal = emiPlan.transactionAmount
  emiAmount = fromMaybe (principal/ toNumber emiPlan.tenure) emiPlan.emiAmount
  totalAmount = fromMaybe emiPlan.transactionAmount emiPlan.totalAmount
  rate = ((emiPlan.interest) / 12.0) / 100.0
  tenure = toNumber emiPlan.tenure
  emiOne = (principal * rate * (pow (1.0 + rate) tenure)) / ((pow (1.0 + rate) tenure) - 1.0 )
  interestByBnk = totalAmount - principal
  emiTwo = (subventionAmount * rate * (pow (1.0 + rate) tenure)) /  ((pow (1.0 + rate) tenure) - 1.0 )
  interestByBnk1 = (emiOne * tenure) - principal
  noCostEMIDiscount = if subventionAmount > 0.0 && plan.noCost
                          then if subventionAmount == principal
                              then interestByBnk
                              else if emiPlan.interest > 0.0
                                  then ((emiTwo * tenure) - subventionAmount)
                                  else 0.0
                          else 0.0
  updatedTotalAmount = if subventionAmount <= 0.0
                          then totalAmount
                          else (principal - noCostEMIDiscount)
  updatedEMIAmount = if emiPlan.interest > 0.0 && subventionAmount >= 0.0
                          then (updatedTotalAmount * rate * (pow (1.0 + rate) tenure)) /  ((pow (1.0 + rate) tenure) - 1.0 )
                          else emiAmount
  emiAmountStr = v $ s $ updatedEMIAmount
  principalStr = v $ s $ principal
  totalStr = v $ s $ updatedTotalAmount
  interestByBnkStr = v $ s $ interestByBnk
  noCostEMIDiscountStr = v $ s $ noCostEMIDiscount
  listItems = listItemsView principalStr interestByBnkStr emiPlan.interest noCostEMIDiscount totalStr emiAmountStr textColor configPayload noCostEMIDiscountStr




listItemsView :: String -> String -> Number -> Number -> String -> String -> String -> ConfigPayload -> String -> Array LineTextInput
listItemsView principalStr interestByBnkStr emiPlanInterest noCostEMIDiscount totalStr emiAmountStr textColor configPayload noCostEMIDiscountStr =
    [
        { leftStr : "Price (1 item)"
        , rightStr : "Rs. "<> principalStr
        , useImage : false
        , textColor
        , addDivider: false
        , fontFace : mapToFontFace "regular"
        }
      , { leftStr : "Interest charged (by bank)"
        , rightStr : "Rs. "<> interestByBnkStr
        , useImage : false
        , textColor
        , addDivider: false
        , fontFace : mapToFontFace "regular"
        }
    ] <> (if (emiPlanInterest >= 0.0 && noCostEMIDiscount <= 0.0) then [] else planLine) <>
    [
        { leftStr : "Total Amount Payable"
        , rightStr : "Rs. " <> totalStr
        , useImage : true
        , textColor
        , addDivider: true
        , fontFace : mapToFontFace "bold"
        }
      ,  { leftStr : "EMI Amount (per month)"
        , rightStr : "Rs. " <> emiAmountStr
        , useImage : false
        , textColor
        , addDivider: false
        , fontFace : mapToFontFace "regular"
        }
    ]
    where
    mapToFontFace input = CPUtils.mapToFontFamily input configPayload

    planLine = [
            { leftStr : "No Cost EMI Discount"
            , rightStr : "Rs. -"<> noCostEMIDiscountStr
            , useImage : false
            , textColor : "#00ba24"
            , addDivider: true
            , fontFace : mapToFontFace "regular"
            }
        ]


type LineTextInput =
  { leftStr :: String
  , rightStr :: String
  , useImage :: Boolean
  , textColor :: String
  , addDivider :: Boolean
  , fontFace :: String
  }

lineText :: ∀ w. Action -> LineTextInput -> ConfigPayload -> PrestoDOM (Effect Unit) w
lineText push li configPayload =
  linearLayout
    [ width MATCH_PARENT
    , height WRAP_CONTENT
    , orientation VERTICAL
    ]
    ([ wrapperLayout m p
        [ linearLayout
            [ width MATCH_PARENT
            , height WRAP_CONTENT
            , background "#ffffff"
            , padding $ Padding 5 5 5 5
            , gravity CENTER
            ]
            [   tv li.leftStr li.useImage
            ,   linearLayout
                [ height $ V 26
                , width $ V 26
                , padding $ Padding 5 3 5 3
                , gravity CENTER
                , onClick action $ const InfoClick
                , visibility if li.useImage then VISIBLE else GONE
                ]
                [ imageView
                    [ width $ V 15
                    , height $ V 15
                    , imageUrl "info_icon"
                    ]
                ]
            ,   linearLayout[width $ V 0, weight 1.0, height MATCH_PARENT][]
            ,   tv li.rightStr false
            ]
        ]
    ] <> if li.addDivider
            then [divider (Margin 0 0 0 5)]
            else []
    )
  where
  m = Margin 0 3 0 3
  p = Padding 16 0 16 0
  tv val isMarginReq = textView [ width WRAP_CONTENT
                                , height WRAP_CONTENT
                                , textSize $ CPUtils.fontSize configPayload
                                , fontStyle li.fontFace
                                , color li.textColor
                                , text val
                                , margin if isMarginReq then (MarginRight 5) else (MarginRight 0)
                                ]
  action = case push of
             InstrumentAction x  -> (x <<< Instrument.ShowDetails)
             PlansAction x       -> (x <<< Plans.ShowDetails)
             EMICheckoutAction x -> (x <<< EMICheckout.ShowDetails)

type PopupInput = {amount :: Number, tenure :: Int, emiAmount :: Number}

popupData :: String -> String -> String -> Array String
popupData amount emi tenure =
  [ "₹" <> amount <>  " will be blocked on your card now. It will be converted into EMI in 3-4 working days"] <>
  (if emi == "0.0" || tenure == "0" then [] else ["You will pay ₹" <> emi <> " per month for " <> tenure <> " months"]) <>
  [ "The total price excludes the tax on the interest"
  , "Curefit is not responsible for the interest charged by the bank"
  , "Curefit cannot refund the interest in case of cancellation, refund or pre-closure"
  ]

popupView :: ∀ w. Action -> ConfigPayload -> PopupInput -> Boolean -> PrestoDOM (Effect Unit) w
popupView push configPayload pi showPopup =
  Popup.view action (PopupConfig.Config updatedConfig) Nothing anim
  where
  anim =
    Just $ PrestoAnim.animationSet
      [ translateInYAnim configPayload (screenHeight unit) showPopup
      , translateOutYAnim configPayload (screenHeight unit) (not showPopup)
      ]

  PopupConfig.Config config = UIConfig.popupConfig configPayload
  updatedConfig = config {
        listStrings = (popupData (toString pi.amount) (toString pi.emiAmount) (toStringAs decimal pi.tenure))
      , open = showPopup
      , close = not showPopup
      , header = "EMI Terms & Conditions"
      , buttonOneVisibility = GONE
      , buttonTwoVisibility = GONE
      , termsVisibility = GONE
      , checkBoxVisibility = GONE
      }

  action = case push of
    InstrumentAction x  -> (x <<< Instrument.PopupAction)
    PlansAction x       -> (x <<< Plans.PopupAction)
    EMICheckoutAction x -> (x <<< EMICheckout.PopupAction)

planText :: EMIData -> Number -> String
planText (EMIData emiData) subventionAmount =
    let
        EMIPlan emiPlan = emiData.emiPlan
        planType = if emiData.interest == 0.0 || emiData.noCost
                        then "No Cost"
                        else (toString emiData.interest) <> "% p.a."
        amount = fromMaybe  "" $ toFixed 2 $ if emiData.amount == 0.0 then (emiPlan.transactionAmount/ toNumber emiPlan.tenure) else emiData.amount
        updatedEMIAmount =  if subventionAmount > 0.0 && emiData.noCost
                                then fromMaybe  "" $ toFixed 2 $ getUpdatedEMIAmount (EMIData emiData) subventionAmount
                                else amount
    in    "Rs. "
        <> updatedEMIAmount
        <> "  |  "
        <> (toStringAs decimal emiData.tenure)
        <> " months  |  "
        <> planType

storedCardsView :: ∀ w. Action -> ConfigPayload -> Array Instruments.StoredCard -> Boolean -> Boolean -> Boolean -> Array (PrestoDOM (Effect Unit) w)
storedCardsView push configPayload cards useCVVInput showRadio lineSeperatorVisibility =
  map
    (\card ->
      PayOpt.view
        (action card)
        (getCardConfig card configPayload useCVVInput showRadio lineSeperatorVisibility)
        false
        Nothing
    )
  cards
  where
  action card = do
    case push of
      InstrumentAction x  -> (x <<< Instrument.ProceedWithStoredCard card)
      PlansAction x       -> (x <<< Plans.StoredCardAction)
      EMICheckoutAction x -> (x <<< EMICheckout.NumbAction)

getCardConfig :: Instruments.StoredCard -> ConfigPayload -> Boolean -> Boolean -> Boolean ->  PayOptConfig.Config
getCardConfig (Instruments.StoredCard card) cP useCVVInput showRadio lineSeperatorVisibility  =
    -- Fetch this value from PPConfig.Utils when implemented properly.
  let
    poViewType = PayOptConfig.TypeTwo
    PayOptConfig.Config defConfig = PayOptConfig.defConfig cP
    formattedIssuerName = getFormattedIssuerName card.cardIssuer
    updatedConfig = defConfig {
        inputAreaVisibility = if useCVVInput then VISIBLE else GONE
    , startAnimation = false
    , primaryLogoVisibility = (if poViewType /= PayOptConfig.TypeOne then GONE else VISIBLE)
    , primaryLogo = ("card_type_" <> String.toLower card.cardBrand)
    , logoUrl = "ic_bank_" <> (emiBankIINMap $ toUpper $ fromMaybe "" $ String.split (String.Pattern " ") card.cardIssuer !! 0)
    , secondaryLogoVisibility = (if poViewType == PayOptConfig.TypeTwo then VISIBLE else GONE)
    , secondaryLogo = ("card_type_" <> String.toLower card.cardBrand)
    , secondaryLogoPosition = PayOptConfig.Left
    , secondaryTextColor = "#5C5C5C"
    , secondaryLogoSize = V 24
    , secondaryText =
        if poViewType == PayOptConfig.TypeTwo then
          String.replace (String.Pattern "-") (String.Replacement " ") (String.drop 10 card.cardNumber)
          else card.cardNumber
    , primaryText = (if formattedIssuerName == ""
                        then card.cardIssuer <> (capWords $ String.toLower card.cardType) <> " Card"
                        else (formattedIssuerName <> " " <> (capWords $ String.toLower card.cardType)) <> " Card")
    , radioButtonIconUrl = "right_arrow"
    , radioButtonGravity = CENTER_VERTICAL
    , buttonVisibility = GONE
    , bottomDefaultExpand = false
    , radioButtonVisibility = if showRadio then VISIBLE else GONE
    , showCvvInfoLogo = false
    , primaryTextFont = CPUtils.fontSemiBold cP
    , lineSeparatorVisibility = if lineSeperatorVisibility then VISIBLE else GONE
    }
  in PayOptConfig.Config updatedConfig

getNoCostEMIData :: Array EMIData -> Array EMIData
getNoCostEMIData emiDataList =
    map(\(EMIData emiData) -> do
        EMIData {
                    amount : emiData.amount
                ,   tenure : emiData.tenure
                ,   noCost : true
                ,   interest : emiData.interest
                ,   bank : emiData.bank
                ,   emiPlan : emiData.emiPlan
                }
        ) emiDataList

getUpdatedEMIAmount :: EMIData -> Number -> Number
getUpdatedEMIAmount (EMIData plan) subventionAmount = updatedEMIAmount
  where
  (EMIPlan emiPlan) = plan.emiPlan
  principal = emiPlan.transactionAmount
  emiAmount = fromMaybe (principal/ toNumber emiPlan.tenure) emiPlan.emiAmount
  totalAmount = fromMaybe emiPlan.transactionAmount emiPlan.totalAmount
  rate = ((emiPlan.interest) / 12.0) / 100.0
  tenure = toNumber emiPlan.tenure
  emiOne = (principal * rate * (pow (1.0 + rate) tenure)) / ((pow (1.0 + rate) tenure) - 1.0 )
  interestByBnk = totalAmount - principal
  emiTwo = (subventionAmount * rate * (pow (1.0 + rate) tenure)) /  ((pow (1.0 + rate) tenure) - 1.0 )
  interestByBnk1 = (emiOne * tenure) - principal
  noCostEMIDiscount = if subventionAmount > 0.0 && plan.noCost
                          then if subventionAmount == principal
                              then interestByBnk
                              else if emiPlan.interest > 0.0
                                  then ((emiTwo * tenure) - subventionAmount)
                                  else 0.0
                          else 0.0
  updatedTotalAmount = if subventionAmount <= 0.0
                          then totalAmount
                          else (principal - noCostEMIDiscount)
  updatedEMIAmount = if emiPlan.interest > 0.0 && subventionAmount >= 0.0
                          then (updatedTotalAmount * rate * (pow (1.0 + rate) tenure)) /  ((pow (1.0 + rate) tenure) - 1.0 )
                          else emiAmount
