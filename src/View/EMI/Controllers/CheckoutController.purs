module View.EMI.Controllers.CheckoutController where

import HyperPrelude.External(Maybe(..),Effect,liftEffect,launchAff_,delay,Milliseconds(..),($),not,(==),(<>),const,(<$>),bind,pure)
import HyperPrelude.Internal (Eval,Prop,afterRender,continue,updateAndExit,continueWithCmd)
import Remote.Types (ConfigPayload(..), defaultMerchantOffer, PaymentOptions(..))
import Service.EC.Types.Instruments (EMIData(..))
import UI.Components.AddCard.Controller as AddNewCard
import UI.Components.AddCard.Types as AddNewCardTypes
import UI.Components.PaymentOptionsController as PayOpt
import UI.Components.Popup.Controller as Popup
import UI.Components.PrimaryButton.Controller as PrimaryButton
import Engineering.Helpers.Commons (PaymentOffer)
import Payments.Core.Commons (getDummyPaymentOffers)
import UI.Components.ToolBar.Controller as ToolBar
import UI.Utils (FieldType(CardNumber), ModalAction(..), LineItemAction, getFieldTypeID)
import JBridge as JBridge
import UI.Components.Popup.Controller as Popup
import Payments.Wallets.Types (MandateType(..))

data ScreenOutput
  = BackPressed
  | UseCard AddNewCardTypes.State EMIData

data Action
  = OnBackPress
  | ToolBarAction ToolBar.Action
  | OverlayClick ModalAction
  | NumbAction PayOpt.Action
  | Proceed PrimaryButton.Action
  | ShowDetails LineItemAction
  | PopupAction Popup.Action
  | AddCardAction AddNewCardTypes.Action
  | CheckoutScreenRendered String
  | ContinueCommand
  | OfferPopupAction Popup.Action

type ScreenInput =
	{ configPayload :: ConfigPayload
	, emiPlan :: EMIData
  , amount :: Number
  , subventionAmount :: Number
  , mandateType :: MandateType
	}

type State =
	{ configPayload :: ConfigPayload
	, entry :: Boolean
	, emiPlan :: EMIData
  , buttonAnim :: Boolean
  , showDetails :: Boolean
  , addCardState :: AddNewCardTypes.State
  , subventionAmount :: Number
  , amount :: Number
  }

updateConfig :: EMIData -> ConfigPayload -> ConfigPayload
updateConfig (EMIData ePlan) (ConfigPayload configPayload) = do
  ConfigPayload $ configPayload { paymentOptions = (\po -> updateCardPO po ) <$> configPayload.paymentOptions }
  where
  updateCardPO (PaymentOptions po ) =
    if po.po == "cards"
      then PaymentOptions $ po { onlyEnable = po.onlyEnable <> Just [ePlan.bank, "Credit"]}
      else PaymentOptions po

initialState :: ScreenInput -> State
initialState input =
	{ configPayload : input.configPayload
	, entry : true
	, emiPlan : input.emiPlan
  , buttonAnim : false
  , showDetails : false
  , addCardState
  , amount : input.amount
  , subventionAmount : input.subventionAmount
  }
  where
  addCardState  =
    AddNewCard.initialState
      { supportedMethods : []
      , cardMethod : AddNewCardTypes.AddNewCard
      , configPayload : (updateConfig input.emiPlan input.configPayload)
      , merchantOffer : defaultMerchantOffer
      , amount : input.amount
      , mandateType : input.mandateType
      , cardBinOffers : getDummyPaymentOffers
      , outages : []
      }

eval :: Action -> State -> Eval Action ScreenOutput State
eval OnBackPress state =
  let updatedState = state {entry = not state.entry} in
  updateAndExit updatedState $ BackPressed

eval (OverlayClick ClickedOutside) state =
  let updatedState = state {entry = not state.entry} in
  updateAndExit updatedState $ BackPressed

eval (ToolBarAction ToolBar.Clicked) state =
  let updatedState = state {entry = not state.entry} in
  updateAndExit updatedState $ BackPressed

eval (PopupAction action) state =
  case action of
    _ -> continue state {showDetails = false}

eval (ShowDetails action) state = continue state {showDetails = true}

eval (NumbAction action) state = do
  case action of
    PayOpt.SecondaryTextTwoClick -> continue state {showDetails = true}
    _ -> continue state {showDetails = true}

eval (AddCardAction action) state = do
  case action of
    (AddNewCardTypes.SubmitCard AddNewCardTypes.AddNewCard) -> let
      newState = state.addCardState { startButtonAnimation = true }
      in updateAndExit (state { addCardState  = newState }) $ UseCard state.addCardState state.emiPlan

    cardAction -> let
      updatedCardState = AddNewCard.eval action state.addCardState
      in continue $ state {addCardState = updatedCardState}

eval (CheckoutScreenRendered id) state = do
  continueWithCmd state [
    do
    _ <- launchAff_ do
      _ <- delay (Milliseconds 300.0)
      liftEffect $ JBridge.requestKeyboardShow id
    pure $ ContinueCommand
  ]

eval action state = continue state

overrides "MainLayout" push state =
  [ afterRender push $ const (CheckoutScreenRendered (getFieldTypeID CardNumber))]

overrides _ push state = []
