module View.EMI.Controllers.PlansController where


import HyperPrelude.External(show,delay,not,($),(<>),(==),Effect,Maybe(..),isJust,liftEffect,pure,bind,launchAff_,Unit,unit,discard,Milliseconds(..),toStringAs,decimal,toString)
import HyperPrelude.Internal (continue, exit, updateAndExit)
import JBridge as JBridge
import PrestoDOM (afterRender,continue, updateAndExit, continueWithCmd)
import Remote.Types (ConfigPayload(..))
import Service.EC.Types.Instruments (EMIData, StoredCard(..))
import UI.Components.ListItem.Controller as ListItem
import UI.Components.PaymentOptionsController as PayOpt
import UI.Components.Popup.Controller as Popup
import UI.Components.PrimaryButton.Controller as PrimaryButton
import UI.Components.ToolBar.Controller as ToolBar
import UI.Utils (ModalAction(..), LineItemAction, resetText)
import Service.EC.Types.Instruments (EMIData(..))
import UI.Components.Popup.Controller as Popup

data ScreenOutput
  = BackPressed
  | ProceedWithPlan EMIData
  | EMIWithStoredCard StoredCard String EMIData

data Action
  = OnBackPress
  | ToolBarAction ToolBar.Action
  | OverlayClick ModalAction
  | StoredCardAction PayOpt.Action
  | SelectPlan EMIData ListItem.Action
  | Proceed PrimaryButton.Action
  | ShowDetails LineItemAction
  | PopupAction Popup.Action
  | SelectPlanX EMIData PayOpt.Action
  | ContinueCommand
  | PlaneScreenRendered String
  | OfferPopupAction Popup.Action

type ScreenInput =
	{ configPayload :: ConfigPayload
	, emiData :: Array EMIData
  , emiStoredCard :: Maybe StoredCard
  , subventionAmount :: Number
	, instrument :: String
  , amount :: Number
	}

type State =
	{ configPayload :: ConfigPayload
	, entry :: Boolean
	, emiPlans :: Array EMIData
	, instrument :: String
  , selectedPlan :: Maybe EMIData
  , buttonAnim :: Boolean
  , showDetails :: Boolean
  , subventionAmount :: Number
  , amount :: Number
  , emiStoredCard :: Maybe StoredCard
  , cvv :: String
  , cvvExpanded :: Boolean
	}

initialState :: ScreenInput -> State
initialState input =
	{ configPayload : input.configPayload
	, entry : true
	, emiPlans : input.emiData
	, instrument : input.instrument
  , selectedPlan : Nothing
  , buttonAnim : false
  , showDetails : false
  , amount : input.amount
  , subventionAmount : input.subventionAmount
  , emiStoredCard : input.emiStoredCard
  , cvv : ""
  , cvvExpanded : false
	}

eval OnBackPress state =
  let updatedState = state {entry = not state.entry} in
  updateAndExit updatedState $ BackPressed

eval (OverlayClick ClickedOutside) state =
  let updatedState = state {entry = not state.entry} in
  updateAndExit updatedState $ BackPressed

eval (ToolBarAction ToolBar.Clicked) state =
  let updatedState = state {entry = not state.entry} in
  updateAndExit updatedState $ BackPressed

eval (SelectPlan plan action) state =
  case action of
    ListItem.RadioClick -> let
      uPlan = case state.selectedPlan of
        Just sPlan -> if sPlan == plan then Nothing else Just plan
        Nothing -> Just plan
      in continue state {selectedPlan = uPlan}

    ListItem.ButtonClicked -> continue state {showDetails = true}

eval (PopupAction action) state =
  case action of
    _ -> continue state {showDetails = false}

eval (Proceed PrimaryButton.Clicked) state =
  case state.selectedPlan of
    Just plan ->
      case state.emiStoredCard of
        Just x -> continue state
        Nothing ->
          updateAndExit state {buttonAnim = true} $ ProceedWithPlan plan
    _ -> continue state

eval (StoredCardAction action) state = continue state

eval (ShowDetails action) state = continue state {showDetails = true}

eval (SelectPlanX plan action) state =
  case action of
    PayOpt.PrimaryTextTwoClick -> continueWithCmd (state {showDetails = true}) $ hideKeyboard Nothing

    PayOpt.PaymentListItemSelection ->
      let
        uPlan = case state.selectedPlan of
          Just sPlan -> if sPlan == plan then Nothing else Just plan
          Nothing -> Just plan

        updatedState = state {selectedPlan = uPlan}

        continueAction =
          if isJust state.emiStoredCard then
            let inputID = getPlanInputID plan in
            -- if plan is selected
            if isJust uPlan
              then continueWithCmd updatedState $ showKeyboard inputID
            -- if plan is deselected
              else continueWithCmd (updatedState {cvv = ""}) $ hideKeyboard (Just inputID)
          else continue (state {selectedPlan = uPlan})

      in continueAction

    PayOpt.PaymentListAction PayOpt.ButtonClicked ->
      case state.selectedPlan of
        Just p ->
          case state.emiStoredCard of
            Just x -> updateAndExit state {buttonAnim = true } $ EMIWithStoredCard x state.cvv p
            Nothing -> updateAndExit state {buttonAnim = true} $ ProceedWithPlan plan
        _ -> continue state

    PayOpt.PaymentListAction (PayOpt.CVVChanged newCvv) -> continue state {cvv = newCvv}

    _ -> continue state

eval action state = continue state

hideKeyboard :: Maybe String -> Array (Effect Action)
hideKeyboard id = let
  resetFn = case id of
    Just id -> liftEffect $ resetText [id]
    Nothing -> liftEffect $ pure unit
  in
  [ do
      launchAff_ do
        _ <- delay (Milliseconds 100.0)
        liftEffect $ JBridge.requestKeyboardHide
        resetFn
      pure ContinueCommand
  ]

showKeyboard :: String -> Array (Effect Action)
showKeyboard id =
  [ do
      _ <- launchAff_ do
        liftEffect $ resetText [id]
        _ <- delay (Milliseconds 300.0)
        liftEffect $ JBridge.requestKeyboardShow id
      pure $ ContinueCommand
  ]

getPlanInputID :: EMIData -> String
getPlanInputID (EMIData emiData) = let
  --tenureStr = toStringAs decimal emiData.tenure
  tenureStr = show emiData.tenure
  interestStr = toString emiData.interest
  in JBridge.getNewIDWithTag (JBridge.EMI_PLAN_ID (tenureStr <> interestStr <> emiData.bank <> (if emiData.noCost then "noCost" else "")))
