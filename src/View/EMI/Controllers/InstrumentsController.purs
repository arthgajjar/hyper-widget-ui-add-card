module View.EMI.Controllers.InstrumentsController where

import HyperPrelude.External(delay,not,($))
import HyperPrelude.Internal (continue, exit, updateAndExit)
import Service.EC.Types.Instruments
import Foreign.Object as H
import Remote.Types (ConfigPayload)
import UI.Components.PaymentOptionsController as PayOpt
import UI.Components.Popup.Controller as Popup
import UI.Components.ToolBar.Controller as ToolBar
import UI.Utils (ModalAction(..), LineItemAction)

data ScreenOutput
  = BackPressed
  | EMIWith String
  | EMIWithStoredCard StoredCard

type ScreenInput =
  { configPayload :: ConfigPayload
  , emiPlans :: H.Object (Array EMIData)
  , storedCards :: Array StoredCard
  , amount :: Number
  , phoneNumber :: String
  }

type State =
  { configPayload :: ConfigPayload
  , entry :: Boolean
  , emiPlans :: H.Object (Array EMIData)
  , storedCards :: Array StoredCard
  , amount :: Number
  , phoneNumber :: String
  }

initialState :: ScreenInput -> State
initialState input =
  { configPayload : input.configPayload
  , entry : true
  , emiPlans : input.emiPlans
  , storedCards : input.storedCards
  , amount : input.amount
  , phoneNumber : input.phoneNumber
  }

data Action
  = OnBackPress
  | ToolBarAction ToolBar.Action
  | OverlayClick ModalAction
  | ProceedWith String PayOpt.Action
  | ShowDetails LineItemAction
  | PopupAction Popup.Action
  | ProceedWithStoredCard StoredCard PayOpt.Action
  | OfferPopupAction Popup.Action


eval OnBackPress state =
  let updatedState = state {entry = not state.entry} in
  updateAndExit updatedState $ BackPressed

eval (OverlayClick ClickedOutside) state =
  let updatedState = state {entry = not state.entry} in
  updateAndExit updatedState $ BackPressed

eval (ToolBarAction ToolBar.Clicked) state =
  let updatedState = state {entry = not state.entry} in
  updateAndExit updatedState $ BackPressed

eval (ProceedWith instrument action) state = do
  case action of
    PayOpt.PaymentListItemSelection -> exit $ EMIWith instrument
    _ -> continue state

eval (ProceedWithStoredCard storedCard action) state =
    exit $ EMIWithStoredCard storedCard

eval action state = continue state
