module View.QuickPay.Controllers.Controller where

import HyperPrelude.External(Maybe(..),launchAff_,liftEffect,(/=),($),not,bind,pure)
import HyperPrelude.Internal (doAff,continue, continueWithCmd, exit, updateAndExit)
import Service.EC.Types.Instruments as Instruments
import JBridge as JBridge
import Engineering.Helpers.Commons as Commons
import Payments.NetBanking.Utils (Bank(..))
import Remote.Types (ConfigPayload(..))
import UI.Components.PaymentOptionsController as PayOpt
import UI.Components.PrimaryButton.Controller as PrimaryButton
import UI.Components.ToolBar.Controller as ToolBar
import UI.Utils (ModalAction(..))
import Payments.Wallets.Types (MandateType(..))
import UI.Components.Popup.Controller as Popup

data Action
  = OnBackPressed
  | ToolBarAction ToolBar.Action
  | QuickPay PayOpt.Action
  | ButtonAction PrimaryButton.Action
  | OverlayClick ModalAction
  | ContinueCommand
  | OfferPopupAction Popup.Action

data QuickPayInstrumentType
  = SavedWallet Instruments.StoredWallet
  | SavedCard Instruments.StoredCard
  | NetBank Bank
  | UpiCollect String
  | ConsumerFinance
  | UpiIntent
  | NoLastUsedInstrument
  | NoQuickPay

data ScreenOutput
  = BackPressed
  | PayWithSavedWallet Instruments.StoredWallet
  | PayWithSavedCard Instruments.StoredCard String Boolean
  | PayWithNetBank Bank
  | PayWithUpiCollect String
  | GotoPaymentPage

type ScreenInput =
  { configPayload :: ConfigPayload
  , quickPayInstrument :: QuickPayInstrumentType
  , amount :: Number
  , mandateType :: MandateType
  , offers :: Array Commons.PaymentOffer
  }

type State =
  { configPayload :: ConfigPayload
  , entry :: Boolean
  , buttonAnim :: Boolean
  , instrument :: QuickPayInstrumentType
  , cvv :: String
  , amount :: Number
  , enableSI :: Boolean
  , mandateType :: MandateType
  , offers :: Array Commons.PaymentOffer
  }

initialState :: ScreenInput -> State
initialState input =
  { configPayload : input.configPayload
  , entry : true
  , buttonAnim : false
  , instrument : input.quickPayInstrument
  , cvv : ""
  , amount : input.amount
  , enableSI : input.mandateType /= None
  , mandateType : input.mandateType
  , offers : input.offers
  }


eval OnBackPressed state = exit BackPressed

eval (QuickPay action) state = do
    case action of
        PayOpt.PaymentListItemButtonClick -> exit GotoPaymentPage

        (PayOpt.PaymentListAction (PayOpt.CVVChanged cvvN)) -> continue state {cvv = cvvN}

        (PayOpt.PaymentListAction (PayOpt.ButtonClicked)) -> do
            let exitAction = case state.instrument of
                                SavedCard card -> PayWithSavedCard card state.cvv state.enableSI
                                _ -> GotoPaymentPage
            updateAndExit state {buttonAnim = true} $ exitAction
        (PayOpt.PaymentListAction (PayOpt.EnableSI)) -> do
            let enableSI = case state.instrument of
                                SavedCard card -> not state.enableSI
                                _ -> false
            continue state {enableSI = enableSI}
        (PayOpt.PaymentListAction (PayOpt.ToastCVVInfo)) ->
          continueWithCmd state [
            do
            _ <- launchAff_ do
                liftEffect $ JBridge.toast "This is the 3 digit number present on the back of your card."
            pure $ ContinueCommand
          ]
        _ -> continue state


eval (ButtonAction PrimaryButton.Clicked) state = let
  exitAction = case state.instrument of
    SavedWallet wallet  -> PayWithSavedWallet wallet
    SavedCard card      -> PayWithSavedCard card state.cvv state.enableSI
    NetBank bank        -> PayWithNetBank bank
    UpiCollect vpa      -> PayWithUpiCollect vpa
    _ -> GotoPaymentPage

  updatedState = case exitAction of
    GotoPaymentPage -> state
    _ -> state {buttonAnim = true}

  in updateAndExit updatedState $ exitAction

eval (OverlayClick ClickedOutside) state = exit BackPressed

eval action state = continue state

eval _ state = continue state
