module View.PaymentPage.QuickPay.Screens.Screen where



import HyperPrelude.External(find,fromMaybe,Maybe(..),isJust,maybe,Effect,(&&),(<>),(==),Unit,($),(#),show,const,(/=),(<<<))
import HyperPrelude.Internal (Length(..), Margin(..), Orientation(..), Padding(..), PrestoDOM, Screen, Visibility(..), background, height, linearLayout, margin, orientation, padding, weight, width)
import Data.Newtype (unwrap)
import Data.Number.Format (toString)
import Data.String (toLower, toUpper)
import Data.String as StrUtils
import Data.String.Yarn (capWords)
import Engineering.Helpers.Commons (PaymentOffer,getIinFromName, getFormattedIssuerName, getNbIin)
import Engineering.Helpers.Events (addCustomBackPress, afterRenderEvent)
import Validation (reqCVVLength)
import Halogen.VDom.DOM.Prop (Prop)
import PPConfig.Utils as CPUtils
import Payments.NetBanking.Utils (Bank(..))
import PrestoDOM.Properties (cornerRadii)
import PrestoDOM.Types.DomAttributes (Corners(..))
import Remote.Types (ConfigPayload(..))
import Service.EC.Types.Instruments as Instruments
import UI.Components.PaymentOptionsConfig as PayOptConfig
import UI.Components.PaymentOptionsView as PayOptView
import UI.Components.PrimaryButton.Config as PrimaryButtonConfig
import UI.Components.PrimaryButton.View as PrimaryButton
import UI.Config as UIConfig
import UI.Constant.Str.Default as STR
import UI.Utils (sectionMargin)
import Utils (_hide_loader)
import View.QuickPay.Controllers.Controller (Action(..), QuickPayInstrumentType(..), ScreenInput, ScreenOutput, State, eval, initialState)
import View.Stock.Container.Commons as UICommons
import View.PaymentPage.Screens.MoreOption (ReqData(..))
import View.PaymentPage.Screens.Utils as PPUtils
import Payments.Wallets.Types (MandateType(..))

screen :: ScreenInput -> Screen Action State ScreenOutput
screen input =
  { initialState: (initialState input)
  , name: "QuickPayScreen"
  , view
  , globalEvents: []
  , eval: eval
  }

view :: ∀ w. (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
view push state =
  UICommons.getParentLayout
    parentInput
    (UICommons.QuickPayAction push)
    (quickPayView push state)
    Nothing
  where
  parentInput =
    { modalView: true
    , modalHeight: WRAP_CONTENT
    , modalAnimationTrigger: state.entry
    , toolbarHeader: "Quick Pay"
    , configPayload: state.configPayload
    , useContainerPadding: false
    , useRelativeLayout: false
    , showToolbar: true
    , amount: state.amount
    , phoneNumber : ""
    , showAmountBar : false
    , orderDescription : "" -- TODO :: add if required
    , customerName : ""
    }

quickPayView :: ∀ w. (Action -> Effect Unit) -> State -> Array (PrestoDOM (Effect Unit) w)
quickPayView push state =
  [ linearLayout
      [ width MATCH_PARENT
      , height WRAP_CONTENT
      , orientation VERTICAL
      , background "#ffffff"
      , afterRenderEvent (_hide_loader)
      , cornerRadii $ Corners 8.0 true true false false
      ]
      [ sectionHeader state.configPayload state.amount
      , linearLayout [ width MATCH_PARENT, height $ V 1, background "#efefef" ] []
      , linearLayout
          [ width MATCH_PARENT
          , height WRAP_CONTENT
          , addCustomBackPress push (const OnBackPressed)
          , orientation VERTICAL
          , sectionPadding state.configPayload
          , sectionMargin state.configPayload
          ]
          ( [ PayOptView.view
                (push <<< QuickPay)
                ( getQuickPayItemConfig
                    state.instrument
                    state.configPayload
                    state.buttonAnim
                    state
                )
                false
                Nothing
            ]
              <> ( case state.instrument of
                    SavedCard x -> []
                    _ ->
                      [ PrimaryButton.view
                          (push <<< ButtonAction)
                          (PrimaryButtonConfig.Config btnConfig)
                      ]
                )
          )
      ]
  ]
  where
  PrimaryButtonConfig.Config stkBtnConfig = UIConfig.primaryButtonConfig state.configPayload
  payStr = STR.getProceed $ fromMaybe "" (state.configPayload # unwrap # _.language)
  btnConfig =
    stkBtnConfig
      { text = payStr
      , startAnimation = state.buttonAnim
      }

sectionPadding :: ∀ i. ConfigPayload -> Prop i
sectionPadding configPayload =
  let uiCardPadding = CPUtils.uiCardHorizontalPadding configPayload
  in  padding (Padding uiCardPadding 0 uiCardPadding 0)

sectionHeader :: ∀ w. ConfigPayload -> Number -> PrestoDOM (Effect Unit) w
sectionHeader configPayload amount = --changed
  linearLayout
    [ width MATCH_PARENT
    , height (V 40)
    , margin (MarginTop 15)
    , orientation HORIZONTAL
    , sectionPadding configPayload
    ]
    []

getQuickPayItemConfig :: QuickPayInstrumentType -> ConfigPayload -> Boolean -> State -> PayOptConfig.Config
getQuickPayItemConfig instrument configPayload  buttonAnim state = case instrument of
  SavedWallet savedWallet -> getSavedWalletConfig savedWallet configPayload state.offers
  SavedCard card -> getSavedCardConfig card buttonAnim (isCvvValid card) configPayload state.enableSI state.mandateType state.offers
  NetBank bank -> getNetBankConfig bank configPayload state.offers
  UpiCollect vpa -> getUpiCollectConfig vpa configPayload state.offers
  _ -> PayOptConfig.defConfig configPayload -- TODO :: implement for other instruments as well.
  where
  isCvvValid (Instruments.StoredCard card) = let
    requiredCVVLength = reqCVVLength card.cardBrand
    in StrUtils.length state.cvv == requiredCVVLength

getSavedWalletConfig :: Instruments.StoredWallet -> ConfigPayload -> Array PaymentOffer ->  PayOptConfig.Config
getSavedWalletConfig (Instruments.StoredWallet (Instruments.Wallet wallet)) configPayload offers = do
  let
    PayOptConfig.Config defaultConfig = PayOptConfig.defConfig configPayload
    --walletOffers = find (\offer -> StrUtils.toUpper (offer.paymentMethod) == (fromMaybe "" wallet.wallet)) offers
    offerDesc = "" --maybe "" (\b -> b.offerText) walletOffers
    updatedConfig =
      defaultConfig
        { inputAreaVisibility = GONE
        , primaryText = ((fromMaybe "NoName" wallet.wallet) <> " Wallet")
        , secondaryText = ("Balance: Rs. " <> (toString $ fromMaybe 0.0 wallet.currentBalance))
        , secondaryTextVisibility = VISIBLE
        , logoUrl = "ic_" <> (StrUtils.toLower (fromMaybe "" wallet.wallet))
        , secondaryTextColor = "#5C5C5C"
        , selectionLabel = "Change"
        , selectionType = PayOptConfig.Label
        , buttonClickable = true
        , displayAreaHeight = V 85
        , quantText = offerDesc
        , quantVisibility = if (CPUtils.ifOfferVisible configPayload && true) then VISIBLE else GONE
        , lineSeparatorVisibility = GONE
        }
  PayOptConfig.Config updatedConfig

getSavedCardConfig :: Instruments.StoredCard -> Boolean -> Boolean -> ConfigPayload -> Boolean -> MandateType -> Array PaymentOffer -> PayOptConfig.Config
getSavedCardConfig (Instruments.StoredCard card) startAnim isCvvValid configPayload enableSI mandateType offers = do
  let
    mandateEnabledForCard = if mandateType == Optional then CPUtils.isInstrumentEnabled configPayload "cards" else true
    mandateSupport = (card.mandateSupport == Just true && mandateEnabledForCard)
    --filteredOfferCards = find (\(offer) -> StrUtils.toUpper (offer.paymentMethod) == card.cardBrand) offers
    offerDesc = "" --maybe "" (\b -> b.offerText) filteredOfferCards
    offerVisibility = CPUtils.ifOfferVisible configPayload && true
    bankLogoURL = if StrUtils.toLower card.cardIssuer == "amex" then "card_type_amex" else ("ic_bank_" <> getIinFromName card.cardIssuer)
    PayOptConfig.Config defaultConfig = PayOptConfig.defConfig configPayload
    formattedIssuerName = getFormattedIssuerName card.cardIssuer
    cardName = if formattedIssuerName == ""
      then card.cardIssuer <> (if card.cardIssuer == "" then "" else " " ) <> (capWords $ toLower card.cardType) <> " Card"
      else (formattedIssuerName <> " " <> (capWords $ toLower card.cardType))
    updatedConfig =
      defaultConfig
        { cvvInputVisibility = VISIBLE
        , primaryText = cardName
        , secondaryText = card.cardNumber
        , cvvPattern = ("^([0-9])+$," <> (show $ reqCVVLength card.cardBrand))
        , secondaryTextColor = "#5C5C5C"
        , logoUrl = bankLogoURL
        , selectionLabel = "Change"
        , selectionType = PayOptConfig.Label
        , inputAreaVisibility = VISIBLE
        , startAnimation = startAnim
        , buttonAlpha = if isCvvValid then 1.0 else 0.5
        , buttonClickable = if isCvvValid then true else false
        , primaryLogo = ("card_type_" <> StrUtils.toLower card.cardBrand)
        , primaryLogoVisibility = VISIBLE
        , lineSeparatorVisibility = GONE
        , topRowVisibility = if mandateType /= None && mandateSupport then VISIBLE else GONE
        , isTopRowClickable = ( mandateType /= Required)
        , useTopRowForSI = mandateSupport
        , topRowText = "Automatically debit my card to renew subscription for every billing cycle"
        , topRowImage = if enableSI
                          then "checkbox1"
                          else "checkbox"
        , tertiaryVisibility = if (mandateSupport && mandateType /= None) then VISIBLE else GONE
        , tertiaryText = if mandateSupport then "Autopay available" else ""
        , quantText = offerDesc
        , quantVisibility = if offerVisibility then VISIBLE else GONE
        , selectionLabelSize = CPUtils.secondaryButtonTextSize configPayload
        }
  PayOptConfig.Config updatedConfig

getNetBankConfig :: Bank -> ConfigPayload -> Array PaymentOffer -> PayOptConfig.Config
getNetBankConfig (Bank bank) configPayload offers = do
  let
    --offerBank = find (\offer -> toUpper offer.paymentMethod == bank.code) offers
    offerDesc = "" --maybe "" (\b -> b.offerText) offerBank
    PayOptConfig.Config defCon = PayOptConfig.defConfig configPayload
    updatedConfig =
      defCon
        { logoUrl = ("ic_bank_" <> (getNbIin bank.code))
        , primaryText = capWords bank.name
        , secondaryText = "Pay using your Internet baking"
        , secondaryTextColor = "#5C5C5C"
        , selectionLabel = "Change"
        , selectionType = PayOptConfig.Label
        , lineSeparatorVisibility = GONE
        , inputAreaVisibility = GONE
        , selectionLabelSize = CPUtils.secondaryButtonTextSize configPayload
        , quantText = offerDesc
        , quantVisibility = if (CPUtils.ifOfferVisible configPayload && true) then VISIBLE else GONE
        }
  PayOptConfig.Config updatedConfig

getUpiCollectConfig :: String -> ConfigPayload -> Array PaymentOffer -> PayOptConfig.Config
getUpiCollectConfig vpa configPayload offers = do
  let
    --offerApp = find (\(offer) -> toUpper offer.paymentMethod == vpa) offers
    offerDesc = "" --maybe "" (\b -> b.offerText) offerApp
    PayOptConfig.Config defCon = PayOptConfig.defConfig configPayload
    updatedConfig =
      defCon
        { logoUrl = "upi_logo"
        , primaryText = vpa
        , secondaryText = "Send a collect request to pay"
        , secondaryTextColor = "#5C5C5C"
        , selectionLabel = "Change"
        , selectionType = PayOptConfig.Label
        , lineSeparatorVisibility = GONE
        , inputAreaVisibility = GONE
        , selectionLabelSize = CPUtils.secondaryButtonTextSize configPayload
        , quantText = offerDesc
        , quantVisibility = if (CPUtils.ifOfferVisible configPayload && true) then VISIBLE else GONE
        }
  PayOptConfig.Config updatedConfig
