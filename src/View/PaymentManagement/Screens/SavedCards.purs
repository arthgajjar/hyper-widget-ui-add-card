module View.PaymentManagement.Screens.SavedCards where


import HyperPrelude.External(length, mapWithIndex, null,Maybe(..),Effect,(==),Unit,(<<<),(<>),(-),($))
import HyperPrelude.Internal (Length(..), Orientation(..), Padding(..), PrestoDOM, Visibility(..), background, cornerRadius, height, linearLayout, orientation, padding, visibility, width)
import Data.String (toLower)
import Data.String as StrUtils
import Data.String.Yarn (capWords)
import Engineering.Helpers.Commons (getFormattedIssuerName, getIinFromName)
import PPConfig.Utils as PPConfig
import Service.EC.Types.Instruments (StoredCard(..))
import UI.Components.PaymentOptionsConfig as PayOptConfig
import UI.Components.PaymentOptionsView as PayOptView
import View.PaymentManagement.Controllers.PaymentManagementController (Action(..), SelectedPaymentInstrument(..), State)
import View.PaymentPage.Screens.Utils as PPUtils

view :: forall w. (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
view push state =
  linearLayout
    [ height WRAP_CONTENT
    , width MATCH_PARENT
    , orientation VERTICAL
    , visibility if null state.savedCards
                    then GONE
                    else VISIBLE
    ]
    [ PPUtils.getSectionHeader "Saved Cards" state.configPayload true
    , linearLayout
      [ height WRAP_CONTENT
      , width MATCH_PARENT
      , orientation VERTICAL
      , cornerRadius 8.0
      , background "#FFFFFF"
      , padding (Padding 0 6 0 6)
      ]
      (mapWithIndex (\i card ->
              PayOptView.view
              (push <<< (SelectCard (SavedCard card)))
              (getSavedCardConfig state card (i == length state.savedCards - 1))
              false
              Nothing
          ) state.savedCards)
    ]

getSavedCardConfig :: State -> StoredCard -> Boolean -> PayOptConfig.Config
getSavedCardConfig state sc@(StoredCard card) hideDivider = let
  isSelected = state.selectedPaymentInstrument == Just (SavedCard sc)
  PayOptConfig.Config config = PayOptConfig.defConfig state.configPayload
  formattedIssuerName = getFormattedIssuerName card.cardIssuer
  isDefault = state.defOption == card.cardReference
  cardConfig = config
    { inputAreaVisibility = GONE --if isSelected then VISIBLE else GONE
    , logoUrl = "ic_bank_" <> getIinFromName card.cardIssuer
    , primaryText = if formattedIssuerName == ""
                            then card.cardIssuer <> (capWords $ toLower card.cardType) <> " Card" -- TODO :: verify if this name format is accepted.
                            else (formattedIssuerName <> " " <> (capWords $ toLower card.cardType))
    , primaryTextFont = if isSelected
                         then PPConfig.fontBold state.configPayload
                         else PPConfig.fontRegular state.configPayload
    , primaryLogoVisibility = VISIBLE
    , primaryLogo = ("card_type_" <> StrUtils.toLower card.cardBrand)
    , secondaryText = card.cardNumber
    , secondaryTextColor = "#5C5C5C"
    , selectionType = PayOptConfig.Label
    , labelFont = PPConfig.fontSemiBold state.configPayload
    , selectionLabel = "Delete"
    , displayAreaHeight = V $ if isDefault
                                then (PPConfig.listItemHeight state.configPayload) - 10
                                else PPConfig.listItemHeight state.configPayload
    , tertiaryVisibility = if isDefault
                              then VISIBLE
                              else GONE
    , tertiaryText = "DEFAULT PAYMENT"
    , cvvId = card.cardNumber <> "_id" -- patch added due to repeating id  "default"
    , tertiaryTextFont = PPConfig.fontSemiBold state.configPayload
    , tertiaryTextColor = "#689F38"
    , lineSeparatorVisibility = if hideDivider then GONE else VISIBLE
    }
  in PayOptConfig.Config cardConfig
