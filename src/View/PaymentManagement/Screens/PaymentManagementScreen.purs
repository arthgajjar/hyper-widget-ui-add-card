module View.PaymentManagement.Screens.PaymentManagementScreen where

import HyperPrelude.External (null,Maybe(..),Effect,Unit,(&&),($),negate,(<>),const,(<<<))
import HyperPrelude.Internal (Gravity(..), Length(..), Margin(..), Orientation(..), PrestoDOM, Screen, Visibility(..), color, fontStyle, gravity, height, imageUrl, imageView, linearLayout, margin, orientation, scrollBarY, scrollView, text, textSize, textView, visibility, width)
import Engineering.Helpers.Events (addCustomBackPress)
import PPConfig.Utils as PPConfig
import UI.Components.Popup.Config as PopupConfig
import UI.Components.Popup.View as Popup
import UI.Config as UIConfig
import UI.Utils (popupAnimation)
import View.PaymentManagement.Controllers.PaymentManagementController (Action(..), ScreenInput, ScreenOutput, SelectedPaymentInstrument(..), State, eval, initialState)
import View.Stock.Container.Commons as UICommons
import View.PaymentManagement.Screens.SavedCards as SavedCards
import View.PaymentManagement.Screens.SavedVPAs as SavedVPAs
import View.PaymentManagement.Screens.WalletManagement as Wallets

screen :: ScreenInput -> Screen Action State ScreenOutput
screen input =
  { initialState : (initialState input)
  , name : "PaymentManagementScreen"
  , view
  , globalEvents : []
  , eval
  }

view :: forall w. (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
view push state =
  UICommons.getParentLayout
    parentInput
    (UICommons.PMAction push)
    (paymentsLayout push state)
    (Just $ Popup.view
        (push <<< PopupAction)
        (PopupConfig.Config popupConfig)
        Nothing
        (popupAnimation state.configPayload state.showPopup)
    )
  where
    parentInput =
      { modalView : PPConfig.ifModalView state.configPayload
      , modalHeight : (V 500)
      , modalAnimationTrigger : true
      , toolbarHeader : "My Payments"
      , configPayload : state.configPayload
      , useContainerPadding : true
      , useRelativeLayout : false
      , amount : -1.0
      , orderDescription : ""
      , phoneNumber : ""
      , showAmountBar : false
      , showToolbar : true
      , customerName : ""
      }
    PopupConfig.Config config = UIConfig.deletePopupConfig state.configPayload
    popupConfig = config
      { open = state.showPopup
      , header = case state.selectedPaymentInstrument of
                    Just (SavedCard _)    -> "Remove Saved Card"
                    Just (SavedVPA _)     -> "Remove Saved UPI"
                    Just (SavedWallet _)  -> "Delink Wallet"
                    _                     -> ""
      , content = "Are you sure you want to " <>
                    case state.selectedPaymentInstrument of
                      Just (SavedCard _)    -> "remove this card?"
                      Just (SavedVPA _)     -> "remove this UPI?"
                      Just (SavedWallet _)  -> "delink this wallet?"
                      _                     -> "?"
      }

paymentsLayout :: forall w. (Action -> Effect Unit) -> State -> Array (PrestoDOM (Effect Unit) w)
paymentsLayout push state =
  [ linearLayout
      [ height MATCH_PARENT
      , width MATCH_PARENT
      , visibility if paymentOptionsPresent
                      then VISIBLE
                      else GONE
      ]
      [ noPaymentsView state ]
  , scrollView
      [ height MATCH_PARENT
      , width MATCH_PARENT
      , scrollBarY false
      , visibility if paymentOptionsPresent
                      then GONE
                      else VISIBLE
      , addCustomBackPress push $ const BackPressed
      ]
      [ linearLayout
          [ height WRAP_CONTENT
          , width MATCH_PARENT
          , orientation VERTICAL
          ]
          [ SavedCards.view push state
          , SavedVPAs.view push state
          , Wallets.view push state
          ]
      ]
  ]
  where
    paymentOptionsPresent :: Boolean
    paymentOptionsPresent =
      null state.savedCards
        && null state.savedVpas
        && null state.savedWallets && null state.unlinkedWallets

noPaymentsView :: forall w. State -> PrestoDOM (Effect Unit) w
noPaymentsView state =
  linearLayout
    [ height MATCH_PARENT
    , width MATCH_PARENT
    , orientation VERTICAL
    , gravity CENTER
    ]
    [ imageView
        [ height $ V 80
        , width $ V 111
        , imageUrl "ic_card_gray"
        , margin $ MarginLeft 10
        ]
    , textView
        [ height WRAP_CONTENT
        , width WRAP_CONTENT
        , text "No payment mode saved yet"
        , color $ PPConfig.fontColor state.configPayload
        , textSize $ PPConfig.headerSize state.configPayload
        , fontStyle $ PPConfig.fontSemiBold state.configPayload
        ]
    ]
