module View.PaymentManagement.Screens.SavedVPAs where


import HyperPrelude.External(length, mapWithIndex, null,Maybe(..),Effect,(==),Unit,(<<<),(<>),(-))
import HyperPrelude.Internal (Length(..), Orientation(..), Padding(..), PrestoDOM, Visibility(..), background, cornerRadius, height, linearLayout, orientation, padding, visibility, width)
import Payments.UPI.Utils (getBankNameFromVpa, getVpaImage)
import Service.EC.Types.Instruments (StoredVPA(..))
import UI.Components.PaymentOptionsConfig as PayOptConfig
import UI.Components.PaymentOptionsView as PayOptView
import View.PaymentManagement.Controllers.PaymentManagementController (Action(..), SelectedPaymentInstrument(..), State)
import View.PaymentPage.Screens.Utils as PPUtils
import PPConfig.Utils as PPConfig


view :: forall w. (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
view push state =
  linearLayout
    [ height WRAP_CONTENT
    , width MATCH_PARENT
    , orientation VERTICAL
    , visibility if null state.savedVpas
                    then GONE
                    else VISIBLE
    ]
    [ PPUtils.getSectionHeader "Saved UPI" state.configPayload true
    , linearLayout
      [ height WRAP_CONTENT
      , width MATCH_PARENT
      , orientation VERTICAL
      , cornerRadius 8.0
      , background "#FFFFFF"
      , padding (Padding 0 6 0 6)
      ]
      (mapWithIndex (\i vpa ->
              PayOptView.view
              (push <<< (SelectVPA (SavedVPA vpa)))
              (getSavedVPAConfig state vpa (i == length state.savedVpas - 1))
              false
              Nothing
          ) state.savedVpas)
    ]

getSavedVPAConfig :: State -> StoredVPA -> Boolean -> PayOptConfig.Config
getSavedVPAConfig state sv@(StoredVPA vpa) hideDivider = let
  isSelected = state.selectedPaymentInstrument == Just (SavedVPA sv)
  isDefault = state.defOption == vpa.vpa
  PayOptConfig.Config config = PayOptConfig.defConfig state.configPayload
  vpaConfig = config
    { inputAreaVisibility = GONE
    , logoUrl = getVpaImage vpa.vpa
    , primaryText = "UPI - " <> (getBankNameFromVpa vpa.vpa)
    , primaryTextFont = if isSelected
                          then PPConfig.fontBold state.configPayload
                          else PPConfig.fontRegular state.configPayload
    , secondaryText = vpa.vpa
    , secondaryTextColor = "#000000"
    , selectionType = PayOptConfig.Label
    , labelFont = PPConfig.fontSemiBold state.configPayload
    , selectionLabel = "Delete"
    , cvvId = vpa.id <> "_id" -- patch added due to repeating id  "default"
    , displayAreaHeight = if isDefault
                        then V ((PPConfig.listItemHeight state.configPayload) - 10)
                        else V (PPConfig.listItemHeight state.configPayload)
    , tertiaryVisibility = if isDefault
                              then VISIBLE
                              else GONE
    , tertiaryText = "DEFAULT PAYMENT"
    , tertiaryTextFont = PPConfig.fontSemiBold state.configPayload
    , tertiaryTextColor = "#689F38"
    , lineSeparatorVisibility = if hideDivider then GONE else VISIBLE
    }
  in PayOptConfig.Config vpaConfig
