module View.PaymentManagement.Screens.WalletManagement where


import HyperPrelude.External(null,Effect,Unit,(&&),($),(<>))
import Payments.Wallets.Types (MandateType(..))
import HyperPrelude.Internal (Length(..), Orientation(..), Padding(..), PrestoDOM, Visibility(..), background, cornerRadius, height, linearLayout, orientation, padding, visibility, width)
import View.PaymentManagement.Controllers.PaymentManagementController (Action, State, getWalletNameFromState)
import View.PaymentPage.Screens.Utils as PPUtils
import View.Wallet.Screens.Utils (ActionType(..), getLinkedWalletsView, getUnlinkedWalletsView)

view :: forall w. (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
view push state =
  linearLayout
    [ height WRAP_CONTENT
    , width MATCH_PARENT
    , orientation VERTICAL
    , visibility if null state.unlinkedWallets && null state.savedWallets
                    then GONE
                    else VISIBLE
    ]
    [ PPUtils.getSectionHeader "Wallets" state.configPayload false
    , linearLayout
        [ height WRAP_CONTENT
        , width MATCH_PARENT
        , orientation VERTICAL
        , cornerRadius 8.0
        , background "#FFFFFF"
        , padding (Padding 0 6 0 6)
        ]
        (
          (linkedWalletsView push state)
          <>
          (unlinkedWalletsView push state)
        )
    ]

linkedWalletsView :: forall w. (Action -> Effect Unit) -> State -> Array (PrestoDOM (Effect Unit) w)
linkedWalletsView push state =
  getLinkedWalletsView $
    { storedWallets : state.savedWallets
    , selectedWallet : getWalletNameFromState state
    , disabledWallets : [""]
    , startBtnAnimation : false
    , configPayload : state.configPayload
    , defaultOption : ""
    , defWallet : state.defOption
    , offers : []
    , hideLastDivider : null state.unlinkedWallets
    , pushReceived : PMScreenAction push
    , amount : 1.0
    , delink : true
    , outages : []
    }

unlinkedWalletsView :: forall w. (Action -> Effect Unit) -> State -> Array (PrestoDOM (Effect Unit) w)
unlinkedWalletsView push state =
  getUnlinkedWalletsView $
      { wallets : state.unlinkedWallets
      , disabledWallets : []
      , selectedWalletCode : getWalletNameFromState state
      , configPayload : state.configPayload
      , ifVisible : true
      , useLabel : true
      , defaultOption : ""
      , defWallet : ""
      , offers : []
      , payLaterEligibility : []
      , startButtonAnimation : true
      , hideLastDivider : true
      , pushReceived : PMScreenAction push
      , amount : 1.0
      , mandateType : None --TODO : tobechecked
      , enableSI : true
      , outages : []
      }
