module View.PaymentManagement.Controllers.PaymentManagementController where

import HyperPrelude.External(Maybe(..), fromMaybe,($),(==),class Eq)
import HyperPrelude.Internal(Eval, continue, exit, updateAndExit)
import Payments.Wallets.Types (MandateType(..))
import Payments.Wallets.Types as WUtils
import Remote.Types (ConfigPayload)
import Service.EC.Types.Instruments (StoredCard(..), StoredVPA(..), StoredWallet(..), Wallet(..))
import Service.EC.Types.Instruments as Instruments
import Service.EC.Types.Response (PaymentSourceResp)
import UI.Components.PaymentOptionsController as PayOptions
import UI.Components.Popup.Controller as Popup
import UI.Components.ToolBar.Controller as ToolBar
import UI.Utils (ModalAction)
import UI.Components.Popup.Controller as Popup


type ScreenInput =
  { savedWallets :: Array StoredWallet
  , savedCards :: Array StoredCard
  , savedVpas :: Array StoredVPA
  , configPayload :: ConfigPayload
  , unlinkedWallets :: Array WUtils.Wallet
  , defOptionType :: String
  , defOption :: String
  , gatewayReferenceId :: String
  , paymentSourceResp :: PaymentSourceResp
  }

data SelectedPaymentInstrument
  = SavedCard StoredCard
  | SavedWallet StoredWallet
  | UnlinkedWallet WUtils.Wallet
  | SavedVPA StoredVPA

instance eqSelectedPaymentInstrument :: Eq SelectedPaymentInstrument where
  eq (SavedCard (StoredCard c1)) (SavedCard (StoredCard c2)) = c1.cardToken == c2.cardToken
  eq (SavedVPA (StoredVPA v1)) (SavedVPA (StoredVPA v2)) = v1.vpa == v2.vpa
  eq (SavedWallet (StoredWallet (Wallet w1))) (SavedWallet (StoredWallet (Wallet w2))) = w1.id == w2.id
  eq (UnlinkedWallet (WUtils.Wallet w1)) (UnlinkedWallet (WUtils.Wallet w2)) = w1.code == w2.code
  eq _ _ = false

type State =
  { savedWallets :: Array StoredWallet
  , savedCards :: Array StoredCard
  , savedVpas :: Array StoredVPA
  , configPayload :: ConfigPayload
  , unlinkedWallets :: Array WUtils.Wallet
  , defOptionType :: String
  , defOption :: String
  , gatewayReferenceId :: String
  , paymentSourceResp :: PaymentSourceResp
  , selectedPaymentInstrument :: Maybe SelectedPaymentInstrument
  , showPopup :: Boolean
  , offers :: Array Instruments.Offer
  , mandateType :: MandateType
  }

initialState :: ScreenInput -> State
initialState input =
  { savedWallets : input.savedWallets
  , savedCards : input.savedCards
  , savedVpas : input.savedVpas
  , configPayload : input.configPayload
  , unlinkedWallets : input.unlinkedWallets
  , defOptionType : input.defOptionType
  , defOption : input.defOption
  , gatewayReferenceId : input.gatewayReferenceId
  , paymentSourceResp : input.paymentSourceResp
  , selectedPaymentInstrument : Nothing
  , showPopup : false
  , offers : []
  , mandateType : None
  }

data Action
  = BackPressed
  | ToolBarAction ToolBar.Action
  | OverlayClick ModalAction
  | PopupAction Popup.Action
  | SelectCard SelectedPaymentInstrument PayOptions.Action
  | SelectVPA SelectedPaymentInstrument PayOptions.Action
  | SelectLinkedWallet SelectedPaymentInstrument PayOptions.Action
  | SelectUnlinkedWallet SelectedPaymentInstrument PayOptions.Action
  | OfferPopupAction Popup.Action

data ScreenOutput
  = OnBackPress
  | LinkWallet WUtils.Wallet PaymentSourceResp
  | DeleteCard StoredCard Boolean
  | DelinkWallet StoredWallet Boolean
  | DeleteVPA StoredVPA Boolean

eval :: Action -> State -> Eval Action ScreenOutput State

eval BackPressed state =
  if state.showPopup
      then continue state { showPopup = false, selectedPaymentInstrument = Nothing }
      else exit OnBackPress

eval (ToolBarAction ToolBar.Clicked) state =
  exit OnBackPress

eval (SelectCard card PayOptions.PaymentListItemButtonClick) state = do
    continue state
        { selectedPaymentInstrument = Just card
        , showPopup = true
        }

eval (SelectLinkedWallet wallet PayOptions.PaymentListItemButtonClick) state =
  continue state
    { selectedPaymentInstrument = Just wallet
    , showPopup = true
    }

eval (SelectUnlinkedWallet w@(UnlinkedWallet wallet)
        PayOptions.PaymentListItemButtonClick) state =
          updateAndExit state { selectedPaymentInstrument = Just w }
            $ LinkWallet wallet state.paymentSourceResp

eval (SelectVPA vpa PayOptions.PaymentListItemButtonClick) state =
  continue state
    { selectedPaymentInstrument = Just vpa
    , showPopup = true
    }

eval (PopupAction action) state =
  case action of

    Popup.ButtonOneClick -> do
      case state.selectedPaymentInstrument of

        Just (SavedCard card@(StoredCard sc)) ->
          updateAndExit state { showPopup = false }
            $ DeleteCard card (sc.cardReference == state.defOption)

        Just (SavedWallet wallet@(StoredWallet (Wallet w))) ->
          updateAndExit state { showPopup = false }
            $ DelinkWallet wallet (w.wallet == Just state.defOption)

        Just (SavedVPA vpa@(StoredVPA sv)) ->
          updateAndExit state { showPopup = false }
            $ DeleteVPA vpa (sv.vpa == state.defOption)

        _ -> continue state

    _ -> continue state { showPopup = false, selectedPaymentInstrument = Nothing }

eval action state = continue state

getWalletNameFromState :: State -> String
getWalletNameFromState state =
  case state.selectedPaymentInstrument of
    Just (UnlinkedWallet (WUtils.Wallet w)) -> w.code
    Just (SavedWallet (StoredWallet (Wallet w))) -> fromMaybe "" w.wallet
    _ -> ""
