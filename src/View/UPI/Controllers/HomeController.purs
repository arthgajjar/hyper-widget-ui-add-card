module View.UPI.Controllers.HomeController where

import HyperPrelude.External((!!),fromMaybe, Maybe(..),Effect,(==),class Eq,Unit,not,($),(/=))
import HyperPrelude.Internal (Eval, Props, continue, exit, updateAndExit, afterRender)
import Data.Foldable (elem)
import PPConfig.Utils (getDisableUpiApps)
import Engineering.Helpers.Commons(PaymentOffer(..))
import PrestoDOM (Eval, Props, continue, exit, updateAndExit, afterRender)
import Remote.Types (ConfigPayload)
import Service.EC.Types.Instruments as Instruments
import Service.UPIIntent.Types(UPIApp(..))
import UI.Components.PaymentOptionsController as PaymentOptions
import UI.Components.Popup.Controller as Popup
import UI.Components.ToolBar.Controller as ToolBar
import UI.Utils (ModalAction(..))
import Foreign.Class (class Decode, class Encode)
import Presto.Core.Utils.Encoding (defaultDecode, defaultEncode)
import Data.Generic.Rep (class Generic)
import Payments.NetBanking.Utils (Bank(..))

newtype StoredUpiVpa = StoredUpiVpa {
  bankImage:: String,
  bankName:: String,
  account:: String,
  vpa:: String
}
derive instance storedUpiVpaGeneric :: Generic StoredUpiVpa _
instance decodeStoredUpiVpa :: Decode StoredUpiVpa where decode = defaultDecode
instance encodeStoredUpiVpa :: Encode StoredUpiVpa where encode = defaultEncode

type ScreenInput =
  { configPayload :: ConfigPayload
  , upiApps :: Array UPIApp
  , savedVpas :: Array Instruments.StoredVPA
  , savedUpiVpas :: Array StoredUpiVpa
  , offers :: Array PaymentOffer
  , defUPI :: String
  , amount :: Number
  , mid :: String
  , phoneNumber :: String
  , orderDesc :: String
  , showOnlyUPIApps :: Boolean
  , customerName :: String
  , allBanks :: Array Bank
  , allCards :: Array Instruments.StoredCard
  , allWallets :: Array Instruments.StoredWallet
  }

data UPIInstrument
  = UPIApplication UPIApp
  | StoredVPA Instruments.StoredVPA

derive instance eqUPIInstrument :: Eq UPIInstrument

type State =
  { configPayload :: ConfigPayload
  , upiApps :: Array UPIApp
  , savedVpas :: Array Instruments.StoredVPA
  , disabledUpiApps :: Array String
  , selectedInstrument :: Maybe UPIInstrument
  , entry :: Boolean
  , buttonAnim :: Boolean
  , offers :: Array PaymentOffer
  , defaultOption :: String
  , defaultApp :: String
  , defUPI :: String
  , amount :: Number
  , phoneNumber :: String
  , orderDesc :: String
  , showOnlyUPIApps :: Boolean
  , showOfferDescPopup :: Boolean
  , activeOfferDesc :: String
  , customerName :: String
  }

initialState :: ScreenInput -> State
initialState input =
  { configPayload : input.configPayload
  , upiApps : input.upiApps
  , savedVpas : input.savedVpas
  , disabledUpiApps : fromMaybe [] (getDisableUpiApps input.configPayload)
  , selectedInstrument : Nothing
  , entry : true
  , buttonAnim : false
  , offers : input.offers
  , defaultOption : ""
  , defaultApp : ""
  , defUPI : input.defUPI
  , amount : input.amount
  , phoneNumber : input.phoneNumber
  , orderDesc : input.orderDesc
  , showOnlyUPIApps : input.showOnlyUPIApps
  , showOfferDescPopup : false
  , activeOfferDesc : ""--(fromMaybe getDummyPaymentOffer $ input.offers !! 0).offerDescription
  , customerName : input.customerName
  }

data ScreenOutput
  = PayWithApp String Boolean
  | PayWithSavedUpi String Boolean
  | OnBackPress
  | AddUpi
  | Switch String
  | SwitchNav String
  | ScanQRCode

data Action
  = BackPress
  | ToolBarAction ToolBar.Action
  | AddUpiId PaymentOptions.Action
  | UPIAppAction UPIApp PaymentOptions.Action
  | SavedVPAAction Instruments.StoredVPA PaymentOptions.Action
  | OverlayClick ModalAction
  | OfferPopupAction Popup.Action

eval :: Action -> State -> Eval Action ScreenOutput State

eval BackPress state =
  if state.showOfferDescPopup then
    continue state {showOfferDescPopup = false}
  else let
    updatedState = state {entry = not state.entry} in
    updateAndExit updatedState $ OnBackPress

eval (ToolBarAction ToolBar.Clicked) state = let
  updatedState = state {entry = not state.entry} in
  updateAndExit updatedState $ OnBackPress

eval (OverlayClick ClickedOutside) state = let
  updatedState = state {entry = not state.entry} in
  updateAndExit updatedState $ OnBackPress

eval (UPIAppAction app@(UPIApp upiApp) action) state = case action of
  PaymentOptions.PaymentListItemSelection ->
    if (isUpiAppDisabled upiApp.appName state.disabledUpiApps)
      then continue state
      else continue $ selectInstrument state $ Just $ UPIApplication app

  PaymentOptions.PaymentListAction PaymentOptions.ButtonClicked ->
    if (isUpiAppDisabled upiApp.appName state.disabledUpiApps)
      then continue state
      else let newState = state {buttonAnim = true } in
        updateAndExit newState $ PayWithApp upiApp.packageName (state.defaultApp /= "")

  PaymentOptions.QuantLayoutClick -> continue state {showOfferDescPopup = true}

  _ -> continue state

eval (SavedVPAAction sVPA@(Instruments.StoredVPA storedVPA) action) state = case action of
  PaymentOptions.PaymentListItemSelection ->
    continue $ selectInstrument state $ Just $ StoredVPA sVPA

  PaymentOptions.PaymentListAction PaymentOptions.ButtonClicked ->
    updateAndExit (state { buttonAnim = true })
      $ PayWithSavedUpi storedVPA.vpa (state.defaultOption /= "")

  _ -> continue state

eval (AddUpiId PaymentOptions.PaymentListItemSelection) state = exit AddUpi

eval (OfferPopupAction action) state = case action of
  Popup.OverlayClick -> continue state {showOfferDescPopup = false}
  Popup.HeaderIconClick -> continue state {showOfferDescPopup = false}
  Popup.ButtonOneClick -> continue state {showOfferDescPopup = false}
  _ -> continue state

eval action state = continue state

overrides :: String -> (Action -> Effect Unit) -> State -> Props (Effect Unit)
overrides str push state = []

isUpiAppDisabled :: String -> Array String -> Boolean
isUpiAppDisabled app disabledUpiApps = app `elem` disabledUpiApps

selectInstrument :: State -> Maybe UPIInstrument -> State
selectInstrument state instrument =
  if instrument == state.selectedInstrument then
    state { selectedInstrument = Nothing }
  else
    state { selectedInstrument = instrument }
