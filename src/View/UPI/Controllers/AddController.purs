module View.UPI.Controllers.AddController where

import HyperPrelude.External(Either(..),Effect,launchAff_,delay,Milliseconds(..),liftEffect,(*),Unit,not,($),(==),const,(||),(-),bind,discard,pure)
import HyperPrelude.Internal (Eval, Props, afterRender, continue, continueWithCmd, exit, updateAndExit)
import Engineering.Helpers.Commons (PaymentOffer,log)
import Engineering.Helpers.Events (attachTimer)
import JBridge as JBridge
import Remote.Types (ConfigPayload)
import PPConfig.Utils (doVerifyVpa) as CPUtils
import UI.Components.EditText.Controller as EditText
import UI.Components.PrimaryButton.Controller as PrimaryButton
import UI.Components.ToolBar.Controller as ToolBar
import UI.Utils (ModalAction(..), setText')
import UI.Components.Popup.Controller as Popup
import Payments.NetBanking.Utils (Bank(..))
import Service.EC.Types.Instruments as Instruments

type ScreenInput =
  { configPayload :: ConfigPayload
  , startCollect :: Boolean
  , collectStatus :: String
  , collectTTL :: Int -- seconds
  , customerName :: String
  , isValidVpa :: Boolean
  , message :: String
  , vpa :: String
  , amount :: Number
  , phoneNumber :: String
  , useAsWidget :: Boolean
  , orderDesc :: String
  , paymentCustomerName :: String
  , mid :: String
  , allBanks :: Array Bank
  , allCards :: Array Instruments.StoredCard
  , allWallets :: Array Instruments.StoredWallet
  , savedVPAs ::   Array Instruments.StoredVPA
  , outages :: Array String
  , offers :: Array PaymentOffer
  }

data ScreenOutput
  = SendWebCollectAt String Boolean Boolean
  | OnBackPress
  | PAYMENT_SUCCESS (Either String String)
  | PAYMENT_TIME_OUT
  | PAYMENT_OTHER_ERROR (Either String String)
  | VerifyVpa String
  | Switch String
  | SwitchNav String
  | OnGuestLogin
  | SendWebCollectAtRedirect String Boolean
  | PayUsingNB String
  | PayUsingWallet String String Boolean Boolean

data Action
  = EditTextAction EditText.Action
  | ToolBarAction ToolBar.Action
  | PrimaryButtonAction PrimaryButton.Action
  | BackPress
  | UseUpiIntent
  | SaveUpiId
  | UPIScreenRendered String
  | ContinueCommand
  | TimerRun Int
  | OverlayClick ModalAction
  | OfferPopupAction Popup.Action

type State =
  { vpa :: String
  , upiIntentFlow :: Boolean
  , timeLeft :: Int
  , startCollect :: Boolean
  , animationDuration :: Int
  , entry :: Boolean
  , buttonAnim :: Boolean
  , collectStatus :: String
  , collectTTL :: Int
  , configPayload :: ConfigPayload
  , saveUpiId :: Boolean
  , showLoader :: Boolean
  , isValidVpa :: Boolean
  , customerName :: String
  , errorMessage :: String
  , amount :: Number
  , phoneNumber :: String
  , isWidget :: Boolean
  , orderDesc :: String
  , focus :: Boolean
  , paymentCustomerName :: String
  }

initialState :: ScreenInput -> State
initialState input =
  { vpa : input.vpa
  , upiIntentFlow : false
  , timeLeft : input.collectTTL
  , startCollect : input.startCollect
  , animationDuration : input.collectTTL * 1000
  , entry : true
  , buttonAnim : false
  , collectStatus : input.collectStatus
  , collectTTL : input.collectTTL
  , configPayload : input.configPayload
  , saveUpiId : false
  , showLoader : false
  , isValidVpa : verifyVpaApiCallResponse
  , customerName : input.customerName
  , errorMessage : input.message
  , amount : input.amount
  , phoneNumber : input.phoneNumber
  , isWidget : input.useAsWidget
  , orderDesc : input.orderDesc
  , focus : not input.startCollect
  , paymentCustomerName : input.paymentCustomerName
  }
  where
  verifyVpaApiCallResponse = if CPUtils.doVerifyVpa input.configPayload
                                then input.isValidVpa
                                else true

eval :: Action -> State -> Eval Action ScreenOutput State

eval (ToolBarAction ToolBar.Clicked) state =
  let updatedState = state {entry = not state.entry} in
    updateAndExit updatedState $ OnBackPress

eval (OverlayClick ClickedOutside) state =
    if state.startCollect
        then continue state
        else let updatedState = state {entry = not state.entry} in
             updateAndExit updatedState $ OnBackPress

eval BackPress state =
  let updatedState = state {entry = not state.entry} in
      updateAndExit updatedState $ OnBackPress

eval (EditTextAction action) state =
    case action of
        (EditText.OnChanged text) -> do
          let changeInVpa = not (text == state.vpa)
          continue state { vpa = text
                        , isValidVpa = if changeInVpa
                                        then not $ CPUtils.doVerifyVpa state.configPayload
                                        else state.isValidVpa
                        , errorMessage = if changeInVpa then "" else state.errorMessage
                        }

        (EditText.EditTextButtonClick) -> updateAndExit state {showLoader = true} $ VerifyVpa state.vpa
        _ -> continue state

eval (PrimaryButtonAction (PrimaryButton.Clicked)) state =
    if state.isValidVpa
        then
            let newState = state {buttonAnim = true, focus = false} in
            updateAndExit newState $ SendWebCollectAt state.vpa state.saveUpiId state.upiIntentFlow
        else continue state

eval (UseUpiIntent) state =
    if state.startCollect
        then continue state
        else continue state { upiIntentFlow = not state.upiIntentFlow }

eval (SaveUpiId) state = continue state { saveUpiId = not state.saveUpiId }

eval (TimerRun _) state = do
    if state.timeLeft == 1 || state.timeLeft == 0
        then  updateAndExit (state {timeLeft = 0}) $ PAYMENT_TIME_OUT
        else  continue state {timeLeft = (state.timeLeft -1)}

eval (UPIScreenRendered id) state = do
  case state.collectStatus of
    "Payment Successfull" -> exit $ (log "PaySucc" PAYMENT_SUCCESS $ Right "")
    "Payment Timed Out" -> exit $ (log "PayFail" PAYMENT_TIME_OUT)

    _ -> do
        let idx = if state.startCollect then "" else id
        continueWithCmd state [
            do
            _ <- launchAff_ do
                _ <- delay (Milliseconds 300.0)
                liftEffect $ JBridge.requestKeyboardShow idx
                liftEffect $ setText' id state.vpa
            pure $ ContinueCommand
        ]

eval _ state = continue state

overrides :: String -> (Action -> Effect Unit) -> State -> Props (Effect Unit)
overrides "MainLayout" push state =
  [ afterRender push $ const (UPIScreenRendered (JBridge.getNewIDWithTag JBridge.VPA ))]

overrides "LoaderLayout" push state = (if state.startCollect then [attachTimer (state.collectTTL * 1000) 1000 push TimerRun] else [])
overrides _ push state = []
