module View.UPI.Screens.AddScreen where


import HyperPrelude.External(decimal, toStringAs,Maybe(..),fromMaybe,Effect,(-),Unit,(/),(*),(&&),(#),(==),($),(<>),(||),const,not,(/=),(<<<),negate,Unit,unit)
import HyperPrelude.Internal(PrestoDOM, Screen, letterSpacing, weight, stroke, alignParentBottom, background, clickable, color, cornerRadius, fontStyle, gravity, height, imageUrl, imageView, linearLayout, margin, onClick, orientation, padding, relativeLayout, singleLine, text, textSize, textView, visibility, width)
import Data.Newtype (unwrap)
import Data.String (toLower)
import Data.String as String
import Engineering.Helpers.Events (addCustomBackPress)
import JBridge as JBridge
import PPConfig.Utils as CPUtils
import PrestoDOM.Animation as PrestoAnim
import PrestoDOM.Types.DomAttributes (Gravity(..), Length(..), Margin(..), Orientation(..), Padding(..), Visibility(..))
import Remote.Types (ConfigPayload(..), InputField(..))
import UI.Components.EditText.Config as EditTextConfig
import UI.Components.EditText.View as EditText
import UI.Components.PrimaryButton.Config as PrimaryButtonConfig
import UI.Components.PrimaryButton.View as PrimaryButton
import UI.Config as UIConfig
import UI.Constant.Str.Default as STR
import UI.Utils (cardPadding, contentMargin, loaderAnim, multipleLine, screenWidth, sectionMargin, wrapInScroll)
import UI.Validation as Validation
import View.UPI.Controllers.AddController (Action(..), ScreenInput, ScreenOutput, State, eval, initialState, overrides)
import View.Stock.Container.Commons as UICommons
import Data.String.Yarn (capWords)

screen :: ScreenInput -> Screen Action State ScreenOutput
screen input =
  { initialState: (initialState input)
  , name: "VpaScreen"
  , view
  , globalEvents: []
  , eval: eval
  }

view :: ∀ w. (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
view push state =
  UICommons.getParentLayout
    parentInput
    (UICommons.AddUpiAction push)
    (upiLayout push state)
    (btnLayout)
  where
  btnAtBottom = CPUtils.isBtnAtBottom state.configPayload && not state.isWidget

  btnLayout =
    if btnAtBottom
      && not state.startCollect
      && (state.upiIntentFlow || state.isValidVpa) then
      Just $ proceedButton push state
    else
      Just $ loaderView push state

  parentInput =
    { modalView: (CPUtils.ifModalView state.configPayload)
    , modalHeight: MATCH_PARENT
    , modalAnimationTrigger: state.entry
    , toolbarHeader: "UPI"
    , configPayload: state.configPayload
    , useContainerPadding: true
    , useRelativeLayout: false
    , amount: state.amount
    , showToolbar: true
    , phoneNumber : state.phoneNumber
    , showAmountBar : true
    , orderDescription : state.orderDesc
    , customerName : state.paymentCustomerName
    }

upiLayout :: ∀ w. (Action -> Effect Unit) -> State -> Array (PrestoDOM (Effect Unit) w)
upiLayout push state =
  wrap
  [ linearLayout (
      [ height MATCH_PARENT
      , width MATCH_PARENT
      , orientation VERTICAL
      , addCustomBackPress push (const BackPress)
      , contentMargin state.configPayload
      ] <> (overrides "MainLayout" push state)) $
      [ addUICard
          [ vpaView push state
          , vpaMetaData state
          , useIntentSelector push state
          , paymentMetaData push state
          ]
      , btnLayout
      ]
  ]
  where
  lang = state.configPayload # unwrap # _.language
  addUICard c =
    if state.isWidget
    then
      linearLayout
        [ width MATCH_PARENT
        , height MATCH_PARENT
        , orientation VERTICAL
        , cardPadding state.configPayload
        ] c
    else
      linearLayout
        [ width MATCH_PARENT
        , height MATCH_PARENT
        , sectionMargin state.configPayload
        ]
        [ UICommons.uiCardLayout state.configPayload c ]

  btnAtBottom = CPUtils.isBtnAtBottom state.configPayload && not state.isWidget
  wrap a = if btnAtBottom then a else wrapInScroll a
  btnLayout =
    if btnAtBottom then
      linearLayout [ height $ V 0 ] []
    else
      if not state.startCollect
        && (state.upiIntentFlow || state.isValidVpa) then
        proceedButton push state
      else
        linearLayout [ height $ V 0 ] []

vpaView :: ∀ w. (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
vpaView push state =
  linearLayout
    [ height MATCH_PARENT
    , width MATCH_PARENT
    , orientation VERTICAL
    ] $
      (if pconf.useMaterialView
      then []
      else [ inputLabel (STR.getUPIText lang) state $ CPUtils.labelFontColor state.configPayload]) <>
      (if state.startCollect
        then [ disabledTextView
              state.vpa
              (EditTextConfig.Config updatedConfig) ]
      else [ EditText.view
              (push <<< EditTextAction)
              (EditTextConfig.Config updatedConfig) ]) <>
      [ vpaMetaData state ] <>
      if CPUtils.showVPAMetadata state.configPayload
        then [ inputLabel (STR.getUpiCollectReq lang) state (if verifyVpaFeature then "#909090" else CPUtils.fontColor state.configPayload) ]
        else []
  where
  lang =  fromMaybe "" (state.configPayload # unwrap # _.language)
  disabledTextView vpa (EditTextConfig.Config config)=
    linearLayout (
      [ width config.cardWidth
      , height config.cardHeight
      , margin
          if config.useMaterialView
          then EditText.addTopMargin config.margin
          else config.margin
      , cornerRadius config.cornerRadius
      ] <> if config.stroke == "" then [] else [stroke config.stroke])
    [ linearLayout
        [ width MATCH_PARENT
        , height $ V $ EditText.getHeight config.cardHeight
        , orientation HORIZONTAL
        , gravity CENTER
        , margin config.inputTextMargin
        ]
        [ textView
            [ width $ V 0
            , height MATCH_PARENT
            , color config.textColor
            , weight 1.0
            , background config.background
            , gravity config.gravity
            , textSize config.textSize
            , fontStyle config.editTextFont
            , padding config.editTextPadding
            , letterSpacing config.letterSpacing
            , text vpa
            ]
        ]
    ]

  vpaValidationOp = Validation.validateData Validation.VPA state.vpa

  ConfigPayload configPayload = state.configPayload

  verifyVpaFeature = CPUtils.doVerifyVpa state.configPayload

  EditTextConfig.Config pconf = UIConfig.editTextConfig state.configPayload

  InputField inputFieldConfig = configPayload.inputField

  cmnConfig =
    pconf
      { hint = STR.getUsernameHint lang
      , focus = state.focus
      , headerText = (STR.getUPIText lang)
      , inputTypeI = 33 -- emailInputType (prevents capitilization of first character)
      , iconHeight = V 25
      , iconText = "Verify"
      , iconTextSize = (CPUtils.fontSizeSmall state.configPayload)
      , editTextId = JBridge.getNewIDWithTag JBridge.VPA
      , iconTextVisibility =
        if verifyVpaFeature && vpaValidationOp && not state.showLoader && not state.isValidVpa then
          VISIBLE
        else
          GONE
      , showLoader = state.showLoader
      , progressBarSize = V 26
      , iconVisibility =
        if state.isValidVpa && verifyVpaFeature then
          VISIBLE
        else
          GONE
      , iconUrl = "approved_tick_green"
      , inputTextMargin = if (toLower inputFieldConfig.type) == "boxed" then (Margin 12 0 12 0) else pconf.inputTextMargin
      }
  updatedConfig =
    if CPUtils.showVPAError state.configPayload
    then cmnConfig { errorVisibility = VISIBLE -- ^ made errorText visible, and will only change errorMsg text, this will make sure that layout is not disturbed when error is shown
                   , errorMsg =
                      if vpaValidationOp || state.vpa == ""
                        then ""
                        else "Please enter a valid UPI ID"
                   }
    else cmnConfig { margin = (Margin 0 0 0 8)
                   , lineSeparatorFocusedColor =
                      if (not state.isValidVpa && state.errorMessage == "Invalid UPI ID")
                      then "#d30019"
                      else "#c7c7c7"
                   }
vpaMetaData :: ∀ w. State -> PrestoDOM (Effect Unit) w
vpaMetaData state =
  linearLayout
  [ height MATCH_PARENT
  , width MATCH_PARENT
  , gravity CENTER_VERTICAL
  , margin if (state.isValidVpa || state.errorMessage /= "") then (Margin 0 0 0 12) else (Margin 0 0 0 0)
  , visibility if ((CPUtils.showVPAMetadata state.configPayload) && (CPUtils.doVerifyVpa state.configPayload)) then VISIBLE else GONE
  ]
  [ imageView
    [ height (V $ CPUtils.fontSize state.configPayload)
    , width (V $ CPUtils.fontSize state.configPayload)
    , margin (Margin 0 0 8 0)
    , imageUrl "tick"
    , visibility if state.isValidVpa then VISIBLE else GONE
    ]
  , textView
    [ width MATCH_PARENT
    , height MATCH_PARENT
    , text
        ( if state.isValidVpa then
            capWords $ toLower state.customerName
          else
            if not state.isValidVpa && state.errorMessage == "" then
              (STR.getUpiCollectReq lang)
            else
              state.errorMessage
        )
    , fontStyle $ CPUtils.fontSemiBold state.configPayload
    , visibility
        if (state.isValidVpa || state.errorMessage /= "")
        then VISIBLE
        else GONE
    , textSize (CPUtils.fontSize state.configPayload)
    , color
        if state.isValidVpa then "#1C873B"
        else "#d30019"
    ]
  ]
  where
  lang = fromMaybe "" (state.configPayload # unwrap # _.language)

useIntentSelector :: ∀ w. (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
useIntentSelector push state =
  linearLayout
    [ width MATCH_PARENT
    , height MATCH_PARENT
    , orientation HORIZONTAL
    , visibility GONE -- upi Intent Checkbox
    , onClick push $ const (UseUpiIntent)
    ]
    [ imageView
        [ imageUrl (if state.upiIntentFlow then "checkbox1" else "checkbox")
        , height $ V 24
        , width $ V 24
        , gravity CENTER
        ]
    , textView
        [ text $ STR.getUpiIntent lang
        , fontStyle $ CPUtils.fontRegular state.configPayload
        , height MATCH_PARENT
        , width MATCH_PARENT
        , gravity CENTER_VERTICAL
        , textSize (CPUtils.fontSize state.configPayload)
        , color "#898989"
        , margin (Margin 5 0 0 0)
        ]
    ]
  where
  lang = fromMaybe "" (state.configPayload # unwrap # _.language)

paymentMetaData :: ∀ w. (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
paymentMetaData push state =
  if state.collectStatus == "" then
    saveOptionView
  else
    payStatusView
  where
  saveOptionView =
    linearLayout
      [ width MATCH_PARENT
      , height MATCH_PARENT
      , orientation HORIZONTAL
      , gravity CENTER
      , onClick push $ const (SaveUpiId)
      , visibility if (CPUtils.showVPASaveOption state.configPayload) then VISIBLE else GONE
      , margin (Margin 0 8 0 8)
      ]
      [ imageView
          [ width (V 18)
          , height (V 18)
          , imageUrl if state.saveUpiId then "checkbox1" else "checkbox"
          , margin (Margin 0 0 5 0)
          ]
      , textView
          [ fontStyle $ CPUtils.fontRegular state.configPayload
          , height MATCH_PARENT
          , width MATCH_PARENT
          , gravity CENTER_VERTICAL
          , textSize (CPUtils.fontSizeSmall state.configPayload)
          , color "#898989"
          , text "Save this UPI ID for faster payments"
          ]
      ]

  payStatusView =
    textView
      [ width MATCH_PARENT
      , height MATCH_PARENT
      , text state.collectStatus
      , textSize (CPUtils.fontSize state.configPayload)
      , background (CPUtils.highlightMessageBackground state.configPayload)
      , padding (CPUtils.highlightMessagePadding state.configPayload)
      , color (CPUtils.highlightMessageTextColor state.configPayload)
      , cornerRadius 4.0
      , margin (Margin 0 60 0 0)
      , fontStyle (CPUtils.highlightMessageTextFontFace state.configPayload)
      ]

loaderView :: ∀ w. (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
loaderView push state =
  relativeLayout
    ( [ width MATCH_PARENT
      , height $ V 50
      , visibility if state.startCollect then VISIBLE else GONE
      , background "#ffffff"
      , alignParentBottom "true,-1"
      ]
        <> overrides "LoaderLayout" push state
    )
    [ PrestoAnim.animationSet
        [ loaderAnim (-(screenWidth unit)) 0 state.animationDuration state.startCollect ]
        $ linearLayout
            [ height MATCH_PARENT
            , width MATCH_PARENT
            , background ("#33" <> (String.drop 1 (CPUtils.primaryColor state.configPayload)))
            , visibility if state.startCollect then VISIBLE else GONE
            ]
            []
    , linearLayout
        [ height MATCH_PARENT
        , width MATCH_PARENT
        , margin (Margin 16 0 16 0)
        ]
        [ textView
            [ height MATCH_PARENT
            , width MATCH_PARENT
            , text "This page will expire in "
            , color "#000000"
            , gravity CENTER_VERTICAL
            , textSize (CPUtils.fontSizeSmall state.configPayload)
            , fontStyle $ CPUtils.fontRegular state.configPayload
            ]
        , textView
            [ height MATCH_PARENT
            , width MATCH_PARENT
            , text $ minutes' <> ":" <> seconds'
            , gravity CENTER_VERTICAL
            , color $ CPUtils.primaryColor state.configPayload
            , textSize (CPUtils.fontSizeSmall state.configPayload)
            , fontStyle $ CPUtils.fontBold state.configPayload
            ]
        , textView
            [ height MATCH_PARENT
            , width MATCH_PARENT
            , text " min."
            , color "#000000"
            , gravity CENTER_VERTICAL
            , textSize (CPUtils.fontSizeSmall state.configPayload)
            , fontStyle $ CPUtils.fontRegular state.configPayload
            ]
        ]
    ]
  where
  totalseconds = state.timeLeft

  minutes = secsToMin (totalseconds)

  seconds = minutesMinusSeconds minutes totalseconds

  minStr = toStringAs decimal minutes

  secStr = toStringAs decimal seconds

  minutes' = if String.length minStr == 2 then minStr else ("0" <> minStr)

  seconds' = if String.length secStr == 2 then secStr else ("0" <> secStr)

secsToMin :: Int -> Int
secsToMin secs = secs / 60

minutesMinusSeconds :: Int -> Int -> Int
minutesMinusSeconds minutes seconds = seconds - (minutes * 60)

inputLabel :: ∀ w. String -> State -> String -> PrestoDOM (Effect Unit) w
inputLabel label state textColor=
  textView
    [ height MATCH_PARENT
    , width MATCH_PARENT
    , text label
    , fontStyle $ CPUtils.fontRegular state.configPayload
    , textSize $ CPUtils.fontSizeSmall state.configPayload
    , margin (MarginBottom 8)
    , singleLine false
    , multipleLine "true"
    , color textColor
    ]

proceedButton :: ∀ w. (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
proceedButton push state =
  linearLayout
    ( [ width MATCH_PARENT
      , height MATCH_PARENT
      ]
        <> if btnAtBottom then
            [ background $ CPUtils.btnBackground state.configPayload
            , alignParentBottom "true,-1"
            ]
          else
            []
    )
    [ PrimaryButton.view
        (push <<< PrimaryButtonAction)
        (PrimaryButtonConfig.Config bConf)
    ]
  where
  vpaValidationOp = Validation.validateData Validation.VPA state.vpa
  lang = fromMaybe "" (state.configPayload # unwrap # _.language)
  ConfigPayload configPayload = state.configPayload
  btnAtBottom = CPUtils.isBtnAtBottom state.configPayload && not state.isWidget
  PrimaryButtonConfig.Config b = UIConfig.primaryButtonConfig state.configPayload
  bConf =
    b
      { text = STR.getProceed lang
      , startAnimation = state.buttonAnim
      , alpha = if vpaValidationOp then 1.0 else 0.5
      , isClickable = vpaValidationOp
      }
