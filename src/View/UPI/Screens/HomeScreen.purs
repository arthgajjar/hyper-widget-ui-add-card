module View.UPI.Screens.HomeScreen where


import HyperPrelude.External(filter, find, length, mapWithIndex, (!!), last, null, elem,Maybe(..), fromMaybe, isJust, maybe,Effect,(#),(<>),Unit,(/=),(==),($),not,(<<<),const,map)
import HyperPrelude.Internal(Gravity(..), Length(..), Margin(..), Orientation(..), Padding(..), PrestoDOM, Prop, Screen, VDom, Visibility(..), background, cornerRadius, height, linearLayout, margin, orientation, padding, scrollBarY, scrollView, translationZ, visibility, weight, width)
import Data.Newtype (unwrap)
import Data.Number.Format (toString)
import Data.String (toUpper, split, Pattern(..), stripPrefix)
import Engineering.Helpers.Commons (PaymentOffer)
import Engineering.Helpers.Events (addCustomBackPress)
import PPConfig.Utils as CPUtils
import PaymentPageConfig as PPConfig
import Payments.Core.Commons (getDummyPaymentOffer)
import Payments.UPI.Utils (getAppNameFromVpa, getVpaImage)
import Remote.Types (PaymentOptions(..), ConfigPayload)
import Service.EC.Types.Instruments as Instruments
import Service.UPIIntent.Types (UPIApp(..)) as UPIIntentTypes
import UI.Components.PaymentOptionsConfig as PaymentOptionsConf
import UI.Components.PaymentOptionsView as PaymentOptionsComp
import UI.Constant.Str.Default as STR
import UI.Utils (boolToVisibility)
import UI.Utils as UIUtils
import View.UPI.Controllers.HomeController (Action(..), UPIInstrument(..), ScreenInput, ScreenOutput, State, eval, initialState, isUpiAppDisabled, overrides)
import View.Stock.Container.Commons as UICommons
import View.PaymentPage.Screens.Utils as PPUtils

screen :: ScreenInput -> Screen Action State ScreenOutput
screen input =
  { initialState: (initialState input)
  , view
  , name: "UpiScreen"
  , globalEvents: []
  , eval: eval
  }

view :: ∀ w. (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
view push state =
  UICommons.getParentLayout
    parentInput
    action
    (upiLayout push state)
    (Just $ UICommons.offerPopup action state.configPayload state.activeOfferDesc state.showOfferDescPopup)
  where
  action = UICommons.UpiHomeAction push
  parentInput =
    { modalView: (CPUtils.ifModalView state.configPayload)
    , modalHeight: MATCH_PARENT
    , modalAnimationTrigger: state.entry
    , toolbarHeader: "UPI"
    , configPayload: state.configPayload
    , useContainerPadding: true
    , useRelativeLayout: false
    , amount: state.amount
    , showToolbar: true
    , phoneNumber : state.phoneNumber
    , showAmountBar : true
    , orderDescription : state.orderDesc
    , customerName : state.customerName
    }

upiLayout :: ∀ w. (Action -> Effect Unit) -> State -> Array (PrestoDOM (Effect Unit) w)
upiLayout push state =
  [ scrollView (
      [ height $ V 0
      , weight 1.0
      , width MATCH_PARENT
      , scrollBarY false
      , addCustomBackPress push (const BackPress)
      ] <> overrides "MainLayout" push state
    )
    [ linearLayout
        [ width MATCH_PARENT
        , height MATCH_PARENT
        , orientation VERTICAL
        , UIUtils.contentMargin state.configPayload
        , padding $ PaddingBottom $ UIUtils.sectionSpace state.configPayload
        ]
        [ upiAppsLayout push state
        , addUpiButtonView push state
        , savedVpaLayout push state
        ]
    ]
  ]

addUpiButtonView :: ∀ w. (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
addUpiButtonView push state =
  if state.showOnlyUPIApps then linearLayout [width $ V 0, height $ V 0] []
  else
    linearLayout
      [ width MATCH_PARENT
      , height MATCH_PARENT
      , orientation VERTICAL
      , UIUtils.sectionMargin state.configPayload
      ]
      ( UICommons.addCurvedWrapper
          state.configPayload
          [ PaymentOptionsComp.view
              (push <<< AddUpiId)
              (PaymentOptionsConf.Config addUpiButtonConfig)
              false
              Nothing
          ]
      )
  where
  lang = fromMaybe "" (state.configPayload # unwrap # _.language)

  PaymentOptionsConf.Config config = PaymentOptionsConf.defConfig state.configPayload

  addUpiButtonConfig =
    config
      { inputAreaVisibility = GONE
      , radioButtonIconUrl = "right_arrow"
      , logoUrl = "green_plus"
      , secondaryTextVisibility = GONE
      , lineSeparatorVisibility = GONE
      , primaryText = STR.getAddUpiText lang
      , displayAreaHeight = V 50
      }

upiAppsLayout :: ∀ w. (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
upiAppsLayout push state =
  linearLayout
    [ height MATCH_PARENT
    , width MATCH_PARENT
    , orientation VERTICAL
    , visibility (if length state.upiApps == 0 then GONE else VISIBLE)
    , UIUtils.sectionMargin state.configPayload
    ] (
    [ PPUtils.getSectionHeader (STR.getUpiApp lang) state.configPayload false ] <>
    ( UICommons.addCurvedWrapper
        state.configPayload $
          getUPIApps
            push
            enabledApps
            state.disabledUpiApps
            selectedApp
            state.configPayload
            true
            state.buttonAnim
            state.offers
    ))
  where
  lang = fromMaybe "" (state.configPayload # unwrap # _.language)
  enabledApps =
    state.upiApps #
    filter
      \(UPIIntentTypes.UPIApp upiApp) -> not (isUpiAppDisabled upiApp.appName state.disabledUpiApps)
  selectedApp = case state.selectedInstrument of
    Just (UPIApplication a@(UPIIntentTypes.UPIApp app)) -> Just a
    _ -> Nothing

getUPIApps :: ∀ w. (Action -> Effect Unit) -> Array UPIIntentTypes.UPIApp -> Array String -> Maybe UPIIntentTypes.UPIApp -> ConfigPayload -> Boolean -> Boolean -> Array PaymentOffer -> Array (PrestoDOM (Effect Unit) w)
getUPIApps push upiApps disabledUPIApps selectedApp configPayload hideLastLine startBtnAnim offers = upiApps #
  map
    \upiApp@(UPIIntentTypes.UPIApp uApp) ->
      PaymentOptionsComp.view
        (push <<< (UPIAppAction upiApp))
        ( getUPIAppConfig
            upiApp
            selectedApp
            configPayload
            hideLastLine
            (isLastItem upiApp)
            (isAppDisabled uApp)
            (isSelected upiApp)
            startBtnAnim
            (offerText uApp)
            (offerDescription uApp)
        )
        false
        Nothing
  where
  isLastItem cUPIApp = case last upiApps of
    Nothing -> true
    Just lastApp -> lastApp == cUPIApp
  isAppDisabled uApp = isUpiAppDisabled uApp.appName disabledUPIApps
  isSelected uApp = Just uApp == selectedApp
  offerElement = fromMaybe getDummyPaymentOffer $ offers !! 0
  fm = fromMaybe []
  offerText uApp = ""
    -- if uApp.packageName `elem` fm offerElement.paymentMethodFilter then
    --   offerElement.offerText
    -- else ""
  offerDescription uApp = ""
    --  if uApp.packageName `elem` fm offerElement.paymentMethodFilter then
    --   offerElement.offerDescription
    -- else ""

getUPIAppConfig
  :: UPIIntentTypes.UPIApp
  -> Maybe UPIIntentTypes.UPIApp
  -> ConfigPayload
  -> Boolean
  -> Boolean
  -> Boolean
  -> Boolean
  -> Boolean
  -> String
  -> String
  -> PaymentOptionsConf.Config
getUPIAppConfig uApp@(UPIIntentTypes.UPIApp upiApp) selectedApp configPayload hideLastLine isLastLine isAppDisabled isSelected startBtnAnim offerText offerDescription = let
  PaymentOptionsConf.Config defaultConfig = PaymentOptionsConf.defConfig configPayload
  lang = fromMaybe "" (configPayload # unwrap # _.language)
  offerText' =
    if offerDescription /= "" then
      UIUtils.offerDescHTML offerText configPayload
    else offerText
  updatedConfig =
    defaultConfig
      { radioButtonIconUrl = if isSelected then "tick" else "circular_radio_button"
      , inputAreaVisibility = boolToVisibility isSelected
      , primaryText = upiApp.appName
      , logoUrl = upiApp.packageName
      , secondaryTextVisibility = boolToVisibility isAppDisabled
      , displayAreaAlpha = if isAppDisabled then 0.5 else 1.0
      , buttonText = defaultConfig.buttonText
      , radioButtonVisibility = boolToVisibility $ not isAppDisabled
      , secondaryText = STR.disabled lang
      , startAnimation = startBtnAnim
      , cvvInputVisibility = GONE
      , cvvId = upiApp.packageName <> "_id" -- patch added due to repeating id  "default"
      , quantText = offerText'
      , quantTextFromHTML = (offerDescription /= "")
      , quantVisibility = boolToVisibility (offerText /= "")
      , enableQuantLayoutClick = (offerDescription /= "")
      , lineSeparatorVisibility =
        case hideLastLine, isLastLine of
          true, true -> GONE
          _, _ -> VISIBLE
      , usePackageIcon = true
      }
  in
  PaymentOptionsConf.Config updatedConfig

savedVpaLayout :: ∀ w. (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
savedVpaLayout push state =
  if state.showOnlyUPIApps then linearLayout [width $ V 0, height $ V 0] []
  else
  linearLayout
    [ height MATCH_PARENT
    , width MATCH_PARENT
    , orientation VERTICAL
    , visibility $ boolToVisibility $ not $ null state.savedVpas
    , UIUtils.sectionMargin state.configPayload
    ](
    [ PPUtils.getSectionHeader (STR.getSavedUpiHeader lang) state.configPayload false ]<>
    ( UICommons.addCurvedWrapper state.configPayload $
        getSavedVPAs
          push
          state.savedVpas
          selectedVPA
          state.buttonAnim
          state.configPayload
    ))
  where
  lang = fromMaybe "" (state.configPayload # unwrap # _.language)
  PaymentOptionsConf.Config w = PaymentOptionsConf.defConfig state.configPayload
  selectedVPA = case state.selectedInstrument of
    Just (StoredVPA a@(Instruments.StoredVPA vpa)) -> Just a
    _ -> Nothing

getSavedVPAs :: ∀ w. (Action -> Effect Unit) -> Array Instruments.StoredVPA -> Maybe Instruments.StoredVPA -> Boolean -> ConfigPayload -> Array (PrestoDOM (Effect Unit) w)
getSavedVPAs push storedVPAs selectedStoredVPA startBtnAnim configPayload = storedVPAs #
  map
    \vpa@(Instruments.StoredVPA savedVpa) ->
      PaymentOptionsComp.view
        ( push <<< (SavedVPAAction vpa))
        ( getSavedVPAConfig
            vpa
            configPayload
            (isSelected vpa)
            (isLastItem vpa)
            startBtnAnim
        )
        false
        Nothing
  where
  isSelected vpa = Just vpa == selectedStoredVPA
  isLastItem vpa = case last storedVPAs of
    Nothing -> true
    Just lastVPA -> lastVPA == vpa


getSavedVPAConfig :: Instruments.StoredVPA -> ConfigPayload -> Boolean -> Boolean -> Boolean -> PaymentOptionsConf.Config
getSavedVPAConfig (Instruments.StoredVPA savedVPA) configPayload isSelected isLast startBtnAnim = let
  PaymentOptionsConf.Config defaultConfig = PaymentOptionsConf.defConfig configPayload
  in
  PaymentOptionsConf.Config
    defaultConfig
      { radioButtonIconUrl = if isSelected then "tick" else "circular_radio_button"
      , radioButtonGravity = TOP_VERTICAL
      , inputAreaVisibility = boolToVisibility isSelected
      , primaryText = "UPI - " <> (getAppNameFromVpa savedVPA.vpa)
      , secondaryText = savedVPA.vpa
      , secondaryTextColor = "#5C5C5C"
      , logoUrl = getVpaImage savedVPA.vpa
      , topRowVisibility = GONE
      , quantVisibility = GONE
      , lineSeparatorVisibility = boolToVisibility (not isLast)
      , cvvInputVisibility = GONE
      , cvvId = savedVPA.id <> "_id" -- patch added due to repeating id  "default"
      , startAnimation = startBtnAnim
      }
