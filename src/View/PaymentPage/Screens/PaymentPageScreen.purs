module View.PaymentPage.Screens.PaymentPageScreen where

import HyperPrelude.External(length, null,Maybe(..),fromMaybe,Effect,Unit,($),map,not,(&&),(||),(==),(<<<),(#),(<>),const,unit,negate)
import HyperPrelude.Internal(Screen,relativeLayout,linearLayout,Length(..), Orientation(..), PrestoDOM, Visibility(..), scrollView,weight,background, height, linearLayout, orientation, visibility, width,clickable,id)
import Engineering.Helpers.Events (addCustomBackPress, afterRenderEvent, onOrderUpdate)
import JBridge as JBridge
import PPConfig.Utils as CPUtils
import PrestoDOM.Animation as PrestoAnim
import PrestoDOM.Properties (scrollBarY)
import UI.Components.AmountBar.View as AmountBar
import UI.Components.ToolBar.View as ToolBar
import UI.Config as UIConfig
import UI.Utils (screenWidth, translateInXAnim, translateOutXAnim, containerPadding, contentMargin, sectionMargin)
import Utils (_hide_loader)
import Utils as SrcUtils
import View.PaymentPage.Controllers.Controller (eval, initialState)
import View.PaymentPage.Controllers.Types (Action(..), ScreenInput, ScreenOutput, State)
import View.Stock.Container.Commons as UICommons
import View.PaymentPage.Screens.MerchantOfferView as MerchantOffer
import View.PaymentPage.Screens.OtherOptions as OtherOptions
import View.PaymentPage.Screens.SavedOptions as SavedOptions
import View.PaymentPage.Screens.Utils as PPUtils
import Flow.Utils (getCustomerName)

screen :: ScreenInput -> Screen Action State ScreenOutput
screen input =
  { initialState: (initialState input)
  , name: "PaymentPageScreen"
  , view
  , globalEvents: []
  , eval: eval
  }

view :: ∀ w. (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
view push state =
  relativeLayout
    [ width MATCH_PARENT
    , height MATCH_PARENT
    ]
    [ linearLayout (
      [ height MATCH_PARENT
      , width MATCH_PARENT
      , background (CPUtils.backgroundColor state.configPayload)
      , orientation VERTICAL
      , addCustomBackPress push (const BackPress)
      , onOrderUpdate push OfferApplied
      , clickable true
      ] <> if state.isFirstScreen then [afterRenderEvent (_hide_loader)] else [])
      [ ToolBar.view (push <<< ToolBarAction) (UIConfig.toolBarConfig state.configPayload)
      , amountBarView Attached state
      , PrestoAnim.entryAnimationSetForward   [ translateInXAnim  state.configPayload (screenWidth unit) true ]
      $ PrestoAnim.exitAnimationSetForward    [ translateOutXAnim state.configPayload (-50) true ]
      $ PrestoAnim.entryAnimationSetBackward  [ translateInXAnim  state.configPayload (-(screenWidth unit)) true ]
      $ PrestoAnim.exitAnimationSetBackward   [ translateOutXAnim state.configPayload (screenWidth unit) true ]
      $ scrollView
          [ weight 1.0
          , width MATCH_PARENT
          , containerPadding state.configPayload
          , scrollBarY true
          ]
          [ linearLayout
              [ width MATCH_PARENT
              , height WRAP_CONTENT
              , orientation VERTICAL
              --, contentMargin state.configPayload
              ]
              [ amountBarView NonAttached state
              -- , merchantFragmentView push state
              , MerchantOffer.view push state
              -- , RecommendedOptions.view push state
              -- , DefaultOption.view push state
              -- , OfferOptions.view push state
              , SavedOptions.view push state
              -- , PPUtils.offersPaneView push state
              , OtherOptions.view push state
              -- , MoreOption.view push state
              ]
          ]
      , PPUtils.termsAndConditions push state
      ]
    , UICommons.offerPopup (UICommons.PPAction push) state.configPayload state.activeOfferDesc state.showOfferDescPopup
    ]

merchantFragmentView :: ∀ w. (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
merchantFragmentView push state =
  linearLayout
    [ height WRAP_CONTENT
    , width MATCH_PARENT
    , id layoutId
    , afterRenderEvent (SrcUtils.attachMerchantView layoutId)
    , visibility GONE
    ]
    []
  where
  layoutId = (JBridge.getNewIDWithTag JBridge.MERCHANT_VIEW)

data AttachPosition = Attached | NonAttached

amountBarView :: ∀ w. AttachPosition -> State -> PrestoDOM (Effect Unit) w
amountBarView position state = do
  let amview = AmountBar.view $ UIConfig.amountBarConfig state.configPayload state.amount state.checkoutDetails.customer_phone_number state.checkoutDetails.orderDescription (getCustomerName state.checkoutDetails)
  let attachAtTop = CPUtils.attachAmountBarAtTop state.configPayload
  let wrapper c= linearLayout[width MATCH_PARENT, height WRAP_CONTENT, sectionMargin state.configPayload][c]
  case position, attachAtTop of
    Attached, true -> linearLayout[width $ V 0, height $ V 0][]
    NonAttached, false ->  linearLayout[width $ V 0, height $ V 0][]
    _, _ -> linearLayout[width $ V 0, height $ V 0][]
