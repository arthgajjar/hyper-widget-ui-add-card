module View.PaymentPage.Screens.SavedOptions where

import HyperPrelude.External(length,Maybe(..),fromMaybe,Effect,Unit,(==),(||),($),(#),(&&),(/=),not,(<>))
import HyperPrelude.Internal (Length(..), Orientation(..), PrestoDOM, Visibility(..), height, linearLayout, orientation, visibility, width)
import Data.Newtype (unwrap)
import Data.String as StrUtils
import PPConfig.Utils as CPUtils
import UI.Constant.Str.Default as STR
import View.PaymentPage.Controllers.Types (Action, State)
import View.PaymentPage.Screens.Utils as PPUtils
import View.Wallet.Screens.Utils as WalletViewUtils
import View.Stock.Container.Commons as UICommons
import UI.Utils as UIUtils
import Flow.Utils (findInstrumentGroup)

view :: ∀ w. (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
view push state =
  linearLayout
    [ width MATCH_PARENT
    , height WRAP_CONTENT
    , orientation VERTICAL
    , UIUtils.sectionMargin state.configPayload
    , visibility
        if  StrUtils.toUpper savedOptionsConfig.otherSaved == "VISIBLE" &&
            StrUtils.toUpper savedOptionsConfig.saved == "VISIBLE" &&
            ( length state.savedWallets /= 0 || length state.savedCards /= 0 ) &&
            not state.voucherApplied
          then VISIBLE
          else GONE
    ] (
    [ PPUtils.getSectionHeader savedPayHeader state.configPayload false ] <>
    ( UICommons.addCurvedWrapper
        state.configPayload
        (( storedWalletsView push state)
        <> (storedCardsView push state)
        <> (PPUtils.getLastUsedAppView push state)
        <> (PPUtils.getSavedVpaView push state))
    ))
  where
  savedPayHeader = STR.getSavedPayHeader $ fromMaybe "" (state.configPayload # unwrap # _.language)
  savedOptionsConfig = CPUtils.savedOptionsConfig state.configPayload


storedWalletsView :: ∀ w. (Action -> Effect Unit) -> State -> Array (PrestoDOM (Effect Unit) w)
storedWalletsView push state =
  if  CPUtils.ifCombinedWallets state.configPayload ||
      length state.savedWallets == 0
    then []
    else
      WalletViewUtils.getLinkedWalletsView
        { pushReceived : (WalletViewUtils.PPScreenAction push)
        , storedWallets : state.savedWallets
        , disabledWallets : state.disabledWallets
        , selectedWallet : (PPUtils.getWalletNameFromState state)
        , startBtnAnimation : state.startPayment
        , configPayload : state.configPayload
        , defaultOption : state.defaultOption
        , defWallet : if state.defaultOptionType == "WALLET"
                        then state.defaultOption
                        else ""
        , offers : state.offers
        , hideLastDivider : (length state.savedCards == 0)
        , amount : state.amount
        , delink : false
        , outages : findInstrumentGroup "WALLET" state.outages
        }

storedCardsView :: ∀ w. (Action -> Effect Unit) -> State -> Array (PrestoDOM (Effect Unit) w)
storedCardsView push state =
  if (length state.savedCards == 0) then []
  else PPUtils.getSavedCardsView push state state.savedCards true
