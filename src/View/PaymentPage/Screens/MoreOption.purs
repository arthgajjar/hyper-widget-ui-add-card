module View.PaymentPage.Screens.MoreOption where

import HyperPrelude.External(Effect,Unit)
import HyperPrelude.Internal (Length(..), Orientation(..), PrestoDOM, background, height, linearLayout, orientation, width)
import Data.String as StrUtils
import Remote.Types (MoreOption(..))
import View.PaymentPage.Controllers.Types (Action, State)



view :: ∀ w  . (Action  -> Effect Unit) -> State  -> PrestoDOM (Effect Unit) w
view push state =
    linearLayout
        [ width MATCH_PARENT
        , height MATCH_PARENT
        , background "#ffffff"
        , orientation VERTICAL
        -- , visibility if (getMoreOptData state.moreOptions VISIBILITY) == "visible" && not state.voucherApplied
        --                 then VISIBLE
        --                 else GONE
        ] []
        -- [ PaymentOptionsView.view (push <<< SelectMoreOption)
        --     (UIConfig.genericCardConfig
        --         (getMoreOptData state.moreOptions LABEL)
        --         (getMoreOptData state.moreOptions ICON)
        --         state.configPayload
        --     )
        --     false
        --     Nothing
        -- ]


data ReqData = VISIBILITY | LABEL | ICON

getMoreOptData :: MoreOption -> ReqData -> String
getMoreOptData (MoreOption moreOption) dataReq = do
    case dataReq of
        VISIBILITY -> (StrUtils.toLower moreOption.visibility)
        LABEL -> moreOption.name
        ICON -> moreOption.icon
