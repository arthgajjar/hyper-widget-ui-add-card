module View.PaymentPage.Screens.MerchantOfferView where

import HyperPrelude.External(filter,(!!),Maybe(..), fromMaybe,Effect,Unit,($),(==),(#),(<>),map)
import HyperPrelude.Internal(Length(..), Margin(..), Orientation(..), Padding(..), PrestoDOM, Visibility(..), background, cornerRadius, height, linearLayout, margin, orientation, padding, visibility, width)
import Data.Newtype (unwrap)
import Data.String (toLower)
import Service.EC.Types.Instruments (StoredCard(..))
import Validation (isCardNumberOutsideBins)
import Service.UPIIntent.Types (UPIApp(..)) as UPIIntentTypes
import Payments.NetBanking.Utils (Bank(..))
import Remote.Types (MerchantOffer(..), VisiblePayOption, PaymentOptions(..))
import UI.Constant.Str.Default as STR
import View.PaymentPage.Controllers.Types (Action, State)
import View.PaymentPage.Controllers.Utils (getLinkedWalletData, getUnlinkedWalletData, walletLinkingStatus)
import View.PaymentPage.Screens.OtherOptions as OtherOptions
import View.PaymentPage.Screens.Utils as PPUtils
import View.Wallet.Screens.Utils as WalletViewUtils


view :: ∀ w. (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
view push state =
    linearLayout
    [ width MATCH_PARENT
    , height MATCH_PARENT
    , orientation VERTICAL
    , margin (Margin 0 0 0 0)
    , visibility if state.voucherApplied
                    then VISIBLE
                    else GONE
    ]
    [ PPUtils.getSectionHeader header state.configPayload false
    , linearLayout
        [ height WRAP_CONTENT
        , width MATCH_PARENT
        , orientation VERTICAL
        , cornerRadius 8.0
        , background "#FFFFFF"
        , padding (Padding 2 2 2 2)
        ] (generateOfferView push state)
    ]
    where
        header = STR.getVoucherAppliedHeader $ fromMaybe "" (state.configPayload # unwrap # _.language)


generateOfferView :: ∀ w. (Action -> Effect Unit) -> State -> Array (PrestoDOM (Effect Unit) w)
generateOfferView push state =
    let (MerchantOffer merchantOffer) = state.merchantOffer in

    case (toLower $ fromMaybe "" merchantOffer.payment_method_type) of

        "wallet" -> do
            case merchantOffer.payment_method of
                Just walletCode -> do
                    let walletLink = walletLinkingStatus walletCode state
                    if walletLink
                        then do
                        let walletDetail = getLinkedWalletData walletCode state.savedWallets
                        (WalletViewUtils.getLinkedWalletsView
                            { pushReceived : (WalletViewUtils.PPScreenAction push)
                            , storedWallets : walletDetail
                            , selectedWallet : (PPUtils.getWalletNameFromState state)
                            , disabledWallets : [""]
                            , startBtnAnimation : state.startPayment
                            , configPayload : state.configPayload
                            , defaultOption : state.defaultOption
                            , defWallet : if state.defaultOptionType == "WALLET"
                                            then state.defaultOption
                                            else ""
                            , offers : state.offers
                            , hideLastDivider : true
                            , amount : state.amount
                            , delink : false
                            , outages : []
                            }
                        )
                        else do
                            let walletDetail = (getUnlinkedWalletData walletCode state.unlinkedWallets )
                            (WalletViewUtils.getUnlinkedWalletsView
                                { pushReceived : (WalletViewUtils.PPScreenAction push)
                                , wallets : walletDetail
                                , disabledWallets : []
                                , selectedWalletCode : ""--state.selectedUnlinkedWallet TODO :: fix this
                                , configPayload : state.configPayload
                                , ifVisible : state.startPayment
                                , payLaterEligibility : []
                                , useLabel : true
                                , defaultOption : state.defaultOption
                                , defWallet : if state.defaultOptionType == "WALLET"
                                                then state.defaultOption
                                                else ""
                                , startButtonAnimation : state.startPayment
                                , offers : state.offers
                                , hideLastDivider : true
                                , amount : state.amount
                                , mandateType : state.mandateType
                                , enableSI : state.enableSI
                                , outages : []
                                }
                            )
                Nothing -> do
                    (WalletViewUtils.getLinkedWalletsView
                        { pushReceived : (WalletViewUtils.PPScreenAction push)
                        , storedWallets : state.savedWallets
                        , selectedWallet : (PPUtils.getWalletNameFromState state)
                        , disabledWallets : [""]
                        , startBtnAnimation : state.startPayment
                        , configPayload : state.configPayload
                        , defaultOption : state.defaultOption
                        , defWallet : if state.defaultOptionType == "WALLET"
                                        then state.defaultOption
                                        else ""
                        , offers : state.offers
                        , hideLastDivider : false
                        , amount : state.amount
                        , delink : false
                        , outages : []
                        }
                    )
                    <> (map (\payOpts -> OtherOptions.getElement push payOpts state)
                            $ PPUtils.getVisiblePaymentOptions state [getPaymentOption "wallets", getPaymentOption "payLater"]
                        )

        -- "card" -> do
        --     let filteredCards = PPUtils.filterCards
        --                             merchantOffer.payment_method
        --                             merchantOffer.payment_card_type
        --                             merchantOffer.payment_card_issuer
        --                             state.savedCards
        --         offerBins = filter (\bins -> Just bins.voucherCode == merchantOffer.offer_code) state.offers !! 0
        --         filteredBins = filter (\(StoredCard card) -> not $ isCardNumberOutsideBins card.cardIsin offerBins) state.savedCards
        --     ( (PPUtils.getSavedCardsView push state filteredBins false) <>
        --       [ OtherOptions.getElement push (getVisiblePayOption "cards" true) state])

        "upi" ->
            case merchantOffer.payment_method of
                Nothing -> do
                    [OtherOptions.getElement push (getVisiblePayOption "upi" true) state]
                Just upiAppName -> do
                    let filteredApps = filter (\(UPIIntentTypes.UPIApp app) -> app.appName == upiAppName) state.upiApps
                    (PPUtils.getUpiAppsView push state filteredApps)

        "nb" ->
            case merchantOffer.payment_method of
                Nothing -> [OtherOptions.getElement push (getVisiblePayOption "nb" true) state]
                Just bankCode -> do
                    let filteredBanks = filter (\(Bank b) -> b.code == bankCode) state.allBanks
                    (PPUtils.getNetBankView push state filteredBanks)

        _ -> [linearLayout[width MATCH_PARENT, height WRAP_CONTENT][]]

getPaymentOption :: String -> PaymentOptions
getPaymentOption po =
    PaymentOptions
        { group: "others"
        , po
        , visibility: "VISIBLE"
        , onlyEnable : Nothing
        , onlyDisable: Nothing
        , isLast : ""
        }

getVisiblePayOption :: String -> Boolean -> VisiblePayOption
getVisiblePayOption po hideDivider =
    { po
    , hideDivider
    }
