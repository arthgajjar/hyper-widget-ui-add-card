module View.PaymentPage.Screens.OfferOptions where

import HyperPrelude.External(elem, filter, null, length,fromMaybe, Maybe(..),Effect,Unit,(&&),(==),($),not,map,(||))
import HyperPrelude.Internal(Length(..), Margin(..), Orientation(..), Padding(..), PrestoDOM, Visibility(..), background, cornerRadius, height, linearLayout, margin, orientation, padding, visibility, width, translationZ)
import Data.Newtype (unwrap)
import Data.Number.Format (toString)
import Engineering.Helpers.Commons (getIinFromName)
import Flow.Utils (filterOffer)
import PPConfig.Utils as CPUtils
import PaymentPageConfig as PPConfig
import Payments.NetBanking.Utils (Bank)
import Payments.Wallets.Types (Wallet(..))
import Payments.Wallets.Utils (removeStoredPayLaterWallets, removePayLaterWallets)
import Remote.Types (VisiblePayOption)
import Service.EC.Types.Instruments as Instruments
import Service.UPIIntent.Types as UPIIntentTypes
import UI.Constant.Str.Default as STR
import UI.Utils as UIUtils
import View.PaymentPage.Controllers.Types (Action, State, PaymentInstrument(..))
import View.PaymentPage.Controllers.Utils (getLinkedWalletData, walletLinkingStatus)
import View.PaymentPage.Screens.Utils as PPUtils
import View.Wallet.Screens.Utils as WalletViewUtils
import Flow.Utils (findInstrumentGroup)

view :: ∀ w  . (Action  -> Effect Unit) -> State  -> PrestoDOM (Effect Unit) w
view push state =
    linearLayout
    [ width MATCH_PARENT
    , height MATCH_PARENT
    , orientation VERTICAL
    , UIUtils.sectionMargin state.configPayload
    , visibility if (CPUtils.ifOfferVisible state.configPayload && not state.voucherApplied) then VISIBLE else GONE
    ]
    [ (PPUtils.getSectionHeader headerString state.configPayload true) -- TODO : Fix STR.getPayOfferHeader
    , getOfferSectionView push state
    ]
    where
        headerString = PPUtils.lang state

type PaymentMethodsWithOffer =
    { offerStoredWallets :: Array Instruments.StoredWallet -- TODO : create a type
    , offerUnlinkedWallets :: Array Wallet
    , offerApps :: Array UPIIntentTypes.UPIApp
    , offerNBs :: Array Bank
    , offerVPAs :: Array Instruments.StoredVPA
    }

getOfferSectionView :: ∀ w. (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w -- TODO : Fix UnlinkedWallet Issue
getOfferSectionView push state = do
    let offers = PPConfig.getOfferOptions state.configPayload

    linearLayout
        [ width MATCH_PARENT
        , height WRAP_CONTENT
        , orientation VERTICAL
        , cornerRadius 5.0 -- TODO : get cornerRadius from Config
        , background "#FFFFFF"
        , padding (Padding 0 6 0 6)
        ]
        (map (\offerOpts -> getOfferElement push offerOpts state paymentMethodswithOffer)
            $  PPUtils.getVisiblePaymentOptions state
                $ CPUtils.updatePOVisibility "unlinkedWallets" (null offerUnlinkedWallets)
                $ CPUtils.updatePOVisibility "savedWallets" (null offerStoredWallets)
                $ CPUtils.updatePOVisibility "nb" (null offerNBs)
                $ CPUtils.updatePOVisibility "upi" (null offerApps || upiDisabled)
                $ CPUtils.updatePOVisibility "savedVPAs" (null offerVPAs || upiDisabled)
                offers)

    where
        offerApps = PPUtils.getOfferUPIApps state.offers $ PPUtils.removeDefaultUPIApps state
        offerNBs = PPUtils.getOfferNBs state.offers $ PPUtils.removeDefaultNB state
        offerUnlinkedWallets = (PPUtils.getUnlinkedOfferWallets state.offers state.unlinkedWallets)
        offerStoredWallets = PPUtils.getStoredOfferWallets state.offers $ PPUtils.removeDefaultWallet state
        offerVPAs = PPUtils.getOfferVPAs state.offers $ PPUtils.removeDefaultVPA state
        upiDisabled = false -- isUPIDisabled state.paymentSourceResp state.gatewayReferenceId
        paymentMethodswithOffer = { offerStoredWallets
             , offerUnlinkedWallets
             , offerApps
             , offerNBs
             , offerVPAs
             }


getOfferElement ::
    ∀ w. (Action -> Effect Unit) -- push
    -> VisiblePayOption -- item
    -> State -- state
    -> PaymentMethodsWithOffer
    -> PrestoDOM (Effect Unit) w
getOfferElement push paymentOption state offerOptions = do
    case paymentOption.po of

        "savedWallets" ->
            wrapperLayout
            (WalletViewUtils.getLinkedWalletsView
                { pushReceived : (WalletViewUtils.PPScreenAction push)
                , storedWallets : state.savedWallets
                , disabledWallets : [""]
                , selectedWallet : (PPUtils.getWalletNameFromState state)
                , startBtnAnimation : state.startPayment
                , configPayload : state.configPayload
                , defaultOption : state.defaultOption
                , defWallet : if state.defaultOptionType == "WALLET"
                                then state.defaultOption
                                else ""
                , offers : state.offers
                , hideLastDivider : (length state.savedCards == 0)
                , amount : state.amount
                , delink : false
                , outages : findInstrumentGroup "WALLET" state.outages
                }
            )

        "unlinkedWallets" ->
            wrapperLayout
            ( WalletViewUtils.getUnlinkedWalletsView
                { pushReceived : (WalletViewUtils.PPScreenAction push)
                , amount : state.amount
                , configPayload : state.configPayload
                , defWallet : if state.defaultOptionType == "WALLET"
                                then state.defaultOption
                                else ""
                , defaultOption : state.defaultOption
                , disabledWallets : state.disabledWallets
                , payLaterEligibility : []
                , enableSI : state.enableSI
                , hideLastDivider : true
                , ifVisible : true
                , mandateType : state.mandateType
                , offers : state.offers
                , selectedWalletCode : ""
                , startButtonAnimation : state.startPayment
                , useLabel : true
                , wallets : state.unlinkedWallets
                , outages : findInstrumentGroup "WALLET" state.outages
                }
            )

        "savedVPAs" ->
            wrapperLayout
            (PPUtils.getSavedVpaView push state)

        "upi" ->
            wrapperLayout
            (PPUtils.getUpiAppsView push state offerOptions.offerApps)

        "nb" ->
            wrapperLayout
            (PPUtils.getNetBankView push state offerOptions.offerNBs)

        _ -> linearLayout [width MATCH_PARENT, height $ V 0][]

    where
        wrapperLayout =
            linearLayout
                [ width MATCH_PARENT
                , height MATCH_PARENT
                , orientation VERTICAL
                ]

        --payLaterDisabled = getDisabledFromStatus state.payLaterWallets state.savedPayLaterWallets
        --removeStoredPayLaterWallets wallets = filter (\(Instruments.StoredWallet (Instruments.Wallet w)) -> not $ fromMaybe "" w.wallet `elem` payLaterDisabled) wallets
        --removeUnlinkedPayLater wallets = filter (\(Instruments.Wallet w) -> not $ w.code `elem` payLaterDisabled) wallets
