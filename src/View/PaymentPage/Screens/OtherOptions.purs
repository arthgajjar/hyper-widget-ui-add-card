module View.PaymentPage.Screens.OtherOptions where

import HyperPrelude.External (length, null,Maybe(..),fromMaybe,Effect,Unit,($),map,not,(&&),(||),(==),(<<<),(#),(<>))
import HyperPrelude.Internal(Length(..), Orientation(..), PrestoDOM, Visibility(..), background, height, linearLayout, orientation, visibility, width)
import Data.Foldable (elem)
import Data.Newtype (unwrap)
import Flow.Utils (findInstrumentGroup)
import PPConfig.Utils as CPUtils
import Remote.Types (ConfigPayload(..), VisiblePayOption)
import UI.Components.PaymentOptionsConfig as PaymentOptionsConfig
import UI.Components.PaymentOptionsView as PaymentOptionsView
import UI.Config as UIConfig
import UI.Constant.Str.Default as STR
import UI.Utils (boolToVisibility)
import UI.Utils as UIUtils
import View.PaymentPage.Controllers.Types (Action(..), PaymentInstrument(..), State)
import View.Stock.Container.Commons as UICommons
import View.PaymentPage.Screens.ExpandedViews as Expanded
import View.PaymentPage.Screens.Utils as PPUtils
import View.Wallet.Screens.Utils as WalletViewUtils

view :: ∀ w. (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
view push state = let
  wrapper =
    linearLayout
      [ width MATCH_PARENT
      , height WRAP_CONTENT
      , orientation VERTICAL
      , visibility $ boolToVisibility $ not state.voucherApplied
      , UIUtils.sectionMargin state.configPayload
      ]
  in
  if CPUtils.attachPOHeaders state.configPayload then
    wrapper $
      map
        (\payOpts -> getElement push payOpts state)
        (PPUtils.getVisiblePaymentOptions state $
          CPUtils.getPaymentOptions state.configPayload)
  else let
    poSpiltted = PPUtils.getExpandedPaymentOptions state (CPUtils.getPaymentOptions state.configPayload)
    viewGen = map (\payOpts -> getElement push payOpts state)
    expandedItemsView = viewGen poSpiltted.expanded
    nonExpandedItemsView  = viewGen poSpiltted.collapsed
  in wrapper $
      expandedItemsView <> (getOtherSectionView push state nonExpandedItemsView)


getOtherSectionView :: ∀ w. (Action -> Effect Unit) -> State -> Array (PrestoDOM (Effect Unit) w) -> Array (PrestoDOM (Effect Unit) w)
getOtherSectionView push state child =
  if CPUtils.expandableOtherOptions cp && CPUtils.expandInAccordion cp
  then [ PaymentOptionsView.view
          (push <<< SelectOtherOptions)
          (Expanded.expandedOtherOptionsConfig cp state.expandOtherOptions)
          false
          (Just [ linearLayout
                  [ width MATCH_PARENT
                  , height WRAP_CONTENT
                  , orientation VERTICAL
                  ] child
                ])
       ]
  else PPUtils.poHeaderLayout cp "Other Options" true child
  where
  cp = state.configPayload


getElement ::
  ∀ w.
  (Action -> Effect Unit) ->
  VisiblePayOption ->
  State ->
  PrestoDOM (Effect Unit) w
getElement push paymentOption state =
  wrapperLayout $ case paymentOption.po of
  "upi"         -> getUPIView push state paymentOption.hideDivider
  "nb"          -> getNBView push state paymentOption.hideDivider
  "cards"       -> getCardsView push state paymentOption.hideDivider
  "wallets"     -> getWalletsView push state paymentOption.hideDivider
  "cod"         -> getCodView push state paymentOption.hideDivider
  "mealCards"   -> getMealCardView push state paymentOption.hideDivider
  "payLater"    -> getPayLaterCardView push state paymentOption.hideDivider
  "cardod"      -> getCrdodView push state paymentOption.hideDivider
  "emi"         -> getEMIView push state paymentOption.hideDivider
  "upiApps"     -> Expanded.onlyUPIAppsView push state
  "upiCollect"  -> Expanded.onlyUPICollectView push state
  "upiAppsWithOther" ->
    if null state.upiApps then
      getUPIView push state paymentOption.hideDivider
    else
      Expanded.upiAppsWithOthersOptions push state
  -- Remaining cases to be handled later on
  "savedCards" ->
    UICommons.addCurvedWrapper
      state.configPayload $
        PPUtils.getSavedCardsView
          push
          state
          (PPUtils.removeDefaultCard state)
          false
  "savedWallets" ->
    if isWalletViewExpanded && isWalletsCombined then []
    else
      WalletViewUtils.getLinkedWalletsView
        { pushReceived : (WalletViewUtils.PPScreenAction push)
        , storedWallets : (PPUtils.removeDefaultWallet state)
        , selectedWallet : (PPUtils.getWalletNameFromState state)
        , startBtnAnimation : state.startPayment
        , configPayload : state.configPayload
        , defaultOption : state.defaultOption
        , defWallet : if state.defaultOptionType == "WALLET"
                        then state.defaultOption
                        else ""
        , offers : state.offers
        , hideLastDivider : false
        , amount : state.amount
        , delink : false
        , disabledWallets : []
        , outages : findInstrumentGroup "WALLET" state.outages
        }

  _ -> [ linearLayout [ width MATCH_PARENT, height $ V 0 ] [] ]
  where
  wrapperLayout =
    linearLayout
      [ width MATCH_PARENT
      , height WRAP_CONTENT
      , orientation VERTICAL
      ]

  (ConfigPayload confPayload) = state.configPayload

  isWalletViewExpanded = CPUtils.ifWalletExpanded state.configPayload

  isWalletsCombined = CPUtils.ifCombinedWallets state.configPayload


getCardsView :: ∀ w. (Action -> Effect Unit) -> State -> Boolean -> Array (PrestoDOM (Effect Unit) w)
getCardsView push state hideDivider =
  if CPUtils.expandCardView state.configPayload then
    Expanded.addCardView push state
  else
    PPUtils.poHeaderLayout state.configPayload (STR.getCard $ PPUtils.lang state) false $
    UICommons.addCurvedWrapper
      state.configPayload
        [ PaymentOptionsView.view
            (push <<< SelectCard)
            (UIConfig.paymentCardConfig state.configPayload hideDivider isDown)
            false
            Nothing
        ]
  where
  outages = findInstrumentGroup "CARD" state.outages
  isDown = "" `elem` outages

getCodView :: ∀ w. (Action -> Effect Unit) -> State -> Boolean -> Array (PrestoDOM (Effect Unit) w)
getCodView push state hideDivider =
  ( UICommons.addCurvedWrapper
      state.configPayload
      [ PaymentOptionsView.view
          (push <<< SelectCod)
          (PaymentOptionsConfig.Config codConfigSelected)
          false
          Nothing
      ]
  )
  where
  isSelected = state.selectedPaymentInstrument == Just CashOnDelivery

  PaymentOptionsConfig.Config config = UIConfig.codConfig state.configPayload hideDivider

  codConfigSelected =
    config
      { radioButtonIconUrl = if isSelected then "tick" else "circular_radio_button"
      , inputAreaVisibility = if isSelected then VISIBLE else GONE
      , startAnimation = state.startPayment
      }

getCrdodView :: ∀ w. (Action -> Effect Unit) -> State -> Boolean -> Array (PrestoDOM (Effect Unit) w)
getCrdodView push state hideDivider =
  ( UICommons.addCurvedWrapper
      state.configPayload
      [ PaymentOptionsView.view
          (push <<< SelectCardOd)
          (PaymentOptionsConfig.Config codConfigSelected)
          false
          Nothing
      ]
  )
  where
  lang = fromMaybe "" (state.configPayload # unwrap # _.language)

  isSelected =  state.selectedPaymentInstrument == Just CardOnDelivery

  PaymentOptionsConfig.Config config = UIConfig.codConfig state.configPayload hideDivider

  codConfigSelected =
    config
      { radioButtonIconUrl = if isSelected then "tick" else "circular_radio_button"
      , inputAreaVisibility = if isSelected then VISIBLE else GONE
      , startAnimation = state.startPayment
      , logoUrl = "ic_cardod"
      , primaryText = (STR.getCardOnDelivery lang)
      }

getAskAFView :: ∀ w. (Action -> Effect Unit) -> State -> Boolean -> Array (PrestoDOM (Effect Unit) w)
getAskAFView push state hideDivider =
  ( UICommons.addCurvedWrapper
      state.configPayload
      [ PaymentOptionsView.view
          (push <<< SelectAskAF)
          (PaymentOptionsConfig.Config codConfigSelected)
          false
          Nothing
      ]
  )
  where
  lang = fromMaybe "" (state.configPayload # unwrap # _.language)

  PaymentOptionsConfig.Config config = UIConfig.askAFConfig state.configPayload hideDivider

  codConfigSelected =
    config
      { logoUrl = "juspay_aaf"
      , primaryText = STR.getAAF lang
      }

getWalletsView :: ∀ w. (Action -> Effect Unit) -> State -> Boolean -> Array (PrestoDOM (Effect Unit) w)
getWalletsView push state hideDivider =
  if ((length state.savedWallets == 0) &&
      (length state.unlinkedWallets == 0) ||
      length state.unlinkedWallets == 0 &&
      not (CPUtils.ifCombinedWallets state.configPayload)) then
    []
  else
    if CPUtils.ifWalletExpanded state.configPayload then
      Expanded.walletsView push state
    else
      PPUtils.poHeaderLayout state.configPayload (STR.getWallets $ PPUtils.lang state) false $
      UICommons.addCurvedWrapper
        state.configPayload
        [ PaymentOptionsView.view
            (push <<< SelectWallet)
            (UIConfig.walletCardConfig state.configPayload hideDivider isDown)
            false
            Nothing
        ]
  where
  outages = findInstrumentGroup "WALLET" state.outages
  isDown = "" `elem` outages

getNBView :: ∀ w. (Action -> Effect Unit) -> State -> Boolean -> Array (PrestoDOM (Effect Unit) w)
getNBView push state hideDivider =
  if CPUtils.getNBViewType state.configPayload then
    Expanded.netBankingView push state
  else
    PPUtils.poHeaderLayout state.configPayload (STR.getNetBanking $ PPUtils.lang state) false $
    UICommons.addCurvedWrapper
      state.configPayload
      [ PaymentOptionsView.view
          (push <<< SelectNb)
          (UIConfig.netBankingCardConfig state.configPayload hideDivider isDown)
          false
          Nothing
      ]
  where
  outages = findInstrumentGroup "NB" state.outages
  isDown = "" `elem` outages

getUPIView :: ∀ w. (Action -> Effect Unit) -> State -> Boolean -> Array (PrestoDOM (Effect Unit) w)
getUPIView push state hideDivider =
  if not state.isUPIEnabled then []
  else
    if CPUtils.getUpiViewType state.configPayload && not null state.upiApps then
      Expanded.upiView push state
    else
      PPUtils.poHeaderLayout state.configPayload (STR.getUpiHeader $ PPUtils.lang state) false $
      UICommons.addCurvedWrapper
        state.configPayload
        [ PaymentOptionsView.view
            (push <<< SelectUpi)
            (UIConfig.upiCardConfig state.configPayload hideDivider isDown)
            false
            Nothing
        ]
  where
  outages = findInstrumentGroup "UPI" state.outages
  isDown = "" `elem` outages

getEMIView :: ∀ w. (Action -> Effect Unit) -> State -> Boolean -> Array (PrestoDOM (Effect Unit) w)
getEMIView push state hideDivider =
	PPUtils.poHeaderLayout state.configPayload (STR.getEMIHeader $ PPUtils.lang state) false $
	UICommons.addCurvedWrapper
		state.configPayload
		[ PaymentOptionsView.view
				(push <<< SelectEMI)
				(UIConfig.emiCardConfig state.configPayload hideDivider)
				false
				Nothing
		]


getMealCardView :: ∀ w. (Action -> Effect Unit) -> State -> Boolean -> Array (PrestoDOM (Effect Unit) w)
getMealCardView push state hideDivider =
  ( UICommons.addCurvedWrapper
      state.configPayload
      [ PaymentOptionsView.view
          (push <<< SelectMealCard)
          (UIConfig.mealCardConfig state.configPayload hideDivider)
          false
          Nothing
      ]
  )

getPayLaterCardView :: ∀ w. (Action -> Effect Unit) -> State -> Boolean -> Array (PrestoDOM (Effect Unit) w)
getPayLaterCardView push state hideDivider =
  if length state.payLaterWallets == 0 && length state.savedPayLaterWallets == 0 then
    []
  else
    PPUtils.poHeaderLayout state.configPayload (STR.getPayLaterHeader $ PPUtils.lang state) false $
    ( UICommons.addCurvedWrapper
        state.configPayload
        [ PaymentOptionsView.view
            (push <<< SelectPayLater)
            (UIConfig.payLaterCardConfig state.configPayload hideDivider)
            false
            Nothing
        ]
    )
