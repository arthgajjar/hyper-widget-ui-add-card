module View.PaymentPage.Screens.DefaultOption where


import HyperPrelude.External (Maybe(..),fromMaybe, isJust,Effect,Unit,(<<<),($),(||),(#),(&&),(==),not)
import HyperPrelude.Internal (Length(..), Orientation(..), Padding(..), PrestoDOM, Visibility(..), background, cornerRadius, height, linearLayout, orientation, padding, visibility, width)
import Data.Newtype (unwrap)
import Data.String as StrUtils
import Service.EC.Types.Instruments as Instruments
import Engineering.Helpers.Commons (getIinFromName)
import PaymentPageConfig as PPConfig
import Service.UPIIntent.Types (UPIApp)
import Payments.Wallets.Utils as WalletUtils
import PPConfig.Utils as CPUtils
import UI.Components.PaymentOptionsView as PayOptView
import UI.Constant.Str.Default as STR
import View.PaymentPage.Screens.Utils (getDefaultCardConfig, getDefaultNBConfig, getDefaultOptionCard, getDefaultOptionDirectWallet, getDefaultOptionUPIApp, getDefaultOptionVPA, getDefaultUpiAppConfig, getDefaultVPAConfig, getDefaultWalletConfig)
import View.PaymentPage.Screens.Utils as PPUtils
import View.PaymentPage.Controllers.Types (Action(..), PaymentInstrument(..), State)

view :: ∀ w . (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
view push state =
    linearLayout
    [ height WRAP_CONTENT
    , width MATCH_PARENT
    , orientation VERTICAL
    , visibility if ((StrUtils.toUpper savedOptionsConfig.preffered) == "VISIBLE" &&
                    (StrUtils.toUpper savedOptionsConfig.saved) == "VISIBLE"  && not state.voucherApplied) && defaultOptionExist
                        then VISIBLE
                        else GONE
    ]
    [
       (PPUtils.getSectionHeader headerText state.configPayload false)
    ,  (wrapperLayout [defView])
    ]
    where
        savedOptionsConfig = CPUtils.savedOptionsConfig state.configPayload
        defaultOptionExist = case state.defaultOptionType of
                                "NB"     -> true
                                "CARD"   -> isJust $ getDefaultOptionCard state
                                "WALLET" -> (isJust $ WalletUtils.getDefaultOptionWallet state.savedWallets)
                                                || (isJust $ getDefaultOptionDirectWallet state)
                                "VPA"    -> isJust $ getDefaultOptionVPA state
                                "UPI"    -> isJust $ getDefaultOptionUPIApp state
                                _        -> false
        headerText =  STR.getDeafultPaymentOptionHeader (fromMaybe "" (state.configPayload # unwrap # _.language))
        wrapperLayout = linearLayout
                        [ width MATCH_PARENT
                        , height WRAP_CONTENT
                        , padding (Padding 2 2 2 2)
                        , cornerRadius 8.0
                        , background "#ffffff"
                        ]
        defView = case state.defaultOptionType of
                    "NB"     -> getDefaultOptionNBView push state
                    "CARD"   -> cardView
                    "VPA"    -> vpaView
                    "UPI"    -> upiAppView
                    "WALLET" -> walletView
                    _        -> linearLayout[visibility GONE][]
        defaultCard = getDefaultOptionCard state
        cardView = case defaultCard of
            Just card -> getDefaultOptionCardView push state card
            Nothing -> linearLayout[visibility GONE][]
        -- defaultNB = getDefaultOptionCard state
        -- nbView = case defaultNB of
        --     Just nb -> getDefaultOptionNBView push state
        --     Nothing -> linearLayout[visibility GONE][]
        defaultVPA = getDefaultOptionVPA state
        vpaView = case defaultVPA of
            Just vpa -> getDefaultOptionVPAView push state vpa
            Nothing -> linearLayout[visibility GONE][]
        defaultUpiApp = getDefaultOptionUPIApp state
        upiAppView = case defaultUpiApp of
            Just app -> getDefaultOptionUpiAppView push state app
            Nothing  -> linearLayout[visibility GONE][]
        defaultWallet1 = WalletUtils.getDefaultOptionWallet state.savedWallets
        defaultWallet2 = getDefaultOptionDirectWallet state
        walletView = case defaultWallet1 of
            Just wallet1 -> getDefaultOptionWalletView push state wallet1
            Nothing      -> case defaultWallet2 of
                                Just wallet2 -> getDefaultOptionWalletView push state wallet2
                                Nothing      -> linearLayout[visibility GONE][]

getDefaultOptionCardView :: ∀ w. (Action -> Effect Unit) -> State -> Instruments.StoredCard -> PrestoDOM (Effect Unit) w
getDefaultOptionCardView push state storedCard =
    PayOptView.view
    (push <<< (StoredCardAction $ StoredCard storedCard))
    (getDefaultCardConfig state storedCard)
    false
    Nothing

getDefaultOptionWalletView :: ∀ w. (Action -> Effect Unit) -> State -> Instruments.StoredWallet -> PrestoDOM (Effect Unit) w
getDefaultOptionWalletView push state storedWallet =
    PayOptView.view
    (push <<< (StoredWalletAction $ StoredWallet storedWallet))
    (getDefaultWalletConfig state storedWallet)
    false
    Nothing

getDefaultOptionNBView :: ∀ w. (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
getDefaultOptionNBView push state =
    let isSelectedNb = false
        -- case state.selectedPaymentMethod of
        --                     Just DefaultPaymentOptionNB -> true
        --                     _                           -> false
    in
    PayOptView.view
    (push <<< PaymentOptionAction)--(DefaultOptionAction DefaultPaymentOptionNB))
    (getDefaultNBConfig state state.defaultOption isSelectedNb)
    false
    Nothing

getDefaultOptionVPAView :: ∀ w. (Action -> Effect Unit) -> State -> Instruments.StoredVPA -> PrestoDOM (Effect Unit) w
getDefaultOptionVPAView push state storedVpa =
    PayOptView.view
    (push <<< PaymentOptionAction)--(DefaultOptionAction DefaultPaymentOptionVPA))
    (getDefaultVPAConfig state storedVpa)
    false
    Nothing

getDefaultOptionUpiAppView :: ∀ w. (Action -> Effect Unit) -> State -> UPIApp -> PrestoDOM (Effect Unit) w
getDefaultOptionUpiAppView push state upiApp =
    PayOptView.view
    (push <<< PaymentOptionAction) --(DefaultOptionAction DefaultPaymentOptionUPIApp))
    (getDefaultUpiAppConfig state upiApp)
    false
    Nothing
