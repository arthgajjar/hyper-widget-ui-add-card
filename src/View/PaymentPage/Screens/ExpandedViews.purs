module View.PaymentPage.Screens.ExpandedViews where

import HyperPrelude.External(Effect,round,Maybe(..),fromMaybe,length, (!!), take, null, filter,(#),(==),Unit,(<>),($),(/=),(&&),(<<<),(||),not,map,(<),const)
import HyperPrelude.Internal(Padding(..),PrestoDOM,Visibility(..),Margin(..),Length(..), Orientation(..),Gravity(..),gravity,linearLayout,height,width,background,margin,orientation,padding,visibility,weight,onClick,clickable,alpha,fontStyle,cornerRadius,textView,text,color,textSize,imageView,imageUrl)
import View.PaymentPage.Controllers.Controller
import View.PaymentPage.Controllers.Types
import Data.Foldable (elem)
import Data.Newtype (unwrap)
import Data.String (Pattern(..))
import Engineering.Helpers.Commons (getIinFromName)
import Flow.Utils (findInstrumentGroup, filterOffer)
import PPConfig.Utils as CPUtils
import Payments.NetBanking.Utils (Bank(..))
import Payments.Wallets.Types as WalletTypes
import Remote.Types (ConfigPayload(..), VisiblePayOption)
import Service.UPIIntent.Types (UPIApp(..)) as UPIIntentTypes
import UI.Components.AddCard.Config as AddCardConfig
import UI.Components.AddCard.View as AddCardView
import UI.Components.Message as Message
import UI.Components.Message.Config as MessageConfig
import UI.Components.PaymentOptions.GridPaymentOptionView as GridPayOption
import UI.Components.PaymentOptionsConfig as PaymentOptionsConfig
import UI.Components.PaymentOptionsView as PaymentOptionsView
import UI.Components.PrimaryButton.Config as PrimaryButtonConfig
import UI.Components.PrimaryButton.View as PrimaryButton
import UI.Config as UIConfig
import UI.Constant.Str.Default as STR
import UI.Utils (boolToVisibility, sectionMargin)
import Utils (arrayJoin, logE)
import View.Stock.Container.Commons as UICommons
import View.NB.Screens.Utils as NBUtils
import View.PaymentPage.Screens.Utils as PPUtils
import View.UPI.Screens.AddScreen as UPIView
import View.Wallet.Screens.Utils as WalletViewUtils
import Payments.Core.Commons (getDummyPaymentOffer)

lang :: State -> String
lang state = fromMaybe "" (state.configPayload # unwrap # _.language)

logoURLMap :: PaymentOptionGroup -> String
logoURLMap poG =
  case poG of
    Wallet 			-> "wallet_icon"
    NetBanking 	-> "net_banking_icon"
    UPI 				-> "ic_upi_icon"
    AddCard 		-> "ic_card"
    UPIApps  		-> "ic_upi_icon"
    UPICollect 	-> "ic_upi_icon"

labelStringMap :: PaymentOptionGroup -> String
labelStringMap poG =
  case poG of
    Wallet 			-> "Wallets"
    NetBanking 	-> "Net Banking"
    UPI 				-> "UPI"
    AddCard 		-> "New Debit/Credit Card"
    UPIApps  		-> "UPI Apps"
    UPICollect 	-> "UPI ID"

expandedSectionConfig :: ConfigPayload -> PaymentOptionGroup -> Boolean -> PaymentOptionsConfig.Config
expandedSectionConfig cP poG expand = let
  PaymentOptionsConfig.Config defaultConfig = PaymentOptionsConfig.defConfig cP
  selectionAtLeft = CPUtils.placeExpansionSelectorAtLeft cP
  separatedSection = CPUtils.ifSeperatedSections cP
  updatedConfig =
    defaultConfig
      { inputAreaVisibility = (if expand then VISIBLE else GONE)
      , topRowVisibility = GONE
      , inputAreaHeight = WRAP_CONTENT
      , secondaryTextVisibility = GONE
      , primaryText = labelStringMap poG
      , cvvId = "defa"
      , logoUrl = if selectionAtLeft then if expand then "tick" else "circular_radio_button" else logoURLMap poG
      , radioButtonVisibility = if selectionAtLeft then GONE else VISIBLE
      , radioButtonIconUrl = if selectionAtLeft then "" else if expand then "tick" else "circular_radio_button"
      , logoSize = if selectionAtLeft then defaultConfig.radioButtonSize else defaultConfig.logoSize
      , lineSeparatorVisibility = if separatedSection then GONE else VISIBLE
      }
  in (PaymentOptionsConfig.Config updatedConfig)

expansionWrapper :: ∀ w. ConfigPayload -> Array (PrestoDOM (Effect Unit) w) -> Array (PrestoDOM (Effect Unit) w)
expansionWrapper cP child =
  if CPUtils.expandInAccordion cP then
    [ linearLayout
      [ width MATCH_PARENT
      , height WRAP_CONTENT
      , orientation VERTICAL
      ]
      child
    ]
  else UICommons.addCurvedWrapper cP child

indentWrapper :: ∀ w. ConfigPayload -> Array (PrestoDOM (Effect Unit) w) -> Array (PrestoDOM (Effect Unit) w)
indentWrapper cP child =
  if CPUtils.indentExpandedSection cP
  then
  [ linearLayout
    [ width MATCH_PARENT
    , height WRAP_CONTENT
    , margin (Margin 32 0 0 0)
    ]
    child
  ]
  else child

expansionSection :: ∀ w. ConfigPayload -> Array (PrestoDOM (Effect Unit) w) -> Array (PrestoDOM (Effect Unit) w)
expansionSection cP child =
  if CPUtils.ifSeperatedSections cP then
    [ linearLayout
        [ width MATCH_PARENT
        , height WRAP_CONTENT
        , orientation VERTICAL
        , sectionMargin cP
        ] child
    ]
  else child

walletsView :: ∀ w. (Action -> Effect Unit) -> State -> Array (PrestoDOM (Effect Unit) w)
walletsView push state =
  if CPUtils.expandInAccordion state.configPayload then
    expansionSection state.configPayload $
    UICommons.addCurvedWrapper state.configPayload $
    [ PaymentOptionsView.view
        (push <<< SelectPOGroup Wallet)
        (expandedSectionConfig state.configPayload Wallet (state.selectedPaymentGroup == Just Wallet))
        false
        (Just $ indentWrapper state.configPayload $ expandedWalletsView push state)
    ]
  else
    PPUtils.poHeaderLayout
      state.configPayload
      (STR.getWallets $ PPUtils.lang state)
      true
      (expandedWalletsView push state)

expandedWalletsView :: ∀ w. (Action -> Effect Unit) -> State -> Array (PrestoDOM (Effect Unit) w)
expandedWalletsView push state =
  expansionWrapper
    state.configPayload $
    ( if CPUtils.ifCombinedWallets state.configPayload then
        WalletViewUtils.getLinkedWalletsView
          { pushReceived : WalletViewUtils.PPScreenAction push
          , storedWallets : state.savedWallets
          , disabledWallets : [""]
          , selectedWallet : (PPUtils.getWalletNameFromState state)
          , startBtnAnimation : state.startPayment
          , configPayload : state.configPayload
          , defaultOption : state.defaultOption
          , defWallet : if state.defaultOptionType == "WALLET"
                          then state.defaultOption
                          else ""
          , delink : false
          , offers : state.offers
          , hideLastDivider : false
          , amount : state.amount
          , outages : findInstrumentGroup "WALLET" state.outages
          }
      else []
    ) <>
    ( WalletViewUtils.getUnlinkedWalletsView
        { pushReceived : (WalletViewUtils.PPScreenAction push)
        , wallets : state.unlinkedWallets
        , disabledWallets : state.disabledWallets
        , selectedWalletCode : selectedUnlinkedWalletCode -- try passing selected wallet
        , configPayload : state.configPayload
        , ifVisible : true
        , payLaterEligibility : []
        , useLabel
        , defaultOption : state.defaultOption
        , defWallet : if state.defaultOptionType == "WALLET"
                        then state.defaultOption
                        else ""
        , startButtonAnimation : state.startPayment
        , offers : state.offers
        , hideLastDivider : true
        , amount : state.amount
        , mandateType : state.mandateType
        , enableSI : state.enableSI
        , outages : findInstrumentGroup "WALLET" state.outages
        }
    )
  where
  useLabel = true
  selectedUnlinkedWalletCode = case state.selectedPaymentInstrument of
    Just (UnlinkedWallet (WalletTypes.Wallet wallet)) -> wallet.code
    _ -> ""

netBankingView :: ∀ w. (Action -> Effect Unit) -> State -> Array (PrestoDOM (Effect Unit) w)
netBankingView push state =
  if null state.allBanks then []
  else
   if CPUtils.expandInAccordion state.configPayload then
    expansionSection state.configPayload $
    UICommons.addCurvedWrapper state.configPayload $
    [ PaymentOptionsView.view
        (push <<< SelectPOGroup NetBanking)
        (expandedSectionConfig state.configPayload NetBanking (state.selectedPaymentGroup == Just NetBanking))
        false
        ( Just $ indentWrapper state.configPayload $ [
            linearLayout
            [ width MATCH_PARENT
            , height WRAP_CONTENT
            , orientation VERTICAL
            ] nbView
          ]
        )
    ]
  else
    PPUtils.poHeaderLayout state.configPayload (STR.getNetBanking $ PPUtils.lang state) true $
      UICommons.addCurvedWrapper
        state.configPayload
        nbView
  where
  header = STR.getNetBanking $ fromMaybe "" (state.configPayload # unwrap # _.language)
  space = CPUtils.uiCardHorizontalPadding state.configPayload
  outages = findInstrumentGroup "NB" state.outages
  isDown = length downBanks /= 0
  isAllDown = "" `elem` outages
  popBanks = map(\(Bank b) -> b.code) state.popularBanks
  downBanks = filter(\bank -> bank `elem` popBanks) outages
  bankNames = map(\(Bank bank) -> bank.name) $ filter(\(Bank bank) -> bank.code `elem` downBanks) state.popularBanks
  enableOtherBanks = isAllDown && CPUtils.restrictOutagePayment state.configPayload
  MessageConfig.Config defConf = UIConfig.outageMessageConfig state.configPayload
  messageConf = defConf
    { margin = (Margin space 4 space 20)
    , text = if isAllDown then (CPUtils.outageRowMessage state.configPayload) <> " this option" else (CPUtils.outageRowMessage state.configPayload) <> " " <> (arrayJoin bankNames ", ")
    , visibility = if isDown || isAllDown then VISIBLE else GONE
    }
  nbView = ( popularBanksView push state downBanks isAllDown) <>
    [ divider false false state.configPayload] <>
    ( secondaryButtonView push ShowAllNB "Other Banks" state enableOtherBanks) <>
    [ Message.view (push <<< OutageMessageClick) (MessageConfig.Config messageConf) ] <>
    [ PPUtils.proceedToPay push NetBanking state
    , divider false false state.configPayload
    ]


popularBanksView :: ∀ w. (Action -> Effect Unit) -> State -> Array String -> Boolean -> Array (PrestoDOM (Effect Unit) w)
popularBanksView push state downBanks isAllBanksDown = let
  selectedBank = case state.selectedPaymentInstrument of
    Just (NetBank bank) -> Just bank
    _ -> Nothing
  in
  NBUtils.getNetBanksLayout (NBUtils.PPScreenAction push)
    { configPayload : state.configPayload
    , selectedBank
    , popularBanks : state.popularBanks
    , useGrid : true
    , downBanks
    , isAllBanksDown
    }

secondaryButtonView :: ∀ w. (Action -> Effect Unit) -> Action -> String -> State -> Boolean -> Array (PrestoDOM (Effect Unit) w)
secondaryButtonView push action label state isDisabled =
  [ linearLayout
    [ width MATCH_PARENT
    , height if (CPUtils.secondaryButtonHeight cP) < 48 then (V 48) else WRAP_CONTENT
    , onClick push $ const action
    , gravity CENTER_VERTICAL
    , clickable (not isDisabled)
    , alpha if isDisabled then 0.5 else 1.0
    ]
    [ linearLayout (
      [ height $ V (CPUtils.secondaryButtonHeight cP)
      , width $ CPUtils.secondaryButtonWidth cP
      , margin if addMargin then MarginLeft 8 else MarginLeft 0
      , gravity buttonAlignment
      , background $ CPUtils.secondaryButtonBackground cP
      , fontStyle $ CPUtils.secondaryButtonFont cP
      ] <> (if (CPUtils.secondaryButtonCornerRadius cP) == 0.0
                  then [ padding $ Padding hSpace 0 hSpace 0 ]
                  else [ padding $ Padding hP 4 hP 4
                       , cornerRadius (CPUtils.secondaryButtonCornerRadius cP)
                       ]
            )
      )
      [ filler leftFillerVisibility
      , textView
          [ height WRAP_CONTENT
          , width WRAP_CONTENT
          , text label
          , color $ CPUtils.secondaryButtonTextColor cP
          , textSize $ CPUtils.secondaryButtonTextSize cP
          ]
      , filler rightFillerVisibility
      , imageView
          [ height $ V (CPUtils.radioIconSize cP)
          , width $ V (CPUtils.radioIconSize cP)
          , imageUrl "right_arrow"
          , visibility rightFillerVisibility
          ]
      ]
    ]
  ]
  where
  cP = state.configPayload
  hSpace = (CPUtils.uiCardHorizontalPadding cP)

  filler show =
    linearLayout
      [ width $ V 0
      , height MATCH_PARENT
      , weight 1.0
      , visibility show
      ] []

  leftFillerVisibility =
    case CPUtils.secondaryButtonAlignment cP of
      RIGHT -> VISIBLE
      _     -> GONE

  rightFillerVisibility =
    case CPUtils.secondaryButtonUseArrow cP, CPUtils.secondaryButtonAlignment cP of
      true, LEFT -> VISIBLE
      _   , _    -> GONE

  buttonAlignment =
    case CPUtils.secondaryButtonAlignment cP of
      LEFT  -> CENTER_VERTICAL
      _     -> CENTER

  addMargin = case (CPUtils.secondaryButtonWidth cP), (CPUtils.horizontalSpace cP /= 0) of
    WRAP_CONTENT, true -> true
    _, _               -> false

  hP = round (CPUtils.secondaryButtonCornerRadius cP)

divider :: ∀ w. Boolean -> Boolean -> ConfigPayload -> PrestoDOM (Effect Unit) w
divider forceShow show configPayload =
  linearLayout
    [ width MATCH_PARENT
    , height $ V 1
    , background "#efefef"
    , visibility case forceShow, (show' show), (CPUtils.lineSepVisibility configPayload) of
        true, _, _ -> VISIBLE
        _, VISIBLE, VISIBLE -> VISIBLE
        _, _, _ -> GONE
    ]
    []

show' :: Boolean -> Visibility
show' x = if x then VISIBLE else GONE

upiView :: ∀ w. (Action -> Effect Unit) -> State -> Array (PrestoDOM (Effect Unit) w)
upiView push state =
  PPUtils.poHeaderLayout state.configPayload (STR.getUpiHeader $ PPUtils.lang state) true $
    UICommons.addCurvedWrapper
      state.configPayload $
      ( upiAppsView push state isDown) <>
      ( collectView ) <>
      [ Message.view (push <<< OutageMessageClick) (MessageConfig.Config messageConf) ] <>
      [ divider false (not CPUtils.ifSeperatedSections state.configPayload) state.configPayload ]

  where
  lang = fromMaybe "" (state.configPayload # unwrap # _.language)
  header = STR.getUpiHeader lang
  PrimaryButtonConfig.Config b = UIConfig.primaryButtonConfig state.configPayload

  outages = findInstrumentGroup "UPI" state.outages
  isDown = "" `elem` outages

  space = CPUtils.uiCardHorizontalPadding state.configPayload

  MessageConfig.Config defConf = UIConfig.outageMessageConfig state.configPayload
  messageConf = defConf
    { margin = (Margin space 4 space 20)
    , text = (CPUtils.outageRowMessage state.configPayload) <> " this option"
    , visibility = if isDown then VISIBLE else GONE
    }

  bConf =
      b {
      text = STR.getProceed lang
      , margin = (Margin 16 0 16 20)
      , startAnimation = state.startPayment
      }
  collectView =
      if CPUtils.addUPICollectToExpandedUPI state.configPayload
        then upiCollectView push state
        else secondaryButtonView push GotoUpiScreen "Other UPI Options" state isDown -- TODO:: "Add UPI ID" should come from UI.Constant.Str.Default

upiAppsWithOthersOptions :: ∀ w. (Action -> Effect Unit) -> State -> Array (PrestoDOM (Effect Unit) w)
upiAppsWithOthersOptions push state =
  if length state.upiApps == 0 || not state.isUPIEnabled then []
  else
    PPUtils.poHeaderLayout
      state.configPayload
      (STR.getUpiHeader $ lang state)
      true $
      UICommons.addCurvedWrapper state.configPayload $ upiAppsViewWithBtn
  where
  upiOffer = fromMaybe getDummyPaymentOffer $ (filterOffer "UPI" state.offers) !! 0
  outages = findInstrumentGroup "UPI" state.outages
  isDown = "" `elem` outages
  enableOtherApps = isDown && CPUtils.restrictOutagePayment state.configPayload
  offerText = "" --upiOffer.offerText
  offerDescription = ""-- upiOffer.offerDescription
  space = CPUtils.uiCardHorizontalPadding state.configPayload
  MessageConfig.Config defConfig = UIConfig.outageMessageConfig state.configPayload
  outageConfig = defConfig
    { margin = (Margin space 4 space 20)
    , text = (CPUtils.outageRowMessage state.configPayload) <> " this option"
    , visibility = boolToVisibility isDown
    }
  offerMessageConfig = UIConfig.offerMessageConfig offerText offerDescription state.configPayload

  upiAppsViewWithBtn = (upiAppsView push state isDown) <>
  [ divider false false state.configPayload] <>
  ( secondaryButtonView push ShowAllUPIApps "Other UPI Options" state enableOtherApps) <>
  [ Message.view (push <<< (OfferMessageClick UPI) ) offerMessageConfig
  , Message.view (push <<< OutageMessageClick) $ MessageConfig.Config outageConfig
  ]

onlyUPIAppsView :: ∀ w. (Action -> Effect Unit) -> State -> Array (PrestoDOM (Effect Unit) w)
onlyUPIAppsView push state =
  if length state.upiApps == 0 || not state.isUPIEnabled then []
  else
    if CPUtils.expandInAccordion state.configPayload then
      expansionSection state.configPayload $
      UICommons.addCurvedWrapper state.configPayload $
      [ PaymentOptionsView.view
          (push <<< SelectPOGroup UPIApps)
          (expandedSectionConfig state.configPayload UPIApps (state.selectedPaymentGroup == Just UPIApps))
          false
          (Just
            $ indentWrapper state.configPayload
            [ linearLayout
              [ width MATCH_PARENT
              , height WRAP_CONTENT
              , orientation VERTICAL
              ] upiView
            ]
            )
      ]
    else
      PPUtils.poHeaderLayout
        state.configPayload
        (STR.getUpiApp $ lang state)
        true $
        UICommons.addCurvedWrapper state.configPayload $ upiAppsView push state isDown

  where
  outages = findInstrumentGroup "UPI" state.outages
  isDown = "" `elem` outages
  upiView =
    (upiAppsView push state isDown) <>
    [ PPUtils.proceedToPay push UPIApps state ]

onlyUPICollectView :: ∀ w. (Action -> Effect Unit) -> State -> Array (PrestoDOM (Effect Unit) w)
onlyUPICollectView push state =
  if not state.isUPIEnabled then []
  else
    if CPUtils.expandInAccordion state.configPayload then
      expansionSection state.configPayload $
      UICommons.addCurvedWrapper state.configPayload $
      [ PaymentOptionsView.view
          (push <<< SelectPOGroup UPICollect)
          (expandedSectionConfig state.configPayload UPICollect (state.selectedPaymentGroup == Just UPICollect))
          false
          (Just $ indentWrapper state.configPayload $ upiCollectView push state)
      ]
    else
      PPUtils.poHeaderLayout
        state.configPayload
        (STR.getUpiCollect $ lang state)
        true $
        UICommons.addCurvedWrapper state.configPayload $ upiCollectView push state

upiAppsView :: ∀ w. (Action -> Effect Unit) -> State -> Boolean -> Array (PrestoDOM (Effect Unit) w)
upiAppsView push state isUPIDown = [
  UICommons.gridScrollView state.configPayload $
    [ linearLayout (
        [ height WRAP_CONTENT
        , width MATCH_PARENT
        , background "#ffffff"
        ] <>
          if CPUtils.gridViewPadding state.configPayload then
            [ margin $ MarginTop 5
            , padding $ Padding 8 8 8 8
            ]
          else [ padding $ Padding 8 0 0 0 ]
        ) $
        take 5 state.upiApps #
        map
          \upiApp ->
              GridPayOption.gridView
              (push <<< (UPIAppAction $ UPIApp upiApp))
              (getUpiConfig upiApp state isUPIDown)
              false
              Nothing
    ]
  ]

upiCollectView :: ∀ w. (Action -> Effect Unit) -> State -> Array (PrestoDOM (Effect Unit) w)
upiCollectView push state =
  [ linearLayout
      [ height WRAP_CONTENT
      , width MATCH_PARENT
      , background "#FFFFFF"
      ]
      (UPIView.upiLayout (push <<< UPICollectAction) state.upiState)
  ]

addCardView :: ∀ w. (Action -> Effect Unit) -> State -> Array (PrestoDOM (Effect Unit) w)
addCardView push state =
  if CPUtils.expandInAccordion state.configPayload
    then
      expansionSection state.configPayload $
      UICommons.addCurvedWrapper state.configPayload $
      [ PaymentOptionsView.view
          (push <<< SelectPOGroup AddCard)
          (expandedSectionConfig state.configPayload AddCard (state.selectedPaymentGroup == Just AddCard))
          false
          (Just $ cardView)
      ]
    else
      PPUtils.poHeaderLayout
        state.configPayload
        (STR.getCard $ lang state)
        true $
        UICommons.addCurvedWrapper state.configPayload cardView

  where
  (AddCardConfig.Config cnf) = UIConfig.addCardConfig state.configPayload
  addCardConfig = AddCardConfig.Config  cnf { cardMargin = Margin 0 0 0 0
                                            , cardTranslation = 0.0
                                            , cardCornerRadius = 0.0
                                            , buttonAtBottom = false }
  cardView = indentWrapper state.configPayload [ AddCardView.view addCardConfig (push <<< AddCardAction)  state.addCardState ]


mapAppName :: String -> String
mapAppName "Amazon Shopping" = "Amazon"
mapAppName "Google Pay" = "GPay"
mapAppName "MakeMyTrip" = "MMT"
mapAppName "HDFC Bank" = "HDFC"
mapAppName el = el

getUpiConfig :: UPIIntentTypes.UPIApp -> State -> Boolean -> PaymentOptionsConfig.Config
getUpiConfig u@(UPIIntentTypes.UPIApp upiApp) state isUPIDown =
  let
    PaymentOptionsConfig.Config config = (PaymentOptionsConfig.defConfig state.configPayload)
    isSelected = state.selectedPaymentInstrument == (Just $ UPIApp u)
    useTick = CPUtils.useLogoTick state.configPayload
    restrictOutagePayment = CPUtils.restrictOutagePayment state.configPayload
  in
  PaymentOptionsConfig.Config
    config
      { primaryText = mapAppName upiApp.appName
      , logoUrl = upiApp.packageName
      , gridTextHeight = V 24
      , usePackageIcon = true
      , cvvId = "defa"
      , primaryTextFont = if isSelected && not useTick then CPUtils.fontBold state.configPayload else CPUtils.fontRegular state.configPayload
      , gridLogoStroke = if isSelected && useTick then CPUtils.gridSelectedStroke state.configPayload else config.gridLogoStroke
      , tickVisibility = if isSelected && useTick then VISIBLE else GONE
      , tickImageUrl = "grid_tick"
      , gridItemAlpha = if isUPIDown && restrictOutagePayment then 0.4 else 1.0
      , allowGridClick = not (isUPIDown && restrictOutagePayment)
      }

expandedOtherOptionsConfig :: ConfigPayload -> Boolean -> PaymentOptionsConfig.Config
expandedOtherOptionsConfig cP expand = let
  PaymentOptionsConfig.Config defaultConfig = PaymentOptionsConfig.defConfig cP
  updatedConfig =
    defaultConfig
      { inputAreaVisibility = if expand then VISIBLE else GONE
      , topRowVisibility = GONE
      , inputAreaHeight = WRAP_CONTENT
      , secondaryTextVisibility = GONE
      , logoVisibility = GONE
      , logoPadding = Padding 0 0 0 0
      , cvvId = "defa"
      , primaryText = "Other Options" -- TODO: Should come from STR Utils
      , radioButtonIconUrl = if expand then "arrow_up" else "arrow_down"
      , primaryTextFont = CPUtils.headerFontFace cP
      }
  in (PaymentOptionsConfig.Config updatedConfig)
