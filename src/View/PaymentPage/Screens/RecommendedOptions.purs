module View.PaymentPage.Screens.RecommendedOptions where

import HyperPrelude.External(length, (!!),fromMaybe, Maybe(..),Effect,Unit,($),map,not,(&&),(||),(==),(<<<),(#),(<>),(-),(=<<),(/=))
import HyperPrelude.Internal(Length(..), Orientation(..), PrestoDOM, Visibility(..), background, height, horizontalScrollView, linearLayout, orientation, scrollBarX, visibility, width)
import Data.Newtype (unwrap)
import Data.Number.Format (toString)
import Data.String as StrUtils
import Engineering.Helpers.Commons (getNameFromIin, getNbIin)
import PPConfig.Utils as CPUtils
import Remote.Types (PaymentOptions(..))
import UI.Components.PaymentOptions.GridPaymentOptionView as GridPayOption
import UI.Components.PaymentOptionsConfig as PaymentOptionsConfig
import UI.Components.PaymentOptionsView as PaymentOptionsView
import UI.Constant.Str.Default as STR
import View.PaymentPage.Controllers.Types (Action(..), State)
import View.PaymentPage.Screens.Utils as PPUtils
import View.Stock.Container.Commons as UICommons
import View.PaymentPage.Controllers.Utils (walletLinkingStatus)


view :: ∀ w  . (Action  -> Effect Unit) -> State  -> PrestoDOM (Effect Unit) w
view push state =
    linearLayout
    [ width MATCH_PARENT
    , height WRAP_CONTENT
    , orientation VERTICAL
    , visibility if state.voucherApplied
                    then GONE
                    else sectionVisibilty
    ]
    [
        PPUtils.getSectionHeader sectionHeader state.configPayload false
        , linearLayout
            [ width MATCH_PARENT
            , height WRAP_CONTENT
            ]
            ( UICommons.addCurvedWrapper
                state.configPayload
                (renderedLayout)
            )
    ]

    where
        lang = fromMaybe "" (state.configPayload # unwrap # _.language)
        sectionHeader = STR.getRecommendedPayHeader lang
        sectionVisibilty = if CPUtils.ifHighlightVisible state.configPayload
                                then VISIBLE
                                else GONE
        renderedLayout =
            case viewType of
                "grid" -> [ scrollLayout ]
                _ ->  [ itemsLayout ]

        scrollLayout =
            horizontalScrollView
            [ height WRAP_CONTENT
            , width MATCH_PARENT
            , background "#ffffff"
            , scrollBarX false
            ]
            [itemsLayout]

        itemsLayout =
            linearLayout
            [ width MATCH_PARENT
            , height WRAP_CONTENT
            , orientation itemsLayoutOrientation
            , visibility itemsVisibility
            ]
            (   (\(PaymentOptions highlightItem) ->
                    (getHighlightElement
                        push
                        (PaymentOptions highlightItem)
                        state
                    )
                )
                =<< (CPUtils.getHighlight state.configPayload)
            )

        itemsVisibility = if (length $ CPUtils.getHighlight state.configPayload) /= 0 then VISIBLE else GONE
        itemsLayoutOrientation = if viewType == "grid" then HORIZONTAL else VERTICAL
        viewType = CPUtils.getHighlightViewType state.configPayload


getHighlightElement ::
    ∀ w  . (Action  -> Effect Unit)
    -> PaymentOptions
    -> State
    -> Array (PrestoDOM (Effect Unit) w)
getHighlightElement push (PaymentOptions payOptions) state = do
    let enabledElements = (getEnabled (PaymentOptions payOptions))
    let lastElement = fromMaybe "" (enabledElements !! ((length enabledElements) - 1))

    case (StrUtils.toUpper payOptions.visibility) of
        "VISIBLE" -> do
            case (StrUtils.toLower payOptions.po) of
                "wallets" -> do

                    map (\iName ->
                            viewFunction
                            ( push <<< PaymentOptionAction) -- todo :: add proper action
                            (getPrefferedConfig "wallet" iName state (lastElement == iName))
                            false
                            Nothing
                        )
                        enabledElements


                "upi" ->
                    map (\iName ->
                            viewFunction
                            ( push <<< PaymentOptionAction) -- todo :: same as above
                            (getPrefferedConfig "upi" iName state (lastElement == iName))
                            false
                            Nothing
                        )
                        enabledElements

                "nb" ->
                    map(\iName ->
                            viewFunction
                            ( push <<< PaymentOptionAction) -- todo : same as above (PopularBankSelected iName))
                            (getPrefferedConfig "nb" iName state (lastElement == iName))
                            false
                            Nothing
                        )
                        enabledElements

                _ -> []
        _ -> []
    where
        viewFunction = let
            viewType = CPUtils.getHighlightViewType state.configPayload
            in
            case viewType of
                "grid" -> GridPayOption.gridView
                _ ->  PaymentOptionsView.view


getPrefferedConfig :: String -> String -> State -> Boolean -> PaymentOptionsConfig.Config
getPrefferedConfig paymentOption instrumentName state isLastItem  = do
    let lang = fromMaybe "" (state.configPayload # unwrap # _.language)
    let PaymentOptionsConfig.Config config = (PaymentOptionsConfig.defConfig state.configPayload)
    case paymentOption of
        "wallet" -> PaymentOptionsConfig.Config  config { cvvInputVisibility = GONE
            , primaryText =  instrumentName
            , logoUrl = "ic_" <> (StrUtils.toLower instrumentName)
            , radioButtonIconUrl = if false -- TODO :: fix this
                                    then "tick"
                                    else "circular_radio_button"
            , radioButtonSize = V (CPUtils.radioIconSize state.configPayload)
            , secondaryTextVisibility = GONE
            , cvvId = "defa"
            , inputAreaVisibility = if false -- TODO :: fix this
                                        then VISIBLE
                                        else GONE
            , buttonText = if (walletLinkingStatus instrumentName state)
                                then (config.buttonText <> " ₹ " <> (toString state.amount))
                                else (STR.getLink lang)
            , startAnimation =  false -- todo :: complete this
            , lineSeparatorVisibility = if isLastItem
                                            then GONE
                                            else VISIBLE
            }

        "upi" -> PaymentOptionsConfig.Config config { cvvInputVisibility = GONE
                , primaryText = instrumentName
                , logoUrl = "ic_" <> (StrUtils.toLower instrumentName)
                , radioButtonIconUrl = if false
                                        then "tick"
                                        else "circular_radio_button"
                , radioButtonSize = V (CPUtils.radioIconSize state.configPayload)
                , secondaryTextVisibility = GONE
                , cvvId = "defa"
                , inputAreaVisibility = if false
                                            then VISIBLE
                                            else GONE
                , buttonText = (config.buttonText <> " ₹ " <> (toString state.amount))
                , secondaryTextFont = CPUtils.fontRegular state.configPayload
                , startAnimation =  false -- todo :: complete this
                , lineSeparatorVisibility = if isLastItem
                                                then GONE
                                                else VISIBLE
                }

        "nb" -> (PaymentOptionsConfig.Config config
                    {  cvvInputVisibility = GONE
                    , radioButtonIconUrl = if false
                                            then "tick"
                                            else "circular_radio_button"
                    , inputAreaVisibility = if false
                                            then VISIBLE
                                            else GONE
                    , cvvId = "defa"
                    , primaryText = getNameFromIin (getNbIin instrumentName)
                    , logoUrl = "ic_bank_" <> (getNbIin instrumentName)
                    , secondaryTextVisibility = GONE
                    , startAnimation = state.startPayment
                    , lineSeparatorVisibility = if isLastItem
                                                    then GONE
                                                    else VISIBLE
                    }
                  )

        _ -> PaymentOptionsConfig.Config  config
                { cvvInputVisibility = GONE
                , primaryText =  STR.getNone lang
                , cvvId = "defa"
                , secondaryText = STR.getNone lang
                , secondaryTextColor = "#000000"
                , logoUrl = "right_arrow" --"ic_" <> (toLower (fromMaybe "" wallet.wallet))
                }

getEnabled :: PaymentOptions -> Array String
getEnabled (PaymentOptions highlightData) = fromMaybe [] highlightData.onlyEnable
