module View.PaymentPage.Screens.Utils where


import HyperPrelude.External(filter, foldl, find, length, mapWithIndex, (!!), partition, elem,head,Maybe(..), fromMaybe, maybe, isJust,Effect,(/=),(&&),(#),(<>),(==),($),Unit,(-),(<<<),(>),(||),map,not,otherwise,show)
import HyperPrelude.Internal (Gravity(..), Length(..), Margin(..), Orientation(..), Padding(..), PrestoDOM, Visibility(..), alpha, background, color, cornerRadius, fontStyle, height, linearLayout, margin, orientation, padding, text, textSize, textView, translationZ, visibility, width)
import View.PaymentPage.Controllers.Controller
import View.PaymentPage.Controllers.Types
import View.PaymentPage.Controllers.Utils
import Data.Foldable (elem)
import Data.Newtype (unwrap)
import Data.Number.Format (toString)
import Data.String as String
import Data.String.CodeUnits (drop)
import Data.String.Yarn (capWords)
import Engineering.Helpers.Commons (PaymentOffer,getIinFromName, getNbIin, getFormattedIssuerName)
import Validation (reqCVVLength)
import Flow.Utils (filterOffer)
import Flow.Utils (findInstrumentGroup)
import JBridge (getNewIDWithTag, Tag(..))
import PPConfig.Utils (iconSize)
import PPConfig.Utils as CPUtils
import PaymentPageConfig as PPConfig
import Payments.Cards.Utils as CardUtils
import Payments.Wallets.Types (MandateType(..))
import Payments.NetBanking.Utils (Bank(..))
import Payments.UPI.Utils (getAppNameFromVpa, getVpaImage)
import Payments.Wallets.Types as WalletTypes
import PrestoDOM.Types.DomAttributes (Corners(..))
import Remote.Types (ConfigPayload(..), PaymentOptions(..), VisiblePayOption(..))
import Service.EC.Types.Instruments as Instruments
import Service.UPIIntent.Types as UPIIntentTypes
import UI.Components.ListItem.Config as ListItemConfig
import UI.Components.ListItem.View as ListItem
import UI.Components.ListItem.View as ListItemView
import UI.Components.Message.Config as MessageConfig
import UI.Components.PaymentOptionsConfig as PaymentOptionsConfig
import UI.Components.PaymentOptionsView as PaymentOptionsView
import UI.Components.PrimaryButton.Config as PrimaryButtonConfig
import UI.Components.PrimaryButton.View as PrimaryButton
import UI.Config as UIConfig
import UI.Constant.Str.Default as STR
import UI.Utils as UIUtils
import View.PaymentPage.Screens.MoreOption (ReqData(..))

getSectionHeader :: ∀ w. String -> ConfigPayload -> Boolean -> PrestoDOM (Effect Unit) w
getSectionHeader headerText configPayload useAlpha =
  textView
    [ width WRAP_CONTENT
    , height WRAP_CONTENT
    , text headerText
    , fontStyle $ CPUtils.headerFontFace configPayload
    , textSize $ CPUtils.headerSize configPayload
    --, margin m
    , padding (Padding 12 12 0 0)
    , color $ CPUtils.headerColor configPayload
    , alpha if useAlpha then 0.6 else 1.0
    ]
  where
  hspace = CPUtils.horizontalSpace configPayload
  uiPadding = CPUtils.uiCardHorizontalPadding configPayload
  bottomMargin = CPUtils.headerMargin configPayload
  tM = if CPUtils.uiCardTranslation configPayload == 0.0 then 0 else UIUtils.translationMargin
  m = if hspace == 0
    then (Margin uiPadding 0 uiPadding bottomMargin) -- In case there is no hSpace it will add uiCardPadding as margin to left & right
    else (Margin tM 0 0 bottomMargin) -- else it will not add any padding & hSpace which is updated in GetParentLayout will be used

getSavedCardsView :: ∀ w  . (Action  -> Effect Unit) -> State -> Array Instruments.StoredCard -> Boolean -> Array (PrestoDOM (Effect Unit) w)
getSavedCardsView push state savedCards hideLastLine =
  map
    (\singleCard ->
      PaymentOptionsView.view
        (push <<< (StoredCardAction $ StoredCard singleCard))
        (getSavedCardConfig state singleCard hideLastLine (ifLastItem singleCard) mandateEnabledForCard)
        false
        Nothing)
    savedCards
  where
  mandateEnabledForCard = if state.mandateType == Optional then CPUtils.isInstrumentEnabled state.configPayload "cards" else true
  lastInd = (length savedCards) - 1
  Instruments.StoredCard lastCard = fromMaybe CardUtils.dummyCard (savedCards !! lastInd)
  ifLastItem (Instruments.StoredCard item) =
    if CPUtils.ifSeperatedSections state.configPayload then
      item.cardReference == lastCard.cardReference
    else false


-- TODO :: create config for secondary Color
getSavedCardConfig :: State -> Instruments.StoredCard -> Boolean -> Boolean -> Boolean -> PaymentOptionsConfig.Config
getSavedCardConfig state c@(Instruments.StoredCard card) hideLastLine isLastItem isMandateEnabledForCard = let
  configPayload@(ConfigPayload confPayload) = state.configPayload
  selectionAtLeft = CPUtils.placeExpansionSelectorAtLeft configPayload
  isSelected = (Just $ StoredCard c) == state.selectedPaymentInstrument
  --offerCards = filter (\(offer) -> String.toUpper (offer.paymentMethodType) == "CARD") state.offers
  --filteredOfferCards = find (\(offer) -> String.toUpper (offer.paymentMethod) == String.toUpper card.cardBrand) offerCards
  offerText = "" -- maybe "" (\b -> b.offerText) filteredOfferCards
  offerDescription = "" --maybe "" (\b -> b.offerDescription) filteredOfferCards
  offerText' =
    if offerDescription /= "" then
      UIUtils.offerDescHTML offerText state.configPayload
    else offerText
  offerVisible = CPUtils.ifOfferVisible state.configPayload && true
  selectedCardNumber = getCardNumberFromState state
  appendCardNumToPrimaryText = CPUtils.appendCardNumToPrimaryText configPayload
  -- getSelectionConfig sets logoURL
  bankLogoURL = if String.toLower card.cardIssuer == "amex" then "card_type_amex" else ("ic_bank_" <> getIinFromName card.cardIssuer)
  PaymentOptionsConfig.Config config = getSelectionConfig state.configPayload isSelected bankLogoURL
  formattedIssuerName = getFormattedIssuerName card.cardIssuer
  requiredCVVLength = reqCVVLength card.cardBrand -- card type here is credit|debit
  isCVVValid = String.length state.cvv == requiredCVVLength
  mandateSupport = isMandateEnabledForCard && card.mandateSupport == Just true
  cardText
    | appendCardNumToPrimaryText =
      (capWords $ String.toLower card.cardType) <> " Card" <> "..." <> (drop 14 $ card.cardNumber)
    | formattedIssuerName == "" =
      card.cardIssuer <> (if card.cardIssuer == "" then "" else " " ) <> (capWords $ String.toLower card.cardType) <> " Card"
    | otherwise = (formattedIssuerName <> " " <> (capWords $ String.toLower card.cardType))

  cardOutages = findInstrumentGroup "CARD" state.outages
  isDown = ((String.toUpper card.cardIssuer) `elem` cardOutages) || ("" `elem` cardOutages)
  restrictPayment = CPUtils.restrictOutagePayment configPayload

  MessageConfig.Config defMessageConf = config.outageMessageConfig
  messageConf = MessageConfig.Config defMessageConf
    { visibility = if isDown then VISIBLE else GONE
    , text = (CPUtils.outageRowMessage configPayload) <> " " <> card.cardIssuer
    , imageVisibility = VISIBLE
    }

  viewConfig = config
    { cvvInputVisibility = VISIBLE
    , cvvId = card.cardNumber <> "_id" -- patch added due to repeating id  "default"
    , primaryText = cardText
    , showCvvInfoLogo = CPUtils.showCvvInfoLogo configPayload
    , cvvPattern = "^([0-9])+$," <> (show $ reqCVVLength card.cardBrand)
    , cvvWidth = V 50
    --, cvvId = getNewIDWithTag $ SAVED_CARD card.cardFingerprint
    , secondaryText = card.cardNumber
    , secondaryTextColor = "#5C5C5C"
    , secondaryTextVisibility = if appendCardNumToPrimaryText then GONE else VISIBLE
    , inputAreaVisibility = if isSelected then VISIBLE else GONE
    , buttonText = if( state.startPayment) then STR.getProcessingPayment (fromMaybe "" (confPayload.language)) else config.buttonText
    , startAnimation = state.startPayment
    , buttonAlpha = if isCVVValid && not (restrictPayment && isDown) then 1.0 else 0.4
    , buttonClickable = isCVVValid && not (restrictPayment && isDown)
    , tertiaryVisibility = if mandateSupport && state.mandateType /= None then VISIBLE else GONE
    , tertiaryText = if mandateSupport then "Autopay available" else ""
    , tertiaryTextColor = CPUtils.tertiaryFontColor configPayload
    , quantText = offerText'
    , quantTextFromHTML = (offerDescription /= "")
    , enableQuantLayoutClick = (offerDescription /= "")
    , quantVisibility = if (CPUtils.ifOfferVisible state.configPayload && true) then VISIBLE else GONE
    , topRowVisibility = if state.mandateType /= None && mandateSupport then VISIBLE else GONE
    , useTopRowForSI = mandateSupport
    , topRowImage = if state.enableSI && isSelected || state.mandateType == Required then "checkbox1" else "checkbox"
    , topRowText = if state.mandateType == None then "" else "Automatically debit my card to renew subscription for every billing cycle"
    , isTopRowClickable = (not (state.mandateType == Required))
    , topRowImageVisibility = if state.mandateType == Required then GONE else VISIBLE
    , lineSeparatorVisibility = if hideLastLine then
                                    if isLastItem then GONE
                                    else VISIBLE
                                    else VISIBLE
    , primaryLogo = ("card_type_" <> String.toLower card.cardBrand)
    , primaryLogoVisibility = VISIBLE
    , radioButtonGravity = START
    , outageMessageConfig = messageConf
    }
  listPaymentOptionsNew = viewConfig
  in (PaymentOptionsConfig.Config listPaymentOptionsNew )

getUpiAppsView :: ∀ w  . (Action  -> Effect Unit) -> State -> Array UPIIntentTypes.UPIApp -> Array (PrestoDOM (Effect Unit) w)
getUpiAppsView push state upiApps =
  map
    (\upiApp ->
      PaymentOptionsView.view
        (push <<< (UPIAppAction $ UPIApp upiApp ))
        (getUpiAppConfig state upiApp)
        false
        Nothing
    )
  $ upiApps

getNetBankView :: ∀ w. (Action  -> Effect Unit) -> State -> Array Bank -> Array (PrestoDOM (Effect Unit) w)
getNetBankView push state netBanks =
  map
    (\bank@(Bank b) ->
      PaymentOptionsView.view
        (push <<< (NetBankingAction (NetBank bank)))
        (getDefaultNBConfig state b.code (isSelected bank))
        false
        Nothing
    )
  $ netBanks
    where
    isSelected bank = (Just $ NetBank bank) == state.selectedPaymentInstrument

getUpiAppConfig :: State -> UPIIntentTypes.UPIApp -> PaymentOptionsConfig.Config
getUpiAppConfig state u@(UPIIntentTypes.UPIApp upiApp) = let
  isSelected = state.selectedPaymentInstrument == (Just $ UPIApp u )
  --appOffer = find (\(offer) -> String.toUpper offer.paymentMethod == upiApp.appName) state.offers
  offerDesc = "" -- maybe "" (\b -> b.offerText) appOffer
  PaymentOptionsConfig.Config config = (PaymentOptionsConfig.defConfig state.configPayload)
  listPaymentOptionsNew = config
    { cvvInputVisibility = GONE
    , radioButtonIconUrl = if isSelected then "tick" else "circular_radio_button"
    , inputAreaVisibility = if isSelected then VISIBLE else GONE
    , primaryText = upiApp.appName
    , logoUrl = upiApp.packageName
    , primaryTextFont = if isSelected then (CPUtils.fontBold state.configPayload) else (CPUtils.fontRegular state.configPayload)
    , secondaryTextVisibility = GONE
    , usePackageIcon = true
    , quantText = offerDesc
    , quantVisibility = if (CPUtils.ifOfferVisible state.configPayload && true) then VISIBLE else GONE
    , startAnimation = state.startPayment
    , lineSeparatorVisibility = GONE
    , inputAreaHeight = V 60
    , cvvId = upiApp.packageName <> "_id" -- patch added due to repeating id  "default"
    }
  in (PaymentOptionsConfig.Config listPaymentOptionsNew )

getDefaultWalletConfig :: State -> Instruments.StoredWallet -> PaymentOptionsConfig.Config
getDefaultWalletConfig state w@(Instruments.StoredWallet (Instruments.Wallet wallet))= let
  isSelectedWallet = state.selectedPaymentInstrument == (Just $ StoredWallet w)
  PaymentOptionsConfig.Config defConfig = (PaymentOptionsConfig.defConfig state.configPayload)
  walletConfig = defConfig
    { cvvInputVisibility = GONE
    , primaryText = capWords $ String.toLower $ (fromMaybe "" wallet.wallet) <> " Wallet"
    , secondaryText = "Balance: "
    , secondaryText2 = "Rs. " <> (toString $ fromMaybe 0.0 wallet.currentBalance)
    , secondaryTextVisibility = if (wallet.linked == Just true) then VISIBLE else GONE
    , logoUrl = "ic_" <> (String.toLower (fromMaybe "" wallet.wallet))
    , radioButtonIconUrl = if isSelectedWallet
                            then "tick"
                            else "circular_radio_button"
    , inputAreaVisibility = if isSelectedWallet
                                then VISIBLE
                                else GONE
    , startAnimation = state.startPayment
    , secondaryTextColor = "#5C5C5C"
    , secondaryLogoVisibility = GONE
    , topRowVisibility = GONE
    , lineSeparatorVisibility = GONE
    , cvvId = wallet.id <> "_id" -- patch added due to repeating id  "default"
    }
  in (PaymentOptionsConfig.Config walletConfig )

getDefaultCardConfig :: State -> Instruments.StoredCard -> PaymentOptionsConfig.Config
getDefaultCardConfig state c@(Instruments.StoredCard card) = let
  PaymentOptionsConfig.Config defConfig = PaymentOptionsConfig.defConfig state.configPayload
  isSelectedCard = state.selectedPaymentInstrument == (Just $ StoredCard c)
  formattedIssuerName = getFormattedIssuerName card.cardIssuer
  cardConf = defConfig
    { cvvInputVisibility = VISIBLE
    , cvvId = card.cardNumber <> "_id" -- patch added due to repeating id  "default"
    , cvvPattern = "^([0-9])+$," <> (show $ reqCVVLength card.cardType)
    , primaryText = if formattedIssuerName == "" then card.cardIssuer else formattedIssuerName
    , secondaryText = card.cardNumber
    , secondaryTextColor = "#5C5C5C"
    , logoUrl = "ic_bank_" <> getIinFromName card.cardIssuer
    , radioButtonIconUrl = if isSelectedCard then "tick" else "circular_radio_button"
    , inputAreaVisibility = if isSelectedCard then VISIBLE else GONE
    , startAnimation = state.startPayment
    , buttonAlpha = if (String.length state.cvv == 3) then 1.0 else 0.4
    , buttonClickable = if (String.length state.cvv == 3) then true else false
    , tertiaryVisibility = GONE
    , lineSeparatorVisibility = GONE
    , tertiaryTextColor = "#749D47"
    , primaryLogo = ("card_type_" <> String.toLower card.cardBrand)
    , primaryLogoVisibility = VISIBLE
    , topRowVisibility = if card.mandateSupport == Just true then VISIBLE else GONE
    , useTopRowForSI = if card.mandateSupport == Just true then true else false
    , topRowText = "Enable this card for SI"
    , topRowImage = if state.enableSI
                      then "checkbox1"
                      else "checkbox"
    }
  in  (PaymentOptionsConfig.Config cardConf)

getDefaultNBConfig :: State -> String -> Boolean -> PaymentOptionsConfig.Config
getDefaultNBConfig state nbCode isSelectedNb = let
  PaymentOptionsConfig.Config defConfig = PaymentOptionsConfig.defConfig state.configPayload
  nbconf = defConfig
    { cvvInputVisibility = GONE
    , secondaryTextVisibility = GONE
    , lineSeparatorVisibility = GONE
    , inputAreaHeight = V 60
    , primaryTextColor = (CPUtils.fontColor state.configPayload)
    , logoUrl = "ic_bank_" <> (getNbIin nbCode)
    , primaryText = (getBankNameFromCode nbCode state.allBanks)
    , radioButtonIconUrl = if isSelectedNb then "tick" else "circular_radio_button"
    , inputAreaVisibility = if isSelectedNb then VISIBLE else GONE
    , startAnimation = state.startPayment
    }
  in  (PaymentOptionsConfig.Config nbconf)

getDefaultVPAConfig :: State -> Instruments.StoredVPA -> PaymentOptionsConfig.Config
getDefaultVPAConfig state v@(Instruments.StoredVPA storedVpa) = let
    isSelectedUpi = state.selectedPaymentInstrument == (Just $ StoredVPA v)
    PaymentOptionsConfig.Config defConfig = PaymentOptionsConfig.defConfig state.configPayload
    upiconf = defConfig
        { cvvInputVisibility = GONE
        , radioButtonIconUrl = if isSelectedUpi then "tick" else "circular_radio_button"
        , inputAreaVisibility = if isSelectedUpi then VISIBLE else GONE
        , secondaryTextVisibility = VISIBLE
        , logoVisibility = VISIBLE
        , startAnimation = state.startPayment
        , lineSeparatorVisibility = GONE
        , primaryTextColor = (CPUtils.fontColor state.configPayload)
        , primaryText = "UPI - " <> (getAppNameFromVpa storedVpa.vpa)
        , secondaryText = storedVpa.vpa
        , logoUrl = getVpaImage storedVpa.vpa
        , cvvId = storedVpa.id <> "_id" -- patch added due to repeating id  "default"
        }
    in  (PaymentOptionsConfig.Config upiconf)

getDefaultUpiAppConfig :: State -> UPIIntentTypes.UPIApp -> PaymentOptionsConfig.Config
getDefaultUpiAppConfig state u@(UPIIntentTypes.UPIApp upiApp) = let
    isSelected = state.selectedPaymentInstrument == (Just $ UPIApp u)
    PaymentOptionsConfig.Config config = (PaymentOptionsConfig.defConfig state.configPayload)
    listPaymentOptionsNew = config
        { cvvInputVisibility = GONE
        , radioButtonIconUrl = if isSelected then "tick" else "circular_radio_button"
        , inputAreaVisibility = if isSelected then VISIBLE else GONE
        , primaryText = upiApp.appName
        , logoUrl = upiApp.packageName
        , primaryTextFont = if isSelected then (CPUtils.fontBold state.configPayload) else (CPUtils.fontRegular state.configPayload)
        , secondaryTextVisibility = GONE
        , usePackageIcon = true
        , buttonFontStyle = CPUtils.fontBold state.configPayload
        , startAnimation = state.startPayment
        , lineSeparatorVisibility = GONE
        , cvvId = upiApp.packageName <> "_id" -- patch added due to repeating id  "default"
        }
    in (PaymentOptionsConfig.Config listPaymentOptionsNew )

getWalletNameFromState :: State -> String
getWalletNameFromState state =
    case state.selectedPaymentInstrument of
        Just selectedInstrument ->
            case selectedInstrument of
                StoredWallet selectedWallet -> do
                    let (Instruments.StoredWallet (Instruments.Wallet walletDetails)) = selectedWallet
                    (fromMaybe "" walletDetails.wallet)
                _ -> ""
        Nothing ->  ""

getCardNumberFromState :: State -> String
getCardNumberFromState state =
    case state.selectedPaymentInstrument of
      Just selectedInstrument ->
        case selectedInstrument of
          StoredCard selectedCard -> do
            let (Instruments.StoredCard (cardDetails)) = selectedCard
            (cardDetails.cardNumber)
          _ -> ""
      _ -> ""

getDefaultOptionCard :: State -> Maybe Instruments.StoredCard
getDefaultOptionCard state = foldl
                                (\out sc@(Instruments.StoredCard card) ->
                                    case out of
                                        Nothing -> Just sc
                                        c  -> c
                                )
                                Nothing
                                state.savedCards

getDefaultOptionDirectWallet :: State -> Maybe Instruments.StoredWallet
getDefaultOptionDirectWallet state = do
    let defWallet = foldl (\out uw@((WalletTypes.Wallet wallet)) ->
                                case out of
                                    Nothing -> do
                                        if (wallet.code == state.defaultOption &&
                                            (not wallet.directDebitSupport))
                                            then (Just uw)
                                            else Nothing
                                    w  -> w
                       )
                       Nothing
                       state.unlinkedWallets
    case defWallet of
        Nothing -> Nothing
        Just (WalletTypes.Wallet wallet) ->
            Just $ Instruments.StoredWallet $
                    Instruments.Wallet $
                        { wallet : Just wallet.code
                        , token : Nothing
                        , linked : Just false
                        , id : ""
                        , current_balance : Nothing
                        , last_refreshed : Nothing
                        , object : Nothing
                        , currentBalance : Nothing
                        , lastRefreshed : Nothing
                        , status : Nothing
                        , metadata : Nothing
                        , gateway_reference_id : Nothing
                        , error_code : Nothing
                        }

getDefaultOptionUPIApp :: State -> Maybe UPIIntentTypes.UPIApp
getDefaultOptionUPIApp state = do
    foldl
        (\out upiApp@(UPIIntentTypes.UPIApp app) -> do
            case out of
                Nothing -> if app.packageName == state.defaultOption then (Just upiApp) else Nothing
                u -> u
        )
        Nothing
        state.upiApps

getDefaultOptionVPA :: State -> Maybe Instruments.StoredVPA
getDefaultOptionVPA state = foldl
                                (\out sv@(Instruments.StoredVPA vpa) ->
                                    case out of
                                        Nothing -> if vpa.vpa == state.defaultOption then (Just sv) else Nothing
                                        v  -> v
                                )
                                Nothing
                                state.savedVpas


filterCards :: Maybe String -> Maybe String -> Maybe String -> Array Instruments.StoredCard -> Array Instruments.StoredCard
filterCards paymentMethod cardType cardIssuer savedCards = do
    let f1 = maybe savedCards (\pm -> filter (\(Instruments.StoredCard sc) -> sc.cardBrand == pm) savedCards) paymentMethod
    let f2 = maybe f1 (\ct -> filter (\(Instruments.StoredCard sc) -> sc.cardType == ct) f1) cardType
    let f3 = maybe f2 (\bank -> filter (\(Instruments.StoredCard sc) -> sc.cardIssuer == bank) f2) cardIssuer
    f3

defaultOfferDesc :: Instruments.OfferDescription
defaultOfferDesc
    = Instruments.OfferDescription
        { offerDescription : Nothing
        , offerDisplay1 : Nothing
        , offerDisplay2 : Nothing
        , offerDisplay3: Nothing
        }

getDummyOffer :: Instruments.Offer
getDummyOffer =
    Instruments.Offer
        { voucherCode : ""
        , visibleToCustomer : Nothing
        , paymentMethodType : Nothing
        , paymentMethod : Nothing
        , offerDescription : Just (defaultOfferDesc)
        , paymentChannel : Just ["Nothing"]
        , paymentMethodFilter : Just ["Nothing"]
        , applicationMode : Nothing
        , calculationMode : Nothing
        , discountValue : Nothing
        , id :Nothing
        , offerToken : Nothing
        }

getBankNameFromCode :: String -> Array Bank -> String
getBankNameFromCode code banks =
    let (Bank bank) = fromMaybe (Bank {name : "", code : ""}) $ (filter (\(Bank b) -> b.code == code) banks) !! 0 in
    bank.name

removeDefaultWallet :: State -> Array Instruments.StoredWallet
removeDefaultWallet state =
  if state.defaultOptionType /= "WALLET"
    then state.savedWallets
    else filter (\(Instruments.StoredWallet (Instruments.Wallet wallet)) -> wallet.wallet /= (Just state.defaultOption)) state.savedWallets

removeDefaultCard :: State -> Array Instruments.StoredCard
removeDefaultCard state =
  if state.defaultOptionType /= "CARD"
    then state.savedCards
    else filter (\(Instruments.StoredCard card) -> card.cardReference /= state.defaultOption) state.savedCards

getVisiblePaymentOptions :: State -> Array PaymentOptions -> Array VisiblePayOption
getVisiblePaymentOptions state paymentOptions = let
  payOptions =
      filter (\(PaymentOptions po) -> String.toUpper po.visibility == "VISIBLE")
        $ CPUtils.updatePOVisibility "payLater" (length state.payLaterWallets == 0 && length state.savedPayLaterWallets == 0)
        $ CPUtils.updatePOVisibility "wallet" (length state.savedWallets == 0 && length state.unlinkedWallets == 0)
        $ CPUtils.updatePOVisibility "nb" (state.mandateType == Required)
        $ CPUtils.updatePOVisibility "upi" (state.mandateType == Required)
        $ CPUtils.updatePOVisibility "cards" (state.mandateType == Required && (not CPUtils.isInstrumentEnabled state.configPayload "cards")) -- hide card section if mandate is required & mandate on cards is disabled.
          paymentOptions
  in
  payOptions #
  mapWithIndex
    \i (PaymentOptions po) ->
      { po : po.po
      , hideDivider : i == (length payOptions - 1)
      }

poHeaderLayout :: ∀ w. ConfigPayload -> String -> Boolean ->  Array (PrestoDOM (Effect Unit) w) -> Array (PrestoDOM (Effect Unit) w)
poHeaderLayout cP header attachHeader child =
  let ifAttachPOHeaders = CPUtils.attachPOHeaders cP
  in if ifAttachPOHeaders || attachHeader
    then
      [ linearLayout
        [ width MATCH_PARENT
        , height WRAP_CONTENT
        , orientation VERTICAL
        , UIUtils.sectionMargin cP
        ]
        ([ getSectionHeader header cP false ] <> child)
      ]
    else
      [ linearLayout
        [ width MATCH_PARENT
        , height WRAP_CONTENT
        , orientation VERTICAL
        ]
        child
      ]

lang :: State -> String
lang state = fromMaybe "" (state.configPayload # unwrap # _.language)


getExpandedPaymentOptions :: State -> Array PaymentOptions -> { expanded :: Array VisiblePayOption, collapsed :: Array VisiblePayOption}
getExpandedPaymentOptions state pOptions = do
    let wE = CPUtils.ifWalletExpanded state.configPayload
    let nE = CPUtils.getNBViewType state.configPayload
    let uE = CPUtils.getUpiViewType state.configPayload
    let expandedItems = (
        ( if wE then ["wallets"] else []) <>
        ( if nE then ["nb"] else [] ) <>
        ( if uE then ["upi"] else [] ) <> ["upiCollect", "upiApps"])

    let elements = partition (\(PaymentOptions po) -> po.po `elem` expandedItems) pOptions
    let expandedItem = getVisiblePaymentOptions state elements.yes
    let collapsedElems = getVisiblePaymentOptions state elements.no
    { expanded : expandedItem
    , collapsed : collapsedElems
    }

-- | TODO: refractor following config
getLastUsedConfig :: State -> UPIIntentTypes.UPIApp -> Boolean -> Boolean -> PaymentOptionsConfig.Config
getLastUsedConfig state a@(UPIIntentTypes.UPIApp app) selected hideLine = let
    configPayload@(ConfigPayload confPayload) = state.configPayload
    selectionAtLeft = CPUtils.placeExpansionSelectorAtLeft configPayload
    -- offers = state.offers
    --upiOffers = filter (\(offer) -> String.toUpper (offer.paymentMethodType) == "UPI") state.offers
    --appOffers = find (\(offer) -> String.toUpper offer.paymentMethod == app.appName) upiOffers -- todo [PICAF-539] offer handling for savedvpa to be done
    --offerDesc = maybe "" (\b -> b.offerText) appOffers
    -- filteredOfferCards = Just getDummyOffer -- should be handled in [PICAF-539]
    --                     -- find
    --                     --     (\(Instruments.Offer offer) ->
    --                     --         case offer.paymentMethod of
    --                     --             Just typePay -> length (filter (\typePay' -> toUpper typePay' == toUpper vpa.cardBrand) typePay) > 0
    --                     --             _ -> false
    --                     --     )
    --                     --     offerCards
    -- validOffer = fromMaybe getDummyOffer filteredOfferCards -- should be handled in [PICAF-539]
    -- offerDesc = fromMaybe defaultOfferDesc $ validOffer.offerDescription -- should be handled in [PICAF-539]
    -- offerText = ((fromMaybe "" offerDesc.offerDisplay1) <> "\n" <> (fromMaybe "" offerDesc.offerDescription)) -- should be handled in [PICAF-539]
    -- offerLines = length ( String.split (String.Pattern "\n") offerText) -- should be handled in [PICAF-539]
    PaymentOptionsConfig.Config config = getSelectionConfig state.configPayload selected app.packageName
    listPaymentOptionsNew = config
        { cvvInputVisibility = GONE
        , primaryText = app.appName
        , secondaryTextColor = "#5C5C5C"
        , inputAreaVisibility = if selected
                                    then VISIBLE
                                    else GONE
        , startAnimation = state.startPayment
        , quantText = ""
        , quantVisibility = if (CPUtils.ifOfferVisible state.configPayload && true) then VISIBLE else GONE
        , lineSeparatorVisibility = if hideLine
                                    then GONE
                                    else VISIBLE
        , secondaryTextVisibility = GONE
        , primaryLogo = app.packageName
        , primaryLogoVisibility = VISIBLE
        , primaryLogoUsePackageIcon = true
        , cvvId = app.packageName <> "_id" -- patch added due to repeating id  "default"
        }
    in (PaymentOptionsConfig.Config listPaymentOptionsNew )

getSavedVpaConfig :: State -> Instruments.StoredVPA -> Boolean -> Boolean -> PaymentOptionsConfig.Config
getSavedVpaConfig state a@(Instruments.StoredVPA vpaDetails) selected hideLine = let --changedDueToTypeMismatch
    --offerApp = find (\offer -> offer.paymentMethod == "com.google.android.apps.nbu.paisa.user") state.offers
    --hasOffer = maybe false (\_ -> isJust $ String.stripPrefix (String.Pattern "ok") $ fromMaybe "" $ String.split (String.Pattern "@") vpaDetails.vpa !! 1 ) offerApp
    --offerDesc = maybe "" (\b -> b.offerText) offerApp
    --isSelected = comparePayInstrument (SavedVPAItem (StoredVpa storedVpa)) state.selectedPaymentMethod
    PaymentOptionsConfig.Config config = getSelectionConfig state.configPayload selected (getVpaImage vpaDetails.vpa)
    listPaymentOptionsNew = config
        { cvvInputVisibility = GONE
        , primaryText = vpaDetails.vpa
        , secondaryTextColor = "#5C5C5C"
        , inputAreaVisibility = if selected
                                    then VISIBLE
                                    else GONE
        , startAnimation = state.startPayment
        , quantText = ""
        , quantVisibility = if (CPUtils.ifOfferVisible state.configPayload && true) then VISIBLE else GONE
        , lineSeparatorVisibility = if hideLine
                                    then GONE
                                    else VISIBLE
        , secondaryTextVisibility = GONE
        , primaryLogo = getVpaImage vpaDetails.vpa
        , primaryLogoVisibility = VISIBLE
        , cvvId = vpaDetails.id <> "_id" -- patch added due to repeating id  "default"
        }
    in (PaymentOptionsConfig.Config listPaymentOptionsNew )

getLastUsedAppView :: ∀ w  . (Action  -> Effect Unit) -> State -> Array (PrestoDOM (Effect Unit) w)
getLastUsedAppView push state =
    if CPUtils.showLastUsedApp state.configPayload
    then map
            (\app ->
                let isSelected = Just (UPIApp app) == state.selectedPaymentInstrument
                in PaymentOptionsView.view
                    (push <<< UPIAppAction (UPIApp app))
                    (getLastUsedConfig state app isSelected false)
                    false
                    Nothing)
            state.lastUsedApp
    else []

getSavedVpaView :: ∀ w  . (Action  -> Effect Unit) -> State -> Array (PrestoDOM (Effect Unit) w)
getSavedVpaView push state =
    if CPUtils.showSavedVPAinPP state.configPayload
    then map
            (\singleVpa ->
                let isSelected = Just (StoredVPA singleVpa) == state.selectedPaymentInstrument
                in
                    PaymentOptionsView.view
                    (push <<< (StoredVpaAction (StoredVPA singleVpa)))
                    (getSavedVpaConfig state singleVpa isSelected false)
                    false
                    Nothing)
            state.savedVpas
    else []
    -- where
    --     lastInd = (length savedVpas) - 1
    --     (Instruments.StoredVPA lastVpa) = fromMaybe (dummyStoredVpa) (savedVpas !! lastInd)
    --     ifLastItem (Instruments.StoredVPA item) = if CPUtils.ifSeperatedSections state.configPayload
    --                                     then (item.vpa == lastVpa.vpa)
    --                                     else false

proceedToPay :: ∀ w. (Action -> Effect Unit) -> PaymentOptionGroup -> State -> PrestoDOM (Effect Unit) w
proceedToPay push po state =
    PrimaryButton.view
        (push <<< ProceedToPay)
        (PrimaryButtonConfig.Config updatedConfig)
    where
    PrimaryButtonConfig.Config defaultConfig = UIConfig.primaryButtonConfig state.configPayload
    enableButton =  case state.selectedPaymentInstrument of
        Just x -> instrumentToGroup x == po
        _ -> false
    updatedConfig = defaultConfig {
        alpha = if enableButton then 1.0 else 0.5,
        isClickable = enableButton,
        startAnimation = state.startPayment,
        visible = if CPUtils.useButtonForGridSelection state.configPayload then VISIBLE else GONE,
        height = V 48
    }

getSelectionConfig :: ConfigPayload -> Boolean -> String -> PaymentOptionsConfig.Config
getSelectionConfig configPayload expand logo =
    let selectionAtLeft = CPUtils.placeExpansionSelectorAtLeft configPayload
        PaymentOptionsConfig.Config defaultConfig = PaymentOptionsConfig.defConfig configPayload
        logoFirst = CPUtils.primaryLogoFirst configPayload
        conf =
            if selectionAtLeft
            then defaultConfig
                    { logoUrl = if expand then "tick" else "circular_radio_button"
                    , radioButtonVisibility = GONE
                    , logoSize = defaultConfig.radioButtonSize
                    }
            else defaultConfig
                    { logoUrl = logo
                    , radioButtonVisibility = VISIBLE
                    , radioButtonIconUrl = if expand then "tick" else "circular_radio_button"
                    }
    in PaymentOptionsConfig.Config $
        if logoFirst
        then conf { primaryTextMargin = Margin 8 0 0 0
                  , primaryLogoPosition = PaymentOptionsConfig.Left }
        else conf

-- | TODO : this was made for Ola UI parity, need to make this configurable
termsAndConditions :: ∀ w. (Action  -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
termsAndConditions push state =
    let (ListItemConfig.Config defCfg) = ListItemConfig.defaultConfig
        size = 12 --if (screenWidth unit < 600) then 8 else 12
        font = CPUtils.fontRegular state.configPayload
        newCfg =
            defCfg
            { text = "By proceeding you agree to Zipcash’s "
            , buttonText = "Terms & Conditions"
            , textSize = size
            , textColor = "#9E9E9E"
            , buttonTextSize = size
            , buttonColor = "#0064C7"
            , textFont = font
            , buttonFont = font
            , width = MATCH_PARENT
            , radioButtonVisibility = GONE
            , radioButtonUrl = ""
            , buttonPadding = Padding 0 5 0 5
            , centerTextPadding = Padding 0 5 0 5
            }
    in linearLayout
        [ width MATCH_PARENT
        , height WRAP_CONTENT
        , orientation VERTICAL
        , visibility (CPUtils.termsAndConditionsInPP state.configPayload)
        ]
        [ linearLayout
            [ width MATCH_PARENT
            , height (V 1)
            , background "#efefef"
            ]
            []
        , ListItemView.view
            (push <<< TermsAndCondition)
            (ListItemConfig.Config newCfg)
        ]


getOfferUPIApps :: Array PaymentOffer -> Array UPIIntentTypes.UPIApp -> Array UPIIntentTypes.UPIApp
getOfferUPIApps offers apps =  apps --changedDueToTypeMismatch
-- getOfferUPIApps offers apps = do
--     let offerApps = map (\offer -> offer.paymentMethod) $ filterOffer "UPI" offers
--     filter (\(UPIIntentTypes.UPIApp app) -> app.packageName `elem` offerApps) apps

getOfferVPAs :: Array PaymentOffer -> Array Instruments.StoredVPA -> Array Instruments.StoredVPA
getOfferVPAs offers vpas = vpas --changedDueToTypeMismatch
-- getOfferVPAs offers vpas = do
--     let offerApp = find (\offer -> offer.paymentMethod == "com.google.android.apps.nbu.paisa.user") offers
--     filter (\(Instruments.StoredVPA vpa) -> maybe false (\_ -> isJust $ String.stripPrefix (String.Pattern "ok") $ fromMaybe "" $ String.split (String.Pattern "@") vpa.vpa !! 1) offerApp) vpas

getOfferNBs :: Array PaymentOffer -> Array Bank -> Array Bank
getOfferNBs offers banks = banks --changedDueToTypeMismatch
-- getOfferNBs offers banks = do
--     let offerNBs = map (\offer -> offer.paymentMethod) $ filterOffer "NB" offers
--     filter (\(Bank bank) -> bank.code `elem` offerNBs) banks

getUnlinkedOfferWallets :: Array PaymentOffer -> Array WalletTypes.Wallet -> Array WalletTypes.Wallet
getUnlinkedOfferWallets offers wallets =  wallets --changedDueToTypeMismatch
-- getUnlinkedOfferWallets offers wallets = do
--     let offerWallets = map (\offer -> offer.paymentMethod) $ filterOffer "WALLET" offers
--     filter (\(WalletTypes.Wallet wallet) -> wallet.code `elem` offerWallets) wallets

getStoredOfferWallets :: Array PaymentOffer -> Array Instruments.StoredWallet -> Array Instruments.StoredWallet
getStoredOfferWallets offers wallets =  wallets --changedDueToTypeMismatch
-- getStoredOfferWallets offers wallets = do
--     let offerWallets = map (\offer -> offer.paymentMethod) $ filterOffer "WALLET" offers
--     filter (\(Instruments.StoredWallet (Instruments.Wallet wallet)) -> fromMaybe "" wallet.wallet `elem` offerWallets) wallets


removeDefaultNB :: State -> Array Bank
removeDefaultNB state =
    if state.defaultOptionType /= "NB"
        then state.allBanks
        else filter (\(Bank bank) -> bank.code /= state.defaultOption) state.allBanks

removeDefaultUPIApps :: State -> Array UPIIntentTypes.UPIApp
removeDefaultUPIApps state =
    if state.defaultOptionType /= "UPI"
        then state.upiApps
        else filter (\(UPIIntentTypes.UPIApp app) -> app.packageName /= state.defaultOption) state.upiApps

removeDefaultVPA :: State -> Array Instruments.StoredVPA
removeDefaultVPA state =
    if state.defaultOptionType /= "VPA"
        then state.savedVpas
        else filter (\(Instruments.StoredVPA vpa) -> vpa.vpa /= state.defaultOption) state.savedVpas

removeDefaultRedirectWallet :: State -> Array WalletTypes.Wallet
removeDefaultRedirectWallet state =
    if state.defaultOptionType /= "WALLET"
        then ((state.unlinkedWallets <> state.payLaterWallets))
        else filter (\(WalletTypes.Wallet wallet) -> wallet.code /= state.defaultOption) (state.unlinkedWallets <> state.payLaterWallets)

offersPaneView :: ∀ w. (Action -> Effect Unit) -> State -> PrestoDOM (Effect Unit) w
offersPaneView push state=
  ListItem.view (push <<< ShowOffer ) (ListItemConfig.Config offerPaneConfig)
  where
  ListItemConfig.Config defaultConfig = UIConfig.listItemConfig state.configPayload
  offersLength = length state.offers
  -- Instruments.Offer offerData = fromMaybe getDummyOffer $ head state.offers
  -- Instruments.OfferDescription desc = fromMaybe defaultOfferDesc offerData.offerDescription
  -- offerText = fromMaybe "" desc.offerDisplay3
  -- buttonText = if offersLength == 1 then ">"
  --               else if offersLength > 1
  --               then "+" <> (toString $ IntUtils.toNumber (offersLength - 1)) <> " more"
  --               else ""
  offerPaneConfig = defaultConfig {
          height = (V 48)
        , translation = 8.0
        , margin = (Margin 10 5 10 5)
        , padding = (Padding 8 8 8 8)
        , corners = (Corners 4.0 true true true true)
        , visibility = if offersLength > 0 then VISIBLE else GONE
        , imageUrl = "offer_pentagon_icon"
        , imageVisibility = VISIBLE
        , imageSize = (V 18)
        -- , text = offerText
        , textColor = (CPUtils.btnColor state.configPayload)
        , textFont = "Roboto-Medium"
        , textMargin = (Margin 5 0 5 0)
        , buttonVisibility = GONE
        , radioButtonSize = (V 18)
        , radioButtonUrl = "right_arrow_grey"
        , radioButtonVisibility = if offersLength == 1 then VISIBLE else GONE
        -- , sideButtonText = buttonText
        , sideButtonColor = "#4082ed"
        , sideButtonFont = "Roboto-Medium"
        , sideButtonVisibility = if offersLength > 1 then VISIBLE else GONE
        , sidePadding = Padding 0 0 0 0
        , centerTextPadding = Padding 5 0 5 0
      }
