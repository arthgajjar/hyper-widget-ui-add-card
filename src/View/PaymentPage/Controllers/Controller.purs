module View.PaymentPage.Controllers.Controller where


import HyperPrelude.External(any, foldl, (:), (!!),Maybe(..), fromMaybe,Tuple(..),(==),($),(/=),not)
import Data.String as StrUtils
import Flow.Utils (findInstrumentGroup, filterOffer, getCustomerName)
import JBridge as JB
import PPConfig.Utils (getDisabledWallets, showLastUsedApp)
import Payments.Core.Commons (getDummyPaymentOffer)
import Payments.NetBanking.Utils (Bank(..)) as NBUtils
import Payments.Wallets.Utils as WUtils
import PrestoDOM (Eval, continue, continueWithCmd, exit, updateAndExit)
import Remote.Types (MerchantOffer(..))
import Payments.Wallets.Types (MandateType(..))
import Service.EC.Types.Instruments as Instruments
import Service.UPIIntent.Types as UPIIntentTypes
import UI.Components.AddCard.Controller as AddCardController
import UI.Components.AddCard.Types as AddCardTypes
import UI.Components.Message.Controller as Message
import UI.Components.PaymentOptionsController as PayOptions
import UI.Components.Popup.Controller as Popup
import UI.Components.PrimaryButton.Controller as PrimaryButton
import UI.Components.ToolBar.Controller as ToolBar
import UI.Utils (FieldType(..), getFieldTypeID)
import View.PaymentPage.Controllers.CardsControls (handleAddCardAction, handleStoredCardAction)
import View.PaymentPage.Controllers.NetBankingControls (handleNetBankingAction)
import View.PaymentPage.Controllers.Types (Action(..), PaymentInstrument(..), PaymentOptionGroup(..), ScreenInput, ScreenOutput(..), State)
import View.PaymentPage.Controllers.UPIControls (handleStoredVPAAction, handleUPIAppAction, handleUPICollectAction)
import View.PaymentPage.Controllers.Utils (hideKeyboard, selectInstrument, selectPaymentGroup, showKeyboardAt, startPayment)
import View.PaymentPage.Controllers.WalletControls (handleLinkedWalletAction, handleUnlinkedWalletAction)
import View.UPI.Controllers.AddController as UPIController
import Data.Generic.Rep.Show (genericShow)


initialState :: ScreenInput -> State
initialState _input =
	{ savedWallets : _input.savedWallets
	, savedCards : _input.savedCards
	, savedVpas : _input.savedVpas
	, savedNbs : _input.savedNbs
	, allBanks : _input.allBanks
	, popularBanks : _input.popularBanks
	, upiApps : upiApps
	, unlinkedWallets : _input.unlinkedWallets
	, disabledWallets : [""]--fromMaybe [""] (getDisabledWallets _input.configPayload) <> WUtils.addDisableWallets _input.payLaterEligibility --changedDueToTypeMismatch
	, payLaterWallets : _input.payLaterWallets
	, savedPayLaterWallets : _input.savedPayLaterWallets
	, selectedPaymentInstrument : Nothing
	, selectedPaymentGroup : Nothing
	, startPayment : false
	, configPayload : _input.configPayload
	, upiState : UPIController.initialState upiInput
	, vpaUsername : ""
	, cvv : ""
	, offers : _input.offers
	, merchantOffer : _input.merchantOffer
  , cardBinOffers : _input.cardBinOffers
	, voucherApplied : voucher.applied == "true"
	, defaultOptionType : ""
	, defaultOption : ""
	, amount : _input.amount
	, checkoutDetails : _input.checkoutDetails
  , lastUsedApp : usedApps
	, addCardState : AddCardController.initialState addCardInput
	, enableSI : _input.mandateType /= None
  , mandateType : _input.mandateType
  , expandOtherOptions : false
  , isFirstScreen : _input.isFirstScreen
  , isUPIEnabled : _input.isUPIEnabled
  , showOfferDescPopup : false
  , activeOfferDesc : ""
  , outages : _input.outages
	}
	where
  MerchantOffer voucher = _input.merchantOffer
  upiInput = getUpiInput _input
  addCardInput = getAddCardInput _input

  (Tuple upiApps usedApps) =
    if showLastUsedApp _input.configPayload
    then foldl
          (\(Tuple upiList usedAppList) item@(UPIIntentTypes.UPIApp app) ->
            let isUsed = any (\(Instruments.AppUsed usedApp) -> (usedApp.packageName == Just app.packageName)) _input.appUsed
            in if isUsed
              then Tuple upiList (item:usedAppList)
              else Tuple (item:upiList) usedAppList)
          (Tuple [] [])
          _input.upiApps
    else (Tuple _input.upiApps [])

getUpiInput :: ScreenInput -> UPIController.ScreenInput
getUpiInput _input  =   { configPayload : _input.configPayload
												, startCollect : false
												, collectStatus : ""
												, collectTTL : 60
												, customerName : ""
												, isValidVpa : false
												, message : ""
												, vpa : ""
												, amount : _input.amount
												, phoneNumber : _input.checkoutDetails.customer_phone_number
												, useAsWidget : true
												, orderDesc : _input.checkoutDetails.orderDescription
												, paymentCustomerName : getCustomerName _input.checkoutDetails
												, allBanks : []
												, allCards : []
												, allWallets: []
												, mid : ""
												, offers : []
												, outages : []
												, savedVPAs : []
												}

getAddCardInput :: ScreenInput -> AddCardTypes.ScreenInput
getAddCardInput _input  =   { supportedMethods : []
												, cardMethod : AddCardTypes.AddNewCard
												, configPayload : _input.configPayload
												, merchantOffer : _input.merchantOffer
												, amount : _input.amount
												, mandateType : _input.mandateType
												, cardBinOffers : _input.cardBinOffers
												, outages : findInstrumentGroup "CARD" _input.outages
												}

eval :: Action -> State -> Eval Action ScreenOutput State

eval BackPress state =
  if state.showOfferDescPopup then
    continue state {showOfferDescPopup = false}
  else exit OnBackPress

eval (ToolBarAction ToolBar.Clicked) state = exit OnBackPress

eval (PaymentOptionAction (PayOptions.PaymentListItemSelection)) state = continue state

eval (SelectCard (PayOptions.PaymentListItemSelection)) state = exit $ CardScreen state.merchantOffer

eval (SelectWallet (PayOptions.PaymentListItemSelection)) state = exit $ WalletScreen state.merchantOffer

eval (SelectPayLater (PayOptions.PaymentListItemSelection)) state = exit $ PayLaterScreen state.merchantOffer

eval (SelectNb (PayOptions.PaymentListItemSelection)) state = exit $ NetBankingScreen state.merchantOffer

eval (SelectUpi (PayOptions.PaymentListItemSelection)) state = exit $ UPIScreen state.merchantOffer false

eval (SelectEMI (PayOptions.PaymentListItemSelection)) state = exit $ EMIScreen state.merchantOffer

eval (SelectOtherOptions (PayOptions.PaymentListItemSelection)) state = continue state {expandOtherOptions = not state.expandOtherOptions}

eval ShowAllNB state = exit $ NetBankingScreen state.merchantOffer

eval ShowAllUPIApps state = exit $ UPIScreen state.merchantOffer false

eval GotoCollectScreen state = exit $ CollectScreen state.merchantOffer

eval GotoUpiScreen state = exit $ UPIScreen state.merchantOffer false

eval sCardAction@(StoredCardAction instrument action) state = handleStoredCardAction sCardAction state

eval sVpaAction@(StoredVpaAction instrument action) state = handleStoredVPAAction sVpaAction state

eval uwAction@(UnlinkedWalletAction instrument action) state = handleUnlinkedWalletAction uwAction state

eval swAction@(StoredWalletAction instrument action) state = handleLinkedWalletAction swAction state

eval nbAction@(NetBankingAction instrument action) state = handleNetBankingAction nbAction state

eval uAction@(UPIAppAction instrument action) state = handleUPIAppAction uAction state

eval ucAction@(UPICollectAction action) state = handleUPICollectAction ucAction state

eval aCardAction@(AddCardAction action) state = handleAddCardAction aCardAction state

eval (SelectCod action) state =
  case action of
    PayOptions.PaymentListItemSelection ->
      continue $ selectInstrument state CashOnDelivery

    PayOptions.PaymentListAction PayOptions.ButtonClicked ->
      updateAndExit (startPayment state) COD

    _ -> continue state

eval (SelectCardOd action) state =
  case action of
    PayOptions.PaymentListItemSelection ->
      continue $ selectInstrument state CardOnDelivery

    PayOptions.PaymentListAction PayOptions.ButtonClicked ->
      updateAndExit (startPayment state) CardOD

    _ -> continue state

eval (SelectAskAF (PayOptions.PaymentListItemSelection)) state = exit $ AskAFriend

eval (OfferApplied voucher@(MerchantOffer offer)) state = do
    continue state { merchantOffer = voucher, voucherApplied = offer.applied == "true"}

eval (SelectMoreOption (PayOptions.PaymentListItemSelection)) state = exit MoreOptionLaunch

eval (SelectPOGroup po (PayOptions.PaymentListItemSelection)) state =
    let updatedState = (selectPaymentGroup state po)
        piInputID = if po == AddCard then getFieldTypeID CardNumber else if po == UPICollect then  JB.getNewIDWithTag $ JB.VPA else ""
        piInputID' = if piInputID == "" then Nothing else Just piInputID
    in case state.selectedPaymentGroup of
        Just UPICollect ->  continueWithCmd updatedState $ hideKeyboard $ piInputID'
        Just AddCard  -> continueWithCmd updatedState $ hideKeyboard $ piInputID'
        _ -> continueWithCmd updatedState $ showKeyboardAt piInputID

eval (ProceedToPay PrimaryButton.Clicked) state =
    case state.selectedPaymentInstrument of
			Just (NetBank (NBUtils.Bank netBank)) ->
				updateAndExit (startPayment state) $ PayUsingNB netBank.code

			Just (UPIApp (UPIIntentTypes.UPIApp upiApp)) ->
				updateAndExit (startPayment state) $ PayWithUpiApp upiApp.packageName state.merchantOffer

			_ -> continue state

-- eval (OfferMessageClick pGroup action) state = case action of --changedDueToTypeMismatch
--   Message.Click -> let
--     instrumentOffer = fromMaybe getDummyPaymentOffer $ (filterOffer (genericShow pGroup)  state.offers) !! 0
--     offerText = instrumentOffer.offerText
--     offerDescription = instrumentOffer.offerDescription
--     in
--     if (offerDescription /= "") then
--       continue state { activeOfferDesc = offerDescription, showOfferDescPopup = true}
--     else continue state

eval (OfferPayOption (Offers (Instruments.Offer offer)) offerName action) state =
    let exactItem = StrUtils.toUpper (fromMaybe "" offer.paymentMethodType) --changedDueToTypeMismatch
    in case exactItem of
            -- "WALLET" -> let newState = state { selectedUnlinkedWallet = offerName, selectedPaymentMethod = Just (Offers (Instruments.Offer offer))}
            --             in case action of
            --                 PayOptions.PaymentListItemSelection -> continue newState
            --                 PayOptions.PaymentListAction PayOptions.ButtonClicked -> updateAndExit newState $ PayUsingWallet offerName (getWalletToken offerName state) false
            --                 PayOptions.PaymentListItemButtonClick -> updateAndExit newState $ LinkWallet offerName state.merchantOffer
            --                 _ -> continue state
            -- -- "CARD" -> let newState = state { selectedPaymentMethod = Just (Offers (ECRTypes.Offer offer))}
            -- --               mOffer = MerchantOffer
            -- --                       {   applied : "true"
            -- --                         , payment_method_type : Just "CARD"
            -- --                         , payment_method : Just offerName
            -- --                         , payment_card_type : Just ""
            -- --                         , payment_card_issuer : Just ""
            -- --                         , offer_code : Just ""
            -- --                         , amount : Just ""
            -- --                       }
            -- --           in case action of
            -- --                 PayOptions.PaymentListItemSelection -> continue newState
            -- --                 PayOptions.PaymentListItemButtonClick -> updateAndExit state $ ( CardScreen mOffer)
            -- --                 PayOptions.PaymentListAction (PayOptions.CVVChanged str) -> continue state {cvv = str }
            -- --                 _ -> continue state
            -- "NB" -> let newState = state { selectedBank = Just offerName, selectedPaymentMethod = Just (Offers (Instruments.Offer offer))}
            --         in case action of
            --             PayOptions.PaymentListItemSelection -> continue newState
            --             PayOptions.PaymentListAction PayOptions.ButtonClicked -> updateAndExit newState $ (PayUsingNB offerName)
            --             _ -> continue state
            -- "UPI" -> let newState = state {  selectedPaymentMethod = Just (Offers (Instruments.Offer offer))}
            --          in case action of
            --             PayOptions.PaymentListItemSelection -> continue newState
            --             PayOptions.PaymentListAction PayOptions.ButtonClicked -> updateAndExit newState $ (PayUsingUpi offerName)
            --             _ -> continue state
            _ -> continue state

-- eval (SelectPOGroup poGroup action) state = handlePaymentGroupAction poGroup action

eval (OfferPopupAction action) state = case action of
  Popup.OverlayClick -> continue state {showOfferDescPopup = false, activeOfferDesc = ""}
  Popup.HeaderIconClick -> continue state {showOfferDescPopup = false, activeOfferDesc = ""}
  Popup.ButtonOneClick -> continue state {showOfferDescPopup = false, activeOfferDesc = ""}
  _ -> continue state

eval action state = continue state where _ = action
