module View.PaymentPage.Controllers.WalletControls where

import HyperPrelude.External(Maybe(..), fromMaybe, maybe,find,(||),($),(&&),(==),not)
import HyperPrelude.Internal(Eval, continue, continueWithCmd, updateAndExit)
import View.PaymentPage.Controllers.Types (Action(..), PaymentInstrument(..), ScreenOutput(..), State)
import View.PaymentPage.Controllers.Utils (getWalletToken, hideKeyboard, selectInstrument, startPayment, walletLinkingStatus)
import Payments.Wallets.Types as WalletTypes
import Service.EC.Types.Instruments as Instruments
import UI.Components.PaymentOptionsController as PayOptions
import PPConfig.Utils as CPUtils
import Data.String as String
import Data.Foldable (elem)
import Flow.Utils (findInstrumentGroup)

handleUnlinkedWalletAction :: Action -> State -> Eval Action ScreenOutput State
handleUnlinkedWalletAction (UnlinkedWalletAction w@(WalletTypes.Wallet wallet) action) state = let
  downWallets = findInstrumentGroup "WALLET" state.outages
  restrictPayment = CPUtils.restrictOutagePayment state.configPayload
  isDown = (wallet.code `elem` downWallets) || ("" `elem` downWallets)
  in
  case action of
    PayOptions.PaymentListItemSelection -> let
      updatedState = selectInstrument state $ UnlinkedWallet w
      in continueWithCmd updatedState $ hideKeyboard Nothing

    PayOptions.PaymentListItemButtonClick -> let
      ifWalletAlreadyLinked = walletLinkingStatus wallet.code state
      exitAction =
        if ifWalletAlreadyLinked
          then PayUsingWallet
            wallet.code
            (getWalletToken wallet.code state)
            false
            ( state.enableSI &&
              wallet.mandateSupport &&
              (CPUtils.isInstrumentEnabled state.configPayload "wallets")
            )
          else LinkWallet wallet.code state.merchantOffer
      newState = selectInstrument state $ UnlinkedWallet w
      in
      if restrictPayment && isDown then continue state
      else updateAndExit newState $ exitAction

    PayOptions.PaymentListAction PayOptions.SetDefault -> let
      defOpt = state.defaultOption == wallet.code
      in continue state { defaultOption = if defOpt then "" else wallet.code }

    PayOptions.PaymentListAction PayOptions.EnableSI ->
      continue state {enableSI = not state.enableSI}

    PayOptions.PaymentListAction PayOptions.ButtonClicked -> do
      if restrictPayment && isDown then continue state
        else
          updateAndExit (startPayment state) $
            PayUsingWallet
              wallet.code
              ""
              false
              ( state.enableSI
                && wallet.mandateSupport
                &&  (CPUtils.isInstrumentEnabled state.configPayload "wallets")
              )

    PayOptions.QuantLayoutClick -> let
     --offerWallets = find (\offer -> String.toUpper (offer.paymentMethod) == wallet.code) state.offers
     offerDescription = "" --maybe "" (\b -> b.offerDescription) offerWallets
     in
     if offerDescription == "" then continue state
     else continue state {showOfferDescPopup = true, activeOfferDesc = offerDescription}

    _ -> continue state

handleUnlinkedWalletAction action state = continue state

handleLinkedWalletAction :: Action -> State -> Eval Action ScreenOutput State
handleLinkedWalletAction (StoredWalletAction ins@(StoredWallet (Instruments.StoredWallet (Instruments.Wallet storedWallet))) action) state =
  case action of
    PayOptions.PaymentListItemSelection -> let
      updatedState = selectInstrument state ins
      in continueWithCmd updatedState $ hideKeyboard Nothing

    PayOptions.PaymentListAction PayOptions.ButtonClicked -> let
      downWallets = findInstrumentGroup "WALLET" state.outages
      restrictPayment = CPUtils.restrictOutagePayment state.configPayload
      isDown = (wallet `elem` downWallets) || ("" `elem` downWallets)
      val = fromMaybe ""
      wallet = val storedWallet.wallet
      walletToken = val storedWallet.token
      in
      if restrictPayment && isDown
      then continue state
      else updateAndExit (startPayment state) $ PayUsingWallet wallet walletToken false false
        -- check if SI is supported for linked wallet or not
        -- TODO :: replace false with if default or not

    PayOptions.QuantLayoutClick -> let
       --offerWallets = find (\offer -> String.toUpper (offer.paymentMethod) == (fromMaybe "" storedWallet.wallet)) state.offers
       offerDescription = "" --maybe "" (\b -> b.offerDescription) offerWallets
       in
       if offerDescription == "" then continue state
       else continue state {showOfferDescPopup = true, activeOfferDesc = offerDescription}

    _ -> continue state

handleLinkedWalletAction action state = continue state
