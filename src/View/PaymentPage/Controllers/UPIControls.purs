module View.PaymentPage.Controllers.UPIControls where


import HyperPrelude.External(Maybe(..),($),(&&),not,(==),otherwise)
import HyperPrelude.Internal (Eval, continue, updateAndExit)
import PPConfig.Utils as CPUtils
import Service.EC.Types.Instruments as Instruments
import Service.UPIIntent.Types as UPIIntentTypes
import Service.EC.Types.Instruments as Instruments
import UI.Components.EditText.Controller as EditText
import UI.Components.PaymentOptionsController as PayOptions
import UI.Components.PrimaryButton.Controller as PrimaryButton
import View.PaymentPage.Controllers.Types (Action(..), PaymentInstrument(..), ScreenOutput(..), State)
import View.PaymentPage.Controllers.Utils (instrumentToGroup, selectInstrument, startPayment)
import View.UPI.Controllers.AddController as UPIController
import Data.Foldable (elem)
import Flow.Utils (findInstrumentGroup)

handleUPIAppAction :: Action -> State -> Eval Action ScreenOutput State
handleUPIAppAction (UPIAppAction app@(UPIApp (UPIIntentTypes.UPIApp upiApp)) action) state =
  case action of
    PayOptions.PaymentListItemSelection ->
      continue (selectInstrument state app) { selectedPaymentGroup = Nothing }

    PayOptions.PaymentListAction PayOptions.ButtonClicked -> let
      updatedState = selectInstrument state app
      in updateAndExit (startPayment state) $ PayWithUpiApp upiApp.packageName state.merchantOffer

    PayOptions.GridClick ->
      let
        updatedState = selectInstrument state app
        piGroup = instrumentToGroup app
        downUPI = findInstrumentGroup "UPI" state.outages
        isDown = "" `elem` downUPI
        restrictPayment = CPUtils.restrictOutagePayment state.configPayload
        upiAppAction
          | CPUtils.useButtonForGridSelection state.configPayload && Just piGroup == state.selectedPaymentGroup
            = continue updatedState  -- ^ when UPIApps button is clicked, toggle selection
          | Just piGroup == state.selectedPaymentGroup
            = updateAndExit (startPayment updatedState) $ PayWithUpiApp upiApp.packageName state.merchantOffer
            -- | when not using gridSelection, and UPIApps button is clicked, perform payment
          | otherwise = updateAndExit (startPayment state) $ PayWithUpiApp upiApp.packageName state.merchantOffer
            -- | when lastUsedUPIApp view is clicked, perform payment without hiding expanded view
      in
      if isDown && restrictPayment then continue state
      else upiAppAction

    _ -> continue state

handleUPIAppAction action state = continue state

handleUPICollectAction :: Action -> State -> Eval Action ScreenOutput State
handleUPICollectAction (UPICollectAction action) state =
  case action of
    UPIController.EditTextAction (EditText.OnChanged text) ->
      let updatedUPIState = state.upiState {vpa = text}
      in continue state
          { upiState = updatedUPIState
          , selectedPaymentInstrument =  Nothing
          }

    UPIController.PrimaryButtonAction PrimaryButton.Clicked ->
      let updatedUPIState = state.upiState {buttonAnim = true}
          updatedState = state {
              upiState = updatedUPIState,
              selectedPaymentInstrument = Nothing
            }
      in updateAndExit updatedState $ SendWebCollect state.upiState.vpa false state.upiState.saveUpiId
      -- TODO :: add case for default saving

    UPIController.SaveUpiId ->
      let updatedUPIState = state.upiState { saveUpiId = not state.upiState.saveUpiId } --not prev.savedUpiId }
      in continue state { upiState = updatedUPIState }

    _ -> continue state

handleUPICollectAction action state = continue state

handleStoredVPAAction :: Action -> State -> Eval Action ScreenOutput State
handleStoredVPAAction (StoredVpaAction instrument@(StoredVPA (Instruments.StoredVPA sVpa)) action) state =
  case action of
    PayOptions.PaymentListItemSelection ->
      continue $ (selectInstrument state instrument) { selectedPaymentGroup = Nothing }

    PayOptions.PaymentListAction PayOptions.ButtonClicked ->
      updateAndExit (startPayment state) $ SendWebCollect sVpa.vpa false false

    _ -> continue state
handleStoredVPAAction _ state = continue state
