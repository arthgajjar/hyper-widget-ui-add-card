module View.PaymentPage.Controllers.Utils where

import HyperPrelude.External (Effect,filter, foldl, (!!),Maybe(..), fromMaybe, isJust,launchAff_, delay, Milliseconds(..),liftEffect,($),(#),(==),($),bind,discard,pure,unit)
import View.PaymentPage.Controllers.Types (Action(..), PaymentInstrument(..), PaymentOptionGroup(..), ScreenInput, State)
import Service.EC.Types.Instruments as Instruments
import JBridge as JBridge
import Payments.Wallets.Types (Wallet(..)) as WalletTypes
import UI.Utils (resetText)

selectInstrument :: State -> PaymentInstrument ->  State
selectInstrument state instrument =
	if state.selectedPaymentInstrument == Just instrument
		then state { selectedPaymentInstrument = Nothing, cvv = "", enableSI = true }
		else state { selectedPaymentInstrument = Just instrument, cvv = "", enableSI = true }

selectPaymentGroup :: State -> PaymentOptionGroup -> State
selectPaymentGroup state po =
    if state.selectedPaymentGroup == Just po
        then state { selectedPaymentGroup = Nothing, selectedPaymentInstrument = Nothing }
        else state { selectedPaymentGroup = Just po, selectedPaymentInstrument = Nothing }

startPayment :: State -> State
startPayment state = state { startPayment = true }

isDefault :: String -> State -> Boolean
isDefault itemCode state = state.defaultOption == itemCode

-- add definition to this
hideKeyboard :: Maybe String -> Array (Effect Action)
hideKeyboard id =
    let resetFn = case id of
                    Just id -> liftEffect $ resetText [id]
                    Nothing -> liftEffect $ pure unit
    in
	[ do
        launchAff_ do
            _ <- delay (Milliseconds 100.0)
            liftEffect $ JBridge.requestKeyboardHide
            resetFn
        pure ContinueCommand
	]

showKeyboardAt :: String -> Array (Effect Action)
showKeyboardAt id =
    [ do
        _ <- launchAff_ do
            liftEffect $ resetText [id]
            _ <- delay (Milliseconds 300.0)
            liftEffect $ JBridge.requestKeyboardShow id
        pure $ ContinueCommand
    ]

walletLinkingStatus :: String -> State -> Boolean
walletLinkingStatus walletName state =
  getLinkedWalletData walletName state.savedWallets !! 0 # isJust


getLinkedWalletData :: String -> Array Instruments.StoredWallet -> Array Instruments.StoredWallet
getLinkedWalletData walletName savedWallets =
  filter
    (\(Instruments.StoredWallet (Instruments.Wallet item)) ->
      (fromMaybe "" item.wallet) == walletName
    )
  savedWallets

getWalletToken :: String -> State -> String
getWalletToken name state = do
  let selected = filter
                  (\(Instruments.StoredWallet (Instruments.Wallet wallet)) -> wallet.wallet == Just name)
                  state.savedWallets
  let x = selected !! 0
  case x of
      Just (Instruments.StoredWallet (Instruments.Wallet wallet)) -> fromMaybe "" wallet.token
      _ -> ""


getUnlinkedWalletData :: String -> Array WalletTypes.Wallet -> Array WalletTypes.Wallet
getUnlinkedWalletData walletName wallets =
    filter (\(WalletTypes.Wallet item) -> (item.code) == walletName) wallets

getDefaultOptionCard :: ScreenInput -> Maybe Instruments.StoredCard
getDefaultOptionCard _input = do
    foldl
        (\out (Instruments.StoredCard card) ->
            case out of
                Nothing -> if (card.cardReference == _input.defOption)
                            then (Just (Instruments.StoredCard card))
                            else Nothing
                Just c  -> (Just c)
        )
        Nothing
        _input.savedCards

getDefaultOptionWallet :: ScreenInput -> Maybe Instruments.StoredWallet
getDefaultOptionWallet _input = do
     foldl
        (\out (Instruments.StoredWallet (Instruments.Wallet wallet)) ->
            case out of
                Nothing -> if ((fromMaybe "" wallet.wallet) == _input.defOption)
                                then Just (Instruments.StoredWallet (Instruments.Wallet wallet))
                                else Nothing
                Just w  -> (Just w)
        )
        Nothing
        _input.savedWallets

instrumentToGroup :: PaymentInstrument -> PaymentOptionGroup
instrumentToGroup pi = case pi of
  StoredWallet _ ->  Wallet
  UnlinkedWallet _ -> Wallet
  NetBank _ -> NetBanking
  UPIApp _ -> UPIApps
  VPA _ -> UPICollect
  _ -> UPI
