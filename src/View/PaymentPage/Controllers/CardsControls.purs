module View.PaymentPage.Controllers.CardsControls where

import HyperPrelude.External(Maybe(..),launchAff_,liftEffect,($),(==),(&&),bind,pure,not)
import HyperPrelude.Internal (Eval, continue, continueWithCmd, updateAndExit)
import View.PaymentPage.Controllers.Types (Action(..), PaymentInstrument(..), ScreenOutput(..), State)
import View.PaymentPage.Controllers.Utils (hideKeyboard, selectInstrument, showKeyboardAt, startPayment)
import Service.EC.Types.Instruments as Instruments
import JBridge as JB
import UI.Components.AddCard.Types as AddCardTypes
import UI.Components.AddCard.Controller as AddCardController
import UI.Components.PaymentOptionsController as PayOptions
import PPConfig.Utils as CPUtils

handleStoredCardAction :: Action -> State -> Eval Action ScreenOutput State
handleStoredCardAction (StoredCardAction c@(StoredCard cd@(Instruments.StoredCard storedCard)) action) state =
	case action of
		PayOptions.PaymentListItemSelection -> do
			let updatedState = selectInstrument state c
			let cvvID = JB.getNewIDWithTag $ JB.SAVED_CARD storedCard.cardFingerprint
			if state.selectedPaymentInstrument == Just c
				then do
					continueWithCmd updatedState $ hideKeyboard $ Just cvvID
				else
					continueWithCmd updatedState $ showKeyboardAt cvvID

		PayOptions.PaymentListAction (PayOptions.CVVChanged cvv) ->
			continue state { cvv = cvv}

		PayOptions.PaymentListAction PayOptions.ButtonClicked ->
			let ifEnableSI = state.enableSI && storedCard.mandateSupport == Just true && (CPUtils.isInstrumentEnabled state.configPayload "cards")
			in updateAndExit (startPayment state) $ PayUsingCard cd state.cvv false ifEnableSI
			-- TODO :: false to be replaced by default.

		PayOptions.PaymentListAction PayOptions.ToastCVVInfo ->
			continueWithCmd state $
				[ do
						_ <- launchAff_ $
							liftEffect $
								JB.toast "This is the 3 digit number present on the back of your card."
						pure $ ContinueCommand
				]
		-- PayOptions.PaymentListAction PayOptions
		-- TODO :: add text for toast from STR

		PayOptions.PaymentListAction PayOptions.EnableSI ->
			continue state { enableSI = not state.enableSI }


		_ -> continue state

handleStoredCardAction action state = continue state

handleAddCardAction :: Action -> State -> Eval Action ScreenOutput State
handleAddCardAction (AddCardAction action) state =
	case action of
		AddCardTypes.SubmitCard AddCardTypes.AddNewCard ->
			let newState = state.addCardState {startButtonAnimation = true}
			in updateAndExit (state { addCardState  = newState }) $ PayUsingNewCard newState
		cardAction ->
			let newState = AddCardController.eval cardAction state.addCardState
			in continue (state { addCardState  = newState })
handleAddCardAction _ state = continue state
