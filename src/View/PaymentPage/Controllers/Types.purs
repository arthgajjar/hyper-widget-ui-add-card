module View.PaymentPage.Controllers.Types where


import HyperPrelude.External (class Eq, class Show, Maybe)
import Engineering.Helpers.Commons (PaymentOffer)
import Payments.NetBanking.Utils (Bank) as NBUtils
import Payments.Wallets.Types (Wallet) as WalletTypes
import Remote.Types (ConfigPayload, MerchantOffer)
import Service.EC.Types.Instruments as Instruments
import Service.EC.Types.Response as ECResponse
import Service.UPIIntent.Types as UPIIntentTypes
import UI.Components.AddCard.Types as AddCardTypes
import UI.Components.ListItem.Controller as ListItem
import UI.Components.Message.Controller as Message
import UI.Components.PaymentOptionsController as PayOptions
import UI.Components.Popup.Controller as Popup
import UI.Components.PrimaryButton.Controller as PrimaryButton
import UI.Components.ToolBar.Controller as ToolBar
import UI.Utils (ModalAction)
import Data.Generic.Rep (class Generic)
import Data.Generic.Rep.Show (genericShow)
import View.UPI.Controllers.AddController as UPIController
import Payments.Wallets.Types (MandateType)
import Payments.Core.Commons (Checkout)

type ScreenInput =
  { savedWallets :: Array Instruments.StoredWallet
  , savedCards :: Array Instruments.StoredCard
  , savedVpas :: Array Instruments.StoredVPA
  , savedNbs :: Array Instruments.StoredNB
  , configPayload :: ConfigPayload
  , unlinkedWallets :: Array WalletTypes.Wallet
  , allBanks :: Array NBUtils.Bank
  , popularBanks :: Array NBUtils.Bank
  , upiApps :: Array UPIIntentTypes.UPIApp
  , offers :: Array PaymentOffer
  , merchantOffer :: MerchantOffer
  , defOptionType :: String
  , defOption :: String
  , payLaterEligibility :: Array Instruments.PaymentMethodsEligibility
  , payLaterWallets :: Array WalletTypes.Wallet
  , savedPayLaterWallets :: Array Instruments.StoredWallet
  , amount :: Number
  , checkoutDetails :: Checkout
  , mandateType :: MandateType
  , appUsed :: Array Instruments.AppUsed
  , isFirstScreen :: Boolean
  , cardBinOffers :: Array PaymentOffer
  , isUPIEnabled :: Boolean
  , outages :: Array ECResponse.OutagesStatus
  , showQuickPay :: Boolean
  , mandateFeature :: Maybe Boolean
  , cardTypes :: Array String
  , isPayLater :: Boolean
  , mid :: String
  , isOfferSection :: Boolean
  , walletOutages :: Array String
  , creditOutages :: Array String
  , debitOutages :: Array String
  }

data ScreenOutput
  = OnBackPress
  | WalletScreen MerchantOffer
  | UPIScreen MerchantOffer Boolean -- showOnlyUPIApps
  | NetBankingScreen MerchantOffer
  | CardScreen MerchantOffer
  | CollectScreen MerchantOffer
  | PayLaterScreen MerchantOffer
  | LinkWallet String MerchantOffer
  | PayUsingWallet String String Boolean Boolean
  | PayUsingUpi String
  | PayUsingSavedUpi String
  | PayWithUpiApp String MerchantOffer
  | PayUsingCard Instruments.StoredCard String Boolean Boolean -- fourth param is for enableSI
  | PayUsingNB String
  | SendWebCollect String Boolean Boolean
  | PayUsingNewCard AddCardTypes.State
  | COD
  | CardOD
  | AskAFriend
  | MoreOptionLaunch
  | EMIScreen MerchantOffer
  -- | OffersScreen

type State =
  { savedWallets :: Array Instruments.StoredWallet
  , savedCards :: Array Instruments.StoredCard
  , savedVpas :: Array Instruments.StoredVPA
  , savedNbs :: Array Instruments.StoredNB
  , allBanks :: Array NBUtils.Bank
  , popularBanks :: Array NBUtils.Bank
  , upiApps :: Array UPIIntentTypes.UPIApp
  , unlinkedWallets :: Array WalletTypes.Wallet
  , disabledWallets :: Array String
  , payLaterWallets :: Array WalletTypes.Wallet
  , savedPayLaterWallets :: Array Instruments.StoredWallet
  , selectedPaymentInstrument :: Maybe PaymentInstrument
  , selectedPaymentGroup :: Maybe PaymentOptionGroup
  , startPayment :: Boolean
  , configPayload :: ConfigPayload
  , upiState :: UPIController.State
  , cvv :: String
  , vpaUsername :: String
  , offers :: Array PaymentOffer
  , merchantOffer :: MerchantOffer
  , voucherApplied :: Boolean
  , defaultOptionType :: String
  , defaultOption :: String
  , amount :: Number
  , checkoutDetails :: Checkout
  , lastUsedApp :: Array UPIIntentTypes.UPIApp
  , addCardState :: AddCardTypes.State
  , mandateType :: MandateType
  , enableSI :: Boolean
  , expandOtherOptions :: Boolean
  , isFirstScreen :: Boolean
  , cardBinOffers :: Array PaymentOffer
  , isUPIEnabled :: Boolean
  , showOfferDescPopup :: Boolean
  , activeOfferDesc :: String
  , outages :: Array ECResponse.OutagesStatus
  }

data PaymentInstrument
  = StoredCard Instruments.StoredCard
  | StoredWallet Instruments.StoredWallet
  | UnlinkedWallet WalletTypes.Wallet
  | NetBank NBUtils.Bank
  | UPIApp UPIIntentTypes.UPIApp
  | VPA String
  | StoredVPA Instruments.StoredVPA
  | CashOnDelivery
  | CardOnDelivery
  | AskAF
  | Offers Instruments.Offer

data PaymentOptionGroup
  = Wallet
  | NetBanking
  | UPI
  | AddCard
  | UPIApps
  | UPICollect


data Action
  = BackPress
  | PaymentOptionAction PayOptions.Action -- is this required ?
  | ToolBarAction ToolBar.Action
  | SelectCard PayOptions.Action
  | SelectWallet PayOptions.Action
  | SelectUpi PayOptions.Action
  | SelectCod PayOptions.Action
  | SelectCardOd PayOptions.Action
  | SelectAskAF PayOptions.Action
  | SelectNb PayOptions.Action
  | SelectMealCard PayOptions.Action
  | SelectPayLater PayOptions.Action
  | SelectOtherOptions PayOptions.Action
  | ContinueCommand -- for executing commands
  | StoredCardAction PaymentInstrument PayOptions.Action
  | StoredWalletAction PaymentInstrument PayOptions.Action
  | StoredVpaAction PaymentInstrument PayOptions.Action
  | UnlinkedWalletAction WalletTypes.Wallet PayOptions.Action
  | NetBankingAction PaymentInstrument PayOptions.Action
  | ShowAllNB
  | UPIAppAction PaymentInstrument PayOptions.Action
  | UPICollectAction UPIController.Action
  | SelectPOGroup PaymentOptionGroup PayOptions.Action
  | AddCardAction AddCardTypes.Action
  | ProceedToPay PrimaryButton.Action
  | GotoCollectScreen
  | ShowAllUPIApps
  | GotoUpiScreen
  | SelectMoreOption PayOptions.Action
  | OfferPayOption PaymentInstrument String PayOptions.Action
  | OfferApplied MerchantOffer
  | OverlayClick ModalAction
  | TermsAndCondition ListItem.Action
  | ShowOffer ListItem.Action
  | SelectEMI PayOptions.Action
  | OfferPopupAction Popup.Action
  | OutageMessageClick Message.Action -- currently no implementation for this action
  | OfferMessageClick PaymentOptionGroup Message.Action

derive instance eqPaymentInstrument :: Eq PaymentInstrument

derive instance eqPaymentOptionGroup :: Eq PaymentOptionGroup
derive instance genericPaymentOptionGroup :: Generic PaymentOptionGroup _
instance showPaymentOptionGroup :: Show PaymentOptionGroup where show = genericShow
