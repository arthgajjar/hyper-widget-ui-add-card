module View.PaymentPage.Controllers.NetBankingControls where

import HyperPrelude.External
import HyperPrelude.Internal (Eval, continue, updateAndExit)
import View.PaymentPage.Controllers.Types (Action(..), PaymentInstrument(..), ScreenOutput(..), State)
import View.PaymentPage.Controllers.Utils (selectInstrument, startPayment)
import UI.Components.PaymentOptionsController as PayOptions
import Payments.NetBanking.Utils (Bank(..))
import PPConfig.Utils as CPUtils
import Data.Foldable (elem)
import Flow.Utils (findInstrumentGroup)


handleNetBankingAction :: Action -> State -> Eval Action ScreenOutput State
handleNetBankingAction (NetBankingAction nb@(NetBank (Bank netBank)) action) state =
  case action of
    PayOptions.PaymentListItemSelection ->
      continue $ selectInstrument state nb

    PayOptions.PaymentListAction PayOptions.ButtonClicked ->
      let updatedState = selectInstrument state nb
      in updateAndExit (startPayment updatedState) $ PayUsingNB netBank.code

    PayOptions.GridClick ->
      let updatedState = selectInstrument state nb in
      if CPUtils.useButtonForGridSelection state.configPayload
        then continue state
        else updateAndExit (startPayment updatedState) $ PayUsingNB netBank.code

    _ -> continue state

handleNetBankingAction action state = continue state
