module Payments.Cards.Utils where

import Prelude

import Data.Array (elem, filter)
import Data.Array as DA
import Data.Maybe (Maybe(..), fromMaybe)
import Data.Newtype (unwrap)
import Service.EC.Types.Instruments (StoredCard(..), MerchantPaymentMethod(..))
import Service.EC.Types.Response (PaymentSourceResp(..))
import Data.String.CodeUnits (indexOf, length, splitAt)
import Remote.Types (CardInfo(..), ConfigPayload(..))
import UI.Components.AddCard.Types as AddCardTypes
import Engineering.Helpers.Commons (filterGatewayRef)
import Payments.Wallets.Types (MandateType(..))
import Data.String.Pattern (Pattern(..))

newtype StoredCardPayment = StoredCardPayment {
      cardToken :: String
    , cardSecurityCode :: String
    , isEmi :: Maybe Boolean
    , emiBank :: Maybe String
    , emiTenure :: Maybe String
    , authType :: Maybe String
    , shouldCreateMandate :: Maybe Boolean
    , cardIsin :: String
    , emiType :: Maybe String
}

newtype NewCardPayment = NewCardPayment {
      cardNumber :: String
    , cardExpMonth :: String
    , cardExpYear :: String
    , nameOnCard :: String
    , cardSecurityCode :: String
    , saveToLocker :: Boolean
    , isEmi :: Maybe Boolean
    , emiBank :: Maybe String
    , emiTenure :: Maybe Int
    , authType :: Maybe String
    , shouldCreateMandate :: Maybe Boolean
    , emiType :: Maybe String
}

data CardToken = CardToken String

getMandateEnabledCards :: Array StoredCard -> Array StoredCard
getMandateEnabledCards storedCards =
  filter
    (\(StoredCard card) -> fromMaybe false card.mandateSupport)
    storedCards

getStoredCards :: PaymentSourceResp -> Array String ->Array StoredCard
getStoredCards (PaymentSourceResp resp) gatewayRefId = do
  case (DA.length gatewayRefId) of
    0 -> resp.cards
    _-> do
      let merchantPaymentMethods = resp.merchantPaymentMethods
      let pos = filterGatewayRef merchantPaymentMethods gatewayRefId
      let cardOptions = --filter (\ (MerchantPaymentMethod method) -> maybe true (\i -> gatewayRefId `elem` i) method.supportedReferenceIds)
                         filter (\ (MerchantPaymentMethod method) -> method.paymentMethodType == "CARD") pos
      let cardq= map (\ (MerchantPaymentMethod method) -> (method.paymentMethod)) cardOptions

      let cards= filter (\(StoredCard card) -> do
                                            let issuer= case card.cardBrand of
                                                          "MASTERCARD" -> "MASTER"
                                                          _-> card.cardBrand

                                            case  ((\i -> issuer `elem` i) cardq) of
                                              true -> true
                                              _ -> false
                                          ) resp.cards
      cards


getValidCardTypes  :: PaymentSourceResp -> Array String -> Array String
getValidCardTypes (PaymentSourceResp resp) gatewayRefId = do
  let merchantPaymentMethods = resp.merchantPaymentMethods

      pos = filterGatewayRef merchantPaymentMethods gatewayRefId
      cardOps = --filter (\ (MerchantPaymentMethod method) -> maybe true (\i -> gatewayRefId `elem` i) method.supportedReferenceIds)
                     filter (\ (MerchantPaymentMethod method) -> method.paymentMethodType == "CARD") pos
      cards= map (\ (MerchantPaymentMethod method) -> (method.paymentMethod)) cardOps
  cards

updateMandateValue :: ConfigPayload -> MandateType -> Array StoredCard -> Array StoredCard
updateMandateValue cp mandateType arr =
  if mandateType == Required then
    let cardEnabled = isInstrumentEnabled cp "cards" in
    if cardEnabled then arr
    else arr #
      map
        \(StoredCard card) -> StoredCard card { mandateSupport = Just false}
  else arr


isInstrumentEnabled :: ConfigPayload -> String -> Boolean
isInstrumentEnabled (ConfigPayload cp) instrument = false
  -- isJust $ cp.mandateInstruments #
  --   find \inst -> String.toLower inst == String.toLower instrument


dummyCard :: StoredCard
dummyCard =
  StoredCard
    { nickname: ""
    , nameOnCard: ""
    , expired: false
    , cardType: ""
    , cardToken: ""
    , cardReference: ""
    , cardNumber: ""
    , cardIssuer: ""
    , cardIsin: ""
    , cardFingerprint: ""
    , cardExpYear: ""
    , cardExpMonth: ""
    , cardBrand: ""
    , count: Nothing
    , lastUsed: Nothing
    , rating: Nothing
    , mandateSupport: Nothing
    }

dummyCardInfo :: CardInfo
dummyCardInfo =
  CardInfo
    { id: ""
    , object: ""
    , bank: ""
    , country: ""
    , "type": ""
    , direct_otp_support: Nothing
    , mandate_support: Just true
    }

getNewCardPaymentPayload :: AddCardTypes.State -> NewCardPayment
getNewCardPaymentPayload addCardState =
  let
    expiryDate = addCardState.formState.expiryDate.value

    indexOfDivider = expiryDate # indexOf (Pattern "/") # fromMaybe (length expiryDate)

    expiryMonth = (expiryDate # splitAt indexOfDivider).before

    expiryYear = (expiryDate # splitAt (indexOfDivider + 1)).after

    saveCard = addCardState.saveCard

    enableSI = addCardState.enableSI

    isDirectOtpSupported = (fromMaybe dummyCardInfo (addCardState.cardInfo) # unwrap # _.direct_otp_support) == Just true
  in
    NewCardPayment
      { cardNumber: addCardState.formState.cardNumber.value
      , cardExpMonth: expiryMonth
      , cardExpYear: expiryYear
      , nameOnCard: addCardState.formState.name.value
      , cardSecurityCode: addCardState.formState.cvv.value
      , saveToLocker: saveCard
      , isEmi: Nothing
      , emiBank: Nothing
      , emiTenure: Nothing
      , authType: if isDirectOtpSupported then Just "OTP" else Nothing
      , shouldCreateMandate: if enableSI then Just enableSI else Nothing
      , emiType : Nothing
      }
