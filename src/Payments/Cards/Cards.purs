module Payments.Cards where

import Prelude

import Data.Either (Either(..))
import Data.Maybe (Maybe(..))
import Data.String (length)
import Service.EC.Types.Request (ECPayload(..), NBPayload(..), SavedCardPayload(..), ListCardsPayload(..), DeleteCardPayload(..), NewCardPayload(..), SDKPayload(..))
import Service.EC.Types.Response (ErrorResult, SDKResponse(..), OrderStatusResp(..))
import Effect.Class (liftEffect)
import Payments.Adapter as Adapter
import Payments.Cards.Utils (CardToken(..), NewCardPayment(..), StoredCardPayment(..))
import Payments.Core.Commons (Checkout, getEndUrls, _merchant_key_id, _order_details, _signature, apiError, getCheckoutDetails, getValueFromPayload, getValueFromPayload', mismatchError)
import Payments.Wallets.Types (WalletTransaction(..))
import Presto.Core.Types.Language.Flow (Flow, doAff)

mkPayReqStoredCard :: StoredCardPayment -> Maybe String -> Maybe String -> Maybe String -> Checkout -> ECPayload
mkPayReqStoredCard (StoredCardPayment storedCard) orderDetails signature merchantKeyId checkout = SavedCard $
  SavedCardPayload
    { action : "cardTxn"
    , endUrls : getEndUrls $ getValueFromPayload' "endUrls"
    , amount : Just $ show checkout.amount
    , orderId : checkout.order_id
    , cardBin : Just storedCard.cardIsin
    , cardToken : storedCard.cardToken
    , cardSecurityCode : storedCard.cardSecurityCode
    , authType : storedCard.authType
    , isEmi : storedCard.isEmi
    , emiBank : storedCard.emiBank
    , emiTenure : storedCard.emiTenure
    , smsPermissionDenyLimit : if storedCard.authType == Just "OTP" then Just 3 else Nothing
    , offerToken : Just checkout.offerCode
    , paymentMethod : Nothing
    , shouldCreateMandate : storedCard.shouldCreateMandate
    , orderDetails
    , signature
    , merchantKeyId
    , cardAlias : Nothing
    , maskedCardNumber : Nothing
    , clientAuthToken : if storedCard.authType == Just "OTP" then Just checkout.session_token else Nothing
    , saveToLocker : Nothing
    , emiType : storedCard.emiType
    }

mkPayReqNewCard :: NewCardPayment -> Maybe String -> Maybe String -> Maybe String -> Checkout -> ECPayload
mkPayReqNewCard (NewCardPayment newCard) orderDetails signature merchantKeyId checkout = NewCard $
  NewCardPayload
    { action : "cardTxn"
    , orderId : Just checkout.order_id
    , amount : Just $ show checkout.amount
    , endUrls : Just $ getEndUrls $ getValueFromPayload' "endUrls"
    , cardNumber : newCard.cardNumber
    , cardExpMonth : newCard.cardExpMonth
    , cardExpYear : newCard.cardExpYear
    , cardSecurityCode : newCard.cardSecurityCode
    , nameOnCard : Just newCard.nameOnCard
    , saveToLocker : Just newCard.saveToLocker
    , isEmi : newCard.isEmi
    , emiBank : newCard.emiBank
    , emiTenure : newCard.emiTenure
    , authType : newCard.authType
    , smsPermissionDenyLimit : if newCard.authType == Just "OTP" then Just 3 else Nothing
    , offerToken : Just checkout.offerCode
    , paymentMethod : Nothing
    , redirectAfterPayment : Nothing
    , shouldCreateMandate : newCard.shouldCreateMandate
    , orderDetails
    , signature
    , merchantKeyId
    , clientAuthToken : if newCard.authType == Just "OTP" then Just checkout.session_token else Nothing
    , cardAlias : Nothing
    , maskedCardNumber : Nothing
    , emiType : newCard.emiType
    }

mkReqListCard :: Checkout -> ECPayload
mkReqListCard checkout = ListCards $
  ListCardsPayload
    { action : "cardList"
    , clientAuthToken : checkout.session_token
    }

mkReqDeleteCard :: CardToken -> Checkout -> ECPayload
mkReqDeleteCard (CardToken cardToken) checkout = DeleteCard $
  DeleteCardPayload
    { action : "deleteCard"
    , cardToken : cardToken
    , clientAuthToken : checkout.session_token
    , cardAlias : Nothing
  }

mkStoredCardTrans :: StoredCardPayment -> Flow (Maybe SDKResponse) --Flow (Either ErrorResult ECRemoteTypes.OrderStatusResp)
mkStoredCardTrans storedCard = do
  orderDetails <- getValueFromPayload _order_details
  signature <- getValueFromPayload _signature
  merchantKeyId <- getValueFromPayload _merchant_key_id
  checkout <- doAff do liftEffect getCheckoutDetails
  Adapter.startECFlow $ mkPayReqStoredCard storedCard orderDetails signature merchantKeyId checkout

mkNewCardTrans :: NewCardPayment -> Flow (Maybe SDKResponse)
mkNewCardTrans newCard = do
  orderDetails <- getValueFromPayload _order_details
  signature <- getValueFromPayload _signature
  merchantKeyId <- getValueFromPayload _merchant_key_id
  checkout <- doAff do liftEffect getCheckoutDetails
  Adapter.startECFlow $ mkPayReqNewCard newCard orderDetails signature merchantKeyId checkout

listStoredCards :: Flow (Maybe SDKResponse)
listStoredCards = do
  checkout <- doAff do liftEffect getCheckoutDetails
  Adapter.startECFlow $ mkReqListCard checkout

deleteCardReq :: CardToken -> Flow (Maybe SDKResponse)
deleteCardReq cardToken = do
  checkout <- doAff do liftEffect getCheckoutDetails
  Adapter.startECFlow $ mkReqDeleteCard cardToken checkout
