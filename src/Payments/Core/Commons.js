exports["getValueFromPayload'"] = getKeyValueFromPayload;

function getKeyValueFromPayload(key) {
	var payload = (window.__payload);
	if (typeof payload != "undefined") {
		if (typeof payload[key] != "undefined") {
			return "" + payload[key];
		} else {
			// console.log("Value not found for key " + key + " in payload: ");
		}
	} else {
		console.log("Payload not found");
	}
	return "";
}

exports.getMerchantViewUrl=function(){
	if (window.addMerchantView)
		return window.addMerchantView
	else
		return ""
}

exports["getValueFromPayload''"] = function (just) {
	return function (nothing) {
		return function (key) {
			return function () {
				var payload = window.__payload;
				if (typeof payload != "undefined") {
					if (typeof payload[key] != "undefined") {
						return just(payload[key]);
					} else {
						//console.log("Value not found for key " + key + " in payload: ");
					}
				} else {
					console.log("Payload not found");
				}
				return nothing;
			}
		}
	}
}
exports.getWalletGateway = function(wallet){
	try{
		var order_details= JSON.parse(window.__payload.order_details);
		var keys = Object.keys(order_details);
		if (wallet.toUpperCase()=="FREECHARGE"){
			if (keys.includes("metadata."+wallet.toUpperCase()+"_V2:gateway_reference_id")){
				return order_details["metadata."+wallet.toUpperCase()+"_V2:gateway_reference_id"]
			}
		}
		
		if (keys.includes("metadata."+wallet.toUpperCase()+":gateway_reference_id")){
			return order_details["metadata."+wallet.toUpperCase()+":gateway_reference_id"]
		}
		if (keys.includes("metadata.JUSPAY:gateway_reference_id")){
			return order_details["metadata.JUSPAY:gateway_reference_id"]
		}
	}
	catch(e){
		return ""
	}
	
	
}

exports.getCheckoutPayload = function () {
	var order_details = {};
	try {
		order_details = JSON.parse(getKeyValueFromPayload("order_details"));
		console.log("from foreign",order_details["options.create_mandate"])
	} catch (err) {
		console.log(err);
	}
	return {
		order_token: getKeyValueFromPayload("session_token"),
		amount: order_details.amount ? order_details["amount"] : getKeyValueFromPayload("amount"),
		order_id: order_details.order_id ? order_details["order_id"] : getKeyValueFromPayload("order_id"),
		merchant_id: order_details.merchant_id ? order_details["merchant_id"] : getKeyValueFromPayload("merchant_id"),
		offerCode: getKeyValueFromPayload("offerCode"),
		customer_phone_number: getKeyValueFromPayload("mobile_number"),
		customer_id: order_details.customer_id ? order_details["customer_id"] : getKeyValueFromPayload("customer_id"),
		customer_email: order_details.customer_email ? order_details["customer_email"] : getKeyValueFromPayload("customer_email"),
		customer_first_name: order_details.first_name ? order_details["first_name"] : getKeyValueFromPayload("firstName"),
		customer_last_name: order_details.last_name ? order_details["last_name"] : getKeyValueFromPayload("lastName"),
		session_token: getKeyValueFromPayload("session_token"),
		client_id: getKeyValueFromPayload("client_id"),
		itemCount: getKeyValueFromPayload("udf_itemCount"),
		cashEnabled: getKeyValueFromPayload("udf_cashDisabled") == "true" ? "false" : "true ",
		disabledWallets: getKeyValueFromPayload("udf_disabled_methods"),
		activity_recreated: getKeyValueFromPayload("activity_recreated"),
		environment: getKeyValueFromPayload("environment"),
		widget_name: getKeyValueFromPayload("widget_name"),
		gateway_reference_id : getReferenceIds(order_details),// order_details["metadata.JUSPAY:gateway_reference_id"] ? order_details["metadata.JUSPAY:gateway_reference_id"] : "",
		order_details : getKeyValueFromPayload("order_details"),
		subventionAmount : getKeyValueFromPayload("metadata.subvention_amount"),
		signature : getKeyValueFromPayload("signature"),
		merchant_key_id : getKeyValueFromPayload("merchant_key_id"),
		language : getKeyValueFromPayload("language"),
		merchant_name : getKeyValueFromPayload("merchant_ui"),
		guest_login_url : getKeyValueFromPayload("guest_login_url"),
		action : getKeyValueFromPayload("action"),
		request_id : getKeyValueFromPayload("requestId"),
		orderDescription : order_details["description"] ? order_details["description"] : "",
		create_mandate : order_details["options.create_mandate"] ? order_details["options.create_mandate"] : "",
		mandate_max_amount : order_details["mandate_max_amount"] ? order_details["mandate_max_amount"] : "",
		"metadata.PAYTM_V2:SUBSCRIPTION_EXPIRY_DATE": order_details["metadata.PAYTM_V2:SUBSCRIPTION_EXPIRY_DATE"] ? order_details["metadata.PAYTM_V2:SUBSCRIPTION_EXPIRY_DATE"] : ((new Date().getFullYear()) + 2).toString() + '-' + (((new Date().getMonth()) + 1)<10 ? '0':'') + ((new Date().getMonth()) + 1).toString() + '-' + ((new Date().getDate()) + 1).toString(),
		"metadata.PAYTM_V2:SUBSCRIPTION_FREQUENCY_UNIT": order_details["metadata.PAYTM_V2:SUBSCRIPTION_FREQUENCY_UNIT"] ? order_details["metadata.PAYTM_V2:SUBSCRIPTION_FREQUENCY_UNIT"] : "MONTH",
		"metadata.PAYTM_V2:SUBSCRIPTION_FREQUENCY": order_details["metadata.PAYTM_V2:SUBSCRIPTION_FREQUENCY"] ? order_details["metadata.PAYTM_V2:SUBSCRIPTION_FREQUENCY"] : "1",
		"metadata.PAYTM_V2:SUBSCRIPTION_START_DATE": order_details["metadata.PAYTM_V2:SUBSCRIPTION_START_DATE"] ? order_details["metadata.PAYTM_V2:SUBSCRIPTION_START_DATE"] : ((new Date().getFullYear())).toString() + '-' + (((new Date().getMonth()) + 1)<10 ? '0':'') + ((new Date().getMonth()) + 1).toString() + '-' + ((new Date().getDate()) + 1).toString(),
		"metadata.PAYTM_V2:SUBSCRIPTION_GRACE_DAYS": order_details["metadata.PAYTM_V2:SUBSCRIPTION_GRACE_DAYS"] ? order_details["metadata.PAYTM_V2:SUBSCRIPTION_GRACE_DAYS"] :"5",
	};
}

function getReferenceIds(order_details){
	const keys= Object.keys(order_details);
	var gatewayKeys = []
	for (var i= 0;i<keys.length;i++){
		if (keys[i].includes("gateway_reference_id")){
			gatewayKeys.push(keys[i])
		}
	}
	var gateway_ids=[]
	for (var i=0;i<gatewayKeys.length;i++){
		if (gateway_ids.indexOf(order_details[gatewayKeys[i]]) ==-1 )
			gateway_ids.push(order_details[gatewayKeys[i]])
	}
	return gateway_ids;
}


exports["saveSessionDetails"] = function (token) {
	return function(mobile_number) {
		return function () {
			window.__payload.session_token = token;
			window.__payload.mobile_number = mobile_number;
			console.warn(window.__payload);
		}
	}
}

exports["startECUPI'"] = function (payload) {
	return function (sc) {
		return function () {
			if (window.__OS === "ANDROID") {
				var cb = function (code) {
					return function (status) {
						return function () {
							console.log("Returning from EC UPI:", code, status);
							var v = {
								code: code,
								status: status
							}
							sc(v)();
						}
					}
				}
				if (JOS) {
					console.log(JOS)
					// if(payload.pay_with_app == undefined)		
					payload.pay_with_app = null;
					JOS.startApp("in.juspay.ec.upi")(payload)(cb)();
				}
			}
		}
	}
}

exports["removeInEligiblePayLaterWallets"] = function(resp){
	var arr = [];
	for(var i=0; i< resp.merchantPaymentMethods.length; i++){
		// if(resp.merchantPaymentMethods[i].paymentMethod !== "LAZYPAY"){
			arr.push(resp.merchantPaymentMethods[i])
		// }
	}
	resp.merchantPaymentMethods = arr;
	return resp;
}

exports["mergeMerchantWithEmandate"] = function(resp){
	var paymentMethodAndType = [];
	for(var i=0; i< resp.merchantPaymentMethods.length; i++){
		var merchantPaymentMethod = resp.merchantPaymentMethods[i]
		var str = merchantPaymentMethod.paymentMethodType + "_" + merchantPaymentMethod.paymentMethod;
		paymentMethodAndType.push(str);
	}
	for(var i=0; i< resp.emandatePaymentMethods.length; i++){
		var emandatePaymentMethod = Object. assign({}, resp.emandatePaymentMethods[i]);
		var paymentMethod = emandatePaymentMethod.paymentMethod;
		if(emandatePaymentMethod.paymentMethod.indexOf("JP_")>=0){
			paymentMethod = emandatePaymentMethod.paymentMethod.split("_")[1];
		}
		var str = emandatePaymentMethod.paymentMethodType + "_" + paymentMethod;
		if(paymentMethodAndType.indexOf(str)< 0 && emandatePaymentMethod.paymentMethodType !== "NB"){
			if(emandatePaymentMethod.paymentMethod == "JP_PAYTM"){
				emandatePaymentMethod.description = "Paytm";
				emandatePaymentMethod.paymentMethod = paymentMethod;
			}
			paymentMethodAndType.push(str);
			resp.merchantPaymentMethods.push(emandatePaymentMethod);
		}
	}
	return resp;
}


function getFromOrderDetails(key){
	var order_details = getKeyValueFromPayload("order_details");
	  if (order_details) {
		  try {
			  var order_detailsJson = JSON.parse(order_details);
			  if (typeof (order_detailsJson[key]) != "undefined") {
				  console.log("Value for key " + key + " in orderDetails is " + order_detailsJson[key]);
				  return "" + order_detailsJson[key];
			  } else {
		  console.log("Value for key " + key + " not found");
			  }
		  } catch(err) {
		console.log("Unable to parse orderDetails JSON", err);
		  }
	  } else {
	  console.log("OrderDetails not found");
	}
	return "";
  }
  
  exports["getValueFromOrderDetails"] = function(key) {
	return function(){
	  return getFromOrderDetails(key);
	}
  }