module Payments.Core.Types where

import Prelude

import Data.Maybe (Maybe)
import Foreign.Class (class Decode, class Encode)
import Presto.Core.Utils.Encoding (defaultDecode, defaultEncode)
import Data.Generic.Rep (class Generic)
import Data.Newtype (class Newtype)
import Foreign.Generic (encodeJSON)

newtype DeleteCardResp = DeleteCardResp {
  cardToken :: String,
  cardReference :: Maybe String,
  deleted :: Boolean
}

derive instance genericDeleteCardResp :: Generic DeleteCardResp _
instance encodeDeleteCardResp :: Encode DeleteCardResp where encode = defaultEncode
instance decodeDeleteCardResp :: Decode DeleteCardResp where decode = defaultDecode
instance showDeleteCardResp :: Show DeleteCardResp where show = encodeJSON


