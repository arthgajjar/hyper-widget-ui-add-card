module Payments.Core.Commons where

import Prelude

import Control.Monad.Except (ExceptT)
import Control.Monad.Free (Free)
import Data.Array ((!!), filter, elem)
import Data.Either (Either(..))
import Data.String (Pattern(..), split)
import Data.Generic.Rep (class Generic)
import Data.Identity (Identity)
import Data.Int (fromString) as IntUtil
import Data.List.Types (NonEmptyList)
import Engineering.Helpers.Commons (liftFlow)
import Data.Maybe (Maybe(..), fromMaybe, maybe)
import Data.Newtype (class Newtype)
import Data.Number (fromString) as NumUtil
import Service.EC.Types.Response (ErrorResult(..), ECResponse(..), SDKResponse(..), PaymentSourceResp(..))
import Service.EC.Types.Request (ECPayload(..), PaymentSourcePayload(..))
import Service.EC.Types.Instruments (Offer(..), OfferDescription(..))
import Effect (Effect)
import Effect.Aff (makeAff, nonCanceler)
import Effect.Class (liftEffect)
import Engineering.Helpers.Commons (getUrl)
import Foreign (ForeignError)
import Foreign.Class (class Decode, class Encode)
import Global.Unsafe (unsafeEncodeURIComponent)
import Presto.Core.Types.API (class RestEndpoint, Header(..), Headers(..), Method(..), defaultDecodeResponse, defaultMakeRequest)
import Presto.Core.Types.Language.Flow (APIResult, Flow, FlowWrapper, callAPI, doAff)
import Presto.Core.Utils.Encoding (defaultDecode, defaultDecodeJSON, defaultEncode)
import Tracker.Tracker as Tracker
import Payments.Adapter as Adapter
import Engineering.Helpers.Commons (PaymentOffer(..))
import Foreign.Class (class Decode, class Encode)
import Foreign.Generic (encodeJSON)
import Presto.Core.Types.API (class RestEndpoint, Method(..), defaultDecodeResponse, defaultMakeRequest, defaultMakeRequest_)
import Presto.Core.Utils.Encoding (defaultDecode, defaultEncode)

foreign import getWalletGateway :: String -> String
foreign import getMerchantViewUrl :: Unit -> String
foreign import getValueFromPayload' :: String -> String
foreign import mergeMerchantWithEmandate :: PaymentSourceResp -> PaymentSourceResp
foreign import removeInEligiblePayLaterWallets :: PaymentSourceResp -> PaymentSourceResp
foreign import saveSessionDetails :: String -> String -> Effect Unit
foreign import getValueFromPayload'' :: (String -> Maybe String) -> Maybe String -> String -> Effect (Maybe String)
foreign import getValueFromOrderDetails :: String -> Effect String
foreign import getCheckoutPayload :: ∀ json. Effect { | json}
-- foreign import startEC' :: MicroAPPInvokeSignature
foreign import startECUPI' :: MicroAPPInvokeSignature

type MicroAppResponse = {
  code :: Int,
  status :: String
}

type AffSuccess s = (s -> Effect Unit)
getEndUrls :: String -> Array String
getEndUrls urls = split (Pattern ",") urls

type MicroAPPInvokeSignature = ∀ a. {|a} -> (AffSuccess MicroAppResponse) -> Effect Unit

newtype StoredUpiVpa = StoredUpiVpa {
  bankImage:: String,
  bankName:: String,
  account:: String,
  vpa:: String
}

newtype UPIEnabledApps = UPIEnabledApps {
  packageName :: String,
  appName :: String
}

newtype AvailableApps = AvailableApps {
  available_apps :: Array UPIEnabledApps
}

newtype ECUPIAppsResponse = ECUPIAppsResponse {
  status :: String,
  response :: AvailableApps
}

newtype OrderStatusReq = OrderStatusReq {
  merchant_id :: String,
  order_id :: String
}

newtype OrderStatusResp = OrderStatusResp {
  order_id :: String,
  status   :: String
}

----------------------------------Keys----------------------------------
_order_details :: String
_order_details = "order_details"

_merchant_key_id :: String
_merchant_key_id = "merchant_key_id"

_signature :: String
_signature = "signature"


derive instance genericOrderStatusReq :: Generic OrderStatusReq _
derive instance newtypeOrderStatusReq :: Newtype OrderStatusReq _
instance decodeOrderStatusReq :: Decode OrderStatusReq where decode = defaultDecode
instance encodeOrderStatusReq :: Encode OrderStatusReq where encode = defaultEncode

derive instance genericOrderStatusResp :: Generic OrderStatusResp _
derive instance newtypeOrderStatusResp :: Newtype OrderStatusResp _
instance decodeOrderStatusResp :: Decode OrderStatusResp where decode = defaultDecode
instance encodeOrderStatusResp :: Encode OrderStatusResp where encode = defaultEncode

instance checkOrderStatus :: RestEndpoint OrderStatusReq (OrderStatusResp) where
  makeRequest (OrderStatusReq reqBody) headers =
    defaultMakeRequest POST ((getUrl' "OrderStatus") ) headers (OrderStatusReq reqBody)
  decodeResponse body = defaultDecodeResponse body


getUrl' :: String -> String
getUrl' reqType = do
  let url= getUrl ""
  let baseUrl = if checkoutDetails.environment == "sandbox" then url else "https://api.juspay.in"
  case reqType of
    "OrderStatus" -> baseUrl <> "/order/payment-status"
    _ -> url<>"/txns"


derive instance availableAppsRespGeneric :: Generic AvailableApps _
derive instance uPIEnabledAppsGeneric :: Generic UPIEnabledApps _

derive instance eCUPIAppsResponseGeneric :: Generic ECUPIAppsResponse _
derive instance storedUpiVpaGeneric :: Generic StoredUpiVpa _


instance decodeAvailableApps :: Decode AvailableApps where decode = defaultDecode
instance encodeAvailableApps :: Encode AvailableApps where encode = defaultEncode

instance decodeUPIEnabledApps :: Decode UPIEnabledApps where decode = defaultDecode
instance encodeUPIEnabledApps :: Encode UPIEnabledApps where encode = defaultEncode

instance decodeECUPIAppsResponse :: Decode ECUPIAppsResponse where decode = defaultDecode
instance encodeECUPIAppsResponse :: Encode ECUPIAppsResponse where encode = defaultEncode

instance decodeStoredUpiVpa :: Decode StoredUpiVpa where decode = defaultDecode
instance encodeStoredUpiVpa :: Encode StoredUpiVpa where encode = defaultEncode

checkoutDetails :: { order_token :: String
                    , amount :: Number
                    , order_id :: String
                    , merchant_id :: String
                    , offerCode :: String
                    , customer_phone_number :: String
                    , customer_id :: String
                    , customer_email :: String
                    , session_token :: String
                    , client_id :: String
                    , itemCount :: Int
                    , cashEnabled :: String
                    , disabledWallets :: String
                    , activity_recreated :: String
                    , environment :: String
                    , widget_name :: String
                    , payment_links :: String
                    , guest_login_url :: String
                    , create_mandate :: String
                    , mandate_max_amount :: String
                    }
checkoutDetails =
    { order_token:    getValueFromPayload' "session_token"
    , amount:          fromMaybe (negate 0.00) $ NumUtil.fromString $ getValueFromPayload' "amount"
    , order_id:        getValueFromPayload' "order_id"
    , merchant_id:     getValueFromPayload' "merchant_id"
    , offerCode:       getValueFromPayload' "offerCode"
    , customer_phone_number:  getValueFromPayload' "customer_phone_number"
    , customer_id:      getValueFromPayload' "customer_id"
    , customer_email:      getValueFromPayload' "customer_email"
    , session_token:      getValueFromPayload' "session_token"
    , client_id:       getValueFromPayload' "client_id"
    , itemCount:       fromMaybe (negate 1) $ IntUtil.fromString $ getValueFromPayload' "udf_itemCount"
    , cashEnabled:     if getValueFromPayload' "udf_cashDisabled" == "true" then "false" else "true"
    , disabledWallets :     getValueFromPayload' "udf_disabled_methods"
    , activity_recreated : if ((getValueFromPayload' "activity_recreated") == "true") then "true" else "false"
    , environment:    if getValueFromPayload' "environment" == "" then "prod" else getValueFromPayload' "environment"
    , widget_name : getValueFromPayload' "widget_name"
    , payment_links:   getValueFromPayload' "payment_links"
    , guest_login_url:  getValueFromPayload' "guest_login_url"
    , create_mandate : getValueFromPayload' "create_mandate"
    , mandate_max_amount : getValueFromPayload' "mandate_max_amount"
    }

type Checkout =
  { order_token :: String
  , amount :: Number
  , order_id :: String
  , merchant_id :: String
  , offerCode :: String
  , customer_phone_number :: String
  , customer_id :: String
  , customer_email :: String
  , session_token :: String
  , client_id :: String
  , itemCount :: Int
  , cashEnabled :: String
  , disabledWallets :: String
  , activity_recreated :: String
  , environment :: String
  , widget_name :: String
  , gateway_reference_id :: Array String
  , merchant_name :: String
  , guest_login_url :: String
  , create_mandate :: String
  , mandate_max_amount :: String
  , customer_first_name :: String
  , customer_last_name :: String
  , orderDescription :: String
  , order_details :: String
  , subventionAmount :: Number
  , signature :: String
  , merchant_key_id :: String
  , language :: String
  , action :: String
  , request_id :: String
  }



getCheckoutDetails :: Effect Checkout
getCheckoutDetails = getCheckoutPayload >>= (\payload -> pure {
    order_token:            payload.order_token
  , amount:                 fromMaybe (negate 0.00) $ NumUtil.fromString $ payload.amount
  , order_id:               payload.order_id
  , merchant_id:            payload.merchant_id
  , offerCode:              payload.offerCode
  , customer_phone_number:  payload.customer_phone_number
  , customer_id:            payload.customer_id
  , customer_email:         payload.customer_email
  , session_token:          payload.session_token
  , client_id:              payload.client_id
  , itemCount:              fromMaybe (negate 1) $ IntUtil.fromString $ payload.itemCount
  , cashEnabled:            payload.cashEnabled
  , disabledWallets:        payload.disabledWallets
  , activity_recreated:     if payload.activity_recreated == "true" then "true" else "false"
  , environment:            if payload.environment == "" then "prod" else payload.environment
  , widget_name:            payload.widget_name
  , gateway_reference_id:   payload.gateway_reference_id
  , merchant_name:          payload.merchant_name
  , guest_login_url:        payload.guest_login_url
  , create_mandate:         payload.create_mandate
  , mandate_max_amount:     payload.mandate_max_amount
  , customer_first_name :   payload.customer_first_name
  , customer_last_name : payload.customer_last_name
  , orderDescription :  payload.orderDescription
  , order_details : payload.order_details
  , subventionAmount : fromMaybe (0.00) $ NumUtil.fromString $ payload.subventionAmount
  , signature : payload.signature
  , merchant_key_id : payload.merchant_key_id
  , language : payload.language
  , action : case payload.action of
              "payment_page" -> "paymentPage"
              "quick_pay"    -> "quickPay"
              a              -> a
  , request_id : payload.request_id
})

ecUPIPayload :: {
          merchant_id :: String
        , client_id :: String
        , display_note :: String
        , order_id :: String
        , currency :: String
        , environment :: String
        , "WHITE_LIST" :: String
        , "UPI_PAYMENT_METHOD" :: String
        , get_available_apps :: String
        , pay_with_app :: Maybe String
  }
ecUPIPayload = {
            merchant_id : getValueFromPayload' "merchant_id"
          , client_id : getValueFromPayload' "client_id"
          ,   display_note : ""
          ,   order_id : getValueFromPayload' "order_id"
          ,   currency : "INR"
          ,   environment : if getValueFromPayload' "environment" == "" then "prod" else getValueFromPayload' "environment"
          ,   "WHITE_LIST" : ""
          ,   "UPI_PAYMENT_METHOD" : "NA"
          ,   get_available_apps : "true"
          ,   pay_with_app : Nothing
        }


getPaymentSourcePayload :: Maybe String -> Maybe String -> Checkout -> ECPayload
getPaymentSourcePayload offers refresh checkout  = let
    val v = if v == "" then Nothing else Just v
	in
    PaymentSource $
      PaymentSourcePayload
        { action: "paymentSource"
        , clientAuthToken : val checkout.session_token
        , offers : offers
        , refresh : refresh
        , signature : val (unsafeEncodeURIComponent checkout.signature)
        , order_details : val (unsafeEncodeURIComponent checkout.order_details)
        , merchant_key_id : val checkout.merchant_key_id
        , mandate_feature : Just "optional"
        , add_emandate_payment_methods : Just "true"
        , supported_reference_ids_feature : Just "true"
        }

getPaymentSource :: Maybe String -> Maybe String -> Flow (Either ErrorResult PaymentSourceResp)
getPaymentSource offers refresh = do
  checkout <- liftFlow $ getCheckoutDetails
  let paymentSourcePayload = getPaymentSourcePayload offers refresh checkout
  resp <- Adapter.startECFlow paymentSourcePayload
  Tracker.trackEvent "hyper_sdk" "api_call" "saved_payment_method" $ (Tracker.toString(resp))
  pure $ case resp of
    Just (SDKResponse response) -> do
      case response.payload of
        PaymentSources psr -> Right psr
        _ -> Left mismatchError
    Nothing -> Left apiError

-- convertMerchantType :: ECRemoteTypes.ECMerchantPaymentMethod  -> ECRemoteTypes.MerchantPaymentMethod
-- convertMerchantType (ECRemoteTypes.ECMerchantPaymentMethod merchantPaymentMethod) = ECRemoteTypes.MerchantPaymentMethod{
--   paymentMethodType : merchantPaymentMethod.payment_method_type,
--   paymentMethod : merchantPaymentMethod.payment_method,
--   description : merchantPaymentMethod.description,
--   supportedReferenceIds:  merchantPaymentMethod.supported_reference_ids,
--   walletDirectDebitSupport :Nothing
-- }

getUPIEnabledAppsRequest :: Free FlowWrapper (ExceptT (NonEmptyList ForeignError) Identity ECUPIAppsResponse)
getUPIEnabledAppsRequest = do
  uPIEnabledApps <- doAff do makeAff(\cb -> (startECUPI' ecUPIPayload $ Right >>> cb) *> pure nonCanceler)
  Tracker.trackEvent "hyper_sdk" "api_call" "upi_apps_configured" $ (Tracker.toString(uPIEnabledApps))
  let decoded = defaultDecodeJSON uPIEnabledApps.status :: _ ECUPIAppsResponse
  pure decoded

getValueFromPayload :: String -> Flow (Maybe String)
getValueFromPayload key = doAff do liftEffect (getValueFromPayload'' Just Nothing key)

getOrderStatusPayload :: Checkout -> OrderStatusReq
getOrderStatusPayload checkout = OrderStatusReq {
    order_id : checkout.order_id,
    merchant_id : checkout.merchant_id
  }

getOrderStatus :: Flow (APIResult OrderStatusResp)
getOrderStatus = do
  checkout <- doAff do liftEffect getCheckoutDetails
  let req = getOrderStatusPayload checkout
  callAPI headers req
  where
    headers = Headers [Header "Content-Type" "application/x-www-form-urlencoded"]

mismatchError :: ErrorResult
mismatchError = ErrorResult { status : "MISMATCH"
                            , errorMessage : "MISMATCH"
                            , errorCode : "MISMATCH"
                            }

apiError :: ErrorResult
apiError = ErrorResult { status : "API_ERROR"
                      , errorMessage : "API_ERROR"
                      , errorCode : "API_ERROR"
                      }

------------------------------------GET SESSION TOKEN API---------------------------------------------
newtype SessionTokenRequest = SessionTokenRequest { customer_id :: String, signature_payload :: String, signature :: String, merchant_key_id :: String}

newtype SessionTokenResponse = SessionTokenResponse {juspay :: TokenDetails, mobile_number :: String}

newtype TokenDetails = TokenDetails {client_auth_token :: String}

newtype SessionTokenBody = SessionTokenBody {  "options.get_client_auth_token" :: String, signature_payload :: String, signature :: String, merchant_key_id :: String, mobile_number :: String, email_address:: String }

getSessionTokenReq :: SessionTokenRequest
getSessionTokenReq = SessionTokenRequest {customer_id : checkoutDetails.customer_id, signature_payload : (getValueFromPayload' _order_details), signature : (getValueFromPayload' _signature), merchant_key_id : (getValueFromPayload' _merchant_key_id)}

getSessionTokenBody :: SessionTokenBody
getSessionTokenBody = SessionTokenBody {"options.get_client_auth_token": "true", signature_payload : (getValueFromPayload' _order_details), signature : (getValueFromPayload' _signature), merchant_key_id : (getValueFromPayload' _merchant_key_id), mobile_number : checkoutDetails.customer_phone_number, email_address : checkoutDetails.customer_email}
derive instance genericSessionTokenResponse :: Generic SessionTokenResponse _
derive instance newtypeSessionTokenResponse :: Newtype SessionTokenResponse _
instance decodeSessionTokenResponse :: Decode SessionTokenResponse where decode = defaultDecode
instance encodeSessionTokenResponse :: Encode SessionTokenResponse where encode = defaultEncode

derive instance genericSessionTokenRequest :: Generic SessionTokenRequest _
derive instance newtypeSessionTokenRequest :: Newtype SessionTokenRequest _
instance decodeSessionTokenRequest :: Decode SessionTokenRequest where decode = defaultDecode
instance encodeSessionTokenRequest :: Encode SessionTokenRequest where encode = defaultEncode

derive instance genericSessionTokenBody :: Generic SessionTokenBody _
derive instance newtypeSessionTokenBody :: Newtype SessionTokenBody _
instance decodeSessionTokenBody :: Decode SessionTokenBody where decode = defaultDecode
instance encodeSessionTokenBody :: Encode SessionTokenBody where encode = defaultEncode

derive instance genericTokenDetails :: Generic TokenDetails _
derive instance newtypeTokenDetails :: Newtype TokenDetails _
instance decodeTokenDetails :: Decode TokenDetails where decode = defaultDecode
instance encodeTokenDetails :: Encode TokenDetails where encode = defaultEncode

instance getSesisonTokenResp :: RestEndpoint SessionTokenRequest (SessionTokenResponse) where
  makeRequest reqBody@(SessionTokenRequest r) headers = do
    defaultMakeRequest POST ((getUrl "")<> "/v2/customers/" <> r.customer_id ) headers (getSessionTokenBody)
    -- defaultMakeRequest POST ((getUrl "")<> uu <> r.customer_id ) headers (getSessionTokenBody)-- <> "?signature_payload=" <> (unsafeEncodeURIComponent r.signature_payload) <> "&signature=" <> (unsafeEncodeURIComponent r.signature) <> "&merchant_key_id=" <> r.merchant_key_id <> "&options.get_client_auth_token=true") headers reqBody
  decodeResponse body = defaultDecodeResponse body

getSessionToken' :: Flow (APIResult SessionTokenResponse)
getSessionToken' = callAPI headers req
  where
    headers = Headers [Header "Content-Type" "application/x-www-form-urlencoded"]
    req = getSessionTokenReq

getSessionToken :: Free FlowWrapper Unit
getSessionToken = do
  r <- getSessionToken'
  case r of
    Right (SessionTokenResponse resp) -> do
      let TokenDetails td = resp.juspay
      _ <- doAff do liftEffect (saveSessionDetails td.client_auth_token resp.mobile_number)
      pure unit
    _ -> pure unit



baseUrl :: String ->  String
baseUrl environment = case environment of
                          "sandbox" -> "https://sandbox.juspay.in"
                          "production_static1" -> "https://api-ns1.juspay.in"
                          _ -> "https://api.juspay.in"

getOffers :: PaymentSourceResp -> Array PaymentOffer
getOffers (PaymentSourceResp resp) =
  resp.offers #
  map
    \(Offer offer) -> PaymentOffer
      { paymentMethodType : if (fromMaybe "" $ fromMaybe [] offer.paymentMethod !! 0) == "GOOGLEPAY" then "UPI" else fromMaybe "" offer.paymentMethodType
      , paymentMethod : if (fromMaybe "" $ fromMaybe [] offer.paymentMethod !! 0) == "GOOGLEPAY" then "com.google.android.apps.nbu.paisa.user" else (fromMaybe "" $ fromMaybe [] offer.paymentMethod !! 0)
      , offerText : maybe "" (\(OfferDescription offerDesc) -> fromMaybe "" offerDesc.offerDisplay1) offer.offerDescription
      , paymentMethodFilter : offer.paymentMethodFilter
      , voucherCode : offer.voucherCode
      , offerDescription : offer.offerDescription
      }



getDummyPaymentOffers :: Array PaymentOffer
getDummyPaymentOffers = [getDummyPaymentOffer]

getDummyPaymentOffer :: PaymentOffer
getDummyPaymentOffer = PaymentOffer
  { paymentMethodType : ""
  , paymentMethod : ""
  , offerText : ""
  , paymentMethodFilter : Nothing
  , voucherCode : ""
  , offerDescription : Nothing
  }
