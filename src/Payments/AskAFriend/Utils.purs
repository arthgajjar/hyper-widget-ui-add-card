module Payments.AskAFriend.Utils where

import Prelude

import Data.Either (Either(..))
import Data.Foldable (elem)
import Data.Time.Duration (Milliseconds(..))
import Engineering.Helpers.Commons (liftFlow)
import JBridge (openWhatsapp) as JBridge
import Payments.Core.Commons (OrderStatusResp(..), checkoutDetails, getOrderStatus)
import Presto.Core.Types.Language.Flow (Flow, delay)
--import View.Common.Container.Processing.Controller as ProcessingScreenController

-- pollOrderStatus :: Flow ProcessingScreenController.ScreenOutput
-- pollOrderStatus = do
--     _ <- delay $ Milliseconds (3000.0)
--     resp <- getOrderStatus
--     case resp of
--         Right (OrderStatusResp response) -> if elem response.status ["PENDING_AUTHENTICATION", "NEW", "STARTED", "PENDING_VBV"]
--                                             then pollOrderStatus
--                                             else if elem response.status ["VBV_SUCCESSFUL","AUTHORIZED","CHARGED"]
--                                                 then pure ProcessingScreenController.Proceed-- "CHARGED"
--                                                 else pure ProcessingScreenController.OnBackPress
--         _ -> pure ProcessingScreenController.OnBackPress --"FAILED"

openWhatsappWithPaymentLink :: Flow Unit
openWhatsappWithPaymentLink = do
    liftFlow $ JBridge.openWhatsapp text
    where
        text = "I am requesting you to pay Rs." <> show checkoutDetails.amount <> ". Proceed with following link " <> checkoutDetails.payment_links
