exports["startECFlow'"] = function (payload) {
    payload = JSON.parse(payload);
    console.log("EC Request", payload);
    payload.payload = payload.payload.contents

    return function (callback) {
        return function () {
            var success = function (code) {
                return function (result) {
                    return function () {
                        result = JSON.parse(result);
                        result = result.payload;
                        result = updateIfPSRResult(result); // This is a hack.
                        result = JSON.stringify(result);
                        console.log("EC Response", result);
                        callback(result)();
                    }
                }
            }

            if (JOS) {
                payload.service_based = "true";
                JOS.emitEvent('in.juspay.ec')("onMerchantEvent")(["process", JSON.stringify(payload)])(success)();

            } else {
                callback("FAIL")();
            }
        }
    }
}


/// -------- POLYFILL FOR ANDROID $4.4.4 -----------///
// if (!Math.trunc) {
// 	Math.trunc = function (v) {
// 		return v < 0 ? Math.ceil(v) : Math.floor(v);
// 	};
// }
/// -------- POLYFILL FOR ANDROID $4.4.4 -----------///

exports["getRequestID"] = function(){
    return window.uuidFN();
}

function updateIfPSRResult(result){
    try {
        if(result.payload.action == "paymentSource"){
            var wallets = result.payload.wallets;
            for(var i=0; i<wallets.length; i++){
                if(wallets[i].currentBalance == ""){
                    delete wallets[i]['currentBalance'];
                }
            }
            result.payload.wallets = wallets;
        }
    } catch (e){
        console.log("updatePSRResult ", e);
    }
    return result;
}
