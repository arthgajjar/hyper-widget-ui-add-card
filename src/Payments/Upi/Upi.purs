module Payments.UPI where

import Prelude

import Data.Either (Either(..))
import Data.Maybe (Maybe(..))
import Data.String (length)
import Debug.Trace (spy)
import Service.EC.Types.Request (ECPayload(..), NBPayload(..), UPIPayload(..))
import Service.EC.Types.Response (ErrorResult(..), SDKResponse(..))
import Effect.Class (liftEffect)
import Payments.Adapter as Adapter
import Payments.Core.Commons (Checkout, getEndUrls, _merchant_key_id, _order_details, _signature, apiError, getCheckoutDetails, getValueFromPayload, getValueFromPayload', mismatchError, checkoutDetails)
import Payments.UPI.Utils (strToMaybe, UPITransaction(..), UpiCollectReq(..), UpiCollectResp, VerifyVpaReq(..), VerifyVpaResp, UpiQRReq(..), UpiQRResp)
import Payments.Wallets.Types (WalletTransaction(..))
import Presto.Core.Types.API (Header(..), Headers(..))
import Presto.Core.Types.Language.Flow (APIResult, Flow, callAPI, doAff)


generateCollectPayload :: String -> Checkout -> Maybe String -> Maybe String -> Maybe String -> UpiCollectReq
generateCollectPayload vpa checkout orderDetails signature merchantKeyId =
  UpiCollectReq
    { order_id : checkout.order_id
    , merchant_id : checkout.merchant_id
    , payment_method_type : "UPI"
    , payment_method : "UPI"
    , txn_type : "UPI_COLLECT"
    , upi_vpa : vpa
    , redirect_after_payment : true
    , format : "json"
    , order_details : orderDetails
    , signature
    , merchant_key_id : merchantKeyId
    , save_to_locker: true
    }


generateQRPayload :: String -> Checkout -> Maybe String -> Maybe String -> Maybe String -> UpiQRReq
generateQRPayload vpa checkout orderDetails signature merchantKeyId =
  UpiQRReq
    { order_id : checkout.order_id
    , merchant_id : checkout.merchant_id
    , payment_method_type : "UPI"
    , payment_method : "UPI"
    , txn_type : "UPI_PAY"
    , upi_vpa : vpa
    , redirect_after_payment : true
    , format : "json"
    , order_details : orderDetails
    , signature
    , merchant_key_id : merchantKeyId
    , save_to_locker: true
    , sdk_params : true
    }

startUpiCollect :: String -> Flow (APIResult UpiCollectResp)
startUpiCollect vpa = do
  orderDetails <- getValueFromPayload _order_details
  signature <- getValueFromPayload _signature
  merchantKeyId <- getValueFromPayload _merchant_key_id
  checkout <- doAff do liftEffect getCheckoutDetails
  let req = generateCollectPayload vpa checkout orderDetails signature merchantKeyId
  callAPI headers req
  where
    headers = Headers [Header "Content-Type" "application/x-www-form-urlencoded"]


startUpiQR :: String -> Flow (APIResult UpiQRResp)
startUpiQR vpa = do
  orderDetails <- getValueFromPayload _order_details
  signature <- getValueFromPayload _signature
  merchantKeyId <- getValueFromPayload _merchant_key_id
  checkout <- doAff do liftEffect getCheckoutDetails
  let req = generateQRPayload vpa checkout orderDetails signature merchantKeyId
  callAPI headers req
  where
    headers = Headers [Header "Content-Type" "application/x-www-form-urlencoded"]

--- Unified ---
verifyVpaId :: Checkout -> String ->  Flow (APIResult VerifyVpaResp)
verifyVpaId checkout vpa = callAPI headers req
  where
    headers = Headers [Header "Content-Type" "application/json"]
    req = generateVerifyVpaPayload checkout vpa


generateVerifyVpaPayload :: Checkout -> String -> VerifyVpaReq
generateVerifyVpaPayload checkout vpa = VerifyVpaReq {
    merchant_id : checkout.merchant_id,
    vpa : vpa
  }


mkUPITrans :: UPITransaction -> Flow (Maybe SDKResponse)
mkUPITrans input = do
  orderDetails <- getValueFromPayload _order_details
  signature <- getValueFromPayload _signature
  merchantKeyId <- getValueFromPayload _merchant_key_id
  checkout <- doAff do liftEffect getCheckoutDetails
  let ecPayload = getUPIPayload input orderDetails signature merchantKeyId Nothing checkout
  Adapter.startECFlow (ecPayload)

getUPIPayload :: UPITransaction -> Maybe String -> Maybe String -> Maybe String -> Maybe Boolean -> Checkout -> ECPayload
getUPIPayload (UPITransaction uPITransaction) orderDetails signature merchantKeyId saveToLocker checkout =
  UPI
    $ UPIPayload
        { action : "upiTxn"
        , endUrls : Just $ getEndUrls $ getValueFromPayload' "endUrls"
        , orderId : checkout.order_id
        , upiSdkPresent : Just uPITransaction.upiSdkPresent
        , paymentMethod : Just uPITransaction.paymentMethod
        , displayNote : Just uPITransaction.displayNote
        , custVpa : strToMaybe uPITransaction.custVpa
        , payWithApp : strToMaybe uPITransaction.payWithApp
        , currency : Nothing
        , getAvailableApps : Nothing
        , handlePolling : Just uPITransaction.handlePolling
        , signature
        , merchantKeyId
        , orderDetails
        , saveToLocker
        , showLoader : Nothing
        }
