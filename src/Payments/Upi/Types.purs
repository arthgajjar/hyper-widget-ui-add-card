module Payments.UPI.Types where

import Prelude

import Data.Maybe (Maybe)
import Foreign.Class (class Decode, class Encode)
import Presto.Core.Utils.Encoding (defaultDecode, defaultEncode)
import Data.Generic.Rep (class Generic)
import Foreign.Generic (encodeJSON)
import Data.Newtype (class Newtype)

newtype PaymentResp = PaymentResp  {
        authentication :: Authentication
      } 

newtype Authentication = Authentication {
          method :: String,
          url :: String
    }


derive instance genericAuthentication :: Generic Authentication _
derive instance newtypeAuthentication :: Newtype Authentication _
instance decodeAuthentication :: Decode Authentication where decode = defaultDecode
instance encodeAuthentication :: Encode Authentication where encode = defaultEncode


derive instance genericPaymentResp :: Generic PaymentResp _
derive instance newtypePaymentResp :: Newtype PaymentResp _
instance decodePaymentResp :: Decode PaymentResp where decode = defaultDecode
instance encodePaymentResp :: Encode PaymentResp where encode = defaultEncode