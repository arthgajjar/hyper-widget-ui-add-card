module Payments.UPI.Utils where

import Prelude

import Data.Array (elem, filter, length, null, sortWith, (!!))
import Data.Array (sortWith)
import Data.Generic.Rep (class Generic)
import Data.Maybe (Maybe(..), fromMaybe, maybe)
import Data.Newtype (class Newtype)
import Data.String (Pattern(..))
import Data.String (Pattern(..), split, stripPrefix, toLower)
import Engineering.Helpers.Commons as Commons
import Foreign.Class (class Decode, class Encode)
import Payments.Core.Commons (AvailableApps(..), ECUPIAppsResponse(..), UPIEnabledApps(..), checkoutDetails)
import Payments.UPI.Types (Authentication(..), PaymentResp(..))
import Payments.Wallets.Utils (getMerchantPaymentMethods)
import Presto.Core.Types.API (class RestEndpoint, Method(..), defaultDecodeResponse, defaultMakeRequest)
import Presto.Core.Utils.Encoding (defaultDecode, defaultEncode)
import Service.EC.Types.Instruments (AppUsed(..), MerchantPaymentMethod(..), StoredVPA(..))
import Service.EC.Types.Response (PaymentSourceResp(..))

newtype UPITransaction = UPITransaction
      { paymentMethod :: String
      ,	displayNote :: String
      ,	upiSdkPresent :: Boolean
      ,	custVpa :: String
      ,	payWithApp :: String
      ,   saveToLocker :: Boolean
      ,   handlePolling :: Boolean
      }

getBankNameFromVpa :: String -> String
getBankNameFromVpa vpa =
  case getHandle vpa of
    "okhdfc" -> "HDFC Bank"
    "okhdfcbank" -> "HDFC Bank"
    "okaxis" -> "Axis Bank"
    "okicici" -> "ICICI Bank"
    "paytm" -> "Paytm"
    "upi" -> "BHIM"
    "ybl" -> "PhonePe"
    _ -> "ID"


getVpaImage :: String -> String
getVpaImage vpa = "ic_upi_icon"

getAppNameFromVpa :: String -> String
getAppNameFromVpa vpa = "ID"

getAppUsed :: PaymentSourceResp -> Array AppUsed
getAppUsed (PaymentSourceResp resp) = resp.appsUsed

getUPIEnabledApps :: ECUPIAppsResponse -> Array UPIEnabledApps
getUPIEnabledApps (ECUPIAppsResponse {response : (AvailableApps availableApps)}) = sortWith (\(UPIEnabledApps a) -> a.appName ) availableApps.available_apps

isUPIEnabled :: PaymentSourceResp -> Boolean
isUPIEnabled resp = let
  merchantUPI = getMerchantPaymentMethods "UPI" resp
  in (length merchantUPI) /= 0

newtype UpiCollectReq = UpiCollectReq {
  order_id :: String,
  merchant_id :: String,
  payment_method_type :: String,
  payment_method :: String,
  txn_type :: String,
  upi_vpa :: String,
  redirect_after_payment :: Boolean,
  format :: String,
  signature :: Maybe String,
  order_details :: Maybe String,
  merchant_key_id :: Maybe String,
  save_to_locker :: Boolean
}

newtype UpiQRReq = UpiQRReq {
  order_id :: String,
  merchant_id :: String,
  payment_method_type :: String,
  payment_method :: String,
  txn_type :: String,
  upi_vpa :: String,
  redirect_after_payment :: Boolean,
  format :: String,
  signature :: Maybe String,
  order_details :: Maybe String,
  merchant_key_id :: Maybe String,
  save_to_locker :: Boolean,
  sdk_params :: Boolean
}


newtype UpiCollectResp = UpiCollectResp {
    order_id :: String
  , txn_id :: String
  , status :: String
  , payment :: PaymentResp
  , txn_uuid :: String
}

newtype UpiQRResp = UpiQRResp {
    order_id :: String
  , txn_id :: String
  , status :: String
  , payment :: PaymentRespQR
  , txn_uuid :: String
}


newtype SdkParams = SdkParams {
  amount :: String,
  customer_first_name :: Maybe String,
  customer_last_name :: Maybe String,
  mcc :: String,
  merchant_name :: String,
  merchant_vpa :: String,
  tid :: String,
  tr :: String
}

newtype PaymentRespQR = PaymentRespQR  {
        authentication :: Authentication,
        sdk_params :: SdkParams
      }

newtype VerifyVpaReq = VerifyVpaReq {
  merchant_id :: String,
  vpa :: String
}

newtype VerifyVpaResp = VerifyVpaResp {
    vpa :: String
  , status :: String
  , customer_name :: String
}

--- UPI Collect ----

isUPIDisabled :: PaymentSourceResp -> String -> Boolean
isUPIDisabled (PaymentSourceResp resp) gatewayRefId = do
  let merchantPaymentMethods = resp.merchantPaymentMethods
      upiOptions = filter (\ (MerchantPaymentMethod method) -> maybe true (\i -> gatewayRefId `elem` i) method.supportedReferenceIds)
                    $ filter (\ (MerchantPaymentMethod method) -> method.paymentMethodType == "UPI") merchantPaymentMethods
  null upiOptions

derive instance genericUpiCollectReq :: Generic UpiCollectReq _
derive instance newtypeUpiCollectReq :: Newtype UpiCollectReq _
instance decodeUpiCollectReq :: Decode UpiCollectReq where decode = defaultDecode
instance encodeUpiCollectReq :: Encode UpiCollectReq where encode = defaultEncode

derive instance genericUpiQRReq :: Generic UpiQRReq _
derive instance newtypeUpiQRReq :: Newtype UpiQRReq _
instance decodeUpiQRReq :: Decode UpiQRReq where decode = defaultDecode
instance encodeUpiQRReq :: Encode UpiQRReq where encode = defaultEncode

derive instance genericUpiCollectResp :: Generic UpiCollectResp _
derive instance newtypeUpiCollectResp :: Newtype UpiCollectResp _
instance decodeUpiCollectResp :: Decode UpiCollectResp where decode = defaultDecode
instance encodeUpiCollectResp :: Encode UpiCollectResp where encode = defaultEncode

derive instance genericUpiQRResp :: Generic UpiQRResp _
derive instance newtypeUpiQRResp :: Newtype UpiQRResp _
instance decodeUpiQRResp :: Decode UpiQRResp where decode = defaultDecode
instance encodeUpiQRResp :: Encode UpiQRResp where encode = defaultEncode

instance makeUpiCollectReq :: RestEndpoint UpiCollectReq ( UpiCollectResp) where
  makeRequest (UpiCollectReq reqBody) headers =
    defaultMakeRequest POST (genEncodedUrl (getUrl UPI_COLLECT) (UpiCollectReq reqBody)) headers (UpiCollectReq reqBody)
  decodeResponse body = defaultDecodeResponse body

instance makeUpiQRReq :: RestEndpoint UpiQRReq ( UpiQRResp) where
  makeRequest (UpiQRReq reqBody) headers =
    defaultMakeRequest POST (genEncodedUrl2 (getUrl UPI_COLLECT) (UpiQRReq reqBody)) headers (UpiQRReq reqBody)
  decodeResponse body = defaultDecodeResponse body

derive instance genericPaymentRespQR :: Generic PaymentRespQR _
derive instance newtypePaymentRespQR :: Newtype PaymentRespQR _
instance decodePaymentRespQR :: Decode PaymentRespQR where decode = defaultDecode
instance encodePaymentRespQR :: Encode PaymentRespQR where encode = defaultEncode

derive instance genericSdkParams :: Generic SdkParams _
derive instance newtypeSdkParams :: Newtype SdkParams _
instance decodeSdkParams :: Decode SdkParams where decode = defaultDecode
instance encodeSdkParams :: Encode SdkParams where encode = defaultEncode
----- Verify Vpa -----

derive instance genericVerifyVpaReq :: Generic VerifyVpaReq _
derive instance newtypeVerifyVpaReq :: Newtype VerifyVpaReq _
instance decodeVerifyVpaReq :: Decode VerifyVpaReq where decode = defaultDecode
instance encodeVerifyVpaReq :: Encode VerifyVpaReq where encode = defaultEncode

derive instance genericVerifyVpaResp :: Generic VerifyVpaResp _
derive instance newtypeVerifyVpaResp :: Newtype VerifyVpaResp _
instance decodeVerifyVpaResp :: Decode VerifyVpaResp where decode = defaultDecode
instance encodeVerifyVpaResp :: Encode VerifyVpaResp where encode = defaultEncode

instance verifyVpa :: RestEndpoint VerifyVpaReq (VerifyVpaResp) where
  makeRequest (VerifyVpaReq reqBody) headers =
    defaultMakeRequest POST ((getUrl VERIFY_VPA )<> "?vpa="<>reqBody.vpa<>"&merchant_id="<>reqBody.merchant_id) headers (VerifyVpaReq reqBody)
  decodeResponse body = defaultDecodeResponse body

genEncodedUrl :: String -> UpiCollectReq -> String
genEncodedUrl baseUrl (UpiCollectReq reqBody) = baseUrl <> "?"<> "upi_vpa="<>reqBody.upi_vpa <>"&txn_type=UPI_COLLECT&signature="<>(fromMaybe "" reqBody.signature)<>"&redirect_after_payment=true&payment_method_type=UPI&payment_method=UPI&order_id="<>reqBody.order_id<>"&order_details="<>(fromMaybe "" reqBody.order_details)<>"&merchant_key_id="<>(fromMaybe "" reqBody.merchant_key_id)<>"&merchant_id="<>reqBody.merchant_id<>"&format=json"




genEncodedUrl2 :: String -> UpiQRReq -> String
genEncodedUrl2 baseUrl (UpiQRReq reqBody) = baseUrl <> "?"<> "&txn_type=UPI_PAY"<>"&redirect_after_payment=true&payment_method_type=UPI&payment_method=UPI&order_id="<>reqBody.order_id<>"&merchant_id="<>reqBody.merchant_id<>"&format=json"


strToMaybe :: String -> Maybe String
strToMaybe "" = Nothing
strToMaybe a = Just a


data URL_TYPE = VERIFY_VPA | UPI_COLLECT

getUrl :: URL_TYPE -> String
getUrl reqType = do
  let url =Commons.getUrl ""
  let baseUrl = if checkoutDetails.environment == "sandbox"
                  then url
                  else "https://api.juspay.in"

  case reqType  of
    UPI_COLLECT  -> baseUrl<>"/txns"
    VERIFY_VPA -> baseUrl <> "/upi/verify-vpa"


getStoredVpas :: PaymentSourceResp -> Array StoredVPA
getStoredVpas (PaymentSourceResp resp) = resp.vpas

getHandle :: String -> String
getHandle vpa = fromMaybe "" $ split (Pattern "@") vpa !! 1
