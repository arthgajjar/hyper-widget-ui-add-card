module Payments.WebRedirect.Flow where

import Effect.Uncurried (EffectFn3, runEffectFn3)
import Data.Either (Either(..))
import Data.Lens ((^.))
import Data.Maybe (Maybe(..), fromMaybe) 
import Effect.Aff (makeAff, nonCanceler)
import Engineering.Helpers.Commons (AffSuccess)
import Engineering.Types.App (MicroAppResponse, liftFlowBT, PaymentPageError, FlowBT)
import Prelude (Unit, pure, ($), (<*), (>>>))
import Presto.Core.Flow (doAff)
import Remote.Accessors (_payment, _authentication, _url, _method, _params)

foreign import redirectGet
    :: EffectFn3
        String
        (Maybe String)
        (AffSuccess MicroAppResponse)
        Unit
foreign import redir :: String -> Unit

foreign import redirectPost
    :: EffectFn3
        String
        String
        (AffSuccess MicroAppResponse)
        Unit



-- startRedirect
--     :: TxnResponse
--     -> FlowBT PaymentPageError MicroAppResponse
-- startRedirect resp = liftFlowBT $ doAff do
--     let auth = resp ^. _payment ^. _authentication
--     makeAff (\cb -> pure nonCanceler <*
--                 case auth ^. _method of
--                      "POST" -> runEffectFn3 redirectPost
--                                     (auth ^. _url)
--                                     (fromMaybe "" $ auth ^. _params)
--                                     (Right >>> cb)
--                      _ -> runEffectFn3 redirectGet
--                                     (auth ^. _url)
--                                     Nothing
--                                     (Right >>> cb)
--             )

