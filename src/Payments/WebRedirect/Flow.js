

exports.redirectGet = function(url, maybe, sc) {
  var obj=JSON.parse(window.__payload.sdk_payload)
  if (obj.integration == "iframe" && obj.merchant_uri) {
    var msg = { event: "redirectGet", data: {url: url}};
    window.parent.postMessage(msg, obj.merchant_uri);
    return;
  }

  location.href = url;
}
exports.redir = function(url) {
  window.open(url, "_top");
  return;
}



exports.redirectPost = function(url, params, sc) {

  if (window.__payload.integration == "iframe" && window.__payload.merchant_uri) {
    var msg = { event: "redirectPost", data: {url: url, params: params}};
    window.parent.postMessage(msg, window.__payload.merchant_uri);
    return;
  }


  var postFrm = document.createElement("form")
  postFrm.style.display = "none"; // ensure that the form is hidden from the user
  postFrm.setAttribute("method", "POST");
  postFrm.setAttribute("action", url);

  if(params != "") {
    params = JSON.parse(params);
    for(var key in params) {
      var value = params[key];
      var field = document.createElement("input");
      field.setAttribute("type", "hidden");
      field.setAttribute("name", key);
      field.setAttribute("value", value);
      postFrm.appendChild(field);
    }
  }

  document.body.appendChild(postFrm)
  // form is now ready
  postFrm.submit();
}

