module Payments.NetBanking where

import Prelude

import Data.Either (Either(..))
import Data.Maybe (Maybe(..))
import Data.String (length)
import Service.EC.Types.Request (ECPayload(..), SDKPayload(..), NBPayload(..))
import Service.EC.Types.Response (ErrorResult(..), SDKResponse(..))
import Effect.Class (liftEffect)
import Payments.Adapter as Adapter
import Payments.Core.Commons (Checkout, getEndUrls, _merchant_key_id, _order_details, _signature, apiError, getCheckoutDetails, getValueFromPayload, getValueFromPayload', mismatchError)
import Payments.Wallets.Types (WalletTransaction(..))
import Presto.Core.Types.Language.Flow (Flow, doAff)
import Engineering.Helpers.Commons (liftFlow)

mkPayReqNB :: String -> Maybe String -> Maybe String -> Maybe String -> Maybe String -> Checkout -> ECPayload
mkPayReqNB code offerToken orderDetails signature merchantKeyId checkout =
  NB
    $ NBPayload
        { action : "nbTxn"
        , orderId : checkout.order_id
        , endUrls : getEndUrls $ getValueFromPayload' "endUrls"
        , paymentMethod : code
        , offerToken
        , amount : checkout.amount
        , orderDetails
        , signature
        , merchantKeyId
        , shouldCreateMandate : Nothing
        , bankIfsc : Nothing
        , bankAccountNumber : Nothing
        , bankBeneficiaryName : Nothing
        , mandateType : Nothing
        , bankId : Nothing
        }

mkNBTrans :: String -> Maybe String -> Flow (Maybe SDKResponse)
mkNBTrans bankCode offerToken = do
  orderDetails <- getValueFromPayload _order_details
  signature <- getValueFromPayload _signature
  merchantKeyId <- getValueFromPayload _merchant_key_id
  checkout <- liftFlow getCheckoutDetails
  Adapter.startECFlow $ (mkPayReqNB bankCode offerToken orderDetails signature merchantKeyId checkout)
