module Payments.NetBanking.Utils where

import Prelude

import Data.Array (elem, filter, foldl, sortWith)
import Data.Array (filter, foldl, length, sortWith)
import Data.Maybe (Maybe(..), maybe)
import Data.String (Pattern(..), stripPrefix, toUpper)
import Service.EC.Types.Response (PaymentSourceResp(..))
import Service.EC.Types.Instruments (StoredNB(..), MerchantPaymentMethod(..))
import Engineering.Helpers.Commons (filterGatewayRef, stripPrefixCustom)
import PaymentPageConfig (ConfigPayload(..))
import PaymentPageConfig as PPConfig

foreign import getNbIin :: String -> String

newtype Bank = Bank {name :: String,  code :: String}

instance eqBank :: Eq Bank where
  eq (Bank a) (Bank b) = a.name == b.name && a.code == b.code

matchBanks :: String -> String -> Boolean
matchBanks ba bb = (ba == bb)

getPopularBanks :: ConfigPayload -> Array Bank -> Array Bank
getPopularBanks (ConfigPayload configPayload) banks =  filter (\(Bank bank) -> foldl(\ x y -> x || matchBanks bank.code  y) false (PPConfig.getPopularBanks (ConfigPayload configPayload))) banks

getOtherBanks :: ConfigPayload -> Array Bank -> Array Bank
getOtherBanks (ConfigPayload configPayload) banks = filter (\(Bank bank) -> foldl(\ x y -> x && not (matchBanks bank.code  y)) true (PPConfig.getPopularBanks (ConfigPayload configPayload))) banks

sortBanks :: Array Bank -> Array Bank
sortBanks banks = sortWith (\(Bank bank) -> bank.name) banks

filterBanks :: String -> Array Bank -> Array Bank
filterBanks searchText banks = filter (\(Bank b) -> case (stripPrefix (Pattern (toUpper searchText)) (toUpper b.name)) of
                                                        Just s -> true
                                                        Nothing -> false
                                      ) banks
filterBanksUpdated :: String -> Array Bank -> Array Bank
filterBanksUpdated searchtext banks = filter (\(Bank b) -> case (stripPrefixCustom ((toUpper searchtext)) (toUpper b.name)) of
                                                            [] -> false
                                                            _ -> true
                                      ) banks

-- getAllBanks ::  PaymentSourceResp ->  Array Bank
-- getAllBanks (PaymentSourceResp resp) =
  


getAllBanks :: PaymentSourceResp -> Array String -> Array Bank
getAllBanks (PaymentSourceResp resp) gatewayRefId = do
  if length gatewayRefId == 0
    then
      let merchantPaymentMethods = resp.merchantPaymentMethods
          nbOptions = filter (\ (MerchantPaymentMethod method) -> method.paymentMethodType == "NB") merchantPaymentMethods in
      sortBanks $ map (\ (MerchantPaymentMethod method) -> (Bank {name: method.description, code: method.paymentMethod})) nbOptions
    else do
      let merchantPaymentMethods = resp.merchantPaymentMethods
          pos = filterGatewayRef merchantPaymentMethods gatewayRefId
          nbOptions = --filter (\ (MerchantPaymentMethod method) -> maybe true (\i -> gatewayRefId `elem` i) method.supportedReferenceIds)
                         filter (\ (MerchantPaymentMethod method) -> method.paymentMethodType == "NB") pos
      sortBanks $ map (\ (MerchantPaymentMethod method) -> (Bank {name: method.description, code: method.paymentMethod})) nbOptions


getStoredNbs :: PaymentSourceResp -> Array StoredNB
getStoredNbs (PaymentSourceResp resp) = resp.nbMethods