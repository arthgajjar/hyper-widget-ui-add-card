module Payments.Wallets.Types where

import Prelude

import Data.Array (elemIndex, find,filter, foldl, sortWith,null, length, (!!))
import Data.Foldable (elem)
import Data.Generic.Rep (class Generic)
import Data.Maybe (Maybe(..), fromMaybe, isJust, maybe)
import Data.Maybe (Maybe(..), fromMaybe, maybe)
import Data.Newtype (class Newtype)
import Data.Newtype (class Newtype)
import Data.String (Pattern(..), split, toUpper)
import Data.String (drop, toLower)
import Data.String (toUpper)
import Data.String.CodeUnits (charAt, singleton)
import Data.String.Yarn (capWords, unwords)
import Engineering.Helpers.Commons (filterGatewayRef)
import Engineering.Helpers.Commons (getUrl)
import Foreign.Class (class Decode, class Encode)
import Presto.Core.Types.API (class RestEndpoint, Method(..), defaultDecodeResponse, defaultMakeRequest)
import Presto.Core.Utils.Encoding (defaultDecode, defaultEncode)
import PaymentPageConfig (ConfigPayload(..))
import Remote.Types (PaymentOptions(..))
import Payments.Core.Commons (baseUrl)


data MandateType = Required | Optional | None

derive instance eqMandateType :: Eq MandateType


newtype WalletTransaction =
	WalletTransaction
	 	{ paymentMethod :: String
		, directWalletToken :: Maybe String
		, sdkPresent :: String
		, mobileNumber :: String
    , mandateSupport :: Boolean
    , shouldLink :: Maybe Boolean
		}

newtype RefreshWallet =
	RefreshWallet
		{ walletId :: String
		, walletName :: String
		}


newtype ListWalletWithBalance = ListWalletWithBalance {
}


newtype CreateWalletTriggerOTP =
	CreateWalletTriggerOTP
		{ paymentMethod :: String
		, sdkWalletIdentifier :: String
		, mobileNumber :: Maybe String
		, walletName :: String
		}

newtype LinkWallet = LinkWallet {
      walletId :: String
    , walletName :: String
    , sdkWalletIdentifier :: String
    , otp :: String
}

newtype DelinkWallet = DelinkWallet {
     walletId :: String
    , walletName :: String
}

newtype Wallet = Wallet {
    code :: String
    , name :: String
    , directDebitSupport :: Boolean
    , mandateSupport :: Boolean
    , status :: Maybe String
}




-------------------------------------------------- Eligibility Call for Pay Later Wallets --------------------------------------------------

newtype PayLaterEligibilityRequest =
    PayLaterEligibilityRequest
        { amount :: String
        , client_auth_token :: String
        , customer_id :: String
        , gateway_reference_id :: String
        }



newtype PayLaterEligibilityResponse =
    PayLaterEligibilityResponse
        { payment_methods_eligibility :: Array PaymentMethodsEligibility } -- Instruments.PaymentMethodsEligibility

newtype PaymentMethodsEligibility =
    PaymentMethodsEligibility
        { status :: String
        , payment_method_type :: String
        , payment_method :: String
        , is_eligible :: Boolean
        , description :: String
        , balance :: Maybe Number
        , gateway_error_message :: Maybe String
        }



newtype GatewayData =
	GatewayData
		{ "SIMPL" :: Maybe GatewayDataItem
		}

newtype GatewayDataItem =
	GatewayDataItem
		{ payload :: String
		, merchant_params :: Maybe String
		}



newtype PayLaterEligibilityWrapReq =
	PayLaterEligibilityWrapReq
		{ environment :: String
		, reqBody :: PayLaterEligibilityRequest
		}


derive instance genericPayLaterEligibilityRequest :: Generic PayLaterEligibilityRequest _
derive instance newtypePayLaterEligibilityRequest :: Newtype PayLaterEligibilityRequest _
instance decodePayLaterEligibilityRequest :: Decode PayLaterEligibilityRequest where decode = defaultDecode
instance encodePayLaterEligibilityRequest :: Encode PayLaterEligibilityRequest where encode = defaultEncode

derive instance genericGatewayData :: Generic GatewayData _
derive instance newtypeGatewayData :: Newtype GatewayData _
instance decodeGatewayData :: Decode GatewayData where decode = defaultDecode
instance encodeGatewayData :: Encode GatewayData where encode = defaultEncode

derive instance genericGatewayDataItem :: Generic GatewayDataItem _
derive instance newtypeGatewayDataItem :: Newtype GatewayDataItem _
instance decodeGatewayDataItem :: Decode GatewayDataItem where decode = defaultDecode
instance encodeGatewayDataItem :: Encode GatewayDataItem where encode = defaultEncode


derive instance genericPayLaterEligibilityWrapReq :: Generic PayLaterEligibilityWrapReq _
derive instance newtypePayLaterEligibilityWrapReq :: Newtype PayLaterEligibilityWrapReq _
instance decodePayLaterEligibilityWrapReq :: Decode PayLaterEligibilityWrapReq where decode = defaultDecode
instance encodePayLaterEligibilityWrapReq :: Encode PayLaterEligibilityWrapReq where encode = defaultEncode



derive instance genericPayLaterEligibilityResponse :: Generic PayLaterEligibilityResponse _
derive instance newtypePayLaterEligibilityResponse :: Newtype PayLaterEligibilityResponse _
instance decodePayLaterEligibilityResponse :: Decode PayLaterEligibilityResponse where decode = defaultDecode
instance encodePayLaterEligibilityResponse :: Encode PayLaterEligibilityResponse where encode = defaultEncode

derive instance genericPaymentMethodsEligibility :: Generic PaymentMethodsEligibility _
derive instance newtypePaymentMethodsEligibility :: Newtype PaymentMethodsEligibility _
instance decodePaymentMethodsEligibility :: Decode PaymentMethodsEligibility where decode = defaultDecode
instance encodePaymentMethodsEligibility :: Encode PaymentMethodsEligibility where encode = defaultEncode

instance getEligibilityDetails :: RestEndpoint PayLaterEligibilityWrapReq PayLaterEligibilityResponse where
    makeRequest (PayLaterEligibilityWrapReq req) headers = do
        let (PayLaterEligibilityRequest r) = req.reqBody
        defaultMakeRequest POST ((baseUrl req.environment) <> "/customers/" <> r.customer_id <> "/eligibility") headers req.reqBody
    decodeResponse body = defaultDecodeResponse body


instance eqWallet :: Eq Wallet where
    eq w1 w2 = compareWallet w1 w2

compareWallet :: Wallet -> Wallet -> Boolean
compareWallet (Wallet w1) (Wallet w2) =
    w1.code == w2.code &&
    w1.name == w2.name &&
    w1.directDebitSupport == w2.directDebitSupport &&
    w1.mandateSupport == w2.mandateSupport
