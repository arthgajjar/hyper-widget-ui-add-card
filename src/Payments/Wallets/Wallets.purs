module Payments.Wallets where

import Prelude

import Data.Array ((!!))
import Data.Array as ARR
import Data.Either (Either(..))
import Data.Maybe (Maybe(..), fromMaybe, isJust)
import Data.Number.Format (toString)
import Data.String (length, toUpper)
import Service.EC.Types.Response (ECResponse(..), ErrorResult(..), SDKResponse(..), OrderStatusResp(..), WalletOpResponse(..))
import Service.EC.Types.Request (ECPayload(..), NBPayload(..), WalletTxnPayload(..), LinkWalletPayload(..), WalletPayload(..), CreateWalletPayload(..))
import Service.EC.Types.Instruments (Wallet(..))
import Effect.Class (liftEffect)
import JBridge (doesSimplExist, getSimplFingerPrint)
import Payments.Core.Commons (Checkout, getEndUrls, getValueFromOrderDetails, _merchant_key_id, _order_details, _signature, apiError, getCheckoutDetails, getWalletGateway, getValueFromPayload, getValueFromPayload', mismatchError)
import Payments.Wallets.Utils (strToMaybe,  PayLaterEligibilityRequest(..), PayLaterEligibilityResponse(..), PaymentMethodsEligibility, dummyPaymentMethodsEligibility, getGatewayId)
import Payments.Wallets.Utils as WalletUtils
import Presto.Core.Types.API (Header(..), Headers(..))
import Presto.Core.Types.Language.Flow (APIResult, Flow, callAPI, doAff)
import Payments.Adapter as Adapter
import Engineering.Helpers.Commons (liftFlow)
import Payments.Wallets.Types (CreateWalletTriggerOTP(..),WalletTransaction(..),RefreshWallet(..))
import Payments.Wallets.Types as PWTypes



getWalletTxnPayload :: Checkout -> WalletTransaction -> ECPayload
getWalletTxnPayload checkout walletData@(WalletTransaction w) = let
  isWalletToken = isJust w.directWalletToken
  walletTxnPayload = WalletTxnPayload $
      { action : "walletTxn"
      , orderId : checkout.order_id
      , clientAuthToken : checkout.session_token
      , endUrls : Just $ getEndUrls $ getValueFromPayload' "endUrls"
      -- TODO :: when eMandateSupport is updated
      , paymentMethod : if w.mandateSupport then "JP_" <> w.paymentMethod else w.paymentMethod
      , shouldLink : w.shouldLink
      , directWalletToken : if isWalletToken then w.directWalletToken else Nothing
      , sdkPresent : if (w.sdkPresent /= "")
                      then Just w.sdkPresent
                      else Nothing
      , walletMobileNumber : Just checkout.customer_phone_number
      , walletEmail : Nothing
      , offerToken : Nothing
      , riskId : Nothing
      , orderDetails : strToMaybe checkout.order_details
      , signature : strToMaybe checkout.signature
      , merchantKeyId : strToMaybe checkout.merchant_key_id
      , mandateType : if w.mandateSupport then Just "EMANDATE" else Nothing
      , shouldCreateMandate : Just w.mandateSupport
      , useFallback : Nothing
      , editFi : Nothing
      }
  in
    if (isWalletToken && (not (w.sdkPresent /= "")) && w.paymentMethod /= "AMAZONPAY")
    then (WalletDirectDebit walletTxnPayload)
    else if (not isWalletToken && not (w.sdkPresent /= "")) then (WalletRedirect walletTxnPayload)
    else (WalletSDKDebit walletTxnPayload)

mkWalletTxn :: WalletTransaction -> Flow (Maybe SDKResponse)
mkWalletTxn walletData@(WalletTransaction w) = do
  checkout <- liftFlow $ getCheckoutDetails
  let walletTxnPayload = getWalletTxnPayload checkout walletData
  Adapter.startECFlow walletTxnPayload

getLinkWalletPayload :: Checkout -> PWTypes.LinkWallet -> ECPayload
getLinkWalletPayload checkout (PWTypes.LinkWallet params) =
  LinkWallet $
    LinkWalletPayload $
      { action : "linkWallet"
      , clientAuthToken : checkout.session_token
      , walletId : params.walletId
      , otp : params.otp
      , walletName : params.walletName
      , sdkWalletIdentifier : Nothing
      , gatewayReferenceId : strToMaybe (getGatewayId checkout.gateway_reference_id)
      }
getDelinkWalletPayload :: Checkout -> PWTypes.DelinkWallet -> ECPayload
getDelinkWalletPayload checkout (PWTypes.DelinkWallet params) =
  DeLinkWallet $
    WalletPayload
      { action : "delinkWallet"
      , clientAuthToken : checkout.session_token
      , walletId : params.walletId
      , walletName : params.walletName
      , sdkWalletIdentifier : Nothing
      }

linkWallet :: PWTypes.LinkWallet -> Flow (Maybe SDKResponse)
linkWallet input = do
    checkout <- doAff do liftEffect getCheckoutDetails
    let linkWalletPayload = getLinkWalletPayload checkout input
    Adapter.startECFlow linkWalletPayload

delinkWallet :: PWTypes.DelinkWallet -> Flow (Maybe SDKResponse)
delinkWallet input = do
  checkout <- doAff do liftEffect getCheckoutDetails
  let delinkWalletPayload = getDelinkWalletPayload checkout input
  Adapter.startECFlow delinkWalletPayload

mkReqCreateWalletTriggerOTP :: Checkout -> CreateWalletTriggerOTP -> String -> ECPayload  -- similar to getCreateWalletPayload?
mkReqCreateWalletTriggerOTP checkout (CreateWalletTriggerOTP params) gatewayReferenceId' = do
  CreateWallet $
    CreateWalletPayload
      { action : "createWallet"
      , clientAuthToken : checkout.session_token
      , walletName : params.walletName
      , sdkWalletIdentifier : strToMaybe params.sdkWalletIdentifier
      , mobileNumber : params.mobileNumber
      , gatewayReferenceId : strToMaybe gatewayReferenceId'
      }

createWalletTriggerOTP :: CreateWalletTriggerOTP -> Flow (Maybe SDKResponse) -- similar to createWallet?
createWalletTriggerOTP params@(CreateWalletTriggerOTP p) = do
    checkout <- liftFlow $ getCheckoutDetails
    gateway_gatewayRefID <- liftFlow $ getValueFromOrderDetails $ mapToGatewayRefIDKey p.walletName
    let
      gatewayReferenceID = if gateway_gatewayRefID == "" then (getGatewayId checkout.gateway_reference_id) else gateway_gatewayRefID
      createWalletPayload = mkReqCreateWalletTriggerOTP checkout params gatewayReferenceID
    Adapter.startECFlow createWalletPayload

mapToGatewayRefIDKey :: String -> String
mapToGatewayRefIDKey walletID = let
  prefix = "metadata."
  suffix = ":gateway_reference_id"
  gatewayID =
    case toUpper walletID of
      a -> a
  in  prefix <> gatewayID <> suffix

getPayLaterEligibilityRequest :: Checkout -> String -> PayLaterEligibilityRequest
getPayLaterEligibilityRequest checkout fingerPrint =
    PayLaterEligibilityRequest
        { amount : toString checkout.amount
        , client_auth_token : checkout.order_token
        , customer_id : checkout.customer_id
        , gateway_reference_id : (getGatewayId checkout.gateway_reference_id)
        }

getEligibilityDetails' :: Checkout -> Flow (APIResult PayLaterEligibilityResponse)
getEligibilityDetails' checkout = do
    exists <- doesSimplExist
    fingerPrint <- if exists
                    then getSimplFingerPrint checkout.customer_phone_number "" checkout.merchant_id
                    else pure ""
    callAPI headers $ req fingerPrint
    where
        req fingerPrint = getPayLaterEligibilityRequest checkout fingerPrint
        headers = Headers [ Header "Content-Type" "application/x-www-form-urlencoded"
                        --   , Header "x-gateway" "simpl"
                          ]

getEligibilityDetails :: Checkout -> Flow (Array PaymentMethodsEligibility)
getEligibilityDetails checkout = do
    r <- getEligibilityDetails' checkout
    case r of
        Right (PayLaterEligibilityResponse resp) -> do
            pure resp.payment_methods_eligibility
        _ -> pure [dummyPaymentMethodsEligibility "SIMPL"]
