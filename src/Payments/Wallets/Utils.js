exports["checkTriggerOtpResponse"] = function (ecResponse) {
    var response  = {
        linked : false
        , error : ""
    }
    var parsed = JSON.parse(ecResponse);
    if(parsed.linked != undefined){
        response.linked = parsed.linked
    }else{
        response.error = "Invalid Number"
    }
    return response;
}

exports["checkSubmitOtpResponse"] = function (ecResponse) {
    console.error(ecResponse)
    var response  = {
        linked : false
        , error : ""
    }
    var parsed = JSON.parse(ecResponse);
    if(parsed.linked != undefined){
        response.linked = parsed.linked
    }else{
        response.error = "Probably Invalid Number"
    }
    return response;
}

exports["getWalletIdFromEcResponse"] = function(ecResponse) {
    var walletId = "";
    var parsedEcResp = JSON.parse(ecResponse);
    walletId = parsedEcResp.id;
    return walletId;
}

exports["parseDelinkRespose"] = function(delinkRepose){
    var linked= "";
    var parsedDelinkResponse = JSON.parse(delinkRepose);
    linked = parsedDelinkResponse.linked;
    return linked;
}

exports["getGatewayId"] = function(list){
    return list[0]
    // if(list.indexOf("bigbasket_b2c_checkout")>=0){
    //     return true;
    // }
    // return false;
}

exports["getWalletMandateSupport"] = function(emandatePaymentMethods){
    return function(merchantPaymentMethod){
        for(var i=0; i< emandatePaymentMethods.length; i++){
            var eMandatePaymentMethod = emandatePaymentMethods[i].paymentMethod.split("_");
            var isWallet = emandatePaymentMethods[i].paymentMethodType == "WALLET"
            if(isWallet && eMandatePaymentMethod.length>1 && eMandatePaymentMethod[1] == merchantPaymentMethod.paymentMethod){
                return true;
            }
        }
        return false;

        
    }
}