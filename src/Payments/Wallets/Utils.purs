module Payments.Wallets.Utils where

import Prelude

import Data.Array (elemIndex, find,filter, foldl, sortWith,null, length, (!!))
import Data.Foldable (elem)
import Data.Generic.Rep (class Generic)
import Data.Maybe (Maybe(..), fromMaybe, isJust, maybe)
import Data.Maybe (Maybe(..), fromMaybe, maybe)
import Data.Newtype (class Newtype)
import Data.String (Pattern(..), split, toUpper)
import Data.String (drop, toLower)
import Data.String (toUpper)
import Data.String.CodeUnits (charAt, singleton)
import Data.String.Yarn (capWords, unwords)
import Service.EC.Types.Instruments (MerchantPaymentMethod(..), StoredWallet(..))
import Service.EC.Types.Instruments as Instruments
import Service.EC.Types.Response (PaymentSourceResp(..))
import Engineering.Helpers.Commons (filterGatewayRef)
import Engineering.Helpers.Commons (getUrl)
import Foreign.Class (class Decode, class Encode)
import Presto.Core.Types.API (class RestEndpoint, Method(..), defaultDecodeResponse, defaultMakeRequest)
import Presto.Core.Utils.Encoding (defaultDecode, defaultEncode)
import PaymentPageConfig (ConfigPayload(..))
import Remote.Types (PaymentOptions(..))
import Payments.Wallets.Types(MandateType(..), WalletTransaction(..),ListWalletWithBalance(..),RefreshWallet(..),CreateWalletTriggerOTP(..),LinkWallet(..),DelinkWallet(..),Wallet(..))

foreign import checkTriggerOtpResponse :: String -> { linked :: Boolean , error :: String }
foreign import checkSubmitOtpResponse :: String -> { linked :: Boolean , error :: String }
foreign import getGatewayId :: Array String -> String

foreign import getWalletIdFromEcResponse :: String -> String
foreign import parseDelinkRespose :: String -> Boolean
foreign import getWalletMandateSupport ::Array (MerchantPaymentMethod) -> MerchantPaymentMethod -> Boolean

mapToMandateType :: String -> MandateType
mapToMandateType create_mandate =
  case create_mandate of
    "optional" -> Optional
    "required" -> Required
    _          -> None


getDisabledPayLater :: forall a.
    { linkedWallets :: Array StoredWallet
    , unlinkedWallets :: Array Wallet
    | a}
    -> Array String
getDisabledPayLater input = do
    let disabledStored = map (\(StoredWallet (Instruments.Wallet w)) -> fromMaybe "" w.wallet)
                            $ filter (\(StoredWallet (Instruments.Wallet w)) -> isJust w.status) input.linkedWallets

    disabledStored
walletMapper :: String -> String
walletMapper payment_method = do
  case payment_method of
    "PAYTM" -> "Paytm"
    "MOBIKWIK" -> "Mobikwik"
    "AMAZONPAY" -> "Amazon Pay"
    "AIRTELMONEY" -> "Airtel Money"
    "SIMPL" -> "Simpl"
    "PAYPAL" -> "Paypal"
    "OLAMONEY" -> "Ola Money"
    "PAYZAPP" -> "Payzapp"
    "FREECHARGE" -> "Freecharge"
    "PHONEPE" -> "PhonePe"
    _ -> formatWallet payment_method

formatWallet :: String -> String
formatWallet wallet = let
  firstChar = singleton $ fromMaybe ' ' $ charAt 0 wallet
  in
    wallet # toLower >>> drop 1 >>> (firstChar <> _)

doesSimplExists :: PaymentSourceResp -> Boolean  --not in android
doesSimplExists (PaymentSourceResp resp) = do
    let merchantPaymentMethods = resp.merchantPaymentMethods
    let wallets = filter (\ (MerchantPaymentMethod method) -> (toUpper method.paymentMethodType) == "WALLET" && (toUpper method.paymentMethod) `elem` payLaterMap) merchantPaymentMethods
    if (length wallets>0)
        then true
        else false

getAllWallets :: PaymentSourceResp -> MandateType -> ConfigPayload -> Array Wallet
getAllWallets psr@(PaymentSourceResp resp) mandateType cP = let
  merchantPaymentMethods = resp.merchantPaymentMethods
  disabledWallets = getDisabledPaymentOptions cP "wallets"
  wallets = filter
    (\(MerchantPaymentMethod method) ->
      (toUpper method.paymentMethodType) == "WALLET")
    merchantPaymentMethods
  in
  filter(\(Wallet w) -> not (w.code `elem` disabledWallets))
  $ map
      (\m@(MerchantPaymentMethod method) ->
        Wallet
          { name: method.description
          , code: method.paymentMethod
          , directDebitSupport : fromMaybe false method.walletDirectDebitSupport
          , mandateSupport : false--hard coded false because of diff in android and web
          , status : Just ("")
          }
      )
    wallets

-- getAllWallets :: PaymentSourceResp -> Maybe (Array String) -> Maybe Boolean -> Array Wallet
-- getAllWallets (PaymentSourceResp resp) gatewayRefId mandateFeature= do
--     case gatewayRefId of
--         Just a -> do
--                     if length a==0
--                         then do
--                             let merchantPaymentMethods = resp.merchantPaymentMethods
--                             let wallets = filter (\ (MerchantPaymentMethod method) -> (toUpper method.paymentMethodType) == "WALLET") merchantPaymentMethods
--                             map (\ (MerchantPaymentMethod method) -> (Wallet { name: walletMapper method.paymentMethod
--                                                                                         , code: method.paymentMethod
--                                                                                         , directDebitSupport : fromMaybe false method.walletDirectDebitSupport
--                                                                                         , mandateSupport : if mandateFeature == Nothing then false else getWalletMandateSupport resp.emandatePaymentMethods (MerchantPaymentMethod method)
--                                                                                         , status : Nothing
--                                                                                         }
--                                                                                         )) wallets
--                         else do
--                             let merchantPaymentMethods = resp.merchantPaymentMethods
--                             let filtered_wallet= filterGatewayRef merchantPaymentMethods a
--                             let wallets = --filter (\ (MerchantPaymentMethod method) -> maybe true (\i -> maybe true (\id -> id `elem` i) gatewayRefId) method.supportedReferenceIds)
--                                             filter (\ (MerchantPaymentMethod method) -> (toUpper method.paymentMethodType) == "WALLET") filtered_wallet
--                             map (\ (MerchantPaymentMethod method) -> (Wallet { name: walletMapper method.paymentMethod
--                                                                                         , code: method.paymentMethod
--                                                                                         , directDebitSupport : fromMaybe false method.walletDirectDebitSupport
--                                                                                         , mandateSupport : if mandateFeature == Nothing then false else getWalletMandateSupport resp.emandatePaymentMethods (MerchantPaymentMethod method)
--                                                                                         , status : Nothing
--                                                                                         --    , status : Nothing
--                                                                                         }
--                                                                                         )) wallets
--         Nothing -> do
--                     let merchantPaymentMethods = resp.merchantPaymentMethods
--                     let wallets = filter (\ (MerchantPaymentMethod method) -> (toUpper method.paymentMethodType) == "WALLET") merchantPaymentMethods
--                     map (\ (MerchantPaymentMethod method) -> (Wallet { name: walletMapper method.paymentMethod
--                                                                                 , code: method.paymentMethod
--                                                                                 , directDebitSupport : fromMaybe false method.walletDirectDebitSupport
--                                                                                 , mandateSupport : if mandateFeature == Nothing then false else getWalletMandateSupport resp.emandatePaymentMethods (MerchantPaymentMethod method)
--                                                                                 , status : Nothing
--                                                                                 }
--                                                                                 )) wallets
--


getStoredWallets :: ConfigPayload -> PaymentSourceResp -> Array StoredWallet
getStoredWallets cP r@(PaymentSourceResp resp) = let
  merchantWallets = getMerchantPaymentMethods "WALLET" r
  disabledWallets = getDisabledPaymentOptions cP "wallets"
  in
  filter(\(StoredWallet (Instruments.Wallet w)) -> not ((fromMaybe "" w.wallet) `elem` disabledWallets))
  $ filter(\(StoredWallet (Instruments.Wallet w)) -> (fromMaybe "" w.wallet) `elem` merchantWallets)
    $ sortWith (\(StoredWallet (Instruments.Wallet w)) -> w.wallet)
      $ filter (\(StoredWallet (Instruments.Wallet w)) -> fromMaybe false w.linked) resp.wallets

getMerchantPaymentMethods :: String -> PaymentSourceResp -> Array String
getMerchantPaymentMethods pig (PaymentSourceResp resp) = let
  merchantPaymentMethods = resp.merchantPaymentMethods
  in
  map (\(MerchantPaymentMethod pm) -> pm.paymentMethod) $ filter (\(MerchantPaymentMethod pm) -> pm.paymentMethodType == pig) merchantPaymentMethods



getDisabledPaymentOptions :: ConfigPayload -> String -> Array String
getDisabledPaymentOptions cP payOpt = let
    poArray = filter(\(PaymentOptions po) -> po.po == payOpt) (getPaymentOptions cP)
    PaymentOptions po = fromMaybe (dummyPO) $ poArray !! 0
    in
    fromMaybe [] po.onlyDisable

dummyPO :: PaymentOptions
dummyPO = PaymentOptions { group : "", po : "", visibility : "", onlyDisable: Nothing, onlyEnable: Nothing, isLast : ""}

getPaymentOptions :: ConfigPayload -> Array PaymentOptions
getPaymentOptions (ConfigPayload configPayload) = configPayload.paymentOptions
-- getAllUnLinkedWallets :: PaymentSourceResp -> Array Wallet
-- getAllUnLinkedWallets psr@(PaymentSourceResp resp) =
--     let linkedWallets = psr
--                             # getStoredWallets
--                             # map (\(StoredWallet (Instruments.Wallet w)) -> fromMaybe "" w.wallet)
--         isLinked :: String -> Boolean
--         isLinked walletMame = (-1) /= ( linkedWallets
--                                         # elemIndex walletMame
--                                         # fromMaybe (-1) )
--     in
--     resp.merchantPaymentMethods
--             # filter (\ (MerchantPaymentMethod method) -> ( method.paymentMethod # toUpper # isLinked # not))
--             # filter (\ (MerchantPaymentMethod method) -> (toUpper method.paymentMethodType) == "WALLET")
--             # map (\ (MerchantPaymentMethod method) -> (Wallet { name: method.description
--                                                                              , code: method.paymentMethod
--                                                                              , directDebitSupport : fromMaybe false method.walletDirectDebitSupport
--                                                                              }))

getSIMPLstatus :: String -> Array PaymentMethodsEligibility -> Maybe String
getSIMPLstatus walletCode payLaterEligibility = do
    let errorMsg = maybe Nothing (\(PaymentMethodsEligibility pme) -> pme.gateway_error_message)
                    $ filter (\(PaymentMethodsEligibility pme) -> pme.payment_method == walletCode) payLaterEligibility !! 0
    case errorMsg of
        Just "insufficient_credit"  -> Just "Insufficient Credit"
        Just "pending_dues"         -> Just "Bill Payment Pending"
        Just "linking_required"     -> Nothing
        Just anythingElse           -> Just $ capWords $ unwords $ split (Pattern "_") anythingElse
        Nothing                     -> Nothing

getIneligibleWallets :: Array PaymentMethodsEligibility -> Array String
getIneligibleWallets payLaterEligibility =
    map (\(PaymentMethodsEligibility pme) -> pme.payment_method)
        $ filter (\(PaymentMethodsEligibility pme) -> not (pme.is_eligible)) payLaterEligibility


getUpdatedBalance :: String -> Array PaymentMethodsEligibility -> Maybe Number
getUpdatedBalance walletCode payLaterEligibility =
    maybe Nothing (\(PaymentMethodsEligibility pme) -> pme.balance)
        $ filter (\(PaymentMethodsEligibility pme) -> pme.payment_method == walletCode) payLaterEligibility !! 0


getAllUnLinkedWallets :: PaymentSourceResp -> MandateType -> ConfigPayload -> Array Wallet
getAllUnLinkedWallets psr mandateType cP = let
  val a = toUpper $ fromMaybe "" a
  storedWallets = map (\(StoredWallet (Instruments.Wallet w)) -> val w.wallet) $ getStoredWallets cP psr
  in
  filter (\(Wallet w) -> not $ toUpper w.code `elem` storedWallets ) $ getAllWallets psr mandateType cP



--
-- getAllUnLinkedWallets :: PaymentSourceResp -> Maybe (Array String) -> Maybe Boolean -> Array Wallet
-- getAllUnLinkedWallets psr@(PaymentSourceResp resp) gatewayRefId mandateFeature= do
--     let linkedWallets = map (\(StoredWallet (Instruments.Wallet w)) -> fromMaybe "" w.wallet)
--                             $ getStoredWallets psr gatewayRefId mandateFeature
--         isLinked :: String -> Boolean
--         isLinked walletName = walletName `elem` linkedWallets
--     case gatewayRefId of
--         Just a -> do
--                     if length a == 0
--                         then do
--                             resp.merchantPaymentMethods
--                                 # filter (\ (MerchantPaymentMethod method) -> ( method.paymentMethod # toUpper # isLinked # not))
--                                 # filter (\ (MerchantPaymentMethod method) -> (toUpper method.paymentMethodType) == "WALLET")
--                                 # filter (\ (MerchantPaymentMethod method) -> (toUpper method.paymentMethod) /= "GOOGLEPAY")
--                                 # map (\ (MerchantPaymentMethod method) -> (Wallet { name: walletMapper method.paymentMethod
--                                                                                                 , code: method.paymentMethod
--                                                                                                 , directDebitSupport : fromMaybe false method.walletDirectDebitSupport
--                                                                                                 , mandateSupport : if mandateFeature == Nothing then false else getWalletMandateSupport resp.emandatePaymentMethods (MerchantPaymentMethod method)
--                                                                                                 , status : Nothing
--                                                                                                 }))
--                         else do
--
--                             let pos = filterGatewayRef resp.merchantPaymentMethods a
--                             pos
--                                     # filter (\ (MerchantPaymentMethod method) -> ( method.paymentMethod # toUpper # isLinked # not))
--                                     # filter (\ (MerchantPaymentMethod method) -> (toUpper method.paymentMethodType) == "WALLET")
--                                     # filter (\ (MerchantPaymentMethod method) -> (toUpper method.paymentMethod) /= "GOOGLEPAY")
--                                     -- # filter (\ (MerchantPaymentMethod method) -> maybe true (\i -> maybe true (\id -> id `elem` i) gatewayRefId) method.supportedReferenceIds)
--                                     # map (\ (MerchantPaymentMethod method) -> (Wallet { name: walletMapper method.paymentMethod
--                                                                                                     , code: method.paymentMethod
--                                                                                                     , directDebitSupport : fromMaybe false method.walletDirectDebitSupport
--                                                                                                     , mandateSupport : if mandateFeature == Nothing then false else getWalletMandateSupport resp.emandatePaymentMethods (MerchantPaymentMethod method)
--                                                                                                     , status : Nothing
--                                                                                                     }))
--         Nothing-> resp.merchantPaymentMethods
--                         # filter (\ (MerchantPaymentMethod method) -> ( method.paymentMethod # toUpper # isLinked # not))
--                         # filter (\ (MerchantPaymentMethod method) -> (toUpper method.paymentMethodType) == "WALLET")
--                         # filter (\ (MerchantPaymentMethod method) -> (toUpper method.paymentMethod) /= "GOOGLEPAY")
--                         # map (\ (MerchantPaymentMethod method) -> (Wallet { name: walletMapper method.paymentMethod
--                                                                                         , code: method.paymentMethod
--                                                                                         , directDebitSupport : fromMaybe false method.walletDirectDebitSupport
--                                                                                         , mandateSupport : if mandateFeature == Nothing then false else getWalletMandateSupport resp.emandatePaymentMethods (MerchantPaymentMethod method)
--                                                                                         , status : Nothing
--                                                                                         }))
payLaterMap :: Array String
payLaterMap = ["LAZYPAY", "SIMPL", "EPAYLATER", "OLAPOSTPAID", "LOANTAP"]

strToMaybe :: String -> Maybe String
strToMaybe a = if a == "" then Nothing else Just a

getPayLaterWallets :: Array Wallet -> Array PaymentMethodsEligibility -> Array Wallet
getPayLaterWallets allWallets  payLaterEligibility=
    map (\(Wallet item) -> Wallet { name : item.name
                                  , code : item.code
                                  , directDebitSupport : item.directDebitSupport
                                  , mandateSupport : item.mandateSupport
                                  , status : getSIMPLstatus item.code payLaterEligibility
                                })
        -- $ filter (\(Wallet item) -> not $ elem item.code $ getIneligibleWallets payLaterEligibility) -- todo remove this
            $ filter (\(Wallet item) -> item.code `elem` payLaterMap)
                allWallets
--    filter
--         (\(Wallet item) -> item.code `elem` payLaterMap)
--         allWallets

removePayLaterWallets :: Array PaymentMethodsEligibility -> Array Wallet -> Array Wallet
removePayLaterWallets payLaterEligibility allWallets =
    filter
        (\(Wallet item) -> not (item.code `elem` payLaterMap))
        allWallets



getStoredPayLaterWallets :: Array StoredWallet -> Array PaymentMethodsEligibility -> Array StoredWallet
getStoredPayLaterWallets allStoredWallets payLaterEligibility =
    map (\(StoredWallet (Instruments.Wallet item)) -> do
            let updatedBalance = getUpdatedBalance (itemName item) payLaterEligibility
            StoredWallet $ Instruments.Wallet
                { wallet : item.wallet
                , token : item.token
                , linked : item.linked
                , id : item.id
                , current_balance : maybe item.current_balance Just updatedBalance
                , last_refreshed : item.last_refreshed
                , object : item.object
                , currentBalance : maybe item.currentBalance Just updatedBalance
                , lastRefreshed : item.lastRefreshed
                , status : maybe Nothing Just $ getSIMPLstatus (itemName item) payLaterEligibility
                , error_code : item.error_code
                , metadata : Nothing
                , gateway_reference_id : Nothing
                }
        )
        -- $ filter (\(StoredWallet (Instruments.Wallet item)) -> not $ elem (itemName item) $ getIneligibleWallets payLaterEligibility)
            $ filter (\(StoredWallet (Instruments.Wallet item)) -> (itemName item) `elem` payLaterMap)
                allStoredWallets
    where
        itemName item = fromMaybe "" item.wallet

removeStoredPayLaterWallets ::  Array PaymentMethodsEligibility -> Array StoredWallet -> Array StoredWallet
removeStoredPayLaterWallets payLaterEligibility allStoredWallets =
    filter
        (\(StoredWallet (Instruments.Wallet item)) -> not ((itemName item) `elem` payLaterMap))
        allStoredWallets
    where
    itemName item = fromMaybe "" item.wallet

keepMandateEnabledWallets ::  MandateType -> Array Wallet -> Array Wallet
keepMandateEnabledWallets mandateType allWallets  =
  if mandateType == Required then
    allWallets #
      filter \(Wallet w) -> w.mandateSupport
  else allWallets

keepOnlyEnabledWallets :: Array Wallet -> Array String -> Array Wallet
keepOnlyEnabledWallets allWallets onlyEnabledWallet =
    if length onlyEnabledWallet == 0
        then allWallets
        else filter
                (\(Wallet item) -> (item.code `elem` onlyEnabledWallet))
                allWallets


getDefaultOptionWallet :: (Array StoredWallet) -> Maybe StoredWallet
getDefaultOptionWallet savedWallets =  foldl
                                        (\out sc@(StoredWallet wallet) ->
                                                case out of
                                                    Nothing -> Just sc
                                                    c  -> c
                                        )
                                        Nothing
                                        savedWallets


getDummyWallet :: Wallet
getDummyWallet = Wallet {
        code : ""
    ,   name : ""
    , directDebitSupport : false
    , mandateSupport : false
    , status : Nothing
    }

redirectionFlowWalletsList :: Array Wallet -> Array String -- Direct Debit Wallets to Follow Redirection Flow
redirectionFlowWalletsList wallets =
  map (\(Wallet wallet) -> wallet.code)
  $ filter (\(Wallet wallet) -> wallet.code `elem` redirectionWallets) wallets
  where
    redirectionWallets = ["SIMPL","OLAPOSTPAID"]



getDummyStoredWallet :: StoredWallet
getDummyStoredWallet = StoredWallet $ Instruments.Wallet{ wallet: Nothing,
        token: Nothing,
        linked: Nothing,
        id: "",
        current_balance: Nothing,
        last_refreshed: Nothing,
        object: Nothing,
        currentBalance: Nothing,
        lastRefreshed: Nothing,
        status: Nothing,
        error_code: Nothing,
        metadata : Nothing,
        gateway_reference_id : Nothing
    }

-------------------------------------------------- Eligibility Call for Pay Later Wallets --------------------------------------------------

newtype PayLaterEligibilityRequest =
    PayLaterEligibilityRequest
        { amount :: String
        , client_auth_token :: String
        , customer_id :: String
        , gateway_reference_id :: String
        }


newtype PayLaterEligibilityResponse =
    PayLaterEligibilityResponse
        { payment_methods_eligibility :: Array PaymentMethodsEligibility } -- Instruments.PaymentMethodsEligibility

newtype PaymentMethodsEligibility =
    PaymentMethodsEligibility
        { status :: String
        , payment_method_type :: String
        , payment_method :: String
        , is_eligible :: Boolean
        , description :: String
        , balance :: Maybe Number
        , gateway_error_message :: Maybe String
        }


addDisableWallets :: Array PaymentMethodsEligibility -> Array String
addDisableWallets payLaterEligibility =
  map (\(PaymentMethodsEligibility pme) -> pme.payment_method)
    $ filter (\(PaymentMethodsEligibility pme) -> not pme.is_eligible && pme.payment_method `elem` payLaterMap) payLaterEligibility



dummyPaymentMethodsEligibility :: String -> PaymentMethodsEligibility
dummyPaymentMethodsEligibility payment_method =
    PaymentMethodsEligibility
        { status : ""
        , payment_method_type : ""
        , payment_method
        , is_eligible : false
        , description : ""
        , balance : Nothing
        , gateway_error_message : Nothing
        }





derive instance genericPayLaterEligibilityRequest :: Generic PayLaterEligibilityRequest _
derive instance newtypePayLaterEligibilityRequest :: Newtype PayLaterEligibilityRequest _
instance decodePayLaterEligibilityRequest :: Decode PayLaterEligibilityRequest where decode = defaultDecode
instance encodePayLaterEligibilityRequest :: Encode PayLaterEligibilityRequest where encode = defaultEncode

derive instance genericPayLaterEligibilityResponse :: Generic PayLaterEligibilityResponse _
derive instance newtypePayLaterEligibilityResponse :: Newtype PayLaterEligibilityResponse _
instance decodePayLaterEligibilityResponse :: Decode PayLaterEligibilityResponse where decode = defaultDecode
instance encodePayLaterEligibilityResponse :: Encode PayLaterEligibilityResponse where encode = defaultEncode

derive instance genericPaymentMethodsEligibility :: Generic PaymentMethodsEligibility _
derive instance newtypePaymentMethodsEligibility :: Newtype PaymentMethodsEligibility _
instance decodePaymentMethodsEligibility :: Decode PaymentMethodsEligibility where decode = defaultDecode
instance encodePaymentMethodsEligibility :: Encode PaymentMethodsEligibility where encode = defaultEncode

instance getEligibilityDetails :: RestEndpoint PayLaterEligibilityRequest PayLaterEligibilityResponse where
   makeRequest reqBody@(PayLaterEligibilityRequest r) headers =
       defaultMakeRequest POST ((getUrl "") <> "/customers/" <> r.customer_id <> "/eligibility") headers reqBody
   decodeResponse body = defaultDecodeResponse body
