module Main where

import Prelude

import Control.Monad.State as S
import MockCore (appFlow)
import Data.Either (Either(..))
import Data.Function.Uncurried (runFn2)
import Effect (Effect)
import Effect.Aff (launchAff_, makeAff, nonCanceler)
import Effect.Aff.AVar (new) as AVar
import Engineering.Helpers.Commons (callAPI', mkNativeRequest, showUI')
import Engineering.OS.Permission (checkIfPermissionsGranted, requestPermissions)
import Foreign.Object (empty)
import Payments.Core.Commons (getCheckoutDetails)
import Presto.Core.Language.Runtime.API (APIRunner)
import Presto.Core.Language.Runtime.Interpreter (PermissionCheckRunner, PermissionRunner(..), PermissionTakeRunner, Runtime(..), UIRunner, run)

-- runFreeAndNativeFlows :: ∀ a b. Discard a => Bind b => Applicative b => b a -> b Unit
-- runFreeAndNativeFlows freeFlow = do
--   freeFlow
--   pure $ unit

main :: Effect Unit
main = do
  let runtime  = Runtime uiRunner permissionRunner apiRunner
  isActivityRecreated <- (getCheckoutDetails >>= \cd -> pure $ cd.activity_recreated == "true")
  let freeFlow = S.evalStateT (run runtime (appFlow isActivityRecreated))
  _ <- launchAff_ (AVar.new empty >>= freeFlow)
  pure unit
  where

  uiRunner :: UIRunner
  uiRunner a = makeAff (\cb -> do
      _ <- runFn2 showUI' (cb <<< Right) ""
      pure $ nonCanceler
                          )
  permissionCheckRunner :: PermissionCheckRunner
  permissionCheckRunner = checkIfPermissionsGranted

  permissionTakeRunner :: PermissionTakeRunner
  permissionTakeRunner = requestPermissions

  permissionRunner :: PermissionRunner
  permissionRunner = PermissionRunner permissionCheckRunner permissionTakeRunner

  apiRunner :: APIRunner
  apiRunner request =
     makeAff (\cb -> do

      _ <-callAPI' (cb <<< Left) (cb <<< Right) (mkNativeRequest request)
      pure $ nonCanceler
    )
