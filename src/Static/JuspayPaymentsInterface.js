window.PAYMENT_PAGE_URI = "http://0.0.0.0:8083"; // This value will be updated by release script

function getPayload(opts) {
  if (opts && Object.keys(opts).length >= 0) {
    var str = "?";
    for (let key in opts) {
      const value = opts[key] || ""
      if (typeof value === "object") {
        str += key.toString() + "=" + JSON.stringify(value) + "&";
      } else {
        str += key.toString() + "=" + value.toString() + "&";
      }
    }
    return str.slice(0, -1);
  } else {
    return "";
  }
}

function updateHeight(entry){
  if (document.getElementById(entry) && window.innerHeight>700){
    document.getElementById(entry).style.height = "700px";
  }
  else if(document.getElementById(entry) && window.innerHeight<420){
    document.getElementById(entry).style.height = "420px";
  }
 
}


var ifrm = document.createElement("iframe");
ifrm.setAttribute("src", window.PAYMENT_PAGE_URI + "/appIframe.html");
ifrm.setAttribute("id","logFrame")
ifrm.style.width = "0px";
ifrm.style.height = "0px";
document.body.appendChild(ifrm);


function parseTosnake (payload) {
  var obj=payload;
  if (obj.merchantId){
    obj.merchant_id=obj.merchantId;
    delete obj["merchantId"];
  }
  if (obj.clientId){
    obj.client_id=obj.clientId;
    delete obj["clientId"];
  }
  if (obj.merchantKeyId){
    obj.merchant_key_id=obj.merchantKeyId;
    delete obj["merchantKeyId"];
  }
  if (obj.customerId){
    obj.customer_id=obj.customerId;
    delete obj["customerId"];
  }
  if (obj.orderId){
    obj.order_id=obj.orderId;
    delete obj["orderId"];
  }
  if (obj.orderDetails){
    obj.order_details=obj.orderDetails;
    delete obj["orderDetails"];
  }
  if (obj.customer_phone){
    obj.customer_mobile=obj.customer_phone;
    delete obj["customer_phone"];
  }
  return obj;
}

let loadRetryCount = 0;
function tryToLoadIframe(url, entryPointID) {
  const elem = document.getElementById(entryPointID);

  if (!elem) {
    if (loadRetryCount > 10) {
      console.error("Unable to find ID:", entryPointID, "in page after 10 retries. JuspayPaymentsSdk will not initialise.");
      return;
    }
    setTimeout(() => {
      tryToLoadIframe(url, entryPointID);
    }, 200);
    loadRetryCount++;

    return;
  }
  elem.style.height =  '0px';
  elem.innerHTML =
    "<iframe id='juspay_iframe' frameborder='0' width='100%' height='100%' src='" +
    url +
    "' ></iframe>";
  window.JuspayPaymentsSdk.IFRAME = document.getElementById(
    "juspay_iframe"
  ).contentWindow;
}

var localHistoryClone =[]
function getJuspayPaymentsSdk() {
  var JuspayPaymentsSdk = {
    IFRAME:null,
    PaymentConstants: {
      MERCHANT_ID: "merchant_id",
      CLIENT_ID: "client_id",
      TRANSACTION_ID: "transaction_id",
      ORDER_ID: "order_id",
      AMOUNT: "amount",
      CUSTOMER_ID: "customer_id",
      CUSTOMER_EMAIL: "customer_email",
      CUSTOMER_MOBILE: "customer_mobile",
      ENV: "env",
      SERVICE: "service",
      CLIENT_AUTH_TOKEN: "client_auth_token",
      END_URLS: "end_urls",
      ITEM_COUNT: "item_count",
      ORDER_DETAILS: "order_details",
      SIGNATURE: "signature",
      OFFER_DETAILS: "offer_details",
      PAYMENT_LOCKING: "payment_locking",
      MERCHANT_KEY_ID: "merchant_key_id",
      MERCHANT_URI: "merchant_uri",
      SDK_META: "sdk_meta",
      ENTRY_POINT: "entry_point",
      INTEGRATION: "integration",
      SDK_PAYLOAD: "sdk_payload",
      GUEST_LOGIN_URL: "guest_login_url",
      LANGUAGE: "language"
    },
    ENVIRONMENT: {
      SANDBOX: "sandbox",
      DEV: "dev",
      PRODUCTION: "production",
      PRE_PROD: "pre_prod"
    }
  };
 
  // const PAYMENT_PAGE_URI = "http://0.0.0.0:8083"; // This value will be updated by release script

  let startAppPayload = null

  function receiveMessage(event) {
    //if (event.origin !== PAYMENT_PAGE_URI)
    // return;
    console.warn("event is ", event)
    if (event && event.data) {
      const msg = event.data;
      var obj = {}
      if((typeof msg) == "string"){
        obj = JSON.parse(msg);
      }
      if(obj.type == "heightUpdate"){
        if (startAppPayload) {
          // JuspayPaymentsSdk.IFRAME.style.height = obj.value;
          document.getElementById(startAppPayload.entry_point_id).style.height = String(obj.value) + 'px';
          // document.getElementById("holder").style.height = obj.value;
        }
      }
      if (msg.event == "redirectPost") {
        var url = msg.data.url;
        var params = msg.data.params;

        fetch(url, {
          method: 'POST',
          body: params
        })
      }
      if (msg.event == "paymentAttempt"){
        eventHandlers.map(fn => fn(msg, {
                   sendResponse: args => {
                     var pay = {
                       fn : msg.callback,
                       args: args
                     }
                     JuspayPaymentsSdk.onMerchantEvent("paymentAttempt",pay);
                   }
                 }));
      }
      if (msg.event == "invokeErrorHandling"){
        eventHandlers.map(fn => fn(msg));
      }
      if (msg.event =="sendEventToHome"){
        eventHandlers.map(fn => fn(msg));
      }
      if (msg.event =="setPageTitle"){
        eventHandlers.map(fn => fn(msg));
        localHistoryClone = (msg.history);
      }
      if (msg.event == "goToPreviousPage"){
        history.back();
      }
      if (msg.event == "redirectGet") {
        
        if (msg.url){
          var url = msg.url+"&session="+msg.guid+"&sn="+msg.sn;
          var integration = msg.integration;
          var entryPointID = msg.entry_point_id;
          if (integration && integration != 'iframe') {
            window.parent.location.href = url;
          } 

        }
        else{
          location.href = msg.data.url;
        }
       
            
          
      }
      if (msg.event == "ExitSDK") {
        const data = JSON.parse(msg.data)
        if (startAppPayload) {
          if (data.error) {
  
            if (startAppPayload.onPaymentAttemptFailure) {
              delete data.return_url;
              delete data.txnUUid;
              delete data.mid;
              startAppPayload.onPaymentAttemptFailure(data)
            } else {
  
              if (startAppPayload.environment == "prod"){
                var url = "https://api.juspay.in/pay/finish/" + data["mid"].toString() + "/" + data["txnUUid"].toString();
                window.parent.location.href = url;
              }else{
              var url = "https://sandbox.juspay.in/pay/finish/" + data["mid"].toString() + "/" + data["txnUUid"].toString();
              window.parent.location.href = url;
              }
            }
          } else {
            const payload = (typeof(data.payload) == "string"? JSON.parse(data.payload) : data.payload);
            if (payload.status == "backpressed") {
              if(startAppPayload.onPaymentCancel){
                startAppPayload.onPaymentCancel(data);
              } 
              // else {
              //   history.back();
              // }
            } else {
              if(startAppPayload.onPaymentSuccess){
                startAppPayload.onPaymentSuccess(data);
              }
            }
          }
        }
        
      }
    }
  }

  window.addEventListener("message", receiveMessage, false);

  function getStartAppPayload(param) {
    var opts = param.requestId ? param.payload : param;
    
    if (typeof opts == "string") {
      try {
        opts = JSON.parse(opts)
      } catch (e) {
        return null;
      }
    }


    opts=parseTosnake(opts);
    return opts
  }
  var prevKnownWidth = -1;
  JuspayPaymentsSdk.startApp = function (param) {
    if (!param.payload && param.requestId) {
      console.error("Payload not found in the bundle");
      return;
    }

    startAppPayload = getStartAppPayload(param)
    if (!startAppPayload) {
      console.error("Invalid payload");
      return;
    }
    if (!startAppPayload.client_id && !startAppPayload.clientId) {
      console.error("Please send client_id in payload");
      return;
    }
    
    var merchant = startAppPayload.client_id || startAppPayload.clientId;

    


    var integration;
    var entryPointID;
    if (startAppPayload.entry_point_id) {
      entryPointID = startAppPayload.entry_point_id;
      integration = "iframe";
      updateHeight(startAppPayload.entry_point_id);
    } 
    else if (param.entry_point_id){
      entryPointID = param.entry_point_id;
      integration = "iframe";
      updateHeight(startAppPayload.entry_point_id);
    }
    else {
      entryPointID = "";
      integration = "redirection"
    }


    if (!startAppPayload.environment) {
      console.error("No value found for environment");
      return;
    }
    if (entryPointID !== ""){
      var entry = entryPointID;
      var updateWidth = setInterval(function(){
        var doc = document.getElementById(entry);

        if(doc){
          width = doc.getBoundingClientRect().width;
          if ((width>= 700 && prevKnownWidth < 700) || (width < 700 && prevKnownWidth >= 700)){
            prevKnownWidth = width;
            JuspayPaymentsSdk.startApp(startAppPayload);
          }
        }
      }, 200)
    }
    var sdk_payload = {
      merchant_uri: window.josOrigin,
      entry_point: entryPointID,
      integration: integration,
      env: startAppPayload.environment,
      client_auth_token: ""
    };

    if (!startAppPayload.merchant_key_id) {
      console.error("No value found for merchantKeyId");
      return;
    }
    var params = {
      merchant_id: startAppPayload.merchant_id,
      merchant_key_id: startAppPayload.merchant_key_id,
      signature: encodeURIComponent(decodeURIComponent(startAppPayload.signature)),
      signature_payload: encodeURIComponent(startAppPayload.order_details),
      sdk_payload: sdk_payload,
      merchant: merchant,
      guest_login_url: startAppPayload.guest_login_url,
      customer_id: startAppPayload.customer_id,
      language: startAppPayload.language,
      popUp: startAppPayload.handlePaymentAttempt,
      addMerchantView: startAppPayload.addMerchantView
    };

    const queryString = getPayload(params)

    var url = window.PAYMENT_PAGE_URI + "/jos.html" + queryString + "&widget_name=paymentPage";

    var eventObj = {
      name : "startApp",
      url : url,
      integration : integration,
      entry_point_id: entryPointID,
      payload : params
    }

    var iFrame = document.getElementById('logFrame').contentWindow;
    iFrame.postMessage(JSON.stringify(eventObj),"*")

    if (integration === 'iframe') {
      tryToLoadIframe(url, entryPointID);
    } 
  };

  JuspayPaymentsSdk.preFetch = function (cliendId) {
    // implement order fetch
  };

  JuspayPaymentsSdk.addMerchantView = function (view) {
    //send this to iframe
  };

  const eventHandlers = []

  JuspayPaymentsSdk.onEvent = function (fn) {
    const newIndex = eventHandlers.length;
    eventHandlers.push(fn)
    return () => {
      eventHandlers.splice(newIndex, 1);
    };
    //send this to iframe
  };

  JuspayPaymentsSdk.onMerchantEvent = function (event, payloadN) {
    var val = {
      type: event,
      value: payloadN
    };
    JuspayPaymentsSdk.IFRAME = document.getElementById('juspay_iframe')
    JuspayPaymentsSdk.IFRAME.contentWindow.postMessage(JSON.stringify(val), "*");
  };

  JuspayPaymentsSdk.sendEvent = JuspayPaymentsSdk.onMerchantEvent;

  // Return:
  // - true if we will handle backpress
  // - false if merchant should handle it
  JuspayPaymentsSdk.handleBackPress = function () {
    const willHandle = localHistoryClone.length > 0;
    if (willHandle) {
      JuspayPaymentsSdk.sendEvent('goBack', {});
    }
    return willHandle;
  }

  return JuspayPaymentsSdk;
}

if (!window.JuspayPaymentsSdk) {
  window.JuspayPaymentsSdk = getJuspayPaymentsSdk();
}

if (!window.PIL) {
  // This means this script has been loaded via version pinning
  window.PIL = {
    load: () => Promise.resolve(window.JuspayPaymentsSdk)
  }
}