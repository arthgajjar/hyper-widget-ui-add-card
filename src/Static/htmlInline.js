function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it.return != null) it.return(); } finally { if (didErr) throw err; } } }; }

            function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

            function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

            function callOnLoad() {
            window.onunload = function () {};

            var body = document.body;
            var clickPayload = {
                type: "click"
            };
            body.addEventListener('click', function (e) {
                window.postMessage(JSON.stringify(clickPayload), "*");
            });
            // window.__OS = "WEB";
            var query = window.location.href.split("?")[1] || "";
            var queriesObj = query.split("&").reduce(function (acc, str) {
                var key = str.split("=")[0];
                var val = decodeURIComponent(str.split("=")[1]);
                acc[key] = val;
                return acc;
            }, {});
            var missingRequiredKeys = ["signature_payload", "signature", "merchant_key_id", // "customer_id",
            "sdk_payload"].filter(function (key) {
                return !queriesObj[key];
            });

            if (missingRequiredKeys.length > 0) {
                console.error("Missing required keys: ", missingRequiredKeys);
            }

            var signaturePayloadObj = JSON.parse(queriesObj.signature_payload);
            var sdkPayloadObj = JSON.parse(queriesObj.sdk_payload);
            window.__payload = {
                entry_point: sdkPayloadObj.entry_point,
                amount: signaturePayloadObj.amount,
                merchant_id: queriesObj.merchant_id,
                environment: sdkPayloadObj.env,
                customer_phone_number: signaturePayloadObj.customer_phone,
                customer_email: signaturePayloadObj.customer_email,
                order_id: signaturePayloadObj.order_id,
                customer_id: queriesObj.customer_id,
                merchant_ui: queriesObj.merchant,
                order_details: queriesObj.signature_payload,
                merchant_key_id: queriesObj.merchant_key_id,
                sdk_payload: queriesObj.sdk_payload,
                signature: queriesObj.signature,
                guest_login_url: queriesObj.guest_login_url,
                widget_name: queriesObj.widget_name //"upiWaitingScreen"

            };
            window.__upi = queriesObj.upi_id;
            var order_details = signaturePayloadObj;
            var key = ["guid", String(queriesObj.merchant_id), String(order_details.order_id), String(order_details.timestamp)].join(":");

            if (queriesObj.session) {
                localStorage.setItem(key, queriesObj.session);
            }

            var key_sn = ["sn", String(queriesObj.merchant_id), String(order_details.order_id), String(order_details.timestamp)].join(":");

            if (queriesObj.sn) {
                localStorage.setItem(key_sn, queriesObj.sn);
            }

            var newUrl = window.location.href.split("&session");
            history.replaceState(null, null, newUrl[0]); // Remove this check once Sandbox resolves bb_staging
            // if (queriesObj.merchant_id=="bb_staging"){
            //     window.__payload.merchant_id="olacabs_sandbox"
            // }

            if (queriesObj.addMerchantView) {
                window.addMerchantView = queriesObj.addMerchantView;
            } else {
                window.addMerchantView = "";
            }

            if (queriesObj.language) {
                window.language = queriesObj.language;
            } else {
                window.language = "english";
            }

            if (queriesObj.popUp) {
                window.popUp = queriesObj.popUp;
            }

            var merchant_name_arr = queriesObj.merchant.split("_");
            if (merchant_name_arr.length > 1) merchant_name_arr.pop();
            var prevKnownHeight = 0;
            var merchantForUpdateHeight = ["zee5", "bigbasket"];
            var updateHeight = setInterval(function () {
                var isMobile = window.innerWidth < 700;
                var isDesktop = !isMobile; // const isDefault = false;

                if (window.location === window.parent.location) {
                return; // console.log("The page is in an iFrame"); 
                } // if(!(merchant_name && merchantForUpdateHeight.includes(merchant_name.toLowerCase()))){
                //     return;
                // }


                var mid = JSON.parse(window.__payload.order_details)["merchant_id"];
                var isDefaultExist = localStorage.getItem(mid + ":defOption") != undefined;
                var docList = document.getElementsByClassName("screenRoot"); // var list = [...docList].map(el => Number(el.id));
                // list = list.sort((a,b) => {return (a-b)});

                var list = [];

                var _iterator = _createForOfIteratorHelper(docList),
                    _step;

                try {
                for (_iterator.s(); !(_step = _iterator.n()).done;) {
                    var el = _step.value;
                    list.push(Number(el.id));
                }
                } catch (err) {
                _iterator.e(err);
                } finally {
                _iterator.f();
                }

                var max = -1;

                for (var i = 0; i < list.length; i++) {
                if (max < list[i]) {
                    max = list[i];
                }
                }

                doc = document.getElementById(max);

                if (!doc) {
                return;
                }

                var height = doc.getBoundingClientRect().height;
                var def_height = isDefaultExist ? 900 : 600;

                if (isDesktop) {
                if (window.isPaymentLocked && false) {
                    height = Math.min(height, def_height);
                } else {
                    height = def_height;
                }
                }

                if (prevKnownHeight == height) {
                return;
                } else {
                prevKnownHeight = height;
                }

                var heightPayload = {
                type: "heightUpdate",
                value: height
                };
                parent.postMessage(JSON.stringify(heightPayload), "*");
            }, 200); // var prevKnownWidth = -1
            // var updateWidth = setInterval(function(){
            //     var doc = true;
            //     if(doc){
            //     width = body.getBoundingClientRect().width;
            //     if ((width != prevKnownWidth)){
            //         prevKnownWidth = width;
            //         try {
            //                 if(window.isObject(window.onWidthUpdate) && window.onWidthUpdate[window.__dui_screen]) {
            //                     window.onWidthUpdate[window.__dui_screen](offer_details || "");
            //                 }
            //                 } catch (e) {
            //                 console.log(e);
            //                 }
            //     }
            //     }
            // }, 200)
            // console.log("merchantForUpdateHeight.indexOf(merchant_name) >= 0", merchantForUpdateHeight.indexOf(merchant_name) >= 0)

            var merchant_name = merchant_name_arr.toString();
            document.title = merchant_name.charAt(0).toUpperCase() + merchant_name.slice(1) + " Payment Page";
            }

            callOnLoad();